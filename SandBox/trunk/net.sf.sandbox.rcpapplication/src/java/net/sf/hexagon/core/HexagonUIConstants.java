//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.core;

// - IMPORT SECTION .........................................................................................
import net.sf.core.app.CoreUIConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public interface HexagonUIConstants extends CoreUIConstants {
	// - C O L O R   D E F I N I T I O N S
	// - F O N T   D E F I N I T I O N S
	//- B A C K G R O U N D   C O L O R S
}

// - UNUSED CODE ............................................................................................
