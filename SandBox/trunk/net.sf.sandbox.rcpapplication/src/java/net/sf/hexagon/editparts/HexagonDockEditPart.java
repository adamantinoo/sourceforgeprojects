//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import org.eclipse.gef.EditPolicy;

import net.sf.gef.core.editparts.AbstractNodeEditPart;
import net.sf.gef.core.policies.GNodePolicy;
import net.sf.hexagon.figures.HexagonDockFigure;
import net.sf.hexagon.figures.HexagonPlanetFigure;
import net.sf.hexagon.models.HexagonDock;
import net.sf.hexagon.models.HexagonPlanet;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonDockEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.sandbox.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonDockEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (HexagonPlanet.NAME_CHANGED.equals(prop)) {
			// - The references to the model and figure objects.
			final HexagonPlanetFigure fig = (HexagonPlanetFigure) getFigure();
			HexagonDock planet = getCastedModel();
			fig.setName(planet.getName());
			return;
		}
		//		if (HexagonPlanet.HOVER_ACTIVATION.equals(prop)) {
		//			// - The references to the model and figure objects.
		//			final HexagonPlanetFigure fig = (HexagonPlanetFigure) getFigure();
		//			fig.refreshContents();
		//			return;
		//		}
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		HexagonDockFigure fig = (HexagonDockFigure) this.getFigure();
		HexagonDock theModel = this.getCastedModel();

		// - Update figure visuals from current model data.
		//		fig.setCoordinates(theModel.getPosition());
		fig.setName(theModel.getName());
	}

	@Override
	public HexagonDock getCastedModel() {
		return (HexagonDock) getModel();
	}
}

// - UNUSED CODE ............................................................................................
