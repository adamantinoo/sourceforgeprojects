//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.Disposable;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.ISelectableFigure;
import net.sf.hexagon.models.HexagonDock;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonDockFigure extends HexagonShape implements ISelectableFigure, Disposable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger							= Logger.getLogger("net.sf.sandbox.figures");
	private static Font		FONT_DEFAULT				= new Font(Display.getDefault(), "Tahoma", 7, SWT.BOLD);
	private static Color	COLOR_LIGHT_GREEN		= new Color(Display.getDefault(), 0x80, 0xFF, 0x80);
	private static Color	COLOR_MEDIUM_GREEN	= new Color(Display.getDefault(), 0x00, 0xE0, 0x00);
	// - F I E L D - S E C T I O N ............................................................................
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int						selected						= EditPart.SELECTED_NONE;
	private HexagonDock		planetModel					= null;
	private final Figure	planetContainer			= new Figure();
	private final Figure	dockContainer				= new Figure();
	private final Figure	factoryContainer		= new Figure();

	private Label					nameLabel;
	private boolean				hovering						= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonDockFigure(HexagonDock model) {
		super();
		planetModel = model;
		setOpaque(false);
		initialize();
		refreshContents();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return selected;
	}

	public boolean getHover() {
		return hovering;
	}

	public void refreshContents() {
		this.removeAll();
		addPlanetContents();
		if (isSelected()) {
			addSelectedView();
		}

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	private void addSelectedView() {
		this.setBackgroundColor(COLOR_LIGHT_GREEN);
	}

	private void addHoverView() {
		this.setBackgroundColor(COLOR_MEDIUM_GREEN);
	}

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == getSelected()) || (EditPart.SELECTED == getSelected()))
			return true;
		else return false;
	}

	public boolean isHover() {
		return hovering;
	}

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(final int value) {
		selected = value;
	}

	public void setHover(boolean value) {
		hovering = value;
	}

	private void initialize() {
		removeAll();
		setLayout();
		//		setBorder(new LineBorder(1));
		this.setBackgroundColor(ColorConstants.white);
	}

	protected void addPlanetContents() {
		nameLabel = new Label(planetModel.getName());
		nameLabel.setFont(FONT_DEFAULT);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.CENTER;
		gridData.verticalAlignment = SWT.CENTER;
		nameLabel.setLabelAlignment(PositionConstants.CENTER);
		this.add(nameLabel);
		this.setConstraint(nameLabel, gridData);
	}

	private void setLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.numColumns = 1;
		setLayoutManager(grid);
	}

	@Override
	public void dispose() {
		FONT_DEFAULT = null;
		//		if (planetContainer instanceof Disposable) {
		//			((Disposable) planetContainer).dispose();
		//		}
	}

	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	public void setCoordinates(final Point location) {
		this.setLocation(location);
	}
}
// - UNUSED CODE ............................................................................................
