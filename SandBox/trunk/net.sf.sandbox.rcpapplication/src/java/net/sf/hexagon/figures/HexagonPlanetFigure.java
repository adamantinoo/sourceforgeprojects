//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.Disposable;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.hexagon.core.HexagonUIConstants;
import net.sf.hexagon.models.HexagonPlanet;
import net.sf.hexagon.models.HexagonalPosition;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This figure holds the representation of a Planet. A planet has many presentation elements that are not
 * fully visible when instantiated but that may appear when the element is selected of when the mouse hovers
 * over its location. So the size of a planet is much more bigger than expected and the location of the center
 * is far away from the border depending if the compressed of hover versions. <br>
 * There are six cells that surround the main center planet cell location. Each of that cells is also another
 * <code>HexagonBaseShape</code> that may be visible or not and that have a relative location to the main cell
 * planet location.
 */
public class HexagonPlanetFigure extends SelectableFigure implements Disposable {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger							= Logger.getLogger("net.sf.sandbox.figures");
	protected static final String		PLANET_ICON				= "icons/Planet.gif";
	protected static final String		FACTORY_ICON			= "icons/Planet.gif";
	protected static final String		MINE_ICON					= "icons/Planet.gif";

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Direct reference to the underlying model to reduce the need to pass and replicate the model information
	 * through the EditPart.
	 */
	private HexagonPlanet						planetModel				= null;
	/**
	 * Main reference to the graphical hexagon that represents this Planet model.
	 * 
	 */
	private final HexagonBaseShape	planetContainer		= new HexagonBaseShape() {
																											public Label	nameLabel;	// Label to contain the Code of the Planet.

																											@Override
																											protected void createContents() {
																												this.setIcon(HexagonPlanetFigure.PLANET_ICON);
																												nameLabel = new Label("X");
																												nameLabel.setFont(HexagonUIConstants.FONT_DEFAULT_BOLD);
																												nameLabel.setLabelAlignment(PositionConstants.CENTER);
																												nameLabel.setOpaque(false);
																												//this.add(nameLabel);
																												//	this.getLayout().setConstraint(nameLabel, 	new Rectangle(2, 2, 14, 14));
																											}

																											public void setName(final String newName) {
																												if (null != newName) nameLabel.setText(newName);
																											}

																										};
	/**
	 * References to the other hexagonal figures where to draw the existence of the Factory or Docking or any
	 * other Planet facilities.
	 */
	private final HexagonBaseShape	factoryContainer	= new HexagonBaseShape() {

																											@Override
																											protected void createContents() {
																												this.setIcon(HexagonPlanetFigure.FACTORY_ICON);
																											}
																										};
	private final HexagonBaseShape	mineContainer			= new HexagonBaseShape() {

																											@Override
																											protected void createContents() {
																												this.setIcon(HexagonPlanetFigure.MINE_ICON);
																											}
																										};
	/**
	 * State of the hovering over this element. If the <code>hovering</code> is <code>true</code> then we have
	 * to show the hover view of the object and make visible other parts of the figure.
	 */
	private boolean									hovering					= false;
	/** Hexagonal coordinate of the hexagonal grid specially created for this map. */
	private HexagonalPosition				location6X				= new HexagonalPosition(0, 0);

	//- F I G U R E   C O N T E N T S
	//	private Label									nameLabel;															// Label to contain the Code of the Planet.
	private XYLayout								baseLayout;																			// Layout manager to allow free positioning of the elements.

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonPlanetFigure(final HexagonPlanet model) {
		super();
		planetModel = model;
		this.setOpaque(false);

		//- Initialize the Figure contents to the default values not using the model. This is the EMPTY state.
		this.initialize();
		this.addHandlers();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void dispose() {
		//		HexagonPlanetFigure.FONT_DEFAULT = null;
		if (factoryContainer instanceof Disposable) ((Disposable) factoryContainer).dispose();
		if (mineContainer instanceof Disposable) ((Disposable) mineContainer).dispose();
	}

	public boolean getHover() {
		return hovering;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[HexagonPlanetFigure ");
		buffer.append("location6X=").append(location6X.toString()).append("");
		buffer.append("\nplanetModel=").append(planetModel.toString()).append("");
		buffer.append("]\n");
		return buffer.toString();
	}

	/**
	 * Set the new tile position. At the completion update the XY real coordinates obtained from the hexagonal
	 * tile position.
	 */
	public void setPosition(final HexagonalPosition newPoint) {
		if (null != newPoint) {
			location6X = newPoint;

			//- Set the correct location for the upper-left corner.
			Point center = location6X.hexagonal2D();
			Point topLeft = new Point(center.x - (18 * 3) / 2, center.y - (18 * 3) / 2);
			this.setLocation(topLeft);
			//- Correct location for

			//- Update the rest of the locations of the adjacent presentation elements.
			//			factoryContainer.setPosition(location6X.getColumn() - 1, location6X.getRow() + 1);
			//			mineContainer.setPosition(location6X.getColumn() + 1, location6X.getRow() - 1);
			//- Calculate the XY locations for every part.
			planetContainer.setLocation(center);
			//	baseLayout.setConstraint(planetContainer, new Rectangle(center.x - 5, center.y - 6, 10, 12));
			center = new HexagonalPosition(location6X.getColumn() - 1, location6X.getRow() + 1).hexagonal2D();
			factoryContainer.setLocation(center);
			//	baseLayout.setConstraint(factoryContainer, new Rectangle(center.x - 5, center.y - 6, 10, 12));
			center = new HexagonalPosition(location6X.getColumn() + 1, location6X.getRow() - 1).hexagonal2D();
			mineContainer.setLocation(center);
			//	baseLayout.setConstraint(mineContainer, new Rectangle(center.x - 5, center.y - 6, 10, 12));
		}
		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		return new Dimension(18 * 3, 18 * 3);
	}

	public boolean isHover() {
		return hovering;
	}

	public void refreshContents() {
		//- Set the default state for contents.
		this.setBackgroundColor(ColorConstants.white);
		//		factoryContainer.setVisible(false);
		//		mineContainer.setVisible(false);

		if (this.isSelected()) this.addSelectedView();
		if (this.isHover()) this.addHoverView();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	public void setCoordinates(final Point location) {
		this.setLocation(location);
	}

	public void setHover(final boolean value) {
		hovering = value;
	}

	public void setName(final String newName) {
		//		if (null != newName) planetContainer.   .n  setName(newName);
	}

	@Override
	protected boolean useLocalCoordinates() {
		return false;
	}

	protected void addHandlers() {
		//- Add any other handlers configured for this object.
		this.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(final MouseEvent me) {
			}

			public void mouseEntered(final MouseEvent me) {
			}

			@Override
			public void mouseExited(final MouseEvent me) {
				//			setSelected(EditPart.SELECTED_NONE);
				HexagonPlanetFigure.this.setHover(false);
				HexagonPlanetFigure.this.refreshContents();
			}

			@Override
			public void mouseHover(final MouseEvent me) {
				HexagonPlanetFigure.this.setHover(true);
				HexagonPlanetFigure.this.refreshContents();
			}

			public void mouseMoved(final MouseEvent me) {
			}
		});
	}

	protected void createContents() {
		this.add(planetContainer);
		planetContainer.setVisible(true);
		//	baseLayout.setConstraint(planetContainer, new Rectangle(0, 0, 18, 18));
		this.add(factoryContainer);
		//		factoryContainer.setVisible(false);
		//		baseLayout.setConstraint(factoryContainer, new Rectangle(0, 0, 18, 18));
		this.add(mineContainer);
		//	mineContainer.setVisible(false);
		//	baseLayout.setConstraint(mineContainer, new Rectangle(0, 0, 18, 18));
	}

	protected void initialize() {
		this.removeAll();
		this.setLayout();
		this.createContents();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	//	private void addPlanetContents() {
	//		nameLabel = new Label(planetModel.getName());
	//		nameLabel.setFont(HexagonPlanetFigure.FONT_DEFAULT);
	//		//		GridData gridData = new GridData();
	//		//		gridData.horizontalAlignment = SWT.CENTER;
	//		//		gridData.verticalAlignment = SWT.CENTER;
	//		nameLabel.setLabelAlignment(PositionConstants.CENTER);
	//		this.add(nameLabel);
	//		baseLayout.setConstraint(nameLabel, new Rectangle(2, 2, 14, 14));
	//	}

	protected void setLayout() {
		baseLayout = new XYLayout();
		this.setLayoutManager(baseLayout);
	}

	private void addHoverView() {
		this.setBackgroundColor(HexagonUIConstants.COLOR_MEDIUM_GREEN);
		factoryContainer.setVisible(true);
		mineContainer.setVisible(true);
	}

	private void addSelectedView() {
		this.setBackgroundColor(HexagonUIConstants.COLOR_LIGHT_BLUE);
		factoryContainer.setVisible(true);
		mineContainer.setVisible(true);
	}
}
// - UNUSED CODE ............................................................................................
