//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.models;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonPlanet extends HexagonBaseModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long		serialVersionUID	= 4054064011374006170L;
	//	private static Logger	logger				= Logger.getLogger("net.sf.sandbox.models");

	// - P R O P E R T I E S ..................................................................................
	public static final String	NAME_CHANGED			= HexagonPlanet.class.getSimpleName() + ".NAME_CHANGED";
	//	public static final String	HOVER_ACTIVATION	= HexagonPlanet.class.getSimpleName() + ".HOVER_ACTIVATION";

	// - F I E L D - S E C T I O N ............................................................................
	private String							name							= "NOT SET";
	private boolean							hasFactory				= false;
	private int									waterStorage			= 0;
	private int									oilStorage				= 0;
	private int									ironStorage				= 0;
	private int									fuelStorage				= 0;
	private final boolean				showDocks					= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonPlanet(String newName) {
		name = newName;
		//		HexagonDock planet = new HexagonDock("D1");
		//		planet.setLocation(new Abstract2DLocation(60 - 18, 60 - 18));
		//		addChild(planet);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getWaterStorage() {
		return waterStorage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWaterStorage(int waterStorage) {
		this.waterStorage = waterStorage;
	}

	public int getOilStorage() {
		return oilStorage;
	}

	public void setOilStorage(int oilStorage) {
		this.oilStorage = oilStorage;
	}

	public int getIronStorage() {
		return ironStorage;
	}

	public void setIronStorage(int ironStorage) {
		this.ironStorage = ironStorage;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[HexagonPlanet ");
		buffer.append("name=").append(getName()).append(", ");
		buffer.append("waterStorage=").append(waterStorage).append("");
		buffer.append("\n     position(col,row)=").append(this.getPosition().getColumn()).append(" - ").append(
				this.getPosition().getRow()).append(", ");
		buffer.append("]\n");
		return buffer.toString();
	}

	public int getFuelStorage() {
		return fuelStorage;
	}

	public void setFuelStorage(int fuelStorage) {
		this.fuelStorage = fuelStorage;
	}

	//	public void activateHover(boolean hovering) {
	//		if (hovering) {
	//			showDocks = true;
	//			//- Tell the parent to redisplay the list of children that may have changed with the hover activation
	//			IGEFNode par = this.parent;
	//			int d = 0;
	//			((FigureDeveloperModelStore) this.parent).firePropertyChange(HOVER_ACTIVATION, false, true);
	//			firePropertyChange(HOVER_ACTIVATION, false, true);
	//		} else {
	//			showDocks = false;
	//			firePropertyChange(HOVER_ACTIVATION, true, false);
	//		}
	//	}

	//	@Override
	//	public Vector<IGEFNode> getChildren() {
	//		if (this.showDocks)
	//			return super.getChildren();
	//		else return new Vector<IGEFNode>();
	//	}

	public void activateFactory(boolean flag) {
		hasFactory = flag;
	}
}
// - UNUSED CODE ............................................................................................
