//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import org.eclipse.gef.EditPolicy;

import net.sf.gef.core.editparts.AbstractNodeEditPart;
import net.sf.gef.core.policies.GNodePolicy;
import net.sf.hexagon.figures.HexagonPlanetFigure;
import net.sf.hexagon.models.HexagonPlanet;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonPlanetEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.sandbox.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonPlanetEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (HexagonPlanet.NAME_CHANGED.equals(prop)) {
			// - The references to the model and figure objects.
			final HexagonPlanetFigure fig = (HexagonPlanetFigure) getFigure();
			HexagonPlanet planet = getCastedModel();
			fig.setName(planet.getName());
			return;
		}
		//		if (HexagonPlanet.HOVER_ACTIVATION.equals(prop)) {
		//			refreshChildren();
		//			return;
		//		}
	}

	//	@Override
	//	protected List<IGEFNode> getModelChildren() {
	//		HexagonPlanet theModel = getCastedModel();
	//		return theModel.getChildren();
	//	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		HexagonPlanetFigure fig = (HexagonPlanetFigure) this.getFigure();
		HexagonPlanet theModel = this.getCastedModel();

		// - Update figure visuals from current model data.
		//		HexagonalPosition hexPosition = theModel.getPosition();
		//		Point xyLocation = hexPosition.hexagonal2D();
		//		fig.setLocation(xyLocation);
		fig.setPosition(theModel.getPosition());
		fig.setName(theModel.getName());
	}

	@Override
	public HexagonPlanet getCastedModel() {
		return (HexagonPlanet) getModel();
	}
}

// - UNUSED CODE ............................................................................................
