//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.PolygonShape;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonShape extends PolygonShape {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger		logger			= Logger.getLogger("net.sf.hexagon.figures");
	/**
	 * Factor used to calculate the scale size of a tile. The hexagonal coordinates are not changed but the size
	 * of the tiles may be configured from the outside zoom factor. To perform the new calculations, the hexagon
	 * structure parameters are changed and from them the XY resulting coordinates for any tile.
	 */
	public static int				scaleFactor	= 1;
	public static final int	SIZE				= 6;
	public static int				HEIGHT			= scaleFactor * SIZE / 2;
	public static int				RADIUS			= new Double(0.86 * scaleFactor * SIZE).intValue();

	public static void setScale(final int newScale) {
		scaleFactor = newScale;
		HEIGHT = scaleFactor * SIZE / 2;
		RADIUS = new Double(0.86 * scaleFactor * SIZE).intValue();
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonShape() {
		this.addPoint(new Point(RADIUS, 0));
		this.addPoint(new Point(RADIUS * 2, HEIGHT));
		this.addPoint(new Point(RADIUS * 2, HEIGHT + SIZE));
		this.addPoint(new Point(RADIUS, HEIGHT * 2 + SIZE));
		this.addPoint(new Point(0, HEIGHT + SIZE));
		this.addPoint(new Point(0, HEIGHT));
		this.addPoint(new Point(RADIUS, 0));
	}

	//	public HexagonShape(int displacement) {
	//		this.addPoint(new Point(4 + displacement, 0 + displacement));
	//		this.addPoint(new Point(12 + displacement, 0 + displacement));
	//		this.addPoint(new Point(12 + 4 + displacement, 8 + displacement));
	//		this.addPoint(new Point(12 + displacement, 16 + displacement));
	//		this.addPoint(new Point(4 + displacement, 16 + displacement));
	//		this.addPoint(new Point(0 + displacement, 8 + displacement));
	//		this.addPoint(new Point(4 + displacement, 0 + displacement));
	//	}

	//	public void initDefault() {
	//	}
	//	public void initDisplaced(int displacement) {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		return new Dimension(RADIUS * 2, SIZE + HEIGHT * 2);
	}
}

// - UNUSED CODE ............................................................................................
