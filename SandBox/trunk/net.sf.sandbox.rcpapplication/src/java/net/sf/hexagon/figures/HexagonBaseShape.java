//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;

import net.sf.gef.core.figures.ISelectableFigure;
import net.sf.hexagon.models.HexagonalPosition;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class HexagonBaseShape extends HexagonShape implements ISelectableFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger		logger	= Logger.getLogger("net.sf.hexagon.figures");
	public static final int	SIZE		= 6;
	public static int				HEIGHT	= HexagonBaseShape.scaleFactor * HexagonBaseShape.SIZE / 2;
	public static int				RADIUS	= new Double(0.86 * HexagonBaseShape.scaleFactor * HexagonBaseShape.SIZE).intValue();

	public static void setScale(final int newScale) {
		HexagonBaseShape.scaleFactor = newScale;
		HexagonBaseShape.HEIGHT = HexagonBaseShape.scaleFactor * HexagonBaseShape.SIZE / 2;
		HexagonBaseShape.RADIUS = new Double(0.86 * HexagonBaseShape.scaleFactor * HexagonBaseShape.SIZE).intValue();
	}

	// - F I E L D - S E C T I O N ............................................................................
	private XYLayout					figureBaseLayout;

	/** Hexagonal coordinate of the hexagonal grid specially created for this map. */
	private HexagonalPosition	location6X	= new HexagonalPosition(0, 0);
	/**
	 * Location of the center of the figure in the XY plane. This location is calculated from the zoom factor
	 * and the hexagonal real coordinates.
	 */
	private final Point				location2D	= location6X.hexagonal2D();
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int								selected		= EditPart.SELECTED_NONE;
	/**
	 * State of the hovering over this element. If the <code>hovering</code> is <code>true</code> then we have
	 * to show the hover view of the object and make visible other parts of the figure.
	 */
	private boolean						hovering		= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonBaseShape() {
		super();
		initialize();
	}

	//
	//	public HexagonBaseShape(final int displacement) {
	//		super(displacement);
	//		initialize();
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public boolean getHover() {
		return hovering;
	}

	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return selected;
	}

	public boolean isHover() {
		return hovering;
	}

	//	public abstract void hoverPresentation();

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == this.getSelected()) || (EditPart.SELECTED == this.getSelected()))
			return true;
		else return false;
	}

	public void setHover(final boolean value) {
		hovering = value;
	}

	//	public abstract void refreshContents();

	//	public abstract void selectedPresentation();

	/**
	 * Set the new tile position. At the completion update the XY real coordinates obtained from the hexagonal
	 * tile position.
	 */
	public void setPosition(final HexagonalPosition newPoint) {
		if (null != newPoint) {
			location6X = newPoint;
			//- Set the correct location for the upper-left corner.
			Point center = location6X.hexagonal2D();
			Point topLeft = new Point(center.x - 16, center.y - 16);
			this.setLocation(topLeft);
			//	this.setLocation(location6X.hexagonal2D());
		}
	}

	/** Set the new hexagonal position from the row and columns parameters that define the tile position. */
	public void setPosition(int column, int row) {
		//- Convert values to the positive quadrant
		if (column < 1) column *= -1;
		if (row < 1) row *= -1;

		this.setPosition(new HexagonalPosition(column, row));
	}

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(final int value) {
		selected = value;
	}

	protected abstract void createContents();

	//	public abstract void standardPresentation();

	protected AbstractLayout getLayout() {
		return figureBaseLayout;
	}

	protected void initialize() {
		this.removeAll();
		this.setLayout();
		//DEBUG Those are debug lines and more initialization
		//		setBorder(new LineBorder(1));
		//		this.setBackgroundColor(ColorConstants.white);
		this.createContents();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	protected void setIcon(final String iconName) {
		// TODO Auto-generated method stub

	}

	protected void setLayout() {
		figureBaseLayout = new XYLayout();
		this.setLayoutManager(figureBaseLayout);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[HexagonBaseShape ");
		buffer.append("SIZE=").append(SIZE).append(", ");
		buffer.append("HEIGHT=").append(HEIGHT).append("");
		buffer.append("RADIUS=").append(RADIUS).append("");
		buffer.append("\nlocation6X=").append(location6X.toString()).append("");
		buffer.append("\nlocation2D=").append(location2D).append("");
		buffer.append("]\n");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
