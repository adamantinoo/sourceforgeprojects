//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

import net.sf.hexagon.figures.HexagonBaseShape;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Hexagonal points determine the location in rows and columns of the center point of a new hexagonal tile.
 * The coordinate origin is 0,0 and the even and odd rows are laterally displaced by the RADIUS factor that id
 * the size by SIN(60�).
 */
public class HexagonalPosition {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.hexagon.models");

	// - F I E L D - S E C T I O N ............................................................................
	private int						column	= 0;
	private int						row			= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonalPosition() {
	}

	public HexagonalPosition(final int newColumn, final int newRow) {
		column = newColumn;
		row = newRow;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public Point hexagonal2D() {
		int coordX = 0;
		int coordY = 0;
		//- Check if the row is pair or even.
		if ((row % 2) > 0) {
			coordX = column * 2 * HexagonBaseShape.RADIUS + HexagonBaseShape.RADIUS;
			coordY = row * (HexagonBaseShape.HEIGHT + HexagonBaseShape.SIZE);
		} else {
			coordX = column * 2 * HexagonBaseShape.RADIUS;
			coordY = row * (HexagonBaseShape.HEIGHT + HexagonBaseShape.SIZE);
		}
		//- Displace from the top-left corner to the center of the tile.
		return new Point(coordX + HexagonBaseShape.RADIUS, coordY + HexagonBaseShape.HEIGHT + HexagonBaseShape.SIZE / 2);
	}

	public void setColumn(final int column) {
		this.column = column;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[HexagonalPosition ");
		buffer.append("column=").append(column).append(", ");
		buffer.append("row=").append(row).append("");
		buffer.append("]\n");
		return buffer.toString();
	}

	public void setRow(final int row) {
		this.row = row;
	}
}

// - UNUSED CODE ............................................................................................
