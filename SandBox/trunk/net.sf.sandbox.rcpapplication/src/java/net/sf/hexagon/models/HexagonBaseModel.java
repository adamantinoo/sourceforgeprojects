//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.hexagon.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.gef.core.models.AbstractGEFNode;
import net.sf.sandbox.models.Abstract2DLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonBaseModel extends AbstractGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger						= Logger.getLogger("net.sf.sandbox.models");
	private static final long		serialVersionUID	= -6912175362817791956L;

	// - F I E L D - S E C T I O N ............................................................................
	private Abstract2DLocation	position					= new Abstract2DLocation();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HexagonBaseModel() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	public Abstract2DLocation getLocation() {
	//		return position;
	//	}

	public HexagonalPosition getPosition() {
		return new HexagonalPosition(position.getCoordinateY(), position.getCoordinateX());
	}

	public void setPosition(Abstract2DLocation newLocation) {
		if (null != newLocation) position = newLocation;
	}
}
// - UNUSED CODE ............................................................................................
