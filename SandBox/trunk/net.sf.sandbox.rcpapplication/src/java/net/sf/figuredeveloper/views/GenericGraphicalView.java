//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.figuredeveloper.views;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.gef.ui.parts.SelectionSynchronizer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import net.sf.sandbox.models.GenericModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class GenericGraphicalView extends ViewPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger	= Logger.getLogger("net.sf.figuredeveloper");

	// - F I E L D - S E C T I O N ............................................................................
	private IViewSite							viewSite;
	private DefaultEditDomain			editDomain;
	private GraphicalViewer				graphicalViewer;
	private SelectionSynchronizer	synchronizer;
	/** This is the structure that contains the model data to be presented on the viewer. */
	private GenericModelStore			dataContainer;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GenericGraphicalView() {
		this.setEditDomain(new DefaultEditDomain(null));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void createPartControl(final Composite parent) {
		//		createGraphicalViewer(parent);
		final GraphicalViewer viewer = new ScrollingGraphicalViewer();
		viewer.createControl(parent);

		//setGraphicalViewer(viewer);
		this.getEditDomain().addViewer(viewer);
		graphicalViewer = viewer;

		this.configureGraphicalViewer();
		{//hookGraphicalViewer();
			this.getSelectionSynchronizer().addViewer(this.getGraphicalViewer());
			this.getSite().setSelectionProvider(this.getGraphicalViewer());
		}
		this.initializeGraphicalViewer();
	}

	/**
	 * This <code>init</code> has to create and connect the <code>AbstractModelStore</code> to the Viewer. For
	 * this reason any new View must declare this method and perform the initialization of such fields.
	 */
	@Override
	public void init(final IViewSite site) throws PartInitException {
		viewSite = site;
		super.init(site);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		graphicalViewer.getControl().setFocus();
	}

	/**
	 * Called to configure the graphical viewer before it receives its contents. This is where the root editpart
	 * should be configured. Subclasses should extend or override this method as needed.
	 */
	protected void configureGraphicalViewer() {
		this.getGraphicalViewer().getControl().setBackground(ColorConstants.white);
	}

	protected GenericModelStore getContainer() {
		return dataContainer;
	}

	protected GenericModelStore getContents() {
		return this.getContainer();
	}

	protected DefaultEditDomain getEditDomain() {
		return editDomain;
	}

	protected GraphicalViewer getGraphicalViewer() {
		return graphicalViewer;
	}

	public void setContainer(GenericModelStore dataContainer) {
		this.dataContainer = dataContainer;
	}

	/**
	 * Returns the selection synchronizer object. The synchronizer can be used to sync the selection of 2 or
	 * more EditPartViewers.
	 * 
	 * @return the synchronizer
	 */
	protected SelectionSynchronizer getSelectionSynchronizer() {
		if (synchronizer == null) synchronizer = new SelectionSynchronizer();
		return synchronizer;
	}

	protected void initializeGraphicalViewer() {
		final GraphicalViewer viewer = this.getGraphicalViewer();
		viewer.setContents(this.getContents()); // Set the contents of this graphical.
	}

	/**
	 * Sets the EditDomain for this EditorPart.
	 * 
	 * @param ed
	 *          the domain
	 */
	protected void setEditDomain(final DefaultEditDomain ed) {
		editDomain = ed;
	}
}
// - UNUSED CODE ............................................................................................
