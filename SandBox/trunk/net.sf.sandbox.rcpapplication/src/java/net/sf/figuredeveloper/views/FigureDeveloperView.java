//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.figuredeveloper.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;

import net.sf.figuredeveloper.factories.FigureDeveloperEditPartFactory;
import net.sf.figuredeveloper.factories.FigureDeveloperFigureFactory;
import net.sf.figuredeveloper.models.FigureDeveloperModelStore;
import net.sf.gef.core.models.AbstractGEFNode;
import net.sf.sandbox.app.SandBoxActivator;
import net.sf.sandbox.models.GenericModelStore;
import net.sf.sandbox.models.UISandBoxModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class FigureDeveloperView extends GenericGraphicalView implements PropertyChangeListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger														= Logger.getLogger("net.sf.figuredeveloper");
	public static final String	ID																= "net.sf.figuredeveloper.views.FigureDeveloperView.id";
	public static final String	FIGUREDEVELOPERDRAFTMODELSTORE_ID	= "net.sf.figuredeveloper.views.FigureDeveloperView.ModelStore.id";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FigureDeveloperView() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void init(final IViewSite site) throws PartInitException {
		super.init(site);

		//- Connect this view with the content provider that contains the singleton model.
		final GenericModelStore container = (GenericModelStore) SandBoxActivator
				.getByID(FigureDeveloperView.FIGUREDEVELOPERDRAFTMODELSTORE_ID);
		if (null == container) {
			this.setContainer(new FigureDeveloperModelStore());
			SandBoxActivator.addReference(FigureDeveloperView.FIGUREDEVELOPERDRAFTMODELSTORE_ID, this.getContainer());
		}
		this.getContainer().addPropertyChangeListener(this);
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop))
			this.getContainer().fireStructureChange(UISandBoxModelStore.MODEL_STRUCTURE_CHANGED, null, null);
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		//		this.getGraphicalViewer().getControl().setBackground(ColorConstants.black);
		final GraphicalViewer viewer = this.getGraphicalViewer();
		viewer.setRootEditPart(new ScalableRootEditPart());
		viewer.setEditPartFactory(new FigureDeveloperEditPartFactory(new FigureDeveloperFigureFactory()));
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		//		// - Register this as a listener to the SelectionInfoView selection provider.
		//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		//		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);

		//		// configure the context menu provider
		//		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		//		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		//		viewer.setContextMenu(cmProvider);
		//		getSite().registerContextMenu(cmProvider, viewer);
	}

}
// - UNUSED CODE ............................................................................................
