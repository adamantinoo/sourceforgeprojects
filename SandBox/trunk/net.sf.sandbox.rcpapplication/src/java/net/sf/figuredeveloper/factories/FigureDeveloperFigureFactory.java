//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedFigureFactory.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.figuredeveloper.factories;

// - IMPORT SECTION .........................................................................................
import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;

import net.sf.gef.core.factories.IFigureFactory;
import net.sf.hexagon.figures.HexagonDockFigure;
import net.sf.hexagon.figures.HexagonPlanetFigure;
import net.sf.hexagon.models.HexagonDock;
import net.sf.hexagon.models.HexagonPlanet;

// - CLASS IMPLEMENTATION ...................................................................................
public class FigureDeveloperFigureFactory implements IFigureFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.factories");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedFigureFactory() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Figure createFigure(EditPart part, IGEFModel unit) {
		if (unit instanceof HexagonPlanet) return new HexagonPlanetFigure((HexagonPlanet) unit);
		if (unit instanceof HexagonDock) return new HexagonDockFigure((HexagonDock) unit);
		throw new IllegalArgumentException();
	}

	public PolylineConnection createConnection(IWireModel newWire) {
		// TODO Auto-generated method stub
		return null;
	}

	public Figure createFigure(EditPart part, IGEFModel unit, String subType) {
		return this.createFigure(part, unit);
	}
}

// - UNUSED CODE ............................................................................................
