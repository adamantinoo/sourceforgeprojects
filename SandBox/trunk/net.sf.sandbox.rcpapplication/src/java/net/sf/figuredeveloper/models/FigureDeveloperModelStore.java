//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.figuredeveloper.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.hexagon.models.HexagonPlanet;
import net.sf.sandbox.models.Abstract2DLocation;
import net.sf.sandbox.models.GenericModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class FigureDeveloperModelStore extends GenericModelStore {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.hexagon.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FigureDeveloperModelStore() {
		//- Initialize hardcoded seed information
		HexagonPlanet planet = new HexagonPlanet("A");
		planet.setPosition(new Abstract2DLocation(5, 5));
		planet.setWaterStorage(30);
		planet.setIronStorage(12);
		planet.setOilStorage(24);
		planet.setFuelStorage(60);
		addTopElement(planet);

		//		planet = new HexagonPlanet("B");
		//		planet.setPosition(new Abstract2DLocation(7, 5));
		//		planet.setWaterStorage(30);
		//		planet.setIronStorage(12);
		//		planet.setOilStorage(24);
		//		planet.setFuelStorage(60);
		//		planet.activateFactory(true);
		//		addTopElement(planet);
		//
		//		planet = new HexagonPlanet("C");
		//		planet.setPosition(new Abstract2DLocation(5, 7));
		//		planet.setWaterStorage(30);
		//		planet.setIronStorage(12);
		//		planet.setOilStorage(24);
		//		planet.setFuelStorage(60);
		//		addTopElement(planet);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
