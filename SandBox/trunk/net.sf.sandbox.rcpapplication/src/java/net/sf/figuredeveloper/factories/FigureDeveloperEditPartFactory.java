//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.figuredeveloper.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.figuredeveloper.editparts.FigureDeveloperModelStoreContainerEditPart;
import net.sf.figuredeveloper.models.FigureDeveloperModelStore;
import net.sf.gef.core.editparts.AbstractEditPartFactory;
import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.gef.core.enums.EAbstractVariantType;
import net.sf.gef.core.factories.IFigureFactory;
import net.sf.gef.core.models.AbstractVariantGEFNode;
import net.sf.hexagon.editparts.HexagonPlanetEditPart;
import net.sf.hexagon.models.HexagonPlanet;

// - CLASS IMPLEMENTATION ...................................................................................
public class FigureDeveloperEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.sandbox.factories");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FigureDeveloperEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Creates a new <code>EditPart</code> from the received model element. The first test if to check if that
	 * element has a variant type because then the EditPart may differ from variant to variant.
	 */
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		//- Check for variant model elements
		if (modelElement instanceof AbstractVariantGEFNode) { return getVariantPartForElement(modelElement,
				((AbstractVariantGEFNode) modelElement).getVariant()); }
		if (modelElement instanceof FigureDeveloperModelStore) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ "FigureDeveloperModelStore" + "]");
			return new FigureDeveloperModelStoreContainerEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}

	protected AbstractGenericEditPart getVariantPartForElement(Object modelElement, Enum<EAbstractVariantType> variantType) {
		if (modelElement instanceof HexagonPlanet) {
			//- Get the model variant that will be really used to generate the part end the figure
			//		variant =  modelElement.getModelType();
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "."
					+ variantType.name() + "]");
			if (variantType)
				return new HexagonPlanetEditPart();
			else return getPartForElementHexagon(figureType);
		}
	}

	protected AbstractGenericEditPart getPartForElementHexagon(String modelElementName) {
		if (modelElementName.equalsIgnoreCase("HexagonPlanetA")) {
			logger.info("Generating EditPart for model class " + modelElementName + "]");
			return new HexagonPlanetEditPart();
		}
		//		if (modelElement instanceof HexagonDock) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "]");
		//			return new HexagonDockEditPart();
		//		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElementName != null) ? modelElementName.getClass().getName() : "null"));
	}
}
// - UNUSED CODE ............................................................................................
