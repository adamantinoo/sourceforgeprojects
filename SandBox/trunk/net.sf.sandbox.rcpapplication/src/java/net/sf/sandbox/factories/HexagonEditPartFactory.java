//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.sandbox.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.gef.core.editparts.AbstractEditPartFactory;
import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.hexagon.editparts.HexagonPlanetEditPart;
import net.sf.hexagon.models.HexagonPlanet;

// - CLASS IMPLEMENTATION ...................................................................................
public class HexagonEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.sandbox.factories");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public HexagonEditPartFactory() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof HexagonPlanet) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "]");
			return new HexagonPlanetEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}
}

// - UNUSED CODE ............................................................................................
