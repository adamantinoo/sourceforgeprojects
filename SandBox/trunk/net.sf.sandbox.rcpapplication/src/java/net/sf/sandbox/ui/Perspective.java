package net.sf.sandbox.ui;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import net.sf.figuredeveloper.views.FigureDeveloperView;
import net.sf.sandbox.views.View;

public class Perspective implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		layout.setFixed(true);

		layout.addStandaloneView(View.ID, false, IPageLayout.LEFT, 1.0f, editorArea);
		layout.addStandaloneView(FigureDeveloperView.ID, false, IPageLayout.RIGHT, 1.0f, editorArea);
	}

}
