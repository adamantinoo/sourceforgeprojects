//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.sandbox.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class Abstract2DLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger			= Logger.getLogger("net.sf.sandbox.models");

	// - F I E L D - S E C T I O N ............................................................................
	private int						coordinateX	= 0;
	private int						coordinateY	= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Abstract2DLocation() {
	}

	public Abstract2DLocation(int X, int Y) {
		setLocation(X, Y);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setLocation(int X, int Y) {
		coordinateX = X;
		coordinateY = Y;
	}

	public int getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}

	public int getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
	}
}

// - UNUSED CODE ............................................................................................
