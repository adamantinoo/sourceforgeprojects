//  PROJECT:        net.sf.sandbox.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.sandbox.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;

import org.apache.batik.swing.JSVGCanvas;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;

import net.sf.gef.core.editors.BaseGraphicalEditor;
import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.sandbox.models.UISandBoxModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class DraftHexagonView extends View implements PropertyChangeListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger				= Logger.getLogger("net.sf.sandbox.rcpapplication");
	public static final String		ID						= "net.sf.sandbox.views.DraftHexagonView.id";

	// - F I E L D - S E C T I O N ............................................................................
	/** The view cannot be an editor at the same time, so delegate all editor actions to this editor. */
	LocalGraphicalDetailedEditor	detailEditor	= null;
	/** This is the root of the editor's model. */
	private UISandBoxModelStore		editorContainer;
	private Composite							viewerRoot;
	private IViewSite							viewSite;
	/** The SVG canvas. */
	protected JSVGCanvas					svgCanvas			= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DraftHexagonView() {
	}

	/**
	 * This is a required method that is get called when the view is created and is responsible for the creation
	 * of all the view data structures and content management. This is the method called during creation and
	 * initialization of the view. The view must be able to change their presentation dynamically depending on
	 * the selection, so there should be a link point where other content structures can plug-in to be
	 * displayed.<br>
	 * This class will set as the top presentation element of a new <code>GraphicalDetailedEditor</code> that
	 * will present the selection received as a new MVC pattern
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// - Create a new editor and initialize it based on this view.
		viewerRoot = parent;
		detailEditor = new LocalGraphicalDetailedEditor(parent, this);
	}

	public UISandBoxModelStore getContainer() {
		return editorContainer;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		detailEditor.setFocus();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		viewSite = site;
		super.init(site);

		//- Connect this view with the content provider that contains the singleton model.
		//		editorContainer = (UISandBoxModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		if (null == editorContainer) {
			editorContainer = new UISandBoxModelStore();
			svgCanvas.addPropertyChangeListener(this);
		}
		editorContainer.addPropertyChangeListener(this);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) {
			editorContainer.fireStructureChange(UISandBoxModelStore.MODEL_STRUCTURE_CHANGED, null, null);
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

class LocalGraphicalDetailedEditor extends BaseGraphicalEditor {
	//	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.views");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	ID	= "net.sf.sandbox.DraftHexagonView.LocalGraphicalDetailedEditor.id";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private DraftHexagonView		detailedView;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public LocalGraphicalDetailedEditor(Composite parent, DraftHexagonView detailedView) {
		try {
			setEditDomain(new DefaultEditDomain(this));
			this.detailedView = detailedView;
			// - Register the view. This will remove the requirement to have the view declared as a static singleton
			//IMPACT The Activator should have more functionalities than the standard one.
			//			SandBoxActivator.addReference(ID, this);

			//- Access the selection editor to copy the initialization to this view editor.
			init(this.detailedView.getSite());
			createGraphicalViewer(parent);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public UISandBoxModelStore getContents() {
		if (null != detailedView)
			return detailedView.getContainer();
		else return new UISandBoxModelStore();
	}

	public void init(IWorkbenchPartSite site) throws PartInitException {
		LocalEmptyEditorSite editorSite = new LocalEmptyEditorSite(site);
		setSite(editorSite);
		setInput(null);
		getCommandStack().addCommandStackListener(this);
		//		// - Add this editor selection listener to the list of listeners of this window
		//		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
		initializeActionRegistry();
	}

	@Override
	protected void initializeGraphicalViewer() {
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(getContents()); // Set the contents of this graphical.
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setRootEditPart(new ScalableRootEditPart());
		//IMPACT Each new editor should provide the EditPart factory and the Figure factory
		//		viewer.setEditPartFactory(new PilotEditPartFactory(new PilotFigureFactory()));
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		//		// - Register this as a listener to the SelectionInfoView selection provider.
		//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		//		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);

		//		// configure the context menu provider
		//		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		//		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		//		viewer.setContextMenu(cmProvider);
		//		getSite().registerContextMenu(cmProvider, viewer);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Do nothing or we can bypass signaling to the main editor

	}

	//	@Override
	//	public void dispose() {
	//		// - Unregister this from the SelectionInfoView selection provider.
	//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
	//		if (null != provider) ((ISelectionProvider) provider).removeSelectionChangedListener(this);
	//		super.dispose();
	//	}

}

// - UNUSED CODE ............................................................................................
