package net.sf.sandbox.app;

import org.osgi.framework.BundleContext;

import net.sf.core.app.BaseActivator;

/**
 * The activator class controls the plug-in life cycle
 */
public class SandBoxActivator extends BaseActivator {

	// The plug-in ID
	public static final String			PLUGIN_ID	= "net.sf.sandbox.rcpapplication";

	// The shared instance
	private static SandBoxActivator	plugin;

	//	/** Hash map where I can store and then retrieve global items. */
	//	//TODO This cannot be a Map because there can be more then one editor active at the same time.
	//	private static HashMap<Object, Object>	registry	= new HashMap<Object, Object>();

	/**
	 * The constructor
	 */
	public SandBoxActivator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}
}
