######################################################################
# AWK
#	Script to aggregate detailed campaing data into Date - Hour - Campaing
######################################################################
BEGIN {
	OFS = "\t"   # output field separator is a tab
}
{
	# Record model
	# 05/06/11 00:01:41 [RESIDENCIAL] [PREPAGO] [1-5735140251] [625895161]
	# Date is field $1 but from hour we have to get just the hour
	split( $2, fieldsHour, ":" )
	# Campaing has to be stripped form the []
#	split( $5, campaing1, "[" )
#	split( campaing1[2], campaing2, "]" )
	campaingCOUNTER[ $1, fieldsHour[1] ":00", $3, $4, $5 ]++
}

END {
	for( record in campaingCOUNTER ) {
		# Index data is: 1-Date 2-Hour 3-Segment 4-Contract 5-Campaing
		split( record, data, SUBSEP )
		printf( "%s %s %s %s %s %2.0i\n", data[1], data[2], data[3], data[4], data[5], campaingCOUNTER[ data[1], data[2], data[3], data[4], data[5] ] )
	}
}

