######################################################################
# AWK
#	Script to aggregate detailed campaing data into Date - Hour - Campaing
######################################################################
BEGIN {
	OFS = "\t"   # output field separator is a tab
}
{
	# Record model
	# 01/07/2011 0:13; 1-5306740234;626894568;  
	# Date is field $1 but from hour we have to get just the hour
	split( $2, fieldsHour, ":" )
	# Campaing is the lst field but has to be extracted from the MSISDN
	split( $3, campaing, ";" )
	if ( fieldsHour[1] < 10 ) {
		campaingCOUNTER[ $1, "0" fieldsHour[1] ":00", campaing[1] ]++
	} else {
		campaingCOUNTER[ $1, fieldsHour[1] ":00", campaing[1] ]++
	}
}

END {
	for( record in campaingCOUNTER ) {
		# Index data is: 1-Date 2-Hour 3-Campaing
		split( record, data, SUBSEP )
		printf( "%s %s %s %2.0i\n", data[1], data[2], data[3], campaingCOUNTER[ data[1], data[2], data[3] ] )
	}
}

