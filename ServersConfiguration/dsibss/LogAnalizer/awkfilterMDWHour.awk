BEGIN {
	OFS="\t"   # output field separator is a tab
}
/# / {
        # Get the day, mont and year from the source data
        split( $1, arrFecha, "-" )
        split( $2, arrHora, ":" )

        date = arrFecha[3] "/" arrFecha[2] "/" arrFecha[1]
        time = arrHora[1] ":00"
		if ( $9 == "" )
			skip
		else
        	contadoresSTUCK[$9, date, time]++
}

END {
        for( registro in contadoresSTUCK ) {
                split( registro, data, SUBSEP )
                printf( "%s %s %s %2.0i\n", data[2], data[3], data[1], contadoresSTUCK[data[1], data[2], data[3]] )
        }
}

