{
	# Get the day, mont and year from the source data
	split( $1, d1M, "<" )
	split( $2, d1D, "," )
	split( $4, d2H, ":" )
	if ( d1D[1] < 10 ) {
		d1D[1] = "0" d1D[1]
	}
#	date = d1D[1] "/" d1M[2] "/" $3
	date = $3 "/" d1M[2] "/" d1D[1]
	time = d2H[1] ":00 " $5
	a1STUCK[date, time]++
}
END {
	for( count in a1STUCK ) {
		split( count, data, SUBSEP )
		printf( "%s %s %2.0i\n", data[1], data[2], a1STUCK[data[1], data[2]] )
	}
}

