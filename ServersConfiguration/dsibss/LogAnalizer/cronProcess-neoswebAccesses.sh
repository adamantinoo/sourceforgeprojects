#!/bin/bash

###
# Get the logs of neos service and process them to get the daily accumulated for each of the middleware operations.
###
HOMEDIR="/home/ldediego/loganalizer"
LOGDIR="${HOMEDIR}/logs"
PROCESSNAME="neos.webaccess.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/neos/web"
DATADEST="${HOMEDIR}/neosweb"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	let DAYOFMONTH=$2-1
	echo "Being called with CRON flag." ;
fi
echo "Month: ${MONTHNUMBER}"
echo "Day: ${DAYOFMONTH}"
#echo "... Remove data files already processed."

echo "... Copy data files to local storage."
cd ${DATADEST}
if [ ! -e weblogp5.website.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "Executing: scp -B swfcfe@${TARGET5}:${DATASOURCE}/neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET5}:${DATASOURCE}/neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
	rename -v 's/neoswebsite/weblogp5.website/' neoswebsite.log* ;
fi

if [ ! -e weblogp6.website.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "Executing: scp -B swfcfe@${TARGET6}:${DATASOURCE}/neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET6}:${DATASOURCE}/neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
	rename -v 's/neoswebsite/weblogp6.website/' neoswebsite.log* ;
fi

echo "... Process log and extract aggregated data"
DESTINATIONDAY=${DATADEST}/neoswebsite${MONTHNUMBER}${DAYOFMONTH}.txt
echo "Destination: ${DESTINATIONDAY}"
echo "Executing: gzip -dc ${DATADEST}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterINITNEOS.awk day=${DAYOFMONTH} month=${MONTHNUMBER} year=${YEAR} | sort > ${DESTINATIONDAY}"
gzip -dc ${DATADEST}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterINITNEOS.awk day=${DAYOFMONTH} month=${MONTHNUMBER} year=${YEAR} | sort > ${DESTINATIONDAY}

echo "... Group result into final aggregation file."
cat neoswebsite_init.neos.txt ${DESTINATIONDAY} | sort --batch-size=1021 > neoswebsite_init.neos.new.txt
cp neoswebsite_init.neos.new.txt neoswebsite_init.neos.txt

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

