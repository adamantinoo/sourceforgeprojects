######################################################################
# AWK
#	Script to aggregate hourly aggregated data into a DATA /CAMPAING
#		aggregation single file
######################################################################
BEGIN {
	OFS="\t"   # output field separator is a tab
}
{
	# Record model
	# 01/07/11 01:00 [EMPRESA] [POSPAGO] [1-4875163034] 57
	cualificationsByCampaing[$1, $5]+=$6
}

END {
	for( record in cualificationsByCampaing ) {
		split( record, data, SUBSEP )
		printf( "%s %s %2.0i\n", data[1], data[2], cualificationsByCampaing[ data[1], data[2] ] )
	}
}

