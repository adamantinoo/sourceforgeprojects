#!/bin/bash

###
# Get the logs of the PPOW statistics and process them to obtain the next information:
#	- The detail of MSISDN and Contract, segment and campaing for a "Listado de terminales"
#		the is understood to be a qualification.
#	- The detail of MSISDN that do not qualify a campaing.
#	- The aggregated data from qualifications for DATE HOUR SEGMENT CONTRACT CAMPAING
#	- The aggregated data from qualifications for DATE SEGMENT CONTRACT CAMPAING
#	- The aggregated data from qualifications for DATE CAMPAING
#	- The aggregated data from no qualifications for DATE HOUR
###
HOMEDIR="/home/ldediego/loganalizer"
LOGDIR="${HOMEDIR}/logs"
PROCESSNAME="PPOW.qualifications.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
AREA="${HOMEDIR}/ppow"
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/ppo"
DATADEST="${AREA}/sourcedata"
MONTHNUMBER=$1
DAYOFMONTH=$2

echo " "
echo "... START OF PROCESS. `date +%F%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	let DAYOFMONTH=$2-2
	echo "Being called with CRON flag." ;
fi
echo "Month: ${MONTHNUMBER}"
echo "Day: ${DAYOFMONTH}"
#echo "... Remove data files already processed."
#rm --force --verbose ${AREA}/*services.log*

echo "... Copy data files to local storage."
cd ${DATADEST}
if [ ! -e estadisticas_ppo1.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo3.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo2.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo4.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi

if [ ! -e ecare_ppo1p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo3p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo2p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo4p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi

echo "... Process log and extract aggregated data"
DESTINATIONDETAILEDQUAL=${AREA}/ppowqualification${MONTHNUMBER}${DAYOFMONTH}detail.txt
DESTINATIONDETAILEDNOQUAL=${AREA}/ppownoqualification${MONTHNUMBER}${DAYOFMONTH}detail.txt

DESTINATIONHOURQUAL=${AREA}/ppowqualification${MONTHNUMBER}${DAYOFMONTH}houraverage.txt
#DESTINATIONDAYQUAL=${AREA}/ppowqualification${MONTHNUMBER}${DAYOFMONTH}day.txt
#DESTINATIONCAMPAINGQUAL=${AREA}/ppowqualification${MONTHNUMBER}${DAYOFMONTH}campaing.txt

DESTINATIONHOURNOQUAL=${AREA}/ppownotqualification${MONTHNUMBER}${DAYOFMONTH}hour.txt
echo "    Qualifications Detail records: ${DESTINATIONDETAILEDQUAL}"
echo "    Not qualified Detail records: ${DESTINATIONDETAILEDNOQUAL}"
echo "    Not Qualified Agregated by Hour: ${DESTINATIONHOURNOQUAL}"

echo "... Process source data to generate detailed files."
echo "    Executing: gzip -dc ${DATADEST}/estadisticas*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterPPOWCualificationDetailed.awk > ${DESTINATIONDETAILEDQUAL}"
gzip -dc ${DATADEST}/estadisticas*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterPPOWQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDQUAL}
echo "    Executing: gzip -dc ${DATADEST}/ecare*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterPPOWNotQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDNOQUAL}"
gzip -dc ${DATADEST}/ecare*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterPPOWNotQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDNOQUAL}

echo "... Process detailed file and generate aggregation files."
echo "    Executing: awk --file=${HOMEDIR}/awkfilterPPOWCualificationAggregationHour.awk ${DESTINATIONDETAILEDQUAL} | sort > ${DESTINATIONHOURQUAL}"
awk --file=${HOMEDIR}/awkfilterPPOWQualificationAggregationHour.awk ${DESTINATIONDETAILEDQUAL} | sort > ${DESTINATIONHOURQUAL}
echo "    Executing: awk --file=${HOMEDIR}/awkfilterPPOWNoQualificationAggregationHour.awk ${DESTINATIONDETAILEDNOQUAL} | sort > ${DESTINATIONHOURNOQUAL}"
awk --file=${HOMEDIR}/awkfilterPPOWNoQualificationAggregationHour.awk ${DESTINATIONDETAILEDNOQUAL} | sort > ${DESTINATIONHOURNOQUAL}

echo "... Group aggregation data into single file."
cd ${AREA}
echo "    Qualifications Agregated by Hour: ppowcualificationhouraverage.txt"
cat ppowcualificationhouraverage.txt ${DESTINATIONHOURAVERAGE} | awk --file ${HOMEDIR}/awkfilterPPOWCualificationHourGrouping.awk | sort > ppowcualificationhouraverage.new.txt
cp ppowcualificationhouraverage.new.txt ppowcualificationhouraverage.txt

echo "    Qualifications Agregated by Day: ppowcualificationdayaverage.txt"
cat ppowcualificationhouraverage.txt ${DESTINATIONHOURAVERAGE} | awk --file ${HOMEDIR}/awkfilterPPOWCualificationDayGrouping.awk | sort > ppowcualificationdayaverage.new.txt
cp ppowcualificationdayaverage.new.txt ppowcualificationdayaverage.txt

echo "    Qualifications Agregated by Campaing: ppowcualificationcampaing.txt"
cat ppowcualificationhouraverage.txt ${DESTINATIONHOURAVERAGE} | awk --file ${HOMEDIR}/awkfilterPPOWCualificationCampaingGrouping.awk | sort > ppowcualificationcampaing.new.txt
cp ppowcualificationcampaing.new.txt ppowcualificationcampaing.txt

echo "    Not Qualified Agregated by Hour: ppownotqualificationhour.txt"
cat ppownotqualificationhour.txt ${DESTINATIONHOURNOQUAL} | sort > ppownotqualificationhour.new.txt
cp ppownotqualificationhour.new.txt ppownotqualificationhour.txt

#cat neosmdwbyhouraverage.txt ${DESTINATIONHOURAVERAGE} | sort > neosmdwbyhouraverage.new.txt
#cp neosmdwbyhouraverage.new.txt neosmdwbyhouraverage.txt
#cat neosmdwtimeouts.txt ${DESTINATIONTO} | sort --key=1,2 --batch-size=1021 > neosmdwtimeouts.new.txt
#cp neosmdwtimeouts.new.txt neosmdwtimeouts.txt
#echo "Executing: awk --file ${HOMEDIR}/awkfilterTOMDWAggregated.awk neosmdwtimeouts.txt | sort > neosmdwtimeoutsaggregated.txt"
#awk --file ${HOMEDIR}/awkfilterTOMDWAggregated.awk neosmdwtimeouts.txt | sort > neosmdwtimeoutsaggregated.txt

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

