######################################################################
# AWK
#	Script to filter detailed data and generate a new detailed log 
#	with only the required data to process aggregations.
######################################################################
BEGIN {
	OFS = "\t"   # output field separator is a tab
}
/Listado de terminales/{
	# Record model
	# [05/06/11 00:00:00][INFO ][EstadisticasLogger] Listado de terminales - Zona publica|  Tipo usuario [RESIDENCIAL]  Tipo contrato [POSPAGO] Campania [1-5306740234]  |sessionId [LnzvNqqQD1TZ07f65ccKbJSFyR2Flkkcs3rsvXtnBh7LGbXWQsh8!-2008830309!1255240542!1307224800670] 
	# [05/06/11 00:14:18][INFO ][EstadisticasLogger] Listado de terminales - Zona privada|  Tipo usuario [RESIDENCIAL]  Tipo contrato [POSPAGO]  MSISDN [653909932]  Campania [1-5306740234]  |uid [653909932] sessionId [nyDnNqtbn21tn6FknbPfLqDh8q2tvDlL2Y3Fn1GpsDLfSs2hY61D!-2008830309!1255240542!1307225595880] 

	# Print the filtered data to the new detailed file
	split( $1, arrayDate, "[" )
	split( $2, arrayHour, "]" )
	if ( $16 == "Campania" ) {
		printf( "%s %s %s %s %s %s\n", arrayDate[2], arrayHour[1], $12, $15, $17, "NOMSISDN" )
	} else {
		printf( "%s %s %s %s %s %s\n", arrayDate[2], arrayHour[1], $12, $15, $19, $17 )
	}		
}

