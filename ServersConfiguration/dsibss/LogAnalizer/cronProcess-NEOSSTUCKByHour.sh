#!/bin/bash

###
# Get the logs of neos service and process them to get the daily accumulated for each of the middleware operations.
###
HOMEDIR="/home/ldediego/loganalizer"
LOGDIR="${HOMEDIR}/logs"
PROCESSNAME="NEOS.stuckbyhour.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/neos/services"
DATADEST="${HOMEDIR}/nohup"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
echo "... Copy data files to local storage."
cd ${DATADEST}
scp -B swfcfe@${TARGET5}:${DATASOURCE}/nohupneos*.* .
scp -B swfcfe@${TARGET6}:${DATASOURCE}/nohupneos*.* .

echo "... Process log and extract aggregated data"
gzip -dc nohupneos*.gz | grep "<Error> <WebLogicServer> <BEA-000337> <\[STUCK\]" > NEOSSTUCKdata.log
cat nohupneos*.log | grep "<Error> <WebLogicServer> <BEA-000337> <\[STUCK\]" >> NEOSSTUCKdata.log
awk --file ${HOMEDIR}/awkfilterSTUCK.awk NEOSSTUCKdata.log | sort > NEOSSTUCKcounter.txt

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

