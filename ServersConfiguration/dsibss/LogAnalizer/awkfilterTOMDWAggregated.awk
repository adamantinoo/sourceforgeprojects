BEGIN {
	OFS="\t"   # output field separator is a tab
}
{
		TOCounter[$1, $2]+=$4
}

END {
        for( registro in TOCounter ) {
                split( registro, data, SUBSEP )
                printf( "%s %s %2.0i\n", data[1], data[2], TOCounter[data[1], data[2]] )
        }
}

