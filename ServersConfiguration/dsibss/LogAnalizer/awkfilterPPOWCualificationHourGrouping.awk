BEGIN {
	OFS="\t"   # output field separator is a tab
}
{
	# Record model
	# 06/07/11 10:54 [EMPRESA] [POSPAGO] [1-5430537407] 28
	cualificationsByDate[$1, $2, $3, $4, $5]+=$6
}

END {
	for( record in cualificationsByDate ) {
		split( record, data, SUBSEP )
		printf( "%s %s %s %s %s %2.0i\n", data[1], data[2], data[3], data[4], data[5], cualificationsByDate[ data[1], data[2], data[3], data[4], data[5] ] )
	}
}

