BEGIN {
	OFS="\t"   # output field separator is a tab
}
/# / {
        # Get the day, mont and year from the source data
        split( $1, arrFecha, "-" )
        split( $2, arrHora, ":" )

        date = arrFecha[3] "/" arrFecha[2] "/" arrFecha[1]
        time = arrHora[1] ":00"
		if ( $9 == "" )
			skip
		else {
        	MDWCounter[$9, date, time]++
			MDWTimer[$9, date, time]+=$3
		}
}

END {
        for( registro in MDWCounter ) {
                split( registro, data, SUBSEP )
                printf( "%s %s %s %2.0i %4.1f\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]] )
        }
}

