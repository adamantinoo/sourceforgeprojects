#!/bin/bash

###
# Get the logs of neos service and process them to get the daily accumulated for each of the middleware operations.
###
HOMEDIR="/home/ldediego/loganalizer"
LOGDIR="${HOMEDIR}/logs"
PROCESSNAME="mdwoperations.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
AREA="/home/ldediego/loganalizer/weblog"
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/neos/services"
DATADEST="${HOMEDIR}/weblog"
MONTHNUMBER=$1
DAYOFMONTH=$2

echo " "
echo "... START OF PROCESS. `date +%F%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	let DAYOFMONTH=$2-1
	echo "Being called with CRON flag." ;
fi
echo "Month: ${MONTHNUMBER}"
echo "Day: ${DAYOFMONTH}"
#echo "... Remove data files already processed."
#rm --force --verbose ${AREA}/*services.log*

echo "... Copy data files to local storage."
cd ${AREA}
if [ ! -e weblogp5.services.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "Executing: scp -B swfcfe@${TARGET5}:${DATASOURCE}/neosservices.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET5}:${DATASOURCE}/neosservices.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
	rename -v 's/neosservices/weblogp5.services/' neosservices.log*;
fi
if [ ! -e weblogp6.services.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "Executing: scp -B swfcfe@${TARGET6}:${DATASOURCE}/neosservices.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET6}:${DATASOURCE}/neosservices.log.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz .
	rename -v 's/neosservices/weblogp6.services/' neosservices.log*;
fi

echo "... Process log and extract aggregated data"
DESTINATIONDAY=${DATADEST}/neosmdw${MONTHNUMBER}${DAYOFMONTH}date.txt
DESTINATIONTO=${DATADEST}/neosmdwtimeout${MONTHNUMBER}${DAYOFMONTH}date.txt
echo "Destination: ${DESTINATIONDAY}"
echo "Executing: gzip -dc ${DATADEST}/*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterMDWDay.awk > ${DESTINATIONDAY}"
gzip -dc ${DATADEST}/*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterMDWDay.awk > ${DESTINATIONDAY}
echo "Executing: gzip -dc ${DATADEST}/*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterTOMDW.awk > ${DESTINATIONTO}"
gzip -dc ${DATADEST}/*.2011-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterTOMDW.awk > ${DESTINATIONTO}

echo "... Group result into final aggregation file."
cat neosmdwbydate.txt ${DESTINATIONDAY} | awk --file ${HOMEDIR}/awkfilterMDWDayGrouping.awk | sort > neosmdwbydate.new.txt
cp neosmdwbydate.new.txt neosmdwbydate.txt
cat neosmdwtimeouts.txt ${DESTINATIONTO} | sort --key=1,2 --batch-size=1021 > neosmdwtimeouts.new.txt
cp neosmdwtimeouts.new.txt neosmdwtimeouts.txt
echo "Executing: awk --file ${HOMEDIR}/awkfilterTOMDWAggregated.awk neosmdwtimeouts.txt | sort > neosmdwtimeoutsaggregated.txt"
awk --file ${HOMEDIR}/awkfilterTOMDWAggregated.awk neosmdwtimeouts.txt | sort > neosmdwtimeoutsaggregated.txt

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

