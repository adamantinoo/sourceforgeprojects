BEGIN {
	OFS="\t"   # output field separator is a tab
}
{
	# Group by date and aggregate data for same date and operation
	operationsMDW[$1, $2]+=$3
}

END {
	for( record in operationsMDW ) {
		split( record, data, SUBSEP )
		printf( "%s %s %2.0i\n", data[1], data[2], operationsMDW[data[1], data[2]] )
	}
}

