BEGIN {
	OFS="\t"   # output field separator is a tab
}
/ \/init.neos/ {
        # Get the day, mont and year from the source data
        split( $1, arrHora, ":" )

        time = arrHora[2] ":00"
        contadoresINIT[arrHora[1]]++
		averageTime[arrHora[1]]+=$2
}

END {
        for( registro in contadoresINIT ) {
                split( registro, data, SUBSEP )
                printf( "%s/%s/%s %s %2.0i %2.2f\n", day, month, year, data[1], contadoresINIT[data[1]], averageTime[data[1]]/contadoresINIT[data[1]]  )
        }
}

