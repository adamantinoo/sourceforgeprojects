#!/bin/bash

###
# Get the logs of neos service and process them to get the daily accumulated for each of the middleware operations.
###
HOMEDIR="/home/ldediego/loganalizer"
LOGDIR="${HOMEDIR}/logs"
PROCESSNAME="FEW.stuckbyhour.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/fewspweb"
DATADEST="${HOMEDIR}/nohup"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
echo "... Copy data files to local storage."
cd ${DATADEST}
scp -B swfcfe@${TARGET7}:${DATASOURCE}/nohupfewspweb*.* .
scp -B swfcfe@${TARGET8}:${DATASOURCE}/nohupfewspweb*.* .

echo "... Process log and extract aggregated data"
gzip -dc nohupfewspweb*.gz | grep "<Error> <WebLogicServer> <BEA-000337> <\[STUCK\]" > FEWSTUCKdata.log
cat nohupfewspweb*.log | grep "<Error> <WebLogicServer> <BEA-000337> <\[STUCK\]" >> FEWSTUCKdata.log
awk --file ${HOMEDIR}/awkfilterSTUCK.awk FEWSTUCKdata.log | sort > FEWSTUCKcounter.txt

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

