#!/bin/bash

###
# Copy logs from areaprivada.orange.es to local storage, decompress and merge the access logs and
# then process them with awstats.
###
HOMEDIR="/srv/webservers/awstats"
LOGDIR="${HOMEDIR}/logs"
LOGNAME="awstats.areaprivada.cronprocess"
LOGFILE="${LOGDIR}/${LOGNAME}.log"
{
AREA="/home/ldediego/loganalizer/areaprivada"
AREA3="/home/ldediego/loganalizer/areaprivada-3"
AREA4="/home/ldediego/loganalizer/areaprivada-4"
TARGET3="AOTLXPRWEB00003"
TARGET4="AOTLXPRWEB00004"
SOURCEFILE="/web/2.2-worker/areaprivada/logs/areaprivada-ssl.log.[$1-$2].bz2"
SOURCEDEST="/srv/webservers/awstats/sourcelogdata"
TRAFFIC="/srv/webservers/awstats/htdocs/traffic"

echo " "
echo "... START OF PROCESS. `date +%F%H:%M:%S`"
echo "... Remove proceesed files not required."
rm -rf ${AREA3}/areaprivada-ssl.log.$1.processed.bz2
rm -rf ${AREA4}/areaprivada-ssl.log.$2.processed.bz2
rm -rf ${SOURCEDEST}/areaprivada.2process.log
rm -rf ${AREA}/*.areaprivada-ssl.log.*

echo "... Copy source files to local storage."
cd ${AREA}
scp -B swfcfe@${TARGET3}:${SOURCEFILE} .
rename 's/areaprivada-ssl.log/aotlxprweb00003.areaprivada-ssl.log/' areaprivada*
scp -B swfcfe@${TARGET4}:${SOURCEFILE} .
rename 's/areaprivada-ssl.log/aotlxprweb00004.areaprivada-ssl.log/' areaprivada*

#echo "... Decompress, sort and merge source data."
#bzcat ${AREA3}/areaprivada-ssl.log.$1.bz2 > ${SOURCEDEST}/areaprivada.$1-3.sorted.log
#bzcat ${AREA4}/areaprivada-ssl.log.$2.bz2 > ${SOURCEDEST}/areaprivada.$2-4.sorted.log
#sort --key=4,5 --batch-size=1021 --merge ${SOURCEDEST}/areaprivada.$1-3.sorted.log ${SOURCEDEST}/areaprivada.$2-4.sorted.log > ${SOURCEDEST}/areaprivada.$1.sorted.log

#echo "... Remove temporal files and start the renamimg for processing."
#rm -rf ${SOURCEDEST}/areaprivada.$1-3.sorted.log
#rm -rf ${SOURCEDEST}/areaprivada.$2-4.sorted.log
#mv ${AREA3}/areaprivada-ssl.log.$1.bz2 ${AREA3}/areaprivada-ssl.log.$1.processed.bz2
#mv ${AREA4}/areaprivada-ssl.log.$2.bz2 ${AREA4}/areaprivada-ssl.log.$2.processed.bz2

echo "... Process log with awstats."
perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -update -showsteps

#echo "... NOT Removing used files."
#mv ${SOURCEDEST}/areaprivada.2process.log /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log
#bzip2 /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log &
#rm -rf /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log

echo "... Generate stats web pages for months 05, 06."
#perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=02 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.201102.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=03 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.201103.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=04 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.201104.html
perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=05 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.201105.html
perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=06 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.201106.html

perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=05 -year=2011 -builddate=201105
perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=06 -year=2011 -builddate=201106

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

