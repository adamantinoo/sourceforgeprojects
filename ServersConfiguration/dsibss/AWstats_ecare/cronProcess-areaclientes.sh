#!/bin/bash

###
# Get the compressed logs for the arecleintes site fron the servers and logmergeresolve them into the awstats processing loop.
###
HOMEDIR="/srv/webservers/awstats"
LOGDIR="${HOMEDIR}/logs"
LOGNAME="awstats.areaclientes.cronprocess"
LOGFILE="${LOGDIR}/${LOGNAME}.log"
{
AREA1="/home/ldediego/loganalizer/areaclientes-1"
AREA2="/home/ldediego/loganalizer/areaclientes-2"
AREA="/home/ldediego/loganalizer/areaclientes"
TARGET1="AOTLXPRWEB00001"
TARGET2="AOTLXPRWEB00002"
SOURCEDIR="/web/2.2-worker/neos/logs"
SOURCEDEST="/srv/webservers/awstats/sourcelogdata"
TRAFFIC="/srv/webservers/awstats/htdocs/traffic"

echo " "
echo "... START OF PROCESS. `date +%F%H%M%S`"
echo "... Copy source files to local storage."
rm -rf ${AREA}/*areaclientes.orange.es*

cd ${AREA}
scp -B swfcfe@${TARGET1}:${SOURCEDIR}/areaclientes.orange.es.log.[$1-$2].bz2 .
scp -B swfcfe@${TARGET1}:${SOURCEDIR}/areaclientes.orange.es-ssl.log.[$1-$2].bz2 .
rename 's/areaclientes.orange.es/aotlxprweb00001.areaclientes.orange.es/' areaclientes*
cd ${AREA}
scp -B swfcfe@${TARGET2}:${SOURCEDIR}/areaclientes.orange.es.log.[$1-$2].bz2 .
scp -B swfcfe@${TARGET2}:${SOURCEDIR}/areaclientes.orange.es-ssl.log.[$1-$2].bz2 .
rename 's/areaclientes.orange.es/aotlxprweb00002.areaclientes.orange.es/' areaclientes*

#echo "... Decompress files and sort contents."
#bzcat ${AREA1}/areaclientes.orange.es.log.$1.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/areaclientes.$1-1.sorted.log
#bzcat ${AREA2}/areaclientes.orange.es.log.$2.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/areaclientes.$2-2.sorted.log
#bzcat ${AREA1}/areaclientes.orange.es-ssl.log.$1.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/areaclientes-ssl.$1-1.sorted.log
#bzcat ${AREA2}/areaclientes.orange.es-ssl.log.$2.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/areaclientes-ssl.$2-2.sorted.log

#bzcat ${AREA1}/areaclientes.orange.es.log.$1.bz2 > ${SOURCEDEST}/areaclientes.$1-1.sorted.log
#bzcat ${AREA2}/areaclientes.orange.es.log.$2.bz2 > ${SOURCEDEST}/areaclientes.$2-2.sorted.log
#bzcat ${AREA1}/areaclientes.orange.es-ssl.log.$1.bz2 > ${SOURCEDEST}/areaclientes-ssl.$1-1.sorted.log
#bzcat ${AREA2}/areaclientes.orange.es-ssl.log.$2.bz2 > ${SOURCEDEST}/areaclientes-ssl.$2-2.sorted.log
#sort --key=4,5 --batch-size=1021 --merge ${SOURCEDEST}/areaclientes.$1-1.sorted.log ${SOURCEDEST}/areaclientes.$2-2.sorted.log ${SOURCEDEST}/areaclientes-ssl.$1-1.sorted.log ${SOURCEDEST}/areaclientes-ssl.$2-2.sorted.log > ${SOURCEDEST}/areaclientes.$1.sorted.log

#echo "... Remove temporal files and start the renamimg for processing."
#rm -rf ${SOURCEDEST}/areaclientes.$1-1.sorted.log
#rm -rf ${SOURCEDEST}/areaclientes.$2-2.sorted.log
#rm -rf ${SOURCEDEST}/areaclientes-ssl.$1-1.sorted.log
#rm -rf ${SOURCEDEST}/areaclientes-ssl.$2-2.sorted.log
#mv ${AREA1}/areaclientes.orange.es.log.$1.bz2 ${AREA1}/areaclientes.orange.es.log.$1.processed.bz2
#mv ${AREA2}/areaclientes.orange.es.log.$2.bz2 ${AREA2}/areaclientes.orange.es.log.$2.processed.bz2
#mv ${AREA1}/areaclientes.orange.es-ssl.log.$1.bz2 ${AREA1}/areaclientes.orange.es-ssl.log.$1.processed.bz2
#mv ${AREA2}/areaclientes.orange.es-ssl.log.$2.bz2 ${AREA2}/areaclientes.orange.es-ssl.log.$2.processed.bz2

echo "... Process log with awstats."
#mv ${SOURCEDEST}/areaclientes.$1.sorted.log ${SOURCEDEST}/areaclientes.2process.log
perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -update -showsteps

#echo "... NOT Removing used files."
#mv ${SOURCEDEST}/areaprivada.2process.log /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log
#bzip2 /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log &
#rm -rf /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log

echo "... Generate stats web pages for months 12, 01."
#perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=12 -year=2010 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201012.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=01 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201101.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=02 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201102.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=03 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201103.html
#perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=04 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201104.html
perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=05 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.201105.html

#perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=02 -year=2011 -builddate=201102
#perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=03 -year=2011 -builddate=201103

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
#} >> $LOGFILE
exit 0

