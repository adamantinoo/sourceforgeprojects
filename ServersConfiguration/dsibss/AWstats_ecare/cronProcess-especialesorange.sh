#!/bin/bash

###
# Decompress a set of logs and then process with awstats into the especialesorange site.
# Parameter $1 - the number of the log set to be processed.
###
HOMEDIR="/srv/webservers/awstats"
LOGDIR="${HOMEDIR}/logs"
LOGNAME="awstats.especialesorange`date +%F%H:%M:%S`"
LOGNAME="awstats.especialesorange.cronprocess"
LOGFILE="${LOGDIR}/${LOGNAME}.log"
{
AREA1="/home/ldediego/loganalizer/especialesorange-1"
AREA2="/home/ldediego/loganalizer/especialesorange-2"
TARGET1="AOTLXPRWEB00001"
TARGET2="AOTLXPRWEB00002"
SOURCEFILE="/web/2.2-worker/especialesorange/logs/tienda.especialesorange.com.log.1.bz2"
SOURCEDEST="/srv/webservers/awstats/sourcelogdata"
TRAFFIC="/srv/webservers/awstats/htdocs/traffic"

echo " "
echo "... START OF PROCESS. `date +%F%H%M%S`"
echo "... Copy source files to local storage."
cd ${AREA1}
scp -B swfcfe@${TARGET1}:${SOURCEFILE} .
cd ${AREA2}
scp -B swfcfe@${TARGET2}:${SOURCEFILE} .

echo "... Decompress files and sort contents."
bzcat ${AREA1}/tienda.especialesorange.com.log.$1.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/especialesorange.$1-1.sorted.log
bzcat ${AREA2}/tienda.especialesorange.com.log.$2.bz2 | sort --key=4,5 --batch-size=1021 > ${SOURCEDEST}/especialesorange.$2-2.sorted.log
sort --key=4,5 --batch-size=1021 --merge ${SOURCEDEST}/especialesorange.$1-1.sorted.log ${SOURCEDEST}/especialesorange.$2-2.sorted.log > ${SOURCEDEST}/especialesorange.$1.sorted.log

echo "... Remove temporal files and start the renamimg for processing."
rm -rf ${SOURCEDEST}/especialesorange.$1-1.sorted.log
rm -rf ${SOURCEDEST}/especialesorange.$2-2.sorted.log
mv ${AREA1}/tienda.especialesorange.com.log.$1.bz2 ${AREA1}/tienda.especialesorange.com.log.$1.processed.bz2
mv ${AREA2}/tienda.especialesorange.com.log.$2.bz2 ${AREA2}/tienda.especialesorange.com.log.$2.processed.bz2

echo "... Process log with awstats."
mv ${SOURCEDEST}/especialesorange.$1.sorted.log ${SOURCEDEST}/especialesorange.2process.log
perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -update -showsteps -showcorrupted -showdropped

#echo "... NOT Removing used files."
#mv ${SOURCEDEST}/areaprivada.2process.log /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log
#bzip2 /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log &
#rm -rf /srv/webservers/awstats/sourcelogdata/areaprivada.$1.processed.log

echo "... Generate stats web pages for months 12, 01, 02, 03."
#perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -month=12 -year=2010 -output -staticlinks > ${TRAFFIC}/awstats.especialesorange.201012.html
#perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -month=01 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.especialesorange.201101.html
#perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -month=02 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.especialesorange.201102.html
#perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -month=03 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.especialesorange.201103.html
perl /usr/lib/cgi-bin/awstats.pl -config=especialesorange -month=04 -year=2011 -output -staticlinks > ${TRAFFIC}/awstats.especialesorange.201104.html

#perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=02 -year=2011 -builddate=201102
#perl /usr/share/awstats/tools/awstats_buildstaticpages.pl -configdir=/etc/awstats -config=areaprivada -awstatsprog=/usr/lib/cgi-bin/awstats.pl -dir=/srv/webservers/awstats/htdocs/traffic -month=03 -year=2011 -builddate=201103


echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

