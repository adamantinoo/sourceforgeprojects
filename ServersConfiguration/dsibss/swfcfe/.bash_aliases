### Handy aliases
#echo "...Running ./.bash_aliases"
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'
alias duo='du -ks * '
alias duos='duo | sort -nr '
alias v='ls -la'
alias pl='ps -elf | grep $1'
alias pp='ps -ef | grep $1'
alias rm='rm -i'
alias atl='atq | sort --key=5 '
alias listen='netstat -na | grep LISTEN '
alias prweb01='ssh aotlxprweb00001 -l swfcfe '
alias prweb02='ssh aotlxprweb00002 -l swfcfe '
alias prweb03='ssh aotlxprweb00003 -l swfcfe '
alias prweb04='ssh aotlxprweb00004 -l swfcfe '
alias prweb05='ssh aotlxprweb00005 -l swfcfe '
alias prweb06='ssh aotlxprweb00006 -l swfcfe '
alias prweb07='ssh aotlxprweb00007 -l swfcfe '
alias prweb08='ssh aotlxprweb00008 -l swfcfe '
alias wlp5='ssh weblogp5 -l swfcfe '
alias wlp6='ssh weblogp6 -l swfcfe '
alias wlp7='ssh weblogp7 -l swfcfe '
alias wlp8='ssh weblogp8 -l swfcfe '
alias wll05='ssh aotlxprwlg00005 -l swfcfe '
alias wll06='ssh aotlxprwlg00006 -l swfcfe '
alias sb01='ssh siebelsrv01 -l swfcfe '
alias sb02='ssh siebelsrv02 -l swfcfe '
alias sb03='ssh siebelsrv03 -l swfcfe '
alias sb04='ssh siebelsrv04 -l swfcfe '
alias prsed01='ssh aotlxprsed00001 -l swfcfe '
alias prsed02='ssh aotlxprsed00002 -l swfcfe '
alias hh='history '

alias nohupfilter='grep -v "saotlxprwlg00005un.reflect.GeneratedSerializationConstructorAccessor" | grep -v "sun.reflect.GeneratedMethodAccessor" | grep -v "GeneratedConstructorAccessor" '

### Aliases fro common use svn commands
alias sst=" svn status "

### Common working paths
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

