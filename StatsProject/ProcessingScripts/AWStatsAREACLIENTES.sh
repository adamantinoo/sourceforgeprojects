#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.AWSTATS.PROCESSING
#	FILE NAME:		$Id: AWStatsAREAPRIVADA.sh 95 2012-02-06 15:08:25Z swfcfe $
#	LAST UPDATE:	$Date: 2012-02-06 16:08:25 +0100 (lun, 06 feb 2012) $
#	REVISION:		$Revision: 95 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		YYYYMMDD (INITIALS) - Action.
#
#	DESCRIPTION:
#	Script to download, prepare and launch the AWStats processing loop in where
#		the access logs from the frontend Apaches get accounted and some usage
#		statistics get generated.
#
#	USAGE:
#		bash /home/swfcfe/StatsProject/ProcessingScripts/AWStatsAREAPRIVADA.sh 
#
#	PARAMETERS:
#		$1 NRO	Number of log files to download and to accumulate for processing.
#		$2 REMOVESWITCH if we wand to purge or not the current contents of
#			the processing folder before starting. This switch also stops the
#			download of the log files.
######################################################################

### GLOBAL DEFINITIONS
HOMEDIR="/home/swfcfe/StatsProject"
SCRIPTDIR="${HOMEDIR}/ProcessingScripts"
SOURCEDATADIR="${HOMEDIR}/SourceData"
LOGDIR="${HOMEDIR}/logs"

PROCESSNAME="AWStatsAREACLIENTES.ecare.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
NRO=$1
if [ -z "$NRO" ]
then
	NRO=1
fi
REMOVESWITCH=$2

### FUNCTION LOADING
######################################################################

### FUNCTION DEFINITIONS
### DownloadAccessLog( SERVER ORIGINPATH LOCALNAME FILECOUNT )
function DownloadAccessLog {
	for (( c=1; c<=$4; c++ ))
	do
		echo "    <...>scp -BCp swfcfe@$1:$2.$c.bz2 $3.$c.bz2"
		scp -BCp swfcfe@$1:$2.$c.bz2 $3.$c.bz2
	done
}
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [NRO]$1 [REMOVESWITCH]$2"

### Define the variables to download data and perform the first part of the process.
WORKAREA=${SOURCEDATADIR}/areaclientes
STATSDATA=${SOURCEDATADIR}/statsdata/areaclientes
echo "${WORKAREA}"
cd ${WORKAREA}
WEBSRV01="AOTLXPRWEB00001"
WEBSRV02="AOTLXPRWEB00002"
WEBSRV05="AOTLXPRWEB00005"
WEBSRV06="AOTLXPRWEB00006"

### Prepare the data. Depending on switch we purge or download.
if [ "${REMOVESWITCH}" == "DOWNLOAD" ]
then
	echo "    <...>rm --force --verbose ${WORKAREA}/aotlxprweb0000[1-6].areaclientes.orange.es*.bz2"
	rm --force --verbose ${WORKAREA}/aotlxprweb0000[1-6].areaclientes.orange.es*.bz2
	### Call function to perform a controllated download by iteration
	DownloadAccessLog ${WEBSRV01} "/web/2.2-worker/neos/logs/areaclientes.orange.es.log" "aotlxprweb00001.areaclientes.orange.es.log" ${NRO}
	DownloadAccessLog ${WEBSRV01} "/web/2.2-worker/neos/logs/areaclientes.orange.es-ssl.log" "aotlxprweb00001.areaclientes.orange.es-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV02} "/web/2.2-worker/neos/logs/areaclientes.orange.es.log" "aotlxprweb00002.areaclientes.orange.es.log" ${NRO}
	DownloadAccessLog ${WEBSRV02} "/web/2.2-worker/neos/logs/areaclientes.orange.es-ssl.log" "aotlxprweb00002.areaclientes.orange.es-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV05} "/web/2.2-worker/neos/logs/areaclientes.orange.es.log" "aotlxprweb00005.areaclientes.orange.es.log" ${NRO}
	DownloadAccessLog ${WEBSRV05} "/web/2.2-worker/neos/logs/areaclientes.orange.es-ssl.log" "aotlxprweb00005.areaclientes.orange.es-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV06} "/web/2.2-worker/neos/logs/areaclientes.orange.es.log" "aotlxprweb00006.areaclientes.orange.es.log" ${NRO}
	DownloadAccessLog ${WEBSRV06} "/web/2.2-worker/neos/logs/areaclientes.orange.es-ssl.log" "aotlxprweb00006.areaclientes.orange.es-ssl.log" ${NRO}
fi

read -p "Check the files downloaded & Press [Enter] key to start processing..."

TRAFFIC=${SOURCEDATADIR}/Traffic/areaclientes
echo "... Making backup of AWStats database file..."
DATETODAY=`date +%Y%m%d`
MNOW=`date +%m`
YNOW=`date +%Y`
MLAST=`date --date="${NRO} days ago" +%m`
YLAST=`date --date="${NRO} days ago" +%Y`
echo "    MNOW=${MNOW}"
echo "    YNOW=${YNOW}"
echo "    MLAST=${MLAST}"
echo "    YLAST=${YLAST}"
if [ "${MNOW}" != "${MLAST}" ]
then
	echo "    <...>cp ${STATSDATA}/awstats${MLAST}${YLAST}.areaclientes.txt ${STATSDATA}/awstats${MLAST}${YLAST}.areaclientes.backup${DATETODAY}.bck"
	cp ${STATSDATA}/awstats${MLAST}${YLAST}.areaclientes.txt ${STATSDATA}/awstats${MLAST}${YLAST}.areaclientes.backup${DATETODAY}.bck
fi
echo "    <...>cp ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.txt ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.backup${DATETODAY}.bck"
cp ${STATSDATA}/awstats${MNOW}${YNOW}.areaclientes.txt ${STATSDATA}/awstats${MNOW}${YNOW}.areaclientes.backup${DATETODAY}.bck

echo "... Process log with awstats. Using logmergeresolve to process source data"
echo "    <...>perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -update -showsteps -LogFile=\"perl /usr/share/awstats/tools/logresolvemerge.pl -dnscache=${STATSDATA}/dnscachelastupdate.areaclientes.hash ${WORKAREA}/*.bz2 | \""
perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -update -showsteps -LogFile="perl /usr/share/awstats/tools/logresolvemerge.pl -dnscache=${STATSDATA}/dnscachelastupdate.areaclientes.hash ${WORKAREA}/*.bz2 | "

echo "... Generate stats web pages for months."
if [ "${MNOW}" != "${MLAST}" ]
then
	perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=${MLAST} -year=${YLAST} -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.${YLAST}${MLAST}.html
fi
perl /usr/lib/cgi-bin/awstats.pl -config=areaclientes -month=${MNOW} -year=${YNOW} -output -staticlinks > ${TRAFFIC}/awstats.areaclientes.${YNOW}${MNOW}.html

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

