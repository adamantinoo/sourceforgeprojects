#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.AWSTATS.PROCESSING
#	FILE NAME:		$Id: AWStatsAREAPRIVADA.sh 99 2012-02-08 14:33:29Z swfcfe $
#	LAST UPDATE:	$Date: 2012-02-08 15:33:29 +0100 (mié, 08 feb 2012) $
#	REVISION:		$Revision: 99 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		YYYYMMDD (INITIALS) - Action.
#
#	DESCRIPTION:
#	Script to download, prepare and launch the AWStats processing loop in where
#		the access logs from the frontend Apaches get accounted and some usage
#		statistics get generated.
#
#	USAGE:
#		bash /home/swfcfe/StatsProject/ProcessingScripts/AWStatsAREAPRIVADA.sh 
#
#	PARAMETERS:
#		$1 NRO	Number of log files to download and to accumulate for processing.
#		$2 REMOVESWITCH if we wand to purge or not the current contents of
#			the processing folder before starting. This switch also stops the
#			download of the log files.
######################################################################

### GLOBAL DEFINITIONS
HOMEDIR="/home/swfcfe/StatsProject"
SCRIPTDIR="${HOMEDIR}/ProcessingScripts"
LIBRARYDIR="${HOMEDIR}/Libraries"
SOURCEDATADIR="${HOMEDIR}/SourceData"
LOGDIR="${HOMEDIR}/logs"

PROCESSNAME="AWStatsAREAPRIVADA.ecare.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
NRO=$1
if [ -z "$NRO" ]
then
	NRO=1
fi
REMOVESWITCH=$2

### FUNCTION LOADING
######################################################################

### FUNCTION DEFINITIONS
### DownloadAccessLog( SERVER ORIGINPATH LOCALNAME FILECOUNT )
function DownloadAccessLog {
	for (( c=1; c<=$4; c++ ))
	do
		echo "    <...>scp -BCp swfcfe@$1:$2.$c.bz2 $3.$c.bz2"
		scp -BCp swfcfe@$1:$2.$c.bz2 $3.$c.bz2
	done
}
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [NRO]$1 [REMOVESWITCH]$2"

### Define the variables to download data and perform the first part of the process.
WORKAREA=${SOURCEDATADIR}/areaprivada
STATSDATA=${SOURCEDATADIR}/statsdata/areaprivada
cd ${WORKAREA}
WEBSRV03="AOTLXPRWEB00003"
WEBSRV04="AOTLXPRWEB00004"
WEBSRV07="AOTLXPRWEB00007"
WEBSRV08="AOTLXPRWEB00008"

### Prepare the data. Depending on switch we purge or download.
if [ "${REMOVESWITCH}" == "DOWNLOAD" ]
then
	echo "    <...>rm --force --verbose ${WORKAREA}/aotlxprweb0000[3-8].areaprivada-ssl.log.*.bz2"
	rm --force --verbose ${WORKAREA}/aotlxprweb0000[3-8].areaprivada-ssl.log.*.bz2
	### Call function to perform a controllated download by iteration
	DownloadAccessLog ${WEBSRV03} "/web/2.2-worker/areaprivada/logs/areaprivada-ssl.log" "aotlxprweb00003.areaprivada-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV04} "/web/2.2-worker/areaprivada/logs/areaprivada-ssl.log" "aotlxprweb00004.areaprivada-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV07} "/web/2.2-worker/areaprivada/logs/areaprivada-ssl.log" "aotlxprweb00007.areaprivada-ssl.log" ${NRO}
	DownloadAccessLog ${WEBSRV08} "/web/2.2-worker/areaprivada/logs/areaprivada-ssl.log" "aotlxprweb00008.areaprivada-ssl.log" ${NRO}
fi

read -p "Check the files downloaded & Press [Enter] key to start processing..."

TRAFFIC="${SOURCEDATADIR}/Traffic/areaprivada"
echo "... Making backup of AWStats database file..."
DATETODAY=`date +%Y%m%d`
MNOW=`date +%m`
YNOW=`date +%Y`
MLAST=`date --date="${NRO} days ago" +%m`
YLAST=`date --date="${NRO} days ago" +%Y`
echo "    MNOW=${MNOW}"
echo "    YNOW=${YNOW}"
echo "    MLAST=${MLAST}"
echo "    YLAST=${YLAST}"
if [ "${MNOW}" != "${MLAST}" ]
then
	echo "    <...>cp ${STATSDATA}/awstats${MLAST}${YLAST}.areaprivada.txt ${STATSDATA}/awstats${MLAST}${YLAST}.areaprivada.backup${DATETODAY}.bck"
	cp ${STATSDATA}/awstats${MLAST}${YLAST}.areaprivada.txt ${STATSDATA}/awstats${MLAST}${YLAST}.areaprivada.backup${DATETODAY}.bck
fi
echo "    <...>cp ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.txt ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.backup${DATETODAY}.bck"
cp ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.txt ${STATSDATA}/awstats${MNOW}${YNOW}.areaprivada.backup${DATETODAY}.bck

echo "... Process log with awstats. Using logmergeresolve to process source data"
echo "    <...>perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -update -showsteps -LogFile=\"perl /usr/share/awstats/tools/logresolvemerge.pl -dnscache=${STATSDATA}/dnscachelastupdate.areaprivada.hash ${WORKAREA}/*.bz2 | \""
perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -update -showsteps -LogFile="perl /usr/share/awstats/tools/logresolvemerge.pl -dnscache=${STATSDATA}/dnscachelastupdate.areaprivada.hash ${WORKAREA}/*.bz2 | "

echo "... Generate stats web pages for months."
if [ "${MNOW}" != "${MLAST}" ]
then
	perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=${MLAST} -year=${YLAST} -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.${YLAST}${MLAST}.html
fi
perl /usr/lib/cgi-bin/awstats.pl -config=areaprivada -month=${MNOW} -year=${YNOW} -output -staticlinks > ${TRAFFIC}/awstats.areaprivada.${YNOW}${MNOW}.html

echo "... END OF PROCESS. `date +%F%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

