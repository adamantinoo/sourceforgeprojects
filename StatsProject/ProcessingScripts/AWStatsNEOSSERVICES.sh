#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.AWSTATS.PROCESSING
#	FILE NAME:		$Id: AWStatsNEOSSERVICES.sh 117 2012-03-12 17:42:26Z swfcfe $
#	LAST UPDATE:	$Date: 2012-03-12 18:42:26 +0100 (lun, 12 mar 2012) $
#	REVISION:		$Revision: 117 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		YYYYMMDD (INITIALS) - Action.
#
#	DESCRIPTION:
#	Script to download, prepare and launch the AWStats processing loop for
#		some set of "access logs". The processing generates statistics
#		and usage accounting of the selected site.
#	This script processes the uncompressed access logs to the service
#		weblogic part of the NEOS application server.
#
#	USAGE:
#		bash /home/swfcfe/StatsProject/ProcessingScripts/AWStatsNEOSSERVICES.sh 2 DOWNLOAD
#
#	PARAMETERS:
#		$1 NRO	Number of log files to download and to accumulate for processing.
#		$2 REMOVESWITCH =DOWNLOAD if we want to purge the current contents of
#			the processing folder before starting the download of the selected number
#			of files.
######################################################################

### GLOBAL DEFINITIONS
HOMEDIR="/home/swfcfe/StatsProject"
SCRIPTDIR="${HOMEDIR}/ProcessingScripts"
LIBRARYDIR="${HOMEDIR}/Libraries"
SOURCEDATADIR="${HOMEDIR}/SourceData"
LOGDIR="${HOMEDIR}/logs"

PROCESSNAME="AWStatsNEOSSERVICES.ecare.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
NRO=$1
if [ -z "$NRO" ]
then
	NRO=1
fi
REMOVESWITCH=$2

### FUNCTION LOADING
######################################################################

### FUNCTION DEFINITIONS
### DownloadAccessLog( SERVER ORIGINPATTERN LOCALPATTERN FILECOUNT )
function DownloadAccessLog {
	for (( c=1; c<=$4; c++ ))
	do
		### Calculate the date in a backward fashion to generate the right filename to download.
		FILEDATE=`date --date "$c days ago" +%Y-%m-%d`
		echo "    <...>scp -BCp swfcfe@$1:$2.${FILEDATE} $3.${FILEDATE}"
		scp -BCp swfcfe@$1:$2.${FILEDATE} $3.${FILEDATE}
	done
}
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [NRO]$1 [REMOVESWITCH]$2"

### Define the variables to download data and perform the first part of the process.
SERVICENAME=neosservices
WORKAREA=${SOURCEDATADIR}/${SERVICENAME}
STATSDATA=${SOURCEDATADIR}/statsdata/${SERVICENAME}
cd ${WORKAREA}
WEBSRV01="weblogp5"
WEBSRV02="weblogp6"

### Prepare the data. Depending on switch we purge or download.
if [ "${REMOVESWITCH}" == "DOWNLOAD" ]
then
	echo "    <...>rm --force --verbose ${WORKAREA}/weblogp[5-6].access.log*.gz"
	rm --force --verbose ${WORKAREA}/weblogp[5-6].access.log*.gz
	### Call function to perform a controllated download by iteration
	DownloadAccessLog ${WEBSRV01} "/weblogic10/user_projects/internet/servers/neos1p/servers/neos1p/logs/access.log" "weblogp5.access.log" ${NRO}
	echo "... Compressing files to use the same logmerge functions."
	echo "    <...>gzip ${WORKAREA}/weblogp5.access.log*"
	gzip ${WORKAREA}/weblogp5.access.log*
	DownloadAccessLog ${WEBSRV02} "/weblogic10/user_projects/internet/servers/neos2p/servers/neos2p/logs/access.log" "weblogp6.access.log" ${NRO}
	echo "... Compressing files to use the same logmerge functions."
	echo "    <...>gzip ${WORKAREA}/weblogp6.access.log*"
	gzip ${WORKAREA}/weblogp6.access.log*
fi

#read -p "Check the files downloaded & Press [Enter] key to start processing..."

TRAFFIC="${HOMEDIR}/Traffic/${SERVICENAME}"
echo "... Making backup of AWStats database file..."
DATETODAY=`date +%Y%m%d`
MNOW=`date +%m`
YNOW=`date +%Y`
MLAST=`date --date="${NRO} days ago" +%m`
YLAST=`date --date="${NRO} days ago" +%Y`
echo "    MNOW=${MNOW}"
echo "    YNOW=${YNOW}"
echo "    MLAST=${MLAST}"
echo "    YLAST=${YLAST}"
if [ "${MNOW}" != "${MLAST}" ]
then
	echo "    <...>cp ${STATSDATA}/awstats${MLAST}${YLAST}.${SERVICENAME}.txt ${STATSDATA}/awstats${MLAST}${YLAST}.${SERVICENAME}.backup${DATETODAY}.bck"
	cp ${STATSDATA}/awstats${MLAST}${YLAST}.${SERVICENAME}.txt ${STATSDATA}/awstats${MLAST}${YLAST}.${SERVICENAME}.backup${DATETODAY}.bck
fi
echo "    <...>cp ${STATSDATA}/awstats${MNOW}${YNOW}.${SERVICENAME}.txt ${STATSDATA}/awstats${MNOW}${YNOW}.${SERVICENAME}.backup${DATETODAY}.bck"
cp ${STATSDATA}/awstats${MNOW}${YNOW}.${SERVICENAME}.txt ${STATSDATA}/awstats${MNOW}${YNOW}.${SERVICENAME}.backup${DATETODAY}.bck

echo "... Process log with awstats. Using logmergeresolve to process source data"
echo "    <...>perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -update -showsteps -LogFile=\"perl /usr/share/awstats/tools/logresolvemerge.pl -dnscache=${STATSDATA}/dnscachelastupdate.${SERVICENAME}.hash ${WORKAREA}/*.gz | \""
perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -update -showsteps -LogFile="perl /usr/share/awstats/tools/logresolvemerge.pl ${WORKAREA}/*.gz | "

echo "... Generate stats web pages for months."
if [ "${MNOW}" != "${MLAST}" ]
then
	echo "    <...>perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -month=${MLAST} -year=${YLAST} -output -staticlinks > ${TRAFFIC}/awstats.${SERVICENAME}.${YLAST}${MLAST}.html"
	perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -month=${MLAST} -year=${YLAST} -output -staticlinks > ${TRAFFIC}/awstats.${SERVICENAME}.${YLAST}${MLAST}.html
fi
echo "    <...>perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -month=${MNOW} -year=${YNOW} -output -staticlinks > ${TRAFFIC}/awstats.${SERVICENAME}.${YNOW}${MNOW}.html"
perl /usr/lib/cgi-bin/awstats.pl -config=${SERVICENAME} -month=${MNOW} -year=${YNOW} -output -staticlinks > ${TRAFFIC}/awstats.${SERVICENAME}.${YNOW}${MNOW}.html

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

