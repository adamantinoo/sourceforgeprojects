#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.LOGSTASH.MONITORING
#	FILE NAME:		$Id: UpdateNEOSRRD.sh 80 2012-01-23 17:21:09Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 17:21:09 +0000 (Mon, 23 Jan 2012) $
#	REVISION:		$Revision: 80 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2016 by Orange Spain All Rights Reserved.
#	TO DO:
#
#	DESCRIPTION:
#	Download tail for log files. Keeps a track of the number of the original file on first
#		access and then downloads the new lines from that same last access. Uses an exact
#		tail number to remove duplicated lines.
#	Parameters define origin and destination files.
#
#	USAGE:
#		bash /home/swfcfe/MonitoringProject/ProcessingScripts/GenericLogDownloader.sh AOTLXPRWEB00013 /logs/areaclientes/ logname1.log areaclientes UPDATE
#
#	PARAMETERS:
#		$1 server to process. Name of the log source server.
#		$2 source log directory path.
#		$3 source log file name.
#		$4 destination directory for tracking.
#		$5 [OPTIONAL] Filter to be applied to the download process.
#		$6 UPDATESWITCH if we allow to update the RRD database. Any other value does not update the database
#				but make all other processing. Also does not modify the stored last record processed.
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/Kibana"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/Kibana/SourceData"
export LOGDIR="${HOMEDIR}/Kibana/logs"

PROCESSNAME="LogDownloader.kibana.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
TARGET=$1
LOGSOURCEPATH=$2
LOGSOURCENAME=$3
TARGETDIRECTORY=$4
FILTER=$5
UPDATESWITCH=$6

### FUNCTION DEFINITIONS
source "${LIBRARYDIR}/CommonLibrary.sh"
### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
# Gets the end of the file from the last point processed that is a line number
#	stored in a local control file.
# PARAMETERS:
#	FILLERP1	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	FILLERP2	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	TARGETSERVER	The network name of the target servar that contains the logs.
#	SIZECONTROL		The name of the local file used to control the already processed lines.
#	SERVERLOGPATH	The path of the server log to download.
#	LOCALLOGPATH	The path of the local server log
#	TAILFILTER		The piece of script passed and oncatenated with the downloading "tail" to improve some
#		preprocessed uses.
######################################################################

### FUNCTION DEFINITIONS

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [TARGET]$1 [LOGSOURCEPATH]$2 [LOGSOURCENAME]$3 [TARGETDIRECTORY]$4 [FILTER]$5 [UPDATESWITCH]$6"
FILTERDATE=`date +%Y-%m-%d`
FILTERMINUTE=`date +%M`
FILTERHOUR=`date +%H:%M`
#echo "    FILTERDATE=${FILTERDATE}"
#echo "    FILTERMINUTE=${FILTERMINUTE}"
#echo "    FILTERHOUR=${FILTERHOUR}"
WORKDIR=${SOURCEDATADIR}/${TARGETDIRECTORY}
echo "    WORKDIR=${WORKDIR}"
cd ${WORKDIR}

echo "    SOURCEDATADIR=${SOURCEDATADIR}"

### Get current file sizes to only process new lines. Check if current data exists.
echo "... Downloading data, Parameter configured source"
GetRemoteDataExact $1 $2 ${TARGET} "${WORKDIR}/${TARGET}-${LOGSOURCENAME}.lastsize.txt" ${LOGSOURCEPATH}${LOGSOURCENAME} "${WORKDIR}/${TARGET}-${LOGSOURCENAME}" "${FILTER}"

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

