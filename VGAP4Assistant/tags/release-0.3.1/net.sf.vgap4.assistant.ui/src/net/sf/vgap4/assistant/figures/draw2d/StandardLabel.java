//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class StandardLabel extends Label {
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public StandardLabel() {
		setFont(AssistantConstants.FONT_MAP_DEFAULT);
		setLabelAlignment(PositionConstants.LEFT);
	}

	public StandardLabel(final String text) {
		this();
		setText(text);
	}

	public StandardLabel(String identifier, Image iconImage) {
		this(identifier);
		this.setIcon(iconImage);
	}
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
