//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.policies;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Node;

import net.sf.vgap4.assistant.editparts.DetailedCommonEditPart;
import net.sf.vgap4.assistant.editparts.DetailedContainerEditPart;
import net.sf.vgap4.assistant.models.AssistantNode;

// - CLASS IMPLEMENTATION ...................................................................................
public class GraphLayoutManager extends AbstractLayout {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger							logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");
	private static final Insets				PADDING	= new Insets(4, 2, 4, 2);

	// - F I E L D - S E C T I O N ............................................................................
	private DetailedContainerEditPart	rootPart;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedContainerEditPart() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GraphLayoutManager(DetailedContainerEditPart diagram) {
		this.rootPart = diagram;
	}

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint, int hHint) {
		container.validate();
		List<AssistantNode> children = container.getChildren();
		Rectangle result = new Rectangle().setLocation(container.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++)
			result.union(((IFigure) children.get(i)).getBounds());
		result.resize(container.getInsets().getWidth(), container.getInsets().getHeight());
		return result.getSize();
	}

	public void layout(IFigure container) {
		DirectedGraph graph = new DirectedGraph();
		Map<DetailedCommonEditPart, Node> partsToNodes = new HashMap<DetailedCommonEditPart, Node>();
		rootPart.contributeNodesToGraph(graph, partsToNodes);
		new DirectedGraphLayout().visit(graph); // Calculate layout
		rootPart.applyGraphResults(graph, partsToNodes); // Resize and reposition figures.
	}
}
// - UNUSED CODE ............................................................................................
