//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class MultiLabelLine extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger	= Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");

	// - F I E L D - S E C T I O N ............................................................................
	private final GridLayout	grid	= new GridLayout();

	//	private final int					numCols	= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MultiLabelLine() {
		grid.horizontalSpacing = 2;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 2;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addColumn(final String columnText, final int textStyle, final Color columnColor) {
		final Label column = new StandardLabel(columnText);
		column.setForegroundColor(columnColor);
		column.setFont(new Font(Display.getDefault(), "Tahoma", 8, textStyle));
		this.add(column);
		//    grid.numColumns = numCols++;
		this.layout();
	}
}

// - UNUSED CODE ............................................................................................
