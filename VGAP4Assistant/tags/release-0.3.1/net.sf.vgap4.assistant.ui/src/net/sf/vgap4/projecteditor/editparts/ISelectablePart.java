//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.swt.widgets.Composite;

// - CLASS IMPLEMENTATION ...................................................................................
public interface ISelectablePart {
	public IPropertyPage createPropertyPage(final Composite top, boolean singleSelected);
}

// - UNUSED CODE ............................................................................................
