//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.exceptions.ResourceException;
import net.sf.vgap4.assistant.factories.IconImageFactory;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.factories.LetterImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.MiningInformation;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedBaseFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final Base	baseModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedBaseFigure(final Base model) {
		super(model);
		baseModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	private void addEstimates(final Figure parent) {
		//- Process model data to get the recommended levels for some structures.
		final int colonists = baseModel.getFieldNumber("Colonists");
		final int mines = Math.min(colonists / 100, 50);
		final int factories = Math.min(colonists / 1000, 50);

		//- Process model to get the timed events.

		Label estimates = null;
		final StringBuffer recommendations = new StringBuffer("Recommended: ");
		recommendations.append("Mines - ");
		try {
			estimates = ImageFactory.getLabel("icons/starship.gif");
			estimates.setText("Recommended: " + baseModel.getFieldNumber("Factories"));
			estimates.setIconTextGap(3);
		} catch (final ResourceException e) {
			estimates = new StandardLabel("Recommended: " + baseModel.getFieldNumber("Factories"));
		} finally {
			estimates.setSize(this.getPreferredSize());
			//			parent.add(estimates);
		}
	}

	private void addMineralsInfo(final Figure parent) {
		final StandardLabel mineralLabel = new StandardLabel("Minerales:");
		mineralLabel.setFont(AssistantConstants.FONT_MAP_BOLD);
		parent.add(mineralLabel);
		//DEBUG Check mineral values.
		final int max = 1500;
		//		final int res = baseModel.getFieldNumber("resN");
		//		final int sur = baseModel.getFieldNumber("eleN");
		final MineralBarFigure neutronium = new MineralBarFigure("N", AssistantConstants.COLOR_STANDARD_RED,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		neutronium.setLevels(max, baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
		//		final MineralLine neutroniumLine = new MineralLine(MiningInformation.NEUTRONIUM_NAME, neutronium);
		//		neutroniumLine.setValues(baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
		//    if (true) neutroniumLine.setBorder(new LineBorder());
		parent.add(neutronium);
		neutronium.setToolTip(new Label(
				"Cantidad de Neutronio (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure duranium = new MineralBarFigure("D", AssistantConstants.COLOR_STANDARD_DARKBLUE,
				AssistantConstants.COLOR_STANDARD_BLUE);
		duranium.setLevels(max, baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
		//		final MineralLine duraniumLine = new MineralLine(MiningInformation.DURANIUM_NAME, duranium);
		//		duraniumLine.setValues(baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
		//    if (true) duraniumLine.setBorder(new LineBorder());
		parent.add(duranium);
		duranium.setToolTip(new Label(
				"Cantidad de Duranium (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure tritanium = new MineralBarFigure("T", AssistantConstants.COLOR_STANDARD_DARKGREN,
				AssistantConstants.COLOR_STANDARD_GREEN);
		tritanium.setLevels(max, baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
		//		final MineralLine tritaniumLine = new MineralLine(MiningInformation.TRITANIUM_NAME, tritanium);
		//		tritaniumLine.setValues(baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
		//    if (true) tritaniumLine.setBorder(new LineBorder());
		parent.add(tritanium);
		tritanium.setToolTip(new Label(
				"Cantidad de Tritanium (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure molybdenum = new MineralBarFigure("M", AssistantConstants.COLOR_BROWN,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		molybdenum.setLevels(max, baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
		//		final MineralLine molybdenumLine = new MineralLine(MiningInformation.MOLYBDENUM_NAME, molybdenum);
		//		molybdenumLine.setValues(baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
		//    if (true) molybdenumLine.setBorder(new LineBorder());
		parent.add(molybdenum);
		molybdenum
				.setToolTip(new Label("Cantidad de Molybdenunm en la base. Cada bloque de color representa 150 unidades."));
	}

	@Override
	protected void addAdditionalContents() {
		this.addMineralsInfo(group);
		this.addStructuresInfo(group);
		this.addEstimates(group);
		this.addExtractionInfo(group);
	}

	protected void addExtractionInfo(final Figure parent) {
		final int onPlanet = baseModel.getOnPlanet();
		final AssistantMap map = baseModel.getMap();
		final Planet planet = map.getPlanet4Id(onPlanet);
		final LetterImageFactory factory = new LetterImageFactory();

		for (int minId = MiningInformation.NEUTRONIUM_ID; minId <= MiningInformation.MOLYBDENUM_ID; minId++) {
			final StandardLabel extractionLabel = new StandardLabel(planet.getExtractionLabel(minId));
			factory.setInitial(MiningInformation.decode(minId));
			extractionLabel.setIcon(factory.getImage());
			parent.add(extractionLabel);
		}
	}

	protected void addStructuresInfo(final Figure parent) {
		Label factories = null;
		Label farms = null;
		Label smelters = null;
		Label mines = null;
		try {
			factories = ImageFactory.getLabel("icons/factory.gif");
			factories.setText("Factories: " + baseModel.getFieldNumber("Factories") + "/" + baseModel.getFactoryEstimates());
			factories.setIconTextGap(3);
			factories.setToolTip(new StandardLabel("Factorias en la base/Factorias recomendadas"));
			farms = ImageFactory.getLabel("icons/farm.gif");
			farms.setText("Farms: " + baseModel.getFieldNumber("Farm") + "/" + baseModel.getFarmEstimates());
			farms.setIconTextGap(3);
			farms.setToolTip(new StandardLabel("Farms en la base/Farms recomendadas"));
			smelters = ImageFactory.getLabel("icons/smelter.gif");
			smelters.setText("Smelters: " + baseModel.getFieldNumber("Smelters") + "/" + baseModel.getSmelterEstimates());
			smelters.setIconTextGap(3);
			smelters.setToolTip(new StandardLabel("Smelters en la base/Smelters recomendadas"));
			mines = ImageFactory.getLabel("icons/mine.gif");
			mines.setText("Mines: " + baseModel.getFieldNumber("Mines") + "/" + baseModel.getMineEstimates());
			mines.setIconTextGap(3);
			mines.setToolTip(new StandardLabel("Minas en la base/Minas recomendadas"));
		} catch (final ResourceException e) {
			factories = new StandardLabel("Factories: " + baseModel.getFieldNumber("Factories"));
			farms = new StandardLabel("Farms: " + baseModel.getFieldNumber("Farms"));
			smelters = new StandardLabel("Smelters: " + baseModel.getFieldNumber("Smelters"));
			mines = new StandardLabel("Mines: " + baseModel.getFieldNumber("Mines"));
		} finally {
			//			factories.setSize(getPreferredSize());
			parent.add(factories);
			//			farms.setSize(getPreferredSize());
			parent.add(farms);
			smelters.setSize(this.getPreferredSize());
			parent.add(smelters);
			mines.setSize(this.getPreferredSize());
			parent.add(mines);
		}
	}

	@Override
	protected Image createDetailedIcon() {
		//- Generate the image for the current element.
		final IconImageFactory imageFactory = new IconImageFactory();
		imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
		if (baseModel.getFFStatus().equals(AssistantConstants.FFSTATUS_FOE)) imageFactory.setEnemy(true);
		return imageFactory.generateImage();
	}
}

// - UNUSED CODE ............................................................................................
