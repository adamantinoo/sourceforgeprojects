//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.exceptions.ResourceException;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.ui.Activator;

// - CLASS IMPLEMENTATION ...................................................................................
public class ImageFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger										logger				= Logger.getLogger("net.sf.vgap4.assistant.factories");

	// - F I E L D - S E C T I O N ............................................................................
	private static Hashtable<String, Image>	imageRegistry	= new Hashtable<String, Image>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ImageFactory() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public static Image getImage(String reference) throws ResourceException {
		//- Locate the image in the cache registry.
		Image image = imageRegistry.get(reference);
		if (null == image) {
			//- Create a new image from the file.
			ImageData imageData = Activator.getImageDescriptor(reference).getImageData();
			if (null != imageData) {
				image = new Image(Display.getCurrent(), imageData);
				imageRegistry.put(reference, image);
				return image;
			}
			throw new ResourceException("The resource has not been found on the plug-in namespace.", reference);
		} else
			return image;
	}

	public static Figure getFigure(String reference) throws ResourceException {
		return getLabel(reference);
	}

	public static Label getLabel(String reference) throws ResourceException {
		Label imageFigure = new StandardLabel();
		imageFigure.setIcon(getImage(reference));
		imageFigure.setIconAlignment(PositionConstants.LEFT);
		imageFigure.setIconTextGap(0);
		return imageFigure;
	}
}
// - UNUSED CODE ............................................................................................
