//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.views;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.projecteditor.editparts.ISelectablePart;
import net.sf.vgap4.projecteditor.editparts.PlanetEditPart;
import net.sf.vgap4.projecteditor.editparts.ShipEditPart;
import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This sample class demonstrates how to plug-in a new workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly, but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace). The view is connected to the model using
 * a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be presented in the view. Each view can
 * present the same model objects using different labels and icons, if needed. Alternatively, a single label
 * provider can be shared between views in order to ensure that objects of the same type are presented in the
 * same way everywhere.
 * <p>
 */
public class DetailedCardView extends ViewPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger				= Logger.getLogger("net.sf.vgap4.projecteditor.views");
	// - G L O B A L - C O N S T A N T S
	public static final String		ID						= "net.sf.vgap4.projecteditor.views.DetailedCardView.id";
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/**
	 * Top element in the display hierarchy. The children of this element are the ones disposed when changing
	 * the selection.
	 */
	private Composite							top;
	/**
	 * Stores the list of used pages to be removed when the selection changes. If not removed the pages stay
	 * dangling from the listener notification and receive events when they are not visible or used.
	 */
	private Vector<IPropertyPage>	propertyPages	= new Vector<IPropertyPage>();

	// private TreeViewer viewer;

	// private DrillDownAdapter drillDownAdapter;
	// private Action action1;
	// private Action action2;
	// private Action doubleClickAction;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public UnitPropertiesView() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E

	/*
	 * The content provider class is responsible for providing objects to the view. It can wrap existing objects
	 * in adapters or simply return objects as-is. These objects may be sensitive to the current input of the
	 * view, or ignore it and always show the same content (like Task List, for example).
	 */
	/**
	 * The constructor.
	 */
	public DetailedCardView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(ID, this);
	}

	// - P U B L I C - S E C T I O N
	public Composite getTopControl() {
		return top;
	}

	/**
	 * Disposes the elements that are hanging from the top container of the view. This really clears the view
	 * contents and prepares the control to display a new set of elements that will be added in sequence. This
	 * method also clears the list of pages that belong to selected elements that are the be available for
	 * disposal and disconnected from the notification system.
	 */
	public void clearTopControl() {
		final Control[] childs = top.getChildren();
		for (int i = 0; i < childs.length; i++) {
			final Control child = childs[i];
			child.dispose();
		}
		// - Clear property pages references to clean up the listeners.
		final Iterator<IPropertyPage> it = propertyPages.iterator();
		while (it.hasNext()) {
			it.next().dispose();
		}
		propertyPages = new Vector<IPropertyPage>();
	}

	/**
	 * Updates the content of the <code>SelectionView</code> with the selection elements from the
	 * <code>EditorPanel</code>.<br>
	 * The contents presentation depends on the selection. For single units the view presents some information,
	 * for multiple units it displays a list of the selected units and if the selection goes to default (the
	 * background Map) the view displays the whole list of visible units.
	 */
	public void updateSelection(StructuredSelection selectionContent, IWorkbenchPart part) {
		if (!selectionContent.isEmpty()) {
			// - Count the elements in the selection to check if we display a property page or a table
			if (selectionContent.size() > 1) {
				updateMultipleSelection(selectionContent, part);
			} else {
				// - Check if the selection can be processed as a ISelectablePart
				Object aPart = selectionContent.getFirstElement();
				if (aPart instanceof ISelectablePart) {
					// - Get the Part selected (there is only one) and convert it to the higher common interface
					final ISelectablePart selectedPart = (ISelectablePart) selectionContent.getFirstElement();
					updateSingleSelection(selectedPart);
				}
			}
		}
	}

	// - P R O T E C T E D - S E C T I O N
	private void addPropertyPage(IPropertyPage page) {
		propertyPages.addElement(page);
	}

	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	@Override
	public void createPartControl(Composite parent) {
		// singleton = this;
		// - Create a new container and set it to a layout of row elements inside a one column grid.
		top = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		top.setLayout(layout);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		// viewer.getControl().setFocus();
	}

	private void newTableViewer(StructuredSelection selectionContent, final IWorkbenchPart workbenchEditor) {
		// TODO Create a list of special labels (with icon) and put them in a column
		final Composite table = new Composite(top, SWT.NONE);
		final FillLayout grid = new FillLayout();
		// grid.numColumns = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.type = SWT.VERTICAL;
		table.setLayout(grid);
		// final SelectionView vv = this;
		final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
		while (it.hasNext()) {
			final AbstractGraphicalEditPart part = it.next();
			final VGAP4Node model = (VGAP4Node) part.getModel();
			final CLabel lb = new CLabel(table, SWT.SHADOW_OUT);
			lb.setData(part);
			// FIXME Adapt this to the use of multiple pages
			// lb.addMouseListener(new MouseListener() {
			//
			// public void mouseDoubleClick(MouseEvent e) {
			// // // TODO The label has been selected and then we can forward this to the new selection
			// // logger.info("Label " + lb.getText() + " selected");
			// }
			//
			// /**
			// * When the mouse is clicked on any of the labels of the selection, the selection has to change to
			// * this element. To accomplish this I have to change the viewer selection for the current editor
			// with
			// * the selected label.
			// */
			// public void mouseDown(MouseEvent e) {
			// // - We receive the workbench part of the selection that can be identified as the Editor,
			// if (workbenchEditor instanceof VGAP4MapViever) {
			// // - Get a reference to the GraphicalViewer. This is located inside the Editor Page.
			// final VGAP4MapViever editor = (VGAP4MapViever) workbenchEditor;
			// final GraphicalViewer viewer = editor.getGraphicalViewer();
			// // - The Label generic Data contains the EditPart to be selected.
			// final EditPart data = (EditPart) lb.getData();
			// viewer.select(data);
			// logger.info("Label " + lb.getText() + " selected");
			// }
			// }
			//
			// public void mouseUp(MouseEvent e) {
			// // EMPTY method. Not being used
			// }
			//
			// });
			final ImageDescriptor im = Activator.getImageDescriptor("/icons/sample2.gif");
			lb.setImage(im.createImage());
			lb.setText(model.getName());
		}
	}

	private void updateMultipleSelection(StructuredSelection selectionContent, IWorkbenchPart workbench) {
		clearTopControl();
		// - Display a table with the selection elements
		// newTableViewer(selectionContent, workbench);

		final Composite table = new Composite(top, SWT.NONE);
		final FillLayout grid = new FillLayout();
		// grid.numColumns = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.type = SWT.VERTICAL;
		table.setLayout(grid);

		// TODO For each selected element add a new <code>Group</code> that is clickable to set the selection
		final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
		while (it.hasNext()) {
			final AbstractGraphicalEditPart part = it.next();
			final IPropertyPage page = ((ISelectablePart) part).createPropertyPage(table, false);
			addPropertyPage(page);
		}
		top.layout();
	}

	/**
	 * Depending on the part type selected this method creates and connect to the <code>top</code> container
	 * the presentation data that is available for that unit. There are presentation differences depending on
	 * unit side and other game model data (detection, type).
	 */
	private void updateSingleSelection(ISelectablePart selectedPart) {
		// - Detect the type of part selected and do the operations necessary for each type
		if (selectedPart instanceof PlanetEditPart) {
			ISelectablePart war = selectedPart;
			clearTopControl();
			addPropertyPage(war.createPropertyPage(top, true));
		}
		if (selectedPart instanceof ShipEditPart) {
			ISelectablePart war = selectedPart;
			clearTopControl();
			addPropertyPage(war.createPropertyPage(top, true));
		}
		top.layout();
	}
}
// - UNUSED CODE ............................................................................................
