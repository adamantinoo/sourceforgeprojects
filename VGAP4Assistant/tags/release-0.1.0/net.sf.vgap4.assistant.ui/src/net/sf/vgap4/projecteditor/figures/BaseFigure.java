//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Color;

import net.sf.gef.core.figures.SelectableFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseFigure extends SelectableFigure {
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	protected static final int	MARGIN				= 1;
	protected static final int	LABEL_HEIGHT	= 15;
	protected static final int	GAP4					= -4;
	protected static final int	GAP5					= 5;

	protected static final int	CHAR_WIDTH		= 6;
	protected static final int	CHAR_HEIGHT		= 12;
	protected static final int	GAP2					= 2;
	protected static final int	GAP3					= 3;
	protected static final int	LABEL_WIDTH		= (4 + 1) * 6 + GAP2;

	// - F I E L D S
	/** Label to draw the value for the speed property. */
	protected final Label				idLabel				= new UnitDisplayLabel("00");
	/** Label to draw the value if this unit direction. */
	protected final Label				nameLabel			= new UnitDisplayLabel("000");
	protected MineralsFigure		minerals			= new MineralsFigure();
	/** Drawing color for the unit. Depends on the game side. */
	private Color								color					= ColorConstants.darkGray;
	/** Reference to the icon drawing class that represents this unit. */
	protected BaseIconFigure		iconic				= new BaseIconFigure(this);
	// /** Selection state value obtained from the EditPart and used to selection visual feedback. */
	// private int selected = EditPart.SELECTED_NONE;

	private XYLayout						grid;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public BaseFigure() {
		init();
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Before setting the top-left corner location we have to adjust the element by the Hot Spot displacement
	 * because the model coordinates are the coordinates for the planet center. The planet image surrounds that
	 * point.
	 */
	public void setCoordinates(Point location) {
		Point offset = this.getHotSpot();
		Point coords = location;
		coords.x -= offset.x;
		coords.y -= offset.y;
		this.setLocation(coords);
	}

	public void setNeutronium(int[] mineralData) {
		this.minerals.setNeutronium(mineralData);
	}

	public void setDuranium(int[] mineralData) {
		this.minerals.setDuranium(mineralData);
	}

	public void setTritanium(int[] mineralData) {
		this.minerals.setTritanium(mineralData);
	}

	public void setMolybdenum(int[] mineralData) {
		this.minerals.setMolybdenum(mineralData);
	}

	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed
	 * is represented as a numeric label with the speed in knots.
	 */
	public void setId(final int newId) {
		try {
			final NumberFormat nf = NumberFormat.getIntegerInstance();
			nf.setMinimumIntegerDigits(3);
			nf.setMaximumFractionDigits(0);
			idLabel.setText(nf.format(newId));

		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			idLabel.setText(new Integer(newId).toString());
		}
		// - Position the label depending on the new dimension.
		final Dimension iconicSize = iconic.getSize();
		int labelWidth = idLabel.getSize().width;
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = MARGIN + iconicSize.width + MARGIN;
		elementLocation.y = GAP4;
		elementLocation.width = labelWidth;
		elementLocation.height = LABEL_HEIGHT;
		grid.setConstraint(idLabel, elementLocation);

		this.setSize(this.getPreferredSize());
	}

	public void setName(String newName) {
		nameLabel.setText(newName);
		// - Position the label depending on the new dimension.
		final Dimension iconicSize = iconic.getSize();
		int labelWidth = nameLabel.getSize().width;
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width + MARGIN + MARGIN;
		elementLocation.y = GAP5;
		elementLocation.width = labelWidth;
		elementLocation.height = LABEL_HEIGHT;
		grid.setConstraint(nameLabel, elementLocation);

		this.setSize(this.getPreferredSize());
	}

	protected Color getColor() {
		return color;
	}

	protected void setColor(final Color newColor) {
		color = newColor;
		iconic.repaint();
		this.repaint();
	}

	public void setDrawFigure(BaseIconFigure drawFigure) {
		// - Add the icon drawing class instance.
		iconic = drawFigure;
		// - Position the elements inside the layout with precise point locations.
		final Dimension iconicSize = iconic.getSize();
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = MARGIN;
		elementLocation.y = MARGIN;
		elementLocation.width = iconicSize.width + MARGIN;
		elementLocation.height = iconicSize.height + MARGIN;
		grid.setConstraint(iconic, elementLocation);
	}

	// /**
	// * Returns the selection state value. This method is used to inform the painting process of the visual
	// * selection state.
	// *
	// * @return the selection state value.
	// */
	// public int getSelected() {
	// return selected;
	// }
	//
	// /** Sets the selection value to one of the three selection states. */
	// public void setSelected(final int value) {
	// selected = value;
	// logger.info(toString() + "Selected = " + value);
	// }

	// - P U B L I C - S E C T I O N
	public void init() {
		// - Create the complex internal parts of this figure.
		grid = new XYLayout();
		setLayoutManager(grid);
		// idLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// idLabel.setLabelAlignment(PositionConstants.LEFT);
		// nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// nameLabel.setLabelAlignment(PositionConstants.LEFT);
		this.add(iconic);
		this.add(idLabel);
		this.add(nameLabel);
		this.add(minerals);

		// // - Position the elements inside the layout with precise point locations.
		// final Dimension iconicSize = iconic.getSize();
		// Rectangle elementLocation = new Rectangle();
		// elementLocation.x = MARGIN;
		// elementLocation.y = MARGIN;
		// elementLocation.width = iconicSize.width + MARGIN;
		// elementLocation.height = iconicSize.height + MARGIN;
		// grid.setConstraint(iconic, elementLocation);
		//
		// // - Calculate the max width of the labels
		// int labelWidth = Math.max(idLabel.getSize().width, nameLabel.getSize().width);
		// elementLocation = new Rectangle();
		// elementLocation.x = iconicSize.width + MARGIN + MARGIN;
		// elementLocation.y = GAP4;
		// elementLocation.width = labelWidth;
		// elementLocation.height = LABEL_HEIGHT;
		// grid.setConstraint(idLabel, elementLocation);
		//
		Dimension iconicSize = iconic.getSize();
		Dimension min = minerals.getSize();
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width + MARGIN + MARGIN;
		elementLocation.y = GAP5 + 5 + 7;
		elementLocation.width = min.width;
		elementLocation.height = min.height;
		grid.setConstraint(minerals, elementLocation);

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize());
		// DEBUG Comment the next line to check of presentation changes
		// this.repaint();
	}

	public Point getHotSpot() {
		Point offset = iconic.getHotSpot();
		return new Point(offset.x + MARGIN, offset.y + MARGIN);
	}

	// - P R O T E C T E D - S E C T I O N
	protected String decodeSelectionCode(final int selected) {
		if (EditPart.SELECTED_NONE == selected) return "SELECTED_NONE";
		if (EditPart.SELECTED == selected) return "SELECTED";
		if (EditPart.SELECTED_PRIMARY == selected) return "SELECTED_PRIMARY";
		return "UNDEFINED";
	}

	// protected Dimension getRightSideSize() {
	// final Rectangle speedBound = idLabel.getBounds().getCopy();
	// final Rectangle directionBound = nameLabel.getBounds().getCopy();
	// final Rectangle rightSize = new Rectangle();
	// rightSize.x = StrictMath.min(speedBound.x, directionBound.x);
	// rightSize.y = StrictMath.min(speedBound.y, directionBound.y);
	// rightSize.width = StrictMath.max(speedBound.width, directionBound.width);
	// rightSize.height = StrictMath.min(speedBound.height, directionBound.height);
	//
	// final int maxChars = StrictMath.max(idLabel.getText().length(), nameLabel.getText().length());
	// final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH, CHAR_HEIGHT);
	// // return leftLabelSize;
	// return new Dimension(rightSize.width, rightSize.height);
	// }
	// private Dimension getLeftSideSize() {
	// final int maxChars = StrictMath.max(idLabel.getText().length(), nameLabel.getText().length());
	// final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT * 2);
	// // leftLabelSize.width+=MARGIN;
	// // leftLabelSize.height+=MARGIN+MARGIN;
	// return leftLabelSize;
	// }
	//
	// private Figure createLeftSide() {
	// final Figure labelContainer = new Figure();
	// final GridLayout grid = new GridLayout();
	// grid.numColumns = 1;
	// grid.verticalSpacing = 0;
	// grid.horizontalSpacing = 0;
	// grid.marginHeight = 0;
	// grid.marginWidth = 0;
	// labelContainer.setLayoutManager(grid);
	// idLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
	// // speedlabel.setBorder(new LineBorder(1));
	// labelContainer.add(idLabel);
	// nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
	// // dirLabel.setBorder(new LineBorder(1));
	// labelContainer.add(nameLabel);
	// labelContainer.validate();
	// return labelContainer;
	// }
	// - O V E R R I D E - S E C T I O N
	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		int labelWidth = Math.max(Math.max(idLabel.getSize().width, nameLabel.getSize().width),
				this.minerals.getSize().width);

		// final Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = iconicSize.width + MARGIN + MARGIN + labelWidth + MARGIN;
		if (EditPart.SELECTED_NONE == this.getSelected())
			fullSize.height = MARGIN + LABEL_HEIGHT + MARGIN + 0;
		else
			fullSize.height = MARGIN + LABEL_HEIGHT + MARGIN + this.minerals.getSize().height + 1;
		// fullSize.height = GAP2 + Math.max(iconicSize.height, rightSize.height) + GAP2;
		// logger.info(">>> ENTERING");
		// logger.info("iconicSize = " + iconicSize);
		// logger.info("rightSize = " + rightSize);
		// logger.info("fullSize = " + fullSize);
		// logger.info("<<< EXITING");
		return fullSize;
	}

	@Override
	protected void paintChildren(final Graphics graphics) {
		// TODO Not all elements are displayed depending on the selection status.
		if (EditPart.SELECTED_NONE == this.getSelected()) {
			// - Disable the mineral graphic and the native graphic.
			this.minerals.setVisible(false);
			// TODO The same for the native information.
		}
		if (EditPart.SELECTED_PRIMARY == this.getSelected()) this.minerals.setVisible(true);
		if (EditPart.SELECTED == this.getSelected()) this.minerals.setVisible(true);
		// logger.info("Selection state for " + this.nameLabel.getText() + ": " + this.getSelected());
		this.setSize(this.getPreferredSize());
		super.paintChildren(graphics);
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("PlanetFigure[");
		buffer.append(this.getLocation());
		buffer.append(idLabel.getText()).append("-");
		buffer.append(nameLabel.getText()).append("-");
		// buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	// public Rectangle getReferenceBox() {
	// return iconic.getBounds().getCopy();
	// }

	// protected GridLayout createStdLayout(final int margin) {
	// final GridLayout grid = new GridLayout();
	// grid.numColumns = 2;
	// grid.horizontalSpacing = margin;
	// grid.marginHeight = margin;
	// grid.marginWidth = margin;
	// grid.verticalSpacing = 0;
	// return grid;
	// }

	// - I N T E R F A C E - N A M E

}
// - UNUSED CODE ............................................................................................
