//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class Sector extends VGAP4Node {
	private static final long	serialVersionUID	= 7553570053350141158L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private int x;
	// private int y;
	private int								width;
	private int								height;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public Sector(String name, int x, int y, int width, int height) {
		setName(name);
		setLocation(new Point(x, y));
		this.width = width;
		this.height = height;
	}
	// - G E T T E R S / S E T T E R S

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
}
// - UNUSED CODE ............................................................................................
