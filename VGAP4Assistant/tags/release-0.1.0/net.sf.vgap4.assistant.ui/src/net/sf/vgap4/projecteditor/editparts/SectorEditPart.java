//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.gef.EditPolicy;

import net.sf.vgap4.projecteditor.figures.SectorFigure;
import net.sf.vgap4.projecteditor.model.Sector;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class SectorEditPart extends VGAP4NodeEditPart {
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public SectorEditPart() {
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	private Sector getCastedModel() {
		return (Sector) getModel();
	}

	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	/**
	 * Update the visual representation of the Planet (the Figure part) from the model data. In Planet units
	 * this represents all the visible attributes that can be presented on the Properties View.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		SectorFigure fig = (SectorFigure) getFigure();
		Sector sector = getCastedModel();

		// - Update figure visuals from current model data.
		fig.setCoordinates(sector.getLocation());
		fig.setName(sector.getName());
		super.refreshVisuals();
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("SectorEditPart[");
		buffer.append(this.getCastedModel().getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		// // buffer.append(((Figure)this.isSelectable()).getLocation()).append("-");
		// buffer.append(this.getModel().toString()).append("-");
		// buffer.append(this.getFigure().toString()).append("-");
		// buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}
	// - I N T E R F A C E - P R O P E R T Y C H A N G E L I S T E N E R
	// @Override
	// public void propertyChange(PropertyChangeEvent evt) {
	// String prop = evt.getPropertyName();
	// super.propertyChange(evt);
	// }
	//
	// // - I N T E R F A C E - I S E L E C T A B L E P A R T
	// public IPropertyPage createPropertyPage(Composite top, boolean singleSelected) {
	// // - Create a new set of controls for this EditPart.
	// final PlanetPropertyPage page = new PlanetPropertyPage(top);
	// page.setModel((Planet) getModel());
	// // page.createContents();
	// return page;}
}
// - UNUSED CODE ............................................................................................
