//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.parts.AbstractEditPartFactory;
import es.ftgroup.gef.parts.AbstractGenericEditPart;
import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.projecteditor.model.Sector;
import net.sf.vgap4.projecteditor.model.Ship;
import net.sf.vgap4.projecteditor.model.VGAP4Map;
import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4ProjectEditPartFactory extends AbstractEditPartFactory {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public VGAP4ProjectEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - A B S T R A C T - S E C T I O N
	/**
	 * Maps the model structure for this project to the EditPart. It returns a generic
	 * <code>AbstractGenericEditPart</code> that is commonplace for all developments and has some factory
	 * features that allow better parameterization of figure factories.
	 * 
	 * @param modelElement
	 *          the model piece that requests the creation of a new EditPart
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof VGAP4Map) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ "VGAP4ProjectDiagram" + "]");
			return new DiagramEditPart();
		}
		if (modelElement instanceof Sector) {
			logger.info("Generating SectorEditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with name [" + ((VGAP4Node) modelElement).getName() + "]");
			return new SectorEditPart();
		}
		if (modelElement instanceof Planet) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((VGAP4Node) modelElement).getName() + "]");
			return new PlanetEditPart();
		}
		if (modelElement instanceof Base) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((VGAP4Node) modelElement).getName() + "]");
			return new BaseEditPart();
		}
		if (modelElement instanceof Ship) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((VGAP4Node) modelElement).getName() + "]");
			return new ShipEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}
}
// - UNUSED CODE ............................................................................................
