//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4ProjectPaletteFactory {
	private static Logger				logger								= Logger.getLogger("net.sf.vgap4.projecteditor.editor");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	/** Preference ID used to persist the palette location. */
	private static final String	PALETTE_DOCK_LOCATION	= "ShapesEditorPaletteFactory.Location";
	/** Preference ID used to persist the palette size. */
	private static final String	PALETTE_SIZE					= "ShapesEditorPaletteFactory.Size";
	/** Preference ID used to persist the flyout palette's state. */
	private static final String	PALETTE_STATE					= "ShapesEditorPaletteFactory.State";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public VGAP4ProjectPaletteFactory() {
	}

	/**
	 * Creates the PaletteRoot and adds all palette elements. Use this factory method to create a new palette
	 * for your graphical editor.
	 * 
	 * @return a new PaletteRoot
	 */
	public static PaletteRoot createPalette() {
		PaletteRoot palette = new PaletteRoot();
		palette.add(createToolsGroup(palette));
		return palette;
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	/** Create the "Tools" group. */
	private static PaletteContainer createToolsGroup(PaletteRoot palette) {
		PaletteGroup toolGroup = new PaletteGroup("Tools");

		// Add a selection tool to the group
		ToolEntry tool = new PanningSelectionToolEntry();
		toolGroup.add(tool);
		palette.setDefaultEntry(tool);

		// Add a marquee tool to the group
		toolGroup.add(new MarqueeToolEntry());

		return toolGroup;
	}
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
}

// - UNUSED CODE ............................................................................................
