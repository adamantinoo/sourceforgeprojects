//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.views;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import net.sf.gef.core.views.ISynchronizableView;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This sample class demonstrates how to plug-in a new workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly, but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace). The view is connected to the model using
 * a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be presented in the view. Each view can
 * present the same model objects using different labels and icons, if needed. Alternatively, a single label
 * provider can be shared between views in order to ensure that objects of the same type are presented in the
 * same way everywhere.
 * <p>
 */
public class SelectionPropertiesView extends ViewPart implements ISynchronizableView {
	private static Logger				logger					= Logger.getLogger("net.sf.vgap4.assistant.views");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	ID							= "net.sf.vgap4.assistant.sandbox.views.SelectionPropertiesView.id";
	public static final int			NAME_COLUMN			= 0;
	public static final int			LOCATION_COLUMN	= 1;
	public static final int			CLASS_COLUMN		= 2;

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private TableViewer					viewer;
	private Action							action1;
	private Action							action2;
	private Action							doubleClickAction;

	private ViewContentProvider	contentProvider;
	private ViewLabelProvider		labelProvider;
	private Vector							columns					= new Vector();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SelectionPropertiesView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		//- Try to register this view to all editor pages open.
		Object editors = Activator.getRegisteredEditors();
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(Composite parent) {
		contentProvider = new ViewContentProvider();
		labelProvider = new ViewLabelProvider();

		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(labelProvider);
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		TableColumn column = new TableColumn(viewer.getTable(), SWT.NONE);
		column.setWidth(180);
		column.setText("Name");
		this.columns.add(column);

		column = new TableColumn(viewer.getTable(), SWT.NONE);
		column.setWidth(110);
		column.setText("Location");
		this.columns.add(column);

		column = new TableColumn(viewer.getTable(), SWT.NONE);
		column.setWidth(200);
		column.setText("Class");
		this.columns.add(column);

		viewer.setInput(this.contentProvider.getModel());
		viewer.getTable().setLayout(new FillLayout());
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setHeaderVisible(true);

		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				SelectionPropertiesView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(action1);
		manager.add(new Separator());
		manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(action1);
		manager.add(action2);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(action2);
	}

	private void makeActions() {
		action1 = new Action() {
			@Override
			public void run() {
				showMessage("Action 1 executed");
			}
		};
		action1.setText("Action 1");
		action1.setToolTipText("Action 1 tooltip");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
				ISharedImages.IMG_OBJS_INFO_TSK));

		action2 = new Action() {
			@Override
			public void run() {
				showMessage("Action 2 executed");
			}
		};
		action2.setText("Action 2");
		action2.setToolTipText("Action 2 tooltip");
		action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
				ISharedImages.IMG_OBJS_INFO_TSK));
		doubleClickAction = new Action() {
			@Override
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				showMessage("Double-click detected on " + obj.toString());
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "Sample View", message);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	@Override
	public void receiveSelection(Vector<Object> models) {
		// - Pass the model to the ViewContentProvider.
		contentProvider.setModelContents(models);
		//		this.viewer.setInput(models);
		this.viewer.refresh(true);
		this.viewer.refresh(models);
		viewer.setInput(getViewSite());
	}
}

//- CLASS IMPLEMENTATION ...................................................................................

/*
 * The content provider class is responsible for providing objects to the view. It can wrap existing objects
 * in adapters or simply return objects as-is. These objects may be sensitive to the current input of the
 * view, or ignore it and always show the same content (like Task List, for example).
 */

class ViewContentProvider implements IStructuredContentProvider {
	private Vector<Object>	modelContents;

	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	public void dispose() {
	}

	public Object[] getElements(Object parent) {
		//TODO parent is not used at this moment. Review its contents.
		if (null == this.modelContents) return new String[] { "One", "Two", "Three" };
		return this.modelContents.toArray();
	}

	public void setModelContents(Vector<Object> models) {
		this.modelContents = models;
	}

	public Object getModel() {
		if (null == this.modelContents) {
			VGAP4Node node = new VGAP4Node();
			node.setName("-empty selection-");
			return node;
		} else
			return this.modelContents;
	}
}

/** This class can forward the calls to the model if this implements the ITableLabelProvider interface */
class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object obj, int index) {
		if (obj instanceof VGAP4Node) {
			switch (index) {
				case SelectionPropertiesView.NAME_COLUMN:
					return ((VGAP4Node) obj).getName();
				case SelectionPropertiesView.LOCATION_COLUMN:
					String location = ((VGAP4Node) obj).getLocation().x + " - " + ((VGAP4Node) obj).getLocation().y;
					return location;
				case SelectionPropertiesView.CLASS_COLUMN:
					String clasInfo = "N/A";
					if (obj instanceof Planet) {
						Planet planet = (Planet) obj;
						clasInfo = "N" + planet.getTotalNatives() + "/" + "M" + planet.getMineralTotal() / 1000 + "k";
					}
					if (obj instanceof Base) {
						Base base = (Base) obj;
						clasInfo = "M" + base.getMineralTotal() / 1000 + "k/N" + base.getTotalNatives() + "k/C"
								+ base.getColonists();
					}
					return clasInfo;
				default:
					return obj.toString();
			}
		} else
			return obj.toString();
	}

	public Image getColumnImage(Object obj, int index) {
		if (0 == index) {
			//TODO Set the image depending on the class of the selection.
			return getImage(obj);
		} else
			return null;
	}

	@Override
	public Image getImage(Object obj) {
		return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}

	@Override
	public String getText(Object element) {
		if (element instanceof VGAP4Node)
			return ((VGAP4Node) element).getName();
		else
			return element.toString();
	}
}

class NameSorter extends ViewerSorter {
}
// - UNUSED CODE ............................................................................................
