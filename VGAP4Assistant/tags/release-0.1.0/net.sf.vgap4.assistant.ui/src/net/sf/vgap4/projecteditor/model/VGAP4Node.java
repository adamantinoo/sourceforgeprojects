//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4Node extends PositionableUnit {
	private static Logger				logger		= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	FLD_ID		= "VGAP4Node.FLD_ID";

	// - M O D E L F I E L D S
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	private int									idNumber	= -1;
	/**
	 * Stores the planet name as identified in the source data. This is also a copy of the name found on the
	 * <code>PlanetInformation</code>.
	 */
	private String							name;
	private int									lastTurn	= -1;
	private String							notes			= "";

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getIdNumber() {
		return idNumber;
	}

	public String getIdString() {
		return new Integer(idNumber).toString();
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLastTurn() {
		return lastTurn;
	}

	public void setLastTurn(int lastTurn) {
		this.lastTurn = lastTurn;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String text) {
		this.notes = text;
	}
}

// - UNUSED CODE ............................................................................................
