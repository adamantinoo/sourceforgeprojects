//  PROJECT:        net.sf.vgap4.assistant.application
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.app;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import net.sf.vgap4.assistant.actions.NewVGAP4FileAction;

// - CLASS IMPLEMENTATION ...................................................................................
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.assistant.app");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private IWorkbenchAction		newAction;
	private IWorkbenchAction		closeAction;
	private IWorkbenchAction		closeAllAction;
	private IWorkbenchAction		saveAction;
	private IWorkbenchAction		saveAsAction;
	private IWorkbenchAction		saveAllAction;
	private IWorkbenchAction		printAction;
	private IWorkbenchAction		moveAction;
	private IWorkbenchAction		renameAction;
	private IWorkbenchAction		refreshAction;
	private IWorkbenchAction		propertiesAction;
	private IWorkbenchAction		exitAction;
	private IWorkbenchAction		undoAction;
	private IWorkbenchAction		redoAction;
	private IWorkbenchAction		cutAction;
	private IWorkbenchAction		copyAction;
	private IWorkbenchAction		pasteAction;
	private IWorkbenchAction		selectAllAction;
	private IWorkbenchAction		closePerspectiveAction;
	private IWorkbenchAction		closeAllPerspectivesAction;
	private IWorkbenchAction		aboutAction;
	private IWorkbenchAction		preferencesAction;
	private IWorkbenchAction		helpAction;
	private IWorkbenchAction		dynamicHelpAction;
	private IContributionItem		viewsList;
	private IContributionItem		perspectivesList;
	private IWorkbenchAction		introAction;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ApplicationActionBarAdvisor(final IActionBarConfigurer configurer) {
		super(configurer);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Populates the Menu Bar
	 */
	@Override
	protected void fillMenuBar(final IMenuManager menuBar) {
		final MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE); //$NON-NLS-1$
		final MenuManager editMenu = new MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT); //$NON-NLS-1$
		// MenuManager navigateMenu = new MenuManager( "&Navigate", IWorkbenchActionConstants.M_NAVIGATE );
		// //$NON-NLS-1$
		final MenuManager windowMenu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW); //$NON-NLS-1$
		final MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP); //$NON-NLS-1$

		// Adding menus
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		// menuBar.add( navigateMenu );
		// Add a group marker indicating where action set menus will appear.
		menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menuBar.add(windowMenu);
		menuBar.add(helpMenu);

		// Populating File Menu
		fileMenu.add(newAction);
		fileMenu.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
		// fileMenu.add( openFileAction );
		// fileMenu.add( new GroupMarker( IWorkbenchActionConstants.OPEN_EXT ) );
		fileMenu.add(new Separator());
		fileMenu.add(closeAction);
		fileMenu.add(closeAllAction);
		fileMenu.add(new GroupMarker(IWorkbenchActionConstants.CLOSE_EXT));
		fileMenu.add(new Separator());
		fileMenu.add(saveAction);
		fileMenu.add(saveAsAction);
		fileMenu.add(saveAllAction);
		fileMenu.add(new GroupMarker(IWorkbenchActionConstants.SAVE_EXT));
		fileMenu.add(new Separator());
		fileMenu.add(refreshAction);
		fileMenu.add(new Separator());
		fileMenu.add(printAction);
		fileMenu.add(new GroupMarker(IWorkbenchActionConstants.PRINT_EXT));
		fileMenu.add(new Separator());
		// fileMenu.add( importAction );
		// fileMenu.add( exportAction );
		// fileMenu.add( new GroupMarker( IWorkbenchActionConstants.IMPORT_EXT ) );
		// fileMenu.add( new Separator() );
		fileMenu.add(propertiesAction);
		// fileMenu.add( reopenEditorsList );
		fileMenu.add(new GroupMarker(IWorkbenchActionConstants.MRU));
		fileMenu.add(new Separator());
		fileMenu.add(exitAction);

		// Populating Edit Menu
		editMenu.add(undoAction);
		editMenu.add(redoAction);
		editMenu.add(new Separator());
		editMenu.add(cutAction);
		editMenu.add(copyAction);
		editMenu.add(pasteAction);
		editMenu.add(new Separator());
		// editMenu.add( deleteAction );
		editMenu.add(selectAllAction);
		editMenu.add(new Separator());
		editMenu.add(moveAction);
		editMenu.add(renameAction);
		// editMenu.add( new Separator() );
		// editMenu.add( findAction );

		// // Populating Navigate Menu
		// navigateMenu.add( nextAction );
		// navigateMenu.add( previousAction );
		// navigateMenu.add( new Separator( IWorkbenchActionConstants.MB_ADDITIONS ) );
		// navigateMenu.add( new GroupMarker( IWorkbenchActionConstants.NAV_END ) );
		// navigateMenu.add( new Separator() );
		// navigateMenu.add( backwardHistoryAction );
		// navigateMenu.add( forwardHistoryAction );

		// Window
		final MenuManager perspectiveMenu = new MenuManager("Open Perspective", "openPerspective");
		perspectiveMenu.add(perspectivesList);
		windowMenu.add(perspectiveMenu);
		final MenuManager viewMenu = new MenuManager("Show View");
		viewMenu.add(viewsList);
		windowMenu.add(viewMenu);
		windowMenu.add(new Separator());
		windowMenu.add(closePerspectiveAction);
		windowMenu.add(closeAllPerspectivesAction);
		windowMenu.add(new Separator());
		windowMenu.add(preferencesAction);

		// Help
		helpMenu.add(introAction);
		helpMenu.add(new Separator());
		helpMenu.add(helpAction);
		helpMenu.add(dynamicHelpAction);
		// helpMenu.add( reportABug );
		helpMenu.add(new Separator());
		// TODO Review that this configuration now works as default.
		// MenuManager softwareUpdates = new MenuManager( Messages
		// .getString( "ApplicationActionBarAdvisor.Software_Updates" ), "softwareUpdates" ); //$NON-NLS-1$
		// //$NON-NLS-2$
		// softwareUpdates.add( updateAction );
		// softwareUpdates.add( manageConfigurationAction );
		// helpMenu.add( softwareUpdates );
		// helpMenu.add( new Separator() );
		helpMenu.add(aboutAction);
	}

	/**
	 * Creates the actions and registers them. Registering is needed to ensure that key bindings work. The
	 * corresponding commands keybindings are defined in the plugin.xml file. Registering also provides
	 * automatic disposal of the actions when the window is closed.
	 */
	@Override
	protected void makeActions(final IWorkbenchWindow window) {
		//TODO Create our applications actions and register them.
		newAction = new NewVGAP4FileAction(window);
		register(newAction);
		newAction.setText("New...");

		// - Creates the default common actions and registers them.
		//		newAction = ActionFactory.NEW.create(window);
		//		register(newAction);
		//		newAction.setText("New...");
		closeAction = ActionFactory.CLOSE.create(window);
		register(closeAction);
		closeAllAction = ActionFactory.CLOSE_ALL.create(window);
		register(closeAllAction);
		saveAction = ActionFactory.SAVE.create(window);
		register(saveAction);
		saveAsAction = ActionFactory.SAVE_AS.create(window);
		register(saveAsAction);
		saveAllAction = ActionFactory.SAVE_ALL.create(window);
		register(saveAllAction);
		printAction = ActionFactory.PRINT.create(window);
		register(printAction);
		moveAction = ActionFactory.MOVE.create(window);
		register(moveAction);
		renameAction = ActionFactory.RENAME.create(window);
		register(renameAction);
		refreshAction = ActionFactory.REFRESH.create(window);
		register(refreshAction);
		// importAction = ActionFactory.IMPORT.create( window );
		// register( importAction );
		// exportAction = ActionFactory.EXPORT.create( window );
		// register( exportAction );
		propertiesAction = ActionFactory.PROPERTIES.create(window);
		register(propertiesAction);
		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);
		undoAction = ActionFactory.UNDO.create(window);
		register(undoAction);
		redoAction = ActionFactory.REDO.create(window);
		register(redoAction);
		cutAction = ActionFactory.CUT.create(window);
		register(cutAction);
		copyAction = ActionFactory.COPY.create(window);
		register(copyAction);
		pasteAction = ActionFactory.PASTE.create(window);
		register(pasteAction);
		// deleteAction = ActionFactory.DELETE.create( window );
		// register( deleteAction );
		selectAllAction = ActionFactory.SELECT_ALL.create(window);
		register(selectAllAction);
		// findAction = ActionFactory.FIND.create( window );
		// register( findAction );
		closePerspectiveAction = ActionFactory.CLOSE_PERSPECTIVE.create(window);
		register(closePerspectiveAction);
		closeAllPerspectivesAction = ActionFactory.CLOSE_ALL_PERSPECTIVES.create(window);
		register(closeAllPerspectivesAction);
		aboutAction = ActionFactory.ABOUT.create(window);
		aboutAction.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.ABOUT));
		register(aboutAction);
		preferencesAction = ActionFactory.PREFERENCES.create(window);
		preferencesAction.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID,
				ImageKeys.SHOW_PREFERENCES));
		register(preferencesAction);
		helpAction = ActionFactory.HELP_CONTENTS.create(window);
		register(helpAction);
		dynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(window);
		register(dynamicHelpAction);
		viewsList = ContributionItemFactory.VIEWS_SHORTLIST.create(window);
		perspectivesList = ContributionItemFactory.PERSPECTIVES_SHORTLIST.create(window);
		// reopenEditorsList = ContributionItemFactory.REOPEN_EDITORS.create( window );
		introAction = ActionFactory.INTRO.create(window);
		introAction.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(Application.PLUGIN_ID, ImageKeys.INTRO));
		register(introAction);

		// - My user interface additions
		// newDropDownAction = new NewWizardDropDownAction( window );
		// openFileAction = new OpenFileAction( window );
		// register( openFileAction );
		// updateAction = new UpdateAction( window );
		// updateAction.setImageDescriptor( AbstractUIPlugin.imageDescriptorFromPlugin( Application.PLUGIN_ID,
		// ImageKeys.SEARCH_UPDATES ) );
		// register( updateAction );
		// manageConfigurationAction = new ManageConfigurationAction( window );
		// manageConfigurationAction.setImageDescriptor( AbstractUIPlugin.imageDescriptorFromPlugin(
		// Application.PLUGIN_ID, ImageKeys.MANAGE_CONFIGURATION ) );
		// register( manageConfigurationAction );

		// reportABug = new ReportABugAction( window );
		// reportABug.setImageDescriptor( AbstractUIPlugin.imageDescriptorFromPlugin( Application.PLUGIN_ID,
		// ImageKeys.REPORT_BUG ) );
		// register( reportABug );
		//
		// forwardHistoryAction = ActionFactory.FORWARD_HISTORY.create( window );
		// register( forwardHistoryAction );
		//
		// backwardHistoryAction = ActionFactory.BACKWARD_HISTORY.create( window );
		// register( backwardHistoryAction );
		//
		// nextAction = ActionFactory.NEXT.create( window );
		// register( nextAction );
		//
		// previousAction = ActionFactory.PREVIOUS.create( window );
		// register( previousAction );

	}
}
// - UNUSED CODE ............................................................................................
