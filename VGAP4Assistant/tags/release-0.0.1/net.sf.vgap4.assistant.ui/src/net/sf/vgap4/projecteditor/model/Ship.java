//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class Ship extends VGAP4Node {
	private static final long				serialVersionUID	= -5137898479615467215L;
	private static Logger						logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// - M O D E L F I E L D - I D E N T I F I E R S

	// - M O D E L F I E L D S
	/**
	 * This array stores the information processed for this planet (identified by its ID) for any turn. The data
	 * for turn x can be located inside vector slot x.
	 */
	private Vector<ShipInformation>	shipInformation		= new Vector<ShipInformation>();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	public void addTurnInformation(int turn, ShipInformation shipInfo) {
		// - Get the old value for this planet turn information is available.
		// If not null then we are overriding this data.
		try {
			this.shipInformation.setSize(turn + 1);
			if (null != this.shipInformation.get(turn))
				logger.warning("This operation is overriding some previous information stored for this turn");
			this.shipInformation.set(turn, shipInfo);
			logger.info("Adding ship '" + shipInfo.getName() + "' data for turn " + turn + ".");
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			this.shipInformation.setSize(turn + 1);
			this.shipInformation.set(turn, shipInfo);
			aioobe.printStackTrace();
		}
		// - Cache turn information.
		this.setIdNumber(shipInfo.getIdNumber());
		this.setLastTurn(Math.max(this.getLastTurn(), turn));
		this.setName(this.shipInformation.get(this.getLastTurn()).getName());
		this.setLocation(this.shipInformation.get(this.getLastTurn()).getLocation());
		logger.info("Processing ship id [" + shipInfo.getIdNumber() + "] - " + shipInfo.getName() + ". At location ["
				+ shipInfo.getLocation().x + ", " + shipInfo.getLocation().y + "]");
	}
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
}
// - UNUSED CODE ............................................................................................
