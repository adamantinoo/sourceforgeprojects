//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;
import es.ftgroup.ui.figures.IFigureFactory;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;

import net.sf.vgap4.projecteditor.model.Planet;
import net.sf.vgap4.projecteditor.model.Sector;
import net.sf.vgap4.projecteditor.model.Ship;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4ProjectFigureFactory implements IFigureFactory {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public VGAP4ProjectFigureFactory() {
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - I F I G U R E F A C T O R Y
	public PolylineConnection createConnection(IWireModel newWire) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Return a IFigure depending on the instance of the current model element. This allows this EditPart to be
	 * used for both subclasses of Shape.
	 */
	public Figure createFigure(EditPart part, IGEFModel unit) {
		if (unit instanceof Sector) { return new SectorFigure(); }
		if (unit instanceof Planet) { return new PlanetFigure(); }
		if (unit instanceof Ship) { return new ShipFigure(); }
		if (true) {
			// if Shapes gets extended the conditions above must be updated
			throw new IllegalArgumentException();
		}
		return null;
	}

	public Figure createFigure(EditPart part, IGEFModel unit, String subType) {
		return this.createFigure(part, unit);
	}
}

// - UNUSED CODE ............................................................................................
