//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class MineralsFigure extends Figure {
	private static Logger			logger			= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	public static Color				brown				= new Color(Display.getCurrent(), 204, 102, 0);

	// - F I E L D S
	private MineralBarFigure	neutronium	= new MineralBarFigure(ColorConstants.red, ColorConstants.orange, 1);
	private MineralBarFigure	deuterium		= new MineralBarFigure(ColorConstants.darkBlue, ColorConstants.blue, 1);
	private MineralBarFigure	tritanium		= new MineralBarFigure(ColorConstants.darkGreen, ColorConstants.green, 1);
	private MineralBarFigure	molybdenum	= new MineralBarFigure(brown, ColorConstants.orange, 1);

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public MineralsFigure() {
		GridLayout grid = new GridLayout();
		setLayoutManager(grid);
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.add(neutronium);
		this.add(deuterium);
		this.add(tritanium);
		this.add(molybdenum);
		this.setSize(getPreferredSize());
	}

	// - G E T T E R S / S E T T E R S
	public void setNeutronium(int[] mineralData) {
		this.neutronium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setDuranium(int[] mineralData) {
		this.deuterium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setTritanium(int[] mineralData) {
		this.tritanium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setMolybdenum(int[] mineralData) {
		this.molybdenum.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		Dimension barSize = neutronium.getSize();
		barSize.height = (barSize.height + 1) * 4;
		return barSize;
	}
}

// - CLASS IMPLEMENTATION.................................................................................
class MineralBarFigure extends Figure {
	private static final int	BOX_WIDTH			= 4;
	private static final int	BOX_HEIGHT		= 4;
	private static final int	MAX_BOX_COUNT	= 15;

	private final Color				colorSurface;
	private Color							colorReserve;
	private int								maximun				= MAX_BOX_COUNT;
	private int								surface				= 0;
	private int								reserve				= 0;

	public MineralBarFigure(Color colorSurf, Color colorReserve, int line) {
		this.colorSurface = colorSurf;
		this.colorReserve = colorReserve;
		this.setSize(getPreferredSize());
	}

	public void setLevels(int maximun, int surface, int reserve) {
		this.maximun = Math.max(MAX_BOX_COUNT, maximun);

		// - Interpolate values to graphic size.
		this.surface = surface * MAX_BOX_COUNT / this.maximun;
		this.reserve = reserve * MAX_BOX_COUNT / this.maximun;
		this.repaint();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Point location = getLocation();

		// - Draw the small figure boxes
		Rectangle box = new Rectangle();
		box.width = 3;
		box.height = 3;
		box.y = location.y;
		for (int i = 0; i < MAX_BOX_COUNT; i++) {
			// - Select the color depending on the content of mineral.
			graphics.setForegroundColor(ColorConstants.lightGray);
			graphics.setBackgroundColor(ColorConstants.lightGray);
			if (i <= this.reserve) {
				graphics.setForegroundColor(colorReserve);
				graphics.setBackgroundColor(colorReserve);
			} else {
				if (i <= this.surface) {
					graphics.setForegroundColor(colorSurface);
					graphics.setBackgroundColor(colorSurface);
				}
			}
			box.x = location.x + i * BOX_WIDTH;
			graphics.fillRectangle(box);
		}
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return new Dimension(BOX_WIDTH * (MAX_BOX_COUNT + 1), BOX_WIDTH);
	}
}

// - UNUSED CODE ............................................................................................
