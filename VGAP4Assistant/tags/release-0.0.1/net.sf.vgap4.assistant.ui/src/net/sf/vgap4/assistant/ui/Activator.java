package net.sf.vgap4.assistant.ui;

import java.util.HashMap;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String							PLUGIN_ID	= "net.sf.vgap4.projecteditor";

	// The shared instance
	private static Activator								plugin;
	/** Hash map where I can store and then retrieve global items. */
	private static HashMap<Object, Object>	registry	= new HashMap<Object, Object>();

	/**
	 * The constructor
	 */
	public Activator() {
	}

	// - S T A T I C - S E C T I O N
	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path
	 * 
	 * @param path
	 *          the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * Returns an element in the registry that it is identified by the unique ID. If the element is not found in
	 * the registry then an exception is thrown to be cached by any methods that will interpret this runtime
	 * class of exceptions.
	 */
	public static Object getByID(String id) {
		final Object reference = registry.get(id);
		Assert.isNotNull(reference, "Reference in the registry is not found. This is a runtime error.");
		return reference;
	}

	// - G E T T E R S / S E T T E R S
	public static HashMap<Object, Object> getRegistry() {
		return registry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
}
