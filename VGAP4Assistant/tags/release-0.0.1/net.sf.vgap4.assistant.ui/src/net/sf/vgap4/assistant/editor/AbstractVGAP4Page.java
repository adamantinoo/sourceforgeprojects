//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractVGAP4Page extends GraphicalEditorWithFlyoutPalette {
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.assistant.editor");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public AbstractVGAP4Page() {
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	@Override
	protected CommandStack getCommandStack() {
		return super.getCommandStack();
	}
}

// - UNUSED CODE ............................................................................................
