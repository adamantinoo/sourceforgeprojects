//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
public class Base extends VGAP4Node {
	private static final long				serialVersionUID	= -2240881066097908890L;
	private static Logger						logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant.
	 */
	private Vector<BaseInformation>	baseInformation		= new Vector<BaseInformation>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Gathers the current turn information into the structures in the Base to get all information from all
	 * turns. While getting this data the procedure compiles some aggregated information or updates cached data
	 * that may have changed.
	 * 
	 * @param turn
	 *          number the identified the game turn sequence.
	 * @param baseInfo
	 *          parsed information compiled into an instance of <code>BaseInformation</code>.
	 */
	public void addTurnInformation(int turn, BaseInformation baseInfo) {
		// - Get the old value for this planet turn information is available.
		// If not null then we are overriding this data.
		try {
			//- Increase array size if less than current size.
			if (turn >= this.baseInformation.size()) this.baseInformation.setSize(turn + 1);
			if (null != this.baseInformation.get(turn))
				logger.warning("This operation is overriding some previous information stored for this turn");
			this.baseInformation.set(turn, baseInfo);
			logger.info("Adding base '" + baseInfo.getName() + "' data for turn " + turn + ".");
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			this.baseInformation.setSize(turn + 1);
			this.baseInformation.set(turn, baseInfo);
			aioobe.printStackTrace();
		}
		// - Load cache data. This data is the same on all turns.
		if (-1 == this.getIdNumber()) {
			this.setIdNumber(baseInfo.getIdNumber());
			this.setName(baseInfo.getName());
			//			this.setLocation(baseInfo.getLocation());
			logger.info("Processing base id [" + baseInfo.getIdNumber() + "] - " + baseInfo.getName() + ". At location ["
					+ baseInfo.getLocation().x + ", " + baseInfo.getLocation().y + "]");
		}
		this.setLastTurn(turn);
		// - Process max and statistical data.
		this.setLocation(baseInfo.getLocation());
	}
}

// - UNUSED CODE ............................................................................................
