//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractPropertyChanger;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.BaseInformation;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4Map extends AbstractPropertyChanger {
	private static final long								serialVersionUID				= 3361707471258570866L;
	private static Logger										logger									= Logger
																																			.getLogger("net.sf.vgap4.projecteditor.model");

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	/** Property ID to use when new model data is added to the diagram. */
	public static final String							DATA_ADDED_PROP					= "VGAP4Map.DATA_ADDED_PROP";
	/** Property ID to use when a child is added to this diagram. */
	public static final String							CHILD_ADDED_PROP				= "VGAP4Map.CHILD_ADDED_PROP";
	/** Property ID to use when a child is removed from this diagram. */
	public static final String							CHILD_REMOVED_PROP			= "VGAP4Map.CHILD_REMOVED_PROP";
	/** Property ID to use when a the number referencing any of the last turns gets changed. */
	public static final String							LAST_TURN_CHANGED_PROP	= "VGAP4Map.LAST_TURN_CHANGED_PROP";

	// - M O D E L F I E L D S
	private int															lastReadTurn						= -1;																							// Number of the last turn processed.
	private int															lastAvailableTurn				= -1;																							// Number of the higher turn processed.
	/** Number the identifies a unique game code. */
	private String													gameId;
	private String													gameName;																																	// Name given to this game when created.
	/**
	 * Player number that matches the slot number on the game. This number may correspond to another user inside
	 * other games so it is used with the 'gameId' to uniquely identify a player slot on a game set.
	 */
	private int															playerNo								= -1;
	/**
	 * Sectors are virtual structures that keep lists of grouped elements (generally by geographic location).
	 * This field stores the references to all the sectors currently defined on this Map. This is a simple list
	 * structure and currently sectors donot have any sorting order or organization. A sector may contain other
	 * sectors to any level because it simply defines a physical rectangular map area.
	 */
	private final Vector<Sector>						sectors									= new Vector<Sector>();
	/**
	 * Array structure with references to all the Planets that have been identified on this turn or on other
	 * turns. This structure is implemented as an array because it matches the original implementation from CSV
	 * files. Probably a 'Hashtable' will generate less memory usage, over all in very long games where the Id
	 * of some elements may grew high. Planets are the exception because there is a finite number of them and
	 * alwais get the lowest element Id.
	 */
	private final Vector<Planet>						planets									= new Vector<Planet>();
	/**
	 * This structure contains a reference to all player bases. This structure clearly benefits from the
	 * 'Hashtable' implementation because the number of bases for this player is small in relation to the number
	 * of planets and the number of other elements created in the game.
	 */
	private final Hashtable<Integer, Base>	bases										= new Hashtable<Integer, Base>();
	/**
	 * Another array structure that contains all the identified Ships. This list contains all ships owned by
	 * this player ans also the ships detected to other players. That other ships are identified by being the
	 * 'owner' a different code than the game 'playerId'.
	 */
	private final Vector<Ship>							ships										= new Vector<Ship>();
	{
		// - Add some sectors manually to all maps.
		sectors.add(new Sector("SECTOR - Homeworld", 2100, 2600, 300, 200));
		sectors.add(new Sector("SECTOR - Brand", 2300, 2900, 300, 200));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	/**
	 * Return a List of all the visible elements in the diagram. Visible elements are the Planets, the Bases and
	 * the Ships. The returned List should not be modified.
	 */
	public Vector<Object> getChildren() {
		Vector<Object> childs = new Vector<Object>();
		// - Add only objects that are not null
		Iterator<Sector> itsec = this.sectors.iterator();
		while (itsec.hasNext()) {
			Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}
		Iterator<Planet> itp = this.planets.iterator();
		while (itp.hasNext()) {
			Planet planet = itp.next();
			if (null != planet) childs.add(planet);
		}
		Iterator<Ship> its = this.ships.iterator();
		while (its.hasNext()) {
			Ship ship = its.next();
			if (null != ship) childs.add(ship);
		}
		return childs;
	}

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public int getPlayer() {
		return playerNo;
	}

	public void setPlayer(int playerNo) {
		this.playerNo = playerNo;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public int getReadTurn() {
		return lastReadTurn;
	}

	public void setReadTurn(int readTurn) {
		this.lastReadTurn = readTurn;
		lastAvailableTurn = Math.max(lastAvailableTurn, lastReadTurn);
	}

	// - P U B L I C - S E C T I O N
	public boolean isInitialized() {
		if ((-1 == this.lastReadTurn) && (null == gameId))
			return false;
		else
			return true;
	}

	/**
	 * Adds the processed planet information to the model. The planet information identifies the game, the
	 * player and the turn. If they do not match the same information on the model this instance is discarded
	 * and this actions registered with a warning message.<br>
	 * If this information is the first to be added to the model, then the model gets the player and name from
	 * this single instance and successive additions must match on that information.
	 * 
	 * @param turn
	 *          is the number of the turn data we are loading.
	 */
	/*
	 * The diagram structure is such that it has elements to contain all the information to be displayed. The
	 * first aggregation level is the Planet information. There is an slot for any single Planet, but that slot
	 * contains multitude of information, from the different turns and from the game itself. The main contents
	 * of this document is to record the Game Id and the user that is using this document to avoid reading
	 * others player data into the presentation model. The game and player identification area set when the file
	 * is read and initialized when the document is created and never more changed.
	 */
	/**
	 * Add the new turn information to the Planet structure. We have not to check that the owner and game
	 * matches because that test have been performed before arriving this point. But some check has to be
	 * performed to identify when we have not locate the correct Planet. The variable 'thePlanet' may not be
	 * allowed to be NULL because we have a reference on it.
	 */
	public void addPlanetInformation(int turn, PlanetInformation planetInfo) {
		// - Get the Planet structure that matched to the Planet ID.
		Planet thePlanet = getPlanet4Id(planetInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		thePlanet.addTurnInformation(turn, planetInfo);
	}

	public void addShipInformation(int turn, ShipInformation shipInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		Ship theShip = getShip4Id(shipInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theShip.addTurnInformation(turn, shipInfo);
	}

	/**
	 * Add a Planet to this model.
	 * 
	 * @param newPlanet
	 *          a non-null Planet instance
	 */
	protected void addPlanet(Planet newPlanet) {
		if (null != newPlanet) {
			planets.add(newPlanet);
			firePropertyChange(CHILD_ADDED_PROP, null, newPlanet);
		}
	}

	protected void addShip(Ship newShip) {
		if (null != newShip) {
			ships.add(newShip);
			firePropertyChange(CHILD_ADDED_PROP, null, newShip);
		}
	}

	/**
	 * Adds a new element to the diagram model for presentation. There are few types supported and this methods
	 * is in charge of the type detection to incorporate the unit to the right list.
	 * 
	 * @param newUnit
	 */
	public void addChild(PositionableUnit newUnit) {
		// - Check the real type of object to be set on the right vector.
		if (newUnit instanceof Planet) {
			this.addPlanet((Planet) newUnit);
			// - Update the 'dirty' flag to signal that this file need to be saved.
			return;
		}
		if (newUnit instanceof Ship) {
			this.addShip((Ship) newUnit);
			// - Update the 'dirty' flag to signal that this file need to be saved.
			return;
		}
	}

	/**
	 * Remove a Planet from the model. This will happen when the user overrides the interface and chooses to
	 * remove some model and history data.
	 * 
	 * @param obsoletePlanet
	 *          a non-null Planet instance;
	 */
	// public void removeChild(Planet obsoletePlanet) {
	// if (null != obsoletePlanet) {
	// planets.remove(obsoletePlanet);
	// firePropertyChange(CHILD_REMOVED_PROP, null, obsoletePlanet);
	// }
	// }
	//
	// public void removeChild(Ship obsoleteShip) {
	// if (null != obsoleteShip) {
	// ships.remove(obsoleteShip);
	// firePropertyChange(CHILD_REMOVED_PROP, null, obsoleteShip);
	// }
	// }
	public void removeChild(PositionableUnit obsoleteUnit) {
		if (null != obsoleteUnit) {
			planets.remove(obsoleteUnit);
			ships.remove(obsoleteUnit);
			firePropertyChange(CHILD_REMOVED_PROP, null, obsoleteUnit);
		}
	}

	/**
	 * Check for the array size and the array contents. The array starts empty so any try to access any null
	 * element will trigger an exception. INtercept the exceptions and resize the array adecuately to the
	 * current usage.
	 */
	private Planet getPlanet4Id(int id) {
		// - Get the element we are searching for. If exception then resize the array and return a new structure.
		try {
			Planet thePlanet = this.planets.get(id);
			if (null == thePlanet) {
				// - The planet is not detected because it is new data.
				this.planets.setSize(id + 1);
				this.planets.setElementAt(new Planet(), id);
				return this.planets.get(id);
			} else
				return thePlanet;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			// - The planet has not any previous data. Resize the vector to the proper size and create a new one.
			this.planets.setSize(id + 1);
			this.planets.setElementAt(new Planet(), id);
			return this.planets.get(id);
		}
	}

	private Ship getShip4Id(int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		try {
			Ship theShip = this.ships.get(id);
			if (null == theShip) {
				//- The ship is not located because this is a new element.
				this.ships.setSize(id + 1);
				this.ships.setElementAt(new Ship(), id);
				return this.ships.get(id);
			} else
				return theShip;
		} catch (ArrayIndexOutOfBoundsException aioobe) {
			// - The ship array has not any previous data. Resize the vector to the proper size.
			this.ships.setSize(id + 1);
			this.ships.setElementAt(new Ship(), id);
			return this.ships.get(id);
		}
	}

	private Base getBase4Id(int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		try {
			Base theBase = this.bases.get(new Integer(id));
			if (null == theBase) {
				//- The base is not located because this is a new element.
				//				this.bases.setSize(id + 1);
				this.bases.put(new Integer(id), new Base());
				return this.bases.get(new Integer(id));
			} else
				return theBase;
		} catch (Exception aioobe) {
			aioobe.printStackTrace();
			this.bases.put(new Integer(id), new Base());
			return this.bases.get(new Integer(id));
			//					// - The base array has not any previous data. Resize the vector to the proper size.
			//					this.bases.setSize(id + 1);
			//					this.bases.setElementAt(new Base(), id);
			//					return this.bases.get(id);
		}
	}

	public void addBaseInformation(int turn, BaseInformation baseInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		Base theBase = getBase4Id(baseInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theBase.addTurnInformation(turn, baseInfo);
	}
}

// - UNUSED CODE ............................................................................................
