//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class MineralsGraphic extends Figure {
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public MineralsGraphic() {
	// }
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// }

	// class MineralsFigure extends Figure {
	private static final int	MINERAL_MAX_WIDTH	= 40 + 1;
	private static final int	MINERAL_HEIGHT		= 4;
	private static Color			darkBrown					= new Color(Display.getCurrent(), 153, 102, 0);
	private static Color			brown							= new Color(Display.getCurrent(), 204, 153, 0);

	private MineralDraw				neutronium				= new MineralDraw(ColorConstants.red, ColorConstants.orange);
	private MineralDraw				duranium					= new MineralDraw(ColorConstants.darkBlue, ColorConstants.blue);
	private MineralDraw				tritanium					= new MineralDraw(ColorConstants.darkGreen, ColorConstants.green);
	private MineralDraw				molybdenum				= new MineralDraw(darkBrown, brown);

	public MineralsGraphic() {
		GridLayout grid = new GridLayout();
		setLayoutManager(grid);
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.add(neutronium);
		this.add(duranium);
		this.add(tritanium);
		this.add(molybdenum);
		this.setSize(getPreferredSize());
	}

	public void setNeutronium(int ore, int reserve) {
		this.neutronium.setLevels(ore, reserve);
		this.repaint();
	}

	public void setDuranium(int ore, int reserve) {
		this.duranium.setLevels(ore, reserve);
		this.repaint();
	}

	public void setTritanium(int ore, int reserve) {
		this.tritanium.setLevels(ore, reserve);
		this.repaint();
	}

	public void setMolybdenum(int ore, int reserve) {
		this.molybdenum.setLevels(ore, reserve);
		this.repaint();
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return new Dimension(MINERAL_MAX_WIDTH, MINERAL_HEIGHT * 4);
	}
}

class MineralDraw extends Figure {
	private static final int	BOX_WIDTH			= 4;
	private static final int	BOX_HEIGHT		= 4;
	private static final int	BOXES_IN_LINE	= 10;

	private Color							colorOre			= ColorConstants.gray;
	private Color							colorReserve	= ColorConstants.gray;
	private int								reserve				= 10;
	private int								ore						= 1;

	public MineralDraw(Color colorOre, Color colorReserve) {
		this.colorOre = colorOre;
		this.colorReserve = colorReserve;
		this.setSize(getPreferredSize());
	}

	public void setLevels(int ore, int reserve) {
		this.ore = ore;
		this.reserve = reserve;
		this.repaint();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		Point location = getLocation();

		// - Draw the small figure boxes
		Rectangle box = new Rectangle();
		box.y = location.y;
		box.width = 3;
		box.height = 3;
		graphics.setForegroundColor(ColorConstants.gray);
		graphics.setBackgroundColor(ColorConstants.gray);
		for (int i = 0; i < 10; i++) {
			box.x = location.x + i * BOX_WIDTH;
			graphics.fillRectangle(box);
		}
		graphics.setForegroundColor(colorOre);
		graphics.setBackgroundColor(colorOre);
		for (int i = 0; i < this.ore; i++) {
			box.x = location.x + i * BOX_WIDTH;
			graphics.fillRectangle(box);
		}
		graphics.setForegroundColor(colorReserve);
		graphics.setBackgroundColor(colorReserve);
		for (int i = this.ore; i < this.reserve; i++) {
			box.x = location.x + i * BOX_WIDTH;
			graphics.fillRectangle(box);
		}
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return new Dimension(1 + BOXES_IN_LINE * BOX_WIDTH + 1, BOX_HEIGHT);
	}
}

// - UNUSED CODE ............................................................................................
