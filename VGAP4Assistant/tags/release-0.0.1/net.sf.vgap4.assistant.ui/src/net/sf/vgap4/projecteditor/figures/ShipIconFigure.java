//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipIconFigure extends Figure {
	private static Logger	logger			= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	protected ShipFigure	container;
	protected int					drawingSize	= 7;
	/** Drawing color for the unit. Depends on the game side. */
	private final Color		color				= new Color(Display.getCurrent(), 50, 50, 50);

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public ShipIconFigure(ShipFigure parentFigure) {
		container = parentFigure;
		this.setSize(this.getPreferredSize());
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E

	public Color getColor() {
		return color;
	}

	protected ShipFigure getContainer() {
		return container;
	}

	protected boolean isSelected() {
		return container.isSelected();
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		// bound.width -= 1;
		// bound.height -= 1;

		// - Draw the figure body
		graphics.setForegroundColor(ColorConstants.darkGray);
		graphics.setBackgroundColor(ColorConstants.darkGray);
		// graphics.setLineWidth(2);
		graphics.fillRectangle(bound);
		// graphics.setLineWidth(1);
		// if (isSelected()) drawHandles(graphics);

		// // - Draw the figure center
		// bound = getBounds().getCopy();
		// final Point hotspot = getHotSpot();
		// bound.x += hotspot.x + 1;
		// bound.y += hotspot.y + 1;
		// final Point endPoint = new Point(bound.x + 1, bound.y + 1);
		// graphics.setLineWidth(1);
		// graphics.setForegroundColor(getColor());
		// graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}

	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		return new Dimension(drawingSize, drawingSize);
	}

	public Point getHotSpot() {
		return new Point(drawingSize / 2, drawingSize / 2);

	}

	protected void setDrawingSize(final int figureSize) {
		drawingSize = figureSize;
		this.setSize(figureSize, figureSize);
	}
}

// - UNUSED CODE ............................................................................................
