//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipInformation implements Serializable {
	private static final long	serialVersionUID	= 3906934058195215773L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - M O D E L F I E L D - I D E N T I F I E R S

	// - M O D E L F I E L D S
	// /**
	// * Is the turn number initialized form the generic CSV identification file and set up when this instance
	// is
	// * created. Used to check that this information is not already present on the model and to identify the
	// * destination slot.
	// */
	// private int turn = -1;
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	private int								idNumber					= -1;
	/**
	 * Stores the name given to this ship. This name is user configurable and may change from turn to turn so
	 * this information shoud not be cached.
	 */
	private String						name;
	/** Stores the coordinates to the ship location inside the diagram coordinates. */
	private Point							location;

	private String						description;
	private int								owner;
	private int								fuel;
	private int								ord;
	private int								supplies;
	private int								repair;
	private int								cash;
	private int								troops;
	private int								colonists;
	private int								guard;
	private int								duranium;
	private int								tritanium;
	private int								molybdenum;
	private int								food;
	private int								med;
	private int								speed;

	// /**
	// * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	// * insertion validation (this data cannot be inserted inside another planet).
	// */
	// private String ID;
	// /** Stores the planet name as identified in the source data. */
	// private String name;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	public int getIdNumber() {
		return this.idNumber;
	}

	public String getName() {
		return name;
	}

	public Point getLocation() {
		if (null == this.location)
			return new Point(0, 0);
		else
			return location;
	}

	// - P U B L I C - S E C T I O N
	public void loadData(String[] shipFields) {
		try {
			// - Store the data fields in the instance attributes.
			this.idNumber = convert2Integer(shipFields[1].trim());
			name = shipFields[2].trim();
			int x = convert2Integer(shipFields[129].trim());
			int y = convert2Integer(shipFields[130].trim());
			this.location = new Point(x, y);

			description = shipFields[3].trim();
			owner = convert2Integer(shipFields[6].trim());
			// TODO Convert all this attributes into a Cargo class
			fuel = convert2Integer(shipFields[115].trim());
			ord = convert2Integer(shipFields[116].trim());
			supplies = convert2Integer(shipFields[117].trim());
			repair = convert2Integer(shipFields[118].trim());
			cash = convert2Integer(shipFields[119].trim());
			troops = convert2Integer(shipFields[120].trim());
			colonists = convert2Integer(shipFields[121].trim());
			guard = convert2Integer(shipFields[122].trim());
			duranium = convert2Integer(shipFields[123].trim());
			tritanium = convert2Integer(shipFields[124].trim());
			molybdenum = convert2Integer(shipFields[125].trim());
			food = convert2Integer(shipFields[126].trim());
			med = convert2Integer(shipFields[127].trim());
			speed = convert2Integer(shipFields[128].trim());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - P R O T E C T E D - S E C T I O N
	protected int convert2Integer(String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
}

// - UNUSED CODE ............................................................................................
