//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractEndPoint;

import org.eclipse.draw2d.geometry.Point;

import net.sf.gef.core.model.Route;

// - CLASS IMPLEMENTATION ...................................................................................
public class PositionableUnit extends AbstractEndPoint {
	private static final long		serialVersionUID	= -9007441570957929333L;
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	NAME_PROP					= "PositionableUnit.NAME_PROP";
	public static final String	PROP_LOCATION			= "PositionableUnit.PROP_LOCATION";

	// - M O D E L F I E L D S
	/**
	 * This is the location of the unit in the diagram. Contains the x,y coordinates to locate the unit in the
	 * diagram coordinate system.
	 */
	private Point								location;
	private Vector<Route>				sourceConnections	= new Vector<Route>();
	private Vector<Route>				targetConnections	= new Vector<Route>();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public PositionableUnit() {
	}

	// - G E T T E R S / S E T T E R S
	public Point getLocation() {
		if (null == this.location)
			return new Point(0, 0);
		else
			return location;
	}

	public void setLocation(Point location) {
		if (null != location)
			this.location = location;
		else
			logger.warning("Trying to set a NULL into the " + this.toString() + " location attribute.");
	}

	// protected int getX() {
	// return x;
	// }
	//
	// protected void setX(int x) {
	// this.x = x;
	// }
	//
	// protected int getY() {
	// return y;
	// }
	//
	// protected void setY(int y) {
	// this.y = y;
	// }
	//
	// public Point getLocation() {
	// return new Point(this.x, this.y);
	// }
	//
	// public Point getCoordinates() {
	// return new Point(this.x, this.y);
	// }

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// protected int convert2Integer(String fieldValue) {
	// return Integer.parseInt(fieldValue.trim());
	// }

	// - O V E R R I D E - S E C T I O N
	// - W I R E E N D P O I N T - S E C T I O N
	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	@Override
	public void addConnection(final Route wire) {
		if ((wire == null) || (wire.getSource() == wire.getTarget()))
			throw new IllegalArgumentException("Connections cannot be null or closed.");
		if (wire.getSource() == this) {
			sourceConnections.add(wire);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, null, wire);
		} else if (wire.getTarget() == this) {
			targetConnections.add(wire);
			firePropertyChange(TARGET_CONNECTIONS_PROP, null, wire);
		}
	}

	/**
	 * Return the list of outgoing connections that apply to this point.
	 * 
	 * @see net.sourceforge.harpoon.model.units.WireEndPoint#getSourceConnections()
	 */
	@Override
	public List<Route> getSourceConnections() {
		return sourceConnections;
	}

	/**
	 * Returns the list of incoming connections that apply to this point.
	 * 
	 * @see net.sourceforge.harpoon.model.units.WireEndPoint#getTargetConnections()
	 */
	@Override
	public List<Route> getTargetConnections() {
		return targetConnections;
	}

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	@Override
	public void removeConnection(final Route conn) {
		if (conn == null) throw new IllegalArgumentException("Connections cannot be null");
		if (conn.getSource() == this) {
			sourceConnections.remove(conn);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, conn, null);
		} else if (conn.getTarget() == this) {
			targetConnections.remove(conn);
			firePropertyChange(TARGET_CONNECTIONS_PROP, conn, null);
		}
	}

	@Override
	public void setSource(Route wire) {
		this.sourceConnections = new Vector<Route>(1, 1);
		this.sourceConnections.add(wire);
	}

	@Override
	public void setTarget(Route wire) {
		this.targetConnections = new Vector<Route>(1, 1);
		this.targetConnections.add(wire);
	}

}
// - UNUSED CODE ............................................................................................
