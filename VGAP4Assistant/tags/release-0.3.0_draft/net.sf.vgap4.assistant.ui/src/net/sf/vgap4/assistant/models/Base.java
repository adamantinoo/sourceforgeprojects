//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.ui.Activator;

// - CLASS IMPLEMENTATION ...................................................................................
public class Base extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long													serialVersionUID		= -2240881066097908890L;
	private static Logger															logger							= Logger
																																						.getLogger("net.sf.vgap4.assistant.models");
	private static final int													EMPTY_TRIGGER_LEVEL	= 300;
	private static final int													MAX_MINERAL					= 0;
	private static final int													TOTALORE_MINERAL		= 1;
	private static final int													SURFACE_MINERAL			= 2;

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private AssistantMap															map									= null;
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant. Is the user responsibility to add as many turns as available to have all
	 * the data needed to create graphics or take decisions.
	 */
	//	private Vector<BaseInformation>	baseInformation			= new Vector<BaseInformation>();
	private final Hashtable<Integer, BaseInformation>	baseInformation			= new Hashtable<Integer, BaseInformation>();
	//	private transient final BaseInformation						latestTurnInfo			= null;
	//	private BaseInformation														latestTurn					= null;
	/**
	 * Reference to the Planet where this base is located. This may change from turn to turn so this is cached
	 * data that should be accessed though methods to guarantee that all times we get the right reference.
	 */
	//	private Planet																		onPlanet						= null;
	/**
	 * Max ore detected for this planet. This is used to interpolate the drawing extents for the minerals
	 * detected on the planet. It keeps the max values reached by the sum of all the mineral (reserve, ore and
	 * in the surface) found when a new turn is added.
	 */
	private final Vector<Integer>											maxMinerals					= new Vector<Integer>(1);
	{
		maxMinerals.setSize(4);
		maxMinerals.set(MiningInformation.NEUTRONIUM_ID, -1);
		maxMinerals.set(MiningInformation.DURANIUM_ID, -1);
		maxMinerals.set(MiningInformation.TRITANIUM_ID, -1);
		maxMinerals.set(MiningInformation.MOLYBDENUM_ID, -1);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Base(final AssistantMap ownerMap) {
		map = ownerMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Gathers the current turn information into the structures in the Base to get all information from all
	 * turns. While getting this data the procedure compiles some aggregated information or updates cached data
	 * that may have changed.
	 * 
	 * @param turn
	 *          number the identified the game turn sequence.
	 * @param baseInfo
	 *          parsed information compiled into an instance of <code>BaseInformation</code>.
	 */
	public void addTurnInformation(final int turn, final BaseInformation baseInfo) {
		// - Get the old value for this base turn information if available.
		final BaseInformation oldData = baseInformation.get(new Integer(turn));
		if (null != oldData)
			Base.logger.warning("This operation is overriding some previous information stored for this turn");
		baseInformation.put(new Integer(turn), baseInfo);
		Base.logger.info("Adding turn " + turn + " to base '" + baseInfo.getName() + "'");

		//- Special check for the first time we load a turn.
		if (-1 == this.getIdNumber()) {
			this.setIdNumber(baseInfo.getIdNumber());
			this.setName(baseInfo.getName());
		}

		//- Reference the latest turn.
		updateTurnReference(turn);
		this.setOwner(getLatestInfo().getFieldNumber("Owner"));
		//		//- Load the latest turn into the cached reference.
		//		latestTurn = baseInformation.get(new Integer(latestTurnNo));

		//		//- Get the Planet reference.
		//		onPlanet = map.getPlanet4Id(latestTurn.getPlanetId());
		//		onPlanet.setBase(this);
		//		this.setLastTurn(turn);

		// - Process max and statistical data.
		this.setLocation(baseInfo.getLocation());
		Base.logger.info("Processing base id [" + baseInfo.getIdNumber() + "] - " + baseInfo.getName() + ". At location ["
				+ baseInfo.getLocation().x + ", " + baseInfo.getLocation().y + "]");
	}

	/**
	 * Color should be calculated depending on several model data, like the number of resources or the existence
	 * of natives. Initial rules may look similar to the rules used on the Excel game sheet.
	 * <ul>
	 * <li>Bases with orders pending should draw in orange</li>
	 * <li>Bases with without orders pending may select the color from the next list:
	 * <ul>
	 * <li> With the right levels of resources and colonists in light green.</li>
	 * <li>With excess resources to be exported in dark green.</li>
	 * <li>Going to be dismantled or with the underlying planet empty of minerals in dark blue color.</li>
	 * <li>With no update on the last turn in light gray color.</li>
	 * <li>With no determinate information or enough info to take a decision then render a dark gray color.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * 
	 * @return the preferred color to render for this icon on the figure.
	 */
	public Color getColor() {
		//TODO Calculate the color depending on the rules
		Color setColor = Activator.COLOR_DEFAULTBASE;
		//TODO Check for no update on last turn
		//- Check for empty Planet
		//		final int planetMin = this.getOnPlanet().getMineralTotal();
		//		if (planetMin < Base.EMPTY_TRIGGER_LEVEL) setColor = Activator.COLOR_NOMINERALSPLANET;
		return setColor;
	}

	public int[] getDisplayableMineralData(final int mineralId) {
		final int[] data = new int[3];
		data[Base.MAX_MINERAL] = maxMinerals.get(mineralId);
		// - Get the information form the last turn data read for this planet.
		//		final BaseInformation info = baseInformation.get(this.getLastTurn());
		data[Base.TOTALORE_MINERAL] = getLatestInfo().getMiningInformation().get(mineralId).getTotalOre();
		data[Base.SURFACE_MINERAL] = getLatestInfo().getMiningInformation().get(mineralId).getSurfaceMineral();
		return data;
	}

	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	public int getMineralTotal() {
		int total = 0;
		final Iterator<Integer> mit = maxMinerals.iterator();
		while (mit.hasNext()) {
			final int minQty = mit.next().intValue();
			if (minQty > 0) total += minQty;
		}
		return total;
	}

	public String getProperty(final String property) {
		//- Get the information from the latest turn info.
		return this.getLatestInfo().getField(property);
	}

	public int getTotalNatives() {
		//		final BaseInformation bInfo = baseInformation.get(this.getLastTurn());
		final int natives = getLatestInfo().getNativesInfo().getTotalNatives();
		return natives;
	}

	/**
	 * This method return the turn information for the highest turn number that is recorded for this object.
	 * This has not to be the same for every object in the model because some objects may be updated in a new
	 * turn while others not. The relative obsolescence for this information depends on the values of the
	 * <code>lastAvailableTurn</code> that is recorded on the <code>AssistantMap</code>.
	 */
	protected BaseInformation getLatestInfo() {
		return getTurnInfo(this.getLatestTurnNumber());
	}

	/** Locates recursively the latest declared turn information for this model element, whatever it is */
	private BaseInformation getTurnInfo(int turn) {
		BaseInformation info = baseInformation.get(turn);
		if (null == info)
			return getTurnInfo(turn - 1);
		else
			return info;
	}

	/**
	 * This method return true is object turn number matches the <code>AssistantMap.lastAvailableTurn</code>
	 * turn number.
	 */
	public boolean isObsolete(int mapTurn) {
		if (mapTurn == this.getLatestTurnNumber())
			return false;
		else
			return true;
	}

	public int getPlanetId() {
		return getLatestInfo().getPlanetId();
	}

	//	private Planet getOnPlanet() {
	//		return onPlanet;
	//	}
}

// - UNUSED CODE ............................................................................................
