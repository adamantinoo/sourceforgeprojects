//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.parts.AbstractEditPartFactory;
import es.ftgroup.gef.parts.AbstractGenericEditPart;
import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Spot;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AssistantEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Maps the model structure for this project to the EditPart. It returns a generic
	 * <code>AbstractGenericEditPart</code> that is commonplace for all developments and has some factory
	 * features that allow better parameterization of figure factories.
	 * 
	 * @param modelElement
	 *          the model piece that requests the creation of a new EditPart
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof AssistantMap) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ "VGAP4ProjectDiagram" + "]");
			return new DiagramEditPart();
		}
		//		if (modelElement instanceof Sector) {
		//			logger.info("Generating SectorEditPart for model class " + modelElement.getClass().getSimpleName()
		//					+ " with name [" + ((AssistantNode) modelElement).getName() + "]");
		//			return new SectorEditPart();
		//		}
		if (modelElement instanceof Spot) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((AssistantNode) modelElement).getName() + "]");
			return new SpotEditPart();
		}
		//		if (modelElement instanceof Planet) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
		//					+ ((AssistantNode) modelElement).getName() + "]");
		//			return new PlanetEditPart();
		//		}
		//		if (modelElement instanceof Base) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
		//					+ ((AssistantNode) modelElement).getName() + "]");
		//			return new BaseEditPart();
		//		}
		//		if (modelElement instanceof Ship) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
		//					+ ((AssistantNode) modelElement).getName() + "]");
		//			return new ShipEditPart();
		//		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}
}
// - UNUSED CODE ............................................................................................
