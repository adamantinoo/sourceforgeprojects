//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.policies;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.CreateRequest;

import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.projecteditor.model.PositionableUnit;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * EditPolicy for the Figure used by this edit part. Children of XYLayoutEditPolicy can be used in Figures
 * with XYLayout.
 * 
 * @author Elias Volanakis
 */
public class VGAP4XYLayoutEditPolicy extends XYLayoutEditPolicy {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.policies");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public VGAP4XYLayoutEditPolicy() {
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	@Override
	protected Command getCreateCommand(CreateRequest request) {
		Object childClass = request.getNewObjectType();
		if (childClass == Planet.class || childClass == Ship.class) {
			// return a command that can add a Shape to a ShapesDiagram
			return new UnitCreateCommand((PositionableUnit) request.getNewObject(), (AssistantMap) getHost().getModel(), request
					.getLocation());
		}
		return null;
	}

	// - O V E R R I D E - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(ChangeBoundsRequest, EditPart, Object)
	 */
	// @Override
	// protected Command createChangeConstraintCommand(ChangeBoundsRequest request, EditPart child, Object
	// constraint) {
	// if (child instanceof PlanetEditPart && constraint instanceof Rectangle) {
	// // return a command that can move and/or resize a Shape
	// return new ShapeSetConstraintCommand((Shape) child.getModel(), request, (Rectangle) constraint);
	// }
	// return super.createChangeConstraintCommand(request, child, constraint);
	// }
	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(EditPart, Object)
	 */
	@Override
	protected Command createChangeConstraintCommand(EditPart child, Object constraint) {
		// not used in this example
		return null;
	}
	// - I N T E R F A C E - N A M E
}

class UnitCreateCommand extends Command {
	/** The new shape. */
	private final PositionableUnit	newShape;
	/** ShapeDiagram to add to. */
	private final AssistantMap					parent;
	/** The bounds of the new Shape. */
	private final Point							location;

	/**
	 * Create a command that will add a new Shape to a ShapesDiagram.
	 * 
	 * @param newShape
	 *          the new Shape that is to be added
	 * @param parent
	 *          the ShapesDiagram that will hold the new element
	 * @param location
	 *          the bounds of the new shape; the size can be (-1, -1) if not known
	 * @throws IllegalArgumentException
	 *           if any parameter is null, or the request does not provide a new Shape instance
	 */
	public UnitCreateCommand(PositionableUnit newShape, AssistantMap parent, Point location) {
		this.newShape = newShape;
		this.parent = parent;
		this.location = location;
		setLabel("Adding Diagram Unit");
	}

	/**
	 * Can execute if all the necessary information has been provided.
	 * 
	 * @see org.eclipse.gef.commands.Command#canExecute()
	 */
	@Override
	public boolean canExecute() {
		return newShape != null && parent != null && location != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	@Override
	public void execute() {
		newShape.setLocation(location);
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	@Override
	public void redo() {
		//		parent.addChild(newShape);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	@Override
	public void undo() {
		//		parent.removeChild(newShape);
	}

}
// - UNUSED CODE ............................................................................................
