//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Dimension;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class MineralsFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger			= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	// - F I E L D - S E C T I O N ............................................................................
	private final MineralBarFigure	neutronium	= new MineralBarFigure(AssistantConstants.COLOR_STANDARD_RED,
																									ColorConstants.orange);
	private final MineralBarFigure	deuterium		= new MineralBarFigure(ColorConstants.darkBlue, ColorConstants.blue);
	private final MineralBarFigure	tritanium		= new MineralBarFigure(ColorConstants.darkGreen, ColorConstants.green);
	private final MineralBarFigure	molybdenum	= new MineralBarFigure(AssistantConstants.COLOR_BROWN,
																									ColorConstants.orange);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MineralsFigure() {
		final GridLayout grid = new GridLayout();
		this.setLayoutManager(grid);
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.add(neutronium);
		this.add(deuterium);
		this.add(tritanium);
		this.add(molybdenum);
		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		final Dimension barSize = neutronium.getSize();
		barSize.height = (barSize.height + 1) * 4;
		return barSize;
	}

	public void setDuranium(final int[] mineralData) {
		deuterium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setMolybdenum(final int[] mineralData) {
		molybdenum.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setNeutronium(final int[] mineralData) {
		neutronium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}

	public void setTritanium(final int[] mineralData) {
		tritanium.setLevels(mineralData[0], mineralData[1], mineralData[2]);
	}
}
// - UNUSED CODE ............................................................................................
