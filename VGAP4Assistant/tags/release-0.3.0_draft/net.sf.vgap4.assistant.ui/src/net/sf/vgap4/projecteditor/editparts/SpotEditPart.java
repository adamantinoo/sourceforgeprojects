//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.factories.IconImageFactory;
import net.sf.vgap4.assistant.figures.SpotFigure;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.pages.BasePropertyPage;
import net.sf.vgap4.assistant.pages.PlanetPropertyPage;
import net.sf.vgap4.assistant.pages.ShipPropertyPage;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotEditPart extends AssistantNodeEditPart implements ISelectablePart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Instead creating a simple PropertyPage this method should create one page for each element in this
	 * location.
	 */
	public IPropertyPage createPropertyPage(Composite top, boolean singleSelected) {
		Spot spot = this.getCastedModel();
		Vector<AssistantNode> contents = spot.getContents();
		Iterator<AssistantNode> cit = spot.getContents().iterator();
		try {
			while (cit.hasNext()) {
				AssistantNode element = cit.next();
				if (element instanceof Base) {
					final BasePropertyPage page = new BasePropertyPage(top);
					page.setModel((Base) element);
				}
				if (element instanceof Planet) {
					final PlanetPropertyPage page = new PlanetPropertyPage(top);
					page.setModel((Planet) element);
				}
				if (element instanceof Ship) {
					final ShipPropertyPage page = new ShipPropertyPage(top);
					page.setModel((Ship) element);
				}
			}
			//			return page;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		int player = getPlayerCode();

		//- Get the representative model data.
		AssistantNode representative = model.getRepresentative();
		int owner = representative.getOwner();

		//- Create the representing image depending on representative.
		IconImageFactory imageFactory = new IconImageFactory();
		imageFactory.clearActivates();
		if (representative instanceof Base) {
			imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
			if (player == owner)
				imageFactory.activateBase(false);
			else
				imageFactory.activateBase(true);

			//- Show classification from the underlying planet
			Planet planet = this.getOnPlanet(((Base) representative).getPlanetId());
			imageFactory.setPlanetClassification(planet.getPlanetClassification());
		}
		if (representative instanceof Planet) {
			imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
			imageFactory.setInfoLevel(model.infoAvailable());
		}
		if (representative instanceof Ship) {
			imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
			if (player == owner)
				imageFactory.setEnemy(false);
			else
				imageFactory.setEnemy(true);
		}
		//		if (representative instanceof Pod) {
		//			imageFactory.setShape(IconImageFactory.SHAPE_POD);
		//			if (player == owner)
		//				imageFactory.setEnemy(false);
		//			else
		//				imageFactory.setEnemy(true);
		//		}

		//- If this is a multiple spot process the contents to generate decorators.
		if (model.isMultiple()) {
			if (model.hasShip) imageFactory.activateShip(false);
			if (model.hasPods) imageFactory.activatePods(false);
			if (model.hasWings) imageFactory.activateWings(false);
		}
		fig.setIconImage(imageFactory.generateImage());
		fig.setToolTip(getToolTip(model));
		//			fig.setBaseFigure(SpotIconFigure.SHAPE_PLANET);
		//			fig.activateBase();
		//		if (false) {
		//			if (model.hasShip) imageFactory.activateShip(false);
		//			if (model.hasPods) imageFactory.activatePods(false);
		//			if (model.hasWings) imageFactory.activateWings(false);
		//		}
		//		if (false) {
		//			fig.setBaseFigure(SpotIconFigure.SHAPE_PLANET);
		//			if (model.hasShip) fig.activateShip();
		//			if (model.hasPods) fig.activatePods();
		//			if (model.hasWings) fig.activateWings();
		//			if (model.hasNatives()) fig.activateNatives();
		//		}
		//		if (representative instanceof Ship) {
		//			if (player == owner)
		//				fig.setMainColor(AssistantConstants.COLOR_SHIP_DEFAULT);
		//			else
		//				fig.setMainColor(AssistantConstants.COLOR_SHIP_ENEMY);
		//			fig.setBaseFigure(SpotIconFigure.SHAPE_SHIP);
		//			if (model.hasPods) fig.activatePods();
		//			if (model.hasWings) fig.activateWings();
		//		}

		//- Activate decorators depending on the objects located at this spot point.
		//		if (model.hasPlanet) {
		//			//- Check if there are natives.
		//			if (model.hasNatives()) fig.activateNatives();
		//		}
		//		if (model.hasBase) fig.activateBase();
		//		if (model.hasShip) fig.activateShip();
		//		if (model.hasPods) fig.activatePods();
		//		if (model.hasWings) fig.activateWings();

		// - Update figure visuals from current model data.
		//		fig.setModel(model);
		fig.setCoordinates(representative.getLocation());
		fig.setId(representative.getIdNumber());
		fig.setName(representative.getName());

		//- Get the game player code.
		//		int player = getPlayerCode();
		//		final int owner = model.getFieldNumber("owner");
		//		if (player != owner)
		//			fig.setColor(AssistantConstants.COLOR_SHIP_ENEMY);
		//		else
		//			fig.setColor(AssistantConstants.COLOR_SHIP_DEFAULT);

		// - The references to the model and figure objects.
		//		final Figure fig = (Figure) getFigure();

		final Dimension size = fig.getSize().getCopy();
		Point location = representative.getLocation().getCopy();

		//- Adjust location depending on the top-left minimun coordinates.
		AssistantMap map = (AssistantMap) this.getParent().getModel();
		Rectangle boundaries = map.getSpotRegistry().getBoundaries();
		Point origin = new Point(boundaries.x - 30, boundaries.y - 30);
		location.x = location.x - origin.x;
		location.y = location.y - origin.y;
		fig.setLocation(location);

		//- Calculate real map location depending on the zoom factor.
		int zoom = getZoomFactor();
		if (zoom > 1) {
			location.x = location.x * zoom;
			location.y = location.y * zoom;
		}

		//- Change location to the neutral coordinates.
		//		location.x = 3000 - location.x;
		//		location.y = 3000 - location.y;
		final Rectangle bounds = new Rectangle(location, size);
		//		fig.setBounds(bounds);
		//		fig.setLocation(location);
		//		fig.repaint();
		//		fig.revalidate();
		//		fig.validate();
		//		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bounds);
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, fig, bounds);

		//		super.refreshVisuals();
	}

	private Figure getToolTip(Spot model) {
		AssistantNode representative = model.getRepresentative();
		final SpotFigure fig = (SpotFigure) this.getFigure();
		int player = getPlayerCode();
		//	int owner = representative.getOwner();

		//- Create the tooltip with the composite information for this Spot.
		Figure tip = new Figure();
		GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 1;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 1;
		tip.setLayoutManager(grid);
		Label repLabel = new Label(representative.getIdString() + "-" + representative.getName(), fig.getIconImage());
		tip.add(repLabel);
		Iterator<AssistantNode> mit = model.getContents().iterator();
		while (mit.hasNext()) {
			AssistantNode element = mit.next();
			if (element != representative) {
				//- Generate the image for the current element.
				IconImageFactory imageFactory = new IconImageFactory();
				imageFactory.setTip(true);
				if (element instanceof Planet) {
					imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
					imageFactory.setInfoLevel(model.infoAvailable());
					if (model.hasNatives()) imageFactory.activateNatives();
				}
				if (element instanceof Ship) {
					imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
					int owner = element.getOwner();
					if (player == owner)
						imageFactory.setEnemy(false);
					else
						imageFactory.setEnemy(true);
				}

				Label eleLabel = new Label(element.getIdString() + "-" + element.getName(), imageFactory.generateImage());
				tip.add(eleLabel);
				if (element instanceof Planet) {
					imageFactory = new IconImageFactory();
					imageFactory.setTip(true);
					imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
					imageFactory.setInfoLevel(IconImageFactory.INFOLEVEL_NATIVES);
					if (model.hasNatives())
						tip.add(new Label(((Planet) element).getNativeToolTip()), imageFactory.generateImage());
				}
			}
		}
		return tip;
	}

	public Spot getCastedModel() {
		return (Spot) this.getModel();
	}
}

// - UNUSED CODE ............................................................................................
