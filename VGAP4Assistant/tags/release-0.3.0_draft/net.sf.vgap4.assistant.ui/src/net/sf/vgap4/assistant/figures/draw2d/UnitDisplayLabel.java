//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitDisplayLabel extends Label {
  private static Logger     logger      = Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");
  // - S T A T I C - S E C T I O N ..........................................................................
  // - G L O B A L - C O N S T A N T S
  private static final int  CHAR_WIDTH  = 6;
  private static final int  CHAR_HEIGHT = 12;

  // - F I E L D - S E C T I O N ............................................................................
  // - F I E L D S

  // - C O N S T R U C T O R - S E C T I O N ................................................................
  public UnitDisplayLabel(final String text) {
    setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
    setLabelAlignment(PositionConstants.LEFT);
    setText(text);
//    setBorder(new LineBorder());
  }

  // - M E T H O D - S E C T I O N ..........................................................................
  @Override
  public void setText(final String newText) {
    super.setText(newText);
    final Dimension size = new Dimension((newText.length() + 0) * CHAR_WIDTH + 2, CHAR_HEIGHT);
    this.setSize(size);
    this.setPreferredSize(size);
  }
}

// - UNUSED CODE ............................................................................................
