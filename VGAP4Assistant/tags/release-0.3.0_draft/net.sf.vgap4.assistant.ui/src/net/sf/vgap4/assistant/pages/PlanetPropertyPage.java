//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.pages;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.AbstractPropertyPage;

import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import net.sf.vgap4.assistant.figures.DetailedPlanetFigure;
import net.sf.vgap4.assistant.figures.MineralsFigure;
import net.sf.vgap4.assistant.models.MiningInformation;
import net.sf.vgap4.assistant.models.Planet;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetPropertyPage extends AbstractPropertyPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.pages");

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the model. This reference allows complete access to the exported model fields. */
	private Planet				planetModel;
	private Canvas				canvas;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the interface
	 * view at runtime. Other fields may be <code>null</code> and in those situation the code will generate a
	 * informational message.
	 */
	public PlanetPropertyPage(Composite top) {
		super(top);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This method builds up the graphical elements that make the interface between the SWT
	 * <code>container</code> and the more manegeable draw2d <code>Figure</code> graphical interfaces. This
	 * will help to migrate from classical views to GEF controlled editor and use the same detailed figures to
	 * represent object data.
	 */
	/*
	 * Add all the common elements that are visible for a Planet. This depends on the information available on
	 * the model and the information filters set. This current implementation does not show all available
	 * information nor has any active filter.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#createContents()
	 */
	@Override
	protected void createContents() {
		page = new Group(container, SWT.NONE);
		canvas = new Canvas(page, SWT.NONE);
		final LightweightSystem lws = new LightweightSystem(canvas);
		final DetailedPlanetFigure base = new DetailedPlanetFigure(this.getModel());
		base.createContents();
		lws.setContents(base);
		final Dimension baseSize = base.getPreferredSize();
		canvas.setSize(baseSize.width, baseSize.height);
		canvas.setLocation(2, 2);
		final Point pageSize = canvas.getSize();
		page.setSize(pageSize.x + 4, pageSize.y + 10);
	}

	public Canvas getContainer() {
		return canvas;
	}

	/**
	 * Get the associated model element. At this class this model will be sub-classed to the <code>Planet</code>
	 * interface. If the model is null then we should throw an exception to signal some internal code error.
	 */
	@Override
	public Planet getModel() {
		Assert.isNotNull(planetModel, "The page unit model instance is not set up but it is being accessed.");
		return planetModel;
	}

	/**
	 * Store a reference to the model inside the page. Check also if a value existed previously to call the
	 * notification registration mechanism so this instance will receive any property modification to the model
	 * reference.
	 */
	public void setModel(final Planet model) {
		super.setModel(model);
		planetModel = model;
		// - Build up the page once the model is valid and the page can be generated.
		this.createContents();
	}

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * Intercept changes to the model properties. Changes supported by this class are only in the next list of
	 * properties:
	 * <ul>
	 * <li><b>Description</b> - user description for the Planet.</li>
	 * </ul>
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		// TODO Create a new property for the common Node notes
		// if (Planet.NOTES_PROP.equals(prop)) {
		// page.setText(getModel().getName());
		// }
	}

	//[01]
}

// - CLASS IMPLEMENTATION ...................................................................................
class AutosizedText extends Text {
	private static final int	DEFAULT_SIZE	= 8;
	// - F I E L D - S E C T I O N ............................................................................
	private Font							defaultFont		= new Font(Display.getDefault(), "Tahoma", DEFAULT_SIZE, SWT.NORMAL);
	private int								charSize			= DEFAULT_SIZE;

	// - C O N S T R U C T O R S
	public AutosizedText(Composite parent, int style) {
		super(parent, style);
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Allows to change the Font size and then recalculates the text size. This is the only permitted change to
	 * this class font.
	 */
	public void setFontSize(int size) {
		// - Check that sizes are not outside some limits.
		if ((size < 6) || (size > 16)) return;
		defaultFont = new Font(Display.getDefault(), "Tahoma", size, SWT.NORMAL);
		this.charSize = size;
		this.setFont(defaultFont);
	}

	// - O V E R R I D E - S E C T I O N
	/** Disable this method so the font will not be changed and then the size calculations are still valid. */
	@Override
	public void setFont(Font font) {
		super.setFont(defaultFont);
	}

	/** Set the test and at the same time calculate the new resulting size. */
	@Override
	public void setText(String newText) {
		super.setText(newText);
		final Point size = new Point((newText.length() + 1) * charSize + 2, charSize * 2);
		this.setSize(size);
	}
}

// - CLASS IMPLEMENTATION ...................................................................................
class PlanetPropertyPageFigure extends Figure {
	private static Label			locationLabel	= new Label();
	private static Label			location			= new Label();
	private static Label			mineralsLabel	= new Label();
	static {
		locationLabel.setText("Location:");
		location.setText("<location>");
		mineralsLabel.setText("Minerals:");
	}
	protected MineralsFigure	minerals			= new MineralsFigure();

	// public PlanetPropertyPageFigure() {
	// }
	protected void createContents(Planet model) {
		org.eclipse.draw2d.GridLayout grid = new org.eclipse.draw2d.GridLayout();
		grid.horizontalSpacing = 3;
		grid.marginHeight = 3;
		grid.marginWidth = 3;
		grid.numColumns = 1;
		grid.verticalSpacing = 2;
		this.setLayoutManager(new org.eclipse.draw2d.FlowLayout());
		// this.add(locationLabel);
		// this.add(location);
		// this.add(mineralsLabel);
		this.minerals.setNeutronium(model.getDisplayableMineralData(MiningInformation.NEUTRONIUM_ID));
		this.minerals.setDuranium(model.getDisplayableMineralData(MiningInformation.DURANIUM_ID));
		this.minerals.setTritanium(model.getDisplayableMineralData(MiningInformation.TRITANIUM_ID));
		this.minerals.setMolybdenum(model.getDisplayableMineralData(MiningInformation.MOLYBDENUM_ID));
		this.add(minerals);
		this.setSize(this.getPreferredSize());
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return this.minerals.getPreferredSize();
	}
}
// - UNUSED CODE ............................................................................................
//[01]
//private void createDescription(Group container) {
//final GridLayout grid = new GridLayout();
//grid.numColumns = 2;
//grid.horizontalSpacing = 2;
//grid.verticalSpacing = 0;
//grid.marginHeight = 0;
//grid.marginWidth = 0;
//RowLayout row = new RowLayout();
//row.type = SWT.HORIZONTAL;
//row.marginWidth = 2;
//row.marginLeft = 2;
//row.marginRight = 2;
//Composite descriptionControl = new Composite(container, SWT.NONE);
//descriptionControl.setLayout(row);
//final Text descriptionLabel = new Text(descriptionControl, SWT.NONE);
//descriptionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//descriptionLabel.setEditable(false);
//descriptionLabel.setText("Notes:");
//descriptionLabel.setSize(new Point((descriptionLabel.getText().length() + 1) * 8 + 2, 8 * 2));
//final Text descriptionText = new Text(descriptionControl, SWT.NONE);
//descriptionText.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
//descriptionText.setEditable(true);
//descriptionText.addModifyListener(new ModifyListener() {
//public void modifyText(final ModifyEvent event) {
//	logger.info("New Notes text: " + descriptionText.getText());
//	planetModel.setNotes(descriptionText.getText());
//}
//});
//descriptionText.setText(this.getModel().getNotes());
//descriptionText.setSize(new Point((descriptionText.getText().length() + 1) * 8 + 2, 8 * 2));
//// TODO Calculate the widgets size
//// RowData layoutData = new RowData();
//// layoutData.width = grid.marginWidth + descriptionLabel.getSize().x + grid.horizontalSpacing
//// + descriptionText.getSize().x + grid.marginWidth + 20;
//// layoutData.height = grid.marginHeight + Math.max(descriptionLabel.getSize().y,
//// descriptionText.getSize().y)
//// + grid.marginHeight;
//// descriptionControl.setLayoutData(layoutData);
//}
//
//private void createMineralSection(Group container) {
//RowLayout row = new RowLayout();
//row.type = SWT.HORIZONTAL;
//row.marginWidth = 2;
//row.marginLeft = 2;
//row.marginRight = 2;
//Composite mineralControl = new Composite(container, SWT.NONE);
//mineralControl.setLayout(row);
//final Text locationLabel = new Text(mineralControl, SWT.NONE);
//locationLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//locationLabel.setEditable(false);
//locationLabel.setText("Minerals:");
//locationLabel.setSize(new Point((locationLabel.getText().length() + 2) * 8, 8 * 2));
//
//Canvas canvas = new Canvas(mineralControl, SWT.NONE);
//canvas.setLayout(new FillLayout());
//LightweightSystem lws = new LightweightSystem(canvas);
//PlanetPropertyPageFigure base = new PlanetPropertyPageFigure();
//lws.setContents(base);
//base.createContents(this.planetModel);
//Dimension canvasSize = base.getSize();
//canvas.setSize(base.getSize().width, base.getSize().height);
//// TODO Calculate the widgets size
//canvas.setLayoutData(new RowData(canvasSize.width, canvasSize.height * 1));
//}
//
//private void createLocation(Group container) {
//// final GridLayout grid = new GridLayout();
//// grid.numColumns = 2;
//// grid.horizontalSpacing = 2;
//// grid.verticalSpacing = 0;
//// grid.marginHeight = 0;
//// grid.marginWidth = 0;
//RowLayout row = new RowLayout();
//row.type = SWT.HORIZONTAL;
//row.marginWidth = 2;
//row.marginLeft = 2;
//row.marginRight = 2;
//Composite locationControl = new Composite(container, SWT.NONE);
//locationControl.setLayout(row);
//final Text locationLabel = new Text(locationControl, SWT.NONE);
//locationLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//locationLabel.setEditable(false);
//locationLabel.setText("Location:");
//locationLabel.setSize(new Point((locationLabel.getText().length() + 2) * 8 + 2 + 2, 8 * 2));
//final Text locationText = new Text(locationControl, SWT.NONE);
//locationText.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
//locationText.setEditable(false);
//locationText.setText(this.getModel().getLocation().x + " - " + this.getModel().getLocation().y);
//locationLabel.setSize(new Point((locationText.getText().length() + 1) * 8 + 2 + 2, 8 * 2));
//// TODO Calculate the widgets size
//// RowData layoutData = new RowData();
//// layoutData.width = grid.marginWidth + locationLabel.getSize().x + grid.horizontalSpacing +
//// locationText.getSize().x
//// + grid.marginWidth;
//// layoutData.height = grid.marginHeight + Math.max(locationLabel.getSize().y, locationText.getSize().y)
//// + grid.marginHeight;
//// locationControl.setLayoutData(layoutData);
//}
