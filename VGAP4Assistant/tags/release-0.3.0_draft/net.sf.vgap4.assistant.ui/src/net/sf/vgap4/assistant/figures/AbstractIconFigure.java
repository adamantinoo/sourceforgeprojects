//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.Color;

//- CLASS IMPLEMENTATION ...................................................................................
public class AbstractIconFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the parent figure. The parent figure has access to the model information that can also be
	 * accessed through this reference.
	 */
	private final AssistantNodeFigure	container;
	protected int											drawingSize	= 11;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractIconFigure(AssistantNodeFigure homeFigure) {
		super();
		container = homeFigure;
		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Point getHotSpot() {
		return new Point(drawingSize / 2, drawingSize / 2);
	}

	protected AssistantNodeFigure getContainer() {
		return container;
	}

	protected boolean isSelected() {
		return container.isSelected();
	}

	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		return new Dimension(drawingSize, drawingSize);
	}

	protected void setDrawingSize(final int figureSize) {
		drawingSize = figureSize;
		this.setSize(getPreferredSize());
	}

	public Color getColor() {
		return container.getColor();
	}
}
// - UNUSED CODE ............................................................................................
