package net.sf.vgap4.assistant.views;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Node;

import net.sf.vgap4.assistant.models.AssistantNode;

class GraphLayoutManager extends AbstractLayout {

	private DetailedContainerEditPart	rootPart;

	GraphLayoutManager(DetailedContainerEditPart diagram) {
		this.rootPart = diagram;
	}

	@Override
	protected Dimension calculatePreferredSize(IFigure container, int wHint, int hHint) {
		container.validate();
		List<AssistantNode> children = container.getChildren();
		Rectangle result = new Rectangle().setLocation(container.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++)
			result.union(((IFigure) children.get(i)).getBounds());
		result.resize(container.getInsets().getWidth(), container.getInsets().getHeight());
		return result.getSize();
	}

	public void layout(IFigure container) {
		//		GraphAnimation.recordInitialState(container);
		//		if (GraphAnimation.playbackState(container)) return;

		DirectedGraph graph = new DirectedGraph();
		Map<DetailedDefaultEditPart, Node> partsToNodes = new HashMap<DetailedDefaultEditPart, Node>();
		rootPart.contributeNodesToGraph(graph, partsToNodes);
		//		rootPart.contributeEdgesToGraph(graph);
		new DirectedGraphLayout().visit(graph); // Calculate layout
		rootPart.applyGraphResults(graph, partsToNodes); // Resize and reposition figures.
	}

}
