//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Properties;

import org.eclipse.draw2d.geometry.Point;

public class AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long		serialVersionUID	= 2420277850464981241L;

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This structure stores all the information parsed from the .CSV file in a format suitable to be accessed
	 * by name. Instead storing every data inside its own field and the creating accessors to manipulate it, I
	 * have created a single structure in form of a Property list to get each element by name. Some of them will
	 * be able to be cached so the processing overhead to get many times the same information are reduced for
	 * this common accessed data.
	 */
	protected final Properties	info							= new Properties();
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	protected int								idNumber					= -1;
	/** Stores the name for this planet, this name is set to this game map and cannot be changed. */
	protected String						name;
	/**
	 * Stores the coordinates to the planet location inside the diagram coordinates. Those coordinates do not
	 * change and can be cached.
	 */
	protected Point							location;

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getField(final String key) {
		final String value = info.getProperty(key);
		if (null == value) return "";
		return value;
	}

	public int getFieldNumber(final String key) {
		try {
			return this.convert2Integer(this.getField(key));
		} catch (final Exception e) {
			//- The field is null or not convertible to a number. Return the default CERO.
			return 0;
		}
	}

	public int getIdNumber() {
		return idNumber;
	}

	public Point getLocation() {
		if (null == location)
			return new Point(0, 0);
		else
			return location;
	}

	public String getName() {
		return name;
	}

	protected int convert2Integer(final String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}

}
// - UNUSED CODE ............................................................................................
