//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class IconImageFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger								= Logger.getLogger("net.sf.vgap4.assistant.figures");
	public static String				SHAPE_PLANET					= "IconImageFactory.SHAPE_PLANET";
	public static String				SHAPE_SHIP						= "IconImageFactory.SHAPE_SHIP";
	public static String				SHAPE_POD							= "IconImageFactory.SHAPE_POD";
	public static String				SHAPE_WING						= "IconImageFactory.SHAPE_WING";
	public static String				INFOLEVEL_UNEXPLORED	= "IconImageFactory.INFOLEVEL_UNEXPLORED";
	public static String				INFOLEVEL_NOINFO			= "IconImageFactory.INFOLEVEL_NOINFO";
	public static String				INFOLEVEL_INFO				= "IconImageFactory.INFOLEVEL_INFO";
	public static final String	INFOLEVEL_NATIVES			= "IconImageFactory.INFOLEVEL_NATIVES";

	// - F I E L D - S E C T I O N ............................................................................
	private String							shapeCode							= SHAPE_PLANET;
	private Color								mainShapeColor				= AssistantConstants.COLOR_PLANET_UNEXPLORED;
	private Color								nativesColor					= AssistantConstants.COLOR_NATIVES_DEFAULT;
	private Color								shipsColor						= AssistantConstants.COLOR_SHIP_DEFAULT;
	private Color								podsColor							= AssistantConstants.COLOR_SHIP_DEFAULT;
	private Color								wingsColor						= AssistantConstants.COLOR_SHIP_DEFAULT;
	//	private Color					decoratorColor	= AssistantConstants.COLOR_SHIP_DEFAULT;
	private int									drawingSize						= AssistantConstants.SIZE_ICONIMAGE;
	private boolean							baseActive						= false;
	private boolean							shipInOrbit						= false;
	private boolean							podInOrbit						= false;
	private boolean							hasNatives						= false;
	private boolean							wingsInOrbit					= false;
	private boolean							isOnTip								= false;
	private String							informationLevel			= INFOLEVEL_INFO;
	private String							planetClassification	= "B";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IconImageFactory() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void activateBase(boolean enemyFlag) {
		baseActive = true;
		if (enemyFlag)
			mainShapeColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			mainShapeColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateNatives() {
		hasNatives = true;
	}

	public void activatePods(boolean enemyFlag) {
		podInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateShip(boolean enemyFlag) {
		shipInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateWings(boolean enemyFlag) {
		wingsInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void setInfoLevel(String level) {
		this.informationLevel = level;
		if (INFOLEVEL_UNEXPLORED.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_UNEXPLORED;
		if (INFOLEVEL_NOINFO.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_NOINFO;
		if (INFOLEVEL_INFO.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_INFO;
		if (INFOLEVEL_NATIVES.equals(level)) mainShapeColor = AssistantConstants.COLOR_NATIVES_DEFAULT;
	}

	public void setShape(String shape) {
		this.shapeCode = shape;
	}

	public void setTip(boolean onTip) {
		this.isOnTip = onTip;
	}

	public void clearBase() {
		baseActive = false;
	}

	public void clearNatives() {
		hasNatives = false;
	}

	public void clearPods() {
		podInOrbit = false;
	}

	public void clearShip() {
		shipInOrbit = false;
	}

	public void clearWings() {
		wingsInOrbit = false;
	}

	public void clearActivates() {
		baseActive = false;
		hasNatives = false;
		shipInOrbit = false;
		podInOrbit = false;
		wingsInOrbit = false;
	}

	public Image generateImage() {
		final Image image = new Image(Display.getDefault(), drawingSize, drawingSize);
		final GC gc = new GC(image);

		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = new Rectangle(0, 0, drawingSize, drawingSize);

		//- If the image is to be located into a tip, then fill the background color.
		if (isOnTip) {
			gc.setBackground(AssistantConstants.COLOR_TIP);
			gc.fillRectangle(bound);
		}

		//- Draw the shape.
		if (SHAPE_PLANET.equals(shapeCode)) {
			//- Draw the Planet circle
			gc.setBackground(mainShapeColor);
			final Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);

			//- Draw the Planet classification
			if (planetClassification.equals("A")) {
				gc.setForeground(AssistantConstants.COLOR_BRILLIANT_RED);
				gc.drawLine(0, 0, 3, 3);
				gc.drawLine(3, 3, 1, 3);
				gc.drawLine(3, 3, 3, 1);
			}
			if (planetClassification.equals("C")) {
				gc.setForeground(ColorConstants.darkGray);
				gc.drawLine(0, 0, 3, 3);
				gc.drawLine(3, 0, 0, 3);
			}
		}
		if (SHAPE_SHIP.equals(shapeCode)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + 3, bound.y + drawingSize / 2 - 1);
			final Point p2 = new Point(head.x + 10, head.y - 4);
			final Point p3 = new Point(head.x + 10, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			gc.setBackground(shipsColor);
			gc.fillPolygon(triangle.toIntArray());
		}

		//- Draw the Base decorator
		if (baseActive) {
			gc.setForeground(ColorConstants.lightGray);
			//			gc.setBackground(ColorConstants.lightGray);
			gc.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods or wings.
		if (shipInOrbit) {
			gc.setBackground(shipsColor);
			gc.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			gc.setBackground(podsColor);
			gc.fillOval(bound.x + 10, bound.y + 10, 4, 4);
		}
		if (wingsInOrbit) {
			gc.setBackground(wingsColor);
			gc.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}
		if (hasNatives) {
			gc.setBackground(nativesColor);
			gc.fillOval(bound.x + 2, bound.y + 2, 4, 4);
		}
		gc.dispose();
		return image;
	}

	public void setEnemy(boolean enemy) {
		if (enemy)
			shipsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			shipsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void setPlanetClassification(String planetClassification) {
		this.planetClassification = planetClassification;
	}

	//	public void setBaseFigure(final String baseCode) {
	//		((SpotIconFigure) iconic).setBaseFigure(baseCode);
	//	}
	//
	//	public void setDecoratorColor(final Color newDecoratorColor) {
	//		decoratorColor = newDecoratorColor;
	//	}

	//	public void setMainColor(final Color newBaseColor) {
	//		mainShapeColor = newBaseColor;
	//	}
}

// - UNUSED CODE ............................................................................................
