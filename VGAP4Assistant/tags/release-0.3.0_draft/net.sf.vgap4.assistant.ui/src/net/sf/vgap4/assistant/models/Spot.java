//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.factories.IconImageFactory;

// - CLASS IMPLEMENTATION ...................................................................................
public class Spot extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long			serialVersionUID	= 1L;
	private static Logger					logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	private Vector<AssistantNode>	clusterContents		= new Vector<AssistantNode>();
	public boolean								hasBase						= false;
	public boolean								hasPlanet					= false;
	public boolean								hasShip						= false;
	public boolean								hasPods						= false;
	public boolean								hasWings					= false;
	private AssistantNode					representative;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Spot() {
	}

	public Spot(LocationCluster cluster) {
		//		this.locationObjects = cluster;

		//TODO Load the cached data in the spot fields from the cluster representative.
		AssistantNode representative = cluster.getRepresentative();
		setLocation(representative.getLocation());
		setIdNumber(representative.getIdNumber());
		setName(representative.getName());
		setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void add(AssistantNode mapElement) {
		if (mapElement instanceof Ship) {
			hasShip = true;
			if (hasBase || hasPlanet)
				hasShip = true;
			else
				representative = mapElement;
		}
		if (mapElement instanceof Planet) {
			hasPlanet = true;
			if (hasBase)
				hasPlanet = true;
			else
				representative = mapElement;
		}
		if (mapElement instanceof Base) {
			hasBase = true;
			representative = mapElement;
		}
		clusterContents.add(mapElement);
	}

	//	public LocationCluster getContents() {
	//		return locationObjects;
	//	}

	//	public AssistantNode getRepresentative() {
	//		return this.locationObjects.getRepresentative();
	//	}

	public boolean hasContents() {
		if (null != representative) return true;
		return false;
	}

	public boolean hasNatives() {
		if (hasPlanet) {
			Iterator<AssistantNode> cit = clusterContents.iterator();
			while (cit.hasNext()) {
				AssistantNode element = cit.next();
				if (element instanceof Planet) {
					if (((Planet) element).getTotalNatives() > 0) return true;
				}
			}
		}
		return false;
	}

	public AssistantNode getRepresentative() {
		return representative;
	}

	public boolean isMultiple() {
		if (this.clusterContents.size() > 1)
			return true;
		else
			return false;
	}

	public String getPreferredShape() {
		if (hasBase) return "Base";
		if (hasPlanet) return "Planet";
		if (hasShip) return "Ship";
		return "Undefined";
	}

	public Vector<AssistantNode> getContents() {
		return this.clusterContents;
	}

	public String infoAvailable() {
		if (hasPlanet) {
			Planet planet = locatePlanet();
			if (null != planet) return planet.infoAvailable();
		}
		return IconImageFactory.INFOLEVEL_UNEXPLORED;
	}

	private Planet locatePlanet() {
		Iterator<AssistantNode> cit = clusterContents.iterator();
		while (cit.hasNext()) {
			AssistantNode element = cit.next();
			if (element instanceof Planet) { return (Planet) element; }
		}
		return null;
	}

	@Override
	public int getOwner() {
		return this.representative.getOwner();
	}
}

// - UNUSED CODE ............................................................................................
