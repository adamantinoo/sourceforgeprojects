//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sf.vgap4.assistant.figures.AssistantNodeFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public class SectorFigure extends AssistantNodeFigure {
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - F I E L D S
	private int									size					= 200;
	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public SectorFigure() {
	}

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		return new Dimension(size, size);
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();

		// - Draw the figure body
		graphics.setForegroundColor(ColorConstants.black);
		graphics.setBackgroundColor(ColorConstants.black);
		graphics.setLineWidth(2);
		graphics.drawRectangle(bound);
	}
	// - I N T E R F A C E - N A M E
}
// - UNUSED CODE ............................................................................................
