//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sf.vgap4.assistant.figures.AbstractIconFigure;
import net.sf.vgap4.assistant.figures.AssistantNodeFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetIconFigure extends AbstractIconFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PlanetIconFigure(AssistantNodeFigure homeFigure) {
		super(homeFigure);
		setDrawingSize(9);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();

		// - Draw the figure body
		graphics.setForegroundColor(ColorConstants.darkGray);
		graphics.setBackgroundColor(ColorConstants.darkGray);
		graphics.fillOval(bound);
	}
}

// - UNUSED CODE ............................................................................................
