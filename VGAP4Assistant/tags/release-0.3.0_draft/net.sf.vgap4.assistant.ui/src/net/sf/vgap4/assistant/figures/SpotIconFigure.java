//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotIconFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");
	public static String							SHAPE_PLANET	= "SpotIconFigure.SHAPE_PLANET";
	public static String							SHAPE_SHIP		= "SpotIconFigure.SHAPE_SHIP";
	public static String							SHAPE_POD			= "SpotIconFigure.SHAPE_POD";
	public static String							SHAPE_WING		= "SpotIconFigure.SHAPE_WING";

	public boolean										baseActive		= false;
	public boolean										shipInOrbit		= false;
	public boolean										podInOrbit		= false;
	public boolean										hasNatives		= false;
	public boolean										wingsInOrbit	= false;

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the parent figure. The parent figure has access to the model information that can also be
	 * accessed through this reference.
	 */
	private final AssistantNodeFigure	container;
	//	private final XYLayout						xylay					= new XYLayout();
	private String										baseCode			= SpotIconFigure.SHAPE_PLANET;
	protected int											drawingSize		= 11;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotIconFigure(final AssistantNodeFigure homeFigure) {
		super();
		this.setDrawingSize(16);
		container = homeFigure;
		this.setSize(this.getPreferredSize(-1, -1));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Image convertToImage() {
		final Image image = new Image(Display.getDefault(), 16, 16);
		final GC gc = new GC(image);

		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = new Rectangle(0, 0, 16, 16);

		//- Paint the base.
		if (SpotIconFigure.SHAPE_PLANET.equals(baseCode)) {
			//- Draw the Planet circle
			gc.setBackground(((SpotFigure) this.getContainer()).mainShapeColor);
			final Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
		}
		if (SpotIconFigure.SHAPE_SHIP.equals(baseCode)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + 3, bound.y + drawingSize / 2 - 1);
			final Point p2 = new Point(head.x + 10, head.y - 4);
			final Point p3 = new Point(head.x + 10, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			gc.setBackground(((SpotFigure) this.getContainer()).mainShapeColor);
			gc.fillPolygon(triangle.toIntArray());
		}

		//- Draw the Base decorator
		if (baseActive) {
			gc.setForeground(ColorConstants.lightGray);
			gc.setBackground(ColorConstants.lightGray);
			gc.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods.
		if (shipInOrbit) {
			//- Draw the Ship in Orbit decorator
			gc.setBackground(((SpotFigure) this.getContainer()).decoratorColor);
			gc.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			gc.setBackground(AssistantConstants.COLOR_BRILLIANT_BLUE);
			gc.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}

		if (hasNatives) {
			//- Draw the other decorators
			gc.setBackground(ColorConstants.orange);
			gc.fillOval(bound.x + 2, bound.y + 2, 4, 4);
		}
		gc.dispose();
		return image;
	}

	public Point getHotSpot() {
		return new Point(drawingSize / 2, drawingSize / 2);
	}

	protected AssistantNodeFigure getContainer() {
		return container;
	}

	protected boolean isSelected() {
		return container.isSelected();
	}

	protected void setDrawingSize(final int figureSize) {
		drawingSize = figureSize;
		this.setSize(getPreferredSize());
	}

	public Color getColor() {
		return container.getColor();
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(drawingSize, drawingSize);
	}

	public void setBaseFigure(final String baseCode) {
		this.baseCode = baseCode;
		this.repaint();
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = this.getBounds().getCopy();

		//- Paint the base.
		if (SpotIconFigure.SHAPE_PLANET.equals(baseCode)) {
			//- Draw the Planet circle
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).mainShapeColor);
			final Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			graphics.fillOval(planetBounds);
		}
		if (SpotIconFigure.SHAPE_SHIP.equals(baseCode)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + drawingSize / 2, bound.y + 4);
			final Point p2 = new Point(head.x + 8, head.y - 4);
			final Point p3 = new Point(head.x + 8, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).mainShapeColor);
			graphics.fillPolygon(triangle);
		}

		//- Draw the Base decorator
		if (baseActive) {
			graphics.setForegroundColor(ColorConstants.lightGray);
			graphics.setBackgroundColor(ColorConstants.lightGray);
			graphics.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods.
		if (shipInOrbit) {
			//- Draw the Ship in Orbit decorator
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).decoratorColor);
			graphics.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			graphics.setBackgroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
			graphics.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}

		if (hasNatives) {
			//- Draw the other decorators
			graphics.setBackgroundColor(ColorConstants.orange);
			graphics.fillOval(bound.x + 2, bound.y + 2, 4, 4);
		}
		if (false) {
			//- Draw the other decorators
			graphics.setBackgroundColor(AssistantConstants.COLOR_MEDIUM_YELLOW);
			graphics.fillOval(bound.x + 11, bound.y + 11, 4, 4);

			//- Draw the other decorators
			graphics.setBackgroundColor(AssistantConstants.COLOR_LIGHT_BLUE);
			graphics.fillOval(bound.x + 1, bound.y + 6, 4, 4);
		}
	}
}
// - UNUSED CODE ............................................................................................
