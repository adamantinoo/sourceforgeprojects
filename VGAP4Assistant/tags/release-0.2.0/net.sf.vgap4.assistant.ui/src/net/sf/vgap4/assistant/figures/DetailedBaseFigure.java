//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.LineBorder;
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedBaseFigure extends AbstractDetailedFigure {
  // - S T A T I C - S E C T I O N ..........................................................................
  private static Logger logger  = Logger.getLogger("net.sf.vgap4.assistant.figures");

  // - F I E L D - S E C T I O N ............................................................................
  private final Base    baseModel;

  // - C O N S T R U C T O R - S E C T I O N ................................................................
  public DetailedBaseFigure(final Base model) {
    super(model);
    baseModel = model;
  }

  // - M E T H O D - S E C T I O N ..........................................................................
  @Override
  protected void addAdditionalContents() {
    final MultiLabelLine passageLabel = new MultiLabelLine();
    passageLabel.addColumn("Minerals:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
    passageLabel.addColumn("-undef-", SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
    this.add(passageLabel);
    this.createMineralsInfo();
  }

  protected void createMineralsInfo() {
    //DEBUG Check mineral values.
    final int max = 1500;
    final int res = baseModel.getFieldNumber("resN");
    final int sur = baseModel.getFieldNumber("eleN");
    final MineralBarFigure neutronium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_RED,
        AssistantConstants.COLOR_STANDARD_ORANGE);
    neutronium.setLevels(max, baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
    final MineralLine neutroniumLine = new MineralLine("N", neutronium);
    neutroniumLine.setValues(baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
//    if (true) neutroniumLine.setBorder(new LineBorder());
    this.add(neutroniumLine);

    final MineralBarFigure duranium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKBLUE,
        AssistantConstants.COLOR_STANDARD_BLUE);
    duranium.setLevels(max, baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
    final MineralLine duraniumLine = new MineralLine("D", duranium);
    duraniumLine.setValues(baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
//    if (true) duraniumLine.setBorder(new LineBorder());
    this.add(duraniumLine);

    final MineralBarFigure tritanium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKGREN,
        AssistantConstants.COLOR_STANDARD_GREEN);
    tritanium.setLevels(max, baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
    final MineralLine tritaniumLine = new MineralLine("T", tritanium);
    tritaniumLine.setValues(baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
//    if (true) tritaniumLine.setBorder(new LineBorder());
    this.add(tritaniumLine);

    final MineralBarFigure molybdenum = new MineralBarFigure(AssistantConstants.COLOR_BROWN,
        AssistantConstants.COLOR_STANDARD_ORANGE);
    molybdenum.setLevels(max, baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
    final MineralLine molybdenumLine = new MineralLine("M", molybdenum);
    molybdenumLine.setValues(baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
//    if (true) molybdenumLine.setBorder(new LineBorder());
    this.add(molybdenumLine);
  }
}

// - UNUSED CODE ............................................................................................
