//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class Spot extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= 1L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	private LocationCluster		locationObjects;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Spot(LocationCluster cluster) {
		this.locationObjects = cluster;

		//TODO Load the cached data in the spot fields from the cluster representative.
		AssistantNode representative = cluster.getRepresentative();
		setLocation(representative.getLocation());
		setIdNumber(representative.getIdNumber());
		setName(representative.getName());
		setLastTurn(representative.getLastTurn());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public LocationCluster getContents() {
		return locationObjects;
	}

	public AssistantNode getRepresentative() {
		return this.locationObjects.getRepresentative();
	}
}

// - UNUSED CODE ............................................................................................
