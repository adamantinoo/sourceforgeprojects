//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractPropertyChanger;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.BaseInformation;
import net.sf.vgap4.assistant.models.LocationCluster;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.PlanetInformation;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.ShipInformation;
import net.sf.vgap4.assistant.models.Spot;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4Map extends AbstractPropertyChanger {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	/** Property ID to use when new model data is added to the diagram. */
	public static final String							DATA_ADDED_PROP					= "VGAP4Map.DATA_ADDED_PROP";
	/** Property ID to use when a child is added to this diagram. */
	public static final String							CHILD_ADDED_PROP				= "VGAP4Map.CHILD_ADDED_PROP";
	/** Property ID to use when a child is removed from this diagram. */
	public static final String							CHILD_REMOVED_PROP			= "VGAP4Map.CHILD_REMOVED_PROP";
	/** Property ID to use when a the number referencing any of the last turns gets changed. */
	public static final String							LAST_TURN_CHANGED_PROP	= "VGAP4Map.LAST_TURN_CHANGED_PROP";
	private static final long								serialVersionUID				= 3361707471258570866L;
	private static Logger										logger									= Logger
																																			.getLogger("net.sf.vgap4.projecteditor.model");

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D S
	/** Number of the last turn processed. */
	private int															lastReadTurn						= -1;
	/** Number of the higher turn processed. */
	private int															lastAvailableTurn				= -1;
	/** Number the identifies a unique game code. */
	private String													gameId;
	/** Name given to this game when created. */
	private String													gameName;
	private final int												mapSize									= 3000;
	/**
	 * Player number that matches the slot number on the game. This number may correspond to another user inside
	 * other games so it is used with the 'gameId' to uniquely identify a player slot on a game set.
	 */
	private int															playerNo								= -1;
	/**
	 * Sectors are virtual structures that keep lists of grouped elements (generally by geographic location).
	 * This field stores the references to all the sectors currently defined on this Map. This is a simple list
	 * structure and currently sectors do not have any sorting order or organization. A sector may contain other
	 * sectors to any level because it simply defines a physical rectangular map area.
	 */
	private final Vector<Sector>						sectors									= new Vector<Sector>();
	/**
	 * Array structure with references to all the Planets that have been identified on this turn or on other
	 * turns. This structure is implemented as an array because it matches the original implementation from CSV
	 * files. Probably a 'Hashtable' will generate less memory usage, over all in very long games where the Id
	 * of some elements may grew high. Planets are the exception because there is a finite number of them and
	 * alwais get the lowest element Id.
	 */
	private final Vector<Planet>						planets									= new Vector<Planet>();
	/**
	 * This structure contains a reference to all player bases. This structure clearly benefits from the
	 * 'Hashtable' implementation because the number of bases for this player is small in relation to the number
	 * of planets and the number of other elements created in the game.
	 */
	private final Hashtable<Integer, Base>	bases										= new Hashtable<Integer, Base>();
	/**
	 * Another array structure that contains all the identified Ships. This list contains all ships owned by
	 * this player ans also the ships detected to other players. That other ships are identified by being the
	 * 'owner' a different code than the game 'playerId'.
	 */
	private final Vector<Ship>							ships										= new Vector<Ship>();
	private LocationRegistry								locationRegistry				= new LocationRegistry();

	//	{
	//		// - Add some sectors manually to all maps.
	//		sectors.add(new Sector("SECTOR - Homeworld", 2100, 2600, 300, 200));
	//		sectors.add(new Sector("SECTOR - Brand", 2300, 2900, 300, 200));
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addBaseInformation(final int turn, final BaseInformation baseInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Base theBase = this.getBase4Id(baseInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theBase.addTurnInformation(turn, baseInfo);
		//- Register this planet in the location registry.
		this.locationRegistry.register(baseInfo.getLocation(), theBase);
	}

	//	/**
	//	 * Adds a new element to the diagram model for presentation. There are few types supported and this methods
	//	 * is in charge of the type detection to incorporate the unit to the right list.
	//	 * 
	//	 * @param newUnit
	//	 */
	//	public void addChild(final PositionableUnit newUnit) {
	//		// - Check the real type of object to be set on the right vector.
	//		if (newUnit instanceof Planet) {
	//			this.addPlanet((Planet) newUnit);
	//			// - Update the 'dirty' flag to signal that this file need to be saved.
	//			return;
	//		}
	//		if (newUnit instanceof Ship) {
	//			this.addShip((Ship) newUnit);
	//			// - Update the 'dirty' flag to signal that this file need to be saved.
	//			return;
	//		}
	//	}

	/**
	 * Adds the processed planet information to the model. The planet information identifies the game, the
	 * player and the turn. If they do not match the same information on the model this instance is discarded
	 * and this actions registered with a warning message.<br>
	 * If this information is the first to be added to the model, then the model gets the player and name from
	 * this single instance and successive additions must match on that information.
	 * 
	 * @param turn
	 *          is the number of the turn data we are loading.
	 */
	/*
	 * The diagram structure is such that it has elements to contain all the information to be displayed. The
	 * first aggregation level is the Planet information. There is an slot for any single Planet, but that slot
	 * contains multitude of information, from the different turns and from the game itself. The main contents
	 * of this document is to record the Game Id and the user that is using this document to avoid reading
	 * others player data into the presentation model. The game and player identification area set when the file
	 * is read and initialized when the document is created and never more changed.
	 */
	/**
	 * Add the new turn information to the Planet structure. We have not to check that the owner and game
	 * matches because that test have been performed before arriving this point. But some check has to be
	 * performed to identify when we have not locate the correct Planet. The variable 'thePlanet' may not be
	 * allowed to be NULL because we have a reference on it.
	 */
	public void addPlanetInformation(final int turn, final PlanetInformation planetInfo) {
		// - Get the Planet structure that matched to the Planet ID.
		final Planet thePlanet = this.getPlanet4Id(planetInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		thePlanet.addTurnInformation(turn, planetInfo);
		//- Register this planet in the location registry.
		this.locationRegistry.register(planetInfo.getLocation(), thePlanet);
	}

	public void addShipInformation(final int turn, final ShipInformation shipInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Ship theShip = this.getShip4Id(shipInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theShip.addTurnInformation(turn, shipInfo);
		//- Register this planet in the location registry.
		this.locationRegistry.register(shipInfo.getLocation(), theShip);
	}

	/**
	 * Return a List of all the visible elements in the diagram. Visible elements are the Planets, the Bases and
	 * the Ships. The returned List should not be modified.<br>
	 * Visible elements have locations, the list returned is the list of visible elements at the locations. If a
	 * location has more than a single object, then instead returning that object the method creates a new
	 * aggregate that generates a single visible object for the list of common location elements.
	 */
	public Vector<Object> getChildrenNew() {
		final Vector<Object> childs = new Vector<Object>();
		//- Scan the locations and generate the list of visible objects.
		Enumeration<Point> lit = this.locationRegistry.getLocations();
		while (lit.hasMoreElements()) {
			Point location = lit.nextElement();
			LocationCluster objects = this.locationRegistry.getObjects(location);
			if (objects.hasContents()) {
				if (objects.isMultiple())
					childs.add(new Spot(objects));
				else
					childs.add(objects.getRepresentative());
			}
		}
		return childs;
	}

	public Vector<Object> getChildren() {
		final Vector<Object> childs = new Vector<Object>();
		// - Add only objects that are not null
		final Iterator<Sector> itsec = sectors.iterator();
		while (itsec.hasNext()) {
			final Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final Base base = btp.next();
			childs.add(base);
		}
		final Iterator<Planet> itp = planets.iterator();
		while (itp.hasNext()) {
			final Planet planet = itp.next();
			if (null != planet) {
				//TODO - Add only the planets that do not have a base.
				if (!planet.isHasBase()) childs.add(planet);
			}
		}
		final Iterator<Ship> its = ships.iterator();
		while (its.hasNext()) {
			final Ship ship = its.next();
			if (null != ship) childs.add(ship);
		}
		return childs;
	}

	public String getGameId() {
		return gameId;
	}

	/**
	 * Check for the array size and the array contents. The array starts empty so any try to access any null
	 * element will trigger an exception. INtercept the exceptions and resize the array adecuately to the
	 * current usage.
	 */
	public Planet getPlanet4Id(final int id) {
		// - Get the element we are searching for. If exception then resize the array and return a new structure.
		try {
			final Planet thePlanet = planets.get(id);
			if (null == thePlanet) {
				// - The planet is not detected because it is new data.
				planets.setSize(id + 1);
				planets.setElementAt(new Planet(this), id);
				return planets.get(id);
			} else
				return thePlanet;
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			// - The planet has not any previous data. Resize the vector to the proper size and create a new one.
			planets.setSize(id + 1);
			planets.setElementAt(new Planet(this), id);
			return planets.get(id);
		}
	}

	public int getPlayer() {
		return playerNo;
	}

	public int getReadTurn() {
		return lastReadTurn;
	}

	// - P U B L I C - S E C T I O N
	public boolean isInitialized() {
		if ((-1 == lastReadTurn) && (null == gameId))
			return false;
		else
			return true;
	}

	/**
	 * Remove a Planet from the model. This will happen when the user overrides the interface and chooses to
	 * remove some model and history data.
	 * 
	 * @param obsoletePlanet
	 *          a non-null Planet instance;
	 */
	public void removeChild(final PositionableUnit obsoleteUnit) {
		if (null != obsoleteUnit) {
			planets.remove(obsoleteUnit);
			ships.remove(obsoleteUnit);
			this.firePropertyChange(VGAP4Map.CHILD_REMOVED_PROP, null, obsoleteUnit);
		}
	}

	public void setGameId(final String gameId) {
		this.gameId = gameId;
	}

	public void setGameName(final String gameName) {
		this.gameName = gameName;
	}

	public void setPlayer(final int playerNo) {
		this.playerNo = playerNo;
	}

	public void setReadTurn(final int readTurn) {
		lastReadTurn = readTurn;
		lastAvailableTurn = Math.max(lastAvailableTurn, lastReadTurn);
	}

	private Base getBase4Id(final int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		try {
			final Base theBase = bases.get(new Integer(id));
			if (null == theBase) {
				//- The base is not located because this is a new element.
				//				this.bases.setSize(id + 1);
				bases.put(new Integer(id), new Base(this));
				return bases.get(new Integer(id));
			} else
				return theBase;
		} catch (final Exception aioobe) {
			aioobe.printStackTrace();
			bases.put(new Integer(id), new Base(this));
			return bases.get(new Integer(id));
			//					// - The base array has not any previous data. Resize the vector to the proper size.
			//					this.bases.setSize(id + 1);
			//					this.bases.setElementAt(new Base(), id);
			//					return this.bases.get(id);
		}
	}

	private Ship getShip4Id(final int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		try {
			final Ship theShip = ships.get(id);
			if (null == theShip) {
				//- The ship is not located because this is a new element.
				ships.setSize(id + 1);
				ships.setElementAt(new Ship(this), id);
				return ships.get(id);
			} else
				return theShip;
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			// - The ship array has not any previous data. Resize the vector to the proper size.
			ships.setSize(id + 1);
			ships.setElementAt(new Ship(this), id);
			return ships.get(id);
		}
	}

	/**
	 * Add a Planet to this model.
	 * 
	 * @param newPlanet
	 *          a non-null Planet instance
	 */
	protected void addPlanet(final Planet newPlanet) {
		if (null != newPlanet) {
			planets.add(newPlanet);
			this.firePropertyChange(VGAP4Map.CHILD_ADDED_PROP, null, newPlanet);
		}
	}

	protected void addShip(final Ship newShip) {
		if (null != newShip) {
			ships.add(newShip);
			this.firePropertyChange(VGAP4Map.CHILD_ADDED_PROP, null, newShip);
		}
	}

	public void clearLocationRegistry() {
		this.locationRegistry.clear();
	}
}

class LocationRegistry implements Serializable {
	private static final long									serialVersionUID	= 1L;

	private Hashtable<Point, LocationCluster>	locations					= new Hashtable<Point, LocationCluster>();
	private int																minX							= 9000;
	private int																minY							= 9000;
	private int																maxX							= -1;
	private int																maxY							= -1;

	public void clear() {
		this.locations.clear();
		minX = 9000;
		minY = 9000;
		maxX = -1;
		maxY = -1;
	}

	public LocationCluster getObjects(Point location) {
		return this.locations.get(location);
	}

	public Enumeration<Point> getLocations() {
		return this.locations.keys();
	}

	public void register(Point location, AssistantNode mapElement) {
		if ((null != location) && (null != mapElement)) {
			//- Check if this location has already been used. If so add this new element to the vector.
			LocationCluster cluster = this.locations.get(location);
			if (null == cluster) {
				cluster = new LocationCluster();
				cluster.add(mapElement);
				this.locations.put(location, cluster);
			} else {
				cluster.add(mapElement);
			}
			minX = Math.min(minX, location.x);
			minY = Math.min(minY, location.y);
			maxX = Math.max(maxX, location.x);
			maxY = Math.max(maxY, location.y);
		}
	}
}
// - UNUSED CODE ............................................................................................
