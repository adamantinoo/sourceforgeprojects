//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class StandardLabel extends Label {
  // - S T A T I C - S E C T I O N ..........................................................................
  // - F I E L D - S E C T I O N ............................................................................
  // - C O N S T R U C T O R - S E C T I O N ................................................................
  // - M E T H O D - S E C T I O N ..........................................................................
  public StandardLabel() {
    setFont(AssistantConstants.FONT_MAP_DEFAULT);
    setLabelAlignment(PositionConstants.LEFT);
//    setBorder(new LineBorder());
  }

  public StandardLabel(final String text) {
    super();
    setFont(AssistantConstants.FONT_MAP_DEFAULT);
    setLabelAlignment(PositionConstants.LEFT);
//    setBorder(new LineBorder());
    setText(text);
  }
}

// - UNUSED CODE ............................................................................................
