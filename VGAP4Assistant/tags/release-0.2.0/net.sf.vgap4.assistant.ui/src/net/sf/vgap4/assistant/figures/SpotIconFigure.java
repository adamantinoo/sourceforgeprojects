//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ImageFigure;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.projecteditor.figures.BaseIconFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotIconFigure extends AbstractIconFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private XYLayout						xylay						= new XYLayout();
	private AbstractIconFigure	baseIcon;
	private Image								decorationImage	= Activator.getImageDescriptor("icons/SpotDecorator.GIF").createImage();
	private ImageFigure					decorator				= new ImageFigure(decorationImage);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotIconFigure(AssistantNodeFigure homeFigure) {
		super(homeFigure);
		setDrawingSize(13);
		baseIcon = new BaseIconFigure(homeFigure);
		this.setLayoutManager(xylay);
		this.add(baseIcon);
		//		decorator = new ImageFigure(decorationImage);
		decorator.setOpaque(false);
		this.add(decorator);
		relayout();
		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	protected void paintFigure(final Graphics graphics) {
	//		// - Get drawing location. This should be already displaced from the top-left.
	//		final Rectangle bound = this.getBounds().getCopy();
	//
	//		// - Draw the figure body
	//		//DEBUG I have tried only the background color for painting
	//		//		graphics.setForegroundColor(ColorConstants.darkGray);
	//		graphics.setBackgroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
	//		//		graphics.fillOval(bound);
	//		graphics.fillRoundRectangle(bound, 3, 3);
	//	}

	private void relayout() {
		// - Position ICON. Top-left corner of the figure.
		final Dimension iconicSize = new Dimension(this.drawingSize, drawingSize);
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = 0;
		elementLocation.y = 0;
		elementLocation.width = iconicSize.width;
		elementLocation.height = iconicSize.height;
		xylay.setConstraint(baseIcon, elementLocation);

		// - Position DECORATOR. Bottom-left of the icon
		elementLocation = new Rectangle();
		elementLocation.x = 0;
		elementLocation.y = iconicSize.height - 9;
		elementLocation.width = 9;
		elementLocation.height = 9;
		xylay.setConstraint(decorator, elementLocation);
	}

	private void dispose() {
		this.decorationImage.dispose();
		this.decorator = null;
		this.baseIcon = null;
	}

	public void setBaseFigure(AbstractIconFigure baseIconFigure) {
		this.removeAll();
		this.baseIcon = baseIconFigure;
		setDrawingSize(baseIconFigure.drawingSize);
		this.add(baseIcon);
		this.add(decorator);
		relayout();
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return new Dimension(this.drawingSize, drawingSize);
	}
}
// - UNUSED CODE ............................................................................................
