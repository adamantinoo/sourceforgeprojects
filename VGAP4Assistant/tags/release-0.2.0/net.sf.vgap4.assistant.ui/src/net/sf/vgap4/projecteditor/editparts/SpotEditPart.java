//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.gef.EditPolicy;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.figures.SpotFigure;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotEditPart extends VGAP4NodeEditPart implements ISelectablePart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Instead creating a simple PropertyPage this method should create ona page for each element in this
	 * location.
	 */
	public IPropertyPage createPropertyPage(Composite top, boolean singleSelected) {
		return null;
	}

	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();

		// - Update figure visuals from current model data.
		fig.setModel(model);
		fig.setCoordinates(model.getLocation());
		fig.setId(model.getIdNumber());
		fig.setName(model.getName());

		//- Get the game player code.
		//		int player = getPlayerCode();
		//		final int owner = model.getFieldNumber("owner");
		//		if (player != owner)
		//			fig.setColor(AssistantConstants.COLOR_SHIP_ENEMY);
		//		else
		//			fig.setColor(AssistantConstants.COLOR_SHIP_DEFAULT);

		super.refreshVisuals();
	}

	private Spot getCastedModel() {
		return (Spot) this.getModel();
	}
}

// - UNUSED CODE ............................................................................................
