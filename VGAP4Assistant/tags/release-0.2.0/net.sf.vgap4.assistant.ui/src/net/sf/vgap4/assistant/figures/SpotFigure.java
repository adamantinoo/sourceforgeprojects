//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.projecteditor.figures.BaseIconFigure;
import net.sf.vgap4.projecteditor.figures.PlanetIconFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotFigure extends AssistantNodeFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private Spot								spotModel;
	private AssistantNode				spotContents;
	//	private String						shapeName;
	private AssistantNodeFigure	subFigure;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotFigure() {
		//- Add the icon to represent this element.
		this.setDrawFigure(new SpotIconFigure(this));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setModel(Spot model) {
		//- Get the spot contents and process them to know what are our shape.
		this.spotModel = model;
		this.spotContents = model.getRepresentative();
		if (this.spotContents instanceof Base) {
			((SpotIconFigure) this.iconic).setBaseFigure(new BaseIconFigure(this));
			return;
		}
		if (this.spotContents instanceof Planet) {
			((SpotIconFigure) this.iconic).setBaseFigure(new PlanetIconFigure(this));
			return;
		}
		if (this.spotContents instanceof Ship) {
			((SpotIconFigure) this.iconic).setBaseFigure(new ShipIconFigure(this));
			return;
		}
	}

}
// - UNUSED CODE ............................................................................................
