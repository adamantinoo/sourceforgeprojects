//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.model.VGAP4Map;

// - CLASS IMPLEMENTATION ...................................................................................
public class Ship extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long							serialVersionUID	= -5137898479615467215L;
	private static Logger									logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private VGAP4Map											map								= null;
	/**
	 * This array stores the information processed for this planet (identified by its ID) for any turn. The data
	 * for turn x can be located inside vector slot x.
	 */
	private final Vector<ShipInformation>	shipInformation		= new Vector<ShipInformation>();
	private transient ShipInformation			latestTurnInfo		= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Ship(final VGAP4Map ownerMap) {
		map = ownerMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addTurnInformation(final int turn, final ShipInformation shipInfo) {
		// - Get the old value for this planet turn information is available.
		// If not null then we are overriding this data.
		try {
			shipInformation.setSize(turn + 1);
			if (null != shipInformation.get(turn))
				Ship.logger.warning("This operation is overriding some previous information stored for this turn");
			shipInformation.set(turn, shipInfo);
			Ship.logger.info("Adding ship '" + shipInfo.getName() + "' data for turn " + turn + ".");
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			shipInformation.setSize(turn + 1);
			shipInformation.set(turn, shipInfo);
			aioobe.printStackTrace();
		}
		// - Cache turn information.
		this.setIdNumber(shipInfo.getIdNumber());
		this.setLastTurn(Math.max(this.getLastTurn(), turn));
		this.setName(shipInformation.get(this.getLastTurn()).getName());
		this.setLocation(shipInformation.get(this.getLastTurn()).getLocation());
		Ship.logger.info("Processing ship id [" + shipInfo.getIdNumber() + "] - " + shipInfo.getName() + ". At location ["
				+ shipInfo.getLocation().x + ", " + shipInfo.getLocation().y + "]");
	}

	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	public String getCargoDetail() {
		final StringBuffer buffer = new StringBuffer("[");
		buffer.append(this.getLatestInfo().getField("Duranium")).append("D").append("/");
		buffer.append(this.getLatestInfo().getField("Tritanium")).append("T").append("/");
		buffer.append(this.getLatestInfo().getField("Molybdenum")).append("M").append("/");
		buffer.append(this.getLatestInfo().getField("Supplies")).append("S").append("/");
		buffer.append(this.getLatestInfo().getField("Food")).append("F").append("/");
		buffer.append(this.getLatestInfo().getField("Med")).append("M").append("]");
		return buffer.toString();
	}

	public ShipInformation getLatestInfo() {
		if (null == latestTurnInfo) latestTurnInfo = shipInformation.elementAt(this.getLastTurn());
		return latestTurnInfo;
	}

	public String getPassageDetail() {
		final StringBuffer buffer = new StringBuffer("[");
		buffer.append(this.getLatestInfo().getField("Colonists")).append("C").append("/");
		buffer.append(this.getLatestInfo().getField("Troops")).append("T").append("/");
		buffer.append(this.getLatestInfo().getField("HighGuard")).append("HG").append("]");
		return buffer.toString();
	}

	public String getProperty(final String property) {
		//- Get the information from the latest turn info.
		return this.getLatestInfo().getField(property);
	}

	/**
	 * Return the representation color for the ship depending on some factors like if the ship is an enemy or
	 * neutral or has or not fuel.
	 */
	public Color getRepresentationColor() {
		final int owner = this.getLatestInfo().getFieldNumber("owner");
		final int player = map.getPlayer();
		if (owner != player) return AssistantConstants.COLOR_SHIP_ENEMY;

		// TODO Auto-generated method stub
		return AssistantConstants.COLOR_SHIP_DEFAULT;
	}
}
// - UNUSED CODE ............................................................................................
