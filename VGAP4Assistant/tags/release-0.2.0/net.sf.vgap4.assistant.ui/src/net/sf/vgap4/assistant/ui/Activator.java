//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.ui;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.Iterator;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String							PLUGIN_ID								= "net.sf.vgap4.assistant.ui";
	public static final Color								COLOR_DEFAULTBASE				= new Color(Display.getCurrent(), 0x80, 0x80, 0x80);	// Dark gray
	public static final Color								COLOR_NOMINERALSPLANET	= new Color(Display.getCurrent(), 0x66, 0x66, 0xFF);	// Dark purpled blue

	// The plug-in ID
	// - G L O B A L - F I E L D S
	/** The shared instance. */
	private static Activator								plugin;
	/** Hash map where I can store and then retrieve global items. */
	//TODO This cannot be a Map because there can be more then one editor active at the same time.
	private static HashMap<Object, Object>	registry								= new HashMap<Object, Object>();

	// - G L O B A L - M E T H O D S
	public static void addReference(final Object key, final Object newReference) {
		Activator.registry.put(key, newReference);
	}

	/**
	 * Returns an element in the registry that it is identified by the unique ID. If the element is not found in
	 * the registry then an exception is thrown to be cached by any methods that will interpret this runtime
	 * class of exceptions.
	 */
	public static Object getByID(final String id) {
		final Object reference = Activator.registry.get(id);
		Assert.isNotNull(reference, "Reference in the registry is not found. This is a runtime error.");
		return reference;
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return Activator.plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path
	 * 
	 * @param path
	 *          the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(final String path) {
		return AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, path);
	}

	public static AbstractUIPlugin getPlugin() {
		return Activator.plugin;
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Activator() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		Activator.plugin = this;
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		Activator.plugin = null;
		super.stop(context);
	}

	public static Object getRegisteredEditors() {
		Iterator<Object> rit = registry.keySet().iterator();
		Object editors = null;
		while (rit.hasNext()) {
			Object reg = rit.next();
		}
		return null;
	}
}
//- UNUSED CODE ............................................................................................
