//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedShipFigure extends AbstractDetailedFigure {
  // - S T A T I C - S E C T I O N ..........................................................................
  private static Logger     logger  = Logger.getLogger("net.sf.vgap4.assistant.figures");

  // - F I E L D - S E C T I O N ............................................................................
  private final Ship        shipModel;
  private RoundedRectangle  container;

  // - C O N S T R U C T O R - S E C T I O N ................................................................
  public DetailedShipFigure(final Ship model) {
    super(model);
    shipModel = model;
  }

  // - M E T H O D - S E C T I O N ..........................................................................
  @Override
  protected void addAdditionalContents() {
    final MultiLabelLine passageLabel = new MultiLabelLine();
    passageLabel.addColumn("Passage:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
    passageLabel.addColumn("-undef-", SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
    final Label passageDetail = new AutosizeLabel(shipModel.getPassageDetail());
    final MultiLabelLine cargoLabel = new MultiLabelLine();
    cargoLabel.addColumn("Cargo:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
    cargoLabel.addColumn(shipModel.getProperty("Cash" + " MC"), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
    final Label cargoDetail = new AutosizeLabel(shipModel.getCargoDetail());

    //- Add labels to this element for composition.
    this.add(passageLabel);
    this.add(passageDetail);
    this.add(cargoLabel);
    this.add(cargoDetail);

    this.layout();
  }
}

class AutosizeLabel extends Label {

  public AutosizeLabel(final String text) {
    this.setText(text);
    this.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
  }

}

class BoldLabel extends Label {

  public BoldLabel(final String text) {
    this.setText(text);
    this.setFont(AssistantConstants.FONT_MAP_BOLD);
//    this.setBorder(new LineBorder());
  }

  public BoldLabel(final String text, final Color color) {
    this.setText(text);
    this.setForegroundColor(color);
    this.setFont(AssistantConstants.FONT_MAP_BOLD);
//    this.setBorder(new LineBorder());
  }
}

class MultiLabelLine extends Figure {
  private final GridLayout  grid    = new GridLayout();
  private final int         numCols = 0;

  public MultiLabelLine() {
    grid.horizontalSpacing = 2;
    grid.marginHeight = 0;
    grid.marginWidth = 2;
    grid.numColumns = 2;
    grid.verticalSpacing = 0;
    this.setLayoutManager(grid);
//    this.setBorder(new LineBorder());
  }

  public void addColumn(final String columnText, final int textStyle, final Color columnColor) {
    final Label column = new Label(columnText);
    column.setForegroundColor(columnColor);
    column.setFont(new Font(Display.getDefault(), "Tahoma", 8, textStyle));
    this.add(column);
    //    grid.numColumns = numCols++;
    this.layout();
  }
}
// - UNUSED CODE ............................................................................................
