//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

import es.ftgroup.gef.model.AbstractNode;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class UserNote extends AbstractNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= -1878896050552157062L;

	// - F I E L D - S E C T I O N ............................................................................
	private String						note							= "Prueba";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public UserNote() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
// - UNUSED CODE ............................................................................................
