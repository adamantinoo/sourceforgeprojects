//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.actions;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.commands.Command;

import net.sf.vgap4.assistant.models.UserNote;

// - CLASS IMPLEMENTATION ...................................................................................
public class RenameNoteCommand extends Command {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.actions");

	// - F I E L D - S E C T I O N ............................................................................
	private UserNote	source;
	private String		oldNote, note;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RenameNoteCommand() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setSource(UserNote note) {
		source = note;
	}

	public String getOldNote() {
		return oldNote;
	}

	public void setOldNote(String oldNote) {
		this.oldNote = oldNote;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public UserNote getSource() {
		return source;
	}
}

// - UNUSED CODE ............................................................................................
