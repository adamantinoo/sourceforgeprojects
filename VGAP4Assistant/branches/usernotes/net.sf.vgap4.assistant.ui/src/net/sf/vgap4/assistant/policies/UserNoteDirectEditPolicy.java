//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.policies;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Label;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.DirectEditPolicy;
import org.eclipse.gef.requests.DirectEditRequest;

import net.sf.vgap4.assistant.actions.RenameNoteCommand;
import net.sf.vgap4.assistant.models.UserNote;

// - CLASS IMPLEMENTATION ...................................................................................
public class UserNoteDirectEditPolicy extends DirectEditPolicy {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.policies");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public UserNoteDirectEditPolicy() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected Command getDirectEditCommand(DirectEditRequest request) {
		RenameNoteCommand cmd = new RenameNoteCommand();
		cmd.setSource((UserNote) getHost().getModel());
		cmd.setOldNote(((UserNote) getHost().getModel()).getNote());
		cmd.setNote((String) request.getCellEditor().getValue());
		return cmd;
	}

	@Override
	protected void showCurrentEditValue(DirectEditRequest request) {
		String value = (String) request.getCellEditor().getValue();
		((Label) getHostFigure()).setText(value);
		//hack to prevent async layout from placing the cell editor twice.
		//		getHostFigure().getUpdateManager().performUpdate();
	}
}

// - UNUSED CODE ............................................................................................
