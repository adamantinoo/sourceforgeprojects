//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.gef.EditPolicy;

import net.sf.gef.core.model.Route;
import net.sf.vgap4.assistant.models.UserNote;
import net.sf.vgap4.assistant.policies.UserNoteDirectEditPolicy;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedUserNoteEditPart extends DetailedCommonEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedUserNoteEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new UserNoteDirectEditPolicy());
		super.createEditPolicies();
	}

	@Override
	protected List<UserNote> getModelChildren() {
		return Collections.EMPTY_LIST;
	}

	@Override
	protected List<Route> getModelSourceConnections() {
		return Collections.EMPTY_LIST;
	}

	@Override
	protected List<Route> getModelTargetConnections() {
		return Collections.EMPTY_LIST;
	}

	//	@Override
	//	protected void refreshVisuals() {
	//		// - The references to the model and figure objects.
	//		final IFigure fig = this.getFigure();
	//		if (fig instanceof IDetailedFigure) ((IDetailedFigure) fig).createContents();
	//		//		super.refreshVisuals();
	//	}
}

// - UNUSED CODE ............................................................................................
