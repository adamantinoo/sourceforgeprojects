//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.parts.AbstractEditPartFactory;
import es.ftgroup.gef.parts.AbstractGenericEditPart;
import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.vgap4.assistant.editparts.DetailedCommonEditPart;
import net.sf.vgap4.assistant.editparts.DetailedContainerEditPart;
import net.sf.vgap4.assistant.editparts.DetailedUserNoteEditPart;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.DetailedDiagram;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Pod;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.UserNote;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof DetailedDiagram) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with identifier [" + "VGAP4ProjectDiagram" + "]");
			return new DetailedContainerEditPart();
		}
		//				if (modelElement instanceof Sector) {
		//					logger.info("Generating SectorEditPart for model class " + modelElement.getClass().getSimpleName()
		//							+ " with name [" + ((VGAP4Node) modelElement).getName() + "]");
		//					return new DetailedDefaultEditPart();
		//				}
		if (modelElement instanceof Planet) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with identifier [" + ((AssistantNode) modelElement).getIdentifier() + "]");
			return new DetailedCommonEditPart();
		}
		if (modelElement instanceof Base) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with identifier [" + ((AssistantNode) modelElement).getIdentifier() + "]");
			return new DetailedCommonEditPart();
		}
		if (modelElement instanceof Ship) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with identifier [" + ((AssistantNode) modelElement).getIdentifier() + "]");
			return new DetailedCommonEditPart();
		}
		if (modelElement instanceof Pod) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName()
					+ " with identifier [" + ((AssistantNode) modelElement).getIdentifier() + "]");
			return new DetailedCommonEditPart();
		}
		if (modelElement instanceof UserNote) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName());
			return new DetailedUserNoteEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}

}
// - UNUSED CODE ............................................................................................
