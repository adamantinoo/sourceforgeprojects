//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.detailed;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;

import net.sf.vgap4.assistant.figures.IDetailedFigure;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.UserNote;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedUserNoteFigure extends StandardLabel implements IDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures.detailed");

	// - F I E L D - S E C T I O N ............................................................................
	private UserNote	note;

	//	private StandardLabel noteLabel=new StandardLabel();;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedUserNoteFigure(UserNote note) {
		this.note = note;
		this.setText(note.getNote());
		this.setBorder(new LineBorder());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		setText(note.getNote());
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// TODO Auto-generated method stub
		super.paintFigure(graphics);
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		// TODO Auto-generated method stub
		return super.getPreferredSize(hint, hint2);
	}
}

// - UNUSED CODE ............................................................................................
