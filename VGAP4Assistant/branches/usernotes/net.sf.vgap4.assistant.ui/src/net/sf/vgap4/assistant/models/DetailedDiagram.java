//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import es.ftgroup.gef.model.AbstractPropertyChanger;

// - CLASS IMPLEMENTATION ...................................................................................
/** Model for the Detailed container of selected elements that have to be drawn in the DetailedView. */
public class DetailedDiagram extends AbstractPropertyChanger {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= 5181001026031627512L;

	// - F I E L D - S E C T I O N ............................................................................
	/** The list of nodes that should be displayed as the view contents. */
	Vector<AssistantNode>			children					= new Vector<AssistantNode>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add a new element to the list of model elements to be displayed.
	 * 
	 * @param child
	 *          the new element.
	 */
	public void addChild(final AssistantNode child) {
		children.add(child);
	}

	/**
	 * Add a new group of elements to the list of model elements to be displayed.
	 * 
	 * @param contents
	 *          the list of elements to be added to the list.
	 */
	public void addChild(final Vector<AssistantNode> contents) {
		children.addAll(contents);
	}

	public void clear() {
		children.clear();
	}

	/**
	 * Method used bu the EditPart to get the list of model elements that require generation of the MVC
	 * structures.
	 */
	public Vector<AssistantNode> getChildren() {
		//DEBUG Limit selection only to bases to simplify development
		//		final Vector<AssistantNode> bases = new Vector<AssistantNode>();
		//		final Iterator<AssistantNode> bit = children.iterator();
		//		while (bit.hasNext()) {
		//			final AssistantNode child = bit.next();
		//			if (child instanceof Base) bases.add(child);
		//		}
		return children;
	}

}

// - UNUSED CODE ............................................................................................
