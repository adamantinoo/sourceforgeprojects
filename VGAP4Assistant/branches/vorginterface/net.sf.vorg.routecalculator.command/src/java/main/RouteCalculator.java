//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

//- IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.app.VORGCommandApp;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.routecalculator.core.RouterType;
import net.sf.vorg.routecalculator.models.IsochroneRouter;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.Router;
import net.sf.vorg.routecalculator.models.SpeedVector;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCalculator extends VORGCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger					= Logger.getLogger("net.sf.vorg.routecalculator.command.main");
	private static final String			APPLICATIONNAME	= "RouteCalculator";
	/** Reference to the static part of the application */
	private static RouteCalculator	singleton;
	static {
		RouteCalculator.logger.setLevel(Level.OFF);
	}
	static {
		outputName = APPLICATIONNAME + ".output.txt";
	}

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation							startLocation		= null;
	private GeoLocation							endLocation			= null;
	private GeoLocation							waypoint				= null;
	private boolean									printVMC				= false;
	private boolean									printVMG				= false;
	private boolean									onlyDirect			= false;
	private int											cellsToSpan			= 5;
	private RouterType							routerType			= RouterType.NONE;
	private int											heading					= 0;
	private int											windDir					= 0;
	private double									windSpeed				= 0.0;
	private String									navigationFile;
	private double									boatSpeed;

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherited code
		// to be executed instead making a lot of calls.
		RouteCalculator.singleton = new RouteCalculator(args);
		singleton.execute();
		RouteCalculator.exit(0);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are:
	 * <ul>
	 * <li><b>-conf<font color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where
	 * the application will expect the configuration files and data.</li>
	 * <li><b>-res<font color="GREY">[ourcesLocation</font></b> ${RESOURCEDIR} - is the directory where the
	 * application is going to locate the files that contains the SQL statements and other application
	 * resources.
	 */
	public RouteCalculator(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		this.banner();

		// - Process parameters and store them into the instance fields
		this.processParameters(args, RouteCalculator.APPLICATIONNAME);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void processParameters(final String[] args, final String ApplicationName) {
		for (int i = 0; i < args.length; i++) {
			RouteCalculator.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-help")) {
					this.help();
					RouteCalculator.exit(0);
				}
				if (args[i].toLowerCase().startsWith("-sta")) { //$NON-NLS-1$
					startLocation = new GeoLocation();
					startLocation.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					startLocation.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-boat")) { //$NON-NLS-1$
					String boatName = argumentStringValue(args, i);
					i++;
					Boat boat = new Boat();
					try {
						boat.setName(boatName);
						boat.updateBoatData();
						startLocation = boat.getLocation();
						heading = boat.getHeading();
						boatSpeed = boat.getSpeed();
					} catch (DataLoadingException dle) {
						System.out.println(dle.getMessage());
						exit(VORGConstants.GENERICERROR);
					} catch (BoatNotFoundException bnfe) {
						System.out.println(bnfe.getMessage());
						exit(VORGConstants.GENERICERROR);
					}
					continue;
				}
				if (args[i].toLowerCase().startsWith("-nav")) { //$NON-NLS-1$
					navigationFile = argumentStringValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-end")) { //$NON-NLS-1$
					endLocation = new GeoLocation();
					endLocation.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					endLocation.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-way")) { //$NON-NLS-1$
					waypoint = new GeoLocation();
					waypoint.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					waypoint.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-cell")) { //$NON-NLS-1$
					cellsToSpan = this.argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
					onDebug = true;
					logger.setLevel(Level.ALL);
					continue;
				}
				if (args[i].toLowerCase().startsWith("-VMC")) { //$NON-NLS-1$
					heading = this.argumentIntegerValue(args, i);
					i++;
					// if (null == startLocation) {
					// System.out.println("Start location not specified in the right order. -start before -VMC.");
					// RouteCalculator.exit(1);
					// }
					printVMC = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-VMG")) { //$NON-NLS-1$
					heading = this.argumentIntegerValue(args, i);
					i++;
					windDir = this.argumentIntegerValue(args, i);
					i++;
					windSpeed = this.argumentDoubleValue(args, i);
					i++;
					printVMG = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-polar")) { //$NON-NLS-1$
					final int apparent = this.argumentIntegerValue(args, i);
					i++;
					final double windSpeed = this.argumentDoubleValue(args, i);
					i++;
					final SailConfiguration conf = Polars.lookup(apparent, windSpeed);
					final StringBuffer buffer = new StringBuffer("[Polar search results").append('\n');
					buffer.append("Sail=").append(conf.getSail()).append(VORGConstants.NEWLINE);
					buffer.append("Speed=").append(conf.getSpeed()).append(VORGConstants.NEWLINE);
					output(buffer.toString());
					RouteCalculator.exit(0);
				}
				if (args[i].toLowerCase().startsWith("-direct")) { //$NON-NLS-1$
					onlyDirect = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-deep")) { //$NON-NLS-1$
					int deepLevel = argumentIntegerValue(args, i);
					i++;
					Route.setDeepLevel(deepLevel);
					continue;
				}
				if (args[i].toLowerCase().startsWith("-router")) {
					// - Get the router type. Can be DIRECT or FILE or ISO.
					routerType = RouterType.encode(this.argumentStringValue(args, i));
					i++;
					continue;
				}
			}
		}
		// ... Check that required parameters have values.
		// if ((null == startLocation) || (null == endLocation)) RouteCalculator.exit(VORGConstants.NOCONFIG);
	}

	private void help() {
		System.out.println("Command API for the RouteCalcualtor:");
		System.out.println("java -classpath routecalculator.jar net.sf.vorg.routecalculator.command.main.RouteCalculator ");
		System.out.println("Allowed parameters:");
		System.out.println("-sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-way[point] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println();
		System.out.println("Allowed commands");
		System.out.println();
		System.out.println("Allowed toggles");
		System.out.println("-cells   -- number of levels to process in the VMG routing. Not used.");
		// System.out.println("-wind   -- if present then consider wind shift cell for splitting and recalculation.");
		System.out.println("-debug  -- if defined opens the ouput for verbose debugging information.");
		System.out.println();
		System.out.println("Allowed commands");
		System.out
				.println("-VMC <direction> -- calculates the VMC for the cell that contains the -start location over the parameter direction.");
		System.out
				.println("-VMG <direction> <winddir> <windspeed>  -- calculates the VMC for the parameter values. Does not need a cell location.");
		System.out
				.println("-polar <AWD> <windspeed>  -- calculates the exact polars for this AWD on the selected wind speed.");
		System.out
				.println("-router  -- optimizes a route. The next parameters specify the route and the optimizer operation.");
		System.out.println("    <type>  -- if there is a direct route of a loaded one.");
		// System.out.println("    <deep>  -- number of iterations to process between 10 and 60.");
		// System.out.println("    <iterations> -- number of thousands of iterations to descend a deep level.");
		System.out.println();
	}

	/** Runs the optimizer or the route calculator depending on the specified parameters. */
	public void execute() {
		if (printVMC) {
			if (null != startLocation) {
				WindCell cell;
				try {
					cell = WindMapHandler.getWindCell(startLocation);
					final VMCData vmc = new VMCData(heading, cell.getWindDir(), cell.getWindSpeed());
					output(vmc.printReport());
				} catch (LocationNotInMap e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (printVMG) {
			final VMCData vmc = new VMCData(heading, windDir, windSpeed);
			output(vmc.printReport());
		}
		if (routerType == RouterType.ISO) {
			if (null != startLocation) {
				IsochroneRouter theRouter = new IsochroneRouter(routerType);
				SpeedVector vector = new SpeedVector(startLocation, heading, boatSpeed);
				theRouter.setOutputFile(this.navigationFile);
				theRouter.generateIsochrone(vector);
			}
		}
		if (routerType == RouterType.DIRECT) {
			if ((null != startLocation) && (null != endLocation)) {
				Router theRouter = new Router(routerType);
				Route directRoute = theRouter.generateRoute(startLocation, endLocation);
				output("Direct Route");
				output(directRoute.printReport());
				output();
			}
		}
		if (routerType == RouterType.OPTIMDIRECT) {
			if ((null != startLocation) && (null != endLocation)) {
				Router theRouter = new Router(routerType);
				theRouter.optimizeRoute(startLocation, endLocation);
			}
		}
	}

	private void banner() {
		System.out.println(" ____             _        ____      _            _       _             ");
		System.out.println("|  _ \\ ___  _   _| |_ ___ / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
		System.out.println("| |_) / _ \\| | | | __/ _ \\ |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
		System.out.println("|  _ < (_) | |_| | ||  __/ |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
		System.out.println("|_| \\_\\___/ \\__,_|\\__\\___|\\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
		System.out.println();
		System.out.println(VORGConstants.VERSION);
		System.out.println();
	}

	// public static boolean onlyDirect() {
	// return onlyDirect;
	// }
}
// - UNUSED CODE ............................................................................................
