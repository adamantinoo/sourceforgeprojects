//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Locale;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Quadrants;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.routecalculator.internals.AbstractRouteCell;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * A RouteCell defines the geographical part of a route that compresses a single wind cell. Contains the
 * reference points and the wind cell that will be used at the time the cell is crossed. And performs all the
 * mathematical calculations needed to time the route and manage the angles.<br>
 * This type of cell only controls the calculations when the route leg fits completely on a single wind cell.
 * If there is a wind change during the time used to cross the cell, it will be replaced at the ProxyCell by
 * another version for multi-wind cells.
 */
public class RouteCell extends AbstractRouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	// public static double R = 3963.0;
	// - F I E L D - S E C T I O N ............................................................................
	/** The wind cell contained on this route cell. Contains the wind information and location. */
	protected WindCell		cell;
	/** Geographical location when entering the cell. */
	protected GeoLocation	entryLocation;
	/** Geographical location when exiting the cell. */
	protected GeoLocation	exitLocation;
	/** Difference between the exit and entry latitudes. */
	private double				deltaLat;
	/** Difference between the exit and entry longitudes. */
	private double				deltaLon;
	/** The distance is measured in grades so it is not exact. Needs conversion to miles */
	private double				distance;
	protected int					alpha;
	private Quadrants			quadrant;
	private int						apparentHeading;
	/** Time To Cross the cell. The elapsed time to move from the entry to the exit at the current speed. */
	private double				ttc;
	/** The speed reached depending on the wind angle, wind direction and heading. */
	private double				speed;
	/** The recommended sail to reach the speed. */
	private Object				sail;
	private double				loxDistance;
	private RouteControl	leftControl;
	private RouteControl	rightControl;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteCell() {
	}

	/** Load the cell data with the required wind information and the entry and exit locations. */
	public RouteCell(final WindCell windCell, final GeoLocation entry, final GeoLocation exit) {
		cell = windCell;
		entryLocation = entry;
		exitLocation = exit;
		// - Perform the calculations for all the other cell parameters.
		this.calculateCell();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int calcApparent() {
		int angle = Math.abs(cell.getWindDir() - alpha);
		if (angle > 180) angle = angle - 360;
		return Math.abs(angle);
	}

	@Override
	public void optimize() {
	}

	public void calculateCell() {
		// - Calculate time to traverse this cell.
		deltaLat = exitLocation.getLat() - entryLocation.getLat();
		deltaLon = exitLocation.getLon() - entryLocation.getLon();
		// - If destination is the same as the origin the TTC is ZERO.
		if ((deltaLat == 0.0) && (deltaLon == 0.0)) {
			alpha = 0;
			quadrant = Quadrants.QUADRANT_I;
			distance = 0.0;
			loxDistance = 0.0;
			ttc = 0.0;
			return;
		}

		distance = Math.hypot(deltaLat, deltaLon);
		double Ls = Math.toRadians(entryLocation.getLat());
		double Ld = Math.toRadians(exitLocation.getLat());
		double ldelta = Math.toRadians(deltaLon);
		loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		alpha = GeoLocation.adjustAngleTo360(new Long(Math.round(entryLocation.angleTo(exitLocation))).intValue());
		quadrant = Quadrants.q4Angle(alpha);
		apparentHeading = this.calcApparent();

		// - Get the sail type and the speed from the Polars.
		final SailConfiguration configuration = Polars.lookup(apparentHeading, cell.getWindSpeed());
		speed = configuration.getSpeed();
		sail = configuration.getSail();
		if (0.0 == speed)
			ttc = Double.POSITIVE_INFINITY;
		else
			ttc = loxDistance / speed;
	}

	public int getAlpha() {
		return alpha;
	}

	public int getApparent() {
		return apparentHeading;
	}

	@Override
	public WindCell getCell() {
		return cell;
	}

	public double getDistance() {
		return loxDistance;
		// return distance * 60.0;
	}

	@Override
	public GeoLocation getEntryLocation() {
		return entryLocation;
	}

	@Override
	public GeoLocation getExitLocation() {
		return exitLocation;
	}

	public double getSpeed() {
		return speed;
	}

	@Override
	public double getTTC() {
		return ttc;
	}

	public int getWindDirection() {
		return cell.getWindDir();
	}

	public double getWindSpeed() {
		return cell.getWindSpeed();
	}

	@Override
	public String printReport(final int waypointId) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("W").append(waypointId).append('\t');
		buffer.append(exitLocation.toReport()).append('\t');
		buffer.append(exitLocation.getLat()).append('\t');
		buffer.append(exitLocation.getLon()).append('\t');
		buffer.append(alpha).append('\t');
		buffer.append(speed).append('\t');
		buffer.append(sail).append('\t');
		buffer.append(deltaLat).append('\t');
		buffer.append(deltaLon).append('\t');
		buffer.append(loxDistance).append('\t');
		if (ttc == Double.POSITIVE_INFINITY)
			buffer.append(100).append('\t');
		else
			buffer.append(ttc).append('\t');
		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir()).append('\t');
		buffer.append(cell.getTimeStamp());
		return buffer.toString();
	}

	@Override
	public String printStartReport() {
		final StringBuffer buffer = new StringBuffer("START").append('\t');
		buffer.append(entryLocation.toReport()).append("\t");
		buffer.append(entryLocation.getLat()).append('\t');
		buffer.append(entryLocation.getLon()).append('\t');

		buffer.append("\t\t\t\t\t\t\t");

		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir()).append('\t');
		buffer.append(cell.getTimeStamp());

		// buffer.append("\t\t");
		return buffer.toString();
	}

	@Override
	public void setEntryLocation(final GeoLocation entryLocation) {
		this.entryLocation = entryLocation;
		this.calculateCell();
	}

	@Override
	public void setExitLocation(final GeoLocation exitLocation) {
		this.exitLocation = exitLocation;
		this.calculateCell();
	}

	@Override
	public void setWindData(final WindCell windData) {
		cell = windData;
		this.calculateCell();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(VORGConstants.NEWLINE).append("[RouteCell ");
		buffer.append("cell=").append(cell).append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("alpha=").append(alpha).append(",");
		buffer.append("distance=").append(loxDistance).append(",");
		buffer.append("speed=").append(speed).append(",");
		buffer.append("ttc=").append(ttc).append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("entrylocation=").append(entryLocation)
				.append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("exitlocation=").append(exitLocation)
				.append("");
		buffer.append("]");
		return buffer.toString();
	}

	public boolean updateWindCell(final WindCell newCell) {
		if ((newCell.getWindDir() == cell.getWindDir()) && (newCell.getWindSpeed() == cell.getWindSpeed()))
			return false;
		else {
			cell = newCell;
			return true;
		}
	}

	@Override
	public String vrtoolReport(int index) {
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(5);
		nf.setMinimumFractionDigits(5);
		final StringBuffer buffer = new StringBuffer("P; ");
		buffer.append(nf.format(this.entryLocation.getLat())).append(";");
		buffer.append(nf.format(this.entryLocation.getLon() * -1.0)).append(";");
		if (index == 0)
			buffer.append("START").append(VORGConstants.NEWLINE);
		else
			buffer.append("W").append(index).append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	public RouteControl getLeftControl() {
		return leftControl;
	}

	public RouteControl getRightControl() {
		return rightControl;
	}

	public void registerControlLeft(RouteControl control) {
		leftControl = control;
	}

	public void registerControlRight(RouteControl control) {
		rightControl = control;
	}
}

// - CLASS IMPLEMENTATION ...................................................................................
class RouterFormatter {

	// - M E T H O D - S E C T I O N ..........................................................................
	public static String rounded(final double data, final int round) {
		final long target = Math.round(Math.pow(data, round));
		return Double.toString(target / Math.pow(10.0, round));
	}

	public static String toHMS(final double data) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(Math.floor(data)).append("H ");
		buffer.append(data - Math.floor(data)).append("M");
		return buffer.toString();
	}

}
// - UNUSED CODE ............................................................................................
