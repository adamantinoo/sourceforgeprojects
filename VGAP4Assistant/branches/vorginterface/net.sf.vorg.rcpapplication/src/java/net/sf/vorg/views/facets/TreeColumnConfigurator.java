//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views.facets;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Image;

import net.sf.vorg.vorgautopilot.models.ITreeFacet;

// - CLASS IMPLEMENTATION ...................................................................................
public class TreeColumnConfigurator {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger			= Logger.getLogger("net.sf.vorg.views.facets");

	// - F I E L D - S E C T I O N ............................................................................
	private final int							columnId;
	private final String					title;
	private final String					toolTip;
	private final int							preferredWidth;
	//	private final boolean							hasImage;
	private AbstractTextAccesor		textGetter	= null;
	private AbstractImageAccesor	imageGetter	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TreeColumnConfigurator(final int columnCode, final String columnTitle, final String tooltipText,
			final int width, final AbstractTextAccesor textAccessor) {
		columnId = columnCode;
		title = columnTitle;
		toolTip = tooltipText;
		preferredWidth = width;
		//		hasImage = b;
		textGetter = textAccessor;
	}

	public TreeColumnConfigurator(final int columnCode, final String columnTitle, final String tooltipText,
			final int width, final AbstractTextAccesor textAccessor, final AbstractImageAccesor imageAccessor) {
		//		super();
		this(columnCode, columnTitle, tooltipText, width, textAccessor);
		imageGetter = imageAccessor;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getColumnId() {
		return columnId;
	}

	public String getColumnText(final ITreeFacet node) {
		if (null != textGetter)
			return textGetter.getText(node);
		else
			return "N/A";
	}

	public Image getImage(final ITreeFacet node) {
		if (null != imageGetter)
			return imageGetter.getImage(node);
		else
			return null;
	}

	public int getPreferredWidth() {
		return preferredWidth;
	}

	public String getTitle() {
		return title;
	}

	public String getToolTip() {
		return toolTip;
	}

}
// - UNUSED CODE ............................................................................................
