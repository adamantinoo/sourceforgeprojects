//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.app.Activator;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class UIPilotModelStore extends PilotModelStore {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.models");
	private static final long		serialVersionUID	= -2252280374360378912L;
	public static final String	PILOTMODELID			= "net.sf.vorg.models.PilotModelStore.id";

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public UIPilotModelStore() {
		super();
		Activator.addReference(UIPilotModelStore.PILOTMODELID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean runningUI() {
		return true;
	}
}

// - UNUSED CODE ............................................................................................
