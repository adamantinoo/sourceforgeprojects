//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedEditPartFactory.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.gef.core.editparts.AbstractEditPartFactory;
import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.vorg.editparts.ActiveWaypointEditPart;
import net.sf.vorg.editparts.BoatEditPart;
import net.sf.vorg.editparts.PilotBoatEditPart;
import net.sf.vorg.editparts.PilotCommandEditPart;
import net.sf.vorg.editparts.PilotModelContainerEditPart;
import net.sf.vorg.editparts.WaypointEditPart;
import net.sf.vorg.vorgautopilot.models.ActiveWaypoint;
import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof PilotModelStore) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ "VORGDiagram" + "]");
			return new PilotModelContainerEditPart();
		}
		if (modelElement instanceof PilotBoat) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((PilotBoat) modelElement).getBoatName() + "]");
			return new PilotBoatEditPart();
		}
		if (modelElement instanceof Boat) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((Boat) modelElement).getName() + "]");
			return new BoatEditPart();
		}
		if (modelElement instanceof PilotCommand) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "]");
			return new PilotCommandEditPart();
		}
		if (modelElement instanceof ActiveWaypoint) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "]");
			return new ActiveWaypointEditPart();
		}
		if (modelElement instanceof Waypoint) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + "]");
			return new WaypointEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}

}
// - UNUSED CODE ............................................................................................
