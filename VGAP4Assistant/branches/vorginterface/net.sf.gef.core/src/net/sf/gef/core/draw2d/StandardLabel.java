//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: StandardLabel.java 147 2008-05-12 15:57:30Z boneymen $
//  LAST UPDATE:    $Date: 2008-05-12 17:57:30 +0200 (lun, 12 may 2008) $
//  RELEASE:        $Revision: 147 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;

// - CLASS IMPLEMENTATION ...................................................................................
public class StandardLabel extends Label {
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public StandardLabel() {
		this.setFont(Draw2DConstants.FONT_STANDARDLABEL_DEFAULT);
		this.setLabelAlignment(PositionConstants.LEFT);
	}

	public StandardLabel(final String text) {
		this();
		this.setText(text);
	}

	public StandardLabel(final String identifier, final Image iconImage) {
		this(identifier);
		this.setIcon(iconImage);
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public StandardLabel(final String text, final int bold) {
		this();
		this.setText(text);
		if (SWT.BOLD == bold) {
			this.setFont(Draw2DConstants.FONT_STANDARDLABEL_BOLD);
		}
	}
}

// - UNUSED CODE ............................................................................................
