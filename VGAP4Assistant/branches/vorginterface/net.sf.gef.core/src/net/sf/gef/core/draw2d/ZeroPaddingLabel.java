//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ZeroPaddingLabel.java 98 2008-04-09 17:03:00Z boneymen $
//  LAST UPDATE:    $Date: 2008-04-09 19:03:00 +0200 (mié, 09 abr 2008) $
//  RELEASE:        $Revision: 98 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class ZeroPaddingLabel extends Label {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ZeroPaddingLabel(final String text) {
		setFont(Draw2DConstants.FONT_STANDARDLABEL_DEFAULT);
		setLabelAlignment(PositionConstants.LEFT);
		setText(text);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void setText(final String newText) {
		//- Zero pad the text to the specified number of spaces.
		super.setText(newText);
		//		final Dimension size = new Dimension((newText.length() + 0) * CHAR_WIDTH + 2, CHAR_HEIGHT);
		//		this.setSize(size);
		//		this.setPreferredSize(size);
	}
}
// - UNUSED CODE ............................................................................................
