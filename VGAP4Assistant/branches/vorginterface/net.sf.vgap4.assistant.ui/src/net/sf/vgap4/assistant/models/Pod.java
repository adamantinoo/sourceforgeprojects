//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

import net.sf.vgap4.assistant.models.facets.ITreeFacet;
import net.sf.vgap4.assistant.models.facets.ThingTreeFacet;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class Pod extends Thing {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= 2212474651914470112L;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Pod(final AssistantMap map) {
		super(map);
	}

	/** Return an string with the cargo contents of this Pod along with the Pod type. */
	public String getCargoMessage() {
		//FIXME Auto-generated method stub
		return "Pod type";
	}

	public ITreeFacet getTreeFacet() {
		return new ThingTreeFacet(this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[Pod:");
		buffer.append(this.getIdNumber()).append("/");
		buffer.append(this.getLocationString()).append("/");
		buffer.append(this.getFFStatus()).append("/");
		buffer.append("]");
		return '\n' + "[Pod:" + super.toString() + "]";
	}
}

// - UNUSED CODE ............................................................................................
