//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.NativeShape;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Pod;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.models.Thing;
import net.sf.vgap4.assistant.models.Wing;
import net.sf.vgap4.assistant.models.database.RaceDatabase;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class draws the different icons for Map representation based on the contents of the Spot received as
 * the generation command. It will take on account for all Spot contents to fill each one of the decorator
 * slots with the aggregated data.<br>
 * There are 8 slots for decorators. The contents depending on the location are described on the next list:
 * <ul>
 * <li><b>N</b> - High level of contraband present.</li>
 * <li><b>NE</b> - High/Low level of minerals present.</li>
 * <li><b>E</b> - Ships in orbit around planet or base.</li>
 * <li><b>SE</b> - Pods in orbit or attached to a in orbit ship.</li>
 * <li><b>S</b> - Wings in orbit or docked to an orbiting Ship.</li>
 * <li><b>W</b> - Natives present on the Planet's surface.</li>
 * <li><b>NW</b> - Indicator of the farming level. Low-Acceptable-High from race viewpoint.</li>
 * </ul>
 */
public class IconGenerator /* extends IconImageFactory */{
	// - S T A T I C - S E C T I O N ..........................................................................
	public static String										SHAPE_PLANET	= "PLANET";
	public static String										SHAPE_SHIP		= "SHIP";
	public static String										SHAPE_POD			= "CAPSULE";
	public static String										SHAPE_WING		= "WING";
	public static final String							SHAPE_NATIVE	= "NATIVE";
	private static Logger										logger				= Logger.getLogger("net.sf.vgap4.assistant.factories");
	private static Hashtable<String, Image>	iconCache			= new Hashtable<String, Image>();
	private static final String							SHAPE_BASE		= "BASE";
	//	private static final Color							COLOR_MULTIPLEBASES	= AssistantConstants.COLOR_MEDIUM_YELLOW;
	private static final byte								MASK_FOE			= 001;
	private static final byte								MASK_FRIEND		= 002;
	private static final byte								MASK_NEUTRAL	= 004;
	private static Vector<Color>						colorFFMap		= new Vector<Color>(8);
	static {
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_UNDEFINED);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FOE);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FRIEND);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FOEFRIEND);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_NEUTRAL);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FOENEUTRAL);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FRIENDNEUTRAL);
		IconGenerator.colorFFMap.add(AssistantConstants.COLOR_FF_FOEFRIENDNEUTRAL);
	}

	public static Image generateImage(final AssistantNode target, final Color backColor) {
		//BUG The color does not propagate to the instance 
		IconGenerator imageFactory = new IconGenerator();
		imageFactory.setBackColor(backColor);
		if (target instanceof Spot) {
			//- Spots have a more complex generation because it has to detect all elements present on the location
			imageFactory = IconGenerator.generateSpotImage((Spot) target);
		} else {
			//- Compose the icon for a simple graphical element.
			imageFactory = IconGenerator.generateUnitImage(target);
		}
		//- Get the codification code assigned to this image.
		imageFactory.setBackColor(backColor);
		final String code = imageFactory.getGenerationCode();
		//- Search for this code in the cache. If not found then generate the new Image.
		Image cacheHit = IconGenerator.iconCache.get(code);
		cacheHit = null;
		if (null == cacheHit)
			return imageFactory.drawImage(code);
		else
			return cacheHit;
	}

	//[02]
	private static IconGenerator generateSpotImage(final Spot target) {
		//TODO Generate the complex icon with the sum off all spot elements.
		//TODO Detect the type of the representative as an stating point
		final AssistantNode representative = target.getRepresentative();
		final IconGenerator imageFactory = IconGenerator.generateUnitImage(representative);

		//TODO Add the rest of the data to the base image generator.
		final Iterator<AssistantNode> eit = target.getContents().iterator();
		while (eit.hasNext()) {
			final AssistantNode element = eit.next();
			if (representative == element) continue;
			//TODO Process the element to add decorators to the current image.
			if (element instanceof Base) {
				imageFactory.activateBase();
				imageFactory.setEnemyMainStatus(target.getFFStatus());
				//				imageFactory.addBaseModifier(element.getFFStatus());
			}
			if (element instanceof Planet) {
				imageFactory.setPlanetClassification(((Planet) element).getPlanetClassification());
				imageFactory.setPlanetContrabandLevel(((Planet) element).getContrabandLevel());
				imageFactory.setPlanetFarmLevel(((Planet) element).getFarmingLevel());
				if (((Planet) element).hasNatives()) imageFactory.activateNatives();
			}
			if (element instanceof Ship) {
				imageFactory.addShipModifier(element.getFFStatus());
			}
			if (element instanceof Pod) {
				imageFactory.addPodModifier(element.getFFStatus());
			}
			if (element instanceof Wing) {
				imageFactory.addWingModifier(element.getFFStatus());
			}
		}
		return imageFactory;
	}

	private static IconGenerator generateUnitImage(final AssistantNode target) {
		final IconGenerator imageFactory = new IconGenerator();
		//- Compose the icon for a simple graphical element.
		if (target instanceof Base) {
			imageFactory.setShape(IconGenerator.SHAPE_BASE);
			imageFactory.activateBase();
			//			imageFactory.addBaseModifier(target.getFFStatus());
			imageFactory.setEnemyMainStatus(target.getFFStatus());
			final Planet onPlanet = ((Base) target).getOnPlanet();
			imageFactory.setPlanetClassification(onPlanet.getPlanetClassification());
			imageFactory.setPlanetContrabandLevel(onPlanet.getContrabandLevel());
			imageFactory.setPlanetFarmLevel(onPlanet.getFarmingLevel());
			if (onPlanet.hasNatives()) imageFactory.activateNatives();
		}
		if (target instanceof Planet) {
			imageFactory.setShape(IconGenerator.SHAPE_PLANET);
			imageFactory.setInfoLevel(((Planet) target).infoLevel());
			imageFactory.setPlanetClassification(((Planet) target).getPlanetClassification());
			imageFactory.setPlanetContrabandLevel(((Planet) target).getContrabandLevel());
			imageFactory.setPlanetFarmLevel(((Planet) target).getFarmingLevel());
			if (((Planet) target).hasNatives()) imageFactory.activateNatives();
		}
		if (target instanceof Ship) {
			imageFactory.setShape(IconGenerator.SHAPE_SHIP);
			imageFactory.setEnemyMainStatus(target.getFFStatus());
			//TODO Check for pods or wings docked to this ship. This can be obtained from the Ships data.
		}
		if (target instanceof Pod) {
			imageFactory.setShape(IconGenerator.SHAPE_POD);
			imageFactory.setEnemyMainStatus(target.getFFStatus());
		}
		if (target instanceof Wing) {
			imageFactory.setShape(IconGenerator.SHAPE_WING);
			imageFactory.setEnemyMainStatus(target.getFFStatus());
		}
		if (target instanceof Thing) {
			//TODO Check the type of thing to process.
			if (((Thing) target).getDataType().equals(AssistantConstants.MODELTYPE_WING)) {
				imageFactory.setShape(IconGenerator.SHAPE_WING);
				imageFactory.setEnemyMainStatus(target.getFFStatus());
			}
		}
		if (target instanceof NativeShape) {
			//TODO Check the type of thing to process.
			//			if (((Thing) target).getDataType().equals(AssistantConstants.MODELTYPE_WING)) {
			imageFactory.setShape(IconGenerator.SHAPE_NATIVE);
			//				imageFactory.setEnemyMainStatus(target.getFFStatus());
			//			}
		}
		return imageFactory;
	}

	// - F I E L D - S E C T I O N ............................................................................
	private Color		backgroundColor				= ColorConstants.white;
	private String	shapeName							= "-";
	private boolean	hasBase								= false;
	private byte		shapeDecoratorCode		= 000;
	private String	infoLevel							= "-";
	private String	planetClassification	= "-";
	private String	contrabandLevel				= Planet.CONTRABAND_LOW;
	private String	planetFarmLevel				= RaceDatabase.FARMLEVEL_COLD;
	private boolean	hasNatives						= false;
	private byte		shipDecoratorCode			= 000;
	private byte		podDecoratorCode			= 000;
	private byte		wingDecoratorCode			= 000;
	private boolean	shipsInOrbit					= false;
	private boolean	podsInOrbit						= false;
	private boolean	wingsInOrbit					= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	protected void activateBase() {
		hasBase = true;
	}

	protected void activateNatives() {
		hasNatives = true;
	}

	protected void addPodModifier(final String status) {
		podsInOrbit = true;
		if (AssistantConstants.FFSTATUS_FOE.equals(status))
			podDecoratorCode = (byte) (podDecoratorCode | IconGenerator.MASK_FOE);
		if (AssistantConstants.FFSTATUS_FRIEND.equals(status))
			podDecoratorCode = (byte) (podDecoratorCode | IconGenerator.MASK_FRIEND);
		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(status))
			podDecoratorCode = (byte) (podDecoratorCode | IconGenerator.MASK_NEUTRAL);
	}

	protected void addShipModifier(final String status) {
		shipsInOrbit = true;
		if (AssistantConstants.FFSTATUS_FOE.equals(status))
			shipDecoratorCode = (byte) (shipDecoratorCode | IconGenerator.MASK_FOE);
		if (AssistantConstants.FFSTATUS_FRIEND.equals(status))
			shipDecoratorCode = (byte) (shipDecoratorCode | IconGenerator.MASK_FRIEND);
		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(status))
			shipDecoratorCode = (byte) (shipDecoratorCode | IconGenerator.MASK_NEUTRAL);
	}

	protected void addWingModifier(final String status) {
		wingsInOrbit = true;
		if (AssistantConstants.FFSTATUS_FOE.equals(status))
			wingDecoratorCode = (byte) (wingDecoratorCode | IconGenerator.MASK_FOE);
		if (AssistantConstants.FFSTATUS_FRIEND.equals(status))
			wingDecoratorCode = (byte) (wingDecoratorCode | IconGenerator.MASK_FRIEND);
		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(status))
			wingDecoratorCode = (byte) (wingDecoratorCode | IconGenerator.MASK_NEUTRAL);
	}

	//[03]
	protected void setBackColor(final Color backColor) {
		backgroundColor = backColor;
	}

	protected void setEnemyMainStatus(final String status) {
		if (AssistantConstants.FFSTATUS_FRIEND.equals(status))
			shapeDecoratorCode = (byte) (shapeDecoratorCode | IconGenerator.MASK_FRIEND);
		if (AssistantConstants.FFSTATUS_FOE.equals(status))
			shapeDecoratorCode = (byte) (shapeDecoratorCode | IconGenerator.MASK_FOE);
		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(status))
			shapeDecoratorCode = (byte) (shapeDecoratorCode | IconGenerator.MASK_NEUTRAL);
	}

	protected void setInfoLevel(final String level) {
		infoLevel = level;
	}

	protected void setPlanetClassification(final String planetClassification) {
		this.planetClassification = planetClassification;
	}

	protected void setPlanetContrabandLevel(final String contrabandLevel) {
		this.contrabandLevel = contrabandLevel;
	}

	protected void setPlanetFarmLevel(final String farmLevel) {
		planetFarmLevel = farmLevel;
	}

	protected void setShape(final String shape) {
		shapeName = shape;
	}

	private char booleanDecode(final boolean code, final char template) {
		if (code)
			return template;
		else
			return '-';
	}

	private String contrabandDecode(final String code) {
		if (Planet.CONTRABAND_HIGH.equals(code))
			return "C";
		else
			return "-";
	}

	private Color decodeFFColor(final byte code) {
		return IconGenerator.colorFFMap.get(code);
	}

	private Image drawImage(final String code) {
		final Image image = new Image(Display.getDefault(), AssistantConstants.SIZE_ICONIMAGE,
				AssistantConstants.SIZE_ICONIMAGE);
		//		ImageData imageData = image.getImageData();
		//		imageData.transparentPixel=-1;
		final GC gc = new GC(image);

		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = new Rectangle(0, 0, AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);
		//TODO Fill the background with the corresponding color that depends on the icon final location
		gc.setBackground(backgroundColor);
		gc.fillRectangle(bound);

		//- Draw the base ring if we have a base on the location.
		if (hasBase) {
			gc.setForeground(AssistantConstants.COLOR_BASE_RING);
			gc.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw a planet with the correct Base color and then the Base circle.
		if (IconGenerator.SHAPE_BASE.equals(shapeName)) {
			//- Get the color depending if the base is Friend, Foe or has multiple bases.
			if (shapeDecoratorCode > 0) gc.setBackground(this.decodeFFColor(shapeDecoratorCode));
			//			if (000 == baseDecoratorCode) gc.setBackground(AssistantConstants.COLOR_BASE_OWNED);
			//			if (multipleBases) gc.setBackground(IconGenerator.COLOR_MULTIPLEBASES);
			//			if (mainEnemyStatus) gc.setBackground(AssistantConstants.COLOR_BASE_ENEMY);
			this.drawPlanet(gc);

			//- Draw decorators for the planet under the base
			this.drawPlanetClassification(gc);
			this.drawPlanetContraband(gc);
			this.drawPlanetFarming(gc);
			this.drawPlanetNatives(gc);
		}
		//- Draw the Planet icon and its decorators
		if (IconGenerator.SHAPE_PLANET.equals(shapeName)) {
			//- Get the color depending on the information level we have for the Planet.
			if (AssistantConstants.INFOLEVEL_UNEXPLORED.equals(infoLevel))
				gc.setBackground(AssistantConstants.COLOR_PLANET_UNEXPLORED);
			if (AssistantConstants.INFOLEVEL_NOINFO.equals(infoLevel))
				gc.setBackground(AssistantConstants.COLOR_PLANET_NOINFO);
			if (AssistantConstants.INFOLEVEL_INFO.equals(infoLevel)) gc.setBackground(AssistantConstants.COLOR_PLANET_INFO);
			if (AssistantConstants.INFOLEVEL_OBSOLETE.equals(infoLevel))
				gc.setBackground(AssistantConstants.COLOR_PLANET_OBSOLETE);
			//			if (AssistantConstants.INFOLEVEL_NATIVES.equals(infoLevel))
			//				gc.setBackground(AssistantConstants.COLOR_NATIVES_DEFAULT);
			this.drawPlanet(gc);
			if (AssistantConstants.INFOLEVEL_INFO.equals(infoLevel)) {
				this.drawPlanetClassification(gc);
				this.drawPlanetContraband(gc);
				this.drawPlanetFarming(gc);
			}
			this.drawPlanetNatives(gc);
		}
		if (IconGenerator.SHAPE_NATIVE.equals(shapeName)) {
			gc.setBackground(AssistantConstants.COLOR_PLANET_NATIVES);
			this.drawPlanet(gc);
		}
		if (IconGenerator.SHAPE_SHIP.equals(shapeName)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + 3, 0 + (bound.y + 15 / 2 - 0));
			//			final Point head = new Point(bound.x + 3, 11);
			final Point p2 = new Point(head.x + 10 - 1, -1 + head.y - 4);
			final Point p3 = new Point(head.x + 10 - 1, 1 + head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			//			gc.setBackground(AssistantConstants.COLOR_BASE_OWNED);
			gc.setBackground(this.decodeFFColor(shapeDecoratorCode));
			//			if (mainEnemyStatus) gc.setBackground(AssistantConstants.COLOR_BASE_ENEMY);
			gc.fillPolygon(triangle.toIntArray());
		}
		if (IconGenerator.SHAPE_POD.equals(shapeName)) {
			//- Draw the Planet circle
			//			gc.setBackground(AssistantConstants.COLOR_BASE_OWNED);
			gc.setBackground(this.decodeFFColor(shapeDecoratorCode));
			//			if (mainEnemyStatus) gc.setBackground(AssistantConstants.COLOR_BASE_ENEMY);
			Rectangle planetBounds = new Rectangle(bound.x + 2, bound.y + 2, 7, 7);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
			planetBounds = new Rectangle(bound.x + 7, bound.y + 7, 7, 7);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
		}
		if (IconGenerator.SHAPE_WING.equals(shapeName)) {
			//- Draw the Ship shape.
			final PointList triangle1 = new PointList(3);
			Point head = new Point(5, 0);
			Point p2 = new Point(5 - 3, 9);
			Point p3 = new Point(5 + 3, 9);
			triangle1.addPoint(head);
			triangle1.addPoint(p2);
			triangle1.addPoint(p3);

			final PointList triangle2 = new PointList(3);
			head = new Point(10, 15 - 9);
			p2 = new Point(10 - 3, 15);
			p3 = new Point(10 + 3, 15);
			triangle2.addPoint(head);
			triangle2.addPoint(p2);
			triangle2.addPoint(p3);
			gc.setBackground(this.decodeFFColor(shapeDecoratorCode));
			//			gc.setBackground(AssistantConstants.COLOR_BASE_OWNED);
			//			if (mainEnemyStatus) gc.setBackground(AssistantConstants.COLOR_BASE_ENEMY);
			gc.fillPolygon(triangle1.toIntArray());
			gc.fillPolygon(triangle2.toIntArray());
		}

		//- Draw other decorators for ships in orbit or pods or wings.
		if (shipsInOrbit) {
			gc.setBackground(this.decodeFFColor(shipDecoratorCode));
			gc.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podsInOrbit) {
			gc.setBackground(this.decodeFFColor(podDecoratorCode));
			gc.fillOval(bound.x + 10, bound.y + 10, 4, 4);
		}
		if (wingsInOrbit) {
			gc.setBackground(this.decodeFFColor(wingDecoratorCode));
			gc.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}
		//		if (hasNatives) {
		//			gc.setBackground(AssistantConstants.COLOR_NATIVES_DEFAULT);
		//			gc.fillOval(bound.x + 1, bound.y + 1, 4, 4);
		//		}
		//		drawShipsDecorator();
		//		drawPodsDecorator();
		//		drawWingsDecorator();
		//		drawNativesDecorator();
		gc.dispose();

		//		String genCode = this.getGenerationCode();
		IconGenerator.logger.info("Registering in cache Icon code: " + code);
		IconGenerator.iconCache.put(code, image);
		return image;
	}

	private void drawPlanet(final GC gc) {
		final Rectangle bound = new Rectangle(0, 0, AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);
		final Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
		gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
	}

	//[01]

	private void drawPlanetClassification(final GC gc) {
		//- Draw the Planet classification
		if (planetClassification.equals("A")) {
			gc.setForeground(AssistantConstants.COLOR_PLANET_CLASS_A);
			gc.drawLine(15, 0, 15 - 3, 3);
			gc.drawLine(15 - 3, 3, 15 - 1, 3);
			gc.drawLine(15 - 3, 3, 15 - 3, 1);
		}
		if (planetClassification.equals("C")) {
			gc.setForeground(AssistantConstants.COLOR_PLANET_CLASS_C);
			gc.drawLine(15, 0, 15 - 3, 3);
			gc.drawLine(15 - 3, 0, 15, 3);
		}
	}

	private void drawPlanetContraband(final GC gc) {
		final Rectangle bound = new Rectangle(0, 0, AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);
		if (contrabandLevel.equals(Planet.CONTRABAND_HIGH)) {
			gc.setBackground(AssistantConstants.COLOR_CONTRABAND_DECORATOR);
			gc.fillOval(bound.x + 6, bound.y + 0, 4, 4);
		}
	}

	private void drawPlanetFarming(final GC gc) {
		final Rectangle bound = new Rectangle(0, 0, AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);
		gc.setBackground(AssistantConstants.COLOR_FARMING_NORMAL);
		if (planetFarmLevel.equals(RaceDatabase.FARMLEVEL_COLD)) gc.setBackground(AssistantConstants.COLOR_FARMING_COLD);
		if (planetFarmLevel.equals(RaceDatabase.FARMLEVEL_HOT)) gc.setBackground(AssistantConstants.COLOR_FARMING_HOT);
		gc.fillOval(bound.x + 1, bound.y + 1, 4, 4);
	}

	private void drawPlanetNatives(final GC gc) {
		if (hasNatives) {
			final Rectangle bound = new Rectangle(0, 0, AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);
			gc.setBackground(AssistantConstants.COLOR_NATIVES_DECORATOR);
			gc.fillOval(bound.x + 0, bound.y + 6, 4, 4);
		}
	}

	private String farmingDecode(final String code) {
		if (RaceDatabase.FARMLEVEL_HOT.equals(code)) return "H";
		if (RaceDatabase.FARMLEVEL_COLD.equals(code)) return "C";
		if (RaceDatabase.FARMLEVEL_INRANGE.equals(code)) return "-";
		return "-";
	}

	private String getGenerationCode() {
		final StringBuffer code = new StringBuffer();
		code.append(shapeName.charAt(0));
		code.append(shapeDecoratorCode);
		if (IconGenerator.SHAPE_PLANET.equals(shapeName)) {
			if (AssistantConstants.INFOLEVEL_INFO.equals(infoLevel)) {
				code.append(this.infoLevelDecode(infoLevel));
				code.append(this.contrabandDecode(contrabandLevel));
				code.append(planetClassification.charAt(0));
				code.append(this.farmingDecode(planetFarmLevel));
			}
		}
		//		code.append(this.infoLevelDecode(infoLevel));
		//		code.append(this.contrabandDecode(contrabandLevel));
		//		code.append(planetClassification.charAt(0));
		code.append(shipDecoratorCode);
		code.append(podDecoratorCode);
		code.append(wingDecoratorCode);
		code.append(this.booleanDecode(hasNatives, 'N'));
		//		code.append(this.farmingDecode(planetFarmLevel));
		return code.toString();
	}

	private String infoLevelDecode(final String level) {
		if (AssistantConstants.INFOLEVEL_INFO.equals(level)) return "I";
		if (AssistantConstants.INFOLEVEL_NOINFO.equals(level)) return "-";
		if (AssistantConstants.INFOLEVEL_UNEXPLORED.equals(level)) return "X";
		if (AssistantConstants.INFOLEVEL_OBSOLETE.equals(level)) return "O";
		return ("-");
	}
}

// - UNUSED CODE ............................................................................................
//[01]
//	@Override
//	public void activateNatives() {
//		this.hasNatives = true;
//	}
//	@Override
//	public void setPlanetClassification(String planetClassification) {
//		this.planetClassification=planetClassification;
//	}

//[02]
//	public static Image generateImage(final AssistantNode target) {
//		IconGenerator imageFactory = new IconGenerator();
//		if (target instanceof Spot) {
//			//- Spots have a more complex generation because it has to detect all elements present on the location
//			imageFactory = IconGenerator.generateSpotImage((Spot) target);
//		} else {
//			//- Compose the icon for a simple graphical element.
//			imageFactory = IconGenerator.generateUnitImage(target);
//		}
//		//- Get the codification code assigned to this image.
//		final String code = imageFactory.getGenerationCode();
//		//- Search for this code in the cache. If not found then generate the new Image.
//		final Image cacheHit = IconGenerator.iconCache.get(code);
//		if (null == cacheHit)
//			return imageFactory.generateImage(code);
//		else
//			return cacheHit;
//	}
//	public static Image generateImage(final AssistantNode target, final String backgroundType) {
//		IconGenerator imageFactory = new IconGenerator();
//		imageFactory.setBackground(backgroundType);
//		if (target instanceof Spot) {
//			//- Spots have a more complex generation because it has to detect all elements present on the location
//			imageFactory = IconGenerator.generateSpotImage((Spot) target);
//		} else {
//			//- Compose the icon for a simple graphical element.
//			imageFactory = IconGenerator.generateUnitImage(target);
//		}
//		//- Get the codification code assigned to this image.
//		final String code = imageFactory.getGenerationCode();
//		//- Search for this code in the cache. If not found then generate the new Image.
//		final Image cacheHit = IconGenerator.iconCache.get(code);
//		if (null == cacheHit)
//			return imageFactory.generateImage(code);
//		else
//			return cacheHit;
//	}

//[03]
//	private void addBaseModifier(final String status) {
//		if (AssistantConstants.FFSTATUS_FOE.equals(status))
//			baseDecoratorCode = (byte) (baseDecoratorCode | IconGenerator.MASK_FOE);
//		if (AssistantConstants.FFSTATUS_FRIEND.equals(status))
//			baseDecoratorCode = (byte) (baseDecoratorCode | IconGenerator.MASK_FOE);
//		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(status))
//			baseDecoratorCode = (byte) (baseDecoratorCode | IconGenerator.MASK_FOE);
//	}
//	private void setFarmingLevel(String farmingLevel) {
//		this.planetFarmLevel=farmingLevel;
//	}
//
//	private void setContrabandLevel(String contrabandLevel) {
//		this.contrabandLevel=contrabandLevel;
//	}
//	private void drawShipsDecorator() {
//		// TODO Auto-generated method stub
//
//	}
//	private String FFDecode(final String code) {
//		if (AssistantConstants.FFSTATUS_FOE.equals(code)) return "E";
//		if (AssistantConstants.FFSTATUS_FRIEND.equals(code)) return "F";
//		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(code)) return "N";
//		if (AssistantConstants.FFSTATUS_UNDEFINED.equals(code)) return "U";
//		return "U";
//	}
//	private void setBackground(final String backClass) {
//		if (AssistantConstants.COLOR_UNDEFINED_BACKGROUND.equals(backClass))
//			backgroundColor = AssistantConstants.COLOR_UNDEFINED_BACKGROUND;
//	}
//	private void setEnemyMainStatus(final boolean status) {
//		mainEnemyStatus = status;
//	}