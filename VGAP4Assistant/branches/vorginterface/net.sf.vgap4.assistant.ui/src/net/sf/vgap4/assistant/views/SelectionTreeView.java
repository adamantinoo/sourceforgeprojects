//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.views;

// - IMPORT SECTION .........................................................................................
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import net.sf.vgap4.assistant.factories.IconGenerator;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.factories.LetterImageFactory;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.models.facets.BaseTreeFacet;
import net.sf.vgap4.assistant.models.facets.ITreeFacet;
import net.sf.vgap4.assistant.models.facets.PlanetTreeFacet;
import net.sf.vgap4.assistant.models.facets.ShipTreeFacet;
import net.sf.vgap4.assistant.models.facets.SpotTreeFacet;
import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.editparts.ISelectablePart;
import net.sf.vgap4.projecteditor.editparts.SpotEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This sample class demonstrates how to plug-in a new workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly, but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace). The view is connected to the model using
 * a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be presented in the view. Each view can
 * present the same model objects using different labels and icons, if needed. Alternatively, a single label
 * provider can be shared between views in order to ensure that objects of the same type are presented in the
 * same way everywhere.
 * <p>
 */
public class SelectionTreeView extends ViewPart implements ISelectionListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String								ID											= "net.sf.vgap4.assistant.views.SelectionTreeView.id";
	public static final String								TREEVIEWER_ID						= SelectionTreeView.ID + "SelectionTreeProvider";
	public static final int										NAME_COLUMN							= 0;
	public static final int										LOCATION_COLUMN					= 1;
	public static final int										CLASS_COLUMN						= 2;
	private static final int									RESOURCES_COLUMN				= 2;
	private static Logger											logger									= Logger.getLogger("net.sf.vgap4.assistant.views");
	private static Vector<ColumnConfigurator>	treeColumnsConfigurator	= new Vector<ColumnConfigurator>();
	static {
		//- NAME Column 1.
		SelectionTreeView.logger.info("Adding 'Name' configuration to Tree View columns.");
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(SelectionTreeView.NAME_COLUMN, "Name",
				"ID - name", 180, true, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						return facet.getName();
					}
				}, new ImageAccessor() {
					@Override
					public Image getImage(final ITreeFacet facet) {
						return IconGenerator.generateImage(facet.getDelegate(), AssistantConstants.COLOR_BACKGROUND_VIEW);
					}
				}));
		//- LOCATION Column 2.
		SelectionTreeView.logger.info("Adding 'Location' configuration to Tree View columns.");
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(SelectionTreeView.LOCATION_COLUMN, "Location",
				"x coord - y coord", 80, false, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						return facet.getLocationString();
					}
				}));
		//- RACE Column 3.
		SelectionTreeView.logger.info("Adding 'Race' configuration to Tree View columns.");
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(3, "Race", "Race ID - Name of the race", 120,
				true, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ITreeFacet)
							return facet.getRace();
						//						if (facet instanceof BaseTreeFacet) return ((BaseTreeFacet) facet).getRace();
						//						if (facet instanceof PlanetTreeFacet) return ((PlanetTreeFacet) facet).getRace();
						else
							return "---";
					}
				}, new ImageAccessor() {
					@Override
					public Image getImage(final ITreeFacet facet) {
						if (facet instanceof SpotTreeFacet) {
							String FFCode = ((SpotTreeFacet) facet).getFFCode();
							if (FFCode.equals(AssistantConstants.FFSTATUS_FOE))
								return ImageFactory.getImage("icons/FOERaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_FRIEND))
								return ImageFactory.getImage("icons/FRIENDRaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_UNDEFINED))
								return ImageFactory.getImage("icons/UNDEFINEDRaceIcon.gif");
						}
						if (facet instanceof BaseTreeFacet) {
							String FFCode = ((BaseTreeFacet) facet).getFFCode();
							if (FFCode.equals(AssistantConstants.FFSTATUS_FOE))
								return ImageFactory.getImage("icons/FOERaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_FRIEND))
								return ImageFactory.getImage("icons/FRIENDRaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_UNDEFINED))
								return ImageFactory.getImage("icons/UNDEFINEDRaceIcon.gif");
						}
						if (facet instanceof ShipTreeFacet) {
							String FFCode = ((ShipTreeFacet) facet).getFFCode();
							if (FFCode.equals(AssistantConstants.FFSTATUS_FOE))
								return ImageFactory.getImage("icons/FOERaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_FRIEND))
								return ImageFactory.getImage("icons/FRIENDRaceIcon.gif");
							if (FFCode.equals(AssistantConstants.FFSTATUS_UNDEFINED))
								return ImageFactory.getImage("icons/UNDEFINEDRaceIcon.gif");
						}
						return null;
					}
				}));
		//- RESOURCES / CARGO Column 4
		SelectionTreeView.logger.info("Adding 'Info Resource' configuration to Tree View columns.");
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(SelectionTreeView.RESOURCES_COLUMN,
				"Info Resource", "Mineral resources / Ship cargo", 200, true, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						final StringBuffer buffer = new StringBuffer();
						if (facet instanceof SpotTreeFacet) return "N/A";
						if (facet instanceof BaseTreeFacet) return ((BaseTreeFacet) facet).getResources();
						if (facet instanceof PlanetTreeFacet) return ((PlanetTreeFacet) facet).getResources();
						if (facet instanceof ShipTreeFacet) return ((ShipTreeFacet) facet).getCargoMessage();
						//						if (facet instanceof Pod) {
						//							buffer.append(((Pod) facet).getCargoMessage());
						//						}
						return buffer.toString();
					}
				}, new ImageAccessor() {
					@Override
					public Image getImage(final ITreeFacet facet) {
						if (facet instanceof SpotTreeFacet) return null;
						final LetterImageFactory factory = new LetterImageFactory();
						factory.setInitial("N");
						return factory.getImage();
					}
				}));
		//- INFOAREA1 Columns 5.
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(3, "Info Area 1",
				"Generic information area 1", 110, true, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof BaseTreeFacet) return ((BaseTreeFacet) facet).getFarming();
						if (facet instanceof PlanetTreeFacet) return ((PlanetTreeFacet) facet).getFarming();
						return "--";
					}
				}));
		SelectionTreeView.logger.info("Adding 'Info Area 1' configuration to Tree View columns.");
		//- INFOAREA2 Columns 6.
		SelectionTreeView.treeColumnsConfigurator.add(new ColumnConfigurator(3, "Info Area 2",
				"Generic information area 2", 110, true, new TextAccessor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof BaseTreeFacet) return ((BaseTreeFacet) facet).getFarming();
						if (facet instanceof PlanetTreeFacet) return ((PlanetTreeFacet) facet).getFarming();
						return "--";
					}
				}));
		SelectionTreeView.logger.info("Adding 'Info Area 2' configuration to Tree View columns.");
	}

	public static ColumnConfigurator getColumnConfiguration(final int index) {
		return SelectionTreeView.treeColumnsConfigurator.get(index);
	}

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Table structure to present the contents. Those contents are generated by the
	 * <code>ViewTreeContentProvider</code>.
	 */
	private TreeViewer							viewer;
	//	/** Menu actions that are available on this viewer. */
	//	private Action										action1;
	//	private Action										action2;
	//	private Action										doubleClickAction;

	/**
	 * This class provides the data contents for the viewer. This is installed in a MDC pattern where this is
	 * the Model part. This class is feeded from the editor selection where all model information is extracted
	 * and stored for access from the Controller that is implemented by the <code>TableViewer</code>.
	 */
	private ViewTreeContentProvider	contentProvider;
	/**
	 * This class is called to draw each cell contents. It will receive small pieces of the model and is
	 * responsible to map the columns to the different attributes of the model.
	 */
	private ViewTreeLabelProvider		labelProvider;

	//	/**
	//	 * Stores the viewer column elements. It is only required it we like to change the column properties after
	//	 * their creation.
	//	 */
	//	private final Vector<TreeColumn>					columns									= new Vector<TreeColumn>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SelectionTreeView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(SelectionTreeView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		contentProvider = new ViewTreeContentProvider();
		labelProvider = new ViewTreeLabelProvider();

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(labelProvider);
		//TODO Add sorters to allow classification for any of the columns
		//		viewer.setSorter(new NameSorter());
		viewer.setInput(this.getViewSite());

		//- Configure the viewer columns
		this.configureTreeColumns(viewer.getTree());

		//- Configure the view menus and actions.
		//		this.makeActions();
		//		this.hookContextMenu();
		//		this.hookDoubleClickAction();
		//		this.contributeToActionBars();

		//- Register this as a selection provider.
		Activator.addReference(SelectionTreeView.TREEVIEWER_ID, viewer);
	}

	@Override
	public void dispose() {
		// - Remove this from the selection provider.
		final ISelectionService globalSelectionService = this.getSite().getWorkbenchWindow().getSelectionService();
		globalSelectionService.removeSelectionListener(this);
		super.dispose();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		super.init(site);

		// - Add this editor selection listener to the list of listeners of this window
		final ISelectionService globalSelectionService = this.getSite().getWorkbenchWindow().getSelectionService();
		globalSelectionService.addSelectionListener(this);
	}

	/**
	 * This event is fired any time the selection is modified. <br>
	 * The parameters received are the new selection list on the <code>selection</code> parameter and the
	 * <code>EditorPart</code> that points to the originating <i>Editor</i> on the <code>part</code>
	 * parameter.<br>
	 * <br>
	 * From this selection I may extract the selectable elements (the ones that are going to move to the
	 * selection views) and the model information for each of the selected items. Filtering must be
	 * parameterizable thought the user interface.<br>
	 * <br>
	 * TODO This release only supports the selection of part elements up to the level of
	 * <code>AbstractGenericEditPart</code>. When routes or other object appear on the Map it has to be
	 * changed to support them.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(final IWorkbenchPart editorPart, final ISelection selection) {
		if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
			final Vector<ISelectablePart> models = new Vector<ISelectablePart>(2);
			final Iterator<Object> sit = ((StructuredSelection) selection).iterator();
			while (sit.hasNext()) {
				final Object part = sit.next();
				//TODO Apply all the filters that are defined on the list.
				//TODO Filter the EditParts that match the proper interface
				//- Check that the selected object has the right interface before trying to get its model.
				if (part instanceof ISelectablePart) models.add((ISelectablePart) part);
			}
			if (!models.isEmpty()) {
				contentProvider.setModelContents(models);
				viewer.refresh(true);
			}
		}
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	//[01]

	private void configureTreeColumns(final Tree targetTree) {
		//TODO Column configuration is got from a fixed metadata that is defined inside the code.
		//		int columnCount = treeColumnsConfigurator.size();
		TreeColumn column = null;
		final Iterator<ColumnConfigurator> cit = SelectionTreeView.treeColumnsConfigurator.iterator();
		while (cit.hasNext()) {
			final ColumnConfigurator configurator = cit.next();
			column = new TreeColumn(targetTree, SWT.NONE);
			column.setWidth(configurator.getPreferredWidth());
			column.setText(configurator.getTitle());
			column.setToolTipText(configurator.getToolTip());
			//		columns.add(column);

			//		column = new TreeColumn(viewer.getTree(), SWT.NONE);
			//		column.setWidth(110);
			//		column.setText("Location");
			//		columns.add(column);
			//
			//		column = new TreeColumn(viewer.getTree(), SWT.NONE);
			//		column.setWidth(200);
			//		column.setText("Class");
			//		column.setToolTipText("Resource contents");
			//		columns.add(column);
		}
		viewer.getTree().setLayout(new FillLayout());
		viewer.getTree().setLinesVisible(true);
		viewer.getTree().setHeaderVisible(true);
	}
}

class ColumnConfigurator {

	private final int						columnId;
	private final String				title;
	private final String				toolTip;
	private final int						preferredWidth;
	private final boolean				hasImage;
	private final TextAccessor	textGetter;
	private ImageAccessor				imageGetter;

	public ColumnConfigurator(final int nameColumn, final String columnTitle, final String string2, final int width,
			final boolean b, final TextAccessor textAccessor) {
		columnId = nameColumn;
		title = columnTitle;
		toolTip = string2;
		preferredWidth = width;
		hasImage = b;
		textGetter = textAccessor;
	}

	public ColumnConfigurator(final int nameColumn, final String string, final String string2, final int i,
			final boolean b, final TextAccessor textAccessor, final ImageAccessor imageAccessor) {
		//		super();
		this(nameColumn, string, string2, i, b, textAccessor);
		imageGetter = imageAccessor;
	}

	public int getColumnId() {
		return columnId;
	}

	public String getColumnText(final ITreeFacet node) {
		if (null != textGetter)
			return textGetter.getText(node);
		else
			return "N/A";
	}

	public Image getImage(final ITreeFacet node) {
		if (null != imageGetter)
			return imageGetter.getImage(node);
		else
			return null;
	}

	public int getPreferredWidth() {
		return preferredWidth;
	}

	public String getTitle() {
		return title;
	}

	public String getToolTip() {
		return toolTip;
	}

}

abstract class ImageAccessor {
	public Image getImage(final ITreeFacet node) {
		return null;
	}
}

abstract class TextAccessor {
	public String getText(final ITreeFacet node) {
		return "";
	}
}

class TypeSorter extends ViewerSorter {
}

/*
 * The content provider will have at the top level a list of selected Spots from the Map. Each Spot may or may
 * not have children and all then share the same parent that is an abstract element. The content provider
 * class is responsible for providing objects to the view. It can wrap existing objects in adapters or simply
 * return objects as-is. These objects may be sensitive to the current input of the view, or ignore it and
 * always show the same content (like Task List, for example).
 */
class ViewTreeContentProvider implements ITreeContentProvider {
	private final Vector<Spot>										selectionContents	= new Vector<Spot>(3);
	private final Hashtable<AssistantNode, Spot>	backParents				= new Hashtable<AssistantNode, Spot>(6);

	public void dispose() {
	}

	/**
	 * Returns the contents of an spot as the second level of the tree. Only valid for spots that have more than
	 * a single element. In case of simple elements return an empty array.<br>
	 * While generating the list of children, we have to store a back reference for then to point to their
	 * corresponding parent Spot.
	 */
	public Object[] getChildren(final Object parentElement) {
		if (parentElement instanceof Spot) {
			final Vector<AssistantNode> nodes = ((Spot) parentElement).getContents();
			if (nodes.size() < 2) return Collections.EMPTY_LIST.toArray();
			//- Copy the parent references into the back parent pointer table.
			final Iterator<AssistantNode> nit = nodes.iterator();
			while (nit.hasNext())
				backParents.put(nit.next(), (Spot) parentElement);
			return nodes.toArray();
		}
		return Collections.EMPTY_LIST.toArray();
	}

	public Object[] getElements(final Object parent) {
		return selectionContents.toArray();
	}

	public Object getParent(final Object element) {
		if (element instanceof Spot)
			return null;
		else
			return backParents.get(element);
	}

	public boolean hasChildren(final Object target) {
		if (target instanceof Spot) {
			final Vector<AssistantNode> nodes = ((Spot) target).getContents();
			if (nodes.size() > 1)
				return true;
			else
				return false;
		} else
			return false;
	}

	public void inputChanged(final Viewer v, final Object oldInput, final Object newInput) {
	}

	/**
	 * Extract the model elements from the selection EditParts and compose the list of Spots that make the
	 * Viewer selection.
	 */
	public void setModelContents(final Vector<ISelectablePart> selection) {
		selectionContents.clear();
		backParents.clear();
		final Iterator<ISelectablePart> sit = selection.iterator();
		while (sit.hasNext()) {
			final ISelectablePart selectedPart = sit.next();
			if (selectedPart instanceof SpotEditPart) {
				//				Spot spotModel = (Spot) ((SpotEditPart) selectedPart).getModel();
				final Spot spotModel = ((SpotEditPart) selectedPart).getCastedModel();
				selectionContents.add(spotModel);
			}
		}
	}
}

/* This class can forward the calls to the model if this implements the ITableLabelProvider interface */
class ViewTreeLabelProvider extends LabelProvider implements ITableLabelProvider {

	/** Return the model icon only if the column is set to column 0 that is the key column. */
	public Image getColumnImage(final Object target, final int index) {
		final ColumnConfigurator configurator = SelectionTreeView.getColumnConfiguration(index);
		return configurator.getImage(((AssistantNode) target).getTreeFacet());
	}

	public String getColumnText(final Object target, final int index) {
		final ColumnConfigurator configurator = SelectionTreeView.getColumnConfiguration(index);
		if (target instanceof SpotTreeFacet)
			return configurator.getColumnText(((Spot) target).getRepresentative().getTreeFacet());
		else
			return configurator.getColumnText(((AssistantNode) target).getTreeFacet());
		//		ITreeFacet facet = null;
		//		if (part instanceof SpotTreeFacet) {
		//			//			Spot baseModel = (Spot) part;
		//			facet = ((Spot) ((SpotTreeFacet) part).getDelegate()).getRepresentative();
		//		} else
		//			facet = (AssistantNode) part;
		//		if (part instanceof Base) {
		//			model = (AssistantNode) part;
		//		}
		//		if (part instanceof Planet) {
		//			model = (AssistantNode) part;
		//		}
		//		if (part instanceof Ship) {
		//			model = (AssistantNode) part;
		//		}
		//		String text = 
		//		return text;
		//		switch (index) {
		//			case SelectionTreeView.NAME_COLUMN:
		//				return model.getName();
		//			case SelectionTreeView.LOCATION_COLUMN:
		//				return model.getLocationString();
		//			case SelectionTreeView.CLASS_COLUMN:
		//				String clasInfo = "N/A";
		//				if (part instanceof Planet) {
		//					final Planet planet = (Planet) part;
		//					clasInfo = "N" + planet.getTotalNatives() + "/" + "M" + planet.getMaxMinerals() / 1000 + "k";
		//				}
		//				if (part instanceof SpotEditPart) { return "SPOT"; }
		//				if (part instanceof Base) {
		//					//TODO Compose the mineral information quantities
		//					StringBuffer buffer = new StringBuffer();
		//					buffer.append(" N:").append(((Base) part).getFieldNumber("ele N")).append("/").append(
		//							((Base) part).getFieldNumber("Ore N"));
		//					buffer.append(" D:").append(((Base) part).getFieldNumber("ele D")).append("/").append(
		//							((Base) part).getFieldNumber("Ore D"));
		//					buffer.append(" T:").append(((Base) part).getFieldNumber("ele T")).append("/").append(
		//							((Base) part).getFieldNumber("Ore T"));
		//					buffer.append(" M:").append(((Base) part).getFieldNumber("ele M")).append("/").append(
		//							((Base) part).getFieldNumber("Ore M"));
		//					clasInfo = buffer.toString();
		//				}
		//				return clasInfo;
		//			case 3:
		//				String lifeInfo = "N/A";
		//				if (part instanceof Base) {
		//					final Base base = (Base) part;
		//					lifeInfo = "M" + base.getMineralTotal() / 1000 + "k/N" + base.getTotalNatives() + "k/C"
		//							+ base.getProperty("Colonist");
		//				}
		//				return lifeInfo;
		//			default:
		//				return model.toString();
		//		}
		//		return part.toString();
	}

	@Override
	public Image getImage(final Object obj) {
		return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}

	@Override
	public String getText(final Object element) {
		return element.toString();
	}
}
//- UNUSED CODE ............................................................................................
//[01]
//private void contributeToActionBars() {
//	final IActionBars bars = this.getViewSite().getActionBars();
//	this.fillLocalPullDown(bars.getMenuManager());
//	this.fillLocalToolBar(bars.getToolBarManager());
//}
//
//private void fillContextMenu(final IMenuManager manager) {
//	manager.add(action1);
//	manager.add(action2);
//	// Other plug-ins can contribute there actions here
//	manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
//}
//
//private void fillLocalPullDown(final IMenuManager manager) {
//	manager.add(action1);
//	manager.add(new Separator());
//	manager.add(action2);
//}
//
//private void fillLocalToolBar(final IToolBarManager manager) {
//	manager.add(action1);
//	manager.add(action2);
//}
//
//private void hookContextMenu() {
//	final MenuManager menuMgr = new MenuManager("#PopupMenu");
//	menuMgr.setRemoveAllWhenShown(true);
//	menuMgr.addMenuListener(new IMenuListener() {
//		public void menuAboutToShow(final IMenuManager manager) {
//			SelectionTreeView.this.fillContextMenu(manager);
//		}
//	});
//	final Menu menu = menuMgr.createContextMenu(viewer.getControl());
//	viewer.getControl().setMenu(menu);
//	this.getSite().registerContextMenu(menuMgr, viewer);
//}
//
//private void hookDoubleClickAction() {
//	viewer.addDoubleClickListener(new IDoubleClickListener() {
//		public void doubleClick(final DoubleClickEvent event) {
//			doubleClickAction.run();
//		}
//	});
//}
//
//private void makeActions() {
//	action1 = new Action() {
//		@Override
//		public void run() {
//			SelectionTreeView.this.showMessage("Action 1 executed");
//		}
//	};
//	action1.setText("Action 1");
//	action1.setToolTipText("Action 1 tooltip");
//	action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
//			ISharedImages.IMG_OBJS_INFO_TSK));
//
//	action2 = new Action() {
//		@Override
//		public void run() {
//			SelectionTreeView.this.showMessage("Action 2 executed");
//		}
//	};
//	action2.setText("Action 2");
//	action2.setToolTipText("Action 2 tooltip");
//	action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
//			ISharedImages.IMG_OBJS_INFO_TSK));
//	doubleClickAction = new Action() {
//		@Override
//		public void run() {
//			final ISelection selection = viewer.getSelection();
//			final Object obj = ((IStructuredSelection) selection).getFirstElement();
//			SelectionTreeView.this.showMessage("Double-click detected on " + obj.toString());
//		}
//	};
//}
//private void showMessage(final String message) {
//	MessageDialog.openInformation(viewer.getControl().getShell(), "Sample View", message);
//}
