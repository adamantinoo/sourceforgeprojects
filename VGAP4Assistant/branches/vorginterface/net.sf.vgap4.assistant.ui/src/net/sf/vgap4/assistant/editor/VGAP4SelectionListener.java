//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.
package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;


import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.EditorPart;

import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.gef.core.views.ISynchronizableView;
import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.assistant.views.SelectionPropertiesView;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This is the selection listener used to maintain the selection made on the editor reflected on the
 * <i>Selection Data</i> view. The methods receive a selection from the editor and it will filter the
 * selection to just the selectable items that may be reflected on the view. <br>
 * There are no filtering operations on this release, sensor information has been removed from the
 * <i>Controller</i> and enemy units can be selected and show identification information.
 */
public class VGAP4SelectionListener implements ISelectionListener {
	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the editor instance that contains the model and the viewer. From this editor instance one
	 * can access any <i>Model</i>, <i>View</i> or <i>Controller</i> element on the scenery.
	 */
	private final EditorPart		editor;
	/**
	 * Cached reference to the selection view where selection changes should be synchronized with this <i>Editor</i>
	 * selection.<br>
	 * TODO This should be changed to the listener pattern to allow more that one view to receive selection
	 * information.
	 */
	private ISynchronizableView	selectionView;
	/**
	 * List of filters that may be applied to the selection when the information is passed to the listening
	 * views. The user may configure this list to avoid selecting planets that are banned ot that do not meet
	 * some information or resources levels.
	 */
	private Hashtable						filters	= new Hashtable();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * Creates a new <i>Selection Listener</i> and receives the editor reference to keep it for selection
	 * backtracking. This allows to update the editor actions depending on the selection.
	 */
	public VGAP4SelectionListener(EditorPart mapViever) {
		this.editor = mapViever;
	}

	// - P U B L I C S E C T I O N
	/**
	 * This event is fired any time the selection is modified. <br>
	 * The parameters received are the new selection list on the <code>selection</code> parameter and the
	 * <code>EditorPart</code> that points to the originating <i>Editor</i> on the <code>part</code>
	 * parameter.<br>
	 * <br>
	 * From this selection I may extract the selectable elements (the ones that are going to move to the
	 * selection views) and the model information for each of the selected items. Filtering must be
	 * parameterizable thought the user interface.<br>
	 * <br>
	 * TODO This release only supports the selection of part elements up to the level of
	 * <code>AbstractGenericEditPart</code>. When routes or other object appear on the Map it has to be
	 * changed to support them.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart editorPart, ISelection selection) {
		if (!selection.isEmpty()) {
			if (selection instanceof StructuredSelection) {
				//TODO Apply all the filters that are defined on the list.
				//TODO Extract the model information from the selection and compose a new list to be distributed to the views.
				final StructuredSelection sel = (StructuredSelection) selection;
				Iterator<Object> sit = sel.iterator();
				Vector<Object> models = new Vector<Object>();
				while (sit.hasNext()) {
					Object part = sit.next();
					//- Check that the selected object has the right interface before trying to get its model.
					if (part instanceof AbstractGenericEditPart) {
						//- Extract the model and put in on the resulting list.
						Object model = ((AbstractGenericEditPart) part).getModel();
						models.add(model);
					}
				}
				//TODO If the resulting list is not empty, send this list to the listeners (or directly to the views).
				if (!models.isEmpty()) {
					getSelectionView().receiveSelection(models);

				}
			}
		}
	}

	// - P R I V A T E - S E C T I O N
	/**
	 * Accesses the <code>SelectionPropertiesView.ID</code> in the registry and stores a copy in the reference
	 * cache. Selections received will be sent to this view for synchronization and presentation of additional
	 * selection data.<br>
	 * If the view is not located in the registry it is assumed that is not open and this method proceeds to
	 * open it.
	 * 
	 * @return the cached reference to the found View.
	 */
	private ISynchronizableView getSelectionView() {
		if (null == selectionView) {
			selectionView = (ISynchronizableView) Activator.getByID(SelectionPropertiesView.ID);
		}
		return selectionView;
	}
}
//
//class SensorPart extends AbstractGraphicalEditPart {
//
//	@Override
//	protected IFigure createFigure() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected void createEditPolicies() {
//		// TODO Auto-generated method stub
//
//	}
//}

// - UNUSED CODE ............................................................................................
// [01]
// // TODO Review this implementation and optimize the generation of a new selection. It is possible that
// // better performance can be obtained by filtering this at the next call level.
// // StructuredSelection sel = (StructuredSelection) selection;
// // StructuredSelection filteredSel = new StructuredSelection();
// StructuredSelection filteredSel2 = new StructuredSelection();
// final Object[] filteredSel = new AbstractGraphicalEditPart[((StructuredSelection) selection).size()];
// // // int size = sel.size();
// if (!selection.isEmpty()) {
// final Iterator<AbstractGraphicalEditPart> it = ((StructuredSelection) selection).iterator();
// // selectionContent = new Hashtable();
// int index = 0;
// while (it.hasNext()) {
// final AbstractGraphicalEditPart element = it.next();
// if (element instanceof SensorPart)
// continue;
// else
// filteredSel[index++] = element; // - Copy to the new selection to pass away.
//
// // Unit model = ((Unit) element.getModel());
// // String name = model.toString();
// // // - Add the selected element to the selection
// // selectionContent.put(name, element);
// // // int index = 1;
// }
// filteredSel2 = new StructuredSelection(filteredSel);
//
// }
// if (part instanceof SceneryEditor) {
// final SceneryEditor sceneryEditor = (SceneryEditor) part;
// // sceneryEditor.updateActions(sceneryEditor.getEditActions());
// }
