//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $iD: ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.projecteditor.policies.VGAP4XYLayoutEditPolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class DiagramEditPart extends AbstractGenericEditPart {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// private Sector sector;

	// - I N T E R F A C E - P R O P E R T Y C H A N G E L I S T E N E R
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AssistantMap.DATA_ADDED_PROP.equals(prop)) {
			this.refreshChildren();
		}
		if (AssistantMap.CHANGE_ZOOMFACTOR.equals(prop)) {
			//TODO Refresh the location of every figure contained inside the map.
			final Iterator<Object> cit = this.getChildren().iterator();
			while (cit.hasNext()) {
				final Object child = cit.next();
				if (child instanceof SpotEditPart) {
					((SpotEditPart) child).refreshVisuals();
				}
			}
			//DEBUG Check if this line is necessary.
			this.refresh();
		}
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Adds the child's Figure to the {@link #getContentPane() contentPane}.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#addChildVisual(EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		final IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		//		if (childEditPart instanceof SectorEditPart) {
		//			FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.SECTOR_LAYER);
		//			sectorLayer.add(child);
		//			return;
		//		}
		if (childEditPart instanceof SpotEditPart) {
			final FreeformLayer sectorLayer = (FreeformLayer) this.getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
			sectorLayer.add(child);
			return;
		}
		//		if (childEditPart instanceof PlanetEditPart) {
		//			FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		//			sectorLayer.add(child);
		//			return;
		//		}
		//		if (childEditPart instanceof BaseEditPart) {
		//			FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		//			sectorLayer.add(child);
		//			return;
		//		}
		//		if (childEditPart instanceof ShipEditPart) {
		//			FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.SHIP_LAYER);
		//			sectorLayer.add(child);
		//			return;
		//		}
		super.addChildVisual(childEditPart, index);
	}

	// public Sector getSector() {
	// return sector;
	// }

	// - A B S T R A C T - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// - Disallows the removal of this edit part from its parent
		this.installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		// - Handles constraint changes (e.g. moving and/or resizing) and creation of new model elements
		this.installEditPolicy(EditPolicy.LAYOUT_ROLE, new VGAP4XYLayoutEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// - Create an empty figure to control the scroll and the sizing.
		final Figure fig = new FreeformLayer();
		fig.setOpaque(true);
		fig.setBorder(new MarginBorder(3));
		fig.setLayoutManager(new FreeformLayout());

		// TODO Create an additional layer where to put the Sectors.
		// ScalableFreeformRootEditPart manager = (ScalableFreeformRootEditPart)
		// getViewer().getEditPartRegistry().get(LayerManager.ID);
		// layer = manager.getScaledLayers();
		// // return manager.getLayer(layer);
		//
		// IFigure sectorLayer = getLayer(LayerConstants.GRID_LAYER);
		// this.
		// - Create the static router for the connection layer
		final ConnectionLayer connLayer = (ConnectionLayer) this.getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(new ShortestPathConnectionRouter(fig));

		// TODO Add the creation of some rectangles to simulate the sectors.
		// FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.SECTOR_LAYER);
		// sector = new Sector();
		// sectorLayer.add(sector);

		return fig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	protected List<Object> getModelChildren() {
		return this.getCastedModel().getChildren();
	}

	@Override
	protected void refreshChildren() {
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * Remove the child's Figure from the {@link #getContentPane() contentPane}.
	 * 
	 * @see AbstractEditPart#removeChildVisual(EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		//- Locate the right layer.
		IFigure contentPane = this.getContentPane();
		//		if (childEditPart instanceof SectorEditPart) contentPane = getLayer(VGAPScalableFreeformRootEditPart.SECTOR_LAYER);
		if (childEditPart instanceof SpotEditPart) {
			contentPane = this.getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		}
		//		if (childEditPart instanceof PlanetEditPart) contentPane = getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		//		if (childEditPart instanceof BaseEditPart) contentPane = getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		//		if (childEditPart instanceof ShipEditPart) contentPane = getLayer(VGAPScalableFreeformRootEditPart.SHIP_LAYER);
		final IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		try {
			contentPane.remove(child);
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - P R O T E C T E D - S E C T I O N
	private AssistantMap getCastedModel() {
		return (AssistantMap) this.getModel();
	}
}

// - UNUSED CODE ............................................................................................
