//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Button;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.factories.IconGenerator;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.figures.draw2d.RoundedGroup;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractDetailedFigure extends SelectableFigure implements IDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	protected static final Color	COLOR_SHIP_NAME	= new Color(Display.getDefault(), 0, 0, 0);
	protected static final Color	COLOR_BLACK			= new Color(Display.getDefault(), 0, 0, 0);
	//	private static Logger					logger					= Logger.getLogger("net.sf.vgap4.assistant.figures");

	protected RoundedGroup				group						= new RoundedGroup();
	// - F I E L D - S E C T I O N ............................................................................
	private final AssistantNode		genericModel;
	private String								log;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractDetailedFigure(final AssistantNode model) {
		genericModel = model;
		group.groupName.setFont(AssistantConstants.FONT_MAP_BOLD);
		group.groupName.setForegroundColor(AssistantConstants.COLOR_MEDIUM_BLUE);
		this.add(group);

		//- Change the background color depending on FRIEND or FOE.
		if (AssistantConstants.FFSTATUS_UNDEFINED.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_UNDEFINED_BACKGROUND);
		if (AssistantConstants.FFSTATUS_FRIEND.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_FRIEND_BACKGROUND);
		if (AssistantConstants.FFSTATUS_FOE.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_FOE_BACKGROUND);
		if (AssistantConstants.FFSTATUS_NEUTRAL.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_NEUTRAL_BACKGROUND);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		this.setupLayout();
		group.reset();
		//- Set the object's graphical icon. This uses the same common code for tree views.
		this.setIcon(IconGenerator.generateImage(genericModel, group.getBackgroundColor()));
		//- Compose the identification line.
		this.setTitle(genericModel.getIdentifier() + " [T" + genericModel.getLatestTurnNumber() + "]");
		//- Add the location coordinates. This will add a new tooltip with the target locations.
		final MultiLabelLine coordsLabel = new MultiLabelLine();
		coordsLabel.addColumn("Coords:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		coordsLabel.addColumn(genericModel.getLocationString(), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
		this.append(coordsLabel);

		//- Add the specific model information.
		this.addAdditionalContents();
		//TODO Add the Log game information inside a folded element.
		this.addLogPanel();
		//TODO Add the User information editable message area.
		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	protected abstract void addAdditionalContents();

	protected void append(final IFigure newFigure) {
		group.add(newFigure);
	}

	private void addLogPanel() {
		final MultiLabelLine logLabel = new MultiLabelLine();
		logLabel.addColumn("Log Information:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		final Button foldButton = new Button(ImageFactory.getImage("foldButton.gif"));
		logLabel.addColumn(foldButton, SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		log = genericModel.getField("Log");
	}

	protected void setIcon(final Image newIcon) {
		group.groupName.setIcon(newIcon);
		group.groupName.setIconTextGap(2);
	}

	protected void setTitle(final String title) {
		group.setText(title);
	}

	protected void setupLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 2;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}
}

// - UNUSED CODE ............................................................................................
