//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: GraphLayoutManager.java 196 2009-03-12 17:50:48Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-12 18:50:48 +0100 (jue, 12 mar 2009) $
//  RELEASE:        $Revision: 196 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.policies;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.AbstractLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Node;

import net.sf.gef.core.editparts.AbstractDirectedNodeEditPart;
import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.editparts.PilotModelContainerEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotGraphLayoutManager extends AbstractLayout {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger								logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");
	//	private static final Insets					PADDING	= new Insets(4, 2, 4, 2);

	// - F I E L D - S E C T I O N ............................................................................
	private final PilotModelContainerEditPart	rootPart;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedContainerEditPart() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public PilotGraphLayoutManager(final PilotModelContainerEditPart diagram) {
		rootPart = diagram;
	}

	public void layout(final IFigure container) {
		final DirectedGraph graph = new DirectedGraph();
		final Map<AbstractDirectedNodeEditPart, Node> partsToNodes = new HashMap<AbstractDirectedNodeEditPart, Node>();
		rootPart.contributeNodesToGraph(graph, partsToNodes);
		new DirectedGraphLayout().visit(graph); // Calculate layout
		rootPart.applyGraphResults(graph, partsToNodes); // Resize and reposition figures.
	}

	@Override
	protected Dimension calculatePreferredSize(final IFigure container, final int wHint, final int hHint) {
		container.validate();
		final List<AbstractGEFNode> children = container.getChildren();
		final Rectangle result = new Rectangle().setLocation(container.getClientArea().getLocation());
		for (int i = 0; i < children.size(); i++) {
			result.union(((IFigure) children.get(i)).getBounds());
		}
		result.resize(container.getInsets().getWidth(), container.getInsets().getHeight());
		return result.getSize();
	}
}
// - UNUSED CODE ............................................................................................
