//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedContainerEditPart.java 193 2009-03-11 16:03:37Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-11 17:03:37 +0100 (mié, 11 mar 2009) $
//  RELEASE:        $Revision: 193 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.geometry.Insets;

import net.sf.gef.core.editparts.AbstractDirectedEditPart;
import net.sf.vgap4.assistant.models.DetailedDiagram;
import net.sf.vorg.policies.PilotGraphLayoutManager;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotContainerEditPart extends AbstractDirectedEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");
	private static final Insets	PADDING	= new Insets(4, 2, 4, 2);

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedContainerEditPart() {
	//	}

	//	public void applyGraphResults(final DirectedGraph graph, final Map<PilotBoatEditPart, Node> map) {
	//		this.applyChildrenResults(graph, map);
	//	}
	//
	//	public void contributeNodesToGraph(final DirectedGraph graph, final Map<PilotBoatEditPart, Node> map) {
	//		//- Scan all the children for this diagram and add all them to the graph.
	//		Node previousNode = null;
	//		final int counter = 0;
	//		boolean firstNode = true;
	//		final List<PilotBoatEditPart> contents = this.getChildren();
	//		final Iterator<PilotBoatEditPart> cit = contents.iterator();
	//		while (cit.hasNext()) {
	//			//- Get the part, the figure and the model.
	//			final PilotBoatEditPart part = cit.next();
	//
	//			//- Create the node and fill its fields.
	//			final Node node = new Node(part);
	//			node.setPadding(PilotContainerEditPart.PADDING);
	//			node.setRowConstraint(counter);
	//			node.setSize(part.getSize());
	//			if (firstNode) {
	//				//- Do not connect this node with an edge.
	//				previousNode = node;
	//				firstNode = false;
	//				graph.nodes.add(node);
	//				map.put(part, node);
	//			} else {
	//				//- Connect this node and the previous one.
	//				final Edge connectingEdge = new Edge(previousNode, node);
	//				previousNode = node;
	//				graph.nodes.add(node);
	//				graph.edges.add(connectingEdge);
	//				map.put(part, node);
	//			}
	//		}
	//	}
	//
	//	protected void applyChildrenResults(final DirectedGraph graph, final Map<PilotBoatEditPart, Node> map) {
	//		//TODO Calculate the max width of all children.
	//		Iterator<Node> mit = map.values().iterator();
	//		int maxWidth = 0;
	//		while (mit.hasNext()) {
	//			final int nodeWidth = mit.next().width;
	//			if (nodeWidth > maxWidth) {
	//				maxWidth = nodeWidth;
	//			}
	//		}
	//		//- Update the widths of all nodes to the max width.
	//		mit = map.values().iterator();
	//		while (mit.hasNext()) {
	//			final Node node = mit.next();
	//			node.width = maxWidth;
	//			node.x = 2;
	//		}
	//
	//		for (int i = 0; i < this.getChildren().size(); i++) {
	//			final PilotBoatEditPart part = (PilotBoatEditPart) this.getChildren().get(i);
	//			part.applyGraphResults(graph, map);
	//		}
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	protected void createEditPolicies() {
	//		// - Disallows the removal of this edit part from its parent
	//		this.installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
	//		this.installEditPolicy(EditPolicy.NODE_ROLE, null);
	//		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, null);
	//		this.installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
	//	}

	@Override
	protected IFigure createFigure() {
		// - Create an empty figure to control the scroll and the sizing.
		final Figure fig = new FreeformLayer();
		fig.setOpaque(true);
		fig.setBorder(new MarginBorder(3));
		fig.setLayoutManager(new PilotGraphLayoutManager(this));
		return fig;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected List getModelChildren() {
		final DetailedDiagram model = this.getCastedModel();
		return model.getChildren();
	}

	@Override
	protected void refreshChildren() {
		//FIXME Remove this method in final release.
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private DetailedDiagram getCastedModel() {
		return (DetailedDiagram) this.getModel();
	}
}
// - UNUSED CODE ............................................................................................
