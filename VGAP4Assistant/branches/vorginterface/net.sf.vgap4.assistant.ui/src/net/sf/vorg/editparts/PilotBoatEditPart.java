//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SpotEditPart.java 179 2008-07-10 11:51:20Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-10 13:51:20 +0200 (jue, 10 jul 2008) $
//  RELEASE:        $Revision: 179 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.Vector;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.editparts.AbstractDirectedNodeEditPart;
import net.sf.gef.core.model.IGEFNode;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;
import net.sf.vorg.figures.PilotBoatFigure;
import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotBoatEditPart extends AbstractDirectedNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public PilotBoat getCastedModel() {
		return (PilotBoat) this.getModel();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Boat.NAME_CHANGED.equals(prop)) {
			// - The references to the model and figure objects.
			final PilotBoatFigure fig = (PilotBoatFigure) this.getFigure();
			final PilotBoat mod = this.getCastedModel();
			fig.setName(mod.getName());
			return;
		}
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		getContentPane().add(child);
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("[PilotBoatEditPart:");
		buffer.append(this.getCastedModel().getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	//	@Override
	//	public Dimension getSize() {
	//		return new Dimension(this.getFigure().getPreferredSize());
	//	}

	/** Return the Boat and the Commands as its children. */
	@Override
	protected List<IGEFNode> getModelChildren() {
		final Vector<IGEFNode> childList = new Vector<IGEFNode>(2);
		childList.add(this.getCastedModel().getBoat());
		PilotCommand activeCommand = this.getCastedModel().getActiveCommand();
		if (null != activeCommand) childList.add(activeCommand);
		return childList;
	}

	//	protected void applyGraphResults(final DirectedGraph graph, final Map<PilotBoatEditPart, Node> map) {
	//		try {
	//			final Node node = map.get(this);
	//			this.getFigure().setSize(node.width, node.height);
	//			this.getFigure().setBounds(new Rectangle(node.x, node.y, node.width, node.height));
	//		} catch (final Exception e) {
	//			// TODO: handle exception
	//			e.printStackTrace();
	//		}
	//	}

	//	@Override
	//	protected void refreshVisuals() {
	//		// - The references to the model and figure objects.
	//		//		final PilotBoatFigure fig = (PilotBoatFigure) this.getFigure();
	//		final PilotBoat mod = this.getCastedModel();
	//
	//		// - The references to the model and figure objects.
	//		final IFigure fig = this.getFigure();
	//		if (fig instanceof IDetailedFigure) {
	//			((IDetailedFigure) fig).createContents();
	//			//		super.refreshVisuals();
	//		}
	//	}

	//	private void refreshModelVisuals() {
	//		// - The references to the model and figure objects.
	//		//		final SpotFigure fig = (SpotFigure) this.getFigure();
	//		//		final Spot model = this.getCastedModel();
	//		//- Get the representative model data.
	//		final AssistantNode representative = model.getRepresentative();
	//
	//		// - Update figure visuals from current model data.
	//		fig.setCoordinates(representative.getLocation());
	//		fig.setId(representative.getIdNumber());
	//		fig.setName(representative.getName());
	//	}
}
// - UNUSED CODE ............................................................................................
