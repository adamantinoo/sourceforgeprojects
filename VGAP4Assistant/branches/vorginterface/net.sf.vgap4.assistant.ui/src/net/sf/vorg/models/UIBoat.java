//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.models.facets.BoatTreeFacet;
import net.sf.vorg.models.facets.IPilotTreeFacet;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class UIBoat extends Boat {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public UIBoat() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public IPilotTreeFacet getTreeFacet() {
		return new BoatTreeFacet(this);
	}
}

// - UNUSED CODE ............................................................................................
