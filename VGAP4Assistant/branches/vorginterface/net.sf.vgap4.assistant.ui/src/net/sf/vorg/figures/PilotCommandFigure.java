//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.RoundedGroup;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vorg.vorgautopilot.models.PilotCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotCommandFigure extends RoundedGroup {
	// - S T A T I C - S E C T I O N ..........................................................................
	public Font									FONT_BOATNAME	= new Font(Display.getDefault(), "Arial", 14, SWT.NORMAL);
	// - F I E L D - S E C T I O N ............................................................................
	private final PilotCommand	command;

	//	protected RoundedGroup	group					= new RoundedGroup();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommandFigure(final PilotCommand pilotCommand) {
		super();
		command = pilotCommand;
		groupName.setFont(AssistantConstants.FONT_MAP_BOLD);
		groupName.setForegroundColor(AssistantConstants.COLOR_MEDIUM_BLUE);
		//		this.add(group);
		//- Set the object's graphical icon. This uses the same common code for tree views.
		groupName.setIcon(ImageFactory.getImage("icons/Boat.gif"));
		groupName.setIconTextGap(3);
		this.setText("Command Waypoints");

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void add(IFigure figure, Object constraint, int index) {
		//- Change the order of the additions.
		//		if (this.getChildren().size() == 0)
		//			super.add(figure, constraint, index);
		//		else {
		//			if (index == -1) {
		//				this.getChildren().add(this.getChildren().size(), figure);
		//				return;
		//			}
		//			if (index == 0) {
		//				this.getChildren().add(this.getChildren().size(), figure);
		//				return;
		//			}
		super.add(figure, constraint, index);
		//		}
	}
	//	public void setName(final String name) {
	//		this.setText(name);
	//	}

	//	protected void setIcon(final Image newIcon) {
	//		group.groupName.setIcon(newIcon);
	//		group.groupName.setIconTextGap(3);
	//	}
	//
	//	protected void setTitle(final String title) {
	//		group.setText(title);
	//	}
	//
	//	public void createContents() {
	//		this.setupLayout();
	//		group.reset();
	//		//- Set the object's graphical icon. This uses the same common code for tree views.
	//		this.setIcon(ImageFactory.getImage("icons/Boat.gif"));
	//		//- Compose the identification line.
	//		this.setTitle(pilot.getName());
	//
	//		//- Add the figure for the Boat.
	//		BoatFigure boatFigure = (BoatFigure) new PilotFigureFactory().createFigure(null, this.pilot.getBoat());
	//		boatFigure.createContents();
	//		this.append(boatFigure);
	//
	//		addWaypoints();
	//
	//		this.setSize(this.getPreferredSize());
	//		this.repaint();
	//	}

	//	private void addWaypoints() {
	//		RoundedGroup waypointsGroup = new RoundedGroup();
	//		waypointsGroup.groupName.setFont(AssistantConstants.FONT_MAP_BOLD);
	//		waypointsGroup.groupName.setForegroundColor(AssistantConstants.COLOR_MEDIUM_BLUE);
	//		waypointsGroup.setText("Boat Waypoints");
	//
	//		PilotCommand command = pilot.getActiveCommand();
	//		if (null != command) {
	//			Vector<Waypoint> waypoints = command.getWaypoints();
	//			Iterator<Waypoint> wit = waypoints.iterator();
	//			while (wit.hasNext()) {
	//				Waypoint waypoint = wit.next();
	//				WaypointFigure wayPointFigure = (WaypointFigure) new PilotFigureFactory().createFigure(null, waypoint);
	//				wayPointFigure.createContents();
	//				waypointsGroup.add(wayPointFigure);
	//			}
	//		}
	//		append(waypointsGroup);
	//	}

	//	protected void append(final IFigure newFigure) {
	//		group.add(newFigure);
	//	}

	//	protected void setupLayout() {
	//		final GridLayout grid = new GridLayout();
	//		grid.horizontalSpacing = 0;
	//		grid.marginHeight = 0;
	//		grid.marginWidth = 2;
	//		grid.numColumns = 1;
	//		grid.verticalSpacing = 0;
	//		this.setLayoutManager(grid);
	//	}
}
// - UNUSED CODE ............................................................................................
