//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.figures.IDetailedFigure;
import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatFigure extends SelectableFigure implements IDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	public Font		FONT_BOATNAME	= new Font(Display.getDefault(), "Arial", 14, SWT.NORMAL);
	// - F I E L D - S E C T I O N ............................................................................
	private Boat	boatModel			= null;

	//	private MultiLabelLine	name=new MultiLabelLine();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatFigure(Boat model) {
		boatModel = model;
		initialize();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		initialize();
		//		this.removeAll();
		//
		//		final GridLayout grid = new GridLayout();
		//		grid.horizontalSpacing = 0;
		//		grid.marginHeight = 0;
		//		grid.marginWidth = 0;
		//		grid.numColumns = 2;
		//		grid.verticalSpacing = 0;
		//		this.setLayoutManager(grid);
		//		this.setBorder(new LineBorder(1));
		//
		//		Label iconCell = new Label(ImageFactory.getImage("icons/Boat.jpg"));
		//		iconCell.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		//		iconCell.setIconAlignment(PositionConstants.TOP);
		//		iconCell.setIconTextGap(5);
		//		this.add(iconCell);

		addAdditionalContents();
		addWindcellData();
		addVMGData();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	private void setLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.verticalSpacing = 0;
		grid.numColumns = 2;
		this.setLayoutManager(grid);
	}

	private void initialize() {
		this.removeAll();
		setLayout();
		this.setBorder(new LineBorder(1));

		Label iconCell = new Label(ImageFactory.getImage("icons/Boat.jpg"));
		iconCell.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(5);
		this.add(iconCell);
	}

	//public void setName() {
	////name = new MultiLabelLine();
	//	name.setDefaultFont("Arial", 14);
	//	name.addColumn(boatModel.getName(), SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
	//	name.addColumn("Ranking:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
	//	name.addColumn(new Integer(boatModel.getRanking()).toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
	//	
	//}
	protected void addAdditionalContents() {
		Figure container = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.verticalSpacing = 0;
		grid.numColumns = 1;
		container.setLayoutManager(grid);

		MultiLabelLine name = new MultiLabelLine();
		name.setDefaultFont("Arial", 14);
		name.addColumn(boatModel.getName(), SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		name.addColumn("Ranking:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		name.addColumn(new Integer(boatModel.getRanking()).toString() + " / "
				+ new Integer(boatModel.getDiffRanking()).toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//		Label boatName = new Label(boatModel.getName());
		//		boatName.setFont(FONT_BOATNAME);

		//		final MultiLabelLine ranking = new MultiLabelLine();
		//		ranking.setDefaultFont("Tahoma", 10);

		final MultiLabelLine heading = new MultiLabelLine();
		heading.setDefaultFont("Tahoma", 10);
		heading.addColumn("Heading:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		heading.addColumn(new Integer(boatModel.getHeading()).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		//		final MultiLabelLine awd = new MultiLabelLine();
		//		heading.setDefaultFont("Tahoma", 10);
		heading.addColumn("AWD:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		heading.addColumn(new Integer(boatModel.getWindAngle()).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//		final MultiLabelLine speed = new MultiLabelLine();
		//		speed.setDefaultFont("Tahoma", 10);
		heading.addColumn("Speed:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		heading.addColumn(FormatSingletons.nf2.format(boatModel.getSpeed()) + " knts", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		final MultiLabelLine sail = new MultiLabelLine();
		sail.setDefaultFont("Tahoma", 10);
		sail.addColumn("Sail:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		sail.addColumn(boatModel.getSail().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		//		final MultiLabelLine location = new MultiLabelLine();
		//		location.setDefaultFont("Tahoma", 10);
		sail.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		sail.addColumn(boatModel.getLocation().toReport().replace('\t', '-'), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		container.add(name);
		container.add(heading);
		//		container.add(awd);

		//		container.add(speed);
		container.add(sail);

		//		container.add(location);
		this.add(container);
	}

	private void addWindcellData() {
		try {
			WindMapHandler.loadWinds(boatModel.getLocation());
			WindCell cell = WindMapHandler.getWindCell(boatModel.getLocation());
			final VMCData vmg = new VMCData(boatModel.getHeading(), cell);

			GridLayout grid = new GridLayout();
			grid.horizontalSpacing = 10;
			grid.marginHeight = 0;
			grid.marginWidth = 1;
			grid.numColumns = 2;
			grid.verticalSpacing = 0;
			this.setLayoutManager(grid);

			Label iconCell = new Label(ImageFactory.getImage("icons/WindCell.jpg"));
			iconCell.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			iconCell.setIconAlignment(PositionConstants.TOP);
			iconCell.setIconTextGap(5);
			this.add(iconCell);

			Figure container = new Figure();
			grid = new GridLayout();
			grid.horizontalSpacing = 0;
			grid.marginHeight = 0;
			grid.marginWidth = 2;
			grid.numColumns = 1;
			grid.verticalSpacing = 0;
			container.setLayoutManager(grid);

			final MultiLabelLine heading = new MultiLabelLine();
			heading.setDefaultFont("Tahoma", 10);
			heading.addColumn("Wind Direction:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			heading.addColumn(new Integer(cell.getWindDir()).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
			//			final MultiLabelLine speed = new MultiLabelLine();
			//			speed.setDefaultFont("Tahoma", 10);
			heading.addColumn("     Speed:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			heading.addColumn(FormatSingletons.nf2.format(cell.getWindSpeed()) + " knots", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);

			final MultiLabelLine location = new MultiLabelLine();
			location.setDefaultFont("Tahoma", 10);
			location.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			location.addColumn(cell.getLocation().toReport().replace('\t', '-'), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
			//			final MultiLabelLine empty1 = new MultiLabelLine();
			//			empty1.setDefaultFont("Tahoma", 10);
			//			empty1.addColumn("   ", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);

			final MultiLabelLine maxLabel = new MultiLabelLine();
			maxLabel.setDefaultFont("Tahoma", 10);
			maxLabel.addColumn("Max performance:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			//		final MultiLabelLine vmgHeading = new MultiLabelLine();
			//		vmgHeading.setDefaultFont("Tahoma", 10);
			maxLabel.addColumn(" Heading:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			maxLabel.addColumn(new Integer(vmg.getMaxAWD()).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
			//			final MultiLabelLine vmgSpeed = new MultiLabelLine();
			//			vmgSpeed.setDefaultFont("Tahoma", 10);
			maxLabel.addColumn(" Speed:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
			maxLabel.addColumn(FormatSingletons.nf2.format(vmg.getBestSailConfiguration().getSpeed()) + " knts", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);

			container.add(heading);
			//			container.add(speed);
			container.add(location);
			//			container.add(empty1);
			container.add(maxLabel);
			//			container.add(empty2);
			//			container.add(vmgHeading);
			//			container.add(vmgSpeed);

			this.add(container);
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addVMGData() {
		//					// - Add current information for course VMG
		//		try {
		//			WindCell cell = WindMapHandler.getWindCell(boatModel.getGeoLocation());
		//				final VMCData vmg = new VMCData(boatModel.getHeading(), cell);
		//
		//				GridLayout grid = new GridLayout();
		//				grid.horizontalSpacing = 10;
		//				grid.marginHeight = 0;
		//				grid.marginWidth = 1;
		//				grid.numColumns = 2;
		//				grid.verticalSpacing = 0;
		//				this.setLayoutManager(grid);
		//
		//				Label iconCell = new Label(ImageFactory.getImage("icons/VMG.jpg"));
		//				iconCell.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		//				iconCell.setIconAlignment(PositionConstants.TOP);
		//				iconCell.setIconTextGap(5);
		//				this.add(iconCell);
		//
		//				Figure container = new Figure();
		//				grid = new GridLayout();
		//				grid.horizontalSpacing = 0;
		//				grid.marginHeight = 0;
		//				grid.marginWidth = 2;
		//				grid.numColumns = 2;
		//				grid.verticalSpacing = 0;
		//				container.setLayoutManager(grid);

		/*
		 * StringBuffer buffer = new StringBuffer(); NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		 * nf.setMaximumFractionDigits(3); nf.setMinimumFractionDigits(3);
		 * buffer.append("Max Speed=").append(maxConfiguration);
		 * buffer.append(" course=").append(maxAngle).append("]\n"); buffer.append("[VMC results").append('\n');
		 * buffer.append("   Projection heading=").append(targetDirection).append('\n');
		 * buffer.append("   Wind direction=").append(windDirection).append('\n');
		 * buffer.append("   Wind speed=").append(nf.format(windSpeed)).append(" knots\n");
		 * buffer.append("   Port VMC [boat speed=").append(nf.format(leftConfiguration.getSpeed()));
		 * buffer.append("-").append(leftConfiguration.getSail()).append("] [VMC=");
		 * buffer.append(nf.format(leftVMC)).append(" knots - heading="); buffer.append(leftAngle); int awd =
		 * GeoLocation.calculateAWD(windDirection, leftAngle); buffer.append(" AWD=").append(awd
		 * Integer.signum(awd)); buffer.append("]\n");
		 * buffer.append("   Starboard VMC [boat speed=").append(nf.format(rightConfiguration.getSpeed()));
		 * buffer.append("-").append(rightConfiguration.getSail());
		 * buffer.append("] [VMC=").append(nf.format(rightVMC)).append(" knots - heading=");
		 * buffer.append(rightAngle); awd = GeoLocation.calculateAWD(windDirection, rightAngle);
		 * buffer.append(" AWD=").append(awd Integer.signum(awd)); buffer.append("]\n]"); return
		 * buffer.toString();
		 */

		//		} catch (LocationNotInMap e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}
}
// - UNUSED CODE ............................................................................................
