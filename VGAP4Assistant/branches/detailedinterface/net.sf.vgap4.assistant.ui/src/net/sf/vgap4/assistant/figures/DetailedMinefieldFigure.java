//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.models.Pod;
import net.sf.vgap4.assistant.models.Thing;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedMinefieldFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final Thing		podModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedMinefieldFigure(final Pod model) {
		super(model);
		podModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void addAdditionalContents() {
		final MultiLabelLine passageLabel = new MultiLabelLine();
		passageLabel.addColumn("Pod name:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		passageLabel.addColumn(podModel.getName(), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);

		//- Add labels to this element for composition.
		this.append(passageLabel);
	}
}
// - UNUSED CODE ............................................................................................
