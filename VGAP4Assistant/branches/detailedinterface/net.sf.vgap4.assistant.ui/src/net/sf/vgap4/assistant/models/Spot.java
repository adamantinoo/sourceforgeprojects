//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.models.facets.ITreeFacet;
import net.sf.vgap4.assistant.models.facets.SpotTreeFacet;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class Spot extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long						serialVersionUID	= -1007437259123090150L;
	private static Logger								logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	public boolean											hasBase						= false;
	public boolean											hasPlanet					= false;
	public boolean											hasShip						= false;
	public boolean											hasPods						= false;
	public boolean											hasWings					= false;

	// - F I E L D - S E C T I O N ............................................................................
	private final Vector<AssistantNode>	spotContents			= new Vector<AssistantNode>(2);
	private AssistantNode								representative;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Spot() {
	}

	public Spot(final LocationCluster cluster) {
		//		this.locationObjects = cluster;

		//TODO Load the cached data in the spot fields from the cluster representative.
		final AssistantNode representative = cluster.getRepresentative();
		this.setLocation(representative.getLocation());
		this.setIdNumber(representative.getIdNumber());
		this.setName(representative.getName());
		this.setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add a new element to this location spot. Depending on the element to be added we can select a
	 * representative this is the top priority element in the spot in this order:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * Representative delegate may be calculated when adding (that is done more then once) or delayed until the
	 * first request for a representative that will be executed once during the life of the Spot.
	 * 
	 * @param mapElement
	 *          the model node to be added to this spot
	 */
	public void add(final AssistantNode mapElement) {
		if (mapElement instanceof Ship) hasShip = true;
		if (mapElement instanceof Planet) hasPlanet = true;
		if (mapElement instanceof Base) hasBase = true;
		if (mapElement instanceof Pod) hasPods = true;
		if (mapElement instanceof Wing) hasWings = true;
		spotContents.add(mapElement);
	}

	public Vector<AssistantNode> getContents() {
		return spotContents;
	}

	@Override
	public String getField(final String key) {
		return this.getRepresentative().getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return this.getRepresentative().getFieldNumber(key);
	}

	@Override
	public int getOwner() {
		return representative.getOwner();
	}

	public String getPreferredShape() {
		if (hasBase) return "Base";
		if (hasPlanet) return "Planet";
		if (hasShip) return "Ship";
		return "Undefined";
	}

	/**
	 * Tis method delays the calculation of the representativo for this spot and also reduces the possibility to
	 * return null values when new model objects are added to the Map. As explained on the <code>add()</code>
	 * method the priority to select a representative is:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * The representative is calculated by applying a priority factor to each element depending on its class and
	 * then gettinh the top most priority one. This can be done in a single pass over the elements list.
	 * 
	 * @return the model node selected as a representative prioritized class node.
	 */
	public AssistantNode getRepresentative() {
		//- Check if the representative is cached.
		if (null == representative) {
			//- Lookup for a representative. If the size of the spot is 1 then it is clear who it it.
			if (1 == spotContents.size()) {
				this.setRepresentative(spotContents.firstElement());
				return representative;
			} else {
				double repPriority = -1;
				final Iterator<AssistantNode> sit = spotContents.iterator();
				while (sit.hasNext()) {
					final AssistantNode element = sit.next();
					double priority = 0;
					if (element instanceof Ship) priority = element.getIdNumber() * 1000.0;
					if (element instanceof Planet) priority = element.getIdNumber() * 1000.0 * 1000.0;
					if (element instanceof Base) priority = element.getIdNumber() * 1000.0 * 1000.0 * 1000.0;
					if (element instanceof Pod) priority = element.getIdNumber() * 1.0;
					if (element instanceof Wing) priority = element.getIdNumber() * 10.0;
					if (priority > repPriority) {
						this.setRepresentative(element);
						repPriority = priority;
					}
				}
				return representative;
			}
		} else
			return representative;
	}

	public ITreeFacet getTreeFacet() {
		return new SpotTreeFacet(this);
	}

	public boolean hasContents() {
		if (null != representative) return true;
		return false;
	}

	public boolean hasNatives() {
		if (hasPlanet) {
			final Iterator<AssistantNode> cit = spotContents.iterator();
			while (cit.hasNext()) {
				final AssistantNode element = cit.next();
				if (element instanceof Planet) {
					if (((Planet) element).getTotalNatives() > 0) return true;
				}
			}
		}
		return false;
	}

	public String infoLevel() {
		if (hasPlanet) {
			final Planet planet = this.locatePlanet();
			if (null != planet) return planet.infoLevel();
		}
		return AssistantConstants.INFOLEVEL_UNEXPLORED;
	}

	public boolean isMultiple() {
		if (spotContents.size() > 1)
			return true;
		else
			return false;
	}

	public void setRepresentative(final AssistantNode representative) {
		this.representative = representative;

		//- Load the cached data in the spot fields from the cluster representative.
		this.setLocation(representative.getLocation());
		this.setIdNumber(representative.getIdNumber());
		this.setName(representative.getName());
		this.setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("Spot[");
		if (hasBase)
			buffer.append("B");
		else
			buffer.append("-");
		if (hasPlanet)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasShip)
			buffer.append("S");
		else
			buffer.append("-");
		if (hasPods)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasWings)
			buffer.append("W");
		else
			buffer.append("-");
		buffer.append(this.getRepresentative().toString());
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	private Planet locatePlanet() {
		final Iterator<AssistantNode> cit = spotContents.iterator();
		while (cit.hasNext()) {
			final AssistantNode element = cit.next();
			if (element instanceof Planet) return (Planet) element;
		}
		return null;
	}
}

// - UNUSED CODE ............................................................................................
