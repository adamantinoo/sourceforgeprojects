//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.draw2d.Label;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.factories.IconGenerator;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class contains the quantities of natives that are found on the planet's surface. The native race is
 * stored as an index on a fixed size array being the array locations assigned to the next races: 0 -
 * humanoids ... continue with all races...
 */
public class Natives extends FieldTransformer implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= -4235257829153780299L;
	private static String[]		nativeRaces				= new String[10];
	static {
		Natives.nativeRaces[0] = "Humanoid";
		Natives.nativeRaces[1] = "Bovinoid";
		Natives.nativeRaces[2] = "Reptilian";
		Natives.nativeRaces[3] = "Avian";
		Natives.nativeRaces[4] = "Amorphous";
		Natives.nativeRaces[5] = "Insectoid";
		Natives.nativeRaces[6] = "Amphibian";
		Natives.nativeRaces[7] = "Guipsoldal";
		Natives.nativeRaces[8] = "Siliconoid";
		Natives.nativeRaces[9] = "Chupanoid";
	}

	// - F I E L D - S E C T I O N ............................................................................
	private final int[]				nativeCount				= new int[10];
	private boolean						hasNatives				= false;
	/** Total number of natives available at the base at this turn. */
	private int								totalnatives			= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public Natives(final String[] planetFields) {
	//		// - Check to see the type of natives and the native count. Put this on the native array.
	//		for (int counter = 0; counter < 10; counter++) {
	//			nativeCount[counter] = this.convert2Integer(planetFields[10 + counter]);
	//			if (nativeCount[counter] > 0) {
	//				//- Accumulate the number of natives available.
	//				totalnatives += nativeCount[counter];
	//				hasNatives = true;
	//			}
	//		}
	//	}
	public Natives() {
	}

	public Natives(final String[] planetFields, final int startDataIndex) {
		// - Check to see the type of natives and the native count. Put this on the native array.
		for (int counter = 0; counter < 10; counter++) {
			nativeCount[counter] = this.convert2Integer(planetFields[startDataIndex + counter]);
			if (nativeCount[counter] > 0) {
				hasNatives = true;
				totalnatives += nativeCount[counter];
			}
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int[] getNatives() {
		return nativeCount;
	}

	public String getToolTip() {
		final StringBuffer buffer = new StringBuffer();
		boolean added = false;
		for (int counter = 0; counter < 10; counter++) {
			if (nativeCount[counter] > 0) {
				added = true;
				buffer.append(Natives.nativeRaces[counter]).append(":").append(nativeCount[counter]);
			}
		}
		if (added)
			return buffer.toString();
		else
			return null;
	}

	public Vector<Label> getToolTips() {
		final Vector<Label> tips = new Vector<Label>(1);
		final Image nativeIcon = IconGenerator.generateImage(new NativeShape(null), AssistantConstants.COLOR_TIP);
		//		imageFactory.setTip(true);
		//		imageFactory.setShape(SHAPE_PLANET);
		//		imageFactory.setInfoLevel(AssistantConstants.INFOLEVEL_NATIVES);
		//		final Image nativeIcon = imageFactory.generateImage();
		for (int counter = 0; counter < 10; counter++) {
			if (nativeCount[counter] > 0) {
				tips.add(new StandardLabel(Natives.nativeRaces[counter] + ": " + nativeCount[counter], nativeIcon));
			}
		}
		return tips;
	}

	public int getTotalNatives() {
		return totalnatives;
	}

	public boolean isHasNatives() {
		return hasNatives;
	}
}

// - UNUSED CODE ............................................................................................
