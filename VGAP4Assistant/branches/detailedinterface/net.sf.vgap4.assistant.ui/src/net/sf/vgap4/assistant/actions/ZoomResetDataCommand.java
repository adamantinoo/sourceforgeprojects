//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.actions;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.commands.Command;

import net.sf.vgap4.assistant.models.AssistantMap;

//- CLASS IMPLEMENTATION ...................................................................................
public class ZoomResetDataCommand extends Command {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger					logger									= Logger.getLogger("net.sf.vgap4.projecteditor.commands");
	//	private final static String[]	OPEN_FILTER_NAMES				= new String[] { "CSVInfo.txt (Turn descripcion information)" };
	//	private final static String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.txt" };

	// - F I E L D - S E C T I O N ............................................................................
	private final AssistantMap	mapModel;
	private int									lastZoom;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ZoomResetDataCommand(final String label, final AssistantMap assistantMap) {
		super(label);
		mapModel = assistantMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean canExecute() {
		// - This command can be executed anytime.
		return true;
	}

	@Override
	public boolean canUndo() {
		// - We do not store the previous model, so the command can not be undo
		return true;
	}

	/** Change the zoom factor of the underlying Map model and recalculate the figures location */
	@Override
	public void execute() {
		lastZoom = mapModel.getZoomFactor();
		mapModel.resetZoomFactor();
	}

	@Override
	public void undo() {
		mapModel.setZoomFactor(lastZoom);
	}
}
// - UNUSED CODE ............................................................................................
