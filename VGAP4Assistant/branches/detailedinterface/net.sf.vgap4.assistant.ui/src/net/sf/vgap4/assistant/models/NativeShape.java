//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import net.sf.vgap4.assistant.models.facets.ITreeFacet;

// - CLASS IMPLEMENTATION ...................................................................................
public class NativeShape extends Thing {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= -27725121280117794L;

	//	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	public NativeShape(AssistantMap map) {
		super(map);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public ITreeFacet getTreeFacet() {
		// TODO Auto-generated method stub
		return null;
	}
}

// - UNUSED CODE ............................................................................................
