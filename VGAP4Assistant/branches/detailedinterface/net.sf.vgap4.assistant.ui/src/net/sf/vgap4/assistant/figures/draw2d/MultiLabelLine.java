//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import org.eclipse.draw2d.Button;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Allows the addition of multiple labels inside a single line. Labels may have their corresponding icon and a
 * different text style. The minimum number of columns that are configured are 2.
 */
public class MultiLabelLine extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger	= Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");

	// - F I E L D - S E C T I O N ............................................................................
	private final GridLayout			grid		= new GridLayout();
	private final Vector<Figure>	columns	= new Vector<Figure>(2);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MultiLabelLine() {
		grid.horizontalSpacing = 2;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 2;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}

	public int addColumn(final Button button, final int textStyle, final Color columnColor) {
		//		final Label column = new StandardLabel(columnText);
		button.setForegroundColor(columnColor);
		button.setFont(AssistantConstants.FONT_MAP_BOLD);
		columns.add(button);
		if (columns.size() > 2) grid.numColumns = columns.size();
		this.add(button);
		this.layout();
		return columns.size() - 1;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int addColumn(final String columnText, final int textStyle, final Color columnColor) {
		final Label column = new StandardLabel(columnText);
		column.setForegroundColor(columnColor);
		column.setFont(new Font(Display.getDefault(), "Tahoma", 8, textStyle));
		columns.add(column);
		if (columns.size() > 2) grid.numColumns = columns.size();
		this.add(column);
		this.layout();
		return columns.size() - 1;
	}

	public void setIcon(final int columnIndex, final Image icon) {
		if (columnIndex >= columns.size()) return;
		final Figure label = columns.get(columnIndex);
		if (label instanceof Label) ((Label) label).setIcon(icon);
	}
}

// - UNUSED CODE ............................................................................................
