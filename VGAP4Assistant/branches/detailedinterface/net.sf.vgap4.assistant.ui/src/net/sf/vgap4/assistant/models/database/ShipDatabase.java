//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.database;

//- IMPORT SECTION .........................................................................................
import java.util.Hashtable;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipDatabase {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger			= Logger.getLogger("net.sf.vgap4.assistant.models.database");
	public static final String							HUGE_WEAPON		= "HW";
	public static final String							LARGE_WEAPON	= "LW";
	private static Hashtable<Integer, Hull>	shipHulls			= new Hashtable<Integer, Hull>(5);
	static {
		ShipDatabase.shipHulls.put(24, new Hull("Small Space Freighter", 120));
		ShipDatabase.shipHulls.put(32, new Hull("Firestorm Class Cruiser", 90));
	}

	// - S T A T I C   M E T H O D - S E C T I O N ............................................................
	public static String getHullDescription(final int key) {
		Hull hull = shipHulls.get(key);
		if (null != hull)
			return hull.getDescription();
		else
			return "N/A";
	}

	public static int getHullMaxSpeed(int hullCode) {
		Hull hull = shipHulls.get(hullCode);
		if (null != hull)
			return hull.getMaxSpeed();
		else
			return 20;
	}
	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
}

class Hull {

	private String	name;
	private int			maxSpeed	= -1;

	public Hull(String name, int maxSpeed) {
		this.name = name;
		this.maxSpeed = maxSpeed;
	}

	public int getMaxSpeed() {
		if (maxSpeed < 0)
			return 20;
		else
			return this.maxSpeed;
	}

	public String getDescription() {
		if (null == this.name)
			return "N/A";
		else
			return this.name;
	}

}

// - UNUSED CODE ............................................................................................
