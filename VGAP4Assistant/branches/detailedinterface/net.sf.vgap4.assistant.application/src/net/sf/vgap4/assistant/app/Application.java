//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.app;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import net.sf.gef.core.app.AbstractRCPApplication;
import net.sf.vgap4.assistant.intro.WelcomePerspective;

// - CLASS IMPLEMENTATION ...................................................................................
public class Application extends AbstractRCPApplication {
	// - I N N E R C L A S S - S E C T I O N .................................................................
	// - CLASS IMPLEMENTATION ................................................................................
	/** Creates the initial windows and sets the default perspective. There should be a default perspective. */
	class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

		@Override
		public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer configurer) {
			return new ApplicationWorkbenchWindowAdvisor(configurer);
		}

		@Override
		public void eventLoopIdle(final Display display) {
			// FIXME This is the point where I can set my event loop without interfering with other threads.
			super.eventLoopIdle(display);
		}

		@Override
		public String getInitialWindowPerspectiveId() {
			return WelcomePerspective.PERSPECTIVE_ID;
		}

		@Override
		public void initialize(final IWorkbenchConfigurer configurer) {
			super.initialize(configurer);
			configurer.setSaveAndRestore(true);
		}
	}

	// - CLASS IMPLEMENTATION ................................................................................
	/** Configures the application window. */
	class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

		// - C O N S T R U C T O R S
		public ApplicationWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer configurer) {
			super(configurer);
		}

		// - P U B L I C S E C T I O N
		@Override
		public ActionBarAdvisor createActionBarAdvisor(final IActionBarConfigurer configurer) {
			return new ApplicationActionBarAdvisor(configurer);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
		 */
		@Override
		public void postWindowOpen() {
			super.postWindowOpen();
			// - Set the window position
			Display.getCurrent().getShells()[1].setLocation(210, 0);
		}

		/**
		 * This method is called before any window is open. This is the list of options that can be activated:
		 * 
		 * <pre>
		 * configurer.setShowCoolBar(false);
		 * configurer.setShowFastViewBars(false);
		 * configurer.setShowMenuBar(true); // Show the application menu bar
		 * configurer.setShowPerspectiveBar(false);
		 * configurer.setShowProgressIndicator(false);
		 * configurer.setShowStatusLine(false);
		 * </pre>
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#preWindowOpen()
		 */
		@Override
		public void preWindowOpen() {
			final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
			configurer.setInitialSize(new Point(600, 450));
			configurer.setShowCoolBar(true);
			configurer.setShowStatusLine(true);
			configurer.setShowProgressIndicator(true);
			// - Set the title of the main window
			configurer.setTitle("VGA Planets 4 Assistant RCP Application. Beta 0.4.0-detailedinterface");
		}
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	PLUGIN_ID					= Application.APPLICATION_ID;
	/** The application ID. */
	private static final String	APPLICATION_ID		= "net.sourceforge.rcp.harpoon.app.harpoonapplication";
	private static final String	APPLICATION_NAME	= "VGA Planets 4 Assistant";
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.assistant.app");

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public Object start(final IApplicationContext context) {
		// - Register application
		Activator.addReference(Application.APPLICATION_ID, this);

		//TODO Process application arguments
		// processArguments(context.getArguments());
		// final Display display = PlatformUI.createDisplay();
		// HarpoonRegistry.getRegistry().put(DISPLAY_ID, display);

		// - This line fires the creation of the perspective and all its contents. Also enters main loop
		Application.logger.log(Level.INFO, "Starting and creating the " + Application.APPLICATION_NAME
				+ ". Running workbench ApplicationWorkbenchAdvisor");
		return super.start(context, new ApplicationWorkbenchAdvisor());
	}
}

// - UNUSED CODE ............................................................................................
