//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.
package net.sf.gef.core.views;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface ISynchronizableView {
	// - M E T H O D - S E C T I O N ..........................................................................
	public void receiveSelection(Vector<Object> models);
}

// - UNUSED CODE ............................................................................................
