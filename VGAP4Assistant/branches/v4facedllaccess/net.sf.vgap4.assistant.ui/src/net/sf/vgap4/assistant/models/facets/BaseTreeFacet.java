//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.facets;

// - IMPORT SECTION .........................................................................................
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseTreeFacet extends AbstractTreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.facets");

	// - F I E L D - S E C T I O N ............................................................................
	private final Base	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseTreeFacet(final Base target) {
		super(target);
		delegate = target;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public AssistantNode getDelegate() {
		return delegate;
	}

	public String getFarming() {
		final Planet onPlanet = delegate.getOnPlanet();
		final StringBuffer buffer = new StringBuffer();
		buffer.append(onPlanet.getFieldNumber("climate")).append(" [");
		buffer.append(onPlanet.getFieldNumber("soil")).append("] ");
		buffer.append(Planet.farmingDecode(onPlanet.getFarmingLevel())).append(" - ");
		buffer.append(delegate.getFieldNumber("Farm")).append(" / ");
		buffer.append(delegate.getFarmEstimates());
		return buffer.toString();
	}

	public String getResources() {
		final StringBuffer buffer = new StringBuffer();
		//TODO Compose the mineral information quantities
		buffer.append("N:").append((delegate).getFieldNumber("ele N")).append("/").append(
				(delegate).getFieldNumber("Ore N"));
		buffer.append(" D:").append((delegate).getFieldNumber("ele D")).append("/").append(
				(delegate).getFieldNumber("Ore D"));
		buffer.append(" T:").append((delegate).getFieldNumber("ele T")).append("/").append(
				(delegate).getFieldNumber("Ore T"));
		buffer.append(" M:").append((delegate).getFieldNumber("ele M")).append("/").append(
				(delegate).getFieldNumber("Ore M"));
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
