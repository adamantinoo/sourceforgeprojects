//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.facets;

// - IMPORT SECTION .........................................................................................
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Ship;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipTreeFacet extends AbstractTreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.facets");

	// - F I E L D - S E C T I O N ............................................................................
	private Ship	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ShipTreeFacet(Ship target) {
		super(target);
		delegate = target;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public AssistantNode getDelegate() {
		return delegate;
	}

	//	public String getLocationString() {
	//		return delegate.getLocationString();
	//	}
	//
	//	public String getName() {
	//		return delegate.getIdentifier();
	//	}

	public String getCargoMessage() {
		return delegate.getCargoMessage();
	}
}

// - UNUSED CODE ............................................................................................
