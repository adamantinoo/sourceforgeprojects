//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.models.facets.WingDetailedFacet;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedWingFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger						logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final WingDetailedFacet	wingModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedWingFigure(final WingDetailedFacet facet) {
		super(facet.getModel());
		wingModel = facet;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void addAdditionalContents() {
		//TODO Add wing composition with a tooltip to show model characteristics.
		//TODO Add Speed and resources loadout (Fuel, Ord...).
		//TODO Add target information. This is common to some figures.

		final MultiLabelLine passageLabel = new MultiLabelLine();
		passageLabel.addColumn("Pod name:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		passageLabel.addColumn(wingModel.getName(), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);

		//- Add labels to this element for composition.
		this.append(passageLabel);
	}
}
// - UNUSED CODE ............................................................................................
