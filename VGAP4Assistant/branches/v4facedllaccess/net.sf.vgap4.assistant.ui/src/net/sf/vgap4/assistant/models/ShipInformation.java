//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipInformation extends AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= 3906934058195215773L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");

	// - M E T H O D - S E C T I O N ..........................................................................
	public void loadData(final String[] fieldNames, final String[] fields) {
		//- Read the field name/field value pairs from these arrays and store it inside the property list.
		for (int i = 1; i < fieldNames.length; i++) {
			//- Check for null values and empty strings.
			final String fieldName = fieldNames[i].trim();
			final String value = fields[i].trim();
			//			if ((null != value) && ("" != value))
			info.setProperty(fieldName, value);
		}

		// - Store the data fields in the cache attributes.
		idNumber = this.convert2Integer(this.getField("ID Number"));
		name = this.getField("Name");
		final int x = this.convert2Integer(this.getField("x"));
		final int y = this.convert2Integer(this.getField("y"));
		//		location = new Point(x, 3000 - y);
		location = new Point(x, y);
	}
}

// - UNUSED CODE ............................................................................................
