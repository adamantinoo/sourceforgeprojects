//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.facets;

// - IMPORT SECTION .........................................................................................
import net.sf.vgap4.assistant.models.Planet;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetTreeFacet extends AbstractTreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.viewers");

	// - F I E L D - S E C T I O N ............................................................................
	private Planet	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PlanetTreeFacet(Planet target) {
		super(target);
		this.delegate = target;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Return the visible name identifier for this Planet to be shown on Views. This name follows the next
	 * structure:
	 * <ul>
	 * <li>object unique identifier.</li>
	 * <li>Planet name.</li>
	 * <li>Planet decorators - this is a list of codes to signal the Farming Level, Contraband Level, Mineral
	 * Level and the presence of natives.</li>
	 * </ul>
	 */
	@Override
	public String getName() {
		StringBuffer buffer = new StringBuffer(this.delegate.getIdentifier());
		buffer.append("[").append(Planet.farmingDecode(delegate.getFarmingLevel())).append(delegate.getContrabandLevel());
		buffer.append(delegate.getPlanetClassification());
		if (delegate.hasNatives())
			buffer.append("N");
		else
			buffer.append("-");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	public Planet getDelegate() {
		return delegate;
	}

	/**
	 * Returns a string representing the natives and the planet mineral resources in the format:<br>
	 * [N number of natives] / [M unminned+surface ore in k / surface mineral ]
	 */
	public String getResources() {
		StringBuffer buffer = new StringBuffer();
		if (delegate.hasNatives())
			buffer.append("[N ").append(delegate.getTotalNatives()).append("] / [");
		else
			buffer.append("[N -] / [");
		buffer.append("M ").append((delegate).getMaxMinerals() / 1000).append("k]");
		return buffer.toString();
	}

	//	public String getLocationString() {
	//		return delegate.getLocationString();
	//	}

	public String getFarming() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(delegate.getFieldNumber("climate")).append(" [");
		buffer.append(delegate.getFieldNumber("soil")).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
