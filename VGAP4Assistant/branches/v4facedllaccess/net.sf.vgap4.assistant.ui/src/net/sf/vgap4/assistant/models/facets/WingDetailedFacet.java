//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.facets;

import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Wing;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class WingDetailedFacet /* implements IDetailedFacet */{
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.facets");

	// - F I E L D - S E C T I O N ............................................................................
	private final Wing	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WingDetailedFacet(final Wing unit) {
		delegate = unit;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public AssistantNode getModel() {
		return delegate;
	}

	public String getName() {
		return delegate.getName();
	}
}

// - UNUSED CODE ............................................................................................
