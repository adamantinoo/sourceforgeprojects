//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.models.facets.IAssistantFaceted;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AssistantNode extends PositionableUnit implements IAssistantFaceted {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	private static final long		serialVersionUID	= -5137898479615467215L;
	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	FLD_ID						= "AssistantNode.FLD_ID";

	// - M O D E L F I E L D S
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	private int									idNumber					= -1;
	/**
	 * Stores the planet name as identified in the source data. This is also a copy of the name found on the
	 * <code>PlanetInformation</code>.
	 */
	private String							name;
	/**
	 * Reference number to the latest imported turn data. The latest is considered the turn data with the
	 * highest turn number than has been parsed.
	 */
	private int									latestTurnNo			= -1;
	/** Stores the cache of the owner player code of the latest owner for this element. */
	//	private int									owner							= -1;
	private String							notes							= "";
	/**
	 * Stores the Friend or Foe status for this element. Map elements that do not belong to any player have an
	 * Undefined state.
	 */
	private String							FFStatus					= AssistantConstants.FFSTATUS_UNDEFINED;

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getIdNumber() {
		//		if(-1==idNumber)idNumber=this.
		return idNumber;
	}

	public String getIdentifier() {
		return this.getIdString() + " - " + getName();
	}

	public String getLocationString() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(4);
		nf.setMinimumFractionDigits(0);
		return this.getLocation().x + " - " + this.getLocation().y;
	}

	public String getIdString() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(3);
		return nf.format(this.idNumber);
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLatestTurnNumber() {
		return this.latestTurnNo;
	}

	public void setLatestTurnNumber(int latestTurnNo) {
		this.latestTurnNo = latestTurnNo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String text) {
		this.notes = text;
	}

	//	public abstract ITreeFacet getTreeFacet();

	protected void updateTurnReference(int turn) {
		latestTurnNo = Math.max(latestTurnNo, turn);
	}

	//	public void setOwner(int owner) {
	//		this.owner = owner;
	//	}
	public abstract int getOwner();

	//	public abstract void addTurnInformation(final int turn, final AssistantTurnInfo shipInfo);

	public abstract String getField(final String key);

	public abstract int getFieldNumber(final String key);

	public String getFFStatus() {
		return this.FFStatus;
	}

	public void setFFStatus(String status) {
		this.FFStatus = status;
	}
}

// - UNUSED CODE ............................................................................................
