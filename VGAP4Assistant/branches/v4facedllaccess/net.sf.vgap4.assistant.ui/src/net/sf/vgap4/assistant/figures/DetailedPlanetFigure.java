//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;

import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedPlanetFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final Planet	planetModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedPlanetFigure(final Planet model) {
		super(model);
		planetModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void addAdditionalContents() {
		//- Check if there is mineral information available.
		final String level = planetModel.infoLevel();
		if (AssistantConstants.INFOLEVEL_UNEXPLORED.equals(level)) {
			this.addNoInfo();
			return;
		}
		if (AssistantConstants.INFOLEVEL_NOINFO.equals(level)) {
			this.addNoInfo();
			return;
		}
		this.addNativeInfo(group);
		this.addMineralsInfo(group);
	}

	protected void addMineralsInfo(final Figure parent) {
		//DEBUG Check mineral values.
		final StandardLabel mineralLabel = new StandardLabel("Minerales:");
		mineralLabel.setFont(AssistantConstants.FONT_MAP_BOLD);
		parent.add(mineralLabel);

		final int mineralQty = planetModel.getMineralQty();
		final MineralBarFigure neutronium = new MineralBarFigure("N", AssistantConstants.COLOR_STANDARD_RED,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		neutronium.setLevels(mineralQty, planetModel.getFieldNumber("resN"), planetModel.getFieldNumber("eleN"));
		//		final MineralLine neutroniumLine = new MineralLine("N", neutronium);
		//		neutroniumLine.setValues(planetModel.getFieldNumber("resN"), planetModel.getFieldNumber("eleN"));
		//    if (true) neutroniumLine.setBorder(new LineBorder());
		parent.add(neutronium);
		neutronium.setToolTip(new Label(
				"Cantidad de Mineral (reserva/superficie) en el planeta. Proporcional al total de minerales detectados."));

		final MineralBarFigure duranium = new MineralBarFigure("D", AssistantConstants.COLOR_STANDARD_DARKBLUE,
				AssistantConstants.COLOR_STANDARD_BLUE);
		duranium.setLevels(mineralQty, planetModel.getFieldNumber("resD"), planetModel.getFieldNumber("eleD"));
		//		final MineralLine duraniumLine = new MineralLine( duranium);
		//		duraniumLine.setValues(planetModel.getFieldNumber("resD"), planetModel.getFieldNumber("eleD"));
		//    if (true) duraniumLine.setBorder(new LineBorder());
		parent.add(duranium);
		duranium.setToolTip(new Label(
				"Cantidad de Mineral (reserva/superficie) en el planeta. Proporcional al total de minerales detectados."));

		final MineralBarFigure tritanium = new MineralBarFigure("T", AssistantConstants.COLOR_STANDARD_DARKGREN,
				AssistantConstants.COLOR_STANDARD_GREEN);
		tritanium.setLevels(mineralQty, planetModel.getFieldNumber("resT"), planetModel.getFieldNumber("eleT"));
		//		final MineralLine tritaniumLine = new MineralLine("T", tritanium);
		//		tritaniumLine.setValues(planetModel.getFieldNumber("resT"), planetModel.getFieldNumber("eleT"));
		//    if (true) tritaniumLine.setBorder(new LineBorder());
		parent.add(tritanium);
		tritanium.setToolTip(new Label(
				"Cantidad de Mineral (reserva/superficie) en el planeta. Proporcional al total de minerales detectados."));

		final MineralBarFigure molybdenum = new MineralBarFigure("M", AssistantConstants.COLOR_BROWN,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		molybdenum.setLevels(mineralQty, planetModel.getFieldNumber("resM"), planetModel.getFieldNumber("eleM"));
		//		final MineralLine molybdenumLine = new MineralLine("M", molybdenum);
		//		molybdenumLine.setValues(planetModel.getFieldNumber("resM"), planetModel.getFieldNumber("eleM"));
		//    if (true) molybdenumLine.setBorder(new LineBorder());
		parent.add(molybdenum);
		molybdenum.setToolTip(new Label(
				"Cantidad de Mineral (reserva/superficie) en el planeta. Proporcional al total de minerales detectados."));
	}

	//	@Override
	//	protected Image createDetailedIcon() {
	//		//- Generate the image for the current element.
	//		final IconImageFactory imageFactory = new IconImageFactory();
	//		imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
	//		imageFactory.setInfoLevel(planetModel.infoLevel());
	//		if (planetModel.getTotalNatives() > 0) imageFactory.activateNatives();
	//		return imageFactory.generateImage();
	//	}

	private void addNativeInfo(final Figure parent) {
		final Vector<Label> tips = planetModel.getNativesInfo().getToolTips();
		final Iterator<Label> tit = tips.iterator();
		while (tit.hasNext()) {
			parent.add(tit.next());
		}
	}

	private void addNoInfo() {
		final StandardLabel passageLabel = new StandardLabel("Minerales: no disponible");
		passageLabel.setFont(AssistantConstants.FONT_MAP_BOLD);
		this.append(passageLabel);
	}
}
// - UNUSED CODE ............................................................................................
