//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseInformation extends AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= 8806434019146603117L;
	//	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	/** Unique identifier of the Planet where this base is build. */
	private int								planetId					= -1;
	/**
	 * Stores the numbers of all the natives that are found residing on the base. This numbers are diffrent form
	 * the Natives that are still found on the Planet.
	 */
	private Natives						nativeInformation;
	/** Structure with all the mining information available on this planet's turn data. */
	private MiningInformation	miningInfo;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseInformation(final String type) {
		super(type);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public MiningInformation getMiningInfo() {
		if (null == miningInfo)
			return new MiningInformation();
		else
			return miningInfo;
	}

	/**
	 * Get the mining information for all the minerals at once. This simplifies the interface because all this
	 * information is transferred with a single command. Destination will then understand the structure and
	 * extract the right information from the structure.
	 */
	public Vector<Mineral> getMiningInformation() {
		return this.getMiningInfo().getMiningInformation();
	}

	public Natives getNativesInfo() {
		if (null == nativeInformation)
			return new Natives();
		else
			return nativeInformation;
	}

	public int getPlanetId() {
		return planetId;
	}

	/**
	 * Loads reference fields with the information data for easy access and other post processing like the
	 * creation of other structures.
	 */
	@Override
	public void loadCachedData(final String[] fields) {
		super.loadCachedData(fields);
		//- Create internal structures.
		planetId = this.getFieldNumber("Planet ID");
		nativeInformation = new Natives(fields, 6);
		miningInfo = new MiningInformation(fields, 17);
	}
}

// - UNUSED CODE ............................................................................................
