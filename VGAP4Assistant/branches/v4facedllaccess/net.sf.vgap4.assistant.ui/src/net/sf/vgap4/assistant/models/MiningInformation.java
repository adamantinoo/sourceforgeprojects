//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class reads and processes the mineral information that is found on the list of fields receives as the
 * parameter. The data is extracted and composed into the four types of minerals. Information recovered is the
 * quantities of ore or mineral that can be found on the planet, mined or not mined.
 */
public class MiningInformation implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String	NEUTRONIUM_NAME				= "NEUTRONIUM_NAME";
	public static final String	DURANIUM_NAME					= "DURANIUM_NAME";
	public static final String	TRITANIUM_NAME				= "TRITANIUM_NAME";
	public static final String	MOLYBDENUM_NAME				= "MOLYBDENUM_NAME";
	public static final int			NEUTRONIUM_ID					= 0;
	public static final int			DURANIUM_ID						= 1;
	public static final int			TRITANIUM_ID					= 2;
	public static final int			MOLYBDENUM_ID					= 3;
	public static final String	FIELD_UNMINNED				= "MiningInformation.FIELD_UNMINNED";
	public static final String	FIELD_DENSITY					= "MiningInformation.FIELD_DENSITY";
	public static final String	FIELD_ORESURFACE			= "MiningInformation.FIELD_ORESURFACE";
	public static final String	FIELD_MINERALSURFACE	= "MiningInformation.FIELD_MINERALSURFACE";
	private static final long		serialVersionUID			= -1004029385914316762L;
	private static Logger				logger								= Logger.getLogger("net.sf.vgap4.projecteditor.model");

	public static String decode(final int minId) {
		if (MiningInformation.NEUTRONIUM_ID == minId) return "N";
		if (MiningInformation.DURANIUM_ID == minId) return "D";
		if (MiningInformation.TRITANIUM_ID == minId) return "T";
		if (MiningInformation.MOLYBDENUM_ID == minId) return "M";
		return "U";
	}

	// - F I E L D - S E C T I O N ............................................................................
	private final Vector<Mineral>	minerals	= new Vector<Mineral>(1);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * Convert data in the input line into the corresponding fields of this structure. Mineral information
	 * includes all VGAP information related to mining even this may appear at other structures.<br>
	 * Contained data are:
	 * <ul>
	 * <li>mineral extraction percentages</li>
	 * <li>ore and mineral contents at the planet surface</li>
	 * </ul>
	 */
	//	public MiningInformation(String[] planetFields) {
	//		// - Create an entry for every mineral type on the input data
	//		minerals.setElementAt(new Mineral(NEUTRONIUM_NAME, planetFields[23], planetFields[27], planetFields[31],
	//				planetFields[35]), NEUTRONIUM_ID);
	//		minerals.setElementAt(new Mineral(DURANIUM_NAME, planetFields[24], planetFields[28], planetFields[32],
	//				planetFields[36]), DURANIUM_ID);
	//		minerals.setElementAt(new Mineral(TRITANIUM_NAME, planetFields[25], planetFields[29], planetFields[33],
	//				planetFields[37]), TRITANIUM_ID);
	//		minerals.setElementAt(new Mineral(MOLYBDENUM_NAME, planetFields[26], planetFields[30], planetFields[34],
	//				planetFields[38]), MOLYBDENUM_ID);
	//	}
	public MiningInformation() {
	}

	public MiningInformation(final String[] planetFields, final int startDataIndex) {
		final int oreStartIndex = startDataIndex;
		final int mineralStartIndex = startDataIndex + 4;
		minerals.setSize(4);
		minerals.setElementAt(new Mineral(MiningInformation.NEUTRONIUM_NAME, planetFields[oreStartIndex],
				planetFields[mineralStartIndex]), MiningInformation.NEUTRONIUM_ID);
		minerals.setElementAt(new Mineral(MiningInformation.DURANIUM_NAME, planetFields[oreStartIndex + 1],
				planetFields[mineralStartIndex + 1]), MiningInformation.DURANIUM_ID);
		minerals.setElementAt(new Mineral(MiningInformation.TRITANIUM_NAME, planetFields[oreStartIndex + 2],
				planetFields[mineralStartIndex + 2]), MiningInformation.TRITANIUM_ID);
		minerals.setElementAt(new Mineral(MiningInformation.MOLYBDENUM_NAME, planetFields[oreStartIndex + 3],
				planetFields[mineralStartIndex + 3]), MiningInformation.MOLYBDENUM_ID);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Get the mining information for all the minerals at once. This simplifies the interface because all this
	 * information is transferred with a single command. Destination will then understand the structure and
	 * extract the right information from the structure.
	 */
	public Vector<Mineral> getMiningInformation() {
		return minerals;
	}

	public int getUnminned(final int neutroniumId) {
		return minerals.get(neutroniumId).getUnminned();
	}

}

//- CLASS IMPLEMENTATION ...................................................................................
class DataFormatter implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= 6877368012779715581L;

	// - M E T H O D - S E C T I O N ..........................................................................
	protected int convert2Integer(final String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}
}

// - CLASS IMPLEMENTATION ...................................................................................
class Mineral extends DataFormatter implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= -933224270178425247L;

	// - F I E L D - S E C T I O N ............................................................................
	private final String			name;
	private int								density						= 0;
	private int								surfaceOre				= 0;
	private int								surfaceMineral		= 0;
	private int								unminned					= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Mineral(final String mineralName, final String ore, final String ele) {
		// - Create a mineral structure for each group of data.
		name = mineralName;
		density = 0;
		surfaceOre = this.convert2Integer(ore.trim());
		surfaceMineral = this.convert2Integer(ele.trim());
		unminned = 0;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * The constructor initializes the structure with the content data that is received in the parameters. The
	 * structure records the name of the mineral and the four quantities that describe the mineral contents on a
	 * <code>Planet</code>.
	 */
	public Mineral(final String mineralName, final String mineralDensity, final String ore, final String ele,
			final String reserve) {
		// - Create a mineral structure for each group of data.
		name = mineralName;
		density = this.convert2Integer(mineralDensity.trim());
		surfaceOre = this.convert2Integer(ore.trim());
		surfaceMineral = this.convert2Integer(ele.trim());
		unminned = this.convert2Integer(reserve.trim());
	}

	public int getDensity() {
		return density;
	}

	// - G E T T E R S / S E T T E R S
	public String getName() {
		return name;
	}

	public int getSurfaceMineral() {
		return surfaceMineral;
	}

	public int getSurfaceOre() {
		return surfaceOre;
	}

	public int getTotalOre() {
		return surfaceOre + unminned;
	}

	public int getUnminned() {
		return unminned;
	}
}

// - UNUSED CODE ............................................................................................
