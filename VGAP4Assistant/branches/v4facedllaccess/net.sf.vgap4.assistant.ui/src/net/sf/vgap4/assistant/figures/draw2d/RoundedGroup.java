//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class RoundedGroup extends RoundedRectangle {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger		= Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");
	private GridLayout	grid;
	public Label				groupName	= new StandardLabel();

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RoundedGroup() {
		setCornerDimensions(new Dimension(6, 6));
		grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 3;
		grid.marginWidth = 3;
		grid.numColumns = 1;
		grid.verticalSpacing = 2;
		setLayoutManager(grid);
		groupName.setText("indefinido");
		this.add(groupName);
		this.setSize(getPreferredSize());
	}

	public RoundedGroup(String text, int bold) {
		this();
		groupName.setText(text);
		if (SWT.BOLD == bold) groupName.setFont(AssistantConstants.FONT_MAP_BOLD);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setText(String text) {
		groupName.setText(text);
		this.setSize(getPreferredSize());
		this.repaint();
	}

	public void reset() {
		this.removeAll();
		this.add(groupName);
	}
}

// - UNUSED CODE ............................................................................................
