//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
/** Generates the icon with color and the mineral initial to be drawn in the mineral bars. */
public class LetterImageFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger			= Logger.getLogger("net.sf.vgap4.assistant.factories");
	private static Hashtable<String, Image>	cache				= new Hashtable<String, Image>();

	// - F I E L D - S E C T I O N ............................................................................
	private String													initial			= "U";
	private Color														backColor		= AssistantConstants.COLOR_PLANET_UNEXPLORED;
	private final int												drawingSize	= 12;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public Image getImage() {
		//- Check the cache for an image with the same initial.
		Image image = Activator.getImageByKey(initial);
		if (null == image) {
			image = new Image(Display.getDefault(), drawingSize, drawingSize);
			final GC gc = new GC(image);

			// - Get drawing location. This should be already displaced from the top-left.
			final Rectangle bound = new Rectangle(0, 0, drawingSize, drawingSize);

			//- Draw the rounded border and then place the letter.
			gc.setBackground(backColor);
			gc.fillRectangle(bound.x, bound.y, bound.width, bound.height);
			gc.setForeground(ColorConstants.black);
			gc.drawRoundRectangle(bound.x, bound.y, bound.width - 1, bound.height - 1, 2, 2);
			gc.setFont(AssistantConstants.FONT_MAP_DEFAULT);
			gc.setForeground(ColorConstants.white);
			gc.drawText(initial, 3, -1, true);

			gc.dispose();

			//- Cache the new image.
			Activator.addImage(initial, image);
		}
		return image;
	}

	public void setColor(final Color newColor) {
		backColor = newColor;
	}

	//	public Image getImage(String key) {
	//		Image image = cache.get(key);
	//		if (null == image) {
	//			image = getImage();
	//			cache.put(key, image);
	//			return image;
	//		} else
	//			return image;
	//	}

	public void setInitial(final String initial) {
		this.initial = initial;
	}

	//	public static void register(String key, Image obj) {
	//		cache.put(key, obj);
	//	}
}

// - UNUSED CODE ............................................................................................
