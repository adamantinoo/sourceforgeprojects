//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class Spot extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger				= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	private Vector<AssistantNode>	spotContents	= new Vector<AssistantNode>();
	public boolean								hasBase				= false;
	public boolean								hasPlanet			= false;
	public boolean								hasShip				= false;
	public boolean								hasPods				= false;
	public boolean								hasWings			= false;
	private AssistantNode					representative;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Spot() {
	}

	public Spot(LocationCluster cluster) {
		//		this.locationObjects = cluster;

		//TODO Load the cached data in the spot fields from the cluster representative.
		AssistantNode representative = cluster.getRepresentative();
		setLocation(representative.getLocation());
		setIdNumber(representative.getIdNumber());
		setName(representative.getName());
		setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add a new element to this location spot. Depending on the element to be added we can select a
	 * representative this is the top priority element in the spot in this order:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * Representative delegate may be calculated when adding (that is done more then once) or delayed until the
	 * first request for a representative that will be executed once during the life of the Spot.
	 * 
	 * @param mapElement
	 *          the model node to be added to this spot
	 */
	public void add(AssistantNode mapElement) {
		if (mapElement instanceof Ship) hasShip = true;
		if (mapElement instanceof Planet) hasPlanet = true;
		if (mapElement instanceof Base) hasBase = true;
		if (mapElement instanceof Pod) hasPods = true;
		if (mapElement instanceof Wing) hasWings = true;
		spotContents.add(mapElement);
	}

	public boolean hasContents() {
		if (null != representative) return true;
		return false;
	}

	public boolean hasNatives() {
		if (hasPlanet) {
			Iterator<AssistantNode> cit = spotContents.iterator();
			while (cit.hasNext()) {
				AssistantNode element = cit.next();
				if (element instanceof Planet) {
					if (((Planet) element).getTotalNatives() > 0) return true;
				}
			}
		}
		return false;
	}

	/**
	 * Tis method delays the calculation of the representativo for this spot and also reduces the possibility to
	 * return null values when new model objects are added to the Map. As explained on the <code>add()</code>
	 * method the priority to select a representative is:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * The representative is calculated by applying a priority factor to each element depending on its class and
	 * then gettinh the top most priority one. This can be done in a single pass over the elements list.
	 * 
	 * @return the model node selected as a representative prioritized class node.
	 */
	public AssistantNode getRepresentative() {
		//- Check if the representative is cached.
		if (null == representative) {
			//- Lookup for a representative. If the size of the spot is 1 then it is clear who it it.
			if (1 == this.spotContents.size()) {
				setRepresentative(spotContents.firstElement());
				return representative;
			} else {
				double repPriority = -1;
				Iterator<AssistantNode> sit = spotContents.iterator();
				while (sit.hasNext()) {
					AssistantNode element = sit.next();
					double priority = 0;
					if (element instanceof Ship) priority = element.getIdNumber() * 1000.0;
					if (element instanceof Planet) priority = element.getIdNumber() * 1000.0 * 1000.0;
					if (element instanceof Base) priority = element.getIdNumber() * 1000.0 * 1000.0 * 1000.0;
					if (element instanceof Pod) priority = element.getIdNumber() * 1.0;
					if (element instanceof Wing) priority = element.getIdNumber() * 10.0;
					if (priority > repPriority) {
						setRepresentative(element);
						repPriority = priority;
					}
				}
				return representative;
			}
		} else
			return representative;
	}

	public void setRepresentative(AssistantNode representative) {
		this.representative = representative;

		//- Load the cached data in the spot fields from the cluster representative.
		setLocation(representative.getLocation());
		setIdNumber(representative.getIdNumber());
		setName(representative.getName());
		setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	public boolean isMultiple() {
		if (this.spotContents.size() > 1)
			return true;
		else
			return false;
	}

	public String getPreferredShape() {
		if (hasBase) return "Base";
		if (hasPlanet) return "Planet";
		if (hasShip) return "Ship";
		return "Undefined";
	}

	public Vector<AssistantNode> getContents() {
		return this.spotContents;
	}

	public String infoLevel() {
		if (hasPlanet) {
			Planet planet = locatePlanet();
			if (null != planet) return planet.infoLevel();
		}
		return AssistantConstants.INFOLEVEL_UNEXPLORED;
	}

	private Planet locatePlanet() {
		Iterator<AssistantNode> cit = spotContents.iterator();
		while (cit.hasNext()) {
			AssistantNode element = cit.next();
			if (element instanceof Planet) { return (Planet) element; }
		}
		return null;
	}

	@Override
	public int getOwner() {
		return this.representative.getOwner();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("Spot[");
		if (hasBase)
			buffer.append("B");
		else
			buffer.append("-");
		if (hasPlanet)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasShip)
			buffer.append("S");
		else
			buffer.append("-");
		if (hasPods)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasWings)
			buffer.append("W");
		else
			buffer.append("-");
		buffer.append(this.getRepresentative().toString());
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
