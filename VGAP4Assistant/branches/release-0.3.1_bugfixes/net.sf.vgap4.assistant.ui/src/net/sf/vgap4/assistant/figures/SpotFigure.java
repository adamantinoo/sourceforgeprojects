//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.factories.LetterImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	protected static final int	BOUNDARIES	= 1;
	protected static final int	MARGIN			= 1;

	// - F I E L D - S E C T I O N ............................................................................
	/** The positionable layout that is used to draw any default Map element. */
	protected final XYLayout		xylay				= new XYLayout();
	/** Reference to the icon drawing class that represents this unit. */
	/** Label that stores the icon. It has no text but may be assigned the Identification text. */
	protected Label							iconLabel		= new StandardLabel();
	//	/** Label with the numerical serial identifier. This identifier is padded with zeroes to the left. */
	//	protected final Label				idLabel			= new ZeroPaddingLabel("000");
	/** Label with the unit name. */
	protected final Label				nameLabel		= new StandardLabel();
	{
		iconLabel.setIcon(new LetterImageFactory().getImage());
		iconLabel.setText("");
		iconLabel.setIconAlignment(PositionConstants.LEFT);
		iconLabel.setIconTextGap(1);
		iconLabel.setSize(iconLabel.getPreferredSize());
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotFigure() {
		this.setLayoutManager(xylay);
		this.add(iconLabel);
		//		this.add(idLabel);
		this.add(nameLabel);
		this.relayout();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Point getHotSpot() {
		return new Point(AssistantConstants.SIZE_ICONIMAGE + SpotFigure.BOUNDARIES, AssistantConstants.SIZE_ICONIMAGE
				+ SpotFigure.BOUNDARIES);
	}

	public Image getIconImage() {
		return iconLabel.getIcon();
	}

	/**
	 * Before setting the top-left corner location we have to adjust the element by the Hot Spot displacement
	 * because the model coordinates are the coordinates for the planet center. The planet image surrounds that
	 * point.
	 */
	public void setCoordinates(final Point location) {
		final Point offset = this.getHotSpot();
		final Point coords = location;
		coords.x -= offset.x;
		coords.y -= offset.y;
		this.setLocation(coords);
	}

	public void setIconImage(final Image newIcon) {
		iconLabel.setIcon(newIcon);
		this.repaint();
	}

	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed
	 * is represented as a numeric label with the speed in knots.
	 */
	public void setId(final int newId) {
		try {
			final NumberFormat nf = NumberFormat.getIntegerInstance();
			nf.setMinimumIntegerDigits(3);
			nf.setMaximumFractionDigits(0);
			iconLabel.setText(new Integer(newId).toString());
		} catch (final Exception e) {
			iconLabel.setText(new Integer(newId).toString());
		}
		this.relayout();
	}

	public void setName(final String newName) {
		nameLabel.setText(newName);
		this.relayout();
	}

	protected String decodeSelectionCode(final int selected) {
		if (EditPart.SELECTED_NONE == selected) return "SELECTED_NONE";
		if (EditPart.SELECTED == selected) return "SELECTED";
		if (EditPart.SELECTED_PRIMARY == selected) return "SELECTED_PRIMARY";
		return "UNDEFINED";
	}

	protected void relayout() {
		// - Position ICON. Top-left corner of the figure.
		Dimension iconicSize = iconLabel.getPreferredSize();
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = SpotFigure.BOUNDARIES;
		elementLocation.y = SpotFigure.BOUNDARIES;
		elementLocation.width = iconicSize.width + SpotFigure.MARGIN;
		elementLocation.height = iconicSize.height;
		xylay.setConstraint(iconLabel, elementLocation);

		//- Position NAME. Below the idLabel.
		elementLocation = new Rectangle();
		//		elementLocation.x = iconicSize.width + SpotFigure.MARGIN;
		elementLocation.x = iconLabel.getIconBounds().width + SpotFigure.BOUNDARIES;
		elementLocation.y = iconLabel.getPreferredSize().height + SpotFigure.MARGIN - 3;// Move up 3 pixels
		elementLocation.width = nameLabel.getPreferredSize().width;
		elementLocation.height = nameLabel.getPreferredSize().height;
		xylay.setConstraint(nameLabel, elementLocation);

		//- Update the layout and the global size.
		this.layout();
		this.repaint();
		//DEBUG Check if this line is really needed.
		this.setSize(this.getPreferredSize());
	}
}
// - UNUSED CODE ............................................................................................
