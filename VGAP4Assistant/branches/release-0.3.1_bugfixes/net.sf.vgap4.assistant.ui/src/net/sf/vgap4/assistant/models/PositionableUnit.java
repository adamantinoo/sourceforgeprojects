//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractEndPoint;

import org.eclipse.draw2d.geometry.Point;

import net.sf.gef.core.model.Route;
import net.sf.gef.core.model.RouteEndPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class PositionableUnit extends AbstractEndPoint {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	NAME_PROP					= "PositionableUnit.NAME_PROP";
	public static final String	PROP_LOCATION			= "PositionableUnit.PROP_LOCATION";
	private static final long		serialVersionUID	= -9007441570957929333L;
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the location of the unit in the diagram. Contains the x,y coordinates to locate the unit in the
	 * diagram coordinate system.
	 */
	private Point								location;
	private Vector<Route>				sourceConnections	= new Vector<Route>();
	private Vector<Route>				targetConnections	= new Vector<Route>();

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	@Override
	public void addConnection(final Route wire) {
		if ((wire == null) || (wire.getSource() == wire.getTarget()))
			throw new IllegalArgumentException("Connections cannot be null or closed.");
		if (wire.getSource() == this) {
			sourceConnections.add(wire);
			this.firePropertyChange(RouteEndPoint.SOURCE_CONNECTIONS_PROP, null, wire);
		} else if (wire.getTarget() == this) {
			targetConnections.add(wire);
			this.firePropertyChange(RouteEndPoint.TARGET_CONNECTIONS_PROP, null, wire);
		}
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Return a copy of the location for processing. Do not allow that side effect modify the location or other
	 * fields without the use of accessors.
	 */
	public Point getLocation() {
		if (null == location) location = new Point(0, 0);
		return location.getCopy();
	}

	/**
	 * Return the list of outgoing connections that apply to this point.
	 */
	@Override
	public List<Route> getSourceConnections() {
		return sourceConnections;
	}

	/**
	 * Returns the list of incoming connections that apply to this point.
	 */
	@Override
	public List<Route> getTargetConnections() {
		return targetConnections;
	}

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	@Override
	public void removeConnection(final Route conn) {
		if (conn == null) throw new IllegalArgumentException("Connections cannot be null");
		if (conn.getSource() == this) {
			sourceConnections.remove(conn);
			this.firePropertyChange(RouteEndPoint.SOURCE_CONNECTIONS_PROP, conn, null);
		} else if (conn.getTarget() == this) {
			targetConnections.remove(conn);
			this.firePropertyChange(RouteEndPoint.TARGET_CONNECTIONS_PROP, conn, null);
		}
	}

	public void setLocation(final Point newLocation) {
		final Point oldLocation = location;
		location = newLocation;
		this.firePropertyChange(PositionableUnit.PROP_LOCATION, oldLocation, location);
	}

	@Override
	public void setSource(final Route wire) {
		sourceConnections = new Vector<Route>(1, 1);
		sourceConnections.add(wire);
	}

	@Override
	public void setTarget(final Route wire) {
		targetConnections = new Vector<Route>(1, 1);
		targetConnections.add(wire);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append('\n').append("[PositionableUnit:");
		buffer.append("location=").append(location.x).append("-").append(location.y);
		buffer.append("]");
		return buffer.toString();
	}

}
// - UNUSED CODE ............................................................................................
