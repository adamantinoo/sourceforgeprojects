//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.Map;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.EditPolicy;

import net.sf.vgap4.assistant.figures.IDetailedFigure;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.projecteditor.editparts.AssistantNodeEditPart;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedCommonEditPart extends AssistantNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedCommonEditPart() {
	//	}

	public Dimension getSize() {
		return new Dimension(this.getFigure().getPreferredSize());
	}

	//	@Override
	//	public void propertyChange(PropertyChangeEvent evt) {
	//		String prop = evt.getPropertyName();
	//		super.propertyChange(evt);
	//	}

	//	@Override
	//	public void setSelected(final int value) {
	//		super.setSelected(value);
	//	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("[DetailedCommonEditPart:");
		buffer.append(((AssistantNode) this.getModel()).getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	protected void applyGraphResults(final DirectedGraph graph, final Map<DetailedCommonEditPart, Node> map) {
		try {
			final Node node = map.get(this);
			this.getFigure().setSize(node.width, node.height);
			this.getFigure().setBounds(new Rectangle(node.x, node.y, node.width, node.height));
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final IFigure fig = this.getFigure();
		if (fig instanceof IDetailedFigure) ((IDetailedFigure) fig).createContents();
		//		super.refreshVisuals();
	}
}
// - UNUSED CODE ............................................................................................
