//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.views;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import net.sf.vgap4.assistant.editor.GraphicalEditor;
import net.sf.vgap4.assistant.factories.DetailedEditPartFactory;
import net.sf.vgap4.assistant.factories.DetailedFigureFactory;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.DetailedDiagram;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.projecteditor.editparts.ISelectablePart;

// - CLASS IMPLEMENTATION ...................................................................................
public class GraphicalDetailedView extends ViewPart implements ISelectionListener, ISelectionChangedListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String	ID							= "net.sf.vgap4.assistant.views.GraphicalDetailedView.id";
	private static Logger				logger					= Logger.getLogger("net.sf.vgap4.assistant.views");

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/** The view cannot be an editor at the same time, so delegate all editor actions to this editor. */
	GraphicalDetailedEditor			detailEditor		= null;
	/** This is the root of the editor's model. */
	private DetailedDiagram			editorContainer	= new DetailedDiagram();
	private Composite						viewerRoot;
	private IViewSite						viewSite;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GraphicalDetailedView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(GraphicalDetailedView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.<br>
	 * This class will set as the top presentation element a new <code>GraphicalDetailedEditor</code> that
	 * will present the selection received as a new MVC pattern
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// - Create a new editor and initialize it based on this view.
		this.viewerRoot = parent;
		this.detailEditor = new GraphicalDetailedEditor(parent, this);
		//		detailEditor.initializeGraphicalViewer();
		//		detailEditor.configureGraphicalViewer();
	}

	@Override
	public void dispose() {
		//		// - Remove this from the selection provider.
		//		this.getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(this);
		// - Unregister this from the SelectionInfoView selection provider.
		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		if (null != provider) ((ISelectionProvider) provider).removeSelectionChangedListener(this);
		super.dispose();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		this.viewSite = site;
		super.init(site);

		//		// - Add this editor selection listener to the list of listeners of this window
		//		this.getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
		// - Register this as a listener to the SelectionInfoView selection provider.
		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);
	}

	/**
	 * This event is fired any time the selection in the <code>SelectionInfoView</code> is changed. This
	 * method should get the selection parts that match a <code>ISelectablePart</code> and then create their
	 * visualization page to be added to the presentation list.<br>
	 * The parameter is a selection event that contains the final selection.<br>
	 * If the selection is a single object then visualize all their contents, but if the selection are multiple
	 * object, present them in the reduced form and let the user to click on them to expand their contents.
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
	 */
	public void selectionChanged(final IWorkbenchPart editorPart, final ISelection selection) {
		if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
			processSelection((StructuredSelection) selection);
			//			//TODO Apply all the filters that are defined on the list.
			//			//TODO Filter the EditParts that match the proper interface
			//			//			final StructuredSelection selectionContent = ;
			//			//			final StructuredSelection sel = (StructuredSelection) selection;
			//			final Iterator<Object> sit = ((StructuredSelection) selection).iterator();
			//			//TODO Check if the selection are EditParts or Model objects. Depending on this generate the new model.
			//			//- Get the model elements of the selection and move them to the Editor container.
			//			this.editorContainer.clear();
			//			//			final Vector<AssistantNode> models = new Vector<AssistantNode>();
			//
			//			while (sit.hasNext()) {
			//				final Object element = sit.next();
			//				if (element instanceof ISelectablePart) {
			//					//- Get the model of this part.
			//					Object model = ((AbstractEditPart) element).getModel();
			//					if (model instanceof Spot) {
			//						this.editorContainer.addChild(((Spot) model).getContents());
			//						continue;
			//					}
			//					if (model instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) model);
			//				}
			//				if (element instanceof Spot) {
			//					this.editorContainer.addChild(((Spot) element).getContents());
			//					continue;
			//				}
			//				if (element instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) element);
			//			}
			//			//TODO Notify the editor that the model has changed.
			//			this.editorContainer.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
		}
	}

	public void selectionChanged(final SelectionChangedEvent event) {
		//- Get the selection from the event.
		final ISelection selection = event.getSelection();
		if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
			processSelection((StructuredSelection) selection);
			//			final StructuredSelection selectionContent = (StructuredSelection) selection;
			//			//			final boolean singleSelected = true;
			//			this.updateMultipleSelection(selectionContent, null);
			//TODO This now is not used
			//						updateSingleSelection(selectedPart);
		}
	}

	private void processSelection(StructuredSelection selection) {
		//TODO Apply all the filters that are defined on the list.
		//TODO Filter the EditParts that match the proper interface
		//			final StructuredSelection selectionContent = ;
		//			final StructuredSelection sel = (StructuredSelection) selection;
		final Iterator<Object> sit = (selection).iterator();
		//TODO Check if the selection are EditParts or Model objects. Depending on this generate the new model.
		//- Get the model elements of the selection and move them to the Editor container.
		this.editorContainer.clear();
		//			final Vector<AssistantNode> models = new Vector<AssistantNode>();

		while (sit.hasNext()) {
			final Object element = sit.next();
			if (element instanceof ISelectablePart) {
				//- Get the model of this part.
				Object model = ((AbstractEditPart) element).getModel();
				if (model instanceof Spot) {
					this.editorContainer.addChild(((Spot) model).getContents());
					continue;
				}
				if (model instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) model);
			}
			if (element instanceof Spot) {
				this.editorContainer.addChild(((Spot) element).getContents());
				continue;
			}
			if (element instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) element);
		}
		//TODO Notify the editor that the model has changed.
		this.editorContainer.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
	}

	public DetailedDiagram getContainer() {
		return this.editorContainer;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		this.detailEditor.setFocus();
	}

}

class GraphicalDetailedEditor extends GraphicalEditor {
	private static Logger					logger	= Logger.getLogger("net.sf.vgap4.assistant.views");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String		ID			= "net.sf.vgap4.assistant.editors.GraphicalDetailedEditor.id";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private GraphicalDetailedView	detailedView;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GraphicalDetailedEditor(Composite parent, GraphicalDetailedView detailedView) {
		try {
			setEditDomain(new DefaultEditDomain(this));
			this.detailedView = detailedView;
			// - Register the view. This will remove the requirement to have the view declared as a static singleton
			Activator.addReference(ID, this);

			//- Access the selection editor to copy the initialization to this view editor.
			init(this.detailedView.getSite());
			this.createGraphicalViewer(parent);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public DetailedDiagram getContents() {
		if (null != detailedView)
			return this.detailedView.getContainer();
		else
			return new DetailedDiagram();
	}

	public void init(IWorkbenchPartSite site) throws PartInitException {
		EmptyEditorSite editorSite = new EmptyEditorSite(site);
		setSite(editorSite);
		setInput(null);
		getCommandStack().addCommandStackListener(this);
		//		// - Add this editor selection listener to the list of listeners of this window
		//		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
		initializeActionRegistry();
	}

	@Override
	protected void initializeGraphicalViewer() {
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(getContents()); // Set the contents of this graphical.
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setRootEditPart(new ScalableRootEditPart());
		viewer.setEditPartFactory(new DetailedEditPartFactory(new DetailedFigureFactory()));
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		//		// - Register this as a listener to the SelectionInfoView selection provider.
		//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		//		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);

		//		// configure the context menu provider
		//		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		//		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		//		viewer.setContextMenu(cmProvider);
		//		getSite().registerContextMenu(cmProvider, viewer);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Do nothing or we can bypass signaling to the main editor

	}

	//	@Override
	//	public void dispose() {
	//		// - Unregister this from the SelectionInfoView selection provider.
	//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
	//		if (null != provider) ((ISelectionProvider) provider).removeSelectionChangedListener(this);
	//		super.dispose();
	//	}

}

class EmptyEditorSite implements IEditorSite {

	private IWorkbenchPartSite	workbenchSite;

	public EmptyEditorSite(IWorkbenchPartSite site) {
		this.workbenchSite = site;
	}

	public Object getAdapter(Class adapter) {
		return workbenchSite.getAdapter(adapter);
	}

	public String getId() {
		return workbenchSite.getId();
	}

	public IKeyBindingService getKeyBindingService() {
		return workbenchSite.getKeyBindingService();
	}

	public IWorkbenchPage getPage() {
		return workbenchSite.getPage();
	}

	public IWorkbenchPart getPart() {
		return workbenchSite.getPart();
	}

	public String getPluginId() {
		return workbenchSite.getPluginId();
	}

	public String getRegisteredName() {
		return workbenchSite.getRegisteredName();
	}

	public ISelectionProvider getSelectionProvider() {
		return workbenchSite.getSelectionProvider();
	}

	public Object getService(Class api) {
		return workbenchSite.getService(api);
	}

	public Shell getShell() {
		return workbenchSite.getShell();
	}

	public IWorkbenchWindow getWorkbenchWindow() {
		return workbenchSite.getWorkbenchWindow();
	}

	public boolean hasService(Class api) {
		return workbenchSite.hasService(api);
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuManager, selectionProvider);
	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuId, menuManager, selectionProvider);
	}

	public void setSelectionProvider(ISelectionProvider provider) {
		workbenchSite.setSelectionProvider(provider);
	}

	public IEditorActionBarContributor getActionBarContributor() {
		// TODO Auto-generated method stub
		return null;
	}

	public IActionBars getActionBars() {
		// TODO Auto-generated method stub
		return null;
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}
}
// - UNUSED CODE ............................................................................................
