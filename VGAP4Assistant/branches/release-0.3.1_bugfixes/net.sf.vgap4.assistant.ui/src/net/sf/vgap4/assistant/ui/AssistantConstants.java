//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.ui;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface AssistantConstants {
	// - G L O B A L - S E C T I O N ..........................................................................
	// - C O L O R S
	Color		COLOR_STANDARD_RED					= ColorConstants.red;
	Color		COLOR_STANDARD_ORANGE				= ColorConstants.orange;
	Color		COLOR_STANDARD_DARKBLUE			= ColorConstants.darkBlue;
	Color		COLOR_STANDARD_BLUE					= ColorConstants.blue;
	Color		COLOR_STANDARD_DARKGREN			= ColorConstants.darkGreen;
	Color		COLOR_STANDARD_GREEN				= ColorConstants.green;

	Color		COLOR_BROWN									= new Color(Display.getCurrent(), 204, 102, 0);

	Color		COLOR_BRILLIANT_RED					= new Color(Display.getDefault(), 0xFF, 0x33, 0x00);
	Color		COLOR_LIGHT_RED							= new Color(Display.getDefault(), 0xFF, 0x80, 0x80);
	Color		COLOR_MEDIUM_RED						= new Color(Display.getDefault(), 0xE0, 0x33, 0x00);
	Color		COLOR_DARK_RED							= new Color(Display.getDefault(), 0x33, 0x00, 0x00);
	Color		COLOR_BRILLIANT_GREEN				= new Color(Display.getDefault(), 0x00, 0xFF, 0x00);
	Color		COLOR_LIGHT_GREEN						= new Color(Display.getDefault(), 0x80, 0xFF, 0x80);
	Color		COLOR_MEDIUM_GREEN					= new Color(Display.getDefault(), 0x00, 0xE0, 0x00);
	Color		COLOR_DARK_GREEN						= new Color(Display.getDefault(), 0x00, 0xA0, 0x00);
	Color		COLOR_BRILLIANT_BLUE				= new Color(Display.getDefault(), 0x00, 0x00, 0xFF);
	Color		COLOR_LIGHT_BLUE						= new Color(Display.getDefault(), 0x80, 0x80, 0xFF);
	Color		COLOR_MEDIUM_BLUE						= new Color(Display.getDefault(), 0x00, 0x10, 0xE0);
	Color		COLOR_DARK_BLUE							= new Color(Display.getDefault(), 0x00, 0x00, 0x33);
	Color		COLOR_BRILLIANT_YELLOW			= new Color(Display.getDefault(), 0xFF, 0xFF, 0x00);
	Color		COLOR_LIGHTEST_YELLOW				= new Color(Display.getDefault(), 0xFE, 0xFE, 0xEE);
	Color		COLOR_LIGHT_YELLOW					= new Color(Display.getDefault(), 0x80, 0x80, 0x00);
	Color		COLOR_MEDIUM_YELLOW					= new Color(Display.getDefault(), 0xE0, 0xE0, 0x00);
	Color		COLOR_DARK_YELLOW						= new Color(Display.getDefault(), 0x33, 0x33, 0x00);

	Color		COLOR_LIGHTEST_GRAY					= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);

	Color		COLOR_BASE_OWNED						= COLOR_DARK_GREEN;
	Color		COLOR_BASE_ENEMY						= COLOR_BRILLIANT_RED;
	Color		COLOR_PLANET_UNEXPLORED			= ColorConstants.lightGray;
	Color		COLOR_PLANET_NOINFO					= ColorConstants.darkGray;
	Color		COLOR_PLANET_INFO						= COLOR_MEDIUM_BLUE;
	Color		COLOR_PLANET_OBSOLETE				= new Color(Display.getDefault(), 0xE4, 0x81, 0xE4);
	Color		COLOR_SHIP_DEFAULT					= COLOR_DARK_GREEN;
	Color		COLOR_SHIP_ENEMY						= COLOR_BRILLIANT_RED;

	Color		COLOR_NEUTRONIUM_MINERAL		= COLOR_BRILLIANT_RED;
	Color		COLOR_NEUTRONIUM_ORE				= COLOR_STANDARD_ORANGE;
	Color		COLOR_DURANIUM_MINERAL			= COLOR_STANDARD_DARKBLUE;
	Color		COLOR_DURANIUM_ORE					= COLOR_STANDARD_BLUE;
	Color		COLOR_TRITANIUM_MINERAL			= COLOR_STANDARD_DARKGREN;
	Color		COLOR_TRITANIUM_ORE					= COLOR_STANDARD_GREEN;
	Color		COLOR_MOLYBDENUM_MINERAL		= COLOR_BROWN;
	Color		COLOR_MOLYBDENUM_ORE				= COLOR_STANDARD_ORANGE;

	// - F O N T S
	Font		FONT_MAP_DEFAULT						= new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL);
	Font		FONT_MAP_BOLD								= new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD);
	String	MODELTYPE_THING							= "TYPE_THING";
	String	MODELTYPE_POD								= "MODELTYPE_POD";
	int			DEFAULT_ZOOM_FACTOR					= 10;

	// - I N F O R M A T I O N - L E V E L
	int			SIZE_ICONIMAGE							= 16;
	Color		COLOR_NATIVES_DEFAULT				= ColorConstants.orange;
	Color		COLOR_TIP										= ColorConstants.tooltipBackground;

	// - I N F O R M A T I O N - L E V E L S
	String	INFOLEVEL_UNEXPLORED				= "INFOLEVEL_UNEXPLORED";
	String	INFOLEVEL_NOINFO						= "INFOLEVEL_NOINFO";
	String	INFOLEVEL_INFO							= "INFOLEVEL_INFO";
	String	INFOLEVEL_OBSOLETE					= "INFOLEVEL_OBSOLETE";
	String	INFOLEVEL_NATIVES						= "INFOLEVEL_NATIVES";
	String	FFSTATUS_UNDEFINED					= "FFSTATUS_UNDEFINED";
	String	FFSTATUS_FRIEND							= "FFSTATUS_FRIEND";
	String	FFSTATUS_FOE								= "FFSTATUS_FOE";
	Color		COLOR_UNDEFINED_BACKGROUND	= COLOR_LIGHTEST_YELLOW;
	Color		COLOR_FRIEND_BACKGROUND			= new Color(Display.getDefault(), 0xD1, 0xFC, 0xD7);
	Color		COLOR_FOE_BACKGROUND				= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
}

// - UNUSED CODE ............................................................................................
