//PROJECT:        net.sf.vgap4.assistant.ui
//FILE NAME:      $Id$
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editparts;

import java.util.List;

import org.eclipse.draw2d.IFigure;

import net.sf.gef.core.models.IContainerModel;

//- CLASS IMPLEMENTATION ...................................................................................
/** 
 */
public abstract class AbstractDiagramEditPart extends AbstractNodeEditPart implements IContainerPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("es.ftgroup.gef.parts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected abstract IFigure createFigure();

	@Override
	protected void createEditPolicies() {
		//		// - Disallows the removal of this edit part from its parent
		//		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
	}

	@Override
	public List<Object> getModelChildren() {
		Object model = getModel();
		if (model instanceof IContainerModel)
			return ((IContainerModel) getModel()).getChildren();
		else
			return super.getModelChildren();
	}

	//DEBUG This method is only used to set the interception debug point.
	@Override
	protected void refreshChildren() {
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
//- UNUSED CODE ............................................................................................
