//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.models;

// - IMPORT SECTION .........................................................................................
import java.util.List;

//- INTERFACE IMPLEMENTATION ...............................................................................
public interface IEndPoint {
	// - C O N S T A N T - S E C T I O N ......................................................................
	/** Property ID to use when the list of outgoing connections is modified. */
	public static final String	PROP_SOURCE_CONNECTIONS	= "IEndPoint.PROP_SOURCE_CONNECTIONS";
	/** Property ID to use when the list of incoming connections is modified. */
	public static final String	PROP_TARGET_CONNECTIONS	= "IEndPoint.PROP_TARGET_CONNECTIONS";

	// - F I E L D - S E C T I O N ............................................................................
	//	public static Vector<IWireModel>					sources									= new Vector<IWireModel>(1);
	//	public Vector<IWireModel>					targets									= new Vector<IWireModel>(1);

	// - M E T H O D - S E C T I O N ..........................................................................
	//	/**
	//	 * Add an incoming or outgoing connection to this instance.
	//	 * 
	//	 * @param wire
	//	 *          a non-null connection instance
	//	 * @throws IllegalArgumentException
	//	 *           if the connection is null or has not distinct end points
	//	 */
	//	public void addConnection(final Route wire);

	public void addSource(IEndPoint wire);

	public void addTarget(IEndPoint wire);

	/**
	 * Return a List of outgoing Connections.
	 */
	public abstract List<IEndPoint> getSourceConnections();

	/**
	 * Return a List of incoming Connections.
	 */
	public abstract List<IEndPoint> getTargetConnections();

	public void removeSource(final IEndPoint wire);

	public void removeTarget(final IEndPoint wire);
}

// - UNUSED CODE ............................................................................................
