//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editparts;

import es.ftgroup.gef.model.AbstractNode;
import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.gef.core.figures.SelectableFigure;
import net.sf.gef.core.models.IAllowsSelection;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;

//- CLASS IMPLEMENTATION ...................................................................................
/** 
 */
public abstract class AbstractNodeEditPart extends AbstractGenericEditPart implements NodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger				logger	= Logger.getLogger("es.ftgroup.gef.parts");

	// - F I E L D - S E C T I O N ............................................................................
	protected ConnectionAnchor	anchor;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// - I N T E R F A C E - N O D E E D I T P A R T
	public ConnectionAnchor getSourceConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	/**
	 * This method handles the activation and deactivation of selections. Changes the figure selection state and
	 * signals a repaint of that figure to update the display state.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 */
	@Override
	public void setSelected(final int value) {
		super.setSelected(value);

		// - Get the figure and signal a repaint.
		final IFigure fig = getFigure();
		if (fig instanceof SelectableFigure) {
			((SelectableFigure) fig).setSelected(value);
			fig.repaint();
		}

		// - Get the model and check if it allows selection signalling.
		final AbstractNode model = getCastedModel();
		if (model instanceof IAllowsSelection) ((IAllowsSelection) getCastedModel()).setSelection(value);
	}

	@Override
	protected IFigure createFigure() {
		final IFigureFactory factory = getFactory().getFigureFactory();
		final IFigure fig = factory.createFigure(this, getCastedModel());
		return fig;
	}

	@Override
	protected AbstractNode getCastedModel() {
		return (AbstractNode) getModel();
	}

	protected ConnectionAnchor getConnectionAnchor() {
		if (null == anchor) anchor = new ChopboxAnchor(getFigure());
		return anchor;
	}

	protected String getSelectedDecode() {
		if (getSelected() == EditPart.SELECTED_NONE) return "SELECTED_NONE";
		if (getSelected() == EditPart.SELECTED) return "SELECTED";
		if (getSelected() == EditPart.SELECTED_PRIMARY) return "SELECTED_PRIMARY";
		return "UNDEFINED_VALUE";
	}
}

// - UNUSED CODE ............................................................................................
