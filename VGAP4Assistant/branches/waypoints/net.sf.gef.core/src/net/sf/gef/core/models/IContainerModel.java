//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.models;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IContainerModel {
	// - F I E L D - S E C T I O N ............................................................................
	Vector<Object>	children	= new Vector<Object>(1);

	// - M E T H O D - S E C T I O N ..........................................................................
	public List<Object> getChildren();
}
// - UNUSED CODE ............................................................................................
