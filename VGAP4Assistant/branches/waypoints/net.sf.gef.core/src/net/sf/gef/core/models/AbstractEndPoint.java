//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.models;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;

import es.ftgroup.gef.model.AbstractPropertyChanger;

// - CLASS IMPLEMENTATION ...................................................................................
public class AbstractEndPoint extends AbstractPropertyChanger implements IEndPoint {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.gef.core.model");
	private static final long	serialVersionUID	= -8957766087080545673L;

	// - F I E L D - S E C T I O N ............................................................................
	public Vector<IEndPoint>	sources						= new Vector<IEndPoint>(1);
	public Vector<IEndPoint>	targets						= new Vector<IEndPoint>(1);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractEndPoint() {
	}

	public void addSource(IEndPoint wire) {
		sources.add(wire);
	}

	public void addTarget(IEndPoint wire) {
		targets.add(wire);
	}

	public List<IEndPoint> getSourceConnections() {
		return sources;
	}

	public List<IEndPoint> getTargetConnections() {
		return targets;
	}

	public void removeSource(IEndPoint wire) {
		// TODO Auto-generated method stub

	}

	public void removeTarget(IEndPoint wire) {
		// TODO Auto-generated method stub

	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
