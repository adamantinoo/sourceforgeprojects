//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package es.ftgroup.gef.model;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractContainer extends AbstractNode implements IContainerModel {

	//	private static Logger				logger				= Logger.getLogger("es.ftgroup.gef.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	//	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - F I E L D S
	private final Vector<Object>	children	= new Vector<Object>(1);

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public AbstractContainer() {
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E

	public List<Object> getChildren() {
		return children;
	}
}
// - UNUSED CODE ............................................................................................
