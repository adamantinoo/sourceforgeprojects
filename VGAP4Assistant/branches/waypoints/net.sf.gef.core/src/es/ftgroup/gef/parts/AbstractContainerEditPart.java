//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package es.ftgroup.gef.parts;

//- IMPORT SECTION .........................................................................................
import java.util.List;

import net.sf.gef.core.editparts.AbstractNodeEditPart;

import es.ftgroup.gef.model.IContainerModel;

//- CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractContainerEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("es.ftgroup.gef.parts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void createEditPolicies() {
		//		// - Disallows the removal of this edit part from its parent
		//		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
	}

	@Override
	protected List<Object> getModelChildren() {
		Object model = getModel();
		if (model instanceof IContainerModel)
			return ((IContainerModel) getModel()).getChildren();
		else
			return super.getModelChildren();
	}

	//DEBUG This method is only used to set the interception debug point.
	@Override
	protected void refreshChildren() {
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
