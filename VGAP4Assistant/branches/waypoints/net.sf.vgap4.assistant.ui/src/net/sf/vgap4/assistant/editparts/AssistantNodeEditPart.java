//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.editparts.AbstractNodeEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Planet;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantNodeEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (AssistantNode.PROP_LOCATION.equals(prop)) {
			this.refreshVisuals();
			return;
		}
	}

	@Override
	protected AssistantNode getCastedModel() {
		return (AssistantNode) this.getModel();
	}

	//	@Override
	//	protected List<Route> getModelSourceConnections() {
	//		return this.getCastedModel().getSourceConnections();
	//	}
	//
	//	@Override
	//	protected List<Route> getModelTargetConnections() {
	//		return this.getCastedModel().getTargetConnections();
	//	}
	/**
	 * This method is called upon the creation of the EditPart to associate it with behavior modifiers for
	 * selection and other node functions.
	 * 
	 * @see net.sf.gef.core.editparts.AbstractNodeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		//		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		//		super.createEditPolicies();
	}

	//DEBUG Check if this method is required.
	protected Planet getOnPlanet(final int planetId) {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getPlanet4Id(planetId);
	}

	//DEBUG Check if this method is required.
	protected int getPlayerCode() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getPlayer();
	}

	//DEBUG Check if this method is required.
	protected int getZoomFactor() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getZoomFactor();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final Figure fig = (Figure) this.getFigure();

		//		final Dimension size = fig.getSize();
		final Dimension prefSize = fig.getPreferredSize();
		final Point location = this.getCastedModel().getLocation();
		final Rectangle bounds = new Rectangle(location, prefSize);
		((GraphicalEditPart) this.getParent()).setLayoutConstraint(this, this.getFigure(), bounds);
		super.refreshVisuals();
	}
}

// - UNUSED CODE ............................................................................................
