//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $iD: ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.ShortestPathConnectionRouter;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.LayerConstants;
import org.eclipse.gef.editparts.AbstractEditPart;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import net.sf.gef.core.editparts.AbstractDiagramEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.projecteditor.editparts.VGAPScalableFreeformRootEditPart;
import net.sf.vgap4.projecteditor.policies.VGAP4XYLayoutEditPolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class DiagramEditPart extends AbstractDiagramEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		DiagramEditPart.logger.info("Processing property change: " + prop + " on object " + this.toString());

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AssistantMap.DATA_ADDED_PROP.equals(prop)) {
			this.refreshChildren();
		}
		if (AssistantMap.CHANGE_ZOOMFACTOR.equals(prop)) {
			//TODO Refresh the location of every figure contained inside the map.
			final Iterator<Object> cit = this.getChildren().iterator();
			while (cit.hasNext()) {
				final Object child = cit.next();
				if (child instanceof SpotEditPart) ((SpotEditPart) child).refreshVisuals();
			}
			//DEBUG Check if this line is necessary.
			this.refresh();
		}
	}

	//[01]

	/**
	 * Adds the child's Figure to the {@link #getContentPane() contentPane}.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#addChildVisual(EditPart, int)
	 */
	@Override
	protected void addChildVisual(final EditPart childEditPart, final int index) {
		final IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		//		if (childEditPart instanceof SectorEditPart) {
		//			FreeformLayer sectorLayer = (FreeformLayer) getLayer(VGAPScalableFreeformRootEditPart.SECTOR_LAYER);
		//			sectorLayer.add(child);
		//			return;
		//		}
		if (childEditPart instanceof WaypointEditPart) {
			final FreeformLayer sectorLayer = (FreeformLayer) this.getLayer(VGAPScalableFreeformRootEditPart.WAYPOINT_LAYER);
			sectorLayer.add(child);
			return;
		}
		if (childEditPart instanceof ShipRangeEditPart) {
			final FreeformLayer sectorLayer = (FreeformLayer) this.getLayer(VGAPScalableFreeformRootEditPart.WAYPOINT_LAYER);
			sectorLayer.add(child);
			return;
		}
		if (childEditPart instanceof SpotEditPart) {
			final FreeformLayer sectorLayer = (FreeformLayer) this.getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
			sectorLayer.add(child);
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	@Override
	protected void createEditPolicies() {
		// - Disallows the removal of this edit part from its parent
		this.installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		// - Handles constraint changes (e.g. moving and/or resizing) and creation of new model elements
		this.installEditPolicy(EditPolicy.LAYOUT_ROLE, new VGAP4XYLayoutEditPolicy());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// - Create an empty figure to control the scroll and the sizing.
		final Figure fig = new FreeformLayer();
		fig.setOpaque(true);
		fig.setBorder(new MarginBorder(3));
		fig.setLayoutManager(new FreeformLayout());

		// - Create the static router for the connection layer
		final ConnectionLayer connLayer = (ConnectionLayer) this.getLayer(LayerConstants.CONNECTION_LAYER);
		connLayer.setConnectionRouter(new ShortestPathConnectionRouter(fig));
		return fig;
	}

	@Override
	protected AssistantMap getCastedModel() {
		return (AssistantMap) this.getModel();
	}

	@Override
	protected void refreshChildren() {
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * Remove the child's Figure from the {@link #getContentPane() contentPane}.
	 * 
	 * @see AbstractEditPart#removeChildVisual(EditPart)
	 */
	@Override
	protected void removeChildVisual(final EditPart childEditPart) {
		//- Locate the right layer.
		IFigure contentPane = this.getContentPane();
		//		if (childEditPart instanceof SectorEditPart) contentPane = getLayer(VGAPScalableFreeformRootEditPart.SECTOR_LAYER);
		if (childEditPart instanceof WaypointEditPart)
			contentPane = this.getLayer(VGAPScalableFreeformRootEditPart.WAYPOINT_LAYER);
		if (childEditPart instanceof ShipRangeEditPart)
			contentPane = this.getLayer(VGAPScalableFreeformRootEditPart.WAYPOINT_LAYER);
		if (childEditPart instanceof SpotEditPart)
			contentPane = this.getLayer(VGAPScalableFreeformRootEditPart.PLANET_LAYER);
		final IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		try {
			contentPane.remove(child);
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
//	@Override
//	public String toString() {
//		StringBuffer buffer=new StringBuffer('\n');
//		buffer.append("[DiagramEditPart:").append('\n');
//		return super.toString();
//	}
// public Sector getSector() {
// return sector;
// }
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
//	 */
//	@Override
//	protected List<Object> getModelChildren() {
//		return getCastedModel().getChildren();
//	}
