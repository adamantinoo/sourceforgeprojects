//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.editparts.AbstractNodeEditPart;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.Waypoint;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class WaypointEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean isSelectable() {
		return false;
	}

	public void propertyChange(final PropertyChangeEvent evt) {

	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}

	@Override
	protected IFigure createFigure() {
		return new WaypointFigure();
	}

	protected int getZoomFactor() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getZoomFactor();
	}

	@Override
	protected void refreshVisuals() {
		try {
			//			this.refreshFigureImage();
			this.refreshToolTip();
			this.refreshModelVisuals();
			this.refreshFigureLocation();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private Rectangle getMapBoundaries() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getSpotRegistry().getBoundaries().getCopy();
	}

	private int getMapSize() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getMapSize();
	}

	private void refreshFigureLocation() {
		// - The references to the model and figure objects.
		final WaypointFigure fig = (WaypointFigure) this.getFigure();
		final Waypoint model = (Waypoint) this.getCastedModel();

		//- Get the location from the model information and then convert to the final coordinates.
		final Dimension size = fig.getPreferredSize();
		final Point hotSpot = fig.getHotSpot();
		final Point location = model.getLocation();

		//- Invert the coordinates for the Y axis depending on the map size.
		final int mapSize = this.getMapSize();
		location.y = mapSize - location.y;

		//- Adjust location depending on the top-left minimum coordinates.
		final Rectangle boundaries = this.getMapBoundaries();
		final Point origin = new Point(Math.max(boundaries.x - 30, 0), Math.max(boundaries.height - 30, 0));
		location.x = location.x - origin.x;
		location.y = location.y - (mapSize - origin.y);
		location.x -= hotSpot.x;
		location.y -= hotSpot.y;
		fig.setLocation(location);

		//- Calculate real map location depending on the zoom factor.
		final int zoom = this.getZoomFactor();
		if ((zoom > 10) || (zoom < 10)) {
			if (zoom > 0) {
				final double factor = zoom / 10.0;
				location.x = new Double(location.x * factor).intValue();
				location.y = new Double(location.y * factor).intValue();
			}
		}

		final Rectangle bounds = new Rectangle(location, size);
		WaypointEditPart.logger.info("Rebounding instance " + model.getType() + " to " + bounds);
		fig.setBounds(bounds);
		((GraphicalEditPart) this.getParent()).setLayoutConstraint(this, fig, bounds);
	}

	private void refreshModelVisuals() {
		// - The references to the model and figure objects.
		final WaypointFigure fig = (WaypointFigure) this.getFigure();
		final Waypoint model = (Waypoint) this.getCastedModel();

		// - Update figure visuals from current model data.
		fig.setType(model.getType());
		fig.setLocation(model.getLocation());
		fig.setVisible(false);
	}

	private void refreshToolTip() {
		// - The references to the model and figure objects.
		final WaypointFigure fig = (WaypointFigure) this.getFigure();
		final Waypoint model = (Waypoint) this.getCastedModel();

		//- Create the tooltip with the composite information for this Spot.
		final Figure tip = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 1;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 1;
		tip.setLayoutManager(grid);
		final StandardLabel repLabel = new StandardLabel(model.getTurn() + "-" + model.getOrder());
		tip.add(repLabel);
		fig.setToolTip(tip);
	}
}

class WaypointFigure extends Figure {
	private static final int	WAYPOINT_SIZE	= 28 - 4;
	private String						type;
	private int								range;
	private String						FFstatus;

	public Point getHotSpot() {
		return new Point(WaypointFigure.WAYPOINT_SIZE / 2, WaypointFigure.WAYPOINT_SIZE / 2);
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(32, 32);
	}

	//	public void setRange(int range) {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	public void setFfStatus(String status) {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	public void setAttributes(String type, int range, String status) {
	//		this.type = type;
	//		this.range = range;
	//		this.FFstatus = status;
	//	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		final boolean vis = this.isVisible();
		if (Waypoint.WAYPOINT_PASTWAYPOINT.equals(type)) {
			graphics.setForegroundColor(AssistantConstants.COLOR_WAYPOINT_PAST);
			graphics.setBackgroundColor(AssistantConstants.COLOR_WAYPOINT_PAST);
		}
		if (Waypoint.WAYPOINT_CURRENTWAYPOINT.equals(type)) {
			graphics.setForegroundColor(AssistantConstants.COLOR_WAYPOINT_CURRENT);
			graphics.setBackgroundColor(AssistantConstants.COLOR_WAYPOINT_CURRENT);
		}
		if (Waypoint.WAYPOINT_FUTUREWAYPOINT.equals(type)) {
			graphics.setForegroundColor(AssistantConstants.COLOR_WAYPOINT_FUTURE);
			graphics.setBackgroundColor(AssistantConstants.COLOR_WAYPOINT_FUTURE);
		}
		final Rectangle limits = this.getBounds().getCopy();
		limits.width--;
		limits.height--;
		graphics.pushState();
		graphics.setLineWidth(2);
		//		graphics.setForegroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
		graphics.drawOval(limits);
		//		graphics.popState();
		graphics.setBackgroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
		graphics.fillOval(limits.expand(-5, -5));
		graphics.popState();
	}

}
// - UNUSED CODE ............................................................................................
