//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractNode;
import net.sf.gef.core.models.IContainerModel;
import net.sf.vgap4.assistant.models.helpers.NodeFilter;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * An <code>Spot</code> is a complex model structure that is only used to group model elements into a single
 * representable element. Spots are represented to the outside as a single that can be selected from the
 * highest content structure, this is called the <code>representative</code>. Spots are aware of the types
 * of its contents to determine from this structure the corresponding graphical representation. <br>
 * New additions will have more grained control about the Spot contents and functionalities.
 */
public class Spot extends AssistantNode /* implements IContainerModel */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger								logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long						serialVersionUID	= 5087746345064091787L;

	// - F I E L D - S E C T I O N ............................................................................
	public boolean											hasBase						= false;
	public boolean											hasPlanet					= false;
	public boolean											hasShip						= false;
	public boolean											hasPods						= false;
	public boolean											hasWings					= false;
	public boolean											onlyRanges				= true;
	private final Vector<AssistantNode>	spotContents			= new Vector<AssistantNode>();
	private Vector<ShipRange>						speedRange;
	private ShipRange										engineRange;
	private ShipRange										maxRange;

	//	private final Hashtable							counters					= new Hashtable(3);
	private AssistantNode								representative;
	/** List of user Map filters to show/hide some groups of information from the Map. */
	private final Vector<NodeFilter>		filters						= new Vector<NodeFilter>();
	{
		//- Remove from the model the MAXRANGE elements.
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969787L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof ShipRange)
					if (ShipRange.MAXRANGE.equals(((ShipRange) item).getType()))
						return false;
					else
						return true;
				else
					return true;
			}
		});
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969787L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof ShipRange)
					if (ShipRange.SPEED.equals(((ShipRange) item).getType()))
						return false;
					else
						return true;
				else
					return true;
			}
		});
		//		filters.add(new NodeFilter() {
		//			private static final long	serialVersionUID	= 5089730478538969787L;
		//
		//			@Override
		//			public boolean test(final AbstractNode item) {
		//				if (item instanceof Waypoint) {
		//				} else
		//					return true;
		//			}
		//		});
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public Spot() {
	//	}

	//	public Spot(LocationCluster cluster) {
	//		//		this.locationObjects = cluster;
	//
	//		//TODO Load the cached data in the spot fields from the cluster representative.
	//		AssistantNode representative = cluster.getRepresentative();
	//		setLocation(representative.getLocation());
	//		setIdNumber(representative.getIdNumber());
	//		setName(representative.getName());
	//		setLatestTurnNumber(representative.getLatestTurnNumber());
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add a new element to this location spot. Depending on the element to be added we can select a
	 * representative this is the top priority element in the spot in this order:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * Representative delegate may be calculated when adding (that is done more then once) or delayed until the
	 * first request for a representative that will be executed once during the life of the Spot.
	 * 
	 * @param mapElement
	 *          the model node to be added to this spot
	 */
	public void add(final AssistantNode mapElement) {
		//TODO Detect the type of element and the FF status to add it to the right counter.
		onlyRanges = false;
		if (mapElement instanceof Ship) hasShip = true;
		if (mapElement instanceof Planet) hasPlanet = true;
		if (mapElement instanceof Base) hasBase = true;
		if (mapElement instanceof Pod) hasPods = true;
		if (mapElement instanceof Wing) hasWings = true;
		spotContents.add(mapElement);
	}

	/**
	 * Add a new element that has an inheritance path from <code>AbstractNode</code>. This are the
	 * representation for the simple <code>ShipRange</code> that only add some graphical representation to the
	 * Map but not to the rest of the Spot contents.
	 */
	public void add(final ShipRange rangeElement) {
		if (ShipRange.ENGINE.equals(rangeElement.getType())) {
			if (null == engineRange)
				engineRange = rangeElement;
			else {
				final int current = engineRange.getRange();
				final int newRange = rangeElement.getRange();
				if (newRange > current) engineRange = rangeElement;
			}
		}
		if (ShipRange.MAXRANGE.equals(rangeElement.getType())) {
			if (null == maxRange)
				maxRange = rangeElement;
			else {
				final int current = maxRange.getRange();
				final int newRange = rangeElement.getRange();
				if (newRange > current) maxRange = rangeElement;
			}
		}
		if (ShipRange.SPEED.equals(rangeElement.getType())) {
			if (null == speedRange) {
				speedRange = new Vector<ShipRange>(1);
				speedRange.add(rangeElement);
			} else {
				speedRange.add(rangeElement);
				//				int current = this.speedRange.getRange();
				//				int newRange = rangeElement.getRange();
				//				if (newRange > current) speedRange = rangeElement;
			}
		}
	}

	/**
	 * There is a difference between the Spot contents and the Spot children. While the contents is the list of
	 * elements that are located in that position, the children are new structures that depend hierarchically
	 * from the elements that compose the contents. So the children is the complete list of all the children of
	 * the Spot contents.<br>
	 * We have to filter out the list of all available children because not all have to be presented for
	 * visualization. Only are candidates the max range for any ship (but this is only informative and if the
	 * display is cluttered will be filtered out) and the current speeds for friend and enemy ships.
	 */
	public List<Object> getChildren() {
		final Vector<Object> children = new Vector<Object>(2);
		final ShipRange maxRange = null;
		final Iterator<AssistantNode> cit = this.getContents().iterator();
		while (cit.hasNext()) {
			final AssistantNode element = cit.next();
			//- For any content check if it can contain children.
			if (element instanceof IContainerModel) {
				Spot.logger.info("Detected children structures. Processing children for " + this.getName());
				final List<Object> candidates = ((IContainerModel) element).getChildren();
				//TODO Apply any filtering. This is affects to ranges and waypoints.
				final Iterator<Object> rit = candidates.iterator();
				while (rit.hasNext()) {
					final Object candidate = rit.next();
					if (candidate instanceof ShipRange) {
						if (this.applyFilters((AbstractNode) candidate)) children.add(candidate);
					}
					if (candidate instanceof Waypoint) {
						if (this.applyFilters((AbstractNode) candidate)) children.add(candidate);
					}
				}
			}
		}
		return children;
	}

	public Vector<AssistantNode> getContents() {
		return spotContents;
	}

	@Override
	public String getField(final String key) {
		return representative.getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return representative.getFieldNumber(key);
	}

	@Override
	public int getOwner() {
		return representative.getOwner();
	}

	public String getPreferredShape() {
		if (hasBase) return "Base";
		if (hasPlanet) return "Planet";
		if (hasShip) return "Ship";
		return "Undefined";
	}

	/**
	 * Return the max range for any of the types that are registered inside this Spot. All ships on the Spot add
	 * ranges to the same Spot range list. This method filters the ranges to return the one that covers all
	 * others on the same point.
	 */
	public Vector<ShipRange> getRanges() {
		//		Vector<ShipRange> ranges = new Vector<ShipRange>(3);
		//				if (null != this.engineRange) ranges.add(engineRange);
		//				if (null != this.maxRange) ranges.add(maxRange);
		//		if (null != this.speedRange) ranges.add(speedRange);
		if (null == speedRange)
			return new Vector<ShipRange>(1);
		else
			return speedRange;
	}

	/**
	 * Tis method delays the calculation of the representativo for this spot and also reduces the possibility to
	 * return null values when new model objects are added to the Map. As explained on the <code>add()</code>
	 * method the priority to select a representative is:
	 * <ul>
	 * <li>Base</li>
	 * <li>Planet</li>
	 * <li>Ship</li>
	 * <li>Thing</li>
	 * </ul>
	 * The representative is calculated by applying a priority factor to each element depending on its class and
	 * then gettinh the top most priority one. This can be done in a single pass over the elements list.
	 * 
	 * @return the model node selected as a representative prioritized class node.
	 */
	public AssistantNode getRepresentative() {
		//- Check if the representative is cached.
		if (null == representative) {
			//- Lookup for a representative. If the size of the spot is 1 then it is clear who it it.
			if (1 == spotContents.size()) {
				this.setRepresentative(spotContents.firstElement());
				return representative;
			} else {
				double repPriority = -1;
				final Iterator<AssistantNode> sit = spotContents.iterator();
				while (sit.hasNext()) {
					final AssistantNode element = sit.next();
					double priority = 0;
					if (element instanceof Ship) priority = element.getIdNumber() * 1000.0;
					if (element instanceof Planet) priority = element.getIdNumber() * 1000.0 * 1000.0;
					if (element instanceof Base) priority = element.getIdNumber() * 1000.0 * 1000.0 * 1000.0;
					if (element instanceof Pod) priority = element.getIdNumber() * 1.0;
					if (element instanceof Wing) priority = element.getIdNumber() * 10.0;
					if (priority > repPriority) {
						this.setRepresentative(element);
						repPriority = priority;
					}
				}
				return representative;
			}
		} else
			return representative;
	}

	public boolean hasContents() {
		if (null != representative) return true;
		return false;
	}

	public boolean hasNatives() {
		if (hasPlanet) {
			final Iterator<AssistantNode> cit = spotContents.iterator();
			while (cit.hasNext()) {
				final AssistantNode element = cit.next();
				if (element instanceof Planet) {
					if (((Planet) element).getTotalNatives() > 0) return true;
				}
			}
		}
		return false;
	}

	public String infoLevel() {
		if (hasPlanet) {
			final Planet planet = this.locatePlanet();
			if (null != planet) return planet.infoLevel();
		}
		return AssistantConstants.INFOLEVEL_UNEXPLORED;
	}

	public boolean isMultiple() {
		if (spotContents.size() > 1)
			return true;
		else
			return false;
	}

	public void setRepresentative(final AssistantNode representative) {
		this.representative = representative;

		//- Load the cached data in the spot fields from the cluster representative.
		this.setLocation(representative.getLocation());
		this.setIdNumber(representative.getIdNumber());
		this.setName(representative.getName());
		this.setLatestTurnNumber(representative.getLatestTurnNumber());
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("Spot[");
		if (hasBase)
			buffer.append("B");
		else
			buffer.append("-");
		if (hasPlanet)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasShip)
			buffer.append("S");
		else
			buffer.append("-");
		if (hasPods)
			buffer.append("P");
		else
			buffer.append("-");
		if (hasWings)
			buffer.append("W");
		else
			buffer.append("-");
		buffer.append(this.getRepresentative().toString());
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	private boolean applyFilters(final AbstractNode item) {
		//- Apply only filters that match the class or generic filters
		final Iterator<NodeFilter> fit = filters.iterator();
		while (fit.hasNext()) {
			final NodeFilter filter = fit.next();
			boolean result = filter.test(item);
			//- If the result is filtered then do not continue. Otherwise apply other filters until filtered or end.
			if (!result) return false;
		}
		return true;
	}

	private Planet locatePlanet() {
		final Iterator<AssistantNode> cit = spotContents.iterator();
		while (cit.hasNext()) {
			final AssistantNode element = cit.next();
			if (element instanceof Planet) return (Planet) element;
		}
		return null;
	}
}

// - UNUSED CODE ............................................................................................
