//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.gef.core.models.IContainerModel;
import net.sf.vgap4.assistant.models.database.ShipDatabase;
import net.sf.vgap4.assistant.models.database.ShipDatabase.Weapon;
import net.sf.vgap4.assistant.models.helpers.WaypointList;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class Ship extends AssistantNode implements IContainerModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long													serialVersionUID	= -5137898479615467215L;
	//	private static final String												HULLDATABASE_FILENAME	= "HullDatabase";
	//	private static final String												BUNDLE_HULLDATABASE		= "net.sf.vgap4.assistant.models.resources"
	//																																							+ Ship.HULLDATABASE_FILENAME;
	private static Logger															logger						= Logger
																																					.getLogger("net.sf.vgap4.projecteditor.model");
	//	private static transient Properties								hullDatabase;
	//	private static ResourceManager										hullDatabaseManager		= null;
	//	static {
	//		hullDatabaseManager = ResourceManager.getManager(BUNDLE_HULLDATABASE);
	//	}

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private AssistantMap															map								= null;
	/**
	 * This array stores the information processed for this planet (identified by its ID) for any turn. The data
	 * for turn x can be located inside vector slot x.
	 */
	private final Hashtable<Integer, ShipInformation>	shipInformation		= new Hashtable<Integer, ShipInformation>();
	private WaypointList															waypoints					= new WaypointList();
	private final Vector															weaponLoadout			= new Vector(2);
	private final ShipConfigurationPart								hypEngine					= ShipConfigurationPart.NA;
	private final ShipConfigurationPart								engine						= ShipConfigurationPart.NA;
	private final ShipConfigurationPart								shield						= ShipConfigurationPart.NA;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Ship(final AssistantMap ownerMap) {
		map = ownerMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addTurnInformation(final int turn, final ShipInformation shipInfo) {
		// - Get the old value for this base turn information if available.
		final ShipInformation oldData = shipInformation.get(turn);
		if (null != oldData)
			Ship.logger.warning("This operation is overriding some previous information stored for this turn");

		shipInfo.setTurnNumber(turn);
		shipInformation.put(turn, shipInfo);
		Ship.logger.info("Adding turn " + turn + " to Ship '" + shipInfo.getName() + "'");

		//		try {
		//			shipInformation.setSize(this.getLatestTurnNumber() + 1);
		//			//			if (null != shipInformation.get(turn))
		//			//				Ship.logger.warning("This operation is overriding some previous information stored for this turn");
		//			shipInformation.set(turn, shipInfo);
		//			Ship.logger.info("Adding ship '" + shipInfo.getName() + "' data for turn " + turn + ".");
		//		} catch (final ArrayIndexOutOfBoundsException aioobe) {
		//			shipInformation.setSize(turn + 1);
		//			shipInformation.set(turn, shipInfo);
		//			aioobe.printStackTrace();
		//		}

		// - Cache turn information. Must be set because upper classes do not have access to this information.
		this.setCachedInformation(turn, shipInfo);
		//TODO Get the weapon configuration and store it for later reference.
		this.getWeaponLoadout();
		Ship.logger.info("Processing ship id [" + shipInfo.getIdNumber() + "] - " + shipInfo.getName() + ". At location ["
				+ shipInfo.getLocation().x + ", " + shipInfo.getLocation().y + "]");
		//TODO process the Waypoints and the the correct properties depending on turn and location.
		this.processWaypoints(turn);
	}

	public String getCargoDetail() {
		final StringBuffer buffer = new StringBuffer("[");
		buffer.append(this.getLatestInfo().getField("Duranium")).append("D").append("/");
		buffer.append(this.getLatestInfo().getField("Tritanium")).append("T").append("/");
		buffer.append(this.getLatestInfo().getField("Molybdenum")).append("M").append("/");
		buffer.append(this.getLatestInfo().getField("Supplies")).append("S").append("/");
		buffer.append(this.getLatestInfo().getField("Food")).append("F").append("/");
		buffer.append(this.getLatestInfo().getField("Med")).append("M").append("]");
		return buffer.toString();
	}

	/**
	 * This method is part of the <code>IContainerModel</code> interface and its target is to get the list of
	 * model elements that will have a separate controller in the MVC pattern and that will define another
	 * hierarchy level in the model structure.<br>
	 * Children elements for the <code>Ship</code> class are:
	 * <ul>
	 * <li>The range circles that will show graphically as colorful circles with the range of space that can be
	 * reached on the next turn at the current speed, and the max range that can be reached due to the engines
	 * mounted on the ship's hull.</li>
	 * <li>The ships waypoints that show the path traveled and the path set by the user to continue navigation.
	 * There could be filters for both sets and also for the ranges set.</li>
	 * </ul>
	 * Range circles may represent 3 values:
	 * <ul>
	 * <li>The max range available to the hull. This is a fixed value that is set for any hull and that will
	 * not be exceeded even the hull is mounted with more powerful engines. This is the limit speed designed for
	 * this hull.</li>
	 * <li>The max speed of the engines. This value is the max range that a ship can travel with their current
	 * engines.</li>
	 * <li>The current speed. The user may select to travel to a different speed that the maximum and this can
	 * be set turn by turn.</li>
	 * </ul>
	 * For FOE ships where some of this parameters are not known, the range to be shown should be the value for
	 * the known parameter, if the hull is known then the max hull speed and if some data of the ships engines
	 * is known, the current speed or the engines max speed. <br>
	 * <br>
	 * For Waypoints, there can be differentiated between:
	 * <ul>
	 * <li>Past waypoints. The ones that where recorded from past turns and that lay in the past of the path
	 * walked by the unit.</li>
	 * <li>Final waypoints. The points where the ship was located at the beginning of a turn. Used to make the
	 * cut point between past and future waypoints.</li>
	 * <li>Future waypoints. The ones configured by the user but that will be walked in next turns.</li>
	 * </ul>
	 */
	public List<Object> getChildren() {
		final Vector<Object> ranges = new Vector<Object>(2);

		//		//- Process engine ranges.
		//		int maxRange = ShipDatabase.getHullMaxSpeed(getFieldNumber("hull"));
		//		ranges.add(new ShipRange(ShipRange.MAXRANGE, maxRange, getLocation(), this.getFFStatus()));
		//		int engineSpeed = ShipDatabase.getEngineSpeed(getFieldNumber("Engine"));
		//		ranges.add(new ShipRange(ShipRange.ENGINE, engineSpeed, getLocation(), this.getFFStatus()));
		//		int speed = getFieldNumber("Speed");
		//		ranges.add(new ShipRange(ShipRange.SPEED, speed, getLocation(), this.getFFStatus()));

		//- Process the waypoints
		ranges.addAll(this.getWaypoints());
		return ranges;
	}

	public String getEngineName() {
		final int engineCode = this.getFieldNumber("hull");
		return ShipDatabase.getEngineDescription(engineCode);
	}

	@Override
	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	public String getHullName() {
		final int hullCode = this.getFieldNumber("hull");
		return ShipDatabase.getHullDescription(hullCode);
	}

	public AssistantMap getMap() {
		return map;
	}

	@Override
	public int getOwner() {
		return this.getLatestInfo().getFieldNumber("owner");
	}

	public String getPassageDetail() {
		final StringBuffer buffer = new StringBuffer("[");
		buffer.append(this.getLatestInfo().getField("Colonists")).append("C").append("/");
		buffer.append(this.getLatestInfo().getField("Troops")).append("T").append("/");
		buffer.append(this.getLatestInfo().getField("HighGuard")).append("HG").append("]");
		return buffer.toString();
	}

	public Vector<ShipRange> getShipRanges() {
		final Vector<ShipRange> ranges = new Vector<ShipRange>(2);
		//- Process engine ranges.
		final int maxRange = ShipDatabase.getHullMaxSpeed(this.getFieldNumber("hull"));
		ranges.add(new ShipRange(ShipRange.MAXRANGE, maxRange, this.getLocation(), this.getFFStatus()));
		final int engineSpeed = ShipDatabase.getEngineSpeed(this.getFieldNumber("Engine"));
		ranges.add(new ShipRange(ShipRange.ENGINE, engineSpeed, this.getLocation(), this.getFFStatus()));
		final int speed = this.getFieldNumber("Speed");
		ranges.add(new ShipRange(ShipRange.SPEED, speed, this.getLocation(), this.getFFStatus()));
		return ranges;
	}

	public WaypointList getWaypoints() {
		if (null == waypoints) waypoints = new WaypointList();
		return waypoints;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Ship:");
		buffer.append("id=").append(this.getIdentifier()).append(" - ");
		buffer.append('\n').append(super.toString());
		buffer.append("]");
		return buffer.toString();
	}

	//[01]
	/**
	 * This method return the turn information for the highest turn number that is recorded for this object.
	 * This has not to be the same for every object in the model because some objects may be updated in a new
	 * turn while others not. The relative obsolescence for this information depends on the values of the
	 * <code>lastAvailableTurn</code> that is recorded on the <code>AssistantMap</code>.
	 */
	protected ShipInformation getLatestInfo() {
		return this.getTurnInfo(this.getLatestTurnNumber());
	}

	protected void setCachedInformation(final int turn, final ShipInformation shipInfo) {
		this.updateTurnReference(turn);
		this.setIdNumber(shipInfo.getIdNumber());
		this.setName(this.getLatestInfo().getName());
		this.setLocation(this.getLatestInfo().getLocation());
		//- Calculate the FF status for this ship.
		if (map.getPlayer() == this.getLatestInfo().getFieldNumber("owner"))
			this.setFFStatus(AssistantConstants.FFSTATUS_FRIEND);
		else
			this.setFFStatus(AssistantConstants.FFSTATUS_FOE);

		//- Clear other cached data for reloading.
		//		waypoints = null;
	}

	/** Locates recursively the latest declared turn information for this model element, whatever it is */
	private ShipInformation getTurnInfo(final int turn) {
		final ShipInformation info = shipInformation.get(turn);
		if (null == info)
			return this.getTurnInfo(turn - 1);
		else
			return info;
	}

	private void getWeaponLoadout() {
		final ShipInformation turnInfo = this.getLatestInfo();
		weaponLoadout.clear();
		//TODO Load Point Defense Weapons
		for (int i = 1; i <= 10; i++) {
			final String refName = "PD Weapon  " + i;
			final int present = turnInfo.getFieldNumber(refName);
			if (present > 0) {
				final Weapon weapon = ShipDatabase.getWeapon("PD", present);
				if (null != weapon) weaponLoadout.add(ShipDatabase.getWeapon("PD", present));
			}
		}
		//TODO Load Point Defense Weapons
		for (int i = 1; i <= 20; i++) {
			final String refName = "Large Weapon  " + i;
			final int present = turnInfo.getFieldNumber(refName);
			if (present > 0) {
				final Weapon weapon = ShipDatabase.getWeapon(ShipDatabase.LARGE_WEAPON, present);
				weaponLoadout.add(weapon);
			}
		}
	}

	//	private Vector<Waypoint> getWaypoints() {
	//		if (null == this.waypoints) {
	//			waypoints = new Vector(2);
	//			processWaypoints();
	//		}
	//		return waypoints;
	//	}

	/**
	 * When processing a turn, the first task is to mark all waypoints from previous turns as PAST waypoints.
	 * Then the next operation is to remove duplicated points applying the priority constraints that CURRENT and
	 * FUTURE have more priority than PAST.<br>
	 * Marking previous waypoints as obsolete points can be done in different ways. To be allowed to perform any
	 * of this tests, the waypoint has to have recorded the turn number to which it belongs, and also the
	 * waypoint order on the data source for that turn.<br>
	 * Waypoints have to be ordered by those fields so middle insertions are posiible and diplicated easily
	 * removed.
	 * 
	 * @param turn
	 *          the current turn number to be processed.
	 */
	private void processWaypoints(final int turn) {
		int order = 0;
		this.getWaypoints().add(
				new Waypoint(turn, order++, Waypoint.WAYPOINT_CURRENTWAYPOINT, this.getLocation().x, this.getLocation().y));
		for (int i = 1; i <= 6; i++) {
			final int x = this.getFieldNumber("X Way Point  " + i);
			final int y = this.getFieldNumber("Y Way Point  " + i);
			if ((0 == x) && (0 == y)) continue;
			this.getWaypoints().add(new Waypoint(turn, order++, Waypoint.WAYPOINT_FUTUREWAYPOINT, x, y));
		}
		this.getWaypoints().orderAndFilter();
	}

	//	private static void connectHullDatabase() {
	//		//TODO Create a new Resource Manager to get access to the properties.
	//		hullDatabaseManager = ResourceManager.getManager(BUNDLE_HULLDATABASE);
	//	}
}

class ShipConfigurationPart implements Serializable {
	public static final ShipConfigurationPart	NA;
	private static final long									serialVersionUID	= -4230628471921090979L;
	static {
		NA = new ShipConfigurationPart();
		ShipConfigurationPart.NA.setName("No Aplica");
	}
	private String														name;

	private void setName(final String string) {
		name = string;
	}

}
// - UNUSED CODE ............................................................................................
//[01]
//	public String getProperty(final String property) {
//		//- Get the information from the latest turn info.
//		return this.getLatestInfo().getField(property);
//	}

//	/**
//	 * Return the representation color for the ship depending on some factors like if the ship is an enemy or
//	 * neutral or has or not fuel.
//	 */
//	public Color getRepresentationColor() {
//		final int owner = this.getLatestInfo().getFieldNumber("owner");
//		final int player = map.getPlayer();
//		if (owner != player) return AssistantConstants.COLOR_SHIP_ENEMY;
//
//		return AssistantConstants.COLOR_SHIP_DEFAULT;
//	}

//		public ShipInformation getLatestInfo() {
//			if (null == latestTurnInfo) latestTurnInfo = shipInformation.elementAt(this.getLastTurn());
//			return latestTurnInfo;
//		}
