//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import net.sf.gef.core.models.IContainerModel;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AssistantContainer extends AssistantNode implements IContainerModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= -2551357783317434133L;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	public abstract int getOwner();
	//
	//	@Override
	//	public abstract String getField(final String key);
	//
	//	@Override
	//	public abstract int getFieldNumber(final String key);
}

// - UNUSED CODE ............................................................................................
