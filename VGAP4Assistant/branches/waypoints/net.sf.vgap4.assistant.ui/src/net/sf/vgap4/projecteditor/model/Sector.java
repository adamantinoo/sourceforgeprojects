//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.models.AssistantNode;

// - CLASS IMPLEMENTATION ...................................................................................
public class Sector extends AssistantNode {
	private static final long	serialVersionUID	= 7553570053350141158L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");

	// - F I E L D - S E C T I O N ............................................................................
	private final int					width;
	private final int					height;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Sector(final String name, final int x, final int y, final int width, final int height) {
		this.setName(name);
		this.setLocation(new Point(x, y));
		this.width = width;
		this.height = height;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public String getField(final String key) {
		return key;
	}

	@Override
	public int getFieldNumber(final String key) {
		try {
			return this.convert2Integer(key);
		} catch (final Exception exc) {
			//- The field is null or not convertible to a number. Return the default ZERO.
			return 0;
		}
	}

	@Override
	public int getOwner() {
		return 0;
	}

	protected int convert2Integer(final String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}
}
// - UNUSED CODE ............................................................................................
