//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;

import es.ftgroup.gef.model.AbstractNode;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AssistantNode extends AbstractNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String	NAME_PROP					= "PositionableUnit.NAME_PROP";
	public static final String	PROP_LOCATION			= "PositionableUnit.PROP_LOCATION";
	public static final String	FLD_ID						= "AssistantNode.FLD_ID";
	//	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	private static final long		serialVersionUID	= -5137898479615467215L;

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	// - M O D E L F I E L D S
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	private int									idNumber					= -1;
	/**
	 * Stores the planet name as identified in the source data. This is also a copy of the name found on the
	 * <code>PlanetInformation</code>.
	 */
	private String							name;
	/**
	 * Stores the Friend or Foe status for this element. Map elements that do not belong to any player have an
	 * Undefined state.
	 */
	private String							FFStatus					= AssistantConstants.FFSTATUS_UNDEFINED;
	/**
	 * Reference number to the latest imported turn data. The latest is considered the turn data with the
	 * highest turn number than has been parsed.
	 */
	private int									latestTurnNo			= -1;
	/** Stores the cache of the owner player code of the latest owner for this element. */
	private String							notes							= "";
	/**
	 * This is the location of the unit in the diagram. Contains the x,y coordinates to locate the unit in the
	 * diagram coordinate system.
	 */
	private Point								location;

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getFFStatus() {
		return FFStatus;
	}

	//protected abstract void setCachedInformation();
	public abstract String getField(final String key);

	public abstract int getFieldNumber(final String key);

	public String getIdentifier() {
		return this.getIdString() + " - " + this.getName();
	}

	public int getIdNumber() {
		return idNumber;
	}

	public int getLatestTurnNumber() {
		return latestTurnNo;
	}

	/**
	 * Return a copy of the location for processing. Do not allow that side effect modify the location or other
	 * fields without the use of accessors.
	 */
	public Point getLocation() {
		if (null == location) location = new Point(0, 0);
		return location.getCopy();
	}

	public String getLocationString() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(4);
		nf.setMinimumFractionDigits(0);
		return this.getLocation().x + " - " + this.getLocation().y;
	}

	public String getName() {
		return name;
	}

	public String getNotes() {
		return notes;
	}

	public abstract int getOwner();

	public void setFFStatus(final String status) {
		FFStatus = status;
	}

	public void setIdNumber(final int idNumber) {
		this.idNumber = idNumber;
	}

	public void setLatestTurnNumber(final int latestTurnNo) {
		this.latestTurnNo = latestTurnNo;
	}

	public void setLocation(final Point newLocation) {
		final Point oldLocation = location;
		location = newLocation;
		this.firePropertyChange(AssistantNode.PROP_LOCATION, oldLocation, location);
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setNotes(final String text) {
		notes = text;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append('\n').append("[AssistantNode:");
		buffer.append("id=").append(idNumber);
		buffer.append("FFStatus=").append(FFStatus);
		buffer.append("location=").append(location.x).append("-").append(location.y);
		buffer.append("]");
		return buffer.toString();
	}

	private String getIdString() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(3);
		return nf.format(idNumber);
	}

	protected void updateTurnReference(final int turn) {
		latestTurnNo = Math.max(latestTurnNo, turn);
	}
}

// - UNUSED CODE ............................................................................................
