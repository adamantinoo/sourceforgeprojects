//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class IconImageFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static String	SHAPE_PLANET					= "IconImageFactory.SHAPE_PLANET";
	public static String	SHAPE_SHIP						= "IconImageFactory.SHAPE_SHIP";
	public static String	SHAPE_POD							= "IconImageFactory.SHAPE_POD";
	public static String	SHAPE_WING						= "IconImageFactory.SHAPE_WING";
	//	private static Logger	logger								= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private String				shapeCode							= IconImageFactory.SHAPE_PLANET;
	private Color					mainShapeColor				= AssistantConstants.COLOR_PLANET_UNEXPLORED;
	private final Color		nativesColor					= AssistantConstants.COLOR_NATIVES_DEFAULT;
	private Color					shipsColor						= AssistantConstants.COLOR_SHIP_DEFAULT;
	private Color					podsColor							= AssistantConstants.COLOR_SHIP_DEFAULT;
	private final Color		wingsColor						= AssistantConstants.COLOR_SHIP_DEFAULT;
	//	private Color					decoratorColor	= AssistantConstants.COLOR_SHIP_DEFAULT;
	private final int			drawingSize						= AssistantConstants.SIZE_ICONIMAGE;
	private boolean				baseActive						= false;
	private boolean				shipInOrbit						= false;
	private boolean				podInOrbit						= false;
	private boolean				hasNatives						= false;
	private boolean				wingsInOrbit					= false;
	private boolean				isOnTip								= false;
	//	private String				informationLevel			= AssistantConstants.INFOLEVEL_INFO;
	private String				planetClassification	= "B";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IconImageFactory() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void activateBase(final boolean enemyFlag) {
		baseActive = true;
		if (enemyFlag)
			mainShapeColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			mainShapeColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateNatives() {
		hasNatives = true;
	}

	public void activatePods(final boolean enemyFlag) {
		podInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateShip(final boolean enemyFlag) {
		shipInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void activateWings(final boolean enemyFlag) {
		wingsInOrbit = true;
		if (enemyFlag)
			podsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			podsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void clearActivates() {
		baseActive = false;
		hasNatives = false;
		shipInOrbit = false;
		podInOrbit = false;
		wingsInOrbit = false;
	}

	public void clearBase() {
		baseActive = false;
	}

	public void clearNatives() {
		hasNatives = false;
	}

	public void clearPods() {
		podInOrbit = false;
	}

	public void clearShip() {
		shipInOrbit = false;
	}

	public void clearWings() {
		wingsInOrbit = false;
	}

	public Image generateImage() {
		final Image image = new Image(Display.getDefault(), drawingSize, drawingSize);
		final GC gc = new GC(image);

		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = new Rectangle(0, 0, drawingSize, drawingSize);

		//- If the image is to be located into a tip, then fill the background color.
		if (isOnTip) {
			gc.setBackground(AssistantConstants.COLOR_TIP);
			gc.fillRectangle(bound);
		}
		gc.setAlpha(0);
		gc.fillRectangle(bound);
		gc.setAlpha(255);

		//- Draw the shape.
		if (IconImageFactory.SHAPE_PLANET.equals(shapeCode)) {
			//- Draw the Planet circle
			gc.setBackground(mainShapeColor);
			final Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);

			//- Draw the Planet classification
			if (planetClassification.equals("A")) {
				gc.setForeground(AssistantConstants.COLOR_BRILLIANT_RED);
				gc.drawLine(15, 0, 15 - 3, 3);
				gc.drawLine(15 - 3, 3, 15 - 1, 3);
				gc.drawLine(15 - 3, 3, 15 - 3, 1);
			}
			if (planetClassification.equals("C")) {
				gc.setForeground(ColorConstants.darkGray);
				gc.drawLine(15, 0, 15 - 3, 3);
				gc.drawLine(15 - 3, 0, 15, 3);
			}
		}
		if (IconImageFactory.SHAPE_SHIP.equals(shapeCode)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + 3, bound.y + drawingSize / 2 - 1);
			final Point p2 = new Point(head.x + 10 - 1, head.y - 4);
			final Point p3 = new Point(head.x + 10 - 1, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			gc.setBackground(shipsColor);
			gc.fillPolygon(triangle.toIntArray());
		}
		if (IconImageFactory.SHAPE_POD.equals(shapeCode)) {
			//- Draw the Planet circle
			gc.setBackground(shipsColor);
			Rectangle planetBounds = new Rectangle(bound.x + 2, bound.y + 2, 7, 7);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
			planetBounds = new Rectangle(bound.x + 7, bound.y + 7, 7, 7);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
		}
		if (IconImageFactory.SHAPE_WING.equals(shapeCode)) {
			//- Draw the Ship shape.
			final PointList triangle = new PointList(3);
			final Point head = new Point(bound.x + 3, bound.y + drawingSize / 2 - 1);
			final Point p2 = new Point(head.x + 10, head.y - 4);
			final Point p3 = new Point(head.x + 10, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			gc.setBackground(shipsColor);
			gc.fillPolygon(triangle.toIntArray());
		}
		//- Draw the Base decorator
		if (baseActive) {
			gc.setForeground(ColorConstants.lightGray);
			//			gc.setBackground(ColorConstants.lightGray);
			gc.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods or wings.
		if (shipInOrbit) {
			gc.setBackground(shipsColor);
			gc.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			gc.setBackground(podsColor);
			gc.fillOval(bound.x + 10, bound.y + 10, 4, 4);
		}
		if (wingsInOrbit) {
			gc.setBackground(wingsColor);
			gc.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}
		if (hasNatives) {
			gc.setBackground(nativesColor);
			gc.fillOval(bound.x + 1, bound.y + 1, 4, 4);
		}
		gc.dispose();
		return image;
	}

	public void setEnemy(final boolean enemy) {
		if (enemy)
			shipsColor = AssistantConstants.COLOR_SHIP_ENEMY;
		else
			shipsColor = AssistantConstants.COLOR_SHIP_DEFAULT;
	}

	public void setInfoLevel(final String level) {
		//		informationLevel = level;
		if (AssistantConstants.INFOLEVEL_UNEXPLORED.equals(level))
			mainShapeColor = AssistantConstants.COLOR_PLANET_UNEXPLORED;
		if (AssistantConstants.INFOLEVEL_NOINFO.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_NOINFO;
		if (AssistantConstants.INFOLEVEL_INFO.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_INFO;
		if (AssistantConstants.INFOLEVEL_OBSOLETE.equals(level)) mainShapeColor = AssistantConstants.COLOR_PLANET_OBSOLETE;
		if (AssistantConstants.INFOLEVEL_NATIVES.equals(level)) mainShapeColor = AssistantConstants.COLOR_NATIVES_DEFAULT;
	}

	public void setPlanetClassification(final String planetClassification) {
		this.planetClassification = planetClassification;
	}

	public void setShape(final String shape) {
		shapeCode = shape;
	}

	public void setTip(final boolean onTip) {
		isOnTip = onTip;
	}
}

// - UNUSED CODE ............................................................................................
