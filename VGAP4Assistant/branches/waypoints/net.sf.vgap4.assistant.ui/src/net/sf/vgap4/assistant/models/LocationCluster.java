//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class LocationCluster implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long			serialVersionUID	= 1L;

	// - F I E L D - S E C T I O N ............................................................................
	private Vector<AssistantNode>	clusterContents		= new Vector<AssistantNode>();
	private boolean								hasBase						= false;
	private boolean								hasPlanet					= false;
	private boolean								hasShip						= false;
	private AssistantNode					representative;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public LocationCluster() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void add(AssistantNode mapElement) {
		if (mapElement instanceof Ship) {
			hasShip = true;
			if (hasBase || hasPlanet)
				hasShip = true;
			else
				representative = mapElement;
		}
		if (mapElement instanceof Planet) {
			hasPlanet = true;
			if (hasBase)
				hasPlanet = true;
			else
				representative = mapElement;
		}
		if (mapElement instanceof Base) {
			hasBase = true;
			representative = mapElement;
		}
		clusterContents.add(mapElement);
	}

	public boolean hasContents() {
		if (null != representative) return true;
		return false;
	}

	public AssistantNode getRepresentative() {
		return representative;
	}

	public boolean isMultiple() {
		if (this.clusterContents.size() > 1)
			return true;
		else
			return false;
	}

	public String getPreferredShape() {
		if (hasBase) return "Base";
		if (hasPlanet) return "Planet";
		if (hasShip) return "Ship";
		return "Undefined";
	}

	public Vector<AssistantNode> getContents() {
		return this.clusterContents;
	}
}
// - UNUSED CODE ............................................................................................
