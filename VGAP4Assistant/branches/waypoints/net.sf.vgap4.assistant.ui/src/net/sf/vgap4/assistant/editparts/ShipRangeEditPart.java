//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.editparts.AbstractNodeEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.ShipRange;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipRangeEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public ShipRangeEditPart() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean isSelectable() {
		return false;
	}

	public void propertyChange(final PropertyChangeEvent evt) {

	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}

	//	@Override
	//	public ShipRange getCastedModel() {
	//		return (ShipRange) this.getModel();
	//	}

	@Override
	protected IFigure createFigure() {
		return new RangeFigure();
	}

	protected int getZoomFactor() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getZoomFactor();
	}

	@Override
	protected void refreshVisuals() {
		try {
			//			this.refreshFigureImage();
			//			this.refreshToolTip();
			this.refreshModelVisuals();
			this.refreshFigureLocation();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private Rectangle getMapBoundaries() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getSpotRegistry().getBoundaries().getCopy();
	}

	private int getMapSize() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getMapSize();
	}

	private void refreshFigureLocation() {
		// - The references to the model and figure objects.
		final RangeFigure fig = (RangeFigure) this.getFigure();
		final ShipRange model = (ShipRange) this.getCastedModel();

		//- Get the location from the model information and then convert to the final coordinates.
		final Dimension size = fig.getPreferredSize();
		final Point hotSpot = fig.getHotSpot();
		final Point location = model.getLocation();

		//- Invert the coordinates for the Y axis depending on the map size.
		final int mapSize = this.getMapSize();
		location.y = mapSize - location.y;

		//- Adjust location depending on the top-left minimum coordinates.
		final Rectangle boundaries = this.getMapBoundaries();
		final Point origin = new Point(Math.max(boundaries.x - 30, 0), Math.max(boundaries.height - 30, 0));
		location.x = location.x - origin.x;
		location.y = location.y - (mapSize - origin.y);
		location.x -= hotSpot.x;
		location.y -= hotSpot.y;
		fig.setLocation(location);

		//- Calculate real map location depending on the zoom factor.
		final int zoom = this.getZoomFactor();
		if ((zoom > 10) || (zoom < 10)) {
			if (zoom > 0) {
				final double factor = zoom / 10.0;
				location.x = new Double(location.x * factor).intValue();
				location.y = new Double(location.y * factor).intValue();
			}
		}

		final Rectangle bounds = new Rectangle(location, size);
		ShipRangeEditPart.logger.info("Rebounding instance " + model.getType() + " to " + bounds);
		fig.setBounds(bounds);
		((GraphicalEditPart) this.getParent()).setLayoutConstraint(this, fig, bounds);
	}

	private void refreshModelVisuals() {
		// - The references to the model and figure objects.
		final RangeFigure fig = (RangeFigure) this.getFigure();
		final ShipRange model = (ShipRange) this.getCastedModel();

		//		 fig.setType(((ShipRange) model).getType());
		//		 fig.setRange(model.getRange());
		//		 fig.setFfStatus(model.getFFStatus());
		fig.setAttributes((model).getType(), model.getRange(), model.getFFStatus());
	}
}

class RangeFigure extends Figure {
	private String	type;
	private int			range;
	private String	FFstatus;

	public Point getHotSpot() {
		return new Point(range / 2, range / 2);
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(range, range);
	}

	@Override
	public void paint(final Graphics graphics) {
		super.paint(graphics);
	}

	public void setAttributes(final String type, final int range, final String status) {
		this.type = type;
		this.range = range;
		FFstatus = status;
	}

	//	public void setFfStatus(final String status) {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	public void setRange(final int range) {
	//		// TODO Auto-generated method stub
	//
	//	}
	//
	//	public void setType(final String type) {
	//		// TODO Auto-generated method stub
	//
	//	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		if (AssistantConstants.FFSTATUS_FOE.equals(FFstatus))
			graphics.setBackgroundColor(AssistantConstants.COLOR_FOE_BACKGROUND);
		else
			graphics.setBackgroundColor(AssistantConstants.COLOR_FRIEND_BACKGROUND);
		final Rectangle boundaries = this.getBounds().getCopy();
		boundaries.width--;
		boundaries.height--;
		graphics.setAlpha(256 / 3);
		graphics.fillOval(boundaries);
	}
}
// - UNUSED CODE ............................................................................................
