//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.database;

import java.util.Hashtable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipDatabase {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static class Hull {

		private String	name;
		private int			maxSpeed	= -1;

		public Hull(String name, int maxSpeed) {
			this.name = name;
			this.maxSpeed = maxSpeed;
		}

		public int getMaxSpeed() {
			if (maxSpeed < 0)
				return 20;
			else
				return this.maxSpeed;
		}

		public String getDescription() {
			if (null == this.name)
				return "N/A";
			else
				return this.name;
		}

	}

	public static class Engine {
		private String	name;
		private int			speed	= -1;

		public Engine(final String name, final int speed) {
			this.name = name;
			this.speed = speed;
		}

		public String getName() {
			return name;
		}

		public int getSpeed() {
			if (speed < 0)
				return 20;
			else
				return speed;
		}
	}

	public static class Weapon {
		private final String	type;
		private final String	name;
		private final int			maxRange;

		public Weapon(final String type, final String name, final int range) {
			this.type = type;
			this.name = name;
			maxRange = range;
		}

		public int getMaxRange() {
			return maxRange;
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}
	}

	//	private static Logger			logger			= Logger.getLogger("net.sf.vgap4.assistant.models.database");
	public static final String								HUGE_WEAPON		= "HW";
	public static final String								LARGE_WEAPON	= "LW";
	private static Hashtable<Integer, Hull>		shipHulls			= new Hashtable<Integer, Hull>(5);
	private static Hashtable<String, Weapon>	weapons				= new Hashtable<String, Weapon>(5);
	private static Hashtable<Integer, Engine>	engines				= new Hashtable<Integer, Engine>(10);
	private static Hashtable<Integer, Engine>	hypEngines		= new Hashtable<Integer, Engine>(10);
	static {
		ShipDatabase.shipHulls.put(24, new Hull("Small Space Freighter", 120));
		ShipDatabase.shipHulls.put(32, new Hull("Firestorm Class Cruiser", 90));
	}
	static {
		ShipDatabase.weapons.put(ShipDatabase.HUGE_WEAPON + 1, new Weapon(ShipDatabase.HUGE_WEAPON, "Super Laser", 100));
		ShipDatabase.weapons
				.put(ShipDatabase.HUGE_WEAPON + 2, new Weapon(ShipDatabase.HUGE_WEAPON, "Nemesis Torpedo", 100));
		ShipDatabase.weapons.put(ShipDatabase.HUGE_WEAPON + 3, new Weapon(ShipDatabase.HUGE_WEAPON,
				"World Crusher Missile", 100));
		ShipDatabase.weapons.put(ShipDatabase.HUGE_WEAPON + 4,
				new Weapon(ShipDatabase.HUGE_WEAPON, "Anti-Matter Maul", 100));
		ShipDatabase.weapons.put(ShipDatabase.HUGE_WEAPON + 5, new Weapon(ShipDatabase.HUGE_WEAPON, "Proto-Matter Cannon",
				100));
	}

	static {
		ShipDatabase.engines.put(1, new Engine("N20 Fusion Drive", 20));
		ShipDatabase.engines.put(2, new Engine("N30 Fusion Drive", 30));
		ShipDatabase.engines.put(3, new Engine("Lasno-1 Ion Drive", 10));
		ShipDatabase.engines.put(4, new Engine("Lasno-2 Ion Drive", 10));
		ShipDatabase.engines.put(5, new Engine("R50 Mega-Fusion", 10));
		ShipDatabase.engines.put(6, new Engine("I-17 Impulse", 10));
		ShipDatabase.engines.put(7, new Engine("I-20 Impulse", 10));
		ShipDatabase.engines.put(8, new Engine("I-25 Impulse", 50));
		ShipDatabase.engines.put(9, new Engine("Electrogravitic", 10));
		ShipDatabase.engines.put(10, new Engine("Turbo Thruster", 10));
		ShipDatabase.engines.put(11, new Engine("Tylium Thruster", 10));
		ShipDatabase.engines.put(12, new Engine("Energy Conversion", 10));
		ShipDatabase.engines.put(13, new Engine("Scalar Wave Thruster", 10));
		ShipDatabase.engines.put(14, new Engine("Lazar Drive", 10));
		ShipDatabase.engines.put(15, new Engine("FLT-1 Warp Drive", 100));
		ShipDatabase.engines.put(16, new Engine("FLT-2 Warp Drive", 120));
		ShipDatabase.engines.put(17, new Engine("FLT-3 Warp Drive", 10));
		ShipDatabase.engines.put(18, new Engine("FLT-4 Warp Drive", 10));
		ShipDatabase.engines.put(19, new Engine("FLT-5 Warp Drive", 10));
		ShipDatabase.engines.put(20, new Engine("Transwarp Drive", 10));
	}

	static {
		ShipDatabase.hypEngines.put(1, new Engine("Yoyodyne 88", 10));
		ShipDatabase.hypEngines.put(2, new Engine("Microfold 200", 10));
		ShipDatabase.hypEngines.put(3, new Engine("Yoyodyne 300", 10));
		ShipDatabase.hypEngines.put(4, new Engine("Keplan 450", 10));
		ShipDatabase.hypEngines.put(5, new Engine("Keplan 505", 10));
		ShipDatabase.hypEngines.put(6, new Engine("Keplan 660", 10));
		ShipDatabase.hypEngines.put(7, new Engine("Yoyodyne 900", 10));
		ShipDatabase.hypEngines.put(8, new Engine("Soleum H1000", 10));
		ShipDatabase.hypEngines.put(9, new Engine("Warhop 2020", 10));
		ShipDatabase.hypEngines.put(10, new Engine("Cydonia 3000", 10));
	}

	public static String getEngineDescription(final int key) {
		final Engine reference = ShipDatabase.engines.get(key);
		if (null == reference)
			return "N/A";
		else
			return reference.getName();
	}

	// - S T A T I C   M E T H O D - S E C T I O N ............................................................
	public static String getHullDescription(final int key) {
		Hull hull = shipHulls.get(key);
		if (null != hull)
			return hull.getDescription();
		else
			return "N/A";
	}

	public static String getHypEngineDescription(final int key) {
		final Engine reference = ShipDatabase.hypEngines.get(key);
		if (null == reference)
			return "N/A";
		else
			return reference.getName();
	}

	public static Weapon getWeapon(final String type, final int code) {
		return ShipDatabase.weapons.get(type + code);
	}

	public static String getWeaponName(final String type, final int code) {
		final Weapon reference = ShipDatabase.weapons.get(type + code);
		if (null == reference)
			return "N/A";
		else
			return reference.getName();
	}

	public static int getHullMaxSpeed(int hullCode) {
		Hull hull = shipHulls.get(hullCode);
		if (null != hull)
			return hull.getMaxSpeed();
		else
			return 20;
	}

	public static int getEngineSpeed(int engineCode) {
		final Engine reference = ShipDatabase.engines.get(engineCode);
		if (null != reference)
			return reference.getSpeed();
		else
			return 20;
	}

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
}
// - UNUSED CODE ............................................................................................
