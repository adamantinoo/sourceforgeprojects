//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.helpers;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;

import es.ftgroup.gef.model.AbstractNode;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class NodeFilter implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.helpers");
	private static final long	serialVersionUID	= -9182167801973177453L;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public abstract boolean test(final AbstractNode item);
}
// - UNUSED CODE ............................................................................................
