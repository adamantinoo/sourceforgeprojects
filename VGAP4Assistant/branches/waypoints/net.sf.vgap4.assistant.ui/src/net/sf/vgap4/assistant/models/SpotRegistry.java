//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class stores the list and the methods to handle the list of visible elements that are to be
 * represented on a Map. This is kept as a complex structure that contains the Location-Spot pairs that have
 * all the visible elements.
 */
public class SpotRegistry implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger									logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long							serialVersionUID	= -5733718016742691873L;

	// - F I E L D - S E C T I O N ............................................................................
	private final Hashtable<Point, Spot>	locations					= new Hashtable<Point, Spot>();
	private int														minX							= 9000;
	private int														minY							= 9000;
	private int														maxX							= -1;
	private int														maxY							= -1;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public SpotRegistry() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void clear() {
		locations.clear();
		minX = 9000;
		minY = 9000;
		maxX = -1;
		maxY = -1;
	}

	public Rectangle getBoundaries() {
		return new Rectangle(minX, minY, maxX, maxY);
	}

	//[01]
	/** Return the list of Spots that contain more data or from other the ShipRanges. */
	public Collection<Spot> getLocationData() {
		final Vector<Spot> spots = new Vector<Spot>(locations.size());
		final Iterator<Spot> sit = locations.values().iterator();
		while (sit.hasNext()) {
			final Spot spot = sit.next();
			if (true != spot.onlyRanges) spots.add(spot);
		}
		return spots;
	}

	/**
	 * Get the aggregated and filtered list of ShipRanges stored inside the spots. For any single spot get the
	 * max engine range and return a single element to be represented on the Map.
	 */
	public Vector<ShipRange> getRanges() {
		final Vector<ShipRange> ranges = new Vector<ShipRange>(2);
		final Iterator<Spot> sit = locations.values().iterator();
		while (sit.hasNext()) {
			final Spot spot = sit.next();
			ranges.addAll(spot.getRanges());
		}
		return ranges;
	}

	/**
	 * Adds a new element to the list of spot contents and creates a new spot if this element location is
	 * different from the current registered locations. If the location matches with a precedent element, both
	 * are stored at the same point and the Spot becomes a multiple container spot.
	 */
	public void register(final Point location, final AssistantNode mapElement) {
		if ((null != location) && (null != mapElement)) {
			//- Check if this location has already been used. If so add this new element to the vector.
			Spot cluster = locations.get(location);
			if (null == cluster) {
				//- New location. Create and initialize a new Spot.
				cluster = new Spot();
				cluster.add(mapElement);
				locations.put(location, cluster);
			} else {
				cluster.add(mapElement);
			}

			//- Update Map extents depending on the detected coordinates for the spots.
			this.updateBoundaries(location);
		}
	}

	/**
	 * Add a new element that has an inheritance path from <code>AbstractNode</code>. This are the
	 * representation for the simple <code>ShipRange</code> that only add some graphical representation to the
	 * Map but not to the rest of the Spot contents.
	 */
	public void register(final Point location, final ShipRange rangeElement) {
		if ((null != location) && (null != rangeElement)) {
			//- Check if this location has already been used. If so add this new element to the vector.
			Spot cluster = locations.get(location);
			if (null == cluster) {
				//- New location. Create and initialize a new Spot.
				cluster = new Spot();
				cluster.add(rangeElement);
				locations.put(location, cluster);
			} else {
				cluster.add(rangeElement);
			}

			//- Update Map extents depending on the detected coordinates for the spots.
			this.updateBoundaries(location);
		}
	}

	private void updateBoundaries(final Point location) {
		minX = Math.min(minX, location.x);
		minY = Math.min(minY, location.y);
		maxX = Math.max(maxX, location.x);
		maxY = Math.max(maxY, location.y);
	}
}
// - UNUSED CODE ............................................................................................
//[01]
//	public Enumeration<Point> getLocations() {
//		return locations.keys();
//	}
//public Spot getSpot(final Point location) {
//return locations.get(location);
//}
