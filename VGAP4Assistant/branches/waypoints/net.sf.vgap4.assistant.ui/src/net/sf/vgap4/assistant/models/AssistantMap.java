//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractDiagram;
import es.ftgroup.gef.model.AbstractNode;
import net.sf.vgap4.assistant.models.helpers.NodeFilter;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.model.Sector;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantMap extends AbstractDiagram {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	/** Property ID to use when new model data is added to the diagram. */
	public static final String								DATA_ADDED_PROP					= "AssistantMap.DATA_ADDED_PROP";
	/** Property ID to use when a child is added to this diagram. */
	public static final String								CHILD_ADDED_PROP				= "AssistantMap.CHILD_ADDED_PROP";
	/** Property ID to use when a child is removed from this diagram. */
	public static final String								CHANGE_ZOOMFACTOR				= "AssistantMap.CHANGE_ZOOMFACTOR";
	/** Property ID to use when a the number referencing any of the last turns gets changed. */
	public static final String								LAST_TURN_CHANGED_PROP	= "AssistantMap.LAST_TURN_CHANGED_PROP";
	private static final long									serialVersionUID				= 3361707471258570866L;

	private static Logger											logger									= Logger.getLogger("net.sf.vgap4.assistant.models");
	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D S
	/** Number of the last turn processed. */
	private int																lastReadTurn						= -1;
	/** Number of the higher turn processed. */
	private int																lastAvailableTurn				= -1;
	/** Number the identifies a unique game code. */
	private String														gameId;
	/** Name given to this game when created. */
	private String														gameName;
	/**
	 * This value is an estimation. Y can not get such information form the .CSV files data. It should be a
	 * configuration parameter editable though the Application Properties.
	 */
	private final int													mapSize									= 3000;
	/**
	 * Player number that matches the slot number on the game. This number may correspond to another user inside
	 * other games so it is used with the 'gameId' to uniquely identify a player slot on a game set.
	 */
	private int																playerNo								= -1;
	/**
	 * Sectors are virtual structures that keep lists of grouped elements (generally by geographic location).
	 * This field stores the references to all the sectors currently defined on this Map. This is a simple list
	 * structure and currently sectors do not have any sorting order or organization. A sector may contain other
	 * sectors to any level because it simply defines a physical rectangular map area.
	 */
	private final Vector<Sector>							sectors									= new Vector<Sector>();
	/**
	 * Array structure with references to all the Planets that have been identified on this turn or on other
	 * turns. This structure is implemented as an array because it matches the original implementation from CSV
	 * files. Probably a <code>Hashtable</code> will generate less memory usage, over all in very long games
	 * where the Id of some elements may grew high. Planets are the exception because there is a finite number
	 * of them and alwais get the lowest element Id.
	 */
	private final Hashtable<Integer, Planet>	planets									= new Hashtable<Integer, Planet>();
	/**
	 * This structure contains a reference to all player bases. This structure clearly benefits from the
	 * 'Hashtable' implementation because the number of bases for this player is small in relation to the number
	 * of planets and the number of other elements created in the game.
	 */
	private final Hashtable<Integer, Base>		bases										= new Hashtable<Integer, Base>();
	/**
	 * Another array structure that contains all the identified Ships. This list contains all ships owned by
	 * this player and also the ships detected to other players. That other ships are identified by being the
	 * 'owner' a different code than the game 'playerId'.
	 */
	private final Hashtable<Integer, Ship>		ships										= new Hashtable<Integer, Ship>();
	/**
	 * This structure stores the generic elements supported by the model. Any model element with an unique ID
	 * may be stored together with other objects if the client classes are able to understand the type
	 * overloading.
	 */
	private final Hashtable<Integer, Thing>		things									= new Hashtable<Integer, Thing>();
	/**
	 * This structure stores the map elements but clustered by the location where they are presented. This will
	 * simplify the rendering of multiple elements at the same place reducing map data overlaying.<br>
	 * The structure is updated when the model suffers an structural change, like when new turn data is read or
	 * in the future when any user element is added to the model.
	 */
	private final SpotRegistry								spots										= new SpotRegistry();
	/**
	 * Stores the list of all defined waypoints obtained from all ships that are visible and selectable. This
	 * list is used to generate the correct EditParts for Map visualization.
	 */
	private Vector<Waypoint>									waypoints								= new Vector<Waypoint>(1);
	private int																zoomFactor							= AssistantConstants.DEFAULT_ZOOM_FACTOR;
	/**
	 * The detected maximum of the sum of all available minerals for all known planets. This value is used to
	 * calculate the normal deviation and to classify the Planets accordingly to their mineral reserves.
	 */
	private final long												maxTotalMinerals				= 0;
	/** List of user Map filters to show/hide some groups of information from the Map. */
	private final Vector<NodeFilter>					filters									= new Vector<NodeFilter>();
	{
		//- Initialize to remove from the Map all bases that are not found on the latest turn.
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969787L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof Base) {
					//- Test if the base is defined in the last turn information.
					//- Get the Map latest turn.
					final int mapTurn = ((Base) item).getMap().getLastAvailableTurn();
					//- Get the Base latest turn.
					final int itemTurn = ((AssistantNode) item).getLatestTurnNumber();
					if (itemTurn == mapTurn)
						return true;
					else
						return false;
				} else
					return true;
			}
		});
		//- Initialize to remove from the Map all Ships that have been destroyed in previous turns.
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969788L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof Ship) {
					//- Test if the ship is defined in the last turn information.
					//- Get the Map latest turn.
					final int mapTurn = ((Ship) item).getMap().getLastAvailableTurn();
					//- Get the Base latest turn.
					final int itemTurn = ((AssistantNode) item).getLatestTurnNumber();
					if (itemTurn == mapTurn)
						return true;
					else
						return false;
				} else
					return true;
			}
		});
		//- Remove from the model the MAXRANGE elements.
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969787L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof ShipRange)
					if (ShipRange.MAXRANGE.equals(((ShipRange) item).getType()))
						return false;
					else
						return true;
				else
					return true;
			}
		});
		filters.add(new NodeFilter() {
			private static final long	serialVersionUID	= 5089730478538969787L;

			@Override
			public boolean test(final AbstractNode item) {
				if (item instanceof ShipRange)
					if (ShipRange.ENGINE.equals(((ShipRange) item).getType()))
						return false;
					else
						return true;
				else
					return true;
			}
		});
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addBaseInformation(final int turn, final BaseInformation baseInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Base theBase = this.getBase4Id(baseInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theBase.addTurnInformation(turn, baseInfo);
	}

	/**
	 * Add the new turn information to the Planet structure. We have not to check that the owner and game
	 * matches because that test have been performed before arriving this point. But some check has to be
	 * performed to identify when we have not located the correct Planet. The variable 'thePlanet' may not be
	 * allowed to be NULL because we have a reference on it.
	 */
	public void addPlanetInformation(final int turn, final PlanetInformation planetInfo) {
		// - Get the Planet structure that matched to the Planet ID.
		final Planet thePlanet = this.getPlanet4Id(planetInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		thePlanet.addTurnInformation(turn, planetInfo);
	}

	public void addShipInformation(final int turn, final ShipInformation shipInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Ship theShip = this.getShip4Id(shipInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theShip.addTurnInformation(turn, shipInfo);
	}

	public void addThingInformation(final int turn, final AssistantTurnInfo turnInfo) {
		// - Check for the turn identification and the model ID to locate where to store this data.
		final Thing theThing = this.getThing4Id(turnInfo.getIdNumber(), turnInfo.getModelType());
		theThing.addTurnInformation(turn, turnInfo);
	}

	public void calculatePlanetClassification() {
		if (true) {
			//- Calculate the maximum and minimum.
			//			int sumMinerals = 0;
			int minMinerals = Integer.MAX_VALUE;
			int maxMinerals = -1;
			//			int planetCounter = 0;

			//		int total = 0;
			Iterator<Planet> pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				final int total = thePlanet.getTotalOre();
				if (total > 0) {
					//					sumMinerals += total;
					minMinerals = Math.min(minMinerals, total);
					maxMinerals = Math.max(maxMinerals, total);
					//					planetCounter++;
				}
			}
			//			final double d1 = 1.0 * maxMinerals - minMinerals * 1.0;
			final int lowRange = minMinerals + (maxMinerals - minMinerals) / 5;
			final int highRange = maxMinerals - (maxMinerals - minMinerals) / 4;

			//- Set classification depending on the range
			pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				thePlanet.setPlanetClassification("B");
				final int total = thePlanet.getTotalOre();
				if (total <= lowRange) thePlanet.setPlanetClassification("C");
				if (total >= highRange) thePlanet.setPlanetClassification("A");
				AssistantMap.logger.info("Set Planet [" + thePlanet.getName() + "] classification to "
						+ thePlanet.getPlanetClassification());
			}
		}
		if (false) {
			//- Get correlation data for classification.
			//- Calculate the median.
			int sumMinerals = 0;
			int minMinerals = Integer.MAX_VALUE;
			int maxMinerals = -1;
			int planetCounter = 0;

			//		int total = 0;
			Iterator<Planet> pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				final int total = thePlanet.getTotalOre();
				if (total > 0) {
					sumMinerals += total;
					minMinerals = Math.min(minMinerals, total);
					maxMinerals = Math.max(maxMinerals, total);
					planetCounter++;
				}
			}
			final double d1 = 1.0 * maxMinerals - minMinerals * 1.0;

			//- Calculate normalized data.
			double nxsum = 0;
			pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				final int x = thePlanet.getTotalOre();
				if (x > 0) {
					final double nx = (x - minMinerals) / d1;
					nxsum += nx;
				}
			}
			final double mu = nxsum / planetCounter;

			//- Calculate range limits.
			double minxmu2 = Double.MAX_VALUE;
			double maxxmu2 = -1.0;
			pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				final int x = thePlanet.getTotalOre();
				if (x > 0) {
					final double nx = (x - minMinerals) / d1;
					final double xmu2 = Math.pow(nx - mu, 2.0);
					minxmu2 = Math.min(minxmu2, xmu2);
					maxxmu2 = Math.max(maxxmu2, xmu2);
				}
			}
			final double rangeup = .8 * (maxxmu2 - minxmu2);
			final double rangedown = .2 * (maxxmu2 - minxmu2);

			//- Normalize value and calculate (N(x) - mu)^2 and classification
			pit = planets.values().iterator();
			while (pit.hasNext()) {
				final Planet thePlanet = pit.next();
				thePlanet.setPlanetClassification("C");
				final int x = thePlanet.getTotalOre();
				if (x > 0) {
					thePlanet.setPlanetClassification("B");
					final double nx = (x - minMinerals) / d1;
					final double xmu2 = Math.pow(nx - mu, 2.0);
					if (xmu2 > rangeup) thePlanet.setPlanetClassification("A");
					if (xmu2 < rangedown) thePlanet.setPlanetClassification("C");
				}
			}
		}
	}

	public String dump() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[AssistantMap:");
		buffer.append("gameId=").append(gameId);
		buffer.append("gameName=").append(gameName).append('\n');
		buffer.append("lastAvailableTurn=").append(lastAvailableTurn);
		buffer.append("lastReadTurn=").append(lastReadTurn).append('\n');
		buffer.append("mapSize=").append(mapSize);
		buffer.append("maxTotalMinerals=").append(maxTotalMinerals);
		buffer.append("playerNo=").append(playerNo);
		buffer.append("zoomFactor=").append(zoomFactor).append('\n');

		//- Dump the information about the Bases
		final Iterator<Base> bit = bases.values().iterator();
		while (bit.hasNext()) {
			buffer.append(bit.next().dump());
		}

		buffer.append("]").append('\n');
		return buffer.toString();
	}

	/**
	 * This method is called when the structure of the model contents of the Map are changed. Initially this
	 * should call the EditPart to initiate any Viewer changes that correspond to the model changes, but in this
	 * particular case and in behalf for optimization, the model structures are also changed so this is reversed
	 * from the normal situation.
	 * 
	 * @see es.ftgroup.gef.model.AbstractPropertyChanger#fireStructureChange(java.lang.String, java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public void fireStructureChange(final String property, final Object dataSet, final Object change) {
		super.fireStructureChange(property, dataSet, change);
	}

	public Base getBase4Id(final int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		final Base theBase = bases.get(id);
		if (null == theBase) {
			//- The base is not located because this is a new element.
			bases.put(id, new Base(this));
			return this.getBase4Id(id);
		} else
			return theBase;
	}

	/**
	 * Return a List of all the visible elements in the Map. In this method the object that lies in the same
	 * location are clustered together inside a <code>Spot</code> structure that will reduce Map presentation
	 * and will clean up the Map.<br>
	 * There are additional types of objects that even they can be on the same location as an Spot are not
	 * included inside one of them, they are the Waypoints. Initially they generate a new EditPart and are not
	 * visible. Visibility in controlled by selection.
	 * 
	 * @return the list of visible Map objects for construction of the EditPart list.
	 */
	@Override
	public Vector<Object> getChildren() {
		final Vector<Object> childs = new Vector<Object>();
		//		//- Add sectors that are a type of object not related with presentation spots.
		//		final Iterator<Sector> itsec = sectors.iterator();
		//		while (itsec.hasNext()) {
		//			final Sector sector = itsec.next();
		//			if (null != sector) childs.add(sector);
		//		}

		childs.addAll(spots.getLocationData());
		childs.addAll(spots.getRanges());
		childs.addAll(this.getWaypoints());
		return childs;
	}

	public String getGameId() {
		return gameId;
	}

	public int getLastAvailableTurn() {
		return lastAvailableTurn;
	}

	public int getLastReadTurn() {
		return lastReadTurn;
	}

	public int getMapSize() {
		return mapSize;
	}

	/**
	 * Check for the array size and the array contents. The array starts empty so any try to access any null
	 * element will trigger an exception. INtercept the exceptions and resize the array adecuately to the
	 * current usage.
	 */
	public Planet getPlanet4Id(final int id) {
		final Planet thePlanet = planets.get(new Integer(id));
		if (null == thePlanet) {
			planets.put(new Integer(id), new Planet(this));
			return planets.get(new Integer(id));
		} else
			return thePlanet;
	}

	public int getPlayer() {
		return playerNo;
	}

	public int getReadTurn() {
		return lastReadTurn;
	}

	public Ship getShip4Id(final int id) {
		final Ship theShip = ships.get(id);
		if (null == theShip) {
			ships.put(id, new Ship(this));
			return ships.get(id);
		} else
			return theShip;
	}

	public SpotRegistry getSpotRegistry() {
		return spots;
	}

	public Thing getThing4Id(final int id, final String modelType) {
		// - Get the element we are searching for. If null create a new empty instance.
		final Thing searched = things.get(new Integer(id));
		if (null == searched) {
			//- Not found so presumed a new element. Get the type from the parameter to create a new instance.
			if (AssistantConstants.MODELTYPE_THING.equals(modelType)) {
				things.put(new Integer(id), new Thing(this));
				return this.getThing4Id(id, modelType);
			}
			if (AssistantConstants.MODELTYPE_POD.equals(modelType)) {
				things.put(new Integer(id), new Pod(this));
				return this.getThing4Id(id, modelType);
			}
			return this.getThing4Id(id, AssistantConstants.MODELTYPE_THING);
		} else
			return searched;
	}

	public int getZoomFactor() {
		return zoomFactor;
	}

	public boolean isInitialized() {
		if ((-1 == lastReadTurn) && (null == gameId))
			return false;
		else
			return true;
	}

	public void resetZoomFactor() {
		final int oldFactor = zoomFactor;
		zoomFactor = AssistantConstants.DEFAULT_ZOOM_FACTOR;
		this.firePropertyChange(AssistantMap.CHANGE_ZOOMFACTOR, oldFactor, zoomFactor);
	}

	public void setGameId(final String gameId) {
		this.gameId = gameId;
	}

	public void setGameName(final String gameName) {
		this.gameName = gameName;
	}

	public void setPlayer(final int playerNo) {
		this.playerNo = playerNo;
	}

	public void setReadTurn(final int readTurn) {
		lastReadTurn = readTurn;
		AssistantMap.logger.info("Setting lastReadTurn to " + readTurn);
		lastAvailableTurn = Math.max(lastAvailableTurn, lastReadTurn);
		AssistantMap.logger.info("Setting lastAvailableTurn to " + readTurn);
	}

	public void setZoomFactor(final int newZoomFactor) {
		if (newZoomFactor > 0) {
			final int oldFactor = zoomFactor;
			zoomFactor = newZoomFactor;
			this.firePropertyChange(AssistantMap.CHANGE_ZOOMFACTOR, oldFactor, zoomFactor);
		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("[AssistantMap:").append('\n');
		buffer.append("gameId=").append(gameId);
		buffer.append("gameName=").append(gameName);
		buffer.append("lastAvailableTurn=").append(lastAvailableTurn);
		buffer.append("lastReadTurn=").append(lastReadTurn);
		buffer.append("mapSize=").append(mapSize);
		buffer.append("maxTotalMinerals=").append(maxTotalMinerals);
		buffer.append("playerNo=").append(playerNo);
		buffer.append("zoomFactor=").append(zoomFactor);
		buffer.append("]");
		return buffer.toString();
	}

	/**
	 * The method updates the spot structures used to keep the related objects that lie in the same and that are
	 * clustered together inside a single <code>Spot</code> structure.<br>
	 * There is also some object filtering to not shown what is selected by the user to be filtered. This is the
	 * initial approach and more development is expected related to filtering.<br>
	 * The waypoint branch has added a new visible Map element that is the Ship Range. This element is
	 * represented as a colorful circle with the size of the greatest ship engine range that is located in that
	 * particular Spot. For representing this we have to get the list of Shop's ranges for any ship located in
	 * the spot and then filter the max representable element. Some filtering is performed because currently,
	 * even other rages are generated, only engine ranges are presented. This is also an element susceptible to
	 * be filtered out by a filtering option.
	 */
	public void updateSpotData() {
		//- Scan the structures to select candidate elements to be grouped into spots.
		spots.clear();
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final Base element = btp.next();
			//- Apply any Base filtering.
			AssistantMap.logger.info("Registering spot coordinates for " + element.toString());
			if (this.applyFilters(element)) {
				spots.register(element.getLocation(), element);
				AssistantMap.logger.info("Adding to spot " + element.getLocationString() + " current element.");
			} else
				AssistantMap.logger.info("Filtered out current element.");
		}
		final Iterator<Planet> itp = planets.values().iterator();
		while (itp.hasNext()) {
			final Planet element = itp.next();
			spots.register(element.getLocation(), element);
		}
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship element = its.next();
			//- Apply any Ship filtering.
			AssistantMap.logger.info("Registering spot coordinates for " + element.toString());
			if (this.applyFilters(element)) {
				spots.register(element.getLocation(), element);
				AssistantMap.logger.info("Adding to spot " + element.getLocationString() + " current element.");

				//TODO This element can add more model element to the representation. Search for ShipRanges.
				final Vector<ShipRange> ranges = element.getShipRanges();
				//TODO Apply any filtering. This is affects to ranges and waypoints.
				final Iterator<ShipRange> rit = ranges.iterator();
				while (rit.hasNext()) {
					final ShipRange range = rit.next();
					AssistantMap.logger.info("Registering spot coordinates for " + range.toString());
					if (this.applyFilters(range)) {
						spots.register(range.getLocation(), range);
						AssistantMap.logger.info("Adding to spot " + range.getLocationString() + " current element.");
					} else
						AssistantMap.logger.info("Filtered out current element.");
				}
			} else
				AssistantMap.logger.info("Filtered out current element.");
		}
		final Iterator<Thing> tts = things.values().iterator();
		while (tts.hasNext()) {
			final Thing element = tts.next();
			spots.register(element.getLocation(), element);
		}
		AssistantMap.logger.info("Generating a new set of spots");
	}

	/**
	 * Collects all waypoints from the visible Ships on the Map. The waypoints are filtered to only the ships
	 * that are present on the latest turn.
	 */
	public void updateWaypointData() {
		waypoints.clear();
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship element = its.next();
			//- Apply any Ship filtering.
			if (this.applyFilters(element)) {
				AssistantMap.logger.info("Analyzing Ship '" + element.getName() + "' for waypoints");
				waypoints.addAll(element.getWaypoints());
			} else
				AssistantMap.logger.info("Filtered out current element.");
		}
	}

	public void zoomMinus() {
		this.setZoomFactor(this.getZoomFactor() - 1);
	}

	public void zoomPlus() {
		this.setZoomFactor(this.getZoomFactor() + 1);
	}

	private boolean applyFilters(final AbstractNode item) {
		//TODO Apply only filters that match the class or generic filters
		final Iterator<NodeFilter> fit = filters.iterator();
		while (fit.hasNext()) {
			final NodeFilter filter = fit.next();
			boolean result = filter.test(item);
			//- If the result is filtered then do not continue. Otherwise apply other filters until filtered or end.
			if (!result) return false;
		}
		return true;
	}

	private Vector<Waypoint> getWaypoints() {
		if (null == waypoints) waypoints = new Vector<Waypoint>(1);
		return waypoints;
	}
}
// - UNUSED CODE ............................................................................................
