//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.ui;

// - IMPORT SECTION .........................................................................................
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import net.sf.vgap4.assistant.views.GraphicalDetailedView;
import net.sf.vgap4.assistant.views.SelectionPropertiesView;

// - CLASS IMPLEMENTATION ...................................................................................
public class MapPerspective implements IPerspectiveFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	protected static final String	MAPPERSPECTIVE_ID	= "net.sf.vgap4.assistant.ui.MapPerspective.id";

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Creates the and configures the editor location and the views that appear on the workspace. This method
	 * defines the relative locations of the elements and the relative sizes. The total size of the workspace is
	 * decided by the user ot the initialization code of the application. The locations of the views and it
	 * sizes are hardcoded inside this method and cannot de declared as configuracle properties.<br>
	 * The elements defines in the Harpoon perspective are three:
	 * <ul>
	 * <li>The Editor panel where we load and display the scenery map.</li>
	 * <li>The Property and selection view, where we display selection properties or selection contents.</li>
	 * <li>The message are where we display game messages and commands results.</li>
	 * </ul>
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		// TODO Register the perspective
		Activator.addReference(MAPPERSPECTIVE_ID, this);

		// - Activate the Editor area. The maps are shown on editor parts.
		final String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);

		// - Define the two views that compose the main presentation window
		layout.addStandaloneView(SelectionPropertiesView.ID, true, IPageLayout.BOTTOM, 0.80f, editorArea);
		layout.getViewLayout(SelectionPropertiesView.ID).setCloseable(true);
		layout.addStandaloneView(GraphicalDetailedView.ID, true, IPageLayout.RIGHT, 0.70f, editorArea);
		layout.getViewLayout(GraphicalDetailedView.ID).setCloseable(true);
	}
}
//- UNUSED CODE ............................................................................................
