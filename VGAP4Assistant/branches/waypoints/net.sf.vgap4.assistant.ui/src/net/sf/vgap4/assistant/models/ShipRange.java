//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.text.NumberFormat;

import es.ftgroup.gef.model.AbstractNode;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipRange extends AbstractNode implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	public static final String	ENGINE						= "ENGINE";
	public static final String	MAXRANGE					= "MAXRANGE";
	public static final String	SPEED							= "SPEED";
	private static final long		serialVersionUID	= -7626725915833816871L;

	// - F I E L D - S E C T I O N ............................................................................
	private final String				type;
	private final int						range;
	private final Point					location;
	private String							FFStatus					= AssistantConstants.FFSTATUS_UNDEFINED;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ShipRange(final String engineType, final int engineSpeed, final Point location, final String FFStatus) {
		type = engineType;
		range = engineSpeed;
		this.location = location;
		this.FFStatus = FFStatus;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getFFStatus() {
		return FFStatus;
	}

	public Point getLocation() {
		return location.getCopy();
	}

	public String getLocationString() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(4);
		nf.setMinimumFractionDigits(0);
		return this.getLocation().x + " - " + this.getLocation().y;
	}

	public int getRange() {
		return range;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[ShipRange:");
		buffer.append("type=").append(type).append(" - ");
		buffer.append("range=").append(range).append(" - ");
		buffer.append("FFStatus=").append(FFStatus).append(" - ");
		buffer.append("location=").append(location);
		buffer.append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
