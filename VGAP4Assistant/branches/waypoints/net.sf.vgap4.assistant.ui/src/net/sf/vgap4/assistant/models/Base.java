//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class Base extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long													serialVersionUID		= -2240881066097908890L;
	private static Logger															logger							= Logger
																																						.getLogger("net.sf.vgap4.assistant.models");
	private static final int													EMPTY_TRIGGER_LEVEL	= 300;
	private static final int													MAX_MINERAL					= 0;
	private static final int													TOTALORE_MINERAL		= 1;
	private static final int													SURFACE_MINERAL			= 2;

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private AssistantMap															map									= null;
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant. Is the user responsibility to add as many turns as available to have all
	 * the data needed to create graphics or take decisions.
	 */
	//	private Vector<BaseInformation>	baseInformation			= new Vector<BaseInformation>();
	private final Hashtable<Integer, BaseInformation>	baseInformation			= new Hashtable<Integer, BaseInformation>();
	//	private transient final BaseInformation						latestTurnInfo			= null;
	//	private BaseInformation														latestTurn					= null;
	/**
	 * Reference to the Planet where this base is located. This may change from turn to turn so this is cached
	 * data that should be accessed though methods to guarantee that all times we get the right reference.
	 */
	private int																				onPlanet						= -1;
	/**
	 * Max ore detected for this planet. This is used to interpolate the drawing extents for the minerals
	 * detected on the planet. It keeps the max values reached by the sum of all the mineral (reserve, ore and
	 * in the surface) found when a new turn is added.
	 */
	private final Vector<Integer>											maxMinerals					= new Vector<Integer>(1);
	{
		maxMinerals.setSize(4);
		maxMinerals.set(MiningInformation.NEUTRONIUM_ID, -1);
		maxMinerals.set(MiningInformation.DURANIUM_ID, -1);
		maxMinerals.set(MiningInformation.TRITANIUM_ID, -1);
		maxMinerals.set(MiningInformation.MOLYBDENUM_ID, -1);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Base(final AssistantMap ownerMap) {
		map = ownerMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Gathers the current turn information into the structures in the Base to get all information from all
	 * turns. While getting this data the procedure compiles some aggregated information or updates cached data
	 * that may have changed.
	 * 
	 * @param turn
	 *          number the identified the game turn sequence.
	 * @param baseInfo
	 *          parsed information compiled into an instance of <code>BaseInformation</code>.
	 */
	public void addTurnInformation(final int turn, final BaseInformation baseInfo) {
		// - Get the old value for this base turn information if available.
		final BaseInformation oldData = baseInformation.get(new Integer(turn));
		//		if (null != oldData)
		//			Base.logger.warning("This operation is overriding some previous information stored for this turn");
		baseInformation.put(new Integer(turn), baseInfo);
		Base.logger.info("Adding turn " + turn + " to Base '" + baseInfo.getName() + "'");

		//- Special check for the first time we load a turn.
		//		if (-1 == this.getIdNumber()) {
		this.setIdNumber(baseInfo.getIdNumber());
		this.setName(baseInfo.getName());
		//		}

		//- Reference the latest turn.
		this.updateTurnReference(turn);
		this.setName(this.getLatestInfo().getName());
		onPlanet = this.getLatestInfo().getFieldNumber("Planet ID");
		//- Calculate the FF status for this ship.
		if (map.getPlayer() == this.getLatestInfo().getFieldNumber("Owner"))
			this.setFFStatus(AssistantConstants.FFSTATUS_FRIEND);
		else
			this.setFFStatus(AssistantConstants.FFSTATUS_FOE);
		//		this.setOwner(getLatestInfo().getFieldNumber("Owner"));
		//		//- Load the latest turn into the cached reference.
		//		latestTurn = baseInformation.get(new Integer(latestTurnNo));

		//		//- Get the Planet reference.
		//		onPlanet = map.getPlanet4Id(latestTurn.getPlanetId());
		//		onPlanet.setBase(this);
		//		this.setLastTurn(turn);

		// - Process max and statistical data.
		this.setLocation(this.getLatestInfo().getLocation());
		Base.logger.info("Processing base id [" + this.getLatestInfo().getIdNumber() + "] - "
				+ this.getLatestInfo().getName() + ". At location [" + this.getLatestInfo().getLocation().x + ", "
				+ this.getLatestInfo().getLocation().y + "]");
	}

	public String dump() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[Base:");

		//- Dump the turn information.
		//		Iterator<BaseInformation> tit = this.baseInformation.values().iterator();
		//		while(tit.hasNext()) {
		//			buffer.append(tit.next().dump());
		//		}

		buffer.append("maxMinerals=").append(maxMinerals.toString());
		buffer.append(super.toString());
		buffer.append("]").append('\n');
		return buffer.toString();
	}

	public int extracted(final int neutroniumId) {
		final AssistantMap map = this.getMap();
		final Planet planet = map.getPlanet4Id(onPlanet);
		//		final int unminnedLatest = planet.getFieldNumber("resN");
		return planet.extracted(neutroniumId);
	}

	public int[] getDisplayableMineralData(final int mineralId) {
		final int[] data = new int[3];
		data[Base.MAX_MINERAL] = maxMinerals.get(mineralId);
		// - Get the information form the last turn data read for this planet.
		//		final BaseInformation info = baseInformation.get(this.getLastTurn());
		data[Base.TOTALORE_MINERAL] = this.getLatestInfo().getMiningInformation().get(mineralId).getTotalOre();
		data[Base.SURFACE_MINERAL] = this.getLatestInfo().getMiningInformation().get(mineralId).getSurfaceMineral();
		return data;
	}

	//	/**
	//	 * Color should be calculated depending on several model data, like the number of resources or the existence
	//	 * of natives. Initial rules may look similar to the rules used on the Excel game sheet.
	//	 * <ul>
	//	 * <li>Bases with orders pending should draw in orange</li>
	//	 * <li>Bases with without orders pending may select the color from the next list:
	//	 * <ul>
	//	 * <li> With the right levels of resources and colonists in light green.</li>
	//	 * <li>With excess resources to be exported in dark green.</li>
	//	 * <li>Going to be dismantled or with the underlying planet empty of minerals in dark blue color.</li>
	//	 * <li>With no update on the last turn in light gray color.</li>
	//	 * <li>With no determinate information or enough info to take a decision then render a dark gray color.</li>
	//	 * </ul>
	//	 * </li>
	//	 * </ul>
	//	 * 
	//	 * @return the preferred color to render for this icon on the figure.
	//	 */
	//	public Color getColor() {
	//		//TODO Calculate the color depending on the rules
	//		final Color setColor = Activator.COLOR_DEFAULTBASE;
	//		//TODO Check for no update on last turn
	//		//- Check for empty Planet
	//		//		final int planetMin = this.getOnPlanet().getMineralTotal();
	//		//		if (planetMin < Base.EMPTY_TRIGGER_LEVEL) setColor = Activator.COLOR_NOMINERALSPLANET;
	//		return setColor;
	//	}

	/**
	 * This method calculates the most optimum number of factories for this base taking on account a multitude
	 * of factors.<br>
	 * The formula used for the calculations depends on the maturity phase defined for the Base. There are
	 * identified 3 phases and each one has a different formula to set u the right number of factories. Even
	 * this formulas, thare is a limit to the number of factories that is set up to <b>sqrt(Colonists x 2)</b><br>
	 * <b>Creation Phase</b>
	 * 
	 * <pre>
	 * Factor�as -&gt; Colonos/1000 si &circ;MC &lt;= 0
	 * Factor�as -&gt; incrementar en 1 si &circ;MC � 1 si &circ;MC &gt; 1 check for Colonist limits
	 * </pre>
	 * 
	 * <b>Defense Phase</b>
	 * 
	 * @return the number of factories recommended for this base.
	 */
	public int getFactoryEstimates() {
		//TODO get the evolution phase for this base.
		//- Get the difference in MC between latest turns.
		final int cash = this.getFieldNumberDiff("Cash");
		if (cash > 1) {
			final int colonists = this.getFieldNumber("Colonists");
			final double factories = Math.sqrt(new Double(colonists).doubleValue() * 2.0);
			final int max = new Double(factories).intValue();
			final int currentFacts = this.getFieldNumber("Factories");
			return Math.min(currentFacts + 1, max);
		} else
			return this.getFieldNumber("Colonists") / 1000;
	}

	/**
	 * Calculates the number of farms recommended for this base. It takes on account the number of colonists,
	 * and the <b>Soil</b> information in order to get the maximum value allowed. It is not checked the farming
	 * range.
	 * 
	 * @return the recommended number of farms set for this base
	 */
	public int getFarmEstimates() {
		final int soil = map.getPlanet4Id(this.getPlanetId()).getFieldNumber("soil");
		return Math.min(soil, this.getFieldNumber("Colonists") / 1000);
	}

	@Override
	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	/**
	 * Get the value difference between the latest turn and the previous turn for a property value.
	 * 
	 * @param key
	 *          the name of the property to calculate
	 * @return the difference between the latest value and the previous value. There is no account for skipped
	 *         turns or obsolete data.
	 */
	public int getFieldNumberDiff(final String key) {
		final int current = this.getLatestInfo().getFieldNumber(key);
		int previous = current;
		final BaseInformation previousInfo = this.getPreviousInfo();
		if (null != previousInfo) previous = previousInfo.getFieldNumber(key);
		return current - previous;
	}

	public AssistantMap getMap() {
		return map;
	}

	/**
	 * Return the estimated number of Mines calculated for this Base. There is an empirical limit in 100 Mines
	 * so we do not go for too many resources
	 * 
	 * @return the number of mines recommended.
	 */
	public int getMineEstimates() {
		return Math.min(100, this.getFieldNumber("Colonists") / 100);
	}

	/**
	 * Returns the contents on the selected mineral or the sum of all minerals on the base if no parameter is
	 * specified.
	 * 
	 * @param mineralName
	 *          the name of the mineral to locate and evaluate. If this parameter is null then return the sum of
	 *          all the minerals present on the base.
	 * @return the number of Kt for the mineral or minerals.
	 */
	public int getMineralsOnBase(final String mineralName) {
		int total = 0;
		if (null == mineralName) {
			//- Sum all the minerals.
			total = this.getFieldNumber("ele N");
			total += this.getFieldNumber("ele D");
			total += this.getFieldNumber("ele T");
			total += this.getFieldNumber("ele M");
		} else {
			if (MiningInformation.NEUTRONIUM_NAME.equals(mineralName)) total = this.getFieldNumber("ele N");
			if (MiningInformation.DURANIUM_NAME.equals(mineralName)) total = this.getFieldNumber("ele D");
			if (MiningInformation.TRITANIUM_NAME.equals(mineralName)) total = this.getFieldNumber("ele T");
			if (MiningInformation.MOLYBDENUM_NAME.equals(mineralName)) total = this.getFieldNumber("ele M");
		}
		return total;
	}

	public int getMineralTotal() {
		int total = 0;
		final Iterator<Integer> mit = maxMinerals.iterator();
		while (mit.hasNext()) {
			final int minQty = mit.next().intValue();
			if (minQty > 0) total += minQty;
		}
		return total;
	}

	public int getOnPlanet() {
		return onPlanet;
	}

	@Override
	public int getOwner() {
		return this.getLatestInfo().getFieldNumber("Owner");
	}

	public int getPlanetId() {
		return this.getLatestInfo().getPlanetId();
	}

	public String getProperty(final String property) {
		//- Get the information from the latest turn info.
		return this.getLatestInfo().getField(property);
	}

	/**
	 * This method calculates the number of smelters recommended for this specific planet. The recommendation is
	 * calculated with the use of the next algorithm:
	 * <ol>
	 * <li>Check the new growing of the ore in the base from previous turns.</li>
	 * <li>If the new ore is growing, try to create another smelter but limit the number of smelters to 12.</li>
	 * </ol>
	 * 
	 * @return the number of smelters that result recommended.
	 */
	public int getSmelterEstimates() {
		//- Get the ore growing numbers.
		final BaseInformation infoNow = this.getLatestInfo();
		final BaseInformation infoPrev = this.getPreviousInfo();
		if (null == infoPrev)
			return 0;
		else {
			int oreIncrease = infoNow.getFieldNumber("Ore N");
			oreIncrease += infoNow.getFieldNumber("Ore D");
			oreIncrease += infoNow.getFieldNumber("Ore T");
			oreIncrease += infoNow.getFieldNumber("Ore M");
			oreIncrease -= infoPrev.getFieldNumber("Ore N");
			oreIncrease -= infoPrev.getFieldNumber("Ore D");
			oreIncrease -= infoPrev.getFieldNumber("Ore T");
			oreIncrease -= infoPrev.getFieldNumber("Ore M");
			final int currentSmelters = infoNow.getFieldNumber("Smelters");
			if ((oreIncrease > 10) && (currentSmelters < 12))
				return currentSmelters + 1;
			else
				return currentSmelters;
		}
	}

	public int getTotalNatives() {
		//		final BaseInformation bInfo = baseInformation.get(this.getLastTurn());
		final int natives = this.getLatestInfo().getNativesInfo().getTotalNatives();
		return natives;
	}

	/**
	 * This method return true is object turn number matches the <code>AssistantMap.lastAvailableTurn</code>
	 * turn number.
	 */
	public boolean isObsolete(final int mapTurn) {
		if (mapTurn == this.getLatestTurnNumber())
			return false;
		else
			return true;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("[Base:");
		buffer.append("onPlanet=" + onPlanet).append("]");
		buffer.append(super.toString());
		buffer.append("]");
		return buffer.toString();
	}

	/**
	 * Return the previous turn information i�f available. It can be that the user only has loaded a turn and we
	 * can not reference the vector size as a valid counter of the data present on the structure.
	 */
	private BaseInformation getPreviousInfo() {
		//TODO Scan the Vector from the latest turn number downwards until another info is found.
		final int top = this.getLatestTurnNumber();
		int index = top - 1;
		while (index >= 0) {
			final BaseInformation testInfo = baseInformation.get(index);
			if (null != testInfo) return testInfo;
			index--;
		}
		return null;
	}

	/** Locates recursively the latest declared turn information for this model element, whatever it is */
	private BaseInformation getTurnInfo(final int turn) {
		final BaseInformation info = baseInformation.get(turn);
		if (null == info)
			return this.getTurnInfo(turn - 1);
		else
			return info;
	}

	/**
	 * This method return the turn information for the highest turn number that is recorded for this object.
	 * This has not to be the same for every object in the model because some objects may be updated in a new
	 * turn while others not. The relative obsolescence for this information depends on the values of the
	 * <code>lastAvailableTurn</code> that is recorded on the <code>AssistantMap</code>.
	 */
	protected BaseInformation getLatestInfo() {
		return this.getTurnInfo(this.getLatestTurnNumber());
	}
}

// - UNUSED CODE ............................................................................................
