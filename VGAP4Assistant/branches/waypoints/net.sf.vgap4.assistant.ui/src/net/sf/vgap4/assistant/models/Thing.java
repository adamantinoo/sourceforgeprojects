//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

import java.util.Hashtable;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class Thing extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger																logger		= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private AssistantMap																map;
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant. Is the user responsibility to add as many turns as available to have all
	 * the data needed to create graphics or take decisions.
	 */
	private final Hashtable<Integer, AssistantTurnInfo>	turnList	= new Hashtable<Integer, AssistantTurnInfo>();
	/**
	 * Records the type of the data structures that are stored inside the properties. This will allow to manage
	 * more different data models from additional .CSV files without extending the classes the implement the
	 * model. External classes should be aware of this field to be allowed to interpret the read data model.
	 */
	private String																			dataType	= AssistantConstants.MODELTYPE_THING;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Thing(AssistantMap map) {
		this.map = map;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Sets this information as the information for the specified turn. During the setup of this turn data, we
	 * can replace older turn data and this may happen if the user load again the same set of ,CSV files with
	 * the same or updated data. I also process the contents to update the common information that is mostly
	 * accessible and considered cached or statistical informations.<br>
	 * 
	 * @param turn
	 *          the number of the turn that has been processed as the identification got from the CVS file.
	 * @param turnInfo
	 *          Source data structure stored in a generic Properties structure. The real model meaning for that
	 *          data depends on the model type that is another field information. Parsed data that has been
	 *          filtered and mapped to the corresponding fields.
	 */
	public void addTurnInformation(final int turn, final AssistantTurnInfo turnInfo) {
		turnList.put(turn, turnInfo);
		turnInfo.setTurnNumber(turn);
		updateTurnReference(turn);
		logger.info("Adding turn info to model. Turn=" + turn + " - Element Name=" + turnInfo.getName() + " - ID="
				+ turnInfo.getIdNumber());

		// - Load field cached data. This data may change from turn to turn.
		this.setIdNumber(this.getLatestInfo().getIdNumber());
		this.setName(this.getLatestInfo().getName());
		this.setLocation(this.getLatestInfo().getLocation());
		setDataType(turnInfo.dataType);
		logger.info("Updating Object cache. [" + turnInfo.getIdNumber() + "] - " + turnInfo.getName() + ". At location ["
				+ turnInfo.getLocation().x + "-" + turnInfo.getLocation().y + "]");

		//- Do any specific processing.
		processTurnInfo(turnInfo);
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	protected void processTurnInfo(AssistantTurnInfo turnInfo) {
	}

	/**
	 * This method return the turn information for the highest turn number that is recorded for this object.
	 * This has not to be the same for every object in the model because some objects may be updated in a new
	 * turn while others not. The relative obsolescence for this information depends on the values of the
	 * <code>lastAvailableTurn</code> that is recorded on the <code>AssistantMap</code>.
	 */
	protected AssistantTurnInfo getLatestInfo() {
		return getTurnInfo(this.getLatestTurnNumber());
	}

	@Override
	public int getOwner() {
		return this.getLatestInfo().getFieldNumber("Owner");
	}

	@Override
	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	/** Locates recursively the latest declared turn information for this model element, whatever it is */
	private AssistantTurnInfo getTurnInfo(int turn) {
		AssistantTurnInfo info = this.turnList.get(turn);
		if (null == info)
			return getTurnInfo(turn - 1);
		else
			return info;
	}
}

// - UNUSED CODE ............................................................................................
