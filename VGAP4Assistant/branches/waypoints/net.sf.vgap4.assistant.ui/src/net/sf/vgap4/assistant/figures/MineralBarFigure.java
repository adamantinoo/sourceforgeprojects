//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.factories.LetterImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class draws the bar figure that represents the quantities of minerals present on Planets or Bases.
 * This is a generic generator that will represent a box with a series of blocks in 3 different colors that
 * represent the current level of ore and mineral.
 */
public class MineralBarFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger				= Logger.getLogger("net.sf.vgap4.assistant.figures");
	private static final int	BOX_WIDTH			= 4;
	private static final int	BOX_HEIGHT		= 6;
	private static final int	MAX_BOX_COUNT	= 15;

	// - F I E L D - S E C T I O N ............................................................................
	private final Color				colorOre;
	private final Color				colorMineral;
	/** The initial letter to be drawn inside the mineral box. */
	private String						initial				= "U";
	private final Label				initialLabel	= new StandardLabel();
	private final Label				dataLabel			= new StandardLabel();
	private int								maximun				= MineralBarFigure.MAX_BOX_COUNT;
	private int								ore						= 0;
	private int								mineral				= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * Constructor for a new Mineral Bar.
	 * 
	 * @param initial
	 *          letter to be inserted in the mineral icon.
	 * @param colorOre
	 *          the color to represent the ore boxes.
	 * @param colorMineral
	 *          the color to represent the mineral boxes.
	 */
	public MineralBarFigure(final String initial, final Color colorOre, final Color colorMineral) {
		this.colorOre = colorOre;
		this.colorMineral = colorMineral;
		this.initial = initial;

		//- Create the figure layout and position the contained elements.
		this.setupLayout();
		//- Create the mineral symbol label.
		this.addMineralIcon();
		//- Create the mineral bar.
		this.addMineralBar();
		this.add(dataLabel);

		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Sets the number of boxes to be drawn in color depending on the calculated maximum that represents all the
	 * boxes and the current values for the ore and mineral contents.
	 * 
	 * @param maximun
	 * @param ore
	 * @param mineral
	 */
	public void setLevels(final int maximun, final int ore, final int mineral) {
		this.maximun = Math.max(MineralBarFigure.MAX_BOX_COUNT, maximun);

		// - Interpolate values to graphic size.
		this.ore = ore * MineralBarFigure.MAX_BOX_COUNT / this.maximun;
		this.mineral = mineral * MineralBarFigure.MAX_BOX_COUNT / this.maximun;
		dataLabel.setText(ore + "/" + mineral);
		this.repaint();
	}

	private void addMineralBar() {
		this.add(new Figure() {
			@Override
			public Dimension getPreferredSize(final int hint, final int hint2) {
				return new Dimension(MineralBarFigure.BOX_WIDTH * MineralBarFigure.MAX_BOX_COUNT + 2,
						MineralBarFigure.BOX_HEIGHT);
			}

			@Override
			protected void paintFigure(final Graphics graphics) {
				// - Get drawing location. This should be already displaced from the top-left.
				final Point location = this.getLocation();

				// - Draw the small figure boxes
				final Rectangle box = new Rectangle();
				box.width = MineralBarFigure.BOX_WIDTH - 1;
				box.height = MineralBarFigure.BOX_HEIGHT - 1;
				box.y = location.y;
				for (int i = 0; i < MineralBarFigure.MAX_BOX_COUNT; i++) {
					// - Select the color depending on the content of mineral.
					graphics.setBackgroundColor(ColorConstants.lightGray);
					if (i <= (mineral + ore)) {
						graphics.setBackgroundColor(colorMineral);
					} else {
						if (i <= ore) {
							graphics.setBackgroundColor(colorOre);
						}
					}
					box.x = location.x + i * MineralBarFigure.BOX_WIDTH;
					graphics.fillRectangle(box);
				}
			}
		});
	}

	private void addMineralIcon() {
		final LetterImageFactory factory = new LetterImageFactory();
		factory.setColor(colorOre);
		factory.setInitial(initial);
		initialLabel.setIcon(factory.getImage());
		this.add(initialLabel);
	}

	private void setupLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 1;
		grid.marginHeight = 0;
		grid.marginWidth = 1;
		grid.numColumns = 3;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}

	//[01]
}
// - UNUSED CODE ............................................................................................
//[01]
//	public Color getColorOre() {
//		return colorOre;
//	}

//	public void setValues(final int unminned, final int surface) {
//	}
//	@Override
//	protected void paintFigure(final Graphics graphics) {
//		// - Get drawing location. This should be already displaced from the top-left.
//		final Point location = this.getLocation();
//
//		// - Draw the small figure boxes
//		final Rectangle box = new Rectangle();
//		box.width = BOX_WIDTH - 1;
//		box.height = BOX_HEIGHT - 1;
//		box.y = location.y;
//		for (int i = 0; i < MineralBarFigure.MAX_BOX_COUNT; i++) {
//			// - Select the color depending on the content of mineral.
//			graphics.setBackgroundColor(ColorConstants.lightGray);
//			if (i <= (mineral + ore)) {
//				graphics.setBackgroundColor(colorMineral);
//			} else {
//				if (i <= ore) {
//					graphics.setBackgroundColor(colorOre);
//				}
//			}
//			box.x = location.x + i * MineralBarFigure.BOX_WIDTH;
//			graphics.fillRectangle(box);
//		}
//	}
