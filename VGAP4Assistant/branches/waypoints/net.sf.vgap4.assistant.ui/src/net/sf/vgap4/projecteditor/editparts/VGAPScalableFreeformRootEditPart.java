//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.FreeformLayeredPane;
import org.eclipse.draw2d.LayeredPane;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAPScalableFreeformRootEditPart extends ScalableFreeformRootEditPart {
	private static Logger				logger					= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	SECTOR_LAYER		= "SECTOR_LAYER";
	public static final String	WAYPOINT_LAYER	= "WAYPOINT_LAYER";
	public static final String	PLANET_LAYER		= "PLANET_LAYER";
	public static final String	SHIP_LAYER			= "SHIP_LAYER";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public VGAPScalableFreeformRootEditPart() {
	// }
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	protected LayeredPane createPrintableLayers() {
		FreeformLayeredPane layeredPane = new FreeformLayeredPane();
		layeredPane.add(new FreeformLayer(), PRIMARY_LAYER);
		layeredPane.add(new FreeformLayer(), SECTOR_LAYER);
		layeredPane.add(new FreeformLayer(), WAYPOINT_LAYER);
		layeredPane.add(new FreeformLayer(), PLANET_LAYER);
		layeredPane.add(new FreeformLayer(), SHIP_LAYER);
		layeredPane.add(new ConnectionLayer(), CONNECTION_LAYER);
		return layeredPane;
	}
	// - I N T E R F A C E - N A M E
}

// - UNUSED CODE ............................................................................................
