//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import es.ftgroup.gef.parts.AbstractContainerEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantContainerEditPart extends AbstractContainerEditPart {
	private static final String	PROP_ADD_CHILD		= "PROP_ADD_CHILD";
	private static final String	PROP_REMOVE_CHILD	= "PROP_REMOVE_CHILD";

	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public AssistantContainerEditPart() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (PROP_ADD_CHILD.equals(prop)) {
			this.refreshVisuals();
			return;
		}
		if (PROP_REMOVE_CHILD.equals(prop)) {
			this.refreshVisuals();
			return;
		}
	}
	//
	//	protected IContainerModel getCastedModel() {
	//		return (IContainerModel) this.getModel();
	//	}
}

// - UNUSED CODE ............................................................................................
