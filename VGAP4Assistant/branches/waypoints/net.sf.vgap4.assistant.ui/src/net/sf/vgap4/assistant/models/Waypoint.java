//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractNode;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class Waypoint extends AbstractNode implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String	WAYPOINT_FUTUREWAYPOINT		= "WAYPOINT_FUTUREWAYPOINT";
	public static final String	WAYPOINT_CURRENTWAYPOINT	= "WAYPOINT_CURRENTWAYPOINT";
	public static final String	WAYPOINT_PASTWAYPOINT			= "WAYPOINT_PASTWAYPOINT";
	private static Logger				logger										= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long		serialVersionUID					= 1442878400889965313L;

	// - F I E L D - S E C T I O N ............................................................................
	private final int						turn;
	private final int						order;
	private final Point					location;
	private String							type;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public Waypoint(String waypointType, int x, int y) {
	//		this.type = waypointType;
	//		this.location = new Point(x, y);
	//		logger.info("Setting location of waypoint type [" + type + "] to: " + x + " - " + y);
	//	}

	public Waypoint(final int turn, final int order, final String waypointType, final int coordX, final int coordY) {
		this.turn = turn;
		this.order = order;
		type = waypointType;
		location = new Point(coordX, coordY);
		Waypoint.logger.info("Setting location of waypoint type [" + type + "] to: " + coordX + " - " + coordY);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public boolean checkLocation(final Point location) {
		if ((this.location.x == location.x) && (this.location.y == location.y))
			return true;
		else
			return false;
	}

	public Point getLocation() {
		return location.getCopy();
	}

	public int getOrder() {
		return order;
	}

	public int getTurn() {
		return turn;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "[Waypoint: turn=" + turn + "-order=" + order + "-type=" + type + "-location=" + location.x + "-"
				+ location.y + "]";
	}
}
// - UNUSED CODE ............................................................................................
