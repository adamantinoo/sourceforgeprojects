//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.test;

// - IMPORT SECTION .........................................................................................
import junit.framework.TestCase;
import net.sf.vgap4.assistant.models.Waypoint;
import net.sf.vgap4.assistant.models.helpers.WaypointList;

// - CLASS IMPLEMENTATION ...................................................................................
public class WaypointListTest extends TestCase {
	private static String	VALIDATE_Initial;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:1-order:0-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("turn:1-order:1-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("turn:1-order:2-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_Initial = buffer.toString();
	}
	private static String	VALIDATE_TurnAddition;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:2-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:3-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:3-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:3-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:4-order:0-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("turn:4-order:1-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("turn:4-order:2-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_TurnAddition = buffer.toString();
	}
	private static String	VALIDATE_NewCurrent;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:5-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:5-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:6-order:0-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_NewCurrent = buffer.toString();
	}
	private static String	VALIDATE_SeparatedTurns;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:1-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:1-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:1-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:4-order:0-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("turn:4-order:1-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("turn:4-order:2-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_SeparatedTurns = buffer.toString();
	}
	private static String	VALIDATE_InvertedTurns;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:1-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:1-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:1-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:4-order:0-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("turn:4-order:1-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("turn:4-order:2-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_InvertedTurns = buffer.toString();
	}
	private static String	VALIDATE_MixedInsertion;
	static {
		StringBuffer buffer = new StringBuffer("[WaypointList");
		buffer.append("turn:2-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:3-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:4-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:2-order:5-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');

		buffer.append("turn:4-order:0-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:4-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:4-order:2-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');

		buffer.append("turn:5-order:1-type:").append(Waypoint.WAYPOINT_PASTWAYPOINT).append('\n');
		buffer.append("turn:5-order:2-type:").append(Waypoint.WAYPOINT_CURRENTWAYPOINT).append('\n');
		buffer.append("turn:6-order:1-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("turn:6-order:2-type:").append(Waypoint.WAYPOINT_FUTUREWAYPOINT).append('\n');
		buffer.append("]");
		VALIDATE_MixedInsertion = buffer.toString();
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.test");

	// - F I E L D - S E C T I O N ............................................................................
	private WaypointList	wlInitial					= new WaypointList();
	private WaypointList	wlTurnAddition		= new WaypointList();
	private WaypointList	wlNewCurrent			= new WaypointList();
	private WaypointList	wlSeparatedTurns	= new WaypointList();
	private WaypointList	wlInvertedTurns		= new WaypointList();
	private WaypointList	wlMixedInsertion	= new WaypointList();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WaypointListTest() {
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		//- Initialize different waypoint lists with test configurations.
		wlTurnAddition.clear();
		wlTurnAddition.add(new Waypoint(2, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 2000, 1000));
		wlTurnAddition.add(new Waypoint(2, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2100, 1100));
		wlTurnAddition.add(new Waypoint(2, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2200, 1200));
		wlTurnAddition.add(new Waypoint(3, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 3000, 1000));
		wlTurnAddition.add(new Waypoint(3, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 3100, 1100));
		wlTurnAddition.add(new Waypoint(3, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 3200, 1200));

	}

	public void testAddFirstTurn() {
		wlInitial.clear();
		wlInitial.add(new Waypoint(1, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 1000, 1000));
		wlInitial.add(new Waypoint(1, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1100, 1100));
		wlInitial.add(new Waypoint(1, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1200, 1200));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlInitial.dump(), VALIDATE_Initial);
	}

	public void testAddNewLatestTurn() {
		wlTurnAddition.add(new Waypoint(4, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 4000, 1000));
		wlTurnAddition.add(new Waypoint(4, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4100, 1100));
		wlTurnAddition.add(new Waypoint(4, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4200, 1200));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlTurnAddition.dump(), VALIDATE_TurnAddition);
	}

	public void testAddNewCurrent() {
		//- Initialize different waypoint lists with test configurations.
		wlNewCurrent.clear();
		wlNewCurrent.add(new Waypoint(5, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 5000, 1000));
		wlNewCurrent.add(new Waypoint(5, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5100, 1100));

		wlNewCurrent.add(new Waypoint(6, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 6000, 1000));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlNewCurrent.dump(), VALIDATE_NewCurrent);
	}

	public void testAddSeparatedTurns() {
		wlSeparatedTurns.add(new Waypoint(1, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 1000, 1000));
		wlSeparatedTurns.add(new Waypoint(1, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1100, 1100));
		wlSeparatedTurns.add(new Waypoint(1, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1200, 1200));
		wlSeparatedTurns.add(new Waypoint(4, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 4000, 1000));
		wlSeparatedTurns.add(new Waypoint(4, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4100, 1100));
		wlSeparatedTurns.add(new Waypoint(4, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4200, 1200));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlSeparatedTurns.dump(), VALIDATE_SeparatedTurns);
	}

	public void testAddInvertedTurns() {
		wlInvertedTurns.add(new Waypoint(4, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 4000, 1000));
		wlInvertedTurns.add(new Waypoint(4, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4100, 1100));
		wlInvertedTurns.add(new Waypoint(4, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4200, 1200));
		wlInvertedTurns.add(new Waypoint(1, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 1000, 1000));
		wlInvertedTurns.add(new Waypoint(1, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1100, 1100));
		wlInvertedTurns.add(new Waypoint(1, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 1200, 1200));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlInvertedTurns.dump(), VALIDATE_InvertedTurns);
	}

	public void testMixedInsertion() {
		wlMixedInsertion.add(new Waypoint(5, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5100, 1000));
		wlMixedInsertion.add(new Waypoint(5, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 5000, 1000));
		wlMixedInsertion.add(new Waypoint(5, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5000, 1000));
		wlMixedInsertion.add(new Waypoint(6, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 5000, 1000));
		wlMixedInsertion.add(new Waypoint(6, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5100, 1000));
		wlMixedInsertion.add(new Waypoint(6, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5000, 1000));

		wlMixedInsertion.add(new Waypoint(2, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2100, 1000));
		wlMixedInsertion.add(new Waypoint(2, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2200, 1000));
		wlMixedInsertion.add(new Waypoint(2, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 2000, 1000));
		wlMixedInsertion.add(new Waypoint(2, 3, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2300, 1000));
		wlMixedInsertion.add(new Waypoint(2, 4, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2400, 1000));
		wlMixedInsertion.add(new Waypoint(2, 5, Waypoint.WAYPOINT_FUTUREWAYPOINT, 2000, 1000));

		wlMixedInsertion.add(new Waypoint(4, 2, Waypoint.WAYPOINT_FUTUREWAYPOINT, 5000, 1000));
		wlMixedInsertion.add(new Waypoint(4, 1, Waypoint.WAYPOINT_FUTUREWAYPOINT, 4100, 1000));
		wlMixedInsertion.add(new Waypoint(4, 1, Waypoint.WAYPOINT_PASTWAYPOINT, 4100, 1000));
		wlMixedInsertion.add(new Waypoint(4, 0, Waypoint.WAYPOINT_CURRENTWAYPOINT, 4000, 1000));

		//TODO Validate the resulting list with what is expected.
		assertEquals(wlMixedInsertion.dump(), VALIDATE_MixedInsertion);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
