//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.test;

// - IMPORT SECTION .........................................................................................
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vgap4.assistant.models.AssistantMap;

// - CLASS IMPLEMENTATION ...................................................................................
public class LoadTurnTest extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger		= Logger.getLogger("net.sf.vgap4.assistant.loadturn");

	// - F I E L D - S E C T I O N ............................................................................
	String								step0file	= "U:/ldiego/Workstage/EclipseRuntimes/RCP Applications/graphicaleditorview.116.0.vgap4";
	String								step1file	= "U:/ldiego/Workstage/EclipseRuntimes/RCP Applications/graphicaleditorview.116.1.vgap4";
	String								step2file	= "U:/ldiego/Workstage/EclipseRuntimes/RCP Applications/graphicaleditorview.116.2.vgap4";

	private AssistantMap	diagram		= new AssistantMap();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public LoadTurnTest() {
	//	}
	//
	//	public LoadTurnTest(String name) {
	//		super(name);
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	protected void setUp() throws Exception {
	//		super.setUp();
	//	}

	public void testLoadBaseData0() throws Exception {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(step0file));
			diagram = (AssistantMap) in.readObject();
			in.close();
			System.out.print(diagram.dump());
		} catch (EOFException eof) {
			// TODO Auto-generated catch block
			eof.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			// TODO Auto-generated catch block
			cnfe.printStackTrace();
		}

	}

	public void testLoadBaseData1() throws Exception {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(step1file));
			diagram = (AssistantMap) in.readObject();
			in.close();
			System.out.print(diagram.dump());
		} catch (EOFException eof) {
			// TODO Auto-generated catch block
			eof.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			// TODO Auto-generated catch block
			cnfe.printStackTrace();
		}

	}

	public void testLoadBaseData2() throws Exception {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(step2file));
			diagram = (AssistantMap) in.readObject();
			in.close();
			System.out.print(diagram.dump());
		} catch (EOFException eof) {
			// TODO Auto-generated catch block
			eof.printStackTrace();
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			// TODO Auto-generated catch block
			cnfe.printStackTrace();
		}

	}
	//
	//	public void testLoadPlanetData() {
	//
	//	}
}

// - UNUSED CODE ............................................................................................
