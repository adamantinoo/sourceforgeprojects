//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.dnd.TemplateTransferDropTargetListener;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.requests.CreationFactory;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.PartInitException;

import net.sf.vgap4.projecteditor.commands.ImportTurnDataAction;
import net.sf.vgap4.projecteditor.editparts.VGAP4ProjectEditPartFactory;
import net.sf.vgap4.projecteditor.editparts.VGAPScalableFreeformRootEditPart;
import net.sf.vgap4.projecteditor.figures.VGAP4ProjectFigureFactory;
import net.sf.vgap4.projecteditor.model.VGAP4Map;

// - CLASS IMPLEMENTATION ...................................................................................
public class MainMapPage extends AbstractVGAP4Page {
	private static Logger		logger	= Logger.getLogger("net.sf.vgap4.assistant.editor");
	// - G L O B A L - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	// - G L O B A L - F I E L D S
	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the <code>MultiPageEditor</code> that is the root element for the map presentation. This
	 * reference allows access to the model and other Editor structures.
	 */
	private VGAP4MapViever	editor;

	// /** The selection listener for this editor instance. Every editor has its own listener. */
	// protected final ISelectionListener selectionListener = new VGAP4SelectionListener(this);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MainMapPage(VGAP4MapViever mapViewer) {
		this.editor = mapViewer;
		setEditDomain(new DefaultEditDomain(this));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Delegate this operations to the parent editor that is a MultiPageEditor and that keeps all i/o
	 * references.
	 */
	@Override
	public final void doSave(IProgressMonitor monitor) {
		editor.doSave(monitor);
	}

	/**
	 * Delegate this operations to the parent editor that is a MultiPageEditor and that keeps all i/o
	 * references.
	 */
	@Override
	public final void doSaveAs() {
		editor.doSaveAs();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
	}

	/**
	 * Delegate this operations to the parent editor that is a MultiPageEditor and that keeps all i/o
	 * references.
	 */
	@Override
	public final boolean isDirty() {
		return editor.isDirty();
	}

	/**
	 * Delegate this operations to the parent editor that is a MultiPageEditor and that keeps all i/o
	 * references.
	 */
	@Override
	public final boolean isSaveAsAllowed() {
		return editor.isSaveAsAllowed();
	}

	/**
	 * Returns the selection listener. The listener corresponds to the listener declared at the parent editor.
	 * 
	 * @return the <code>ISelectionListener</code> defined at the parent.
	 */
	protected ISelectionListener getSelectionListener() {
		return editor.getSelectionListener();
	}

	/**
	 * Create a transfer drop target listener. When using a CombinedTemplateCreationEntry tool in the palette,
	 * this will enable model element creation by dragging from the palette.
	 * 
	 * @see #createPaletteViewerProvider()
	 */
	private TransferDropTargetListener createTransferDropTargetListener() {
		return new TemplateTransferDropTargetListener(getGraphicalViewer()) {
			@Override
			protected CreationFactory getFactory(Object template) {
				return new SimpleFactory((Class) template);
			}
		};
	}

	@Override
	protected PaletteRoot getPaletteRoot() {
		return this.editor.getPaletteRoot();
	}

	public VGAP4Map getMapModel() {
		return this.editor.getModel();
	}

	/**
	 * Set up the editor's initial content (after creation).
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#initializeGraphicalViewer()
	 */
	@Override
	protected void initializeGraphicalViewer() {
		super.initializeGraphicalViewer();
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(getMapModel()); // set the contents of this editor

		// listen for dropped parts
		viewer.addDropTargetListener(createTransferDropTargetListener());
		getCommandStack().markSaveLocation();
	}

	/**
	 * Configure the graphical viewer before it receives contents.
	 * <p>
	 * This is the place to choose an appropriate RootEditPart and EditPartFactory for your editor. The
	 * RootEditPart determines the behavior of the editor's "work-area". For example, GEF includes zoomable and
	 * scrollable root edit parts. The EditPartFactory maps model elements to edit parts (controllers).
	 * </p>
	 * 
	 * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	 */
	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setEditPartFactory(new VGAP4ProjectEditPartFactory(new VGAP4ProjectFigureFactory()));
		viewer.setRootEditPart(new VGAPScalableFreeformRootEditPart());
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		// configure the context menu provider
		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		viewer.setContextMenu(cmProvider);
		getSite().registerContextMenu(cmProvider, viewer);
	}

	public String getPageName() {
		return "MAIN MAP";
	}

	// protected void createPageControl(Composite parent)
	// {
	// Composite composite = new Composite(parent, SWT.NONE);
	// composite.setBackground(parent.getBackground());
	// composite.setLayout(new GridLayout(2, false));
	//      
	// createPaletteViewer(composite);
	// GridData gd = new GridData(GridData.FILL_VERTICAL);
	// gd.widthHint = 125;
	// getPaletteViewer().getControl().setLayoutData(gd);
	//      
	// createGraphicalViewer(composite);
	// gd = new GridData(GridData.FILL_BOTH);
	// gd.widthHint = 275;
	// getGraphicalViewer().getControl().setLayoutData(gd);
	// }
	// /**
	// * Creates the GraphicalViewer on the specified <code>Composite</code>.
	// * @param parent the parent composite
	// */
	// private void createGraphicalViewer(Composite parent)
	// {
	// viewer = new ScrollingGraphicalViewer();
	// viewer.createControl(parent);
	//
	// // configure the viewer
	// viewer.getControl().setBackground(parent.getBackground());
	// viewer.setRootEditPart(new ScalableFreeformRootEditPart());
	// viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));
	//
	// // hook the viewer into the editor
	// registerEditPartViewer(viewer);
	//
	// // configure the viewer with drag and drop
	// configureEditPartViewer(viewer);
	//
	// // initialize the viewer with input
	// viewer.setEditPartFactory(new GraphicalEditPartsFactory());
	// viewer.setContents(getWorkflow());
	// }
	@Override
	public String getTitle() {
		return "Present and Display the complete known Map";
	}

	public void setDirty(boolean dirty) {
		editor.setDirty(dirty);
	}
	// public final void createPartControl(Composite parent)
	// {
	// Composite composite = new Composite(parent, SWT.NONE);
	// GridLayout layout = new GridLayout();
	// layout.marginHeight = 10;
	// layout.marginWidth = 10;
	// layout.verticalSpacing = 5;
	// layout.horizontalSpacing = 5;
	// layout.numColumns = 1;
	// composite.setLayout(layout);
	// composite.setBackground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
	// composite.setForeground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
	//
	// // label on top
	// Label label =
	// new Label(composite, SWT.HORIZONTAL | SWT.SHADOW_OUT | SWT.LEFT);
	// label.setText(getTitle());
	// label.setFont(
	// JFaceResources.getFontRegistry().get(JFaceResources.HEADER_FONT));
	// label.setBackground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
	// label.setForeground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
	// label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	//
	// // now the main editor page
	// composite = new Composite(composite, SWT.NONE);
	// composite.setLayout(new FillLayout());
	// composite.setBackground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
	// composite.setForeground(
	// parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
	// composite.setLayoutData(new GridData(GridData.FILL_BOTH));
	// createPageControl(composite);
	// }
}
// - UNUSED CODE ............................................................................................
