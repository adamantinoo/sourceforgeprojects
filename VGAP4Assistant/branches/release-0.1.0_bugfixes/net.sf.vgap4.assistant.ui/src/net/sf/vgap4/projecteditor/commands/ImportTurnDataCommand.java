//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.commands;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.wizard.ProgressMonitorPart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

import net.sf.vgap4.assistant.editor.MainMapPage;
import net.sf.vgap4.assistant.models.BaseInformation;
import net.sf.vgap4.assistant.models.PlanetInformation;
import net.sf.vgap4.projecteditor.model.ShipInformation;
import net.sf.vgap4.projecteditor.model.VGAP4Map;

//- CLASS IMPLEMENTATION ...................................................................................
public class ImportTurnDataCommand extends Command {
	private static Logger					logger									= Logger.getLogger("net.sf.vgap4.projecteditor.commands");

	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private final static String[]	OPEN_FILTER_NAMES				= new String[] { "CSVInfo.txt (Turn descripcion information)" };
	private final static String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.txt" };

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private final MainMapPage			editor;
	private String								projectName;
	private String								projectPath;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ImportTurnDataCommand(String label, MainMapPage editor) {
		super(label);
		this.editor = editor;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void execute() {
		// - Open a file selector to select the file to import into the model.
		String dataFileName = this.openDialog(editor.getSite().getWorkbenchWindow());
		if (null == dataFileName) {
			//- The user selected the cancel. Do not open the file and abort the command.
			return;
		} else {
			VGAP4Map diagram = editor.getMapModel();
			//			try {
			//TODO Open a new operation to load the data
			final WorkspaceModifyOperation operation = new ReadTurnWorkspaceModifyOperation(dataFileName, projectPath,
					diagram);
			Display.getCurrent().asyncExec(new Runnable() {
				public void run() {
					try {
						operation.run(new ProgressMonitorPart(editor.getSite().getWorkbenchWindow().getShell(), null));
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			editor.setDirty(true);
			//			// - Turn information is spread into several files. Create an instance to handle those
			//			TurnReader turnData = new TurnReader(dataFileName, diagram);
			//			if (turnData.readTurnDescription()) {
			//				turnData.readPlanetData(projectPath);
			//				turnData.readBaseData(projectPath);
			//				turnData.readShipData(projectPath);
			//
			//				// - Signal the diagram that there have been some model changes
			//				diagram.fireStructureChange(VGAP4Map.DATA_ADDED_PROP, null, null);
			//			}
			//		} catch (FileNotFoundException fnfe) {
			//			// - The command has failed for some cause, but the command is not deactivated.
			//			System.err.println("File <" + dataFileName + "> cannot be found.");
			//			fnfe.printStackTrace();
			//		} catch (IOException ioe) {
			//			// TODO Notify the user the error during the read on the file contents.
			//			ioe.printStackTrace();
			//			} catch (InvocationTargetException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			} catch (InterruptedException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}
		}
	}

	private String openDialog(IWorkbenchWindow window) {
		// - Get the user to choose an scenery file.
		final FileDialog fileChooser = new FileDialog(window.getShell(), SWT.OPEN);
		// FIXME Configure the text of the dialog to be presented to the user.
		fileChooser.setText("Select the player's turn CSVInfo.txt turn info file.");
		fileChooser.setFilterPath(null);
		fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(OPEN_FILTER_NAMES);
		this.projectName = fileChooser.open();
		this.projectPath = fileChooser.getFilterPath();
		return this.projectName;
	}

	@Override
	public boolean canExecute() {
		// - This command can be executed anytime.
		return true;
	}

	@Override
	public boolean canUndo() {
		// - We do not store the previous model, so the command can not be undo
		return false;
	}
}

class ReadTurnWorkspaceModifyOperation extends WorkspaceModifyOperation {

	private String		dataFileName;
	private String		projectPath;
	private VGAP4Map	mainmap;

	public ReadTurnWorkspaceModifyOperation(String dataFileName, String projectPath, VGAP4Map diagram) {
		this.dataFileName = dataFileName;
		this.projectPath = projectPath;
		this.mainmap = diagram;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException, InvocationTargetException,
			InterruptedException {
		// - Turn information is spread into several files. Create an instance to handle those
		try {
			monitor.beginTask("reading turn data", 40000);
			TurnReader turnData = new TurnReader(dataFileName, mainmap);
			monitor.subTask("Reading turn definition");
			boolean valid = turnData.readTurnDescription();
			monitor.worked(10000);
			if (valid) {
				turnData.readPlanetData(projectPath, monitor);
				turnData.readBaseData(projectPath, monitor);
				monitor.worked(10000);
				turnData.readShipData(projectPath, monitor);
				monitor.worked(10000);

				// - Signal the diagram that there have been some model changes
				mainmap.fireStructureChange(VGAP4Map.DATA_ADDED_PROP, null, null);
				monitor.done();
			}
		} catch (FileNotFoundException fnfe) {
			// - The command has failed for some cause, but the command is not deactivated.
			System.err.println("File <" + dataFileName + "> cannot be found.");
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			// TODO Notify the user the error during the read on the file contents.
			ioe.printStackTrace();
		}
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
class TurnReader {
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private String		descriptionFileName;
	private VGAP4Map	diagram;

	private String		gameName;
	private String		gameId;
	private int				turn;
	private int				player;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TurnReader(String dataFileName, VGAP4Map diagram) {
		this.descriptionFileName = dataFileName;
		this.diagram = diagram;

		//TODO Open a progress indicator while reading the turn data.

	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * CSVInfo.txt file contains two source lines and four data fields. This method reads that file and
	 * processes its contents to extract those elements.<br>
	 * Also validates that this information matches the same fields on the diagram or that the diagram is not
	 * initialized, when then this method will load the fields with the read information.
	 * 
	 * @throws FileNotFoundException
	 */
	public boolean readTurnDescription() throws FileNotFoundException, IOException {
		// - Open the file for reading.
		BufferedReader reader = new BufferedReader(new FileReader(this.descriptionFileName));

		// - Read the two lines
		String line1 = reader.readLine();
		String line2 = reader.readLine();

		// - Line 1 only contains the game name. Get it directly.
		this.gameName = line1.trim();

		// - Line 2 is complex and there is no separator between fields. Scan it with a parser.
		// try {
		Pattern pat = Pattern.compile("Game ID:(.+)Turn:(.+)Player:(.+)");
		Matcher match = pat.matcher(line2);
		if (match.matches()) {
			this.gameId = match.group(1).trim();
			this.turn = Integer.parseInt(match.group(2).trim());
			this.player = Integer.parseInt(match.group(3).trim());

			// - Check that this diagram contains data for the game and player.
			//- If diagram is not initialized then set those fields.
			if (this.diagram.isInitialized()) {
				// - Check that this information belongs to the right game, player and turn
				//TODO If the turn does not match the already stored identification throw an exception
				if ((this.gameId.equals(this.diagram.getGameId())) && (this.player == this.diagram.getPlayer()))
					return true;
				else
					return false;
			} else {
				// - Set the game identification into the diagram.
				this.diagram.setGameId(this.gameId);
				this.diagram.setPlayer(this.player);
				this.diagram.setGameName(this.gameName);
				this.diagram.setReadTurn(this.turn);
				return true;
			}
		} else
			return false;
	}

	/**
	 * Open the Planet.csv file and read and process their contents to a set of <code>PlanetInformation</code>
	 * instances.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 * @throws FileNotFoundException
	 *           if the Planet.csv file is not found.
	 */
	public void readPlanetData(String projectPath, IProgressMonitor monitor) throws FileNotFoundException {
		BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Planet.csv"));
		// - Read the data into the model.
		try {
			int lines = countLines(reader);
			int units = 10000 / lines;
			// - The first line of the file contains the names on the data fields. We use this line to check that
			// this file has the right format before trying to read.
			if (checkPlanetFormat(reader.readLine())) {
				// - Read line by line the planet data information. Split the line into the data fields.
				String line = reader.readLine();
				monitor.worked(units);
				do {
					// - Split the line into their components
					String[] planetFields = line.split(",");
					// - Process the data creating a new Planet instance.
					PlanetInformation planetInfo = new PlanetInformation();
					planetInfo.loadData(planetFields);

					// - Add the new instance to the model.
					diagram.addPlanetInformation(this.turn, planetInfo);

					line = reader.readLine();
					monitor.worked(units);
				} while (line != null);
			}
			reader.close();
		} catch (IOException exc) {
			// - Some input error. It must be notified to the user so register a TODO
			// TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException aioobs) {
			// - The initialization of some array has failed.
			aioobs.printStackTrace();
		}

	}

	private int countLines(BufferedReader reader) throws IOException {
		return 307;
		//		reader.mark(0);
		//		int lines = 0;
		//		String line = reader.readLine();
		//		while (null != line) {
		//			line = reader.readLine();
		//			lines++;
		//		}
		//		reader.reset();
		//		return lines;
	}

	/**
	 * Open the Base.csv and read and process the base data. Base information is split upon many .csv files. I
	 * should read lla that are affected by the data model.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 * @throws FileNotFoundException
	 *           if the Base.csv file is not found.
	 */
	public void readBaseData(String projectPath, IProgressMonitor monitor) throws FileNotFoundException {
		BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Base.csv"));
		// - Read the data into the model.
		try {
			// - The first line of the file contains the names on the data fields. We use this line to check that
			// this file has the right format before trying to read.
			if (checkBaseFormat(reader.readLine())) {
				// - Read line by line the planet data information. Split the line into the data fields.
				String line = reader.readLine();
				do {
					// - Split the line into their components
					String[] baseFields = line.split(",");
					// - Process the data creating a new Planet instance.
					BaseInformation baseInfo = new BaseInformation();
					baseInfo.loadData(baseFields);

					// - Add the new instance to the model.
					diagram.addBaseInformation(this.turn, baseInfo);

					line = reader.readLine();
				} while (line != null);
			}
			reader.close();
		} catch (IOException exc) {
			// - Some input error. It must be notified to the user so register a TODO
			// TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	/**
	 * Open the Ship.csv and read and process the ship data.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 * @throws FileNotFoundException
	 *           if the Ship.csv file is not found.
	 */
	public void readShipData(String projectPath, IProgressMonitor monitor) throws FileNotFoundException {
		BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Ship.csv"));
		// - Read the data into the model.
		try {
			// - The first line of the file contains the names on the data fields. We use this line to check that
			// this file has the right format before trying to read.
			if (checkShipFormat(reader.readLine())) {
				// - Read line by line the planet data information. Split the line into the data fields.
				String line = reader.readLine();
				do {
					// - Split the line into their components
					String[] shipFields = line.split(",");
					// - Process the data creating a new Planet instance.
					ShipInformation shipInfo = new ShipInformation();
					shipInfo.loadData(shipFields);

					// - Add the new instance to the model.
					diagram.addShipInformation(this.turn, shipInfo);

					line = reader.readLine();
				} while (line != null);
			}
			reader.close();
		} catch (IOException exc) {
			// - Some input error. It must be notified to the user so register a TODO
			// TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	// - P R O T E C T E D - S E C T I O N
	// TODO This method has no implementation. I have to find a procedure to verify that we are reading a proper
	// formated file
	private boolean checkPlanetFormat(String line) {
		String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<PlanetIndex>"))
			return true;
		else
			return false;
	}

	private boolean checkShipFormat(String line) {
		String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<ShipIndex>"))
			return true;
		else
			return false;
	}

	private boolean checkBaseFormat(String line) {
		String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<BaseIndex>"))
			return true;
		else
			return false;
	}
}
// - UNUSED CODE ............................................................................................
