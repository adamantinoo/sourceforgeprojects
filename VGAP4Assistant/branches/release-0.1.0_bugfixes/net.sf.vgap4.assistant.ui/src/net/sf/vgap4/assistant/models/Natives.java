//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class contains the quantities of natives that are found on the planet's surface. The native race is
 * stored as an index on a fixed size array being the array locations assigned to the next races: 0 -
 * humanoids ... continue with all races...
 */
public class Natives extends FieldTransformer implements Serializable {
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final long	serialVersionUID	= -4235257829153780299L;
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private final int[]				nativeCount				= new int[10];
	private boolean						hasNatives				= false;
	/** Total number of natives available at the base at this turn. */
	private int								totalnatives			= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Natives(String[] planetFields) {
		// - Check to see the type of natives and the native count. Put this on the native array.
		for (int counter = 0; counter < 10; counter++) {
			nativeCount[counter] = convert2Integer(planetFields[10 + counter]);
			if (nativeCount[counter] > 0) {
				//- Accumulate the number of natives available.
				this.totalnatives += nativeCount[counter];
				hasNatives = true;
			}
		}
	}

	public Natives(String[] planetFields, int startDataIndex) {
		// - Check to see the type of natives and the native count. Put this on the native array.
		for (int counter = 0; counter < 10; counter++) {
			nativeCount[counter] = convert2Integer(planetFields[startDataIndex + counter]);
			if (nativeCount[counter] > 0) hasNatives = true;
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int[] getNatives() {
		return this.nativeCount;
	}

	public boolean isHasNatives() {
		return hasNatives;
	}

	public int getTotalNatives() {
		return this.totalnatives;
	}
}

// - UNUSED CODE ............................................................................................
