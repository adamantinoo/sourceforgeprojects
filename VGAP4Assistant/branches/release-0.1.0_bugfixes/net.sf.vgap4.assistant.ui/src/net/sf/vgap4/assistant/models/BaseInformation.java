//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseInformation implements Serializable {
	private static final long		serialVersionUID	= 8806434019146603117L;
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME			= "CONSTANT_VALUE";

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S

	// - M O D E L F I E L D S
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	private int									idNumber					= -1;
	/** Unique identifier of the Planet where this base is build. */
	private int									planetId					= -1;
	/**
	 * Stores the name given to this ship. This name is user configurable and may change from turn to turn so
	 * this information shoud not be cached.
	 */
	private String							name;
	/** Stores the coordinates to the ship location inside the diagram coordinates. */
	private Point								location;
	/**
	 * Stores the numbers of all the natives that are found residing on the base. This numbers are diffrent form
	 * the Natives that are still found on the Planet.
	 */
	private Natives							nativeInformation	= null;
	/** Structure with all the mining information available on this planet's turn data. */
	private MiningInformation		miningInfo;
	private int									food;
	private int									medicalUnits;
	private int									repairUnits;
	private int									supplies;
	private int									smelters;
	private int									crew;
	private int									troop;
	private int									highGuard;
	private int									colonists;
	private int									cash;
	private int									factories;
	private int									mines;
	private int									ordnance;
	private int									city;
	private int									terraformer;
	private int									farm;
	private int									undercity;

	//	/** This is the user editable attribute to store any user notes that keep associated to the Base.*/
	//	private String notes;
	// - F I E L D S

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseInformation() {
	}

	public int getIdNumber() {
		return idNumber;
	}

	public String getName() {
		return name;
	}

	public Natives getNativesInfo() {
		return this.nativeInformation;
	}

	/**
	 * The location of the base matches the location of the planet where this base is located. This location may
	 * change from turn to turn because some races have the ability to move bases to other locations.
	 * 
	 * @return the Point that represent the map location of the homing planet.
	 */
	public Point getLocation() {
		return location;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Loads the contents on the source fields into the corresponding attributes of this instance, processing
	 * them as required to convert string to number if necessary.
	 */
	public void loadData(String[] planetFields) {
		try {
			// - Store the data fields in the instance attributes.
			this.idNumber = convert2Integer(planetFields[3].trim());
			this.planetId = convert2Integer(planetFields[4].trim());
			name = planetFields[2].trim();
			nativeInformation = new Natives(planetFields, 6);
			miningInfo = new MiningInformation(planetFields, 17);
			food = convert2Integer(planetFields[25].trim());
			medicalUnits = convert2Integer(planetFields[26].trim());
			repairUnits = convert2Integer(planetFields[39].trim());
			supplies = convert2Integer(planetFields[40].trim());

			smelters = convert2Integer(planetFields[47].trim());
			crew = convert2Integer(planetFields[48].trim());
			troop = convert2Integer(planetFields[49].trim());
			highGuard = convert2Integer(planetFields[50].trim());
			setColonists(convert2Integer(planetFields[51].trim()));
			cash = convert2Integer(planetFields[52].trim());
			factories = convert2Integer(planetFields[53].trim());
			mines = convert2Integer(planetFields[54].trim());
			ordnance = convert2Integer(planetFields[60].trim());

			city = convert2Integer(planetFields[181].trim());
			terraformer = convert2Integer(planetFields[182].trim());
			farm = convert2Integer(planetFields[185].trim());
			undercity = convert2Integer(planetFields[290].trim());
			//FIXME get the fields for the base coordinates
			int x = convert2Integer(planetFields[407].trim());
			int y = convert2Integer(planetFields[408].trim());
			this.location = new Point(x, 3000 - y);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	protected int convert2Integer(String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getColonists() {
		return colonists;
	}

	public int getPlanetId() {
		return this.planetId;
	}

	/**
	 * Get the mining information for all the minerals at once. This simplifies the interface because all this
	 * information is transferred with a single command. Destination will then understand the structure and
	 * extract the right information from the structure.
	 */
	public Vector<Mineral> getMiningInformation() {
		return this.miningInfo.getMiningInformation();
	}
}

////- CLASS IMPLEMENTATION ...................................................................................
//class FieldTransformer implements Serializable {
//	private static final long	serialVersionUID	= 6877368012779715581L;
//
//	protected int convert2Integer(String fieldValue) {
//		return Integer.parseInt(fieldValue.trim());
//	}
//}
// - UNUSED CODE ............................................................................................
