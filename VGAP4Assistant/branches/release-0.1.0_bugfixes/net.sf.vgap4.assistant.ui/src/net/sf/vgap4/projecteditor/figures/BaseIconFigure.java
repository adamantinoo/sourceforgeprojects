//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.ui.Activator;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This type of classes will draw an iconic representation of an element. Each one will add some decorators to
 * identify some of the characteristics, like color or small drawings on the corners.<br>
 * To know which decorators or the final shape of them it has to get access to the underlying model data that
 * it is stored on the main figure.
 */
public class BaseIconFigure extends Figure {
	private static Logger			logger			= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private final BaseFigure	container;
	private int								drawingSize	= 11;
	/** Drawing color for the unit. Depends on the game side. */
	private Color							color				= Activator.COLOR_DEFAULTBASE;

	// TODO Change the scope of drawingSize to class static

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseIconFigure(final BaseFigure parentFigure) {
		container = parentFigure;
		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Color getColor() {
		return color;
	}

	public Point getHotSpot() {
		return new Point(drawingSize / 2, drawingSize / 2);
	}

	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		return new Dimension(drawingSize, drawingSize);
	}

	private void dispose() {
		color = null;
	}

	protected BaseFigure getContainer() {
		return container;
	}

	protected boolean isSelected() {
		return container.isSelected();
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = this.getBounds().getCopy();

		// - Draw the figure body
		//FIXME I have tried only the background color for painting
		//		graphics.setForegroundColor(ColorConstants.darkGray);
		graphics.setBackgroundColor(this.getColor());
		//		graphics.fillOval(bound);
		graphics.fillRoundRectangle(bound, 3, 3);
	}

	protected void setDrawingSize(final int figureSize) {
		drawingSize = figureSize;
		this.setSize(figureSize, figureSize);
	}
}

// - UNUSED CODE ............................................................................................
