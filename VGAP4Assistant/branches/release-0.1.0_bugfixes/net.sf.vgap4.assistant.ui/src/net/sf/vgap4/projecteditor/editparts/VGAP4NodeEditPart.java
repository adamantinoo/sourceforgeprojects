//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.logging.Logger;

import es.ftgroup.gef.parts.AbstractNodeEditPart;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.model.Route;
import net.sf.vgap4.projecteditor.model.PositionableUnit;
import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
public class VGAP4NodeEditPart extends AbstractNodeEditPart {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S
	// protected ConnectionAnchor anchor;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public VGAP4NodeEditPart() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (PositionableUnit.PROP_LOCATION.equals(prop)) {
			refreshVisuals();
			return;
		}
		// TODO Activate property changes for connections
		// if (Shape.SOURCE_CONNECTIONS_PROP.equals(prop)) {
		// refreshSourceConnections();
		// } if (Shape.TARGET_CONNECTIONS_PROP.equals(prop)) {
		// refreshTargetConnections();
		// }
	}

	// - P R O T E C T E D - S E C T I O N
	private VGAP4Node getCastedModel() {
		return (VGAP4Node) getModel();
	}

	// protected ConnectionAnchor getConnectionAnchor() {
	// if (null == anchor) {
	// anchor = new ChopboxAnchor(getFigure());
	// }
	// return anchor;
	// }

	// - O V E R R I D E - S E C T I O N
	// @Override
	// protected IFigure createFigure() {
	// IFigureFactory factory = this.getFactory().getFigureFactory();
	// IFigure fig = factory.createFigure(this, this.getCastedModel());
	// fig.setOpaque(true);
	// return fig;
	// }
	//
	// @Override
	// protected void createEditPolicies() {
	// // EMPTY method body
	// }

	@Override
	protected List<Route> getModelSourceConnections() {
		return getCastedModel().getSourceConnections();
	}

	@Override
	protected List<Route> getModelTargetConnections() {
		return getCastedModel().getTargetConnections();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final Figure fig = (Figure) getFigure();

		final Dimension size = fig.getSize();
		logger.info("Calculated figure size: " + size);
		Point location = getCastedModel().getLocation();
		//- Change location to the neutral coordinates.
		//		location.x = 3000 - location.x;
		//		location.y = 3000 - location.y;
		final Rectangle bounds = new Rectangle(location, size);
		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bounds);
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), bounds);
		super.refreshVisuals();
	}

}

// - UNUSED CODE ............................................................................................
