//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.draw2d.Figure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.models.MiningInformation;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.projecteditor.figures.PlanetFigure;
import net.sf.vgap4.projecteditor.model.VGAP4Node;
import net.sf.vgap4.projecteditor.pages.PlanetPropertyPage;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetEditPart extends VGAP4NodeEditPart implements ISelectablePart {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	private Planet getCastedModel() {
		return (Planet) getModel();
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	/**
	 * Update the visual representation of the Planet (the Figure part) from the model data. In Planet units
	 * this represents all the visible attributes that can be presented on the Properties View.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final PlanetFigure fig = (PlanetFigure) getFigure();
		final Planet model = getCastedModel();

		// - Update figure visuals from current model data.
		fig.setCoordinates(model.getLocation());
		fig.setId(model.getIdNumber());
		fig.setName(model.getName());

		fig.setNeutronium(model.getDisplayableMineralData(MiningInformation.NEUTRONIUM_ID));
		fig.setDuranium(model.getDisplayableMineralData(MiningInformation.DURANIUM_ID));
		fig.setTritanium(model.getDisplayableMineralData(MiningInformation.TRITANIUM_ID));
		fig.setMolybdenum(model.getDisplayableMineralData(MiningInformation.MOLYBDENUM_ID));
		super.refreshVisuals();
	}

	/**
	 * Changes in the selection of PlanetEditParts change the elements that are visible on the presentation map.
	 * If a <code>Planet</code> is selected then the mineral information and the native information is then
	 * visible. But all that operations take place on the <code>PlanetFigure</code>.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 * @see es.ftgroup.gef.parts.AbstractNodeEditPart#setSelected(int)
	 */
	@Override
	public void setSelected(final int value) {
		super.setSelected(value);
		// this.getCastedModel().setSelected(value);
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("PlanetEditPart[");
		buffer.append(((VGAP4Node) this.getModel()).getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		// buffer.append(((Figure)this.isSelectable()).getLocation()).append("-");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	// - I N T E R F A C E - P R O P E R T Y C H A N G E L I S T E N E R
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		super.propertyChange(evt);
	}

	// - I N T E R F A C E - I S E L E C T A B L E P A R T
	public IPropertyPage createPropertyPage(Composite top, boolean singleSelected) {
		// - Create a new set of controls for this EditPart.
		final PlanetPropertyPage page = new PlanetPropertyPage(top);
		page.setModel((Planet) getModel());
		// page.createContents();
		return page;
	}
}
// - UNUSED CODE ............................................................................................
