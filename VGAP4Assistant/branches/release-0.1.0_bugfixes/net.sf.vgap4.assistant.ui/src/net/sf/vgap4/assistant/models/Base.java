//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.projecteditor.model.VGAP4Map;
import net.sf.vgap4.projecteditor.model.VGAP4Node;

// - CLASS IMPLEMENTATION ...................................................................................
public class Base extends VGAP4Node {
	private static final long										serialVersionUID		= -2240881066097908890L;
	private static Logger												logger							= Logger.getLogger("net.sf.vgap4.assistant.models");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final int										EMPTY_TRIGGER_LEVEL	= 300;
	private static final int										MAX_MINERAL					= 0;
	private static final int										TOTALORE_MINERAL		= 1;
	private static final int										SURFACE_MINERAL			= 2;
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private VGAP4Map														map									= null;
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant. Is the user responsibility to add as many turns as available to have all
	 * the data needed to create graphics or take decisions.
	 */
	//	private Vector<BaseInformation>	baseInformation			= new Vector<BaseInformation>();
	private Hashtable<Integer, BaseInformation>	baseInformation			= new Hashtable<Integer, BaseInformation>();
	/**
	 * Pointer to the latest imported turn data. The latest is considered the turn data with the highest turn
	 * number than has been parsed.
	 */
	private int																	latestTurnNo				= -1;
	private BaseInformation											latestTurn					= null;
	/**
	 * Reference to the Planet where this base is located. This may change from turn to turn so this is cached
	 * data that should be accessed though methods to guarantee that all times we get the right reference.
	 */
	private Planet															onPlanet						= null;
	/**
	 * Max ore detected for this planet. This is used to interpolate the drawing extents for the minerals
	 * detected on the planet. It keeps the max values reached by the sum of all the mineral (reserve, ore and
	 * in the surface) found when a new turn is added.
	 */
	private Vector<Integer>											maxMinerals					= new Vector<Integer>(1);
	{
		maxMinerals.setSize(4);
		maxMinerals.set(MiningInformation.NEUTRONIUM_ID, -1);
		maxMinerals.set(MiningInformation.DURANIUM_ID, -1);
		maxMinerals.set(MiningInformation.TRITANIUM_ID, -1);
		maxMinerals.set(MiningInformation.MOLYBDENUM_ID, -1);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Base(VGAP4Map ownerMap) {
		this.map = ownerMap;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getMineralTotal() {
		int total = 0;
		Iterator<Integer> mit = maxMinerals.iterator();
		while (mit.hasNext()) {
			int minQty = mit.next().intValue();
			if (minQty > 0) total += minQty;
		}
		return total;
	}

	/**
	 * Color should be calculated depending on several model data, like the number of resources or the existence
	 * of natives. Initial rules may look similar to the rules used on the Excel game sheet.
	 * <ul>
	 * <li>Bases with orders pending should draw in orange</li>
	 * <li>Bases with without orders pending may select the color from the next list:
	 * <ul>
	 * <li> With the right levels of resources and colonists in light green.</li>
	 * <li>With excess resources to be exported in dark green.</li>
	 * <li>Going to be dismantled or with the underlying planet empty of minerals in dark blue color.</li>
	 * <li>With no update on the last turn in light gray color.</li>
	 * <li>With no determinate information or enough info to take a decision then render a dark gray color.</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * 
	 * @return the preferred color to render for this icon on the figure.
	 */
	public Color getColor() {
		//TODO Calculate the color depending on the rules
		Color setColor = Activator.COLOR_DEFAULTBASE;
		//TODO Check for no update on last turn
		//- Check for empty Planet
		int planetMin = getOnPlanet().getMineralTotal();
		if (planetMin < EMPTY_TRIGGER_LEVEL) setColor = Activator.COLOR_NOMINERALSPLANET;
		return setColor;
	}

	private Planet getOnPlanet() {
		return onPlanet;
	}

	public int getTotalNatives() {
		BaseInformation bInfo = baseInformation.get(this.getLastTurn());
		int natives = bInfo.getNativesInfo().getTotalNatives();
		return natives;
	}

	/**
	 * Gathers the current turn information into the structures in the Base to get all information from all
	 * turns. While getting this data the procedure compiles some aggregated information or updates cached data
	 * that may have changed.
	 * 
	 * @param turn
	 *          number the identified the game turn sequence.
	 * @param baseInfo
	 *          parsed information compiled into an instance of <code>BaseInformation</code>.
	 */
	public void addTurnInformation(int turn, BaseInformation baseInfo) {
		// - Get the old value for this base turn information if available.
		// If not null then we are overriding this data.
		//		try {
		BaseInformation oldData = this.baseInformation.get(new Integer(turn));
		if (null != oldData) logger.warning("This operation is overriding some previous information stored for this turn");
		this.baseInformation.put(new Integer(turn), baseInfo);
		logger.info("Adding turn " + turn + " to base '" + baseInfo.getName() + "'");
		//		} catch (ArrayIndexOutOfBoundsException aioobe) {
		//			this.baseInformation.setSize(turn + 1);
		//			this.baseInformation.set(turn, baseInfo);
		//			aioobe.printStackTrace();
		//		}
		// - Load cache data. This data is the same on all turns.
		//- Special check for the first time we load a turn.
		if (-1 == this.getIdNumber()) {
			this.setIdNumber(baseInfo.getIdNumber());
			this.setName(baseInfo.getName());
		}

		//- Reference the latest turn.
		this.latestTurnNo = Math.max(this.latestTurnNo, turn);
		this.latestTurn = this.baseInformation.get(new Integer(this.latestTurnNo));

		//- Get the Planet reference.
		this.onPlanet = this.map.getPlanet4Id(this.latestTurn.getPlanetId());
		this.onPlanet.setBase(this);
		this.setLastTurn(turn);

		// - Process max and statistical data.
		this.setLocation(baseInfo.getLocation());
		logger.info("Processing base id [" + baseInfo.getIdNumber() + "] - " + baseInfo.getName() + ". At location ["
				+ baseInfo.getLocation().x + ", " + baseInfo.getLocation().y + "]");
	}

	public int getColonists() {
		BaseInformation bInfo = getLastTurnInfo();
		return bInfo.getColonists();
	}

	private BaseInformation getLastTurnInfo() {
		try {
			return baseInformation.get(this.getLastTurn());
		} catch (Exception e) {
			return new BaseInformation();
		}
	}

	public int[] getDisplayableMineralData(int mineralId) {
		int[] data = new int[3];
		data[MAX_MINERAL] = maxMinerals.get(mineralId);
		// - Get the information form the last turn data read for this planet.
		BaseInformation info = this.baseInformation.get(this.getLastTurn());
		data[TOTALORE_MINERAL] = info.getMiningInformation().get(mineralId).getTotalOre();
		data[SURFACE_MINERAL] = info.getMiningInformation().get(mineralId).getSurfaceMineral();
		return data;
	}
}

// - UNUSED CODE ............................................................................................
