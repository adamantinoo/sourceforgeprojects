//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package es.ftgroup.gef.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractNode;
import es.ftgroup.ui.figures.IFigureFactory;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.gef.core.model.IAllowsSelection;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractNodeEditPart extends AbstractGenericEditPart implements NodeEditPart {
	private static Logger				logger	= Logger.getLogger("es.ftgroup.gef.parts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S
	protected ConnectionAnchor	anchor;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public AbstractNodeEditPart() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	private AbstractNode getCastedModel() {
		return (AbstractNode) getModel();
	}

	protected ConnectionAnchor getConnectionAnchor() {
		if (null == anchor) anchor = new ChopboxAnchor(getFigure());
		return anchor;
	}

	protected String getSelectedDecode() {
		if (getSelected() == EditPart.SELECTED_NONE) return "SELECTED_NONE";
		if (getSelected() == EditPart.SELECTED) return "SELECTED";
		if (getSelected() == EditPart.SELECTED_PRIMARY) return "SELECTED_PRIMARY";
		return "UNDEFINED_VALUE";
	}

	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	protected IFigure createFigure() {
		IFigureFactory factory = getFactory().getFigureFactory();
		IFigure fig = factory.createFigure(this, getCastedModel());
		// fig.setOpaque(true);
		return fig;
	}

	@Override
	protected void createEditPolicies() {
		// EMPTY method body
	}

	/**
	 * This method handles the activation and deactivation of selections. Changes the figure selection state and
	 * signals a repaint of that figure to update the display state.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 */
	@Override
	public void setSelected(final int value) {
		super.setSelected(value);

		// - Get the figure and signal a repaint.
		final IFigure fig = getFigure();
		if (fig instanceof SelectableFigure) {
			((SelectableFigure) fig).setSelected(value);
			fig.repaint();
		}

		// - Get the model and check if it allows selection signalling.
		AbstractNode model = getCastedModel();
		if (model instanceof IAllowsSelection) ((IAllowsSelection) getCastedModel()).setSelection(value);
	}

	// - I N T E R F A C E - N O D E E D I T P A R T
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return getConnectionAnchor();
	}
}

// - UNUSED CODE ............................................................................................
