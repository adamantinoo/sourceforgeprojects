//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.pages;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import es.ftgroup.gef.pages.AbstractPropertyPage;

import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import net.sf.vgap4.assistant.figures.DetailedShipFigure;
import net.sf.vgap4.assistant.models.Ship;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipPropertyPage extends AbstractPropertyPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.pages");
	// - G L O B A L - C O N S T A N T S

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the model. This reference allows complete access to the exported model fields. */
	private Ship		shipModel;
	private Canvas	canvas;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the interface
	 * view at runtime. Other fields may be <code>null</code> and in those situation the code will generate a
	 * informational message.
	 */
	public ShipPropertyPage(final Composite top) {
		super(top);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void createContents() {
		page = new Group(container, SWT.NONE);
		canvas = new Canvas(page, SWT.NONE);
		final LightweightSystem lws = new LightweightSystem(canvas);
		final DetailedShipFigure base = new DetailedShipFigure(this.getModel());
		base.createContents();
		lws.setContents(base);
		final Dimension baseSize = base.getPreferredSize();
		canvas.setSize(baseSize.width, baseSize.height);
		canvas.setLocation(2, 2);
		final Point pageSize = canvas.getSize();
		page.setSize(pageSize.x + 4, pageSize.y + 10);
	}

	public Canvas getContainer() {
		return canvas;
	}

	/**
	 * Get the associated model element. At this class this model will be sub-classed to the <code>Planet</code>
	 * interface. If the model is null then we should throw an exception to signal some internal code error.
	 */
	@Override
	public Ship getModel() {
		Assert.isNotNull(shipModel, "The page unit model instance is not set up but it is being accessed.");
		return shipModel;
	}

	public Point getSize() {
		return canvas.getSize();
	}

	//[01]

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * Intercept changes to the model properties. Changes supported by this class are only in the next list of
	 * properties:
	 * <ul>
	 * <li><b>Description</b> - user description for the Planet.</li>
	 * </ul>
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		// TODO Create a new property for the common Node notes
		// if (Planet.NOTES_PROP.equals(prop)) {
		// page.setText(getModel().getName());
		// }
	}

	/**
	 * Store a reference to the model inside the page. Check also if a value existed previously to call the
	 * notification registration mechanism so this instance will receive any property modification to the model
	 * reference.
	 */
	public void setModel(final Ship model) {
		super.setModel(model);
		shipModel = model;
		// - Build up the page once the model is valid and the page can be generated.
		this.createContents();
	}
}
// - UNUSED CODE ............................................................................................
//[01]

//	protected void createContents2() {
//		// - Create the Group that contains all contents.
//		final RowLayout rowLayout = new RowLayout();
//		rowLayout.type = SWT.VERTICAL;
//		rowLayout.marginLeft = 2;
//		rowLayout.marginRight = 2;
//		rowLayout.marginBottom = 1;
//		rowLayout.marginHeight = 0;
//		rowLayout.marginTop = 1;
//		rowLayout.wrap = false;
//
//		// - Create the group and set the name
//		FillLayout pageLayout = new FillLayout();
//		pageLayout.type = SWT.VERTICAL;
//		page = new Group(container, SWT.NONE);
//		page.setLayout(rowLayout);
//		page.setText(getModel().getIdString() + " - " + getModel().getName());
//
//		// - Add contents to the Group.
//		this.createLocation(page);
//		this.createMineralSection(page);
//		this.createDescription(page);
//		page.computeSize(SWT.DEFAULT, SWT.DEFAULT);
//	}
//
//	private void createDescription(Group container) {
//		final GridLayout grid = new GridLayout();
//		grid.numColumns = 2;
//		grid.horizontalSpacing = 2;
//		grid.verticalSpacing = 0;
//		grid.marginHeight = 0;
//		grid.marginWidth = 0;
//		RowLayout row = new RowLayout();
//		row.type = SWT.HORIZONTAL;
//		row.marginWidth = 2;
//		row.marginLeft = 2;
//		row.marginRight = 2;
//		Composite descriptionControl = new Composite(container, SWT.NONE);
//		descriptionControl.setLayout(row);
//		final Text descriptionLabel = new Text(descriptionControl, SWT.NONE);
//		descriptionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//		descriptionLabel.setEditable(false);
//		descriptionLabel.setText("Notes:");
//		descriptionLabel.setSize(new Point((descriptionLabel.getText().length() + 1) * 8 + 2, 8 * 2));
//		final Text descriptionText = new Text(descriptionControl, SWT.NONE);
//		descriptionText.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
//		descriptionText.setEditable(true);
//		descriptionText.addModifyListener(new ModifyListener() {
//			public void modifyText(final ModifyEvent event) {
//				logger.info("New Notes text: " + descriptionText.getText());
//				shipModel.setNotes(descriptionText.getText());
//			}
//		});
//		descriptionText.setText(this.getModel().getNotes());
//		descriptionText.setSize(new Point((descriptionText.getText().length() + 1) * 8 + 2, 8 * 2));
//		// TODO Calculate the widgets size
//		// RowData layoutData = new RowData();
//		// layoutData.width = grid.marginWidth + descriptionLabel.getSize().x + grid.horizontalSpacing
//		// + descriptionText.getSize().x + grid.marginWidth + 20;
//		// layoutData.height = grid.marginHeight + Math.max(descriptionLabel.getSize().y,
//		// descriptionText.getSize().y)
//		// + grid.marginHeight;
//		// descriptionControl.setLayoutData(layoutData);
//	}
//
//	private void createMineralSection(Group container) {
//		RowLayout row = new RowLayout();
//		row.type = SWT.HORIZONTAL;
//		row.marginWidth = 2;
//		row.marginLeft = 2;
//		row.marginRight = 2;
//		Composite mineralControl = new Composite(container, SWT.NONE);
//		mineralControl.setLayout(row);
//		final Text locationLabel = new Text(mineralControl, SWT.NONE);
//		locationLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//		locationLabel.setEditable(false);
//		locationLabel.setText("Minerals:");
//		locationLabel.setSize(new Point((locationLabel.getText().length() + 2) * 8, 8 * 2));
//
//		//		Canvas canvas = new Canvas(mineralControl, SWT.NONE);
//		//		canvas.setLayout(new FillLayout());
//		//		LightweightSystem lws = new LightweightSystem(canvas);
//		////		PlanetPropertyPageFigure base = new PlanetPropertyPageFigure();
//		////		lws.setContents(base);
//		////		base.createContents(this.shipModel);
//		//		Dimension canvasSize = base.getSize();
//		//		canvas.setSize(base.getSize().width, base.getSize().height);
//		//		// TODO Calculate the widgets size
//		//		canvas.setLayoutData(new RowData(canvasSize.width, canvasSize.height * 1));
//	}
//
//	private void createLocation(Group container) {
//		// final GridLayout grid = new GridLayout();
//		// grid.numColumns = 2;
//		// grid.horizontalSpacing = 2;
//		// grid.verticalSpacing = 0;
//		// grid.marginHeight = 0;
//		// grid.marginWidth = 0;
//		RowLayout row = new RowLayout();
//		row.type = SWT.HORIZONTAL;
//		row.marginWidth = 2;
//		row.marginLeft = 2;
//		row.marginRight = 2;
//		Composite locationControl = new Composite(container, SWT.NONE);
//		locationControl.setLayout(row);
//		final Text locationLabel = new Text(locationControl, SWT.NONE);
//		locationLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD));
//		locationLabel.setEditable(false);
//		locationLabel.setText("Location:");
//		locationLabel.setSize(new Point((locationLabel.getText().length() + 2) * 8 + 2 + 2, 8 * 2));
//		final Text locationText = new Text(locationControl, SWT.NONE);
//		locationText.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
//		locationText.setEditable(false);
//		locationText.setText(this.getModel().getLocation().x + " - " + this.getModel().getLocation().y);
//		locationLabel.setSize(new Point((locationText.getText().length() + 1) * 8 + 2 + 2, 8 * 2));
//		// TODO Calculate the widgets size
//		// RowData layoutData = new RowData();
//		// layoutData.width = grid.marginWidth + locationLabel.getSize().x + grid.horizontalSpacing +
//		// locationText.getSize().x
//		// + grid.marginWidth;
//		// layoutData.height = grid.marginHeight + Math.max(locationLabel.getSize().y, locationText.getSize().y)
//		// + grid.marginHeight;
//		// locationControl.setLayoutData(layoutData);
//	}
