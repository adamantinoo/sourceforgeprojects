//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedBaseFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final Base		baseModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedBaseFigure(final Base model) {
		super(model);
		baseModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void addAdditionalContents() {
		//		RoundedGroup mineralGroup = new RoundedGroup("Minerals:", SWT.BOLD);
		//		this.add(mineralGroup);
		final MultiLabelLine mineralTotalLabel = new MultiLabelLine();
		mineralTotalLabel.addColumn("Minerals:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		mineralTotalLabel.addColumn("N/A", SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
		this.add(mineralTotalLabel);
		this.createMineralsInfo(this);
	}

	protected void createMineralsInfo(Figure parent) {
		//DEBUG Check mineral values.
		final int max = 1500;
		//		final int res = baseModel.getFieldNumber("resN");
		//		final int sur = baseModel.getFieldNumber("eleN");
		final MineralBarFigure neutronium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_RED,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		neutronium.setLevels(max, baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
		final MineralLine neutroniumLine = new MineralLine("N", neutronium);
		neutroniumLine.setValues(baseModel.getFieldNumber("ele N"), baseModel.getFieldNumber("Ore N"));
		//    if (true) neutroniumLine.setBorder(new LineBorder());
		parent.add(neutroniumLine);
		neutroniumLine.setToolTip(new Label(
				"Cantidad de Neutronio (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure duranium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKBLUE,
				AssistantConstants.COLOR_STANDARD_BLUE);
		duranium.setLevels(max, baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
		final MineralLine duraniumLine = new MineralLine("D", duranium);
		duraniumLine.setValues(baseModel.getFieldNumber("ele D"), baseModel.getFieldNumber("Ore D"));
		//    if (true) duraniumLine.setBorder(new LineBorder());
		parent.add(duraniumLine);
		duraniumLine.setToolTip(new Label(
				"Cantidad de Duranium (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure tritanium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKGREN,
				AssistantConstants.COLOR_STANDARD_GREEN);
		tritanium.setLevels(max, baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
		final MineralLine tritaniumLine = new MineralLine("T", tritanium);
		tritaniumLine.setValues(baseModel.getFieldNumber("ele T"), baseModel.getFieldNumber("Ore T"));
		//    if (true) tritaniumLine.setBorder(new LineBorder());
		parent.add(tritaniumLine);
		tritaniumLine.setToolTip(new Label(
				"Cantidad de Tritanium (mineral/ore) en la base. Cada bloque de color representa 150 unidades."));

		final MineralBarFigure molybdenum = new MineralBarFigure(AssistantConstants.COLOR_BROWN,
				AssistantConstants.COLOR_STANDARD_ORANGE);
		molybdenum.setLevels(max, baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
		final MineralLine molybdenumLine = new MineralLine("M", molybdenum);
		molybdenumLine.setValues(baseModel.getFieldNumber("ele M"), baseModel.getFieldNumber("Ore M"));
		//    if (true) molybdenumLine.setBorder(new LineBorder());
		parent.add(molybdenumLine);
		molybdenumLine.setToolTip(new Label(
				"Cantidad de Molybdenunm en la base. Cada bloque de color representa 150 unidades."));
	}
}

// - UNUSED CODE ............................................................................................
