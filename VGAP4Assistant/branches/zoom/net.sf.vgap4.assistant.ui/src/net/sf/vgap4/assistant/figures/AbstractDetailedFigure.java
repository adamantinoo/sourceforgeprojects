//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractDetailedFigure extends SelectableFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger					= Logger.getLogger("net.sf.vgap4.assistant.figures");
	protected static final Color	COLOR_SHIP_NAME	= new Color(Display.getDefault(), 0, 0, 0);
	protected static final Color	COLOR_BLACK			= new Color(Display.getDefault(), 0, 0, 0);

	// - F I E L D - S E C T I O N ............................................................................
	private AssistantNode					genericModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractDetailedFigure(AssistantNode model) {
		this.genericModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		setupLayout();

		Label nameLabel = new BoldLabel(genericModel.getIdentifier());
		nameLabel.setForegroundColor(AssistantConstants.COLOR_MEDIUM_BLUE);
		this.add(nameLabel);

		MultiLabelLine coordsLabel = new MultiLabelLine();
		coordsLabel.addColumn("Coords:", SWT.BOLD, COLOR_BLACK);
		coordsLabel.addColumn(genericModel.getLocationString(), SWT.NORMAL, COLOR_BLACK);
		this.add(coordsLabel);

		addAdditionalContents();
	}

	protected abstract void addAdditionalContents();

	private void setupLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 2;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}
}

// - UNUSED CODE ............................................................................................
