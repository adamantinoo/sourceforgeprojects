//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractVGAP4Page extends GraphicalEditorWithFlyoutPalette {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public AbstractVGAP4Page() {
	}

	@Override
	protected CommandStack getCommandStack() {
		return super.getCommandStack();
	}
}

// - UNUSED CODE ............................................................................................
