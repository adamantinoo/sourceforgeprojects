//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
public class MineralBarFigure extends Figure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger				= Logger.getLogger("net.sf.vgap4.assistant.figures");
	private static final int	BOX_WIDTH			= 4;
	private static final int	BOX_HEIGHT		= 4;
	private static final int	MAX_BOX_COUNT	= 15;

	// - F I E L D - S E C T I O N ............................................................................
	private final Color				colorOre;
	private final Color				colorMineral;
	private int								maximun				= MineralBarFigure.MAX_BOX_COUNT;
	private int								ore						= 0;
	private int								mineral				= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MineralBarFigure(final Color colorOre, final Color colorMin) {
		this.colorOre = colorOre;
		colorMineral = colorMin;
		this.setSize(this.getPreferredSize());
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(MineralBarFigure.BOX_WIDTH * MineralBarFigure.MAX_BOX_COUNT + 2, MineralBarFigure.BOX_HEIGHT);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setLevels(final int maximun, final int ore, final int mineral) {
		this.maximun = Math.max(MineralBarFigure.MAX_BOX_COUNT, maximun);

		// - Interpolate values to graphic size.
		this.ore = ore * MineralBarFigure.MAX_BOX_COUNT / this.maximun;
		this.mineral = mineral * MineralBarFigure.MAX_BOX_COUNT / this.maximun;
		this.repaint();
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		final Point location = this.getLocation();

		// - Draw the small figure boxes
		final Rectangle box = new Rectangle();
		box.width = 3;
		box.height = 3;
		box.y = location.y;
		for (int i = 0; i < MineralBarFigure.MAX_BOX_COUNT; i++) {
			// - Select the color depending on the content of mineral.
			//			graphics.setForegroundColor(ColorConstants.lightGray);
			graphics.setBackgroundColor(ColorConstants.lightGray);
			if (i <= (mineral + ore)) {
				//				graphics.setForegroundColor(colorMineral);
				graphics.setBackgroundColor(colorMineral);
			} else {
				if (i <= ore) {
					//					graphics.setForegroundColor(colorOre);
					graphics.setBackgroundColor(colorOre);
				}
			}
			box.x = location.x + i * MineralBarFigure.BOX_WIDTH;
			graphics.fillRectangle(box);
		}
	}
}
// - UNUSED CODE ............................................................................................
