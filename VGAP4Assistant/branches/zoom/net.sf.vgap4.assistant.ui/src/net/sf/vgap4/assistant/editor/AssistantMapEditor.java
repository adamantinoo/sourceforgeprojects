//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CommandStackListener;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.PrintAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;

import net.sf.vgap4.assistant.models.AssistantMap;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantMapEditor extends MultiPageEditorPart {
	// - G L O B A L - S E C T I O N ..........................................................................
	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editor");
	/** Palette component, holding the tools and shapes. */
	private static PaletteRoot	PALETTE_MODEL;

	// - I N N E R C L A S S - S E C T I O N .................................................................
	// - CLASS IMPLEMENTATION ................................................................................
	/**
	 * This class listens for command stack changes of the pages contained in this editor and decides if the
	 * editor is dirty or not.
	 * 
	 * @author Gunnar Wagenknecht
	 */
	class MultiPageCommandStackListener implements CommandStackListener {
		// - F I E L D - S E C T I O N .........................................................................
		/** the observed command stacks */
		private List<CommandStack>	commandStacks	= new ArrayList<CommandStack>(2);

		// - M E T H O D - S E C T I O N .......................................................................
		/**
		 * Adds a <code>CommandStack</code> to observe.
		 * 
		 * @param commandStack
		 */
		public void addCommandStack(CommandStack commandStack) {
			commandStacks.add(commandStack);
			commandStack.addCommandStackListener(this);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java.util.EventObject)
		 */
		public void commandStackChanged(EventObject event) {
			if (((CommandStack) event.getSource()).isDirty()) {
				// at least one command stack is dirty,
				// so the multi page editor is dirty too
				setDirty(true);
			} else {
				// probably a save, we have to check all command stacks
				boolean oneIsDirty = false;
				for (Iterator<CommandStack> stacks = commandStacks.iterator(); stacks.hasNext();) {
					CommandStack stack = stacks.next();
					if (stack.isDirty()) {
						oneIsDirty = true;
						break;
					}
				}
				setDirty(oneIsDirty);
			}
		}

		/**
		 * Disposed the listener
		 */
		public void dispose() {
			for (Iterator<CommandStack> stacks = commandStacks.iterator(); stacks.hasNext();) {
				(stacks.next()).removeCommandStackListener(this);
			}
			commandStacks.clear();
		}

		/**
		 * Marks every observed command stack beeing saved. This method should be called whenever the editor/model
		 * was saved.
		 */
		public void markSaveLocations() {
			for (Iterator<CommandStack> stacks = commandStacks.iterator(); stacks.hasNext();) {
				CommandStack stack = stacks.next();
				stack.markSaveLocation();
			}
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	/** the multi page editor's dirty state */
	private boolean																isDirty						= false;
	/** This is the root of the editor's model. */
	private AssistantMap													diagram;
	/**
	 * The selection listener for this editor instance. Every editor has its own listener. This listener is in
	 * change to collect the selection on the Main Map and to extract the model information and pass it to the
	 * <code>SelectionPropertiesView</code> for update of the selection list data.
	 */
	protected final ISelectionListener						selectionListener	= new VGAP4SelectionListener(this);
	/** the delegating CommandStack */
	private DelegatingCommandStack								delegatingCommandStack;
	private Hashtable<Integer, AbstractVGAP4Page>	pages							= new Hashtable<Integer, AbstractVGAP4Page>();
	private int																		mapPageId					= -1;
	/** the <code>CommandStackListener</code> */
	private MultiPageCommandStackListener					multiPageCommandStackListener;
	/** the editor's action registry */
	private ActionRegistry												actionRegistry;

	// - C O N S T R U C T O R S
	// /** Create a new VGAP4 Project Editor instance. This is called by the Workspace. */
	// public VGAP4MapViever() {
	// setEditDomain(new DefaultEditDomain(this));
	// }
	private AbstractVGAP4Page getMainMapPage() {
		return pages.get(new Integer(mapPageId));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the <code>CommandStack</code> for this editor.
	 * 
	 * @return the <code>CommandStack</code>
	 */
	protected DelegatingCommandStack getDelegatingCommandStack() {
		if (null == delegatingCommandStack) {
			delegatingCommandStack = new DelegatingCommandStack();
			if (null != getCurrentPage()) delegatingCommandStack.setCurrentCommandStack(getCurrentPage().getCommandStack());
		}
		return delegatingCommandStack;
	}

	/**
	 * Returns the selection listener.
	 * 
	 * @return the <code>ISelectionListener</code>
	 */
	protected ISelectionListener getSelectionListener() {
		return this.selectionListener;
	}

	/**
	 * Returns the global command stack listener.
	 * 
	 * @return the <code>CommandStackListener</code>
	 */
	protected MultiPageCommandStackListener getMultiPageCommandStackListener() {
		if (null == multiPageCommandStackListener) multiPageCommandStackListener = new MultiPageCommandStackListener();
		return multiPageCommandStackListener;
	}

	@Override
	public boolean isDirty() {
		return isDirty;
	}

	/**
	 * Changes the dirty state.
	 * 
	 * @param dirty
	 */
	protected void setDirty(boolean dirty) {
		if (isDirty != dirty) {
			isDirty = dirty;
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	// /**
	// * Returns the selection listener.
	// *
	// * @return the <code>ISelectionListener</code>
	// */
	// protected ISelectionListener getSelectionListener() {
	// return selectionListener;
	// }

	@Override
	public void doSave(IProgressMonitor monitor) {
		try {
			if (getEditorInput() instanceof IFileEditorInput) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(out);
				oos.writeObject(getModel());
				oos.close();
				IFile file = ((IFileEditorInput) getEditorInput()).getFile();
				file.setContents(new ByteArrayInputStream(out.toByteArray()), true, // keep saving, even if IFile is
						// out
						// of sync with the Workspace
						false, // dont keep history
						monitor); // progress monitor
				getMultiPageCommandStackListener().markSaveLocations();
			}
			if (getEditorInput() instanceof FileStoreEditorInput) {
				// - Convert to a suitable File class to access the URI
				final FileStoreEditorInput fse = (FileStoreEditorInput) getEditorInput();
				// - The URI gets the file name and path for the scenery data.
				final URI sceneryURI = fse.getURI();
				final FileOutputStream fos = new FileOutputStream(sceneryURI.getPath());

				// - There are two things to be read, the Model and the scenery properties
				final ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(getModel());
				oos.close();
				// getCommandStack().markSaveLocation();
				// TODO Get the save method from the WorkflowEditor to add progress monitor and error checkings.
				getMultiPageCommandStackListener().markSaveLocations();
				this.setDirty(false);
			}
		} catch (CoreException ce) {
			handleLoadException(ce);
		} catch (IOException ioe) {
			handleLoadException(ioe);
		}
	}

	/** This method should be called on this release. It has to be adapted to the RCP file save method. */
	@Override
	public void doSaveAs() {
		if (false) {
			Shell shell = getSite().getWorkbenchWindow().getShell();
			SaveAsDialog dialog = new SaveAsDialog(shell);
			dialog.setOriginalFile(((IFileEditorInput) getEditorInput()).getFile());
			dialog.open();

			IPath path = dialog.getResult();
			if (path != null) {
				// try to save the editor's contents under a different file name
				final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				try {
					new ProgressMonitorDialog(shell).run(false, // don't fork
							false, // not cancelable
							new WorkspaceModifyOperation() { // run this operation
								@Override
								public void execute(final IProgressMonitor monitor) {
									try {
										ByteArrayOutputStream out = new ByteArrayOutputStream();
										ObjectOutputStream oos = new ObjectOutputStream(out);
										oos.writeObject(getModel());
										oos.close();
										file.create(new ByteArrayInputStream(out.toByteArray()), // contents
												true, // keep saving, even if IFile is out of sync with the Workspace
												monitor); // progress monitor
									} catch (CoreException ce) {
										ce.printStackTrace();
									} catch (IOException ioe) {
										ioe.printStackTrace();
									}
								}
							});
					// set input to the new file
					setInput(new FileEditorInput(file));
					// getCommandStack().markSaveLocation();
					getMultiPageCommandStackListener().markSaveLocations();
				} catch (InterruptedException ie) {
					// should not happen, since the monitor dialog is not cancelable
					ie.printStackTrace();
				} catch (InvocationTargetException ite) {
					ite.printStackTrace();
				}
			}
		}
	}

	/**
	 * Closes this editor.
	 * 
	 * @param save
	 */
	void closeEditor(final boolean save) {
		getSite().getShell().getDisplay().syncExec(new Runnable() {
			public void run() {
				getSite().getPage().closeEditor(AssistantMapEditor.this, save);
			}
		});
	}

	public AssistantMap getModel() {
		return diagram;
	}

	// private void createOutputStream(OutputStream os) throws IOException {
	// ObjectOutputStream oos = new ObjectOutputStream(os);
	// oos.writeObject(getModel());
	// oos.close();
	// }

	private void handleLoadException(Exception e) {
		System.err.println("** Load failed. Using default model. **");
		e.printStackTrace();
		diagram = new AssistantMap();
	}

	@Override
	protected void createPages() {
		// TODO Create the Map as a new Page
		try {
			MainMapPage mainPage = new MainMapPage(this);
			mapPageId = addPage(mainPage, getEditorInput());
			pages.put(new Integer(mapPageId), mainPage);
			setPageText(mapPageId, mainPage.getPageName());
			// - Set active page
			setActivePage(mapPageId);

			// // create compound tasks page
			// compoundTasksPageID = addPage(new CompoundTasksPage(this), getEditorInput());
			// setPageText(compoundTasksPageID, getCompoundTasksPage().getPageName());

			// add command stacks
			// TODO Concentrate all the command stacks into the global one.
			// getMultiPageCommandStackListener().addCommandStack(getWorkflowPage().getCommandStack());
			// getMultiPageCommandStackListener().addCommandStack(getCompoundTasksPage().getCommandStack());

			// activate delegating command stack
			// TODO Concentrate the command stacks.
			getDelegatingCommandStack().setCurrentCommandStack(this.getMainMapPage().getCommandStack());

		} catch (PartInitException pie) {
			ErrorDialog.openError(getSite().getShell(), "Open Error", "An error occured during opening the editor.", pie
					.getStatus());
		}
	}

	// public void commandStackChanged(EventObject event) {
	// firePropertyChange(IEditorPart.PROP_DIRTY);
	// super.commandStackChanged(event);
	// }

	@Override
	public void dispose() {
		// dispose multi page command stack listener
		getMultiPageCommandStackListener().dispose();

		// // remove delegating CommandStackListener
		// getDelegatingCommandStack().removeCommandStackListener(getDelegatingCommandStackListener());

		// remove selection listener
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(getSelectionListener());

		// dispose the ActionRegistry (will dispose all actions)
		getActionRegistry().dispose();

		// important: always call super implementation of dispose
		super.dispose();
	}

	@Override
	protected void firePropertyChange(int propertyId) {
		super.firePropertyChange(propertyId);
		// IMPROVE The update of the actions is disabled.
		// updateActions(editorActionIDs);
	}

	/**
	 * This method is called upon file selection with the matching file extension. File extension associations
	 * may be defined at the plug-in level or at the Workbench configuration Properties.<br>
	 * This method is also used to register our selection listener.<br>
	 * This method should check the validity of the input source by reading the byte model.<br>
	 * The check of the input has to be done by testing what is the method used to call this plug-in, if from an
	 * Eclipse Workspace of from a RCP application in a stand-alone runtime. This has changes upon the File
	 * identification.
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#init(org.eclipse.ui.IEditorSite,org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		//TODO Register the editor instances inside the application registry.
		// TODO We can receive input form a Workspace (as a standard plug-in)
		// or we can get a path that is the data when we are using an RCP application.
		if (input instanceof IFileEditorInput) {
			// TODO When a new file is created is has to be initialized with a new model and then marked for
			// modification.
			try {
				IFile file = ((IFileEditorInput) input).getFile();
				if (file.exists()) {
					try {
						ObjectInputStream in = new ObjectInputStream(file.getContents());
						diagram = (AssistantMap) in.readObject();
						in.close();
					} catch (EOFException eof) {
						logger.warning(eof.getMessage());
						// - The file in empty. Create a new empty diagram
						logger.info("File selected: " + file.getName() + " was found empty. Creating new map.");
						diagram = new AssistantMap();
						this.setDirty(true);
					} catch (IOException e) {
						handleLoadException(e);
					}
				} else {
					// - The file does not exist. Create a new empty diagram
					logger.info("File selected: " + file.getName() + " does not exist. Creating new map.");
					diagram = new AssistantMap();
					this.setDirty(true);
				}
				//TODO Extract the file name from the complete path.
				setPartName(file.getFullPath().lastSegment());
			} catch (CoreException e) {
				handleLoadException(e);
			} catch (ClassNotFoundException e) {
				handleLoadException(e);
			}
		}
		if (input instanceof FileStoreEditorInput) {
			URI sceneryURI = null;
			try {
				// - Convert to a suitable File class to access the URI
				final FileStoreEditorInput fse = (FileStoreEditorInput) input;

				// - The URI gets the file name and path for the scenery data.
				sceneryURI = fse.getURI();
				final FileInputStream fis = new FileInputStream(sceneryURI.getPath());

				// - There are two things to be read, the Model and the scenery properties
				final ObjectInputStream in = new ObjectInputStream(fis);
				diagram = (AssistantMap) in.readObject();
				in.close();
			} catch (EOFException eof) {
				logger.warning(eof.getMessage());
				// - The file in empty. Create a new empty diagram
				logger.info("File selected: " + sceneryURI.getPath() + " was found empty. Creating new map.");
				diagram = new AssistantMap();
				this.setDirty(true);
			} catch (IOException ioe) {
				logger.warning(ioe.getMessage());
				// - The file has give an errory. Create a new empty diagram
				logger.info("File selected: " + sceneryURI.getPath() + " was found empty. Creating new map.");
				diagram = new AssistantMap();
				this.setDirty(true);
			} catch (ClassNotFoundException cnfe) {
				handleLoadException(cnfe);
			} catch (ClassCastException cce) {
				handleLoadException(cce);
			} finally {
				setPartName(sceneryURI.getPath());
			}
		}
		// - This is a valid map file.
		super.init(site, input);

		// - Add this editor selection listener to the list of listeners of this window
		final ISelectionService globalSelectionService = getSite().getWorkbenchWindow().getSelectionService();
		globalSelectionService.addSelectionListener(getSelectionListener());

		// TODO Add printing support
		getActionRegistry().registerAction(new PrintAction(this));

		//TODO Open the preferred perspective is still not open
		//		IWorkbench workbench = PlatformUI.getWorkbench();
		//		showPerspective(String perspectiveId, IWorkbenchWindow window);
		//		IWorkbenchWindow getActiveWorkbenchWindow();

		// // add delegating CommandStackListener
		// getDelegatingCommandStack().addCommandStackListener(getDelegatingCommandStackListener());

		// initialize actions
		// createActions();
	}

	/**
	 * Returns the action registry of this editor.
	 * 
	 * @return the action registry
	 */
	protected ActionRegistry getActionRegistry() {
		if (actionRegistry == null) actionRegistry = new ActionRegistry();
		return actionRegistry;
	}

	// /**
	// * Set up the editor's initial content (after creation).
	// *
	// * @see org.eclipse.gef.ui.parts.GraphicalEditorWithFlyoutPalette#initializeGraphicalViewer()
	// */
	// @Override
	// protected void initializeGraphicalViewer() {
	// super.initializeGraphicalViewer();
	// GraphicalViewer viewer = getGraphicalViewer();
	// viewer.setContents(getModel()); // set the contents of this editor
	//
	// // listen for dropped parts
	// viewer.addDropTargetListener(createTransferDropTargetListener());
	// }

	// @Override
	// public boolean isDirty() {
	// return super.isDirty();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return true;
	}

	// /**
	// * Configure the graphical viewer before it receives contents.
	// * <p>
	// * This is the place to choose an appropriate RootEditPart and EditPartFactory for your editor. The
	// * RootEditPart determines the behavior of the editor's "work-area". For example, GEF includes zoomable
	// and
	// * scrollable root edit parts. The EditPartFactory maps model elements to edit parts (controllers).
	// * </p>
	// *
	// * @see org.eclipse.gef.ui.parts.GraphicalEditor#configureGraphicalViewer()
	// */
	// @Override
	// protected void configureGraphicalViewer() {
	// super.configureGraphicalViewer();
	//
	// GraphicalViewer viewer = getGraphicalViewer();
	// viewer.setEditPartFactory(new VGAP4ProjectEditPartFactory(new VGAP4ProjectFigureFactory()));
	// viewer.setRootEditPart(new VGAPScalableFreeformRootEditPart());
	// viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));
	//
	// // configure the context menu provider
	// ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
	// ((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
	// viewer.setContextMenu(cmProvider);
	// getSite().registerContextMenu(cmProvider, viewer);
	// }

	/**
	 * <p>
	 * Unlike most of the other set methods on this class, this method does not fire a property change. Clients
	 * that call this method from a subclass must ensure that they fire an IWorkbenchPartConstants.PROP_INPUT
	 * property change after calling this method but before leaving whatever public method they are in. Clients
	 * that expose this method as public API must fire the property change within their implementation of
	 * setInput.
	 * </p>
	 */
	@Override
	protected void setInput(IEditorInput input) {
		super.setInput(input);
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	protected PaletteRoot getPaletteRoot() {
		if (PALETTE_MODEL == null) PALETTE_MODEL = VGAP4ProjectPaletteFactory.createPalette();
		return PALETTE_MODEL;
	}

	// public boolean isDirty() {
	// return isDirty;
	// }

	@Override
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);

		// - Refresh content depending on current page
		currentPageChanged();
	}

	@Override
	protected void setActivePage(int pageIndex) {
		super.setActivePage(pageIndex);

		// - Refresh content depending on current page
		currentPageChanged();
	}

	/**
	 * Indicates that the current page has changed.
	 * <p>
	 * We update the DelegatingCommandStack, OutlineViewer and other things here.
	 */
	protected void currentPageChanged() {
		// update delegating command stack
		getDelegatingCommandStack().setCurrentCommandStack(getCurrentPage().getCommandStack());

		// update zoom actions
		// getDelegatingZoomManager().setCurrentZoomManager(getZoomManager(getCurrentPage().getGraphicalViewer()));

		// reinitialize outline page
		// getOutlinePage().initialize(getCurrentPage());
	}

	/**
	 * Returns the current active page.
	 * 
	 * @return the current active page or <code>null</code>
	 */
	private AbstractVGAP4Page getCurrentPage() {
		int active = this.getActivePage();
		return pages.get(new Integer(active));
	}

	// /**
	// * Changes the dirty state.
	// *
	// * @param dirty
	// */
	// private void setDirty(boolean dirty) {
	// if (isDirty != dirty) {
	// isDirty = dirty;
	// firePropertyChange(IEditorPart.PROP_DIRTY);
	// }
	// }

}
// - UNUSED CODE ............................................................................................
