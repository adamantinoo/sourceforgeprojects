//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotFigure extends AssistantNodeFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger					= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	public Color					mainShapeColor	= AssistantConstants.COLOR_PLANET_NOINFO;
	public Color					decoratorColor	= AssistantConstants.COLOR_SHIP_DEFAULT;

	//	private Spot								spotModel;
	//	private AssistantNode				spotContents;
	//	private String						shapeName;
	//	private AssistantNodeFigure	subFigure;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotFigure() {
		//- Add the icon to represent this element.
		this.setDrawFigure(new SpotIconFigure(this));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	public void setModel(Spot model) {
	//		//- Get the spot contents and process them to know what are our shape.
	//		this.spotModel = model;
	//		this.spotContents = model.getRepresentative();
	//		if (this.spotContents instanceof Base) {
	//			((SpotIconFigure) this.iconic).setBaseFigure(new BaseIconFigure(this));
	//			return;
	//		}
	//		if (this.spotContents instanceof Planet) {
	//			((SpotIconFigure) this.iconic).setBaseFigure(new PlanetIconFigure(this));
	//			return;
	//		}
	//		if (this.spotContents instanceof Ship) {
	//			((SpotIconFigure) this.iconic).setBaseFigure(new ShipIconFigure(this));
	//			return;
	//		}
	//	}
	public void setBaseFigure(String baseCode) {
		((SpotIconFigure) this.iconic).setBaseFigure(baseCode);
	}

	//	public void setPlanetColor(Color newBaseColor) {
	//		this.mainShapeColor = newBaseColor;
	//	}

	public void setMainColor(Color newBaseColor) {
		this.mainShapeColor = newBaseColor;
	}

	public void setDecoratorColor(Color newDecoratorColor) {
		this.decoratorColor = newDecoratorColor;
	}

	public void activateBase() {
		((SpotIconFigure) this.iconic).baseActive = true;
	}

	public void activateNatives() {
		((SpotIconFigure) this.iconic).hasNatives = true;
	}

	public void activateShip() {
		((SpotIconFigure) this.iconic).shipInOrbit = true;
	}

	public void activatePods() {
		((SpotIconFigure) this.iconic).podInOrbit = true;
	}

	public void activateWings() {
		((SpotIconFigure) this.iconic).wingsInOrbit = true;
	}

	public void clearActivates() {
		((SpotIconFigure) this.iconic).baseActive = false;
		((SpotIconFigure) this.iconic).hasNatives = false;
		((SpotIconFigure) this.iconic).shipInOrbit = false;
		((SpotIconFigure) this.iconic).podInOrbit = false;
		((SpotIconFigure) this.iconic).wingsInOrbit = false;
	}

	public Image getIconImage() {
		return ((SpotIconFigure) this.iconic).convertToImage();
	}
}
// - UNUSED CODE ............................................................................................
