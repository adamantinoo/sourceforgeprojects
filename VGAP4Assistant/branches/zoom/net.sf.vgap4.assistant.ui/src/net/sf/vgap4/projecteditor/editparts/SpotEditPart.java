//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.figures.SpotFigure;
import net.sf.vgap4.assistant.figures.SpotIconFigure;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.pages.BasePropertyPage;
import net.sf.vgap4.assistant.pages.PlanetPropertyPage;
import net.sf.vgap4.assistant.pages.ShipPropertyPage;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotEditPart extends AssistantNodeEditPart implements ISelectablePart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public SpotEditPart() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Instead creating a simple PropertyPage this method should create one page for each element in this
	 * location.
	 */
	public IPropertyPage createPropertyPage(final Composite top, final boolean singleSelected) {
		final Spot spot = this.getCastedModel();
		final Iterator<AssistantNode> cit = spot.getContents().iterator();
		try {
			while (cit.hasNext()) {
				final AssistantNode element = cit.next();
				if (element instanceof Base) {
					final BasePropertyPage page = new BasePropertyPage(top);
					page.setModel((Base) element);
				}
				if (element instanceof Planet) {
					final PlanetPropertyPage page = new PlanetPropertyPage(top);
					page.setModel((Planet) element);
				}
				if (element instanceof Ship) {
					final ShipPropertyPage page = new ShipPropertyPage(top);
					page.setModel((Ship) element);
				}
			}
			//			return page;
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Spot getCastedModel() {
		return (Spot) this.getModel();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (AssistantMap.CHANGE_ZOOMFACTOR.equals(prop)) {
			this.refreshFigureLocation();
			return;
		}
		super.propertyChange(evt);
	}

	private Rectangle getMapBoundaries() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getSpotRegistry().getBoundaries().getCopy();
	}

	private int getMapSize() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getMapSize();
	}

	private void refreshFigureLocation() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();
		//		AssistantMap map = (AssistantMap) this.getParent().getModel();

		//- Get the location from the model information and then convert to the final coordinates.
		final Dimension size = fig.getSize();
		final Point location = representative.getLocation();

		//- Invert the coordinates for the Y axis depending on the map size.
		final int mapSize = this.getMapSize();
		location.y = mapSize - location.y;

		//- Adjust location depending on the top-left minimum coordinates.
		final Rectangle boundaries = this.getMapBoundaries();
		final Point origin = new Point(Math.max(boundaries.x - 30, 0), Math.max(boundaries.height - 30, 0));
		location.x = location.x - origin.x;
		location.y = location.y - (mapSize - origin.y);
		fig.setLocation(location);

		//- Calculate real map location depending on the zoom factor.
		final int zoom = this.getZoomFactor();
		if ((zoom > 10) || (zoom < 10)) {
			if (zoom > 0) {
				final double factor = zoom / 10.0;
				location.x = new Double(location.x * factor).intValue();
				location.y = new Double(location.y * factor).intValue();
			}
		}

		final Rectangle bounds = new Rectangle(location, size);
		SpotEditPart.logger.info("Rebounding instance " + representative.getName() + " to " + bounds);
		fig.setBounds(bounds);
		((GraphicalEditPart) this.getParent()).setLayoutConstraint(this, fig, bounds);
	}

	private void refreshModelVisuals() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();

		// - Update figure visuals from current model data.
		fig.setCoordinates(representative.getLocation());
		fig.setId(representative.getIdNumber());
		fig.setName(representative.getName());
	}

	private void refreshToolTip() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();

		//- Create the tooltip with the composite information for this Spot.
		final Figure tip = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 1;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 1;
		tip.setLayoutManager(grid);
		final Label repLabel = new Label(representative.getIdString() + "-" + representative.getName(), fig.getIconImage());
		tip.add(repLabel);
		final Iterator<AssistantNode> mit = model.getContents().iterator();
		while (mit.hasNext()) {
			final AssistantNode element = mit.next();
			if (element != representative) {
				final Label eleLabel = new Label(element.getIdString() + "-" + element.getName());
				tip.add(eleLabel);
				if (element instanceof Planet) {
					if (model.hasNatives()) tip.add(new Label(((Planet) element).getNativeToolTip()));
				}
			}
		}
		fig.setToolTip(tip);
	}

	/**
	 * This method is called upon the creation of the EditPart to associate it with behavior modifiers for
	 * selection and other node functions.
	 * 
	 * @see es.ftgroup.gef.parts.AbstractNodeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	/**
	 * Overriding of this function to update the visual part of any model object when a request for model
	 * modification is received. The model change fires a <code>firePropertyChange</code> or
	 * <code>fireStructureChange</code> that usually is intercepted on the <code>propertyChange</code>
	 * method to check what king of View updates are required for the Model change. This method is then called
	 * to fire a complete update of the View side of the MVC pattern.
	 * 
	 * @see net.sf.vgap4.projecteditor.editparts.AssistantNodeEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		final int player = this.getPlayerCode();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();
		final int owner = representative.getOwner();

		//- Set the representing figure depending on representative
		fig.clearActivates();
		if (representative instanceof Base) {
			if (player == owner)
				fig.setMainColor(AssistantConstants.COLOR_BASE_OWNED);
			else
				fig.setMainColor(AssistantConstants.COLOR_BASE_ENEMY);
			fig.setBaseFigure(SpotIconFigure.SHAPE_PLANET);
			fig.activateBase();
			//TODO Change this to allow identification of friend and foe ships and other elements.
			if (model.hasShip) fig.activateShip();
			if (model.hasPods) fig.activatePods();
			if (model.hasWings) fig.activateWings();
		}
		if (representative instanceof Planet) {
			fig.setMainColor(AssistantConstants.COLOR_PLANET_INFO);
			if (-1 == model.infoAvailable()) fig.setMainColor(AssistantConstants.COLOR_PLANET_UNEXPLORED);
			if (0 == model.infoAvailable()) fig.setMainColor(AssistantConstants.COLOR_PLANET_NOINFO);
			fig.setBaseFigure(SpotIconFigure.SHAPE_PLANET);
			if (model.hasShip) fig.activateShip();
			if (model.hasPods) fig.activatePods();
			if (model.hasWings) fig.activateWings();
			if (model.hasNatives()) fig.activateNatives();
		}
		if (representative instanceof Ship) {
			if (player == owner)
				fig.setMainColor(AssistantConstants.COLOR_SHIP_DEFAULT);
			else
				fig.setMainColor(AssistantConstants.COLOR_SHIP_ENEMY);
			fig.setBaseFigure(SpotIconFigure.SHAPE_SHIP);
			if (model.hasPods) fig.activatePods();
			if (model.hasWings) fig.activateWings();
		}

		this.refreshToolTip();
		this.refreshModelVisuals();
		this.refreshFigureLocation();
	}
}

// - UNUSED CODE ............................................................................................
