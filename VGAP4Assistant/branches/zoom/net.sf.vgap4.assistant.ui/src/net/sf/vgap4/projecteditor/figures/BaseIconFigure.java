//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sf.vgap4.assistant.figures.AbstractIconFigure;
import net.sf.vgap4.assistant.figures.AssistantNodeFigure;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This type of classes will draw an iconic representation of an element. Each one will add some decorators to
 * identify some of the characteristics, like color or small drawings on the corners.<br>
 * To know which decorators or the final shape of them it has to get access to the underlying model data that
 * it is stored on the main figure.
 */
public class BaseIconFigure extends AbstractIconFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseIconFigure(final AssistantNodeFigure homeFigure) {
		super(homeFigure);
		setDrawingSize(13);
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = this.getBounds().getCopy();

		// - Draw the figure body
		//DEBUG I have tried only the background color for painting
		//		graphics.setForegroundColor(ColorConstants.darkGray);
		graphics.setBackgroundColor(this.getColor());
		//		graphics.fillOval(bound);
		graphics.fillRoundRectangle(bound, 3, 3);
	}
}

// - UNUSED CODE ............................................................................................
