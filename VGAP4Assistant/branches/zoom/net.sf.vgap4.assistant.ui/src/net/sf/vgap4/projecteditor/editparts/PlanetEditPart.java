//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.draw2d.Figure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.models.MiningInformation;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.pages.PlanetPropertyPage;
import net.sf.vgap4.projecteditor.figures.PlanetFigure;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetEditPart extends AssistantNodeEditPart implements ISelectablePart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - M E T H O D - S E C T I O N ..........................................................................
	public IPropertyPage createPropertyPage(final Composite top, final boolean singleSelected) {
		// - Create a new set of controls for this EditPart.
		final PlanetPropertyPage page = new PlanetPropertyPage(top);
		if (null != page) {
			page.setModel((Planet) this.getModel());
			return page;
		} else
			return null;
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		super.propertyChange(evt);
	}

	/**
	 * Changes in the selection of PlanetEditParts change the elements that are visible on the presentation map.
	 * If a <code>Planet</code> is selected then the mineral information and the native information is then
	 * visible. But all that operations take place on the <code>PlanetFigure</code>.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 * @see es.ftgroup.gef.parts.AbstractNodeEditPart#setSelected(int)
	 */
	@Override
	public void setSelected(final int value) {
		super.setSelected(value);
		// this.getCastedModel().setSelected(value);
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("PlanetEditPart[");
		buffer.append(((AssistantNode) this.getModel()).getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		// buffer.append(((Figure)this.isSelectable()).getLocation()).append("-");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	private Planet getCastedModel() {
		return (Planet) this.getModel();
	}

	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	/**
	 * Update the visual representation of the Planet (the Figure part) from the model data. In Planet units
	 * this represents all the visible attributes that can be presented on the Properties View.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final PlanetFigure fig = (PlanetFigure) this.getFigure();
		final Planet model = this.getCastedModel();

		// - Update figure visuals from current model data.
		fig.setModel(model);
		fig.setCoordinates(model.getLocation());
		fig.setId(model.getIdNumber());
		fig.setName(model.getName());

		fig.setNeutronium(model.getDisplayableMineralData(MiningInformation.NEUTRONIUM_ID));
		fig.setDuranium(model.getDisplayableMineralData(MiningInformation.DURANIUM_ID));
		fig.setTritanium(model.getDisplayableMineralData(MiningInformation.TRITANIUM_ID));
		fig.setMolybdenum(model.getDisplayableMineralData(MiningInformation.MOLYBDENUM_ID));
		super.refreshVisuals();
	}
}
// - UNUSED CODE ............................................................................................
