//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractPropertyChanger;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.model.Sector;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantMap extends AbstractPropertyChanger {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	/** Property ID to use when new model data is added to the diagram. */
	public static final String								DATA_ADDED_PROP					= "AssistantMap.DATA_ADDED_PROP";
	/** Property ID to use when a child is added to this diagram. */
	public static final String								CHILD_ADDED_PROP				= "AssistantMap.CHILD_ADDED_PROP";
	/** Property ID to use when a child is removed from this diagram. */
	public static final String								CHANGE_ZOOMFACTOR				= "AssistantMap.CHANGE_ZOOMFACTOR";
	/** Property ID to use when a the number referencing any of the last turns gets changed. */
	public static final String								LAST_TURN_CHANGED_PROP	= "AssistantMap.LAST_TURN_CHANGED_PROP";
	private static final long									serialVersionUID				= 3361707471258570866L;
	private static Logger											logger									= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D S
	/** Number of the last turn processed. */
	private int																lastReadTurn						= -1;
	/** Number of the higher turn processed. */
	private int																lastAvailableTurn				= -1;
	/** Number the identifies a unique game code. */
	private String														gameId;
	/** Name given to this game when created. */
	private String														gameName;
	/**
	 * This value is an estimation. Y can not get such information form the .CSV files data. It should be a
	 * configuration parameter editable though the Application Properties.
	 */
	private final int													mapSize									= 3000;
	/**
	 * Player number that matches the slot number on the game. This number may correspond to another user inside
	 * other games so it is used with the 'gameId' to uniquely identify a player slot on a game set.
	 */
	private int																playerNo								= -1;
	/**
	 * Sectors are virtual structures that keep lists of grouped elements (generally by geographic location).
	 * This field stores the references to all the sectors currently defined on this Map. This is a simple list
	 * structure and currently sectors do not have any sorting order or organization. A sector may contain other
	 * sectors to any level because it simply defines a physical rectangular map area.
	 */
	private final Vector<Sector>							sectors									= new Vector<Sector>();
	/**
	 * Array structure with references to all the Planets that have been identified on this turn or on other
	 * turns. This structure is implemented as an array because it matches the original implementation from CSV
	 * files. Probably a <code>Hashtable</code> will generate less memory usage, over all in very long games
	 * where the Id of some elements may grew high. Planets are the exception because there is a finite number
	 * of them and alwais get the lowest element Id.
	 */
	private final Hashtable<Integer, Planet>	planets									= new Hashtable<Integer, Planet>();
	/**
	 * This structure contains a reference to all player bases. This structure clearly benefits from the
	 * 'Hashtable' implementation because the number of bases for this player is small in relation to the number
	 * of planets and the number of other elements created in the game.
	 */
	private final Hashtable<Integer, Base>		bases										= new Hashtable<Integer, Base>();
	/**
	 * Another array structure that contains all the identified Ships. This list contains all ships owned by
	 * this player and also the ships detected to other players. That other ships are identified by being the
	 * 'owner' a different code than the game 'playerId'.
	 */
	private final Hashtable<Integer, Ship>		ships										= new Hashtable<Integer, Ship>();
	/**
	 * This structure stores the map elements but clustered by the location where they are presented. This will
	 * simplify the rendering of multiple elements at the same place reducing map data overlaying.<br>
	 * The structure is updated when the model suffers an structuran change, like when new turn data is read or
	 * in the future when any user element is added to the model.
	 */
	//	private final LocationRegistry						locationRegistry				= new LocationRegistry();
	private final SpotRegistry								spots										= new SpotRegistry();
	private int																zoomFactor							= AssistantConstants.DEFAULT_ZOOM_FACTOR;

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addBaseInformation(final int turn, final BaseInformation baseInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Base theBase = this.getBase4Id(baseInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theBase.addTurnInformation(turn, baseInfo);
	}

	/**
	 * Add the new turn information to the Planet structure. We have not to check that the owner and game
	 * matches because that test have been performed before arriving this point. But some check has to be
	 * performed to identify when we have not located the correct Planet. The variable 'thePlanet' may not be
	 * allowed to be NULL because we have a reference on it.
	 */
	public void addPlanetInformation(final int turn, final PlanetInformation planetInfo) {
		// - Get the Planet structure that matched to the Planet ID.
		final Planet thePlanet = this.getPlanet4Id(planetInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		thePlanet.addTurnInformation(turn, planetInfo);
	}

	public void addShipInformation(final int turn, final ShipInformation shipInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Ship theShip = this.getShip4Id(shipInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theShip.addTurnInformation(turn, shipInfo);
	}

	@Override
	public void fireStructureChange(final String property, final Object dataSet, final Object change) {
		spots.clear();

		//- Update the location clustering to be used when the EditPart request the list of children.
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final Base element = btp.next();
			spots.register(element.getLocation(), element);
		}
		final Iterator<Planet> itp = planets.values().iterator();
		while (itp.hasNext()) {
			final Planet element = itp.next();
			spots.register(element.getLocation(), element);
		}
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship element = its.next();
			spots.register(element.getLocation(), element);
		}

		super.fireStructureChange(property, dataSet, change);
	}

	public Base getBase4Id(final int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		//		try {
		final Base theBase = bases.get(new Integer(id));
		if (null == theBase) {
			//- The base is not located because this is a new element.
			bases.put(new Integer(id), new Base(this));
			return this.getBase4Id(id);
		} else
			return theBase;
	}

	/**
	 * Return a List of all the visible elements in the diagram. Visible elements are the Planets, the Bases and
	 * the Ships. The returned List should not be modified.<br>
	 */
	public Vector<Object> getChildren() {
		final Vector<Object> childs = new Vector<Object>();
		// - Add only objects that are not null
		final Iterator<Sector> itsec = sectors.iterator();
		while (itsec.hasNext()) {
			final Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final AssistantNode base = btp.next();
			childs.add(base);
		}
		final Iterator<Planet> itp = planets.values().iterator();
		while (itp.hasNext()) {
			final Planet planet = itp.next();
			if (null != planet) {
				//TODO - Add only the planets that do not have a base.
				if (!planet.isHasBase()) childs.add(planet);
			}
		}
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship ship = its.next();
			if (null != ship) childs.add(ship);
		}
		return this.getChildrenNew();
	}

	/**
	 * Return a List of all the visible elements in the Map. In this method the object that lie in the same
	 * location are clustered together inside a <code>Spot</code> structure that will reduce Map presentation
	 * and will clean up the Map presentation.
	 * 
	 * @return the list of visible Map objects for construction of the EditPart list.
	 */
	public Vector<Object> getChildrenNew() {
		final Vector<Object> childs = new Vector<Object>();

		final Iterator<Sector> itsec = sectors.iterator();
		while (itsec.hasNext()) {
			final Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}

		//- Scan the locations and generate the list of visible objects.
		final Enumeration<Point> lit = spots.getLocations();
		while (lit.hasMoreElements()) {
			childs.add(spots.getSpot(lit.nextElement()));
		}
		return childs;
	}

	public String getGameId() {
		return gameId;
	}

	public int getMapSize() {
		return mapSize;
	}

	/**
	 * Check for the array size and the array contents. The array starts empty so any try to access any null
	 * element will trigger an exception. INtercept the exceptions and resize the array adecuately to the
	 * current usage.
	 */
	public Planet getPlanet4Id(final int id) {
		final Planet thePlanet = planets.get(new Integer(id));
		if (null == thePlanet) {
			planets.put(new Integer(id), new Planet(this));
			return planets.get(new Integer(id));
		} else
			return thePlanet;
	}

	public int getPlayer() {
		return playerNo;
	}

	public int getReadTurn() {
		return lastReadTurn;
	}

	public Ship getShip4Id(final int id) {
		final Ship theShip = ships.get(new Integer(id));
		if (null == theShip) {
			ships.put(new Integer(id), new Ship(this));
			return ships.get(new Integer(id));
		} else
			return theShip;
	}

	public SpotRegistry getSpotRegistry() {
		return spots;
	}

	public int getZoomFactor() {
		return zoomFactor;
	}

	public boolean isInitialized() {
		if ((-1 == lastReadTurn) && (null == gameId))
			return false;
		else
			return true;
	}

	public void resetZoomFactor() {
		final int oldFactor = zoomFactor;
		zoomFactor = AssistantConstants.DEFAULT_ZOOM_FACTOR;
		this.firePropertyChange(AssistantMap.CHANGE_ZOOMFACTOR, oldFactor, zoomFactor);
	}

	public void setGameId(final String gameId) {
		this.gameId = gameId;
	}

	public void setGameName(final String gameName) {
		this.gameName = gameName;
	}

	public void setPlayer(final int playerNo) {
		this.playerNo = playerNo;
	}

	public void setReadTurn(final int readTurn) {
		lastReadTurn = readTurn;
		AssistantMap.logger.info("Setting latReadTurn to " + readTurn);
		lastAvailableTurn = Math.max(lastAvailableTurn, lastReadTurn);
		AssistantMap.logger.info("Setting lastAvailableTurn to " + readTurn);
	}

	public void setZoomFactor(final int newZoomFactor) {
		if (newZoomFactor > 0) {
			final int oldFactor = zoomFactor;
			zoomFactor = newZoomFactor;
			this.firePropertyChange(AssistantMap.CHANGE_ZOOMFACTOR, oldFactor, zoomFactor);
		}
	}

	public void zoomMinus() {
		this.setZoomFactor(this.getZoomFactor() - 1);
	}

	public void zoomPlus() {
		this.setZoomFactor(this.getZoomFactor() + 1);
	}
}
// - UNUSED CODE ............................................................................................
