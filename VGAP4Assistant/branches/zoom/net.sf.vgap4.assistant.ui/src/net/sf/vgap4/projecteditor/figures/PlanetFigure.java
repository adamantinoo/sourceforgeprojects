//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;

import net.sf.vgap4.assistant.figures.AssistantNodeFigure;
import net.sf.vgap4.assistant.figures.MineralsFigure;
import net.sf.vgap4.assistant.models.Planet;

// - CLASS IMPLEMENTATION ...................................................................................
public class PlanetFigure extends AssistantNodeFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger				= Logger.getLogger("net.sf.vgap4.projecteditor.figures");
	protected static final int	MARGIN				= 1;
	protected static final int	LABEL_HEIGHT	= 15;
	protected static final int	GAP4					= -4;
	protected static final int	GAP5					= 5;

	protected static final int	CHAR_WIDTH		= 6;
	protected static final int	CHAR_HEIGHT		= 12;
	protected static final int	GAP2					= 2;
	protected static final int	GAP3					= 3;
	protected static final int	LABEL_WIDTH		= (4 + 1) * 6 + PlanetFigure.GAP2;

	// - F I E L D - S E C T I O N ............................................................................
	private Planet							model;
	private final Label					nativeToolTip	= new Label();
	protected MineralsFigure		minerals;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PlanetFigure() {
		//- Add the icon to represent this element.
		this.setDrawFigure(new PlanetIconFigure(this));

		//- Add additional presentation elements.
		setMinerals(new MineralsFigure());
		//		this.add(minerals);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//FIXME Check if this method is really needed.
	@Override
	public Dimension getPreferredSize(final int wHint, final int hHint) {
		// - Get the sized of the composition objects.
		Dimension size2 = super.getPreferredSize(wHint, hHint);
		final Dimension iconicSize = iconic.getSize();
		final int labelWidth = Math.max(Math.max(idLabel.getSize().width, nameLabel.getSize().width), 41);

		// final Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = iconicSize.width + PlanetFigure.MARGIN + PlanetFigure.MARGIN + labelWidth + PlanetFigure.MARGIN;
		if (EditPart.SELECTED_NONE == this.getSelected())
			fullSize.height = PlanetFigure.MARGIN + PlanetFigure.LABEL_HEIGHT + PlanetFigure.MARGIN + 0;
		else
			fullSize.height = PlanetFigure.MARGIN + PlanetFigure.LABEL_HEIGHT + PlanetFigure.MARGIN + 16 + 1;
		return size2;
	}

	private void setMinerals(MineralsFigure mineralsFigure) {
		this.minerals = mineralsFigure;
		this.add(mineralsFigure);
		this.minLayout();
	}

	private void minLayout() {
		//- Position MINERALS. Below everything else.
		final Dimension iconicSize = iconic.getSize();
		final Dimension min = minerals.getSize();
		final Rectangle elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width + AssistantNodeFigure.MARGIN;
		elementLocation.y = idLabel.getPreferredSize().height + nameLabel.getPreferredSize().height;// Remove margins
		elementLocation.width = min.width;
		elementLocation.height = min.height;
		this.xylay.setConstraint(minerals, elementLocation);

		this.relayout();
	}

	public void setDuranium(final int[] mineralData) {
		minerals.setDuranium(mineralData);
	}

	public void setModel(final Planet model) {
		this.model = model;
		final String nativeTip = model.getNativeToolTip();
		if (null != nativeTip) {
			nativeToolTip.setText(nativeTip);
			this.setToolTip(nativeToolTip);
		} else
			this.setToolTip(null);
	}

	public void setMolybdenum(final int[] mineralData) {
		minerals.setMolybdenum(mineralData);
	}

	public void setNeutronium(final int[] mineralData) {
		minerals.setNeutronium(mineralData);
	}

	public void setTritanium(final int[] mineralData) {
		minerals.setTritanium(mineralData);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("PlanetFigure[");
		buffer.append(this.getLocation());
		buffer.append(idLabel.getText()).append("-");
		buffer.append(nameLabel.getText()).append("-");
		// buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	protected void paintChildren(final Graphics graphics) {
		// TODO Not all elements are displayed depending on the selection status.
		if (EditPart.SELECTED_NONE == this.getSelected()) {
			// - Disable the mineral graphic and the native graphic.
			minerals.setVisible(false);
			// TODO The same for the native information.
		}
		if (EditPart.SELECTED_PRIMARY == this.getSelected()) minerals.setVisible(true);
		if (EditPart.SELECTED == this.getSelected()) minerals.setVisible(true);
		// logger.info("Selection state for " + this.nameLabel.getText() + ": " + this.getSelected());
		this.setSize(this.getPreferredSize());
		super.paintChildren(graphics);
	}
}
// - UNUSED CODE ............................................................................................
