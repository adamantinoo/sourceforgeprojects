//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.MiningInformation;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedPlanetFigure extends AbstractDetailedFigure {
  // - S T A T I C - S E C T I O N ..........................................................................
  private static Logger logger  = Logger.getLogger("net.sf.vgap4.assistant.figures");

  // - F I E L D - S E C T I O N ............................................................................
  private final Planet  planetModel;

  // - C O N S T R U C T O R - S E C T I O N ................................................................
  public DetailedPlanetFigure(final Planet model) {
    super(model);
    planetModel = model;
  }

  // - M E T H O D - S E C T I O N ..........................................................................
  @Override
  protected void addAdditionalContents() {
    final MultiLabelLine passageLabel = new MultiLabelLine();
    passageLabel.addColumn("Minerals:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
    passageLabel.addColumn("-undef-", SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
    this.add(passageLabel);
    this.createMineralsInfo();
  }

  protected void createMineralsInfo() {
    //DEBUG Check mineral values.
    final MineralBarFigure neutronium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_RED,
        AssistantConstants.COLOR_STANDARD_ORANGE);
    neutronium.setLevels(planetModel.getMaxMineral(MiningInformation.NEUTRONIUM_ID),
        planetModel.getFieldNumber("resN"), planetModel.getFieldNumber("eleN"));
    final MineralLine neutroniumLine = new MineralLine("N", neutronium);
    neutroniumLine.setValues(planetModel.getFieldNumber("resN"), planetModel.getFieldNumber("eleN"));
//    if (true) neutroniumLine.setBorder(new LineBorder());
    this.add(neutroniumLine);

    final MineralBarFigure duranium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKBLUE,
        AssistantConstants.COLOR_STANDARD_BLUE);
    duranium.setLevels(planetModel.getMaxMineral(MiningInformation.DURANIUM_ID), planetModel.getFieldNumber("resD"),
        planetModel.getFieldNumber("eleD"));
    final MineralLine duraniumLine = new MineralLine("D", duranium);
    duraniumLine.setValues(planetModel.getFieldNumber("resD"), planetModel.getFieldNumber("eleD"));
//    if (true) duraniumLine.setBorder(new LineBorder());
    this.add(duraniumLine);

    final MineralBarFigure tritanium = new MineralBarFigure(AssistantConstants.COLOR_STANDARD_DARKGREN,
        AssistantConstants.COLOR_STANDARD_GREEN);
    tritanium.setLevels(planetModel.getMaxMineral(MiningInformation.TRITANIUM_ID), planetModel.getFieldNumber("resT"),
        planetModel.getFieldNumber("eleT"));
    final MineralLine tritaniumLine = new MineralLine("T", tritanium);
    tritaniumLine.setValues(planetModel.getFieldNumber("resT"), planetModel.getFieldNumber("eleT"));
//    if (true) tritaniumLine.setBorder(new LineBorder());
    this.add(tritaniumLine);

    final MineralBarFigure molybdenum = new MineralBarFigure(AssistantConstants.COLOR_BROWN,
        AssistantConstants.COLOR_STANDARD_ORANGE);
    molybdenum.setLevels(planetModel.getMaxMineral(MiningInformation.MOLYBDENUM_ID),
        planetModel.getFieldNumber("resM"), planetModel.getFieldNumber("eleM"));
    final MineralLine molybdenumLine = new MineralLine("M", molybdenum);
    molybdenumLine.setValues(planetModel.getFieldNumber("resM"), planetModel.getFieldNumber("eleM"));
//    if (true) molybdenumLine.setBorder(new LineBorder());
    this.add(molybdenumLine);
  }
}

class MineralLine extends Figure {
  public MineralLine(final String initial, final Figure bar) {
    final GridLayout grid = new GridLayout();
    grid.horizontalSpacing = 2;
    grid.marginHeight = 1;
    grid.marginWidth = 2;
    grid.numColumns = 3;
    grid.verticalSpacing = 0;
    this.setLayoutManager(grid);
    final Label mineralInitial = new BoldLabel(initial);
    this.add(mineralInitial);
    this.add(bar);
  }

  public void setValues(final int unminned, final int surface) {
    this.add(new StandardLabel(unminned + "/" + surface));
  }
}
// - UNUSED CODE ............................................................................................
