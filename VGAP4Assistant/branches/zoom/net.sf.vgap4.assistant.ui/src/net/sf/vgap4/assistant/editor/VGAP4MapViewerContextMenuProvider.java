//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.editor;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.actions.ActionFactory;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class adds the context menu (this is the menu that appears when the user clicks with the left mouse
 * button on the edit panel) to the editor panel when editing files of the type VGAP4Project. This diagrams
 * add two additional actions to the menu load Planet and Ship information from external files and
 * incorporates this to the Project Model.
 */
public class VGAP4MapViewerContextMenuProvider extends ContextMenuProvider {
	private static final String		IMPORT_TURN_DATA_ACTION	= "ImportTurnData";
	private static Logger					logger									= Logger.getLogger("net.sf.vgap4.projecteditor.editor");
	public static final String		ZOOMPLUS_ACTION					= "ZoomPlus";
	public static final String		ZOOMMINUS_ACTION				= "ZoomMinus";
	public static final String		ZOOMRESET_ACTION				= "ZoomReset";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/** The editor's action registry. */
	private final ActionRegistry	actionRegistry;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * Instantiate a new menu context provider for the specified EditPartViewer and ActionRegistry.
	 * 
	 * @param viewer
	 *          the editor's graphical viewer
	 * @param registry
	 *          the editor's action registry
	 * @throws IllegalArgumentException
	 *           if registry is <tt>null</tt>.
	 */
	public VGAP4MapViewerContextMenuProvider(EditPartViewer viewer, ActionRegistry registry) {
		super(viewer);
		if (registry == null) { throw new IllegalArgumentException("Expected to receive a not null ActionRegistry"); }
		actionRegistry = registry;
	}

	// - G E T T E R S / S E T T E R S
	private IAction getAction(String actionId) {
		return actionRegistry.getAction(actionId);
	}

	// - P U B L I C - S E C T I O N
	public void registerAction(IAction newAction) {
		// - Add to the registry my action to import the Turn data
		actionRegistry.registerAction(newAction);
	}

	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	/**
	 * Called when the context menu is about to show. Actions, whose state is enabled, will appear in the
	 * context menu. The current version used to get the actions may fail if they are not defined. Trap the
	 * exceptions and add code to do not add actions that are not found.
	 * 
	 * @see org.eclipse.gef.ContextMenuProvider#buildContextMenu(org.eclipse.jface.action.IMenuManager)
	 */
	@Override
	public void buildContextMenu(IMenuManager menu) {
		try {
			// - Add standard action groups to the menu
			GEFActionConstants.addStandardActionGroups(menu);

			// - Add actions to the menu
			IAction action = null;
			action = getAction(ActionFactory.UNDO.getId());
			if (null != action) menu.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
			action = getAction(ActionFactory.REDO.getId());
			if (null != action) menu.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
			action = getAction(ActionFactory.DELETE.getId());
			if (null != action) menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);

			// - Add additional context menu actions
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, getAction(IMPORT_TURN_DATA_ACTION));
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, getAction(ZOOMPLUS_ACTION));
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, getAction(ZOOMMINUS_ACTION));
			menu.appendToGroup(GEFActionConstants.GROUP_EDIT, getAction(ZOOMRESET_ACTION));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
