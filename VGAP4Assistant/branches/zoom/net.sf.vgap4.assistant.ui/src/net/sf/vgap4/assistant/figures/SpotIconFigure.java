//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotIconFigure extends AbstractIconFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");
	public static String	SHAPE_PLANET	= "SpotIconFigure.SHAPE_PLANET";
	public static String	SHAPE_SHIP		= "SpotIconFigure.SHAPE_SHIP";
	public static String	SHAPE_POD			= "SpotIconFigure.SHAPE_POD";
	public static String	SHAPE_WING		= "SpotIconFigure.SHAPE_WING";

	// - F I E L D - S E C T I O N ............................................................................
	private XYLayout			xylay					= new XYLayout();
	//	private AbstractIconFigure	baseIcon;
	//	private Image								decorationImage	= Activator.getImageDescriptor("icons/SpotDecorator.GIF").createImage();
	//	private ImageFigure					decorator				= new ImageFigure(decorationImage);
	private String				baseCode			= SpotIconFigure.SHAPE_PLANET;
	public boolean				baseActive		= false;
	public boolean				shipInOrbit		= false;
	public boolean				podInOrbit		= false;
	public boolean				hasNatives		= false;
	public boolean				wingsInOrbit	= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotIconFigure(AssistantNodeFigure homeFigure) {
		super(homeFigure);
		setDrawingSize(16);
		//		baseIcon = new BaseIconFigure(homeFigure);
		//		this.setLayoutManager(xylay);
		//		this.add(baseIcon);
		//		//		decorator = new ImageFigure(decorationImage);
		//		decorator.setOpaque(false);
		//		this.add(decorator);
		//		relayout();
		//		this.setSize(this.getPreferredSize());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	protected void paintFigure(final Graphics graphics) {
	//		// - Get drawing location. This should be already displaced from the top-left.
	//		final Rectangle bound = this.getBounds().getCopy();
	//
	//		// - Draw the figure body
	//		//DEBUG I have tried only the background color for painting
	//		//		graphics.setForegroundColor(ColorConstants.darkGray);
	//		graphics.setBackgroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
	//		//		graphics.fillOval(bound);
	//		graphics.fillRoundRectangle(bound, 3, 3);
	//	}

	//	private void relayout() {
	//		// - Position ICON. Top-left corner of the figure.
	//		final Dimension iconicSize = new Dimension(this.drawingSize, drawingSize);
	//		Rectangle elementLocation = new Rectangle();
	//		elementLocation.x = 0;
	//		elementLocation.y = 0;
	//		elementLocation.width = iconicSize.width;
	//		elementLocation.height = iconicSize.height;
	//		xylay.setConstraint(baseIcon, elementLocation);
	//
	//		// - Position DECORATOR. Bottom-left of the icon
	//		elementLocation = new Rectangle();
	//		elementLocation.x = 0;
	//		elementLocation.y = iconicSize.height - 9;
	//		elementLocation.width = 9;
	//		elementLocation.height = 9;
	//		xylay.setConstraint(decorator, elementLocation);
	//	}

	//	private void dispose() {
	//		this.decorationImage.dispose();
	//		this.decorator = null;
	//		this.baseIcon = null;
	//	}

	public void setBaseFigure(String baseCode) {
		this.baseCode = baseCode;
		//		this.baseIcon = baseIconFigure;
		//		setDrawingSize(baseIconFigure.drawingSize);
		//		this.add(baseIcon);
		//		this.add(decorator);
		repaint();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();

		//- Paint the base.
		if (SHAPE_PLANET.equals(this.baseCode)) {
			//- Draw the Planet circle
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).mainShapeColor);
			Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			graphics.fillOval(planetBounds);
		}
		if (SHAPE_SHIP.equals(this.baseCode)) {
			//- Draw the Ship shape.
			PointList triangle = new PointList(3);
			Point head = new Point(bound.x + this.drawingSize / 2, bound.y + 4);
			Point p2 = new Point(head.x + 8, head.y - 4);
			Point p3 = new Point(head.x + 8, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).mainShapeColor);
			graphics.fillPolygon(triangle);
		}

		//- Draw the Base decorator
		if (baseActive) {
			graphics.setForegroundColor(ColorConstants.lightGray);
			graphics.setBackgroundColor(ColorConstants.lightGray);
			graphics.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods.
		if (shipInOrbit) {
			//- Draw the Ship in Orbit decorator
			graphics.setBackgroundColor(((SpotFigure) this.getContainer()).decoratorColor);
			graphics.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			graphics.setBackgroundColor(AssistantConstants.COLOR_BRILLIANT_BLUE);
			graphics.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}

		if (hasNatives) {
			//- Draw the other decorators
			graphics.setBackgroundColor(ColorConstants.orange);
			graphics.fillOval(bound.x + 2, bound.y + 2, 4, 4);
		}
		if (false) {
			//- Draw the other decorators
			graphics.setBackgroundColor(AssistantConstants.COLOR_MEDIUM_YELLOW);
			graphics.fillOval(bound.x + 11, bound.y + 11, 4, 4);

			//- Draw the other decorators
			graphics.setBackgroundColor(AssistantConstants.COLOR_LIGHT_BLUE);
			graphics.fillOval(bound.x + 1, bound.y + 6, 4, 4);
		}
	}

	public Image convertToImage() {
		Image image = new Image(Display.getDefault(), 16, 16);
		GC gc = new GC(image);

		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = new Rectangle(0, 0, 16, 16);

		//- Paint the base.
		if (SpotIconFigure.SHAPE_PLANET.equals(this.baseCode)) {
			//- Draw the Planet circle
			gc.setBackground(((SpotFigure) this.getContainer()).mainShapeColor);
			Rectangle planetBounds = new Rectangle(bound.x + 4, bound.y + 4, 8, 8);
			gc.fillOval(planetBounds.x, planetBounds.y, planetBounds.width, planetBounds.height);
		}
		if (SHAPE_SHIP.equals(this.baseCode)) {
			//- Draw the Ship shape.
			PointList triangle = new PointList(3);
			Point head = new Point(bound.x + 3, bound.y + this.drawingSize / 2 - 1);
			Point p2 = new Point(head.x + 10, head.y - 4);
			Point p3 = new Point(head.x + 10, head.y + 4);
			triangle.addPoint(head);
			triangle.addPoint(p2);
			triangle.addPoint(p3);
			gc.setBackground(((SpotFigure) this.getContainer()).mainShapeColor);
			gc.fillPolygon(triangle.toIntArray());
		}

		//- Draw the Base decorator
		if (baseActive) {
			gc.setForeground(ColorConstants.lightGray);
			gc.setBackground(ColorConstants.lightGray);
			gc.drawOval(bound.x + 2, bound.y + 2, 11, 11);
		}

		//- Draw other decorators for ships in orbit or pods.
		if (shipInOrbit) {
			//- Draw the Ship in Orbit decorator
			gc.setBackground(((SpotFigure) this.getContainer()).decoratorColor);
			gc.fillOval(bound.x + 12, bound.y + 6, 4, 4);
		}
		if (podInOrbit) {
			gc.setBackground(AssistantConstants.COLOR_BRILLIANT_BLUE);
			gc.fillOval(bound.x + 6, bound.y + 12, 4, 4);
		}

		if (hasNatives) {
			//- Draw the other decorators
			gc.setBackground(ColorConstants.orange);
			gc.fillOval(bound.x + 2, bound.y + 2, 4, 4);
		}
		gc.dispose();
		return image;
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		return new Dimension(this.drawingSize, drawingSize);
	}
}
// - UNUSED CODE ............................................................................................
