//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.gef.EditPolicy;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

import net.sf.gef.core.model.Route;
import net.sf.vgap4.assistant.figures.AssistantNodeFigure;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.pages.ShipPropertyPage;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipEditPart extends AssistantNodeEditPart implements ISelectablePart {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public DiagramEditPart() {
	// }
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		super.propertyChange(evt);
		// if (Planet.LOCATION_PROP.equals(prop)) {
		// refreshVisuals();
		// return;
		// }
	}

	// - P R O T E C T E D - S E C T I O N
	private Ship getCastedModel() {
		return (Ship) getModel();
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	protected void createEditPolicies() {
		super.createEditPolicies();
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
	}

	@Override
	protected List<Route> getModelSourceConnections() {
		return getCastedModel().getSourceConnections();
	}

	@Override
	protected List<Route> getModelTargetConnections() {
		return getCastedModel().getTargetConnections();
	}

	/**
	 * Update the visual representation of the Planet (the Figure part) from the model data. In Planet units
	 * this represents all the visible attributes that can be presented on the Properties View.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final AssistantNodeFigure fig = (AssistantNodeFigure) getFigure();
		final Ship model = getCastedModel();

		// - Update figure visuals from current model data.
		fig.setCoordinates(model.getLocation());
		fig.setId(model.getIdNumber());
		fig.setName(model.getName());

		//- Get the game player code.
		int player = getPlayerCode();
		final int owner = model.getFieldNumber("owner");
		if (player != owner)
			fig.setColor(AssistantConstants.COLOR_SHIP_ENEMY);
		else
			fig.setColor(AssistantConstants.COLOR_SHIP_DEFAULT);

		super.refreshVisuals();
	}

	// - I N T E R F A C E - I S E L E C T A B L E P A R T
	public IPropertyPage createPropertyPage(Composite top, boolean singleSelected) {
		try {
			// - Create a new set of controls for this EditPart.
			final ShipPropertyPage page = new ShipPropertyPage(top);
			page.setModel((Ship) getModel());
			//		page.createContents((Ship) getModel());
			Point pageSize = page.getSize();
			return page;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}
// - UNUSED CODE ............................................................................................
