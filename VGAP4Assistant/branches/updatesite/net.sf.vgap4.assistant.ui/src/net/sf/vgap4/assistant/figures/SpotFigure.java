//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;

import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Image;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.factories.IconImageFactory;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.figures.draw2d.ZeroPaddingLabel;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotFigure extends SelectableFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger					= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	/** Drawing color for this element. This is the default color but may change depending on model data. */
	//private Color									color				= AssistantConstants.COLOR_SHIP_DEFAULT;
	/** The positionable layout that is used to draw any default Map element. */
	protected final XYLayout	xylay			= new XYLayout();
	/** Reference to the icon drawing class that represents this unit. */
	//protected AbstractIconFigure	iconic			= new AbstractIconFigure(this);
	protected Image						iconic;
	protected Label						iconLabel	= new StandardLabel();
	/** Label with the numerical serial identifier. This identifier is padded with zeroes to the left. */
	protected final Label			idLabel		= new ZeroPaddingLabel("000");
	/** Label with the unit name. */
	protected final Label			nameLabel	= new StandardLabel();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotFigure() {
		//- Initialize the layout and the contents.
		//		xylay = new XYLayout();
		setLayoutManager(xylay);

		//- Create a new image for the icon to represent this element.
		IconImageFactory imageFactory = new IconImageFactory();
		//- This line is the initial setup. This label will be replaced by another label.
		this.add(iconLabel);
		setIconImage(imageFactory.generateImage());
		//		iconLabel = new StandardLabel(iconic);
		//		iconLabel.setIconAlignment(PositionConstants.LEFT);
		//		iconLabel.setIconTextGap(0);
		//		iconLabel.setSize(AssistantConstants.SIZE_ICONIMAGE, AssistantConstants.SIZE_ICONIMAGE);

		//		this.add(iconLabel);
		this.add(idLabel);
		this.add(nameLabel);
		this.relayout();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Point getHotSpot() {
		//	final Point offset = iconic.getHotSpot();
		return new Point(AssistantConstants.SIZE_ICONIMAGE + AssistantNodeFigure.BOUNDARIES,
				AssistantConstants.SIZE_ICONIMAGE + AssistantNodeFigure.BOUNDARIES);
	}

	/**
	 * Before setting the top-left corner location we have to adjust the element by the Hot Spot displacement
	 * because the model coordinates are the coordinates for the planet center. The planet image surrounds that
	 * point.
	 */
	public void setCoordinates(final Point location) {
		final Point offset = this.getHotSpot();
		final Point coords = location;
		coords.x -= offset.x;
		coords.y -= offset.y;
		this.setLocation(coords);
	}

	public Image getIconImage() {
		return this.iconic;
	}

	public void setIconImage(Image newIcon) {
		this.remove(iconLabel);
		iconic = newIcon;
		iconLabel = new StandardLabel();
		iconLabel.setIcon(iconic);
		iconLabel.setText("");
		iconLabel.setIconAlignment(PositionConstants.LEFT);
		iconLabel.setIconTextGap(0);
		iconLabel.setSize(iconLabel.getPreferredSize());
		this.add(iconLabel);
		relayout();
	}

	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed
	 * is represented as a numeric label with the speed in knots.
	 */
	public void setId(final int newId) {
		try {
			final NumberFormat nf = NumberFormat.getIntegerInstance();
			nf.setMinimumIntegerDigits(3);
			nf.setMaximumFractionDigits(0);
			idLabel.setText(nf.format(newId));

		} catch (final Exception e) {
			idLabel.setText(new Integer(newId).toString());
		}
		this.relayout();
	}

	public void setName(final String newName) {
		nameLabel.setText(newName);
		this.relayout();
	}

	protected void relayout() {
		// - Position ICON. Top-left corner of the figure.
		final Dimension iconicSize = iconLabel.getSize();
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = AssistantNodeFigure.BOUNDARIES;
		elementLocation.y = AssistantNodeFigure.BOUNDARIES;
		elementLocation.width = iconicSize.width + AssistantNodeFigure.MARGIN;
		elementLocation.height = iconicSize.height;
		xylay.setConstraint(iconLabel, elementLocation);

		//- Position ID. At the right of the icon.
		elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width + AssistantNodeFigure.MARGIN;
		elementLocation.y = AssistantNodeFigure.BOUNDARIES - 1; //Move up by 1 pixel
		elementLocation.width = idLabel.getPreferredSize().width;
		elementLocation.height = idLabel.getPreferredSize().height + AssistantNodeFigure.MARGIN;
		xylay.setConstraint(idLabel, elementLocation);

		//- Position NAME. Below the idLabel.
		elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width + AssistantNodeFigure.MARGIN;
		elementLocation.y = idLabel.getPreferredSize().height + AssistantNodeFigure.MARGIN - 3;// Move up 3 pixels
		elementLocation.width = nameLabel.getPreferredSize().width;
		elementLocation.height = nameLabel.getPreferredSize().height;
		xylay.setConstraint(nameLabel, elementLocation);

		//- Update the layout and the global size.
		this.layout();
		this.repaint();
		//DEBUG Check if this line is really needed.
		this.setSize(this.getPreferredSize());
	}

	protected String decodeSelectionCode(final int selected) {
		if (EditPart.SELECTED_NONE == selected) return "SELECTED_NONE";
		if (EditPart.SELECTED == selected) return "SELECTED";
		if (EditPart.SELECTED_PRIMARY == selected) return "SELECTED_PRIMARY";
		return "UNDEFINED";
	}
}
// - UNUSED CODE ............................................................................................
