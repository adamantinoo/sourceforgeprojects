//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.logging.Logger;


import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.model.Route;
import net.sf.gef.core.parts.AbstractNodeEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.projecteditor.model.PositionableUnit;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantNodeEditPart extends AbstractNodeEditPart {
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S
	// protected ConnectionAnchor anchor;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public VGAP4NodeEditPart() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (PositionableUnit.PROP_LOCATION.equals(prop)) {
			refreshVisuals();
			return;
		}
		// TODO Activate property changes for connections
		// if (Shape.SOURCE_CONNECTIONS_PROP.equals(prop)) {
		// refreshSourceConnections();
		// } if (Shape.TARGET_CONNECTIONS_PROP.equals(prop)) {
		// refreshTargetConnections();
		// }
	}

	// - P R O T E C T E D - S E C T I O N
	private AssistantNode getCastedModel() {
		return (AssistantNode) getModel();
	}

	@Override
	protected List<Route> getModelSourceConnections() {
		return getCastedModel().getSourceConnections();
	}

	@Override
	protected List<Route> getModelTargetConnections() {
		return getCastedModel().getTargetConnections();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final Figure fig = (Figure) getFigure();

		final Dimension size = fig.getSize();
		Dimension prefSize = fig.getPreferredSize();
		//		logger.info("Calculated figure size: " + size);
		Point location = getCastedModel().getLocation();

		//		//- Calculate real map location depending on the zoom factor.
		//		int zoom = getZoomFactor();
		//		if (zoom > 0) {
		//			location.x = location.x * zoom;
		//			location.y = location.y * zoom;
		//		}

		//- Change location to the neutral coordinates.
		//		location.x = 3000 - location.x;
		//		location.y = 3000 - location.y;
		final Rectangle bounds = new Rectangle(location, prefSize);
		//		fig.setBounds(bounds);
		//		fig.setLocation(location);
		//		fig.repaint();
		//		fig.revalidate();
		//		fig.validate();
		//		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bounds);
		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), bounds);
		super.refreshVisuals();
	}

	protected int getZoomFactor() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getZoomFactor();
	}

	protected int getPlayerCode() {
		final AssistantMap parentModel = (AssistantMap) this.getParent().getModel();
		return parentModel.getPlayer();
	}

	protected Planet getOnPlanet(int planetId) {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getPlanet4Id(planetId);
	}
}

// - UNUSED CODE ............................................................................................
