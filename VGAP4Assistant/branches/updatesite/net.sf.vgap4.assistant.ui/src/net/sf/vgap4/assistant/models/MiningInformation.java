//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class MiningInformation implements Serializable {
	private static final long		serialVersionUID	= -1004029385914316762L;
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public MiningInformation() {
	// }
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// }
	// - CLASS IMPLEMENTATION
	// ...................................................................................
	/**
	 * This class reads and processes the mineral information that is found on the list of fields receives as
	 * the parameter. The data is extracted and composed into the four types of minerals. Information recovered
	 * is the quantities of ore or mineral that can be found on the planet, mined or not mined.
	 */

	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	NEUTRONIUM_NAME		= "MiningInformation.NEUTRONIUM_NAME";
	public static final String	DURANIUM_NAME			= "MiningInformation.DURANIUM_NAME";
	public static final String	TRITANIUM_NAME		= "MiningInformation.TRITANIUM_NAME";
	public static final String	MOLYBDENUM_NAME		= "MiningInformation.MOLYBDENUM_NAME";
	public static final int			NEUTRONIUM_ID			= 0;
	public static final int			DURANIUM_ID				= 1;
	public static final int			TRITANIUM_ID			= 2;
	public static final int			MOLYBDENUM_ID			= 3;

	// - F I E L D - S E C T I O N ............................................................................
	private Vector<Mineral>			minerals					= new Vector<Mineral>(1);
	{
		this.minerals.setSize(4);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * Convert data in the input line into the corresponding fields of this structure. Mineral information
	 * includes all VGAP information related to mining even this may appear at other structures.<br>
	 * Contained data are:
	 * <ul>
	 * <li>mineral extraction percentages</li>
	 * <li>ore and mineral contents at the planet surface</li>
	 * </ul>
	 */
	public MiningInformation(String[] planetFields) {
		// - Create an entry for every mineral type on the input data
		// try {
		minerals.setElementAt(new Mineral(NEUTRONIUM_NAME, planetFields[23], planetFields[27], planetFields[31],
				planetFields[35]), NEUTRONIUM_ID);
		minerals.setElementAt(new Mineral(DURANIUM_NAME, planetFields[24], planetFields[28], planetFields[32],
				planetFields[36]), DURANIUM_ID);
		minerals.setElementAt(new Mineral(TRITANIUM_NAME, planetFields[25], planetFields[29], planetFields[33],
				planetFields[37]), TRITANIUM_ID);
		minerals.setElementAt(new Mineral(MOLYBDENUM_NAME, planetFields[26], planetFields[30], planetFields[34],
				planetFields[38]), MOLYBDENUM_ID);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	public MiningInformation(String[] planetFields, int startDataIndex) {
		int oreStartIndex = startDataIndex;
		int mineralStartIndex = startDataIndex + 4;
		minerals.setElementAt(new Mineral(NEUTRONIUM_NAME, planetFields[oreStartIndex], planetFields[mineralStartIndex]),
				NEUTRONIUM_ID);
		minerals.setElementAt(new Mineral(DURANIUM_NAME, planetFields[oreStartIndex + 1],
				planetFields[mineralStartIndex + 1]), DURANIUM_ID);
		minerals.setElementAt(new Mineral(TRITANIUM_NAME, planetFields[oreStartIndex + 2],
				planetFields[mineralStartIndex + 2]), TRITANIUM_ID);
		minerals.setElementAt(new Mineral(MOLYBDENUM_NAME, planetFields[oreStartIndex + 3],
				planetFields[mineralStartIndex + 3]), MOLYBDENUM_ID);
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Get the mining information for all the minerals at once. This simplifies the interface because all this
	 * information is transferred with a single command. Destination will then understand the structure and
	 * extract the right information from the structure.
	 */
	public Vector<Mineral> getMiningInformation() {
		return this.minerals;
	}

}

// - CLASS IMPLEMENTATION ...................................................................................
class Mineral extends DataFormatter implements Serializable {
	private static final long	serialVersionUID	= -933224270178425247L;
	// - F I E L D - S E C T I O N ............................................................................
	private final String			name;
	private int								density						= 0;
	private int								surfaceOre				= 0;
	private int								surfaceMineral		= 0;
	private int								unminned					= 0;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * The constructor initializes the structure with the content data that is received in the parameters. The
	 * structure records the name of the mineral and the four quantities that describe the mineral contents on a
	 * <code>Planet</code>.
	 */
	public Mineral(String mineralName, String mineralDensity, String ore, String ele, String reserve) {
		// - Create a mineral structure for each group of data.
		name = mineralName;
		this.density = convert2Integer(mineralDensity.trim());
		surfaceOre = convert2Integer(ore.trim());
		surfaceMineral = convert2Integer(ele.trim());
		unminned = convert2Integer(reserve.trim());
	}

	public Mineral(String mineralName, String ore, String ele) {
		// - Create a mineral structure for each group of data.
		name = mineralName;
		this.density = 0;
		surfaceOre = convert2Integer(ore.trim());
		surfaceMineral = convert2Integer(ele.trim());
		unminned = 0;
	}

	// - G E T T E R S / S E T T E R S
	public String getName() {
		return name;
	}

	public int getDensity() {
		return density;
	}

	public int getSurfaceOre() {
		return surfaceOre;
	}

	public int getSurfaceMineral() {
		return surfaceMineral;
	}

	public int getUnminned() {
		return unminned;
	}

	public int getTotalOre() {
		return this.surfaceOre + this.unminned;
	}
}

class DataFormatter implements Serializable {
	private static final long	serialVersionUID	= 6877368012779715581L;

	protected int convert2Integer(String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}
}

// - UNUSED CODE ............................................................................................
