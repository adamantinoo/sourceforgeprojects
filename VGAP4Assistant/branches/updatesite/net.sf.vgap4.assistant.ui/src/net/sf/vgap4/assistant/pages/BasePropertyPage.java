//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.pages;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Logger;


import org.eclipse.core.runtime.Assert;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import net.sf.gef.core.pages.AbstractPropertyPage;
import net.sf.vgap4.assistant.figures.DetailedBaseFigure;
import net.sf.vgap4.assistant.models.Base;

// - CLASS IMPLEMENTATION ...................................................................................
public class BasePropertyPage extends AbstractPropertyPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.pages");

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the model. This reference allows complete access to the exported model fields. */
	private Base					baseModel;
	private Canvas				canvas;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the interface
	 * view at runtime. Other fields may be <code>null</code> and in those situation the code will generate a
	 * informational message.
	 */
	public BasePropertyPage(Composite top) {
		super(top);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This method builds up the graphical elements that make the interface between the SWT
	 * <code>container</code> and the more manegeable draw2d <code>Figure</code> graphical interfaces. This
	 * will help to migrate from classical views to GEF controlled editor and use the same detailed figures to
	 * represent object data.
	 */
	/*
	 * Add all the common elements that are visible for a Planet. This depends on the information available on
	 * the model and the information filters set. This current implementation does not show all available
	 * information nor has any active filter.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#createContents()
	 */
	@Override
	protected void createContents() {
		page = new Group(container, SWT.NONE);
		canvas = new Canvas(page, SWT.NONE);
		final LightweightSystem lws = new LightweightSystem(canvas);
		final DetailedBaseFigure base = new DetailedBaseFigure(this.getModel());
		base.createContents();
		lws.setContents(base);
		final Dimension baseSize = base.getPreferredSize();
		canvas.setSize(baseSize.width, baseSize.height);
		canvas.setLocation(2, 2);
		final Point pageSize = canvas.getSize();
		page.setSize(pageSize.x + 4, pageSize.y + 10);
	}

	public Canvas getContainer() {
		return canvas;
	}

	/**
	 * Get the associated model element. At this class this model will be sub-classed to the <code>Planet</code>
	 * interface. If the model is null then we should throw an exception to signal some internal code error.
	 */
	@Override
	public Base getModel() {
		Assert.isNotNull(baseModel, "The page unit model instance is not set up but it is being accessed.");
		return baseModel;
	}

	/**
	 * Store a reference to the model inside the page. Check also if a value existed previously to call the
	 * notification registration mechanism so this instance will receive any property modification to the model
	 * reference.
	 */
	public void setModel(final Base model) {
		super.setModel(model);
		baseModel = model;
		// - Build up the page once the model is valid and the page can be generated.
		this.createContents();
	}

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * Intercept changes to the model properties. Changes supported by this class are only in the next list of
	 * properties:
	 * <ul>
	 * <li><b>Description</b> - user description for the Planet.</li>
	 * </ul>
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		// TODO Create a new property for the common Node notes
		// if (Planet.NOTES_PROP.equals(prop)) {
		// page.setText(getModel().getName());
		// }
	}

	//[01]
}
// - UNUSED CODE ............................................................................................
