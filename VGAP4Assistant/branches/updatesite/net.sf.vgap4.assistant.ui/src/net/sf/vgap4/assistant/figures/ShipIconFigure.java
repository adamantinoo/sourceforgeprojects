//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.Triangle;
import org.eclipse.swt.graphics.Color;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipIconFigure extends AbstractIconFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private Triangle			arrow		= new Triangle();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ShipIconFigure(AssistantNodeFigure homeFigure) {
		super(homeFigure);
		setDrawingSize(11);
		//		container = parentFigure;

		//- Create the graphic structure. Do not use a layout and calculate sizes in our methods.
		this.arrow.setDirection(PositionConstants.NORTH);
		this.arrow.setFill(true);
		this.arrow.setForegroundColor(AssistantConstants.COLOR_SHIP_DEFAULT);
		this.arrow.setBackgroundColor(AssistantConstants.COLOR_SHIP_DEFAULT);
		this.arrow.setSize(drawingSize, drawingSize + drawingSize / 2);
		this.add(this.arrow);
		this.setSize(this.getPreferredSize());
	}

	public void setColor(Color color) {
		this.arrow.setForegroundColor(color);
		this.arrow.setBackgroundColor(color);
		logger.info("Changing ship color to:" + color.toString());
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// TODO Auto-generated method stub
		super.paintFigure(graphics);
	}
	//[01]
}

// - UNUSED CODE ............................................................................................
//[01]
//public ShipFigure getContainer() {
//return container;
//}
//public Color getColor() {
//return color;
//}

//protected boolean isSelected() {
//return container.isSelected();
//}

//@Override
//protected void paintFigure(final Graphics graphics) {
//// - Get drawing location. This should be already displaced from the top-left.
//Rectangle bound = getBounds().getCopy();
//// bound.width -= 1;
//// bound.height -= 1;
//
//// - Draw the figure body
////graphics.setForegroundColor(ColorConstants.darkGray);
//graphics.setBackgroundColor(ColorConstants.darkGray);
//// graphics.setLineWidth(2);
//graphics.fill
//// graphics.setLineWidth(1);
//// if (isSelected()) drawHandles(graphics);
//
//// // - Draw the figure center
//// bound = getBounds().getCopy();
//// final Point hotspot = getHotSpot();
//// bound.x += hotspot.x + 1;
//// bound.y += hotspot.y + 1;
//// final Point endPoint = new Point(bound.x + 1, bound.y + 1);
//// graphics.setLineWidth(1);
//// graphics.setForegroundColor(getColor());
//// graphics.drawLine(new Point(bound.x, bound.y), endPoint);
//}
