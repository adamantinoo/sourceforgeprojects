//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipFigure extends AssistantNodeFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	//	protected static final int	BOUNDARIES		= 1;
	//
	//	protected static final int	MARGIN				= 1;
	//	protected static final int	LABEL_HEIGHT	= 15;
	//	protected static final int	GAP4					= -4;
	//	protected static final int	GAP5					= 5;
	//
	//	protected static final int	CHAR_WIDTH		= 6;
	//	protected static final int	CHAR_HEIGHT		= 12;
	//	protected static final int	GAP2					= 2;
	//	protected static final int	GAP3					= 3;
	//	protected static final int	LABEL_WIDTH		= (4 + 1) * 6 + GAP2;

	// - F I E L D - S E C T I O N ............................................................................
	//	/** Reference to the icon drawing class that represents this unit. */
	//	protected ShipIconFigure	iconic	= new ShipIconFigure(this);

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ShipFigure() {
		//- Add the icon to represent this element.
		this.setDrawFigure(new ShipIconFigure(this));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("ShipFigure[");
		buffer.append(this.getLocation());
		buffer.append(idLabel.getText()).append("-");
		buffer.append(nameLabel.getText()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	public void setColor(final Color newColor) {
		super.setColor(newColor);
		((ShipIconFigure) this.iconic).setColor(newColor);
		this.repaint();
	}
}
// - UNUSED CODE ............................................................................................
