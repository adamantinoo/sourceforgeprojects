//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;

import net.sf.gef.core.factories.IFigureFactory;
import net.sf.vgap4.assistant.figures.SpotFigure;
import net.sf.vgap4.assistant.models.Spot;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantFigureFactory implements IFigureFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.figures");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public PolylineConnection createConnection(final IWireModel newWire) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Return a IFigure depending on the instance of the current model element. This allows this EditPart to be
	 * used for both subclasses of Shape.
	 */
	public Figure createFigure(final EditPart part, final IGEFModel unit) {
		//		if (unit instanceof Sector) return new SectorFigure();
		if (unit instanceof Spot) return new SpotFigure();
		//		if (unit instanceof Planet) return new PlanetFigure();
		//		if (unit instanceof Base) return new BaseFigure();
		//		if (unit instanceof Ship) return new ShipFigure();
		if (true) //- If Shapes gets extended the conditions above must be updated
			throw new IllegalArgumentException();
		return null;
	}

	public Figure createFigure(final EditPart part, final IGEFModel unit, final String subType) {
		return this.createFigure(part, unit);
	}
}
// - UNUSED CODE ............................................................................................
