//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseInformation extends AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= 8806434019146603117L;
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	/** Unique identifier of the Planet where this base is build. */
	private int								planetId					= -1;
	/**
	 * Stores the numbers of all the natives that are found residing on the base. This numbers are diffrent form
	 * the Natives that are still found on the Planet.
	 */
	private Natives						nativeInformation	= null;
	/** Structure with all the mining information available on this planet's turn data. */
	private MiningInformation	miningInfo;

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Get the mining information for all the minerals at once. This simplifies the interface because all this
	 * information is transferred with a single command. Destination will then understand the structure and
	 * extract the right information from the structure.
	 */
	public Vector<Mineral> getMiningInformation() {
		return miningInfo.getMiningInformation();
	}

	public Natives getNativesInfo() {
		return nativeInformation;
	}

	public int getPlanetId() {
		return planetId;
	}

	/**
	 * Loads the contents on the source fields into the corresponding attributes of this instance, processing
	 * them as required to convert string to number if necessary.
	 */
	public void loadData(final String[] fieldNames, final String[] fields) {
		//- Read the field name/field value pairs from these arrays and store it inside the property list.
		for (int i = 1; i < fieldNames.length; i++) {
			//- Check for null values and empty strings.
			final String fieldName = fieldNames[i].trim();
			final String value = fields[i].trim();
			//			if ((null != value) && ("" != value))
			info.setProperty(fieldName, value);
		}
		// - Store the data fields in the cache attributes.
		idNumber = this.convert2Integer(fields[3].trim());
		planetId = this.convert2Integer(fields[4].trim());
		name = this.getField("Name");
		final int x = this.convert2Integer(fields[407].trim());
		final int y = this.convert2Integer(fields[408].trim());
		location = new Point(x, 3000 - y);
		nativeInformation = new Natives(fields, 6);
		miningInfo = new MiningInformation(fields, 17);
	}
}

// - UNUSED CODE ............................................................................................
