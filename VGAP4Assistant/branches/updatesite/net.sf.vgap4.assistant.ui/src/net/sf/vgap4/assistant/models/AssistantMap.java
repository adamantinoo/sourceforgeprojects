//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;


import org.eclipse.draw2d.geometry.Point;

import net.sf.gef.core.model.AbstractPropertyChanger;
import net.sf.vgap4.projecteditor.model.Sector;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantMap extends AbstractPropertyChanger {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - M O D E L F I E L D - I D E N T I F I E R S
	/** Property ID to use when new model data is added to the diagram. */
	public static final String								DATA_ADDED_PROP					= "AssistantMap.DATA_ADDED_PROP";
	/** Property ID to use when a child is added to this diagram. */
	public static final String								CHILD_ADDED_PROP				= "AssistantMap.CHILD_ADDED_PROP";
	/** Property ID to use when a child is removed from this diagram. */
	public static final String								CHANGE_ZOOMFACTOR				= "AssistantMap.CHANGE_ZOOMFACTOR";
	/** Property ID to use when a the number referencing any of the last turns gets changed. */
	public static final String								LAST_TURN_CHANGED_PROP	= "AssistantMap.LAST_TURN_CHANGED_PROP";
	private static final long									serialVersionUID				= 3361707471258570866L;
	private static Logger											logger									= Logger.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	// - M O D E L F I E L D S
	/** Number of the last turn processed. */
	private int																lastReadTurn						= -1;
	/** Number of the higher turn processed. */
	private int																lastAvailableTurn				= -1;
	/** Number the identifies a unique game code. */
	private String														gameId;
	/** Name given to this game when created. */
	private String														gameName;
	/**
	 * This value is an estimation. Y can not get such information form the .CSV files data. It should be a
	 * configuration parameter editable though the Application Properties.
	 */
	private final int													mapSize									= 3000;
	/**
	 * Player number that matches the slot number on the game. This number may correspond to another user inside
	 * other games so it is used with the 'gameId' to uniquely identify a player slot on a game set.
	 */
	private int																playerNo								= -1;
	/**
	 * Sectors are virtual structures that keep lists of grouped elements (generally by geographic location).
	 * This field stores the references to all the sectors currently defined on this Map. This is a simple list
	 * structure and currently sectors do not have any sorting order or organization. A sector may contain other
	 * sectors to any level because it simply defines a physical rectangular map area.
	 */
	private final Vector<Sector>							sectors									= new Vector<Sector>();
	/**
	 * Array structure with references to all the Planets that have been identified on this turn or on other
	 * turns. This structure is implemented as an array because it matches the original implementation from CSV
	 * files. Probably a <code>Hashtable</code> will generate less memory usage, over all in very long games
	 * where the Id of some elements may grew high. Planets are the exception because there is a finite number
	 * of them and alwais get the lowest element Id.
	 */
	private final Hashtable<Integer, Planet>	planets									= new Hashtable<Integer, Planet>();
	/**
	 * This structure contains a reference to all player bases. This structure clearly benefits from the
	 * 'Hashtable' implementation because the number of bases for this player is small in relation to the number
	 * of planets and the number of other elements created in the game.
	 */
	private final Hashtable<Integer, Base>		bases										= new Hashtable<Integer, Base>();
	/**
	 * Another array structure that contains all the identified Ships. This list contains all ships owned by
	 * this player and also the ships detected to other players. That other ships are identified by being the
	 * 'owner' a different code than the game 'playerId'.
	 */
	private final Hashtable<Integer, Ship>		ships										= new Hashtable<Integer, Ship>();
	/**
	 * This structure stores the map elements but clustered by the location where they are presented. This will
	 * simplify the rendering of multiple elements at the same place reducing map data overlaying.<br>
	 * The structure is updated when the model suffers an structural change, like when new turn data is read or
	 * in the future when any user element is added to the model.
	 */
	//	private final LocationRegistry						locationRegistry				= new LocationRegistry();
	private SpotRegistry											spots										= new SpotRegistry();
	private int																zoomFactor							= 1;
	/**
	 * The detected maximum of the sum of all available minerals for all known planets. This value is used to
	 * calculate the normal deviation and to classify the Planets accordingly to their mineral reserves.
	 */
	private long															maxTotalMinerals				= 0;

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addBaseInformation(final int turn, final BaseInformation baseInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Base theBase = this.getBase4Id(baseInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theBase.addTurnInformation(turn, baseInfo);
		//- Register this planet in the location registry.
		//		locationRegistry.register(baseInfo.getLocation(), theBase);
	}

	/**
	 * Add the new turn information to the Planet structure. We have not to check that the owner and game
	 * matches because that test have been performed before arriving this point. But some check has to be
	 * performed to identify when we have not located the correct Planet. The variable 'thePlanet' may not be
	 * allowed to be NULL because we have a reference on it.
	 */
	public void addPlanetInformation(final int turn, final PlanetInformation planetInfo) {
		// - Get the Planet structure that matched to the Planet ID.
		final Planet thePlanet = this.getPlanet4Id(planetInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		thePlanet.addTurnInformation(turn, planetInfo);

		//- Register this planet in the location registry.
		//		locationRegistry.register(planetInfo.getLocation(), thePlanet);
	}

	public void addShipInformation(final int turn, final ShipInformation shipInfo) {
		// - Check for the turn identification and the planet ID to locate an older version.
		final Ship theShip = this.getShip4Id(shipInfo.getIdNumber());
		// - Add this turn information to the planet array of turn data.
		theShip.addTurnInformation(turn, shipInfo);
		//- Register this planet in the location registry.
		//		locationRegistry.register(shipInfo.getLocation(), theShip);
	}

	//	public void clearLocationRegistry() {
	//		locationRegistry.clear();
	//	}
	public SpotRegistry getSpotRegistry() {
		return this.spots;
	}

	@Override
	public void fireStructureChange(final String property, final Object dataSet, final Object change) {
		spots.clear();

		//- Update the location clustering to be used when the EditPart request the list of children.
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final Base element = btp.next();
			spots.register(element.getLocation().getCopy(), element);
		}
		final Iterator<Planet> itp = planets.values().iterator();
		while (itp.hasNext()) {
			final Planet element = itp.next();
			spots.register(element.getLocation().getCopy(), element);
		}
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship element = its.next();
			spots.register(element.getLocation().getCopy(), element);
		}
		logger.info("Generating a new set of spots");

		//- Calculate classification
		calculatePlanetClassification();

		super.fireStructureChange(property, dataSet, change);
	}

	private void calculatePlanetClassification() {
		//- Get correlation data for classification.
		//- Calculate the median.
		int sumMinerals = 0;
		int minMinerals = Integer.MAX_VALUE;
		int maxMinerals = -1;
		int planetCounter = 0;

		//		int total = 0;
		Iterator<Planet> pit = this.planets.values().iterator();
		while (pit.hasNext()) {
			Planet thePlanet = pit.next();
			int total = thePlanet.getTotalOre();
			if (total > 0) {
				sumMinerals += total;
				minMinerals = Math.min(minMinerals, total);
				maxMinerals = Math.max(maxMinerals, total);
				planetCounter++;
			}
		}
		double d1 = 1.0 * maxMinerals - minMinerals * 1.0;

		//- Calculate normalized data.
		double nxsum = 0;
		pit = this.planets.values().iterator();
		while (pit.hasNext()) {
			Planet thePlanet = pit.next();
			int x = thePlanet.getTotalOre();
			if (x > 0) {
				double nx = (x - minMinerals) / d1;
				nxsum += nx;
			}
		}
		double mu = nxsum / planetCounter;

		//- Calculate range limits.
		double minxmu2 = Double.MAX_VALUE;
		double maxxmu2 = -1.0;
		pit = this.planets.values().iterator();
		while (pit.hasNext()) {
			Planet thePlanet = pit.next();
			int x = thePlanet.getTotalOre();
			if (x > 0) {
				double nx = (x - minMinerals) / d1;
				double xmu2 = Math.pow(nx - mu, 2.0);
				minxmu2 = Math.min(minxmu2, xmu2);
				maxxmu2 = Math.max(maxxmu2, xmu2);
			}
		}
		double rangeup = .8 * (maxxmu2 - minxmu2);
		double rangedown = .2 * (maxxmu2 - minxmu2);

		//- Normalize value and calculate (N(x) - mu)^2 and classification
		pit = this.planets.values().iterator();
		while (pit.hasNext()) {
			Planet thePlanet = pit.next();
			thePlanet.setPlanetClassification("C");
			int x = thePlanet.getTotalOre();
			if (x > 0) {
				thePlanet.setPlanetClassification("B");
				double nx = (x - minMinerals) / d1;
				double xmu2 = Math.pow(nx - mu, 2.0);
				if (xmu2 > rangeup) thePlanet.setPlanetClassification("A");
				if (xmu2 < rangedown) thePlanet.setPlanetClassification("C");
			}
		}
	}

	/**
	 * Return a List of all the visible elements in the diagram. Visible elements are the Planets, the Bases and
	 * the Ships. The returned List should not be modified.<br>
	 */
	public Vector<Object> getChildren() {
		final Vector<Object> childs = new Vector<Object>();
		// - Add only objects that are not null
		final Iterator<Sector> itsec = sectors.iterator();
		while (itsec.hasNext()) {
			final Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}
		final Iterator<Base> btp = bases.values().iterator();
		while (btp.hasNext()) {
			final AssistantNode base = btp.next();
			childs.add(base);
		}
		final Iterator<Planet> itp = planets.values().iterator();
		while (itp.hasNext()) {
			final Planet planet = itp.next();
			if (null != planet) {
				//TODO - Add only the planets that do not have a base.
				if (!planet.isHasBase()) childs.add(planet);
			}
		}
		final Iterator<Ship> its = ships.values().iterator();
		while (its.hasNext()) {
			final Ship ship = its.next();
			if (null != ship) childs.add(ship);
		}
		return getChildrenNew();
	}

	/**
	 * Return a List of all the visible elements in the Map. In this method the object that lie in the same
	 * location are clustered together inside a <code>Spot</code> structure that will reduce Map presentation
	 * and will clean up the Map presentation.
	 * 
	 * @return the list of visible Map objects for construction of the EditPart list.
	 */
	public Vector<Object> getChildrenNew() {
		final Vector<Object> childs = new Vector<Object>();

		final Iterator<Sector> itsec = sectors.iterator();
		while (itsec.hasNext()) {
			final Sector sector = itsec.next();
			if (null != sector) childs.add(sector);
		}

		//- Scan the locations and generate the list of visible objects.
		final Enumeration<Point> lit = spots.getLocations();
		while (lit.hasMoreElements()) {
			childs.add(spots.getSpot(lit.nextElement()));
			//			final Point location = lit.nextElement();
			//			final LocationCluster objects = spots.getSpot(location);
			//			if (objects.hasContents()) {
			//				//				if (objects.isMultiple())
			//				childs.add(new Spot(objects));
			//				//				else
			//				//					childs.add(objects.getRepresentative());
			//			}
		}
		return childs;
	}

	public String getGameId() {
		return gameId;
	}

	/**
	 * Check for the array size and the array contents. The array starts empty so any try to access any null
	 * element will trigger an exception. INtercept the exceptions and resize the array adecuately to the
	 * current usage.
	 */
	public Planet getPlanet4Id(final int id) {
		final Planet thePlanet = planets.get(new Integer(id));
		if (null == thePlanet) {
			planets.put(new Integer(id), new Planet(this));
			return planets.get(new Integer(id));
		} else
			return thePlanet;
	}

	public int getPlayer() {
		return playerNo;
	}

	public int getReadTurn() {
		return lastReadTurn;
	}

	// - P U B L I C - S E C T I O N
	public boolean isInitialized() {
		if ((-1 == lastReadTurn) && (null == gameId))
			return false;
		else
			return true;
	}

	//	/**
	//	 * Remove a Planet from the model. This will happen when the user overrides the interface and chooses to
	//	 * remove some model and history data.
	//	 * 
	//	 * @param obsoleteUnit
	//	 *          a non-null Planet instance;
	//	 */
	//	public void removeChild(final PositionableUnit obsoleteUnit) {
	//		if (null != obsoleteUnit) {
	//			planets.remove(obsoleteUnit);
	//			ships.remove(obsoleteUnit);
	//			this.firePropertyChange(AssistantMap.CHANGE_ZOOMFACTOR, null, obsoleteUnit);
	//		}
	//	}

	public void setGameId(final String gameId) {
		this.gameId = gameId;
	}

	public void setGameName(final String gameName) {
		this.gameName = gameName;
	}

	public void setPlayer(final int playerNo) {
		this.playerNo = playerNo;
	}

	public void setReadTurn(final int readTurn) {
		lastReadTurn = readTurn;
		AssistantMap.logger.info("Setting latReadTurn to " + readTurn);
		lastAvailableTurn = Math.max(lastAvailableTurn, lastReadTurn);
		AssistantMap.logger.info("Setting lastAvailableTurn to " + readTurn);
	}

	public Base getBase4Id(final int id) {
		// - Get the element we are searching for. If exception the resize the array and return a new structure.
		//		try {
		final Base theBase = bases.get(new Integer(id));
		if (null == theBase) {
			//- The base is not located because this is a new element.
			bases.put(new Integer(id), new Base(this));
			return getBase4Id(id);
		} else
			return theBase;
		//		} catch (final Exception aioobe) {
		//			aioobe.printStackTrace();
		//			bases.put(new Integer(id), new Base(this));
		//			return bases.get(new Integer(id));
		//			//					// - The base array has not any previous data. Resize the vector to the proper size.
		//			//					this.bases.setSize(id + 1);
		//			//					this.bases.setElementAt(new Base(), id);
		//			//					return this.bases.get(id);
		//		}
	}

	public Ship getShip4Id(final int id) {
		final Ship theShip = ships.get(new Integer(id));
		if (null == theShip) {
			ships.put(new Integer(id), new Ship(this));
			return ships.get(new Integer(id));
		} else
			return theShip;
	}

	//	protected void addPlanet(final Planet newPlanet) {
	//		if (null != newPlanet) {
	//			planets.add(newPlanet);
	//			this.firePropertyChange(AssistantMap.CHILD_ADDED_PROP, null, newPlanet);
	//		}
	//	}
	//
	//	protected void addShip(final Ship newShip) {
	//		if (null != newShip) {
	//			ships.add(newShip);
	//			this.firePropertyChange(AssistantMap.CHILD_ADDED_PROP, null, newShip);
	//		}
	//	}

	public void zoomPlus() {
		this.zoomFactor++;
		fireStructureChange(CHANGE_ZOOMFACTOR, null, null);
		//		ZoomManager
	}

	public void zoomMinus() {
		this.zoomFactor--;
		fireStructureChange(CHANGE_ZOOMFACTOR, null, null);
	}

	public int getZoomFactor() {
		return this.zoomFactor;
	}

	public String dump() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[AssistantMap:");
		buffer.append("gameId=").append(this.gameId);
		buffer.append("gameName=").append(this.gameName).append('\n');
		buffer.append("lastAvailableTurn=").append(this.lastAvailableTurn);
		buffer.append("lastReadTurn=").append(this.lastReadTurn).append('\n');
		buffer.append("mapSize=").append(this.mapSize);
		buffer.append("maxTotalMinerals=").append(this.maxTotalMinerals);
		buffer.append("playerNo=").append(this.playerNo);
		buffer.append("zoomFactor=").append(this.zoomFactor).append('\n');

		//- Dump the information about the Bases
		Iterator<Base> bit = this.bases.values().iterator();
		while (bit.hasNext()) {
			buffer.append(bit.next().dump());
		}

		buffer.append("]").append('\n');
		return buffer.toString();
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[AssistantMap:");
		buffer.append("gameId=").append(this.gameId);
		buffer.append("gameName=").append(this.gameName);
		buffer.append("lastAvailableTurn=").append(this.lastAvailableTurn);
		buffer.append("lastReadTurn=").append(this.lastReadTurn);
		buffer.append("mapSize=").append(this.mapSize);
		buffer.append("maxTotalMinerals=").append(this.maxTotalMinerals);
		buffer.append("playerNo=").append(this.playerNo);
		buffer.append("zoomFactor=").append(this.zoomFactor);
		buffer.append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
