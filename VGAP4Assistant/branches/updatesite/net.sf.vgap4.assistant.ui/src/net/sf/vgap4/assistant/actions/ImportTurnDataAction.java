//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.actions;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.ui.IWorkbenchPart;

import net.sf.vgap4.assistant.editor.MainMapPage;
import net.sf.vgap4.assistant.ui.Activator;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class implements a <code>WorkbenchPartAction</code> that will be presented to the user as a new menu
 * item on the right click context menu. The action to perform creates a <code>ImportPlanetDataCommand</code>
 * to import some planet data into the <code>diagram</code> model and upgrade it. The action will be
 * performed at any time so the <code>enabled</code> state is ever active.
 */
public class ImportTurnDataAction extends WorkbenchPartAction {
  //  private static Logger logger  = Logger.getLogger("net.sf.vgap4.projecteditor.commands");

  // - F I E L D - S E C T I O N ............................................................................
  // - C O N S T R U C T O R - S E C T I O N ................................................................
  public ImportTurnDataAction(final IWorkbenchPart part) {
    super(part);
  }

  // - M E T H O D - S E C T I O N ..........................................................................
  /**
   * The <code>enable</code> state is ever active because there is no dependency on the external data to
   * detect when this action cannot be executed.
   * 
   * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#isEnabled()
   */
  @Override
  public boolean isEnabled() {
    return true;
  }

  /**
   * This method is called from the UI whenever this menu item is activated. This action does not require
   * access to the event that triggers it so we implement the method <code>run()</code>.
   * 
   * @see org.eclipse.jface.action.Action#run()
   */
  @Override
  public void run() {
    super.run();
    this.execute(new ImportTurnDataCommand("Import Turn Data...", (MainMapPage) this.getWorkbenchPart()));
  }

  /**
   * The <code>enable</code> state is ever active because there is no dependency on the external data to
   * detect when this action cannot be executed.
   * 
   * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#calculateEnabled()
   */
  @Override
  protected boolean calculateEnabled() {
    return true;
  }

  /**
   * Initialize any action data that would be required to execute it.
   * 
   * @see org.eclipse.gef.ui.actions.WorkbenchPartAction#init()
   */
  @Override
  protected void init() {
    super.init();
    this.setId("ImportTurnData");
    this.setDescription("Import VGA Planets CSV data for current turn");
    this.setEnabled(true);
    this.setLazyEnablementCalculation(false);
    this.setImageDescriptor(Activator.getImageDescriptor("icons/ImportTurn.gif"));
    this.setDisabledImageDescriptor(Activator.getImageDescriptor("icons/ImportTurn_off.gif"));
    this.setText("Import Turn Data...");
    this.setToolTipText("Imports CSV Planet data from current turn.");
  }
}
// - UNUSED CODE ............................................................................................
