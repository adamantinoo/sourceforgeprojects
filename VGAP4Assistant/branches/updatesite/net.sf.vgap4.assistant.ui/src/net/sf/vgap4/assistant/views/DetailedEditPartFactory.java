//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

import net.sf.gef.core.factories.AbstractEditPartFactory;
import net.sf.gef.core.factories.IFigureFactory;
import net.sf.gef.core.parts.AbstractGenericEditPart;
import net.sf.vgap4.assistant.figures.DetailedBaseFigure;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.DetailedDiagram;
import net.sf.vgap4.projecteditor.editparts.AssistantNodeEditPart;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedEditPartFactory(IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		if (modelElement instanceof DetailedDiagram) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ "VGAP4ProjectDiagram" + "]");
			return new DetailedContainerEditPart();
		}
		//		if (modelElement instanceof Sector) {
		//			logger.info("Generating SectorEditPart for model class " + modelElement.getClass().getSimpleName()
		//					+ " with name [" + ((VGAP4Node) modelElement).getName() + "]");
		//			return new DetailedDefaultEditPart();
		//		}
		//		if (modelElement instanceof Planet) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
		//					+ ((VGAP4Node) modelElement).getName() + "]");
		//			return new DetailedDefaultEditPart();
		//		}
		if (modelElement instanceof Base) {
			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
					+ ((AssistantNode) modelElement).getName() + "]");
			return new DetailedBaseEditPart();
		}
		//		if (modelElement instanceof Ship) {
		//			logger.info("Generating EditPart for model class " + modelElement.getClass().getSimpleName() + " with name ["
		//					+ ((VGAP4Node) modelElement).getName() + "]");
		//			return new DetailedDefaultEditPart();
		//		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}

}

class DetailedContainerEditPart extends AbstractGenericEditPart {
	private static final Insets	PADDING	= new Insets(4, 2, 4, 2);

	@Override
	protected void createEditPolicies() {
		// - Disallows the removal of this edit part from its parent
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		installEditPolicy(EditPolicy.NODE_ROLE, null);
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, null);
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
	}

	@Override
	protected IFigure createFigure() {
		// - Create an empty figure to control the scroll and the sizing.
		Figure fig = new FreeformLayer();
		fig.setOpaque(true);
		fig.setBorder(new MarginBorder(3));
		fig.setLayoutManager(new GraphLayoutManager(this));
		return fig;
	}

	public void contributeNodesToGraph(DirectedGraph graph, Map<DetailedDefaultEditPart, Node> map) {
		//- Scan all the children for this diagram and add all them to the graph.
		Node previousNode = null;
		int counter = 0;
		boolean firstNode = true;
		List<DetailedDefaultEditPart> contents = this.getChildren();
		Iterator<DetailedDefaultEditPart> cit = contents.iterator();
		while (cit.hasNext()) {
			//- Get the part, the figure and the model.
			DetailedDefaultEditPart part = cit.next();

			//- Create the node and fill its fields.
			Node node = new Node(part);
			node.setPadding(PADDING);
			node.setRowConstraint(counter);
			node.setSize(part.getSize());
			if (firstNode) {
				//- Do not connect this node with an edge.
				previousNode = node;
				firstNode = false;
				graph.nodes.add(node);
				map.put(part, node);
			} else {
				//- Connect this node and the previous one.
				Edge connectingEdge = new Edge(previousNode, node);
				previousNode = node;
				graph.nodes.add(node);
				graph.edges.add(connectingEdge);
				map.put(part, node);
			}
		}
	}

	protected void applyChildrenResults(DirectedGraph graph, Map<DetailedDefaultEditPart, Node> map) {
		for (int i = 0; i < getChildren().size(); i++) {
			DetailedDefaultEditPart part = (DetailedDefaultEditPart) getChildren().get(i);
			part.applyGraphResults(graph, map);
		}
	}

	protected void applyGraphResults(DirectedGraph graph, Map<DetailedDefaultEditPart, Node> map) {
		applyChildrenResults(graph, map);
	}

	@Override
	protected List getModelChildren() {
		DetailedDiagram model = getCastedModel();
		return model.getChildren();
	}

	private DetailedDiagram getCastedModel() {
		return (DetailedDiagram) getModel();
	}

	@Override
	protected void refreshChildren() {
		//FIXME Remove this method in final release.
		try {
			super.refreshChildren();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AssistantMap.DATA_ADDED_PROP.equals(prop)) {
			refreshChildren();
		}
	}
}

class DetailedDefaultEditPart extends AssistantNodeEditPart {

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		super.propertyChange(evt);
	}

	@Override
	public void setSelected(final int value) {
		super.setSelected(value);
	}

	protected void applyGraphResults(DirectedGraph graph, Map<DetailedDefaultEditPart, Node> map) {
		try {
			Node node = map.get(this);
			getFigure().setBounds(new Rectangle(node.x, node.y, node.width, node.height));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public Dimension getSize() {
		return new Dimension(this.getFigure().getPreferredSize());
	}
}

class DetailedBaseEditPart extends DetailedDefaultEditPart {
	private Base getCastedModel() {
		return (Base) getModel();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final DetailedBaseFigure fig = (DetailedBaseFigure) getFigure();
		fig.createContents();
		final Base model = getCastedModel();

		// - Update figure visuals from current model data.
		//		fig.setCoordinates(model.getLocation());
		//		fig.setId(model.getIdNumber());
		//		fig.setName(model.getName());

		// - The references to the model and figure objects.
		//		final Figure fig = (Figure) getFigure();

		final Dimension size = fig.getSize();
		Point location = getCastedModel().getLocation();

		//		//- Calculate real map location depending on the zoom factor.
		//		int zoom = getZoomFactor();
		//		if (zoom > 0) {
		//			location.x = location.x * zoom;
		//			location.y = location.y * zoom;
		//		}

		//- Change location to the neutral coordinates.
		//		location.x = 3000 - location.x;
		//		location.y = 3000 - location.y;
		final Rectangle bounds = new Rectangle(location, size);
		//		fig.setBounds(bounds);
		//		fig.setLocation(location);
		//		fig.repaint();
		//		fig.revalidate();
		//		fig.validate();
		//		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bounds);
		//		((GraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), bounds);
		super.refreshVisuals();
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("DetailedBaseEditPart[");
		buffer.append(((AssistantNode) this.getModel()).getName()).append("-");
		buffer.append(((Figure) this.getFigure()).getLocation()).append("-");
		// buffer.append(((Figure)this.isSelectable()).getLocation()).append("-");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		super.propertyChange(evt);
	}

	@Override
	public Dimension getSize() {
		return super.getSize();
	}
}
// - UNUSED CODE ............................................................................................
