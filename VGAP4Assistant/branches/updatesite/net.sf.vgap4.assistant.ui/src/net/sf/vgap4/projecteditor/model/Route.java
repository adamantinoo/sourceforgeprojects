//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.vgap4.projecteditor.model;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.gef.core.model.AbstractPropertyChanger;


// - CLASS IMPLEMENTATION ...................................................................................
public class Route extends AbstractPropertyChanger {
	private static final long		serialVersionUID	= -1270456357100764725L;
	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.projecteditor.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	SOURCE_PROP				= "Route.SOURCE_PROP";
	public static final String	TARGET_PROP				= "Route.TARGET_PROP";

	/** True, if the connection is attached to its endpoints. */
	private boolean							isConnected;
	/** Stores the <i>source</i> for this connection. */
	protected RouteEndPoint			source;
	/** Stores the <i>target</i> for this connection. */
	protected RouteEndPoint			target;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public Route(final RouteEndPoint from, final RouteEndPoint to) {
		setSource(from);
		setTarget(to);
		source.setSource(this);
		target.setTarget(this);
	}

	// - G E T T E R S / S E T T E R S
	public RouteEndPoint getSource() {
		return source;
	}

	public void setSource(final RouteEndPoint newSource) {
		final Object old = source;
		source = newSource;
		firePropertyChange(SOURCE_PROP, old, source);
	}

	public RouteEndPoint getTarget() {
		return target;
	}

	public void setTarget(final RouteEndPoint newTarget) {
		final Object old = target;
		target = newTarget;
		firePropertyChange(TARGET_PROP, old, target);
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Disconnect this connection from the shapes it is attached to.
	 */
	public void disconnect() {
		if (isConnected) {
			source.removeConnection(this);
			target.removeConnection(this);
			isConnected = false;
		}
	}

	/**
	 * Reconnect this connection. The connection will reconnect with the shapes it was previously attached to.
	 */
	public void reconnect() {
		if (!isConnected) {
			source.addConnection(this);
			target.addConnection(this);
			isConnected = true;
		}
	}

	/**
	 * Reconnect to a different source and/or target shape. The connection will disconnect from its current
	 * attachments and reconnect to the new source and target.
	 * 
	 * @param newSource
	 *          a new source endpoint for this connection (non null)
	 * @param newTarget
	 *          a new target endpoint for this connection (non null)
	 * @throws IllegalArgumentException
	 *           if any of the paramers are null or newSource == newTarget
	 */
	public void reconnect(RouteEndPoint newSource, RouteEndPoint newTarget) {
		if (newSource == null || newTarget == null || newSource == newTarget) { throw new IllegalArgumentException(); }
		disconnect();
		this.source = newSource;
		this.target = newTarget;
		reconnect();
	}
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
}
// - UNUSED CODE ............................................................................................
