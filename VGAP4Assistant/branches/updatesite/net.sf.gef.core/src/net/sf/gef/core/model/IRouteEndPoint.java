//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.gef.core.model;

// - IMPORT SECTION .........................................................................................
import java.util.List;

// - CLASS IMPLEMENTATION ...................................................................................
public interface IRouteEndPoint {
	/** Property ID to use when the list of outgoing connections is modified. */
	public static final String	SOURCE_CONNECTIONS_PROP	= "RouteEndPoint.SOURCE_CONNECTIONS_PROP";
	/** Property ID to use when the list of incoming connections is modified. */
	public static final String	TARGET_CONNECTIONS_PROP	= "RouteEndPoint.TARGET_CONNECTIONS_PROP";

	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	public void addConnection(final Route wire);

	public void setSource(Route wire);

	public void setTarget(Route wire);

	/**
	 * Return a List of outgoing Connections.
	 */
	public abstract List<Route> getSourceConnections();

	/**
	 * Return a List of incoming Connections.
	 */
	public abstract List<Route> getTargetConnections();

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	public void removeConnection(final Route conn);
}

// - UNUSED CODE ............................................................................................
