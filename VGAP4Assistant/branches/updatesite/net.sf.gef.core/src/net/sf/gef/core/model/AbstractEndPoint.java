//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.gef.core.model;

// - IMPORT SECTION .........................................................................................
import java.util.List;


// - CLASS IMPLEMENTATION ...................................................................................
public class AbstractEndPoint extends AbstractNode implements IRouteEndPoint {
	// - F I E L D - S E C T I O N ............................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addConnection(Route wire) {
		// TODO Auto-generated method stub

	}

	public List<Route> getSourceConnections() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Route> getTargetConnections() {
		// TODO Auto-generated method stub
		return null;
	}

	public void removeConnection(Route conn) {
		// TODO Auto-generated method stub

	}

	public void setSource(Route wire) {
		// TODO Auto-generated method stub

	}

	public void setTarget(Route wire) {
		// TODO Auto-generated method stub

	}
}

// - UNUSED CODE ............................................................................................
