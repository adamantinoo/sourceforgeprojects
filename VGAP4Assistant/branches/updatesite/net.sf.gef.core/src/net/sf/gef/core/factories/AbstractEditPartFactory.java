//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.gef.core.factories;

// - IMPORT SECTION .........................................................................................

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sf.gef.core.parts.AbstractGenericEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractEditPartFactory implements EditPartFactory {
	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	protected IFigureFactory	figureFactory;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public AbstractEditPartFactory(IFigureFactory factory) {
		setFigureFactory(factory);
	}

	// - G E T T E R S / S E T T E R S
	public IFigureFactory getFigureFactory() {
		return figureFactory;
	}

	public void setFigureFactory(IFigureFactory figureFactory) {
		this.figureFactory = figureFactory;
	}

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	protected abstract AbstractGenericEditPart getPartForElement(Object modelElement);

	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E

	/**
	 * This method is called whenever a new EditPart is created. The received parameter model indicates the
	 * corresponding EditPart that must be created for any particular model. There is a second parameter called
	 * <code>context</code> that allows a more precise identification of the creation moment and then to allow
	 * proper matching between model and EditPart.
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object modelElement) {
		// - Get EditPart for model element
		AbstractGenericEditPart part = getPartForElement(modelElement);
		// - Store model element in EditPart
		part.setModel(modelElement);
		part.setFactory(this);
		return part;
	}
}

// - UNUSED CODE ............................................................................................
