//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.eclipse.ui;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Assert;
import org.eclipse.ui.plugin.AbstractUIPlugin;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractActivator extends AbstractUIPlugin {
	// private static Logger logger = Logger.getLogger("net.sf.eclipse.core");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// - F I E L D S
	/** The shared instance. */
	private static AbstractUIPlugin					plugin;
	/** Hash map where I can store and then retrieve global items. */
	private static HashMap<Object, Object>	registry	= new HashMap<Object, Object>();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public AbstractActivator() {
	// }
	// - S T A T I C - S E C T I O N
	/**
	 * Returns an element in the registry that it is identified by the unique ID. If the element is not found in
	 * the registry then an exception is thrown to be cached by any methods that will interpret this runtime
	 * class of exceptions.
	 */
	public static Object getByID(String id) {
		final Object reference = registry.get(id);
		Assert.isNotNull(reference, "Reference in the registry is not found. This is a runtime error.");
		return reference;
	}

	public static void addReference(Object key, Object newReference) {
		registry.put(key, newReference);
	}

	// public static HashMap<Object, Object> getRegistry() {
	// return registry;
	// }

	public static AbstractUIPlugin getPlugin() {
		return plugin;
	}

	public static void setPlugin(AbstractUIPlugin plugin) {
		AbstractActivator.plugin = plugin;
	}

	// - G E T T E R S / S E T T E R S

	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		setPlugin(this);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		setPlugin(null);
		super.stop(context);
	}
	// - I N T E R F A C E - N A M E
}

// - UNUSED CODE ............................................................................................
