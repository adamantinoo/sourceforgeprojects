//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Properties;

import org.eclipse.draw2d.geometry.Point;

import net.sf.vgap4.assistant.ui.AssistantConstants;

public class AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long		serialVersionUID	= 2420277850464981241L;

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Stores the number of the turn that matched for this data. This information is relevant to identify
	 * differences for data that are compared between successive turns.
	 */
	private int									turnNumber				= -1;
	/**
	 * This structure stores all the information parsed from the .CSV file in a format suitable to be accessed
	 * by name. Instead storing every data inside its own field and the creating accessors to manipulate it, I
	 * have created a single structure in form of a Property list to get each element by name. Some of them will
	 * be able to be cached so the processing overhead to get many times the same information are reduced for
	 * this common accessed data.
	 */
	protected final Properties	info							= new Properties();
	/**
	 * Stores the reference to this planet unique ID. This is mapped from the input data and used for turn
	 * insertion validation (this data cannot be inserted inside another planet). This field is a copy of the
	 * values of all the instances of planet turn information that can be imported and used to validate this
	 * data before adding new <code>PlanetInformation</code> to the model.
	 */
	protected int								idNumber					= -1;
	/** Stores the name for this planet, this name is set to this game map and cannot be changed. */
	protected String						name;
	/**
	 * Stores the coordinates to the planet location inside the diagram coordinates. Those coordinates do not
	 * change and can be cached.
	 */
	protected Point							location;
	/**
	 * Records the type of the data structures that are stored inside the properties. This will allow to manage
	 * more different data models from additional .CSV files without extending the classes the implement the
	 * model. External classes should be aware of this field to be allowed to interpret the read data model.
	 */
	protected String						dataType					= AssistantConstants.MODELTYPE_THING;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the string stored for the turn property identified by the parameter if found or the empty string
	 * otherwise.
	 * 
	 * @param key
	 *          turn property to be located.
	 * @return the string value associated to that string property.
	 */
	public String getField(final String key) {
		final String value = info.getProperty(key);
		if (null == value) return "";
		return value;
	}

	/**
	 * Loads the list of properties that come inside two arrays into the turn property list. There are two lists
	 * of data, one with the property names and the other with the property values. Both match at the same index
	 * location. From those two list we generate the value pairs to compose the turn properties.
	 * 
	 * @param fieldNames
	 *          the names of the data fields. Area used as keys in the association inside the Property list.
	 * @param fields
	 *          the array of data fields.
	 */
	public void loadData(final String[] fieldNames, final String[] fields) {
		//- Read the field name/field value pairs from these arrays and store it inside the property list.
		try {
			for (int i = 1; i < fieldNames.length; i++) {
				final String fieldName = fieldNames[i].trim();
				final String value = fields[i].trim();
				//- Check for null values and empty strings.
				if (null == value) continue;
				info.setProperty(fieldName, value);
			}
		} catch (ArrayIndexOutOfBoundsException aiobe) {
			//- This will happen when there are no enough data values.
			//--- We can discard any other data.
		}
	}

	/**
	 * Returns the evaluated integer value is anything stored for the turn property identified by the parameter
	 * if found or the value ZERO otherwise.
	 * 
	 * @param key
	 *          turn property to be located.
	 * @return the integer value for that string property.
	 */
	public int getFieldNumber(final String key) {
		try {
			return this.convert2Integer(this.getField(key));
		} catch (final Exception exc) {
			//- The field is null or not convertible to a number. Return the default ZERO.
			return 0;
		}
	}

	public int getIdNumber() {
		return idNumber;
	}

	public Point getLocation() {
		if (null == location)
			return new Point(0, 0);
		else
			return location;
	}

	public String getName() {
		return name;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public void setTurnNumber(final int turnNumber) {
		this.turnNumber = turnNumber;
	}

	protected int convert2Integer(final String fieldValue) {
		return Integer.parseInt(fieldValue.trim());
	}

	public String getModelType() {
		return this.dataType;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("AssistantTurnInfo[");
		buffer.append(getIdNumber() + "-" + getName()).append(",");
		buffer.append("turnNumber=" + turnNumber).append("]");
		buffer.append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
