//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractPropertyChanger;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedDiagram extends AbstractPropertyChanger {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long	serialVersionUID	= 5181001026031627512L;

	// - F I E L D - S E C T I O N ............................................................................
	Vector<AssistantNode>			children					= new Vector<AssistantNode>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void addChild(AssistantNode child) {
		this.children.add(child);
	}

	public Vector<AssistantNode> getChildren() {
		//DEBUG Limit selection only to bases to simplify development
		Vector<AssistantNode> bases = new Vector<AssistantNode>();
		Iterator<AssistantNode> bit = this.children.iterator();
		while (bit.hasNext()) {
			AssistantNode child = bit.next();
			if (child instanceof Base) bases.add(child);
		}
		//		return bases;
		return this.children;
	}

	public void clear() {
		this.children.clear();
	}

	public void addChild(Vector<AssistantNode> contents) {
		children.addAll(contents);
	}
}

// - UNUSED CODE ............................................................................................
