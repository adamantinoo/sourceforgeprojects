//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.figures.draw2d.RoundedGroup;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractDetailedFigure extends SelectableFigure implements IDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger					logger					= Logger.getLogger("net.sf.vgap4.assistant.figures");
	protected static final Color	COLOR_SHIP_NAME	= new Color(Display.getDefault(), 0, 0, 0);
	protected static final Color	COLOR_BLACK			= new Color(Display.getDefault(), 0, 0, 0);

	// - F I E L D - S E C T I O N ............................................................................
	private final AssistantNode		genericModel;
	protected RoundedGroup				group						= new RoundedGroup();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractDetailedFigure(final AssistantNode model) {
		genericModel = model;
		group.groupName.setFont(AssistantConstants.FONT_MAP_BOLD);
		group.groupName.setForegroundColor(AssistantConstants.COLOR_MEDIUM_BLUE);
		this.add(group);

		//- Change the background color depending on FRIEND or FOE.
		if (AssistantConstants.FFSTATUS_UNDEFINED.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_UNDEFINED_BACKGROUND);
		if (AssistantConstants.FFSTATUS_FRIEND.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_FRIEND_BACKGROUND);
		if (AssistantConstants.FFSTATUS_FOE.equals(genericModel.getFFStatus()))
			group.setBackgroundColor(AssistantConstants.COLOR_FOE_BACKGROUND);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		this.setupLayout();
		this.setIcon(this.createDetailedIcon());
		this.setTitle(genericModel.getIdentifier() + " [T" + genericModel.getLatestTurnNumber() + "]");

		final MultiLabelLine coordsLabel = new MultiLabelLine();
		coordsLabel.addColumn("Coords:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		coordsLabel.addColumn(genericModel.getLocationString(), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
		this.append(coordsLabel);

		this.addAdditionalContents();
		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	private void setIcon(final Image newIcon) {
		group.groupName.setIcon(newIcon);
		group.groupName.setIconTextGap(2);
	}

	private void setTitle(final String title) {
		group.setText(title);
	}

	private void setupLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 2;
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
	}

	protected abstract void addAdditionalContents();

	protected void append(final IFigure newFigure) {
		group.add(newFigure);
	}

	protected abstract Image createDetailedIcon();
}

// - UNUSED CODE ............................................................................................
