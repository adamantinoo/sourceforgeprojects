//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.projecteditor.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.logging.Logger;

import es.ftgroup.gef.pages.IPropertyPage;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Composite;

import net.sf.vgap4.assistant.factories.IconImageFactory;
import net.sf.vgap4.assistant.figures.SpotFigure;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Pod;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotEditPart extends AssistantNodeEditPart implements ISelectablePart {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Instead creating a simple PropertyPage this method should create one page for each element in this
	 * location.
	 */
	public IPropertyPage createPropertyPage(final Composite top, final boolean singleSelected) {
		final Spot spot = this.getCastedModel();
		final Iterator<AssistantNode> cit = spot.getContents().iterator();
		try {
			while (cit.hasNext()) {
				final AssistantNode element = cit.next();
				//					if (element instanceof Base) {
				//						final BasePropertyPage page = new BasePropertyPage(top);
				//						page.setModel((Base) element);
				//					}
				//					if (element instanceof Planet) {
				//						final PlanetPropertyPage page = new PlanetPropertyPage(top);
				//						page.setModel((Planet) element);
				//					}
				//					if (element instanceof Ship) {
				//						final ShipPropertyPage page = new ShipPropertyPage(top);
				//						page.setModel((Ship) element);
				//					}
			}
			//			return page;
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public Spot getCastedModel() {
		return (Spot) this.getModel();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (AssistantMap.CHANGE_ZOOMFACTOR.equals(prop)) {
			this.refreshFigureLocation();
			return;
		}
		super.propertyChange(evt);
	}

	private Rectangle getMapBoundaries() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getSpotRegistry().getBoundaries().getCopy();
	}

	private int getMapSize() {
		final AssistantMap map = (AssistantMap) this.getParent().getModel();
		return map.getMapSize();
	}

	//	private Figure getToolTip(final Spot model) {
	//		return null;
	//	}

	private void refreshFigureImage() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		final int player = this.getPlayerCode();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();
		final int owner = representative.getOwner();

		//- Create the representing image depending on representative.
		final IconImageFactory imageFactory = new IconImageFactory();
		imageFactory.clearActivates();
		if (representative instanceof Base) {
			imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
			if (player == owner)
				imageFactory.activateBase(false);
			else
				imageFactory.activateBase(true);

			//- Show classification from the underlying planet
			final Planet planet = this.getOnPlanet(((Base) representative).getPlanetId());
			imageFactory.setPlanetClassification(planet.getPlanetClassification());
			if (model.hasNatives()) imageFactory.activateNatives();
		}
		if (representative instanceof Planet) {
			imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
			imageFactory.setInfoLevel(model.infoLevel());
			if (model.hasNatives()) imageFactory.activateNatives();
		}
		if (representative instanceof Ship) {
			imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
			if (player == owner)
				imageFactory.setEnemy(false);
			else
				imageFactory.setEnemy(true);
		}
		if (representative instanceof Pod) {
			imageFactory.setShape(IconImageFactory.SHAPE_POD);
			if (player == owner)
				imageFactory.setEnemy(false);
			else
				imageFactory.setEnemy(true);
		}

		//- If this is a multiple spot process the contents to generate decorators.
		if (model.isMultiple()) {
			if (model.hasShip) imageFactory.activateShip(false);
			if (model.hasPods) imageFactory.activatePods(false);
			if (model.hasWings) imageFactory.activateWings(false);
		}
		fig.setIconImage(imageFactory.generateImage());
	}

	private void refreshFigureLocation() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();
		//		AssistantMap map = (AssistantMap) this.getParent().getModel();

		//- Get the location from the model information and then convert to the final coordinates.
		final Dimension size = fig.getSize();
		final Point location = representative.getLocation();

		//- Invert the coordinates for the Y axis depending on the map size.
		final int mapSize = this.getMapSize();
		location.y = mapSize - location.y;

		//- Adjust location depending on the top-left minimum coordinates.
		final Rectangle boundaries = this.getMapBoundaries();
		final Point origin = new Point(Math.max(boundaries.x - 30, 0), Math.max(boundaries.height - 30, 0));
		location.x = location.x - origin.x;
		location.y = location.y - (mapSize - origin.y);
		fig.setLocation(location);

		//- Calculate real map location depending on the zoom factor.
		final int zoom = this.getZoomFactor();
		if ((zoom > 10) || (zoom < 10)) {
			if (zoom > 0) {
				final double factor = zoom / 10.0;
				location.x = new Double(location.x * factor).intValue();
				location.y = new Double(location.y * factor).intValue();
			}
		}

		final Rectangle bounds = new Rectangle(location, size);
		SpotEditPart.logger.info("Rebounding instance " + representative.getName() + " to " + bounds);
		fig.setBounds(bounds);
		((GraphicalEditPart) this.getParent()).setLayoutConstraint(this, fig, bounds);
	}

	private void refreshModelVisuals() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();

		// - Update figure visuals from current model data.
		fig.setCoordinates(representative.getLocation());
		fig.setId(representative.getIdNumber());
		fig.setName(representative.getName());
	}

	private void refreshToolTip() {
		// - The references to the model and figure objects.
		final SpotFigure fig = (SpotFigure) this.getFigure();
		final Spot model = this.getCastedModel();
		final int player = this.getPlayerCode();

		//- Get the representative model data.
		final AssistantNode representative = model.getRepresentative();

		//- Create the tooltip with the composite information for this Spot.
		final Figure tip = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 1;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		grid.verticalSpacing = 1;
		tip.setLayoutManager(grid);
		final StandardLabel repLabel = new StandardLabel(representative.getIdentifier(), fig.getIconImage());
		tip.add(repLabel);
		final Iterator<AssistantNode> mit = model.getContents().iterator();
		while (mit.hasNext()) {
			final AssistantNode element = mit.next();
			if (element != representative) {
				//- Generate the image for the current element.
				final IconImageFactory imageFactory = new IconImageFactory();
				imageFactory.setTip(true);
				if (element instanceof Planet) {
					imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
					imageFactory.setInfoLevel(model.infoLevel());
					if (model.hasNatives()) imageFactory.activateNatives();
				}
				if (element instanceof Ship) {
					imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
					final int owner = element.getOwner();
					if (player == owner)
						imageFactory.setEnemy(false);
					else
						imageFactory.setEnemy(true);
				}
				if (element instanceof Pod) {
					imageFactory.setShape(IconImageFactory.SHAPE_POD);
					final int owner = element.getOwner();
					if (player == owner)
						imageFactory.setEnemy(false);
					else
						imageFactory.setEnemy(true);
				}
				final StandardLabel elementLabel = new StandardLabel(element.getIdentifier(), imageFactory.generateImage());
				tip.add(elementLabel);
			}
			if (element instanceof Planet) {
				final IconImageFactory imageFactory = new IconImageFactory();
				imageFactory.setTip(true);
				imageFactory.setShape(IconImageFactory.SHAPE_PLANET);
				imageFactory.setInfoLevel(AssistantConstants.INFOLEVEL_NATIVES);
				if (model.hasNatives()) {
					final StandardLabel nativesLabel = new StandardLabel(((Planet) element).getNativeToolTip(), imageFactory
							.generateImage());
					tip.add(nativesLabel);
				}
			}
		}
		fig.setToolTip(tip);
	}

	/**
	 * This method is called upon the creation of the EditPart to associate it with behavior modifiers for
	 * selection and other node functions.
	 * 
	 * @see es.ftgroup.gef.parts.AbstractNodeEditPart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	/**
	 * Overriding of this function to update the visual part of any model object when a request for model
	 * modification is received. The model change fires a <code>firePropertyChange</code> or
	 * <code>fireStructureChange</code> that usually is intercepted on the <code>propertyChange</code>
	 * method to check what king of View updates are required for the Model change. This method is then called
	 * to fire a complete update of the View side of the MVC pattern.
	 * 
	 * @see net.sf.vgap4.projecteditor.editparts.AssistantNodeEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		try {
			this.refreshFigureImage();
			this.refreshToolTip();
			this.refreshModelVisuals();
			this.refreshFigureLocation();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
