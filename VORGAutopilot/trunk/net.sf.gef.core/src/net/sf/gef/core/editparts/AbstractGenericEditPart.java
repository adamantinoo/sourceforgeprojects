//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package net.sf.gef.core.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeListener;

import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sf.gef.core.model.AbstractPropertyChanger;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractGenericEditPart extends AbstractGraphicalEditPart implements PropertyChangeListener {
	//	private static Logger						logger	= Logger.getLogger("es.ftgorup.gef.model");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S
	private AbstractEditPartFactory	partFactory;

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Activates or deactivates the connection to the property listener. Any change on a target property will
	 * fire a call on the <code>propertyChange</code> method.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		((AbstractPropertyChanger) getModel()).addPropertyChangeListener(this);
		super.activate();
	}

	@Override
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		((AbstractPropertyChanger) getModel()).removePropertyChangeListener(this);
		super.deactivate();
	}

	public AbstractEditPartFactory getFactory() {
		return partFactory;
	}

	public void setFactory(final AbstractEditPartFactory factory) {
		partFactory = factory;
	}
}
// - UNUSED CODE ............................................................................................
