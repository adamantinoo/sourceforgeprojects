//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views.facets;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.graphics.Image;

import net.sf.vorg.core.models.facets.ITreeFacet;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractImageAccesor {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.views.facets");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractImageAccesor() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Image getImage(final ITreeFacet node) {
		return null;
	}
}
// - UNUSED CODE ............................................................................................
