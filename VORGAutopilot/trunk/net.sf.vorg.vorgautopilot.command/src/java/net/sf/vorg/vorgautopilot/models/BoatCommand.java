//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;

import net.sf.vorg.core.enums.ModelStates;
import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VORGURLRequest;
import net.sf.vorg.core.util.TimeZoneAdapter;

//- CLASS IMPLEMENTATION ...................................................................................
public class BoatCommand {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private int								cap										= 0;
	private int								newCap								= 0;
	private Boat							boatTemporalReference	= null;
	private SailConfiguration	sails;
	private String						errorMessage;

	// private Boat boat;
	// private final StringBuffer cookies = new StringBuffer();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void setHeading(int currentcap, int newCap) {
		cap = GeoLocation.adjustAngleTo360(currentcap);
		this.newCap = GeoLocation.adjustAngleTo360(newCap);
	}

	public void setSails(SailConfiguration sailConfiguration) {
		sails = sailConfiguration;
	}

	public Boat getBoatRef() {
		return boatTemporalReference;
	}

	public int getNewHeading() {
		return newCap;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public SailConfiguration getNewSails() {
		return sails;
	}

	public int getHeading() {
		return cap;
	}

	/* Connect to the HTTP server and sends the command to update the boat configuration. */
	public void sendCommand(Boat targetBoat) {
		if (null != targetBoat) {
			boatTemporalReference = targetBoat;
			PilotBoat boat = (PilotBoat) targetBoat.getParent();
			if (null != boat) {
				// - Check if the sail and course configuration are the same before sending the command.
				if (cap == newCap) if (targetBoat.getSail() == sails.getSail()) {
					System.out.println("Same boat configuration. Skip command.\n");
					return;
				}
				// - Send the new configuration to control the boat.
				StringBuffer request = new StringBuffer("/update.php?");
				request.append("voile=" + Sails.encodeSail(sails.getSail()));
				request.append("&").append("error=").append("");
				request.append("&").append("cap=").append(newCap);
				request.append("&").append("currentCap=").append(cap);
				request.append("&").append("identifiantPlayer=").append(boat.getBoatId());
				request.append("&").append("clef=").append(boat.getBoatClef());
				request.append("&").append("state=").append("none");

				StringBuffer cookies = new StringBuffer();
				cookies.append("userid=" + boat.getBoatId());
				cookies.append("&useremail=" + boat.getBoatEmail());

				try {
					VORGURLRequest boatRequest = new VORGURLRequest(request.toString());
					boatRequest.executeGET(cookies.toString());
					String requestData = boatRequest.getData();
					System.out.println(GregorianCalendar.getInstance().getTime() + " - " + "Executed boat change: " + request);
					System.out.println(requestData);

					// - Check if we have received an OK
					String[] data = requestData.split("&");
					//- Element 1 has to be the state.
					if (data.length > 0) {
						String[] state = data[0].split("=");
						String status;
						if (state[0].toLowerCase().equals("state")) {
							status = state[1];
							if (status.toUpperCase().equals("OK")) {
								targetBoat.setSail(sails.getSail());
								errorMessage = null;
								((PilotBoat) targetBoat.getParent()).setErrorMessage(null);
								((PilotBoat) targetBoat.getParent()).setState(ModelStates.READY);
								//- Register the new Action in the list
								((PilotBoat) targetBoat.getParent()).recordChange(new ActionRecord(Boat.BOAT_COMMANDSENTOK, this));
								//- Clear the update status of the Boat because is obsolete
								targetBoat.setLastUpdateDate(TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1")
										.getTime().toString());
							} else {
								//- Probably the key is invalid
								errorMessage = data[1];
								((PilotBoat) targetBoat.getParent()).setErrorMessage(data[1]);
								((PilotBoat) targetBoat.getParent()).setState(ModelStates.ONHOLD);
								//- Register the new Action in the list
								((PilotBoat) targetBoat.getParent()).recordChange(new ActionRecord(Boat.BOAT_COMMANDSENTNOOK, this));
							}
						}
					}
					//					String result = requestData.substring(0, 2);
					//					//					if (result.equals("KO")) XMLAutopilot.disconnect();
					//					// - If command is OK, set the new configuration inside the boat.
					//					targetBoat.setSail(sails.getSail());
				} catch (Exception ex) {
					((PilotBoat) targetBoat.getParent()).setErrorMessage(ex.getLocalizedMessage());
					((PilotBoat) targetBoat.getParent()).setState(ModelStates.ONHOLD);
				}
			}
		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[PilotCommand ");
		buffer.append("course=").append(cap).append(", ");
		buffer.append("newCourse=").append(newCap).append(", ");
		buffer.append("sails=").append(sails).append(", ");
		buffer.append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
