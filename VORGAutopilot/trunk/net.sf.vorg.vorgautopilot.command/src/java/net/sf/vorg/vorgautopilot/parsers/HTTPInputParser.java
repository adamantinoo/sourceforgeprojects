//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.core.models.VORGURLRequest;

// - CLASS IMPLEMENTATION ...................................................................................
public class HTTPInputParser extends XMLFileParser {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	//	private String resourceData;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public HTTPInputParser(final String targetInput) {
		super(targetInput);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public InputTypes getType() {
		return InputTypes.HTTP;
	}

	/**
	 * Computes a new file message digest and compares it to the current stored hash key. Returns true is both
	 * keys are the same that means that the file has not changed sinde the last verification.
	 */
	@Override
	protected boolean checkHashCode() {
		final byte[] newHash = computeHash();
		return MessageDigest.isEqual(newHash, hash);
	}

	@Override
	protected byte[] computeHash() {
		try {
			final MessageDigest inputHash = MessageDigest.getInstance("SHA");
			inputHash.update(bufferFileData().getBytes());
			return inputHash.digest();
		} catch (final NoSuchAlgorithmException nsae) {
			//- Store the exception for its use outside this instance
			lastException = nsae;
			return new byte[0];
		} catch (final IOException ioe) {
			//- Store the exception for its use outside this instance
			lastException = ioe;
			return new byte[0];
		}
	}

	private String bufferFileData() throws IOException {
		final URL url = new URL(inputReference);
		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		return VORGURLRequest.getResourceData(conn);
	}
}

// - UNUSED CODE ............................................................................................
