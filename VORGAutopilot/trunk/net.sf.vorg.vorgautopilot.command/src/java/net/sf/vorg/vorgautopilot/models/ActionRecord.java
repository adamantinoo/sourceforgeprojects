//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.facets.ITreeFacet;
import net.sf.vorg.core.singletons.FormatSingletons;

// - CLASS IMPLEMENTATION ...................................................................................
public class ActionRecord implements ITreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger						= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	public static final int			DATETIME_COLUMN		= 0;
	public static final int			BOAT_COLUMN				= ActionRecord.DATETIME_COLUMN + 1;
	public static final int			LATITUDE_COLUMN		= ActionRecord.DATETIME_COLUMN + 2;
	public static final int			LONGITUDE_COLUMN	= ActionRecord.DATETIME_COLUMN + 3;
	private static final String	SEPARATOR					= "\t";

	// - F I E L D - S E C T I O N ............................................................................
	// - BOAT FIELDS
	private String							type							= "NONE";
	private Calendar						timeStamp;
	private String							boatName;
	private GeoLocation					location;
	private int									heading;
	private double							speed;
	private Sails								sail;
	private int									awd;
	private int									windDirection;
	private double							windSpeed;
	private int									ranking;
	private double							distanceRun;

	// - NEW COMMAND FIELDS
	private int									newHeading;
	private Sails								newSail;
	private double							newSpeed;
	private String							errorMessage;
	private int									newAWD;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ActionRecord(final String actionType, final Boat reference) {
		type = actionType;
		//- Extract the data columns from the reference.
		if (type.equals(Boat.BOAT_UPDATE)) {
			timeStamp = Calendar.getInstance();
			boatName = reference.getName();
			location = reference.getLocation();
			heading = reference.getHeading();
			speed = reference.getSpeed();
			sail = reference.getSail();
			awd = reference.getWindAngle();
			windDirection = reference.getWindCell().getWindDir();
			windSpeed = reference.getWindCell().getWindSpeed();
			ranking = reference.getRanking();
			distanceRun = reference.getDistanceRun();
		}
	}

	public ActionRecord(final String actionType, final BoatCommand reference) {
		type = actionType;
		//- Extract the data columns from the reference.
		if (type.equals(Boat.BOAT_COMMANDSENTOK)) {
			timeStamp = Calendar.getInstance();
			boatName = reference.getBoatRef().getName();
			location = reference.getBoatRef().getLocation();
			heading = reference.getBoatRef().getHeading();
			speed = reference.getBoatRef().getSpeed();
			sail = reference.getBoatRef().getSail();
			awd = reference.getBoatRef().getWindAngle();
			windDirection = reference.getBoatRef().getWindCell().getWindDir();
			windSpeed = reference.getBoatRef().getWindCell().getWindSpeed();
			ranking = reference.getBoatRef().getRanking();
			distanceRun = reference.getBoatRef().getDistanceRun();

			//- Command specific fields
			newHeading = reference.getNewHeading();
			newSail = reference.getNewSails().getSail();
			newSpeed = reference.getNewSails().getSpeed();
			newAWD = GeoLocation.calculateAWD(reference.getBoatRef().getWindCell().getWindDir(), reference.getNewHeading());
		}
		//- Extract the data columns from the reference.
		if (type.equals(Boat.BOAT_COMMANDSENTNOOK)) {
			timeStamp = Calendar.getInstance();
			boatName = reference.getBoatRef().getName();
			location = reference.getBoatRef().getLocation();
			heading = reference.getBoatRef().getHeading();
			speed = reference.getBoatRef().getSpeed();
			sail = reference.getBoatRef().getSail();
			awd = reference.getBoatRef().getWindAngle();
			windDirection = reference.getBoatRef().getWindCell().getWindDir();
			windSpeed = reference.getBoatRef().getWindCell().getWindSpeed();
			ranking = reference.getBoatRef().getRanking();
			distanceRun = reference.getBoatRef().getDistanceRun();

			//- Command specific fields
			newHeading = reference.getNewHeading();
			newSail = reference.getNewSails().getSail();
			newSpeed = reference.getNewSails().getSpeed();
			newAWD = GeoLocation.calculateAWD(reference.getBoatRef().getWindCell().getWindDir(), reference.getNewHeading());

			if (null != reference.getErrorMessage())
				errorMessage = reference.getErrorMessage().trim();
			else
				errorMessage = "";
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getAwd() {
		return new Integer(awd).toString() + "�";
	}

	public String getBoatName() {
		return boatName;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getEventLatitude() {
		return location.formatLatitude();
	}

	public String getEventLongitude() {
		return location.formatLongitude();
	}

	public String getHeading() {
		return new Integer(heading).toString() + "�";
	}

	public String getImageName() {
		if (type.equals(Boat.BOAT_UPDATE)) return "icons/boatupdate.gif";
		if (type.equals(Boat.BOAT_COMMANDSENTOK)) return "icons/boatcommandok.gif";
		if (type.equals(Boat.BOAT_COMMANDSENTNOOK)) return "icons/boatcommandnook.gif";
		return "icons/boatupdate.gif";
	}

	public String getNewHeading() {
		return new Integer(newHeading).toString() + "�";
	}

	public String getNewSail() {
		return newSail.toString();
	}

	public String getNewSpeed() {
		return FormatSingletons.nf1.format(newSpeed) + " knts";
	}

	public String getSail() {
		return sail.toString();
	}

	public String getSpeed() {
		return FormatSingletons.nf1.format(speed) + " knts";
	}

	public String getTimeStamp() {
		final SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM HH:mm:ss");
		return formatter.format(timeStamp.getTime());
		//		return timeStamp.getTime().toString();
	}

	public String getType() {
		return type;
	}

	public String getWindDirection() {
		return new Integer(windDirection).toString() + "�";
	}

	public String getWindSpeed() {
		return FormatSingletons.nf1.format(windSpeed) + " knts";
	}

	public String logOutput() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(getTimeStamp()).append(ActionRecord.SEPARATOR);
		if (type.equals(Boat.BOAT_UPDATE)) {
			buffer.append(getBoatName()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLatitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLongitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getHeading()).append(ActionRecord.SEPARATOR);
			buffer.append(getSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getSail()).append(ActionRecord.SEPARATOR);
			buffer.append(getAwd()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindDirection()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getDistanceRun()).append(ActionRecord.SEPARATOR);
			buffer.append(getRanking());
		}
		if (type.equals(Boat.BOAT_COMMANDSENTOK)) {
			buffer.append(getBoatName()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLatitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLongitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getHeading()).append(ActionRecord.SEPARATOR);
			buffer.append(getSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getSail()).append(ActionRecord.SEPARATOR);
			buffer.append(getAwd()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindDirection()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getDistanceRun()).append(ActionRecord.SEPARATOR);
			buffer.append(getRanking()).append(ActionRecord.SEPARATOR);

			buffer.append(getNewHeading()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewSail()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewAWD());
		}
		if (type.equals(Boat.BOAT_COMMANDSENTNOOK)) {
			buffer.append(getBoatName()).append(ActionRecord.SEPARATOR);
			buffer.append(getErrorMessage()).append(VORGConstants.NEWLINE);

			buffer.append(getTimeStamp()).append(ActionRecord.SEPARATOR);
			buffer.append(getBoatName()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLatitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getEventLongitude()).append(ActionRecord.SEPARATOR);
			buffer.append(getHeading()).append(ActionRecord.SEPARATOR);
			buffer.append(getSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getSail()).append(ActionRecord.SEPARATOR);
			buffer.append(getAwd()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindDirection()).append(ActionRecord.SEPARATOR);
			buffer.append(getWindSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getDistanceRun()).append(ActionRecord.SEPARATOR);
			buffer.append(getRanking()).append(ActionRecord.SEPARATOR);

			buffer.append(getNewHeading()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewSpeed()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewSail()).append(ActionRecord.SEPARATOR);
			buffer.append(getNewAWD());
		}
		return buffer.toString();
	}

	public String getRanking() {
		return new Integer(ranking).toString();
	}

	public String getDistanceRun() {
		return FormatSingletons.nf2.format(distanceRun) + "NM";
	}

	public String getNewAWD() {
		return new Integer(newAWD).toString() + "�";
	}
}

// - UNUSED CODE ............................................................................................
