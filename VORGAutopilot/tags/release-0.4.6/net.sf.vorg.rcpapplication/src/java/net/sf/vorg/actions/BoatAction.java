//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.actions;

// - IMPORT SECTION .........................................................................................
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import net.sf.gef.core.wizards.AbstractWizard;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatAction extends Action {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.actions");

	// - F I E L D - S E C T I O N ............................................................................
	protected PilotModelStore	modelStore	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatAction() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public PilotModelStore getModelStore() {
		return modelStore;
	}

	public void setModelStore(PilotModelStore modelStore) {
		this.modelStore = modelStore;
	}

	public int openWizard(AbstractWizard targetWizard, TreeViewer viewer) {
		IWorkbench workBench = PlatformUI.getWorkbench();
		Shell shell = workBench.getActiveWorkbenchWindow().getShell();
		targetWizard.init(workBench, (IStructuredSelection) viewer.getSelection());

		//- Instantiates the wizard container with the wizard and opens it
		WizardDialog dialog = new WizardDialog(shell, targetWizard);
		dialog.create();
		return dialog.open();
	}
}

// - UNUSED CODE ............................................................................................
