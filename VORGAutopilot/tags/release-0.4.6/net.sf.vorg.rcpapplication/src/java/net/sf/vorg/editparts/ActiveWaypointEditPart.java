//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SpotEditPart.java 179 2008-07-10 11:51:20Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-10 13:51:20 +0200 (jue, 10 jul 2008) $
//  RELEASE:        $Revision: 179 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;

import net.sf.gef.core.editparts.AbstractDirectedNodeEditPart;
import net.sf.gef.core.figures.ISelectableFigure;
import net.sf.vorg.figures.IDetailedFigure;
import net.sf.vorg.policies.GNodePolicy;
import net.sf.vorg.vorgautopilot.models.ActiveWaypoint;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class ActiveWaypointEditPart extends AbstractDirectedNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public ActiveWaypoint getCastedModel() {
		return (ActiveWaypoint) getModel();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Waypoint.WAYPOINT_ACTIVATED.equals(prop)) {
			refreshVisuals();
			//- Send this event up in the chain for a complete model refresh.
			((PropertyChangeListener) getParent()).propertyChange(evt);
		}
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[ActiveWaypointEditPart:");
		buffer.append(getModel().toString()).append("-");
		buffer.append(getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final IFigure fig = getFigure();
		if (fig instanceof IDetailedFigure) {
			((IDetailedFigure) fig).refreshContents();
			super.refreshVisuals();
		}
	}

	@Override
	public void setSelected(int value) {
		//- Pass the selection to the figure
		final ISelectableFigure fig = (ISelectableFigure) getFigure();
		fig.setSelected(value);
		refreshVisuals();
		super.setSelected(value);
	}
}
// - UNUSED CODE ............................................................................................
