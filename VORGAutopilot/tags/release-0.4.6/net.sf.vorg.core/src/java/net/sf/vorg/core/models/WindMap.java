//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

//- IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import net.sf.vorg.core.exceptions.LocationNotInMap;

//- CLASS IMPLEMENTATION ...................................................................................
public class WindMap {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger							logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");
	// - F I E L D - S E C T I O N ............................................................................
	// protected Hashtable mapList = new Hashtable<Object, Object>();
	protected final Vector<WindCell>	cells			= new Vector<WindCell>(10);
	protected String									reference	= "UNDEFINED";
	private Date											timeStamp	= GregorianCalendar.getInstance().getTime();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void addCell(final WindCell newCell) {
		// - Check if this cell is already on the map.
		if (!cells.contains(newCell)) {
			// - Timestamp this cell.
			newCell.timeStamp(timeStamp);
			cells.add(newCell);
		}
	}

	public void setRef(final String mapRef) {
		reference = mapRef;
	}

	public void setTimeStamp(final Date mapDate) {
		timeStamp = mapDate;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[WindMap ");
		buffer.append(reference).append(",");
		buffer.append("nroCells=").append(cells.size()).append("]");
		return buffer.toString();
	}

	//[02]

	/**
	 * Locate the wind cell in the map that contains the specified location.
	 * 
	 * @throws LocationNotInMap
	 */
	protected WindCell cellWithPoint(final GeoLocation start) throws LocationNotInMap {
		final Iterator<WindCell> wit = cells.iterator();
		while (wit.hasNext()) {
			final WindCell cell = wit.next();
			if (cell.doesContain(start)) return cell;
		}
		// - No cell contains this location we have to throw an exception
		throw new LocationNotInMap("The " + start.toString() + " location is not contained inside the Route Map.");
	}

	// [01]

	protected WindCell cellWithPoint(final GeoLocation start, final WindCell skip) throws LocationNotInMap {
		final Iterator<WindCell> wit = cells.iterator();
		while (wit.hasNext()) {
			final WindCell cell = wit.next();
			if (cell.doesContain(start)) // - Skip identities have to be checked with coordinates because cells in different time maps also are
				// different.
				if (cell.isEquivalent(skip))
					continue;
				else
					return cell;
		}
		// - No cell contains this location we have to throw an exception
		throw new LocationNotInMap("The " + start.toString() + " location is not contained inside the Route Map.");
	}
}
// - UNUSED CODE ............................................................................................
// [01]

// public void loadWinds(GeoLocation location) {
// String protocol = "http";
// String host = "volvogame.virtualregatta.com";
// String prefix = "/resources/winds/meteo_";
//
// // - Calculate the wind box that matches this location.
// double latitude = location.getLat();
// int mapLat = new Double(Math.round(Math.floor(latitude) / 10.0) * 10.0).intValue();
// double longitude = location.getLon();
// int mapLon = new Double(Math.round(Math.floor(longitude) / 10.0) * 10.0).intValue();
// String suffix = mapLon + "_" + mapLat + ".xml?rnd=9165";
//
// try {
// URL mapReference = new URL(protocol, host, prefix + suffix);
// InputStream stream = new BufferedInputStream(mapReference.openStream());
// SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
// MapParserHandler handler = new MapParserHandler(this);
// parser.parse(stream, handler);
// // actualMap = handler.getMaps();
// } catch (MalformedURLException mue) {
// // TODO Auto-generated catch block
// mue.printStackTrace();
// } catch (IOException ioe) {
// // TODO Auto-generated catch block
// ioe.printStackTrace();
// } catch (ParserConfigurationException pce) {
// // TODO Auto-generated catch block
// pce.printStackTrace();
// } catch (SAXException se) {
// // TODO Auto-generated catch block
// se.printStackTrace();
// }
// }

//[02]
// /**
// * Calculate the intersection of the line that connects the start and the end points with the cell
// * boundaries. The order of the intersections is clearly related to the main direction of the line
// movement,
// * so the code must have provisions to detect this direction and then order the intersections that are
// * generated.
// *
// * @param targetCell
// */
// public Vector<Intersection> calculateIntersection(final GeoLocation start, final GeoLocation end,
// final WindCell targetCell) throws LocationNotInMap {
// final Vector<Intersection> intersections = new Vector<Intersection>(4);
// // - Calculate the direct path line.
// double deltaLat = end.getLat() - start.getLat();
// double deltaLon = end.getLon() - start.getLon();
// Quadrants quad = Quadrants.q4Angle(start.angleTo(end));
//
// // - Calculate cell limits.
// final Boundaries boundaries = targetCell.getBoundaries();
// double lat;
// double lon;
// GeoLocation intersection;
// if (Quadrants.QUADRANT_I == quad) {
// // - Solve the line intersections with the direct line.
// lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
// intersection = new GeoLocation(lat, boundaries.getWest());
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.W));
// }
// lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
// intersection = new GeoLocation(boundaries.getSouth(), lon);
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.S));
// }
// lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
// intersection = new GeoLocation(boundaries.getNorth(), lon);
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.N));
// }
// lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
// intersection = new GeoLocation(lat, boundaries.getEast());
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.E));
// }
// } else {
// // - Solve the line intersections with the direct line.
// lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
// intersection = new GeoLocation(lat, boundaries.getWest());
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.W));
// }
// lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
// intersection = new GeoLocation(boundaries.getNorth(), lon);
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.N));
// }
// lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
// intersection = new GeoLocation(boundaries.getSouth(), lon);
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.S));
// }
// lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
// intersection = new GeoLocation(lat, boundaries.getEast());
// if (targetCell.contains(intersection)) {
// intersections.add(new Intersection(intersection, Directions.E));
// }
// }
// return intersections;
// }
//
// public VMCRoute calculateVMCRoute(GeoLocation start, GeoLocation end) throws LocationNotInMap {
// // this.start=start;
// // this.end=end;
//
// // - Calculate the direct line to destination.
// final WindCell startCell = this.cellWithPoint(start);
// final WindCell endCell = this.cellWithPoint(end);
//
// // - Buildup the route to the end from the start cell.
// VMCRoute vmcRoute = new VMCRoute();
// Vector<Intersection> intersections = this.calculateIntersection(start, end, startCell);
// vmcRoute.add(startCell, intersections.lastElement().getDirection(), start, intersections.lastElement()
// .getLocation());
//
// // - Get cell one by one following the direct path line.
// // To get the next cell search the one that contains the end intersection.
// WindCell nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), startCell);
// while (!nextCell.equals(endCell)) {
// intersections = this.calculateIntersection(start, end, nextCell);
// vmcRoute.add(nextCell, intersections.firstElement().getDirection(),
// intersections.firstElement().getLocation(),
// intersections.lastElement().getLocation());
// nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), nextCell);
// }
//
// intersections = this.calculateIntersection(start, end, endCell);
// vmcRoute.add(endCell, intersections.firstElement().getDirection(),
// intersections.firstElement().getLocation(), end);
//
// return vmcRoute;
// }
//
// public Route directRoute(final GeoLocation start, final GeoLocation end) throws LocationNotInMap {
// // - Locate the wind cells that contains the start and end locations.
// final WindCell startCell = this.cellWithPoint(start);
// final WindCell endCell = this.cellWithPoint(end);
//
// // - Buildup the route to the end from the start cell.
// final Route directRoute = new Route();
// Vector<Intersection> intersections = this.calculateIntersection(start, end, startCell);
// directRoute.add(startCell, intersections.lastElement().getDirection(), start, intersections.lastElement()
// .getLocation());
//
// // - Get cell one by one following the direct path line.
// // To get the next cell search the one that contains the end intersection.
// WindCell nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), startCell);
// while (!nextCell.equals(endCell)) {
// intersections = this.calculateIntersection(start, end, nextCell);
// directRoute.add(nextCell, intersections.firstElement().getDirection(),
// intersections.firstElement().getLocation(), intersections.lastElement().getLocation());
// nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), nextCell);
// }
//
// intersections = this.calculateIntersection(start, end, endCell);
// directRoute.add(endCell, intersections.firstElement().getDirection(),
// intersections.firstElement().getLocation(),
// end);
//
// return directRoute;
// }
//
// public void optimizeRoute(final GeoLocation start, final GeoLocation end) throws LocationNotInMap,
// CloneNotSupportedException {
// final Route optimRoute = directRoute(start, end);
// System.out.println("Optimized Route");
// optimRoute.optimizeRoute();
// System.out.println(optimRoute.printReport());
// // System.out.println();
// // System.out.println("Time at end - " /*+ GregorianCalendar.getInstance().toString()*/);
// }
