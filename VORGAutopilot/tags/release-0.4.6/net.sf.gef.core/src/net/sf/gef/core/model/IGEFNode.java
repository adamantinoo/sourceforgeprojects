//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import es.ftgroup.gef.model.INodeModel;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IGEFNode extends INodeModel {
	// - F I E L D - S E C T I O N ............................................................................
	//	public IGEFNode					parent		= null;
	//	public Vector<IGEFNode>	children	= new Vector<IGEFNode>();

	// - M E T H O D - S E C T I O N ..........................................................................
	public IGEFNode getParent();

	public void setParent(IGEFNode parent);

	public void addChild(IGEFNode child);

	public Vector<IGEFNode> getChildren();
}
// - UNUSED CODE ............................................................................................
