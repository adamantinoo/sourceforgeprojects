//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.ui.figures;

// - IMPORT SECTION .........................................................................................
import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public interface IFigureFactory {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public PolylineConnection createConnection(IWireModel newWire);

	public Figure createFigure(EditPart part, IGEFModel unit);

	public Figure createFigure(EditPart part, IGEFModel unit, String subType);
}
// - UNUSED CODE ............................................................................................
