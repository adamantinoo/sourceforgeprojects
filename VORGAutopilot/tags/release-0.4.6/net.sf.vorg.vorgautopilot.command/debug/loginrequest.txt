__     _____  ____   ____    _         _              _ _       _   
\ \   / / _ \|  _ \ / ___|  / \  _   _| |_ ___  _ __ (_) | ___ | |_ 
 \ \ / / | | | |_) | |  _  / _ \| | | | __/ _ \| '_ \| | |/ _ \| __|
  \ \ /| |_| |  _ <| |_| |/ ___ \ |_| | || (_) | |_) | | | (_) | |_ 
   \_/  \___/|_| \_\\____/_/   \_\__,_|\__\___/| .__/|_|_|\___/ \__|
                                               |_|                  

Version 1.11 $Revision: 174 $ [26 january 2009]

--------------------------------------------------------------------------------
Tue Feb 03 17:50:58 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 17:50:59 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Tue Feb 03 17:50:59 CET 2009 - Executed boat change: /update.php?voile=8&error=&cap=66&currentCap=342&identifiantPlayer=119878&clef=434d8bc937b3fdba413002306d22cea2119878&state=none
state=OK&error=

--------------------------------------------------------------------------------
Tue Feb 03 17:52:29 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 17:52:30 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 17:54:00 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 17:54:00 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 17:56:01 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 17:56:01 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 17:58:01 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 17:58:02 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:00:06 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9389,lon=122.967][26º 56' N 122º 58' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 18:00:07 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=30
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.1-GENOA - VMC=7.141778489984128 - 345]
Right VMC [boat=10.56-GENOA - VMC=8.543219460599445 - 66]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:02:07 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9508,lon=122.997][26º 57' N 122º 60' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=66]

Tue Feb 03 18:02:08 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=20
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.56-GENOA - VMC=8.321393558086985 - 342]
Right VMC [boat=10.1-GENOA - VMC=7.386672386353621 - 63]
]

Tue Feb 03 18:02:08 CET 2009 - Executed boat change: /update.php?voile=8&error=&cap=342&currentCap=66&identifiantPlayer=119878&clef=434d8bc937b3fdba413002306d22cea2119878&state=none
state=OK&error=

--------------------------------------------------------------------------------
Tue Feb 03 18:04:08 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9508,lon=122.997][26º 57' N 122º 60' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 18:04:08 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=20
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.56-GENOA - VMC=8.321393558086985 - 342]
Right VMC [boat=10.1-GENOA - VMC=7.386672386353621 - 63]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:06:09 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9508,lon=122.997][26º 57' N 122º 60' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 18:06:09 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=20
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.56-GENOA - VMC=8.321393558086985 - 342]
Right VMC [boat=10.1-GENOA - VMC=7.386672386353621 - 63]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:08:10 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9508,lon=122.997][26º 57' N 122º 60' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 18:08:11 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=20
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.56-GENOA - VMC=8.321393558086985 - 342]
Right VMC [boat=10.1-GENOA - VMC=7.386672386353621 - 63]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:10:11 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9508,lon=122.997][26º 57' N 122º 60' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 18:10:13 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=20
Wind direction=24
Wind speed=18.9
Left VMC [boat=10.56-GENOA - VMC=8.321393558086985 - 342]
Right VMC [boat=10.1-GENOA - VMC=7.386672386353621 - 63]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:12:13 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9786,lon=122.987][26º 59' N 122º 59' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=342]

Tue Feb 03 18:12:14 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=32
Wind direction=24
Wind speed=18.9
Left VMC [boat=7.3053333333333335-GENOA - VMC=6.947784870374862 - 14]
Right VMC [boat=10.69-GENOA - VMC=8.756735353449322 - 67]
]

Tue Feb 03 18:12:14 CET 2009 - Executed boat change: /update.php?voile=8&error=&cap=67&currentCap=342&identifiantPlayer=119878&clef=434d8bc937b3fdba413002306d22cea2119878&state=none
state=OK&error=

--------------------------------------------------------------------------------
Tue Feb 03 18:14:14 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9786,lon=122.987][26º 59' N 122º 59' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=67]

Tue Feb 03 18:14:15 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=32
Wind direction=24
Wind speed=18.9
Left VMC [boat=7.3053333333333335-GENOA - VMC=6.947784870374862 - 14]
Right VMC [boat=10.69-GENOA - VMC=8.756735353449322 - 67]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:16:15 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9786,lon=122.987][26º 59' N 122º 59' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=67]

Tue Feb 03 18:16:15 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=32
Wind direction=24
Wind speed=18.9
Left VMC [boat=7.3053333333333335-GENOA - VMC=6.947784870374862 - 14]
Right VMC [boat=10.69-GENOA - VMC=8.756735353449322 - 67]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:18:16 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9786,lon=122.987][26º 59' N 122º 59' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=67]

Tue Feb 03 18:18:16 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=32
Wind direction=24
Wind speed=18.9
Left VMC [boat=7.3053333333333335-GENOA - VMC=6.947784870374862 - 14]
Right VMC [boat=10.69-GENOA - VMC=8.756735353449322 - 67]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:20:17 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9786,lon=122.987][26º 59' N 122º 59' E], speed=10.56, windSpeed=18.8985, AWD=42, heading=67]

Tue Feb 03 18:20:17 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]]
Loading map data for box 120_30.xml?rnd=9165

[VMC results
Projection heading=32
Wind direction=24
Wind speed=18.9
Left VMC [boat=7.3053333333333335-GENOA - VMC=6.947784870374862 - 14]
Right VMC [boat=10.69-GENOA - VMC=8.756735353449322 - 67]
]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:22:18 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=10.69, windSpeed=18.8985, AWD=43, heading=67]

Tue Feb 03 18:22:19 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]
--- Discarded waypoint [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]. Distance 3.704071273612235 below 5 miles.

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Tue Feb 03 18:23:23 CET 2009 - Executed boat change: /update.php?voile=8&error=&cap=18&currentCap=67&identifiantPlayer=119878&clef=434d8bc937b3fdba413002306d22cea2119878&state=none
state=OK&error=

--------------------------------------------------------------------------------
Tue Feb 03 18:24:23 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=10.69, windSpeed=18.8985, AWD=43, heading=18]

Tue Feb 03 18:24:24 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:26:06 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=10.69, windSpeed=18.8985, AWD=43, heading=18]

Tue Feb 03 18:26:08 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:28:01 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=10.69, windSpeed=18.8985, AWD=43, heading=18]

Tue Feb 03 18:28:02 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:30:08 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=10.69, windSpeed=18.8985, AWD=43, heading=18]

Tue Feb 03 18:30:08 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:32:12 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=0.0, windSpeed=18.8985, AWD=6, heading=18]

Tue Feb 03 18:32:12 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:34:15 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=0.0, windSpeed=18.8985, AWD=6, heading=18]

Tue Feb 03 18:34:16 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

--------------------------------------------------------------------------------
Tue Feb 03 18:36:19 CET 2009 - new scan run for configuration: ./config/PolarTestAutopilot.xml

[Boat name=PolarTest, 
location=[GeoLocation lat=26.9902,lon=123.018][26º 59' N 123º 1' E], speed=0.0, windSpeed=18.8985, AWD=6, heading=18]

Tue Feb 03 18:36:20 CET 2009 - Processing command: 
[PilotCommand type=ROUTE
waypoints=[[Waypoint VMG, range=5, AWD=20, [GeoLocation lat=27.05,lon=123.03333333333333][27º 3' N 123º 2' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]], [Waypoint VMG, range=10, AWD=20, [GeoLocation lat=27.5,lon=122.5][27º 30' N 122º 30' E]], [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=28.5,lon=123.5][28º 30' N 123º 30' E]]]]
limits=[[PilotLimits action=CONNECT, border=NOBORDER, [GeoLocation lat=18.0,lon=0.0][18º 0' N 0º 0' W]]]]]

Activating route to waypoint: [Waypoint DIRECT, range=3, AWD=38, [GeoLocation lat=27.133333333333333,lon=123.06666666666666][27º 8' N 123º 4' E]]
Loading map data for box 120_30.xml?rnd=9165

[SailConfiguration GENOA,speed=10.406666666666666]

Same boat configuration. Skip command.

