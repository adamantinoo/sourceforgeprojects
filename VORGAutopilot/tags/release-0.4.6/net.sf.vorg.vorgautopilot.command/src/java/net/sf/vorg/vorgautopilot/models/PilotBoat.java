//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.ModelStates;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.core.CommandStatus;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotBoat extends AbstractGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger					logger		= Logger.getLogger("net.sf.vorg.models");
	private static final long			serialVersionUID	= -3470211489621866861L;
	public static final String		STRUCTURE_CHANGED	= "PilotBoat.STRUCTURE_CHANGED";
	public static final String		STATE_CHANGED			= "PilotBoat.STATE_CHANGED";

	// - F I E L D - S E C T I O N ............................................................................
	// - USER data.
	private String								boatid						= null;
	private String								email							= null;
	private String								clef							= null;

	private Boat									boat							= null;
	private Vector<PilotCommand>	commands					= new Vector<PilotCommand>();
	private ModelStates						state							= ModelStates.EMPTY;
	private String								errorMessage			= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/** Create a new instance with a new Boat that is always tied to this instance. */
	public PilotBoat() {
		boat = new Boat();
		this.addChild(boat);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addCommand(final PilotCommand command) {
		commands.add(command);
		this.addChild(command);
		this.fireStructureChange(PilotBoat.STRUCTURE_CHANGED, this, command);
	}

	public void cleanAactiveWaypoint() {
		final PilotCommand command = this.getActiveCommand();
		if (null != command) {
			command.cleanActiveWaypoint();
			this.fireStructureChange(PilotBoat.STRUCTURE_CHANGED, this, null);
		}
	}

	/**
	 * Clear the list of commands but without interfering with any iterator already created. Do this by removing
	 * the elements from the children list and then clearing the structure.
	 */
	public void clearCommands() {
		final Vector<PilotCommand> oldCommands = commands;
		final Iterator<PilotCommand> cit = commands.iterator();
		while (cit.hasNext()) {
			final PilotCommand pilotCommand = cit.next();
			this.removeChild(pilotCommand);
		}
		commands = new Vector<PilotCommand>();
		this.setDirty(true);
		this.fireStructureChange(PilotBoat.STRUCTURE_CHANGED, this, oldCommands);
	}

	/**
	 * Generate the piece of XML that describes this object for later reconstruction. It calls all the internal
	 * structures recursively to include the XML for the whole hierarchy.
	 * 
	 * @return a string representing the XML format of this instance.
	 */
	public String generatePersistentXML() {
		final StringBuffer buffer = new StringBuffer();
		//- Compose the boat identification line.
		buffer.append("  <boat ");
		buffer.append("name=").append(this.quote(this.getBoatName())).append(" ");
		buffer.append("email=").append(this.quote(this.getBoatEmail())).append(" ");
		buffer.append("boatid=").append(this.quote(this.getBoatId())).append(" ");
		buffer.append("clef=").append(this.quote(this.getBoatClef())).append(" ");
		buffer.append(">").append(VORGConstants.NEWLINE);

		buffer.append(boat.generatePersistentXML());

		//- Iterate though the children to dump also all other persistent information.
		buffer.append("    <pilotcommandlist>").append(VORGConstants.NEWLINE);
		final Iterator<PilotCommand> cit = commands.iterator();
		while (cit.hasNext()) {
			final PilotCommand pilotCommand = cit.next();
			buffer.append(pilotCommand.generatePersistentXML());
		}
		buffer.append("    </pilotcommandlist>").append(VORGConstants.NEWLINE);
		buffer.append("  </boat>");
		return buffer.toString();
	}

	/** Scan the list of commands and return the first one that is active or null in not command found. */
	public PilotCommand getActiveCommand() {
		final Iterator<PilotCommand> cit = commands.iterator();
		while (cit.hasNext()) {
			final PilotCommand command = cit.next();
			final CommandStatus status = command.testActivity();
			if (status == CommandStatus.GO) return command;
		}
		return null;
	}

	/**
	 * Return the reference to the current active waypoint. This is a special instance of a waypoint htat
	 * contains more data and methods to enhance the user interface presentation.
	 */
	public ActiveWaypoint getActiveWaypoint() {
		final PilotCommand command = this.getActiveCommand();
		if (null != command)
			return command.getActiveWaypoint();
		else return null;
	}

	public Boat getBoat() {
		return boat;
	}

	public String getBoatClef() {
		return clef;
	}

	public String getBoatEmail() {
		return email;
	}

	public String getBoatId() {
		return boatid;
	}

	public String getBoatName() {
		return boat.getName();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getLastBoatUpdate() {
		final Date lastDate = boat.getLastBoatUpdate();
		if (null == lastDate)
			return "";
		else return lastDate.toString();
	}

	public ModelStates getState() {
		return state;
	}

	public boolean hasErrors() {
		return (null != errorMessage);
	}

	/**
	 * Gets the first <code>PilotCommand</code> defined for this boat and gives control to it to perform any
	 * task if there are waypoints to be executed.This code belongs the the main processing loop for the
	 * autopilot commanding block.
	 */
	public void performPilot() {
		//- Read again the data from the server for this boat even it is not controlled.
		try {
			boat.updateBoatData();
			//- Check if the boat data was updated by the date detection
			if (boat.wasUpdated()) {
				this.firePropertyChange(Boat.BOAT_UPDATE, null, this);

				//- Check if this boat is still operative. See this on the state.
				if (this.getState() == ModelStates.READY) {
					final PilotCommand command = this.getActiveCommand();
					if (null != command) command.pilot();
				}
				if (this.getState() == ModelStates.ONHOLD) {
					final PilotCommand command = this.getActiveCommand();
					if (null != command) command.pilot();
				}
			}
		} catch (final DataLoadingException dle) {
			//- Save the last state for this boat inside the error message to be displayed by the UI.
			errorMessage = dle.getLocalizedMessage();
			this.setState(ModelStates.ONHOLD);
		} catch (final BoatNotFoundException bnfe) {
			//- Save the last state for this boat inside the error message to be displayed by the UI.
			errorMessage = bnfe.getLocalizedMessage();
			this.setState(ModelStates.EMPTY);
		}
	}

	public void recordChange(final ActionRecord actionRecord) {
		if (null != parent) ((PilotModelStore) parent).recordChange(actionRecord);
	}

	/**
	 * Sets the internal authentication key for the associated boat. This key is required to allow to send
	 * commands to the boat when a waypoint requests that. The model state is changed in accordance to the state
	 * of the three elements that set the authentication information. There is a new method to test that
	 * validity and change the boat state.
	 */
	public void setBoatClef(final String key) {
		if (this.testStringNullness(key))
			clef = null;
		else clef = key;
		this.setDirty(true);
		this.firePropertyChange(Boat.IDENTIFICATION_CHANGED, null, clef);
		this.checkAuthenticationState();
	}

	public void setBoatEmail(final String newEmail) {
		if (this.testStringNullness(newEmail))
			email = null;
		else email = newEmail;
		this.setDirty(true);
		this.firePropertyChange(Boat.IDENTIFICATION_CHANGED, null, clef);
		this.checkAuthenticationState();
	}

	public void setBoatId(final String newBoatid) {
		if (this.testStringNullness(newBoatid))
			boatid = null;
		else boatid = newBoatid;
		this.setDirty(true);
		this.firePropertyChange(Boat.IDENTIFICATION_CHANGED, null, boatid);
		this.checkAuthenticationState();
	}

	public void setBoatName(final String name) {
		if (null != name) {
			boat.setName(name);
			this.firePropertyChange(Boat.NAME_CHANGED, null, name);
		}
	}

	public void setErrorMessage(final String message) {
		if (null != message) errorMessage = message.trim();
	}

	public void setState(final ModelStates newState) {
		final ModelStates oldState = state;
		if (newState == ModelStates.READY) {
			if (state == ModelStates.CONFIGURED) {
				state = newState;
				this.firePropertyChange(PilotBoat.STATE_CHANGED, oldState, newState);
			}
		} else {
			state = newState;
			this.firePropertyChange(PilotBoat.STATE_CHANGED, oldState, newState);
		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[PilotBoat ");
		buffer.append("name=").append(this.getBoatName()).append(", ");
		buffer.append("clef=").append(this.getBoatClef()).append(", ");
		buffer.append("state=").append(this.getState()).append("]");
		buffer.append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	/**
	 * Goes to the game server for a new update of the boat information. This is one of the connection points
	 * were the process may detect if the boat information has changed to optimize the access to read this data
	 * the less times as possible.
	 */
	public void updateBoatData() throws DataLoadingException, BoatNotFoundException {
		boat.updateBoatData();
		this.firePropertyChange(Boat.BOAT_UPDATE, null, boat.getSpeed());
		//- Update the state depending on the model contents.
		this.setState(ModelStates.CONFIGURED);
		if ((null != boatid) && (null != clef)) {
			this.setState(ModelStates.READY);
			errorMessage = null;
		}
	}

	/**
	 * Sets the state of this boat depending on the content of the authentication fields and the other fields
	 * and current state.
	 */
	private void checkAuthenticationState() {
		boolean authenticated = true;
		if (this.testStringNullness(clef)) authenticated = false;
		if (this.testStringNullness(boatid)) authenticated = false;
		if (this.testStringNullness(email)) authenticated = false;
		if (authenticated)
			this.setState(ModelStates.READY);
		else this.setState(ModelStates.CONFIGURED);
	}

	private boolean testStringNullness(final String toTest) {
		if (null == toTest) return true;
		if (toTest.equals("")) return true;
		if (toTest.trim().equals("")) return true;
		return false;
	}

	/** Promotes the dirty state to the parent and up to the final container. */
	void setDirty(final boolean dirtyState) {
		if (null != parent) if (parent instanceof PilotModelStore) ((PilotModelStore) parent).setDirty(dirtyState);
	}
}

// - UNUSED CODE ............................................................................................
