//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.exceptions.InvalidContentException;
import net.sf.vorg.core.enums.InputHandlerStates;
import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class XMLFileParser extends ABoatRouteParser {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public XMLFileParser(final String targetInput) {
		super(targetInput);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public InputTypes getType() {
		return InputTypes.XML;
	}

	/**
	 * Process the data contents of the configured VRTool file and parse the contents to load and update the
	 * data inside the Pilot Model.
	 */
	public boolean loadContents() {
		if (needsReload())
			try {
				System.out
						.println("------------------------------------------------------------------------------------------");
				System.out.println(Calendar.getInstance().getTime() + " - new scan run for configuration: " + getFilePath());
				logger.fine("Model needs reload. Parsing again XML data.");
				final InputStream stream = new BufferedInputStream(new FileInputStream(inputReference));
				final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
				parser.parse(stream, this);
				final byte[] localHash = computeHash();
		logger.fine("Computed hash for file [" + inputReference + "] is " + hexEncode(localHash));
		hash = localHash;
				lastUpdate = Calendar.getInstance();
				return true;
			} catch (final FileNotFoundException fnfe) {
				System.out.println("EEE - " + fnfe.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, fnfe);
				return false;
			} catch (final DataLoadingException dle) {
				System.out.println("EEE - " + dle.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, dle);
				return false;
			} catch (final ParserConfigurationException pce) {
				System.out.println("EEE - " + pce.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, pce);
				return false;
			} catch (final SAXException saxe) {
				System.out.println("EEE - " + saxe.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, saxe);
				return false;
			} catch (final IOException ioe) {
				System.out.println("EEE - " + ioe.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, ioe);
				return false;
			}
		else {
			System.out.println("------------------------------------------------------------------------------------------");
			System.out.println(Calendar.getInstance().getTime() + " - input route configuration not reloaded.");
			return false;
		}
	}

	/** Parse the configuration file and store the parameters into the single boat instance. */
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		// - Get the boat information to be managed.
		if (name.toLowerCase().equals("boat"))
			try {
			//- Check if this boat is already on the store and the clear the list of commands.
			final String boatName = validateNotNull(attributes, "name");
			final PilotBoat existent = boatStore.searchBoatName(boatName);
			if (null == existent) {
				boatConstruction = new PilotBoat();
				boatConstruction.setBoatName(validateNotNull(attributes, "name"));
				boatConstruction.setBoatId(validateNull(attributes, "boatid"));
					boatConstruction.setBoatEmail(validateNull(attributes, "email"));
				boatConstruction.setBoatClef(validateNull(attributes, "clef"));
				boatStore.buildBoat(boatConstruction);
			} else {
				//- Because we do not create the boat we have to assign it directly to the boatConstuction.
				boatConstruction = existent;
				boatConstruction.setBoatName(validateNotNull(attributes, "name"));
				boatConstruction.setBoatId(validateNull(attributes, "boatid"));
					boatConstruction.setBoatEmail(validateNull(attributes, "email"));
				boatConstruction.setBoatClef(validateNull(attributes, "clef"));
				boatConstruction.clearCommands();
			}

			//- Update the boat information for the game server
			boatConstruction.updateBoatData();
		} catch (final InvalidContentException ice) {
			throw new SAXException(ice);
		} catch (final DataLoadingException dle) {
			//- No nothing so this boat is removed from the processing.
		} catch (final BoatNotFoundException bnfe) {
			throw new SAXException(bnfe);
		}
		super.startElement(name, attributes);
	}
}
// - UNUSED CODE ............................................................................................
