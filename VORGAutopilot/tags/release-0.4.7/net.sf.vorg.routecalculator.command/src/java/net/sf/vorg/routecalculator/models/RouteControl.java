//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Limits;

//- CLASS IMPLEMENTATION ...................................................................................
public class RouteControl {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected GeoLocation	location	= new GeoLocation();	;
	protected Directions	direction	= Directions.NS;
	protected RouteCell		left;
	protected RouteCell		right;
	protected GeoLocation	upLocation;
	protected GeoLocation	downLocation;
	// protected GeoLocation directLocation;
	/** Stores the last optimized location to be used as the start point for next iterations. */
	private GeoLocation		optimizedLocation;
	private Limits				controlLimits;

	// private int controlIdentifier;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/** Creates a new control that connect two wind cells, setting a direction and a control location. */
	public RouteControl(final GeoLocation location, final Directions setDirection, final RouteCell leftNode,
			final RouteCell rightNode) {
		this.location = location;
		upLocation = location;
		downLocation = location;
		this.optimizedLocation = location;
		this.direction = setDirection;
		if (setDirection == Directions.N) this.direction = Directions.EW;
		if (setDirection == Directions.S) this.direction = Directions.EW;
		if (setDirection == Directions.E) this.direction = Directions.NS;
		if (setDirection == Directions.W) this.direction = Directions.NS;
		this.left = leftNode;
		this.right = rightNode;
		if (direction == Directions.NS) controlLimits = this.left.getCell().getCeiling();
		if (direction == Directions.EW) controlLimits = this.left.getCell().getWalls();

		// - Register the control inside the route nodes.
		leftNode.registerControlLeft(this);
		rightNode.registerControlRight(this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getCurrentLocation() {
		return location;
	}

	public Directions getDirection() {
		return this.direction;
	}

	public double getFixedLat() {
		return this.left.getExitLocation().getLat();
	}

	public double getFixedLon() {
		return this.left.getExitLocation().getLon();
	}

	public Limits getLimits() {
		return this.controlLimits;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[RouteControl ");
		buffer.append("direction=").append(this.direction).append(", ");
		buffer.append("setup location=").append(this.location).append("");
		// buffer.append("\n              ").append("left location=").append(this.left.getCell().getLocation()).append("");
		// buffer.append("\n              ").append("right location=").append(this.right.getCell().getLocation()).append("");
		buffer.append("\n              ").append("optimizedLocation=").append(this.optimizedLocation).append("]");
		return buffer.toString();
	}

	public double updateLeft(final GeoLocation newLocation) {
		this.left.setExitLocation(newLocation);
		return this.left.getTTC();
	}

	public double updateRight(final GeoLocation newLocation) {
		this.right.setEntryLocation(newLocation);
		return this.right.getTTC();
	}

	public void storeLocation(GeoLocation newLocation) {
		this.updateLeft(newLocation);
		this.updateRight(newLocation);
		location = newLocation;
	}

	public double getLeftTTC() {
		return this.left.getTTC();
	}

	public void setLeft(RouteCell newLeft) {
		left = newLeft;
		left.registerControlLeft(this);
	}

	public void setRight(RouteCell newRigth) {
		right = newRigth;
		right.registerControlRight(this);
	}

	/**
	 * Decrements the upLocation for latitude or longitude dependoing on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustUp(int controlId) {
		if (Directions.NS == direction) {
			// FIXME Move the up location one step up.
			upLocation.setLat(upLocation.getLat() - Route.ITERATION_INCREMENT);
			final Limits iterationLimits = this.getLimits();
			if (upLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with " + upLocation.toReport().replace('\t', ' '));
			this.updateLeft(upLocation);
			this.updateRight(upLocation);
			location = upLocation;

			double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			upLocation.setLon(upLocation.getLon() - Route.ITERATION_INCREMENT);
			final Limits iterationLimits = this.getLimits();
			if (upLocation.getLon() < iterationLimits.getWest()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with " + upLocation.toReport().replace('\t', ' '));
			this.updateLeft(upLocation);
			this.updateRight(upLocation);
			location = upLocation;

			double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		return false;
	}

	/**
	 * Increments the downLocation for latitude or longitude depending on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustDown(int controlId) {
		if (Directions.NS == direction) {
			// - Move the up location one step left.
			downLocation.setLat(downLocation.getLat() + Route.ITERATION_INCREMENT);
			final Limits iterationLimits = this.getLimits();
			if (downLocation.getLat() > iterationLimits.getNorth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with " + downLocation.toReport().replace('\t', ' '));
			this.updateLeft(downLocation);
			this.updateRight(downLocation);
			location = downLocation;

			double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			downLocation.setLon(downLocation.getLon() + Route.ITERATION_INCREMENT);
			final Limits iterationLimits = this.getLimits();
			if (downLocation.getLon() > iterationLimits.getEast()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with " + downLocation.toReport().replace('\t', ' '));
			this.updateLeft(downLocation);
			this.updateRight(downLocation);
			location = downLocation;

			double leftTTC = this.getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		return false;
	}

	public GeoLocation getOptimizedLocation() {
		return optimizedLocation;
	}

	public void setOptimizedLocation(GeoLocation newLocation) {
		optimizedLocation = newLocation;
	}

	/** Sets the start point for the up and down locations to the rounded value of the optimized location. */
	public void reset() {
		upLocation = new GeoLocation(Math.round(optimizedLocation.getLat() * 100.0) / 100.0, Math.round(optimizedLocation
				.getLon() * 100.0) / 100.0);
		downLocation = new GeoLocation(Math.round(optimizedLocation.getLat() * 100.0) / 100.0, Math.round(optimizedLocation
				.getLon() * 100.0) / 100.0);
	}
}
// - UNUSED CODE ............................................................................................
// private boolean performAdjust(int controlId, double increment) {
// return true;
// }
// @Override
// public RouteControl clone() {
// return new RouteControl(this.location, this.direction, this.left, this.right);
// }
// public double getElapsed() {
// return this.left.getTTC() + this.right.getTTC();
// }
// public double getRightTTC() {
// return this.right.getTTC();
// }
// public ProxyCell getLeft() {
// return left;
// }
//
// public ProxyCell getRight() {
// return right;
// }
// public RouteControl(final GeoLocation location, final Directions direction) {
// this.location = location;
// // this.directLocation = location;
// this.optimizedLocation = location;
// this.direction = direction;
// // for (int index = 0; index < 32; index++) {
// // savedConfiguration.add(new GeoLocation(0.0, 0.0));
// // }
// }
// public Vector<GeoLocation> getSavedConfiguration() {
// return savedConfiguration;
// }
// public String printOptimizedData() {
// final StringBuffer buffer = new StringBuffer("[Optim ");
// buffer.append("location=").append(this.optimizedLocation).append("]");
// return buffer.toString();
// }
// public String printReport(final boolean start) {
// // - Update cell calculation before printing the data.
// this.updateLeft(this.optimizedLocation);
// this.updateRight(this.optimizedLocation);
// final StringBuffer buffer = new StringBuffer();
// if (start) {
// buffer.append("START").append('\t');
// buffer.append(this.left.entryLocation.toReport()).append('\n');
// }
// buffer.append("W-").append(this.direction).append('\t');
// buffer.append(this.left.exitLocation.toReport()).append('\t');
// buffer.append("alpha ").append(this.left.getAlpha()).append('\t');
// buffer.append("speed ").append(this.left.getSpeed()).append('\t');
// buffer.append("distance ").append(this.left.getDistance()).append('\t');
// buffer.append("ETA ").append(this.left.getTTC()).append('\n');
// return buffer.toString();
// }
// public void setOptimizedLocation(final GeoLocation newLocation) {
// this.optimizedLocation = newLocation;
// }
// //FIXME This method can be deleted. It is not used.
// public void reset() {
// // this.optimizedLocation = directLocation;
// }
// public void saveControlConfiguration(int controlId, Vector<RouteControl> controls) {
// for (int index = controlId; index < controls.size(); index++) {
// savedConfiguration.set(index, controls.get(index).getOptimizedLocation());
// }
// }
// private boolean processLatitude(GeoLocation target, final double elapsed) {
// }
// private boolean processLongitude(final double startPoint, final double elapsed) {
// final Limits iterationLimits = this.getLimits();
// // final double fixedLat = control.getFixedLat();
// // final long iterations = this.levelIterations(controlId);
// // boolean northFound = false;
// // boolean southFound = false;
// // for (int counter = 0; counter < iterations; counter++) {
// // // - Calculate the control position for the port side.
// // iterationCounter++;
// // double controlRange = upLocation.getLon() - Route.ITERATION_INCREMENT;
// if (startPoint < iterationLimits.getSouth()) return false;
// GeoLocation newLocation = new GeoLocation(this.upLocation.getLat(), startPoint);
// if (controlIdentifier < 1)
// System.out.println("Iterating level " + controlIdentifier + " with " +
// newLocation.toReport().replace('\t', ' '));
// return this.calculateRouteTime(controlIdentifier, this, newLocation, elapsed);
// // if (controlRange == iterationLimits.getSouth()) northFound = true;
// //
// // // - Calculate the control position for the starboard side.
// // iterationCounter++;
// // controlRange = startPoint + Route.ITERATION_INCREMENT * counter;
// // if (controlRange > iterationLimits.getNorth()) controlRange = iterationLimits.getNorth();
// // newLocation = new GeoLocation(fixedLat, controlRange);
// // if (controlId < 1)
// // System.out.println("Iterating level " + controlId + " with " + newLocation.toReport().replace('\t',
// // ' '));
// // if (!southFound) this.calculateRouteTime(controlId, control, newLocation, elapsed);
// // if (controlRange == iterationLimits.getNorth()) southFound = true;
// // }
// }
// /** New function that test for wind shift and then average the result before giving the cell. */
// private boolean calculateRouteTime(final int controlId, final RouteControl control, final GeoLocation
// newLocation,
// final double elapsed) {
// // FIXME This is not the right update to the data necessary. test another way
// control.updateLeft(newLocation);
// control.updateRight(newLocation);
// // setOptimizedLocation(newLocation);
//
// double leftTTC = control.getLeftTTC();
// if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId, elapsed);
// return true;
// }
// [01]
// private boolean lastControl(int controlId) {
// // FIXME Set a way to know it this is the last control.
// return true;
// }
// public void saveControlConfiguration(GeoLocation newLocation, int controlId, Vector<RouteControl>
// controls) {
// // - Get control saved on next level.
// try {
// RouteControl nextControl = controls.get(controlId + 1);
// for (int index = controlId; index < controls.size(); index++) {
// savedConfiguration.set(index, nextControl.savedConfiguration.get(index));
// }
// } catch (final ArrayIndexOutOfBoundsException aioobe) {
// // - Save configuration of last control.
// savedConfiguration.set(controlId, newLocation);
// } finally {
// savedConfiguration.set(controlId, newLocation);
// }
// }
