//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import main.RouteCalculator;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.core.RouterType;

// - CLASS IMPLEMENTATION ...................................................................................
public class IsochroneRouter extends Router {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger			= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private String				outputFile	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsochroneRouter(RouterType type) {
		super(type);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void generateIsochrone(SpeedVector vector) {
		generateIsochroneData(vector, 1.0 * 60.0, "ISO+1h");
		generateIsochroneData(vector, 2.0 * 60.0, "ISO+2h");
		generateIsochroneData(vector, 3.0 * 60.0, "ISO+3h");
	}

	private void generateIsochroneData(SpeedVector point, double time, String name) {
		Vector<GeoLocation> isochrone = new Vector<GeoLocation>(50);
		// GeoLocation target = new GeoLocation();
		// - Iterate for 50 degrees, 25 each side of heading
		for (int deviation = -60; deviation <= 60; deviation++) {
			int course = GeoLocation.adjustAngleTo360(point.getHeading() + deviation);
			// double maxSpeed = 0.0;
			try {
				WindCell cell = WindMapHandler.getWindCell(point.getLocation());
				SailConfiguration polar = Polars.lookup(GeoLocation.calculateAWD(cell.getWindDir(), course), cell
						.getWindSpeed());
				GeoLocation newLocation = point.getLocation().directEstimation(time * 60.0, polar.getSpeed(), course);
				// if (polar.getSpeed() > maxSpeed) {
				// target = newLocation;
				// maxSpeed = polar.getSpeed();
				// }
				isochrone.add(newLocation);
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// - Send the collected data to the NAV file
		if (null != outputFile) send2VRTool(isochrone, name);
		// return target;
	}

	private void send2VRTool(Vector<GeoLocation> isochrone, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			for (Iterator<GeoLocation> iterator = isochrone.iterator(); iterator.hasNext();) {
				GeoLocation point = iterator.next();
				buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLat())).append(";");
				buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
				buffer.append(VORGConstants.NEWLINE);
			}
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// public String vrtoolReport(GeoLocation point) {
	// final StringBuffer buffer = new StringBuffer("P; ");
	// buffer.append(FormatSingletons.nf4.format(point.getLat())).append(";");
	// buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
	// return buffer.toString();
	// }

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	/**
	 * With the start point and the final waypoint, this method calculates the routes for the different VMGs to
	 * both sides of the resulting angle. Those routes are divided on the wind change points and those points
	 * are recorded to remember the route that has advanced more in the general direction of the start-waypoint
	 * angle.
	 */
	public void generateIsochrone(GeoLocation start, GeoLocation waypoint) {
		// - Calculate the base angle
		double baseCourse = start.angleTo(waypoint);
		Vector<GeoLocation> isochrone1 = new Vector<GeoLocation>(50);
		Vector<GeoLocation> isochrone2 = new Vector<GeoLocation>(50);
		double maxFirstDistance = 0.0;
		Route firstRoute = null;
		double maxSecondDistance = 0.0;
		Route secondRoute = null;

		// - Generate the routes using another Router
		for (int deviation = -45; deviation <= 45; deviation++) {
			// if (activeVMG) {
			// - Generate the VMG route if requested.
			final Finder theRouter = new Finder();
			int course = new Double(baseCourse + deviation).intValue();
			final Route vmgRoute = theRouter.getVMGRoute(start, 30, course);
			vmgRoute.setName("VMG Route - " + course);

			// - Get the first and second wind change locations
			GeoLocation firstLocation = vmgRoute.getWindChange(1);
			GeoLocation secondLocation = vmgRoute.getWindChange(2);
			isochrone1.add(firstLocation);
			isochrone2.add(secondLocation);

			// - Calculate the distances from the start to this points to get the isochrones.
			double destAngle = start.angleTo(firstLocation);
			double destDistance = start.distance(firstLocation);
			double projectionAngle = Math.abs(destAngle - baseCourse);
			double projectedDistance = destDistance * Math.cos(Math.toRadians(projectionAngle));
			if (projectedDistance > maxFirstDistance) {
				maxFirstDistance = projectedDistance;
				firstRoute = vmgRoute;
			}
			destAngle = start.angleTo(secondLocation);
			destDistance = start.distance(secondLocation);
			projectionAngle = Math.abs(destAngle - baseCourse);
			projectedDistance = destDistance * Math.cos(Math.toRadians(projectionAngle));
			if (projectedDistance > maxSecondDistance) {
				maxSecondDistance = projectedDistance;
				secondRoute = vmgRoute;
			}
		}
		RouteCalculator.output(firstRoute.printReport());
		RouteCalculator.output(secondRoute.printReport());
		// - Send the collected data to the NAV file
		if (null != outputFile) {
			send2VRTool(isochrone1, "ISO1");
			send2VRTool(isochrone2, "ISO2");
			firstRoute.setName(firstRoute.getName() + " FIRST");
			secondRoute.setName(secondRoute.getName() + " SECOND");
			if (null != firstRoute) send2VRTool(firstRoute, firstRoute.getName());
			if (null != secondRoute) send2VRTool(secondRoute, secondRoute.getName());
		}
	}

	private void send2VRTool(final Route awdRoute, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(this.outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CCFF").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			buffer.append(awdRoute.vrtoolReport());
			buffer.append(VORGConstants.NEWLINE);
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
