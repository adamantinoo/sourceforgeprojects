//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: IDetailedFigure.java 137 2008-05-09 07:10:00Z boneymen $
//  LAST UPDATE:    $Date: 2008-05-09 09:10:00 +0200 (vie, 09 may 2008) $
//  RELEASE:        $Revision: 137 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface IDetailedFigure {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void refreshContents();
}

// - UNUSED CODE ............................................................................................
