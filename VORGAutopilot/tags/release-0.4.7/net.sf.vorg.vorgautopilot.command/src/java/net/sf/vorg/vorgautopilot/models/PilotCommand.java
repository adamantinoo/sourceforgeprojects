//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.AbstractXMLHandler;
import net.sf.core.exceptions.InvalidContentException;
import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.vorgautopilot.core.CommandStatus;
import net.sf.vorg.vorgautopilot.core.CommandTypes;
import net.sf.vorg.vorgautopilot.core.WaypointTypes;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This class processes the pilot command section of the boat configuration file and generates the set of
 * commands and waypoints that describe the new boat instructions.
 */
public class PilotCommand extends AbstractGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger							= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final long		serialVersionUID		= -5791829360139561174L;

	// - F I E L D - S E C T I O N ............................................................................
	private CommandTypes				type								= CommandTypes.NOCOMMAND;
	private Vector<PilotLimits>	limits							= new Vector<PilotLimits>();
	private Vector<Waypoint>		waypointList				= new Vector<Waypoint>();
	private ActiveWaypoint			activeWaypoint			= null;
	public static final String	WAYPOINT_ACTIVATED	= "PilotCommand.WAYPOINT_ACTIVATED";
	public static final String	STRUCTURE_CHANGED		= "PilotCommand.STRUCTURE_CHANGED";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommand(final String typeName) {
		type = CommandTypes.decodeType(typeName);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addLimit(final PilotLimits newLimit) {
		if (null != newLimit) limits.add(newLimit);
	}

	public void addWaypoint(final Waypoint newWaypoint) {
		waypointList.add(newWaypoint);
		addChild(newWaypoint);
		newWaypoint.setState(WaypointStates.NOACTIVE);
	}

	public void cleanActiveWaypoint() {
		if (null != activeWaypoint) activeWaypoint.deactivate();
		activeWaypoint = null;
	}

	public String generatePersistentXML() {
		final StringBuffer buffer = new StringBuffer();
		//- Iterate though the children to dump also all other persistent information.
		buffer.append("      <pilotcommand").append(" type=").append(this.quote(type.toString())).append(">").append(
				VORGConstants.NEWLINE);
		buffer.append("      <pilotlimits>").append(VORGConstants.NEWLINE);
		final Iterator<PilotLimits> lit = limits.iterator();
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			buffer.append(limit.generatePersistentXML());
		}
		buffer.append("      </pilotlimits>").append(VORGConstants.NEWLINE);
		buffer.append("      <waypointlist>").append(VORGConstants.NEWLINE);
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint waypoint = wit.next();
			buffer.append(waypoint.generatePersistentXML("waypoint"));
		}
		buffer.append("      </waypointlist>").append(VORGConstants.NEWLINE);
		buffer.append("      </pilotcommand>").append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	public ActiveWaypoint getActiveWaypoint() {
		return activeWaypoint;
	}

	public Vector<Waypoint> getWaypoints() {
		return waypointList;
	}

	/**
	 * Perform the pilot activity on the associated Boat. If the command has an active waypoint, choose it for
	 * execution. Otherwise scan to select the next waypoint.
	 */
	public void pilot() {
		//		//- Generate the printing report.
		//		StringBuffer reportBuffer = new StringBuffer(VORGConstants.NEWLINE);
		System.out.println();
		if (type == CommandTypes.NOCOMMAND) {
			System.out.println(Calendar.getInstance().getTime() + " - " + "Not processing. Command not Active" + this);
			return;
		}
		if (type == CommandTypes.ROUTE) if ((null != parent) && (parent instanceof PilotBoat)) {
			printReport();
			//- If there is an active waypoint. Update the contents and perform the calculations.
			if (null != activeWaypoint) {
				//- Check if this point has been surpassed. Is so cancel it and search for a new one.
				if (activeWaypoint.isActive()) {
					final Boat refBoat = ((PilotBoat) parent).getBoat();
					// - Make the waypoint to check its own surpassed algorithm.
					if (activeWaypoint.checkSurpassed(refBoat.getLocation())) {
						refBoat.surpassWaypoint(activeWaypoint);
						activeWaypoint = null;
						pilot();
						fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
					} else activeWaypoint.activateWaypoint();
				} else {
					//- The active waypoint is no more active. Clear it and for for another waypoint.
					activeWaypoint = null;
					pilot();
					fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
				}
			} else evaluateWaypoints(((PilotBoat) parent).getBoat());
		} else System.out.println(Calendar.getInstance().getTime() + " - " + "Invalid data for command processing");
	}

	public void startElement(final String name, final Attributes attributes) throws SAXException {
		if (name.toLowerCase().equals("pilotlimits")) limits = new Vector<PilotLimits>();
		if (name.toLowerCase().equals("limit")) {
			final PilotLimits newLimit = new PilotLimits();
			try {
				newLimit.setBorder(AbstractXMLHandler.svalidateNotNull(attributes, "border"));
				newLimit.setLatitude(AbstractXMLHandler.svalidateNull(attributes, "latitude"));
				newLimit.setLongitude(AbstractXMLHandler.svalidateNull(attributes, "longitude"));
				newLimit.setAction(AbstractXMLHandler.svalidateNull(attributes, "action"));
				limits.add(newLimit);
			} catch (final InvalidContentException ice) {
				System.out.println("EEE - " + ice.getLocalizedMessage());
			}
		}
		if (name.toLowerCase().equals("waypointlist")) waypointList = new Vector<Waypoint>();
		if (name.toLowerCase().equals("waypoint"))
			try {
				final WaypointTypes type = WaypointTypes.decodeType(AbstractXMLHandler.svalidateNotNull(attributes, "type"));
				final Waypoint waypoint = new Waypoint(type);
				waypoint.setName(AbstractXMLHandler.svalidateNull(attributes, "name"));
				waypoint.setLatitude(AbstractXMLHandler.svalidateNotNull(attributes, "latitude"));
				waypoint.setLongitude(AbstractXMLHandler.svalidateNotNull(attributes, "longitude"));
				waypoint.setRange(AbstractXMLHandler.svalidateNull(attributes, "range"));
				waypoint.setMinAWD(AbstractXMLHandler.svalidateNull(attributes, "minAWD"));
				waypoint.setMaxAWD(AbstractXMLHandler.svalidateNull(attributes, "maxAWD"));
				waypoint.setAngleParameter(AbstractXMLHandler.svalidateNull(attributes, "angle"));
				if (type == WaypointTypes.ANGLE)
					waypoint.setAngleParameter(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				if (type == WaypointTypes.VMG)
					waypoint.setAngleParameter(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				if (type == WaypointTypes.AWD)
					waypoint.setSelectedAWD(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				addWaypoint(waypoint);
			} catch (final InvalidContentException ice) {
				System.out.println("EEE - " + ice.getLocalizedMessage());
			}
	}

	/** Checks if this command is active or not depending on the state and the actions of its limits. */
	public CommandStatus testActivity() {
		// - Check if the command is active or inactive by limits.
		CommandStatus status = CommandStatus.NOCHANGE;
		//- If a command has no limits it is considered active.
		if (limits.size() == 0) return CommandStatus.GO;
		final Iterator<PilotLimits> lit = limits.iterator();
		GeoLocation boatLocation = null;
		if (null != parent) if (parent instanceof PilotBoat) boatLocation = ((PilotBoat) parent).getBoat().getLocation();
		if (null == boatLocation) return CommandStatus.NOGO;
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			//- Check this limit to the current boat location, that was obtained before.
			final CommandStatus test = limit.testLimit(boatLocation);
			if (test != CommandStatus.NOCHANGE) status = test;
		}
		return status;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[PilotCommand ");
		buffer.append("type=").append(type).append("\n");
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			buffer.append("").append(wp).append("\n");
		}
		return buffer.toString();
	}

	private void evaluateWaypoints(final Boat boat) {
		// - Get the current boat location.
		final GeoLocation boatLocation = boat.getLocation();
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			//- Send the boat to the waypoint for internal reference.
			wp.setBoat(boat);
			// - Test if the waypoint has been surpassed by checking the points of the boat route
			if (boat.checkSurpassed(wp)) wp.setState(WaypointStates.SURPASSED);
			// - Check if the waypoint is still active.
			if (wp.isActive()) {
				//- Make the waypoint to check its own surpassed algorithm.
				if (wp.checkSurpassed(boatLocation)) {
					boat.surpassWaypoint(wp);
					continue;
				}
				activeWaypoint = wp.activateWaypoint();
				firePropertyChange(PilotCommand.WAYPOINT_ACTIVATED, null, activeWaypoint);
				break;
			}
		}
	}

	private void printReport() {
		System.out.println("==========================================================================================");
		System.out.println(Calendar.getInstance().getTime() + " - " + "Processing Boat -- "
				+ ((PilotBoat) parent).getBoatName().toUpperCase());
		System.out.println(((PilotBoat) parent).getBoat().printReport());
		System.out.println();
		System.out.println("Processing configuration command" + this);
	}
}
// - UNUSED CODE ............................................................................................
