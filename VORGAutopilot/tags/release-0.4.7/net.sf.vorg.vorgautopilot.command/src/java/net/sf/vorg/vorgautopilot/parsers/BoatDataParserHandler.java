//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen$
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008)$
//  RELEASE:        $Revision: 174$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.AbstractXMLHandler;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatDataParserHandler extends AbstractXMLHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger						= Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat		boatRef;
	private String				elementData				= "";
	private boolean				ended							= false;
	private final boolean	processingFriends	= false;
	private Boat					buildFriend				= new Boat();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatDataParserHandler(Boat boat) {
		boatRef = boat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		//- Detect when the boat is not on the game database
		if (name.toLowerCase().equals("error")) throw new BoatNotFoundException("Boat not found. ");

		// - Start processing the list of friends.
		//		if (name.toLowerCase().equals("amis")) processingFriends = true;
		if (name.toLowerCase().equals("boat")) if (processingFriends) buildFriend = new Boat();
		if (name.toLowerCase().equals("position")) boatRef.setLastUpdateDate(validateNotNull(attributes, "date"));
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		StringBuffer realData = new StringBuffer();
		realData.append(ch);
		elementData = realData.subSequence(start, start + length).toString();
	}

	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {
		super.endElement(uri, localName, name);
		if (processingFriends) {
			if (name.toLowerCase().equals("pseudo")) buildFriend.setName(elementData);
			if (name.toLowerCase().equals("latitude")) buildFriend.setLatitude(elementData);
			if (name.toLowerCase().equals("longitude")) buildFriend.setLongitude(elementData);
			if (name.toLowerCase().equals("voile")) buildFriend.setSail(elementData);
			if (name.toLowerCase().equals("vitesse")) buildFriend.setSpeed(new Double(elementData).doubleValue());
			if (name.toLowerCase().equals("wind_speed")) buildFriend.setWindSpeed(new Double(elementData).doubleValue());
			if (name.toLowerCase().equals("wind_angle")) buildFriend.setWindAngle(new Integer(elementData).intValue());
			if (name.toLowerCase().equals("cap")) buildFriend.setHeading(new Integer(elementData).intValue());
			// - Use this tag to detect the end of the friend.
			//			if (name.toLowerCase().equals("classement")) boatRef.addFriend(buildFriend);
		} else {
			if (ended) return;
			// - Terminate the parsing
			if (name.toLowerCase().equals("position")) ended = true;
			if (name.toLowerCase().equals("latitude")) boatRef.setLatitude(elementData);
			if (name.toLowerCase().equals("longitude")) boatRef.setLongitude(elementData);
			if (name.toLowerCase().equals("voile")) boatRef.setSail(elementData);
			if (name.toLowerCase().equals("vitesse")) boatRef.setSpeed(new Double(elementData).doubleValue());
			if (name.toLowerCase().equals("wind_speed")) boatRef.setWindSpeed(new Double(elementData).doubleValue());
			if (name.toLowerCase().equals("wind_angle")) boatRef.setWindAngle(new Integer(elementData).intValue());
			if (name.toLowerCase().equals("cap")) boatRef.setHeading(new Integer(elementData).intValue());
			if (name.toLowerCase().equals("classement")) boatRef.setRanking(new Integer(elementData).intValue());
			//			if (name.toLowerCase().equals("IsArrived"))// - If the boat has arrived to destination, switch off the autopilot.
			//			int value = new Integer(elementData).intValue();
			//				if (value == 1) XMLAutopilot.setTerminate(true);
		}
	}
}

// - UNUSED CODE ............................................................................................
