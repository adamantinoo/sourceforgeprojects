//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.Map;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Insets;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.Node;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractDirectedNodeEditPart extends AbstractNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.gef.core.editparts");
	public static final Insets	PADDING	= new Insets(4, 2, 4, 2);

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractDirectedNodeEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Dimension getSize() {
		return new Dimension(getFigure().getPreferredSize());
	}

	protected void applyGraphResults(final DirectedGraph graph, final Map<AbstractDirectedNodeEditPart, Node> map) {
		try {
			final Node node = map.get(this);
			getFigure().setSize(node.width, node.height);
			getFigure().setBounds(new Rectangle(node.x, node.y, node.width, node.height));
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
