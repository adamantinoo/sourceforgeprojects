//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.exceptions;

// - IMPORT SECTION .........................................................................................
import org.xml.sax.SAXException;

// - CLASS IMPLEMENTATION ...................................................................................
public class DataLoadingException extends SAXException {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= -7963243319897974664L;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DataLoadingException(String message) {
		super(message);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
