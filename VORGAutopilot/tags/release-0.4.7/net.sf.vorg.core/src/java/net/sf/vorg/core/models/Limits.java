//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class Limits {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core.models");
	public static final String	NORTH_SOUTH			= "NS";
	public static final String	EAST_WEST				= "EW";

	// - F I E L D - S E C T I O N ............................................................................
	private String							type						= Limits.NORTH_SOUTH;
	private double							westSouthlimit	= 0.0;
	private double							eastNorthLimit	= 0.0;

	public Limits(final double left, final double right) {
		westSouthlimit = left;
		eastNorthLimit = right;
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Limits(final String type, final double left, final double right) {
		this.type = type;
		westSouthlimit = left;
		eastNorthLimit = right;
	}

	public double getEast() {
		return eastNorthLimit;
	}

	public double getNorth() {
		return eastNorthLimit;
	}

	public double getSouth() {
		return westSouthlimit;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double getWest() {
		return westSouthlimit;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[WindCell ");
		buffer.append("type=").append(type).append(",");
		buffer.append("WestSouth=").append(westSouthlimit).append(",");
		buffer.append("EastNorth=").append(eastNorthLimit).append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
