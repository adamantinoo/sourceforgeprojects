//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;
import es.ftgroup.ui.figures.IFigureFactory;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.EditPart;

import net.sf.vgap4.assistant.figures.DetailedBaseFigure;
import net.sf.vgap4.assistant.figures.DetailedPlanetFigure;
import net.sf.vgap4.assistant.figures.DetailedPodFigure;
import net.sf.vgap4.assistant.figures.DetailedShipFigure;
import net.sf.vgap4.assistant.figures.DetailedWingFigure;
import net.sf.vgap4.assistant.models.Base;
import net.sf.vgap4.assistant.models.Planet;
import net.sf.vgap4.assistant.models.Pod;
import net.sf.vgap4.assistant.models.Ship;
import net.sf.vgap4.assistant.models.Wing;
import net.sf.vgap4.assistant.models.facets.WingDetailedFacet;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedFigureFactory implements IFigureFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.factories");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public DetailedFigureFactory() {
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Figure createFigure(EditPart part, IGEFModel unit) {
		//		if (unit instanceof Sector) { return new DetailedDefaultFigure(); }
		if (unit instanceof Planet) { return new DetailedPlanetFigure((Planet) unit); }
		if (unit instanceof Base) { return new DetailedBaseFigure((Base) unit); }
		if (unit instanceof Ship) { return new DetailedShipFigure((Ship) unit); }
		if (unit instanceof Pod) { return new DetailedPodFigure((Pod) unit); }
		if (unit instanceof Wing) { return new DetailedWingFigure(new WingDetailedFacet((Wing) unit)); }
		// If Shapes gets extended the conditions above must be updated
		throw new IllegalArgumentException();
	}

	public PolylineConnection createConnection(IWireModel newWire) {
		// TODO Auto-generated method stub
		return null;
	}

	public Figure createFigure(EditPart part, IGEFModel unit, String subType) {
		return this.createFigure(part, unit);
	}
}

// - UNUSED CODE ............................................................................................
