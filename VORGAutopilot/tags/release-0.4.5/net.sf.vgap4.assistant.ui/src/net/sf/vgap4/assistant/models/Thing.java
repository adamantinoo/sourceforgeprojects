//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class Thing extends AssistantNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long														serialVersionUID	= 2367870834728265299L;
	private static Logger																logger						= Logger
																																						.getLogger("net.sf.vgap4.assistant.models");

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * This is the reference to the container structure of the model. This backward reference is required to
	 * access some global map data or to interconnect with other model elements.
	 */
	private final AssistantMap													map;
	/**
	 * Structure where to store all the parsed Base information from the different turns that the user has
	 * imported inside the Assistant. Is the user responsibility to add as many turns as available to have all
	 * the data needed to create graphics or take decisions.
	 */
	private final Hashtable<Integer, AssistantTurnInfo>	turnList					= new Hashtable<Integer, AssistantTurnInfo>();
	/**
	 * Records the type of the data structures that are stored inside the properties. This will allow to manage
	 * more different data models from additional .CSV files without extending the classes the implement the
	 * model. External classes should be aware of this field to be allowed to interpret the read data model.
	 */
	private String																			dataType					= AssistantConstants.MODELTYPE_THING;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Thing(final AssistantMap map) {
		this.map = map;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Sets this information as the information for the specified turn. During the setup of this turn data, we
	 * can replace older turn data and this may happen if the user load again the same set of ,CSV files with
	 * the same or updated data. I also process the contents to update the common information that is mostly
	 * accessible and considered cached or statistical informations.<br>
	 * 
	 * @param turn
	 *          the number of the turn that has been processed as the identification got from the CVS file.
	 * @param turnInfo
	 *          Source data structure stored in a generic Properties structure. The real model meaning for that
	 *          data depends on the model type that is another field information. Parsed data that has been
	 *          filtered and mapped to the corresponding fields.
	 */
	public void addTurnInformation(final int turn, final AssistantTurnInfo turnInfo) {
		turnInfo.setTurnNumber(turn);
		turnList.put(turn, turnInfo);
		this.updateTurnReference(turn);
		this.setDataType(turnInfo.dataType);
		Thing.logger.info("Adding to " + this.getDataType() + " '" + turnInfo.getName() + "' data for turn " + turn + ".");

		//- Do any specific processing.
		this.loadCachedData(turnInfo);
	}

	public String getDataType() {
		return dataType;
	}

	@Override
	public String getField(final String key) {
		return this.getLatestInfo().getField(key);
	}

	@Override
	public int getFieldNumber(final String key) {
		return this.getLatestInfo().getFieldNumber(key);
	}

	public AssistantMap getMap() {
		if (null == map)
			return new AssistantMap();
		else
			return map;
	}

	@Override
	public int getOwner() {
		return this.getLatestInfo().getFieldNumber("Owner");
	}

	public String getRace() {
		final int owner = this.getOwner();
		if (owner > 0)
			return "[" + owner + "]-" + this.getMap().getPlayerRace(owner);
		else
			return "---";
	}

	public void setDataType(final String dataType) {
		this.dataType = dataType;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[Thing:");
		buffer.append(this.getIdentifier()).append("/");
		buffer.append(dataType).append("/");
		buffer.append(this.getLocationString()).append("/");
		buffer.append(this.getFFStatus()).append("/");
		buffer.append("]");
		return buffer.toString();
	}

	/**
	 * This method return the turn information for the highest turn number that is recorded for this object.
	 * This has not to be the same for every object in the model because some objects may be updated in a new
	 * turn while others not. The relative obsolescence for this information depends on the values of the
	 * <code>lastAvailableTurn</code> that is recorded on the <code>AssistantMap</code>.
	 */
	protected AssistantTurnInfo getLatestInfo() {
		return this.getTurnInfo(this.getLatestTurnNumber());
	}

	//	protected void processTurnInfo(AssistantTurnInfo turnInfo) {
	//	}

	/**
	 * Return the previous turn information if available. It can be that the user only has loaded a turn and we
	 * can not reference the vector size as a valid counter of the data present on the structure.
	 */
	protected AssistantTurnInfo getPreviousInfo() {
		//TODO Scan the Vector from the latest turn number downwards until another info is found.
		final int top = this.getLatestTurnNumber();
		int index = top - 1;
		while (index >= 0) {
			final AssistantTurnInfo testInfo = turnList.get(index);
			if (null != testInfo) return testInfo;
			index--;
		}
		return null;
	}

	//	@Override
	//	public ITreeFacet getTreeFacet() {
	//		return new ThingTreeFacet(this);
	//	}

	/** Locates recursively the latest declared turn information for this model element, whatever it is */
	protected AssistantTurnInfo getTurnInfo(final int turn) {
		final AssistantTurnInfo info = turnList.get(turn);
		if (null == info)
			return this.getTurnInfo(turn - 1);
		else
			return info;
	}

	protected void loadCachedData(final AssistantTurnInfo turnInfo) {
		// - Load field cached data. This data may change from turn to turn.
		this.setIdNumber(this.getLatestInfo().getIdNumber());
		this.setName(this.getLatestInfo().getName());
		this.setLocation(this.getLatestInfo().getLocation());
		this.setFFStatus(this.detectFFStatus());
	}

	private String detectFFStatus() {
		//TODO Calculate the FF status for this Thing.
		final int owner = this.getLatestInfo().getFieldNumber("Owner");
		//- If the object has no owner then set FF to UNDEFINED.
		if (0 == owner) return AssistantConstants.FFSTATUS_UNDEFINED;
		final int player = this.getMap().getPlayer();
		//	boolean isFriend = false;
		final Vector<Integer> friends = this.getMap().getFriends(player);
		final Iterator<Integer> fit = friends.iterator();
		while (fit.hasNext()) {
			if (fit.next() == owner) {
				//- The player ir recorded as a friend. Detdct this to return the right code.
				if (player == owner)
					return AssistantConstants.FFSTATUS_FRIEND;
				else
					return AssistantConstants.FFSTATUS_NEUTRAL;
			}
		}
		if (owner == player)
			return AssistantConstants.FFSTATUS_FRIEND;
		else
			return AssistantConstants.FFSTATUS_FOE;

		//	if (map.getPlayer() == this.getLatestInfo().getFieldNumber("Owner"))
		//		this.setFFStatus(AssistantConstants.FFSTATUS_FRIEND);
		//	else
		//		this.setFFStatus(AssistantConstants.FFSTATUS_FOE);
		//		logger.info("Updating Object cache. [" + turnInfo.getIdNumber() + "] - " + turnInfo.getName() + ". At location ["
		//				+ turnInfo.getLocation().x + "-" + turnInfo.getLocation().y + "]");

	}
}

// - UNUSED CODE ............................................................................................
