//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.factories;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.ui.figures.IFigureFactory;
import net.sf.gef.core.editparts.AbstractEditPartFactory;
import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Spot;
import net.sf.vgap4.projecteditor.editparts.DiagramEditPart;
import net.sf.vgap4.projecteditor.editparts.SpotEditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class AssistantEditPartFactory extends AbstractEditPartFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AssistantEditPartFactory(final IFigureFactory factory) {
		super(factory);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Maps the model structure for this project to the EditPart. It returns a generic
	 * <code>AbstractGenericEditPart</code> that is commonplace for all developments and has some factory
	 * features that allow better parameterization of figure factories.
	 * 
	 * @param modelElement
	 *          the model piece that requests the creation of a new EditPart
	 * @throws RuntimeException
	 *           if no match was found (programming error)
	 */
	@Override
	protected AbstractGenericEditPart getPartForElement(final Object modelElement) {
		if (modelElement instanceof AssistantMap) {
			AssistantEditPartFactory.logger.info("Generating EditPart for model class "
					+ modelElement.getClass().getSimpleName() + " with name [" + "VGAP4ProjectDiagram" + "]");
			return new DiagramEditPart();
		}
		//		if (modelElement instanceof Sector) {
		//			logger.info("Generating SectorEditPart for model class " + modelElement.getClass().getSimpleName()
		//					+ " with name [" + ((AssistantNode) modelElement).getName() + "]");
		//			return new SectorEditPart();
		//		}
		if (modelElement instanceof Spot) {
			AssistantNode representative = ((Spot) modelElement).getRepresentative();
			if (null != representative)
				AssistantEditPartFactory.logger.info("Generating EditPart for model class "
						+ modelElement.getClass().getSimpleName() + " with name ["
						+ ((Spot) modelElement).getRepresentative().getName() + "]");
			else {
				//TODO Test this piece of code by bloking at this point
				representative = ((Spot) modelElement).getRepresentative();
				final boolean bloking = true;
			}
			return new SpotEditPart();
		}
		throw new RuntimeException("Can't create part for model element: "
				+ ((modelElement != null) ? modelElement.getClass().getName() : "null"));
	}
}
// - UNUSED CODE ............................................................................................
