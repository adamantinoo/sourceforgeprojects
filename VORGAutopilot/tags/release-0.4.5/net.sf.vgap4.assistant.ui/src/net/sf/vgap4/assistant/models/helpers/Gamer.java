//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.helpers;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;

import net.sf.vgap4.assistant.models.AssistantTurnInfo;

// - CLASS IMPLEMENTATION ...................................................................................
public class Gamer extends AssistantTurnInfo implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger				logger						= Logger.getLogger("net.sf.vgap4.assistant.models.helpers");
	private static final long	serialVersionUID	= -3165395810500600757L;
	private static final int	ATTACK_BIT_MASK		= 0x0001;
	private Vector<Integer>		playerFriends			= new Vector<Integer>(1);
	private int								race							= -1;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Gamer(final String type) {
		super(type);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Vector<Integer> getFriends() {
		if (null == playerFriends) playerFriends = new Vector<Integer>(1);
		return playerFriends;
	}

	//[01]
	public int getRace() {
		if (-1 == race) race = this.getFieldNumber("Race");
		return race;
	}

	/**
	 * Decodes some of the player information records into accessible structures to be used during assistant
	 * operations.
	 */
	@Override
	protected void loadCachedData(final String[] fields) {
		//- Load Player name.
		idNumber = this.getFieldNumber("Number");
		name = this.getField("Name");

		//- Load race code.
		race = this.getFieldNumber("Race");
		//- Process the Attack switch.
		final String attackSwitch = this.getField("Attack Switches");
		if ((null == attackSwitch) || ("".equals(attackSwitch))) {
			//- Do nothing.
			//			playerFriends = new long[1];
		} else {
			long attack = new Long(attackSwitch).longValue();
			if (attack > 0) {
				//FIXME We need to know the number of players beforehand from the Map.
				int noPlayers = info.size();
				noPlayers = 7;
				playerFriends.clear();
				//				playerFriends = new long[noPlayers];
				for (int i = 0; i < noPlayers; i++) {
					final long attackRaceValue = attack & Gamer.ATTACK_BIT_MASK;
					if (0 == attackRaceValue) playerFriends.add(i + 1);
					//					playerFriends[i] = attackRaceValue;
					//- Skip processed bit.
					attack = attack >> 1;
				}
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
//	/**
//	 * Returns the string stored for the turn property identified by the parameter if found or the empty string
//	 * otherwise.
//	 * 
//	 * @param key
//	 *          turn property to be located.
//	 * @return the string value associated to that string property.
//	 */
//	public String getField(final String key) {
//		final String value = info.getProperty(key);
//		if (null == value) return "";
//		return value;
//	}
//	/**
//	 * Returns the evaluated integer value is anything stored for the turn property identified by the parameter
//	 * if found or the value ZERO otherwise.
//	 * 
//	 * @param key
//	 *          turn property to be located.
//	 * @return the integer value for that string property.
//	 */
//	public int getFieldNumber(final String key) {
//		try {
//			return this.convert2Integer(this.getField(key));
//		} catch (final Exception exc) {
//			//- The field is null or not convertible to a number. Return the default ZERO.
//			return 0;
//		}
//	}
//	/**
//	 * Loads the list of properties read from the CSV file and processes some of its contents to expand that
//	 * information into the instance fields for caching and easy access to coded values. Loading ir performed by
//	 * the super class while data processing is performed in another private method.
//	 * 
//	 * @param fieldNames
//	 *          the names of the data fields. Area used as keys in the association inside the Property list.
//	 * @param fields
//	 *          the array of data fields.
//	 */
//	@Override
//	public void loadData(final String[] fieldNames, final String[] fields) {
//		super.loadData(fieldNames, fields);
//	}
//	protected int convert2Integer(final String fieldValue) {
//		return Integer.parseInt(fieldValue.trim());
//	}
