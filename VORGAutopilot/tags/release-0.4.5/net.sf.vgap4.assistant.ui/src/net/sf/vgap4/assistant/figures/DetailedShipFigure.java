//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Label;
import org.eclipse.swt.SWT;

import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.figures.draw2d.StandardLabel;
import net.sf.vgap4.assistant.models.Ship;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailedShipFigure extends AbstractDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger			logger	= Logger.getLogger("net.sf.vgap4.assistant.figures");

	// - F I E L D - S E C T I O N ............................................................................
	private final Ship	shipModel;

	//	private RoundedRectangle	container;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DetailedShipFigure(final Ship model) {
		super(model);
		shipModel = model;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected void addAdditionalContents() {
		//TODO Add the hull type and name

		final MultiLabelLine passageLabel = new MultiLabelLine();
		passageLabel.addColumn("Pasajeros:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		passageLabel.addColumn(" ", SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
		final Label passageDetail = new StandardLabel(shipModel.getPassageDetail());
		final MultiLabelLine cargoLabel = new MultiLabelLine();
		cargoLabel.addColumn("Cargo:", SWT.BOLD, AbstractDetailedFigure.COLOR_BLACK);
		cargoLabel.addColumn(shipModel.getField("Cash" + " MC"), SWT.NORMAL, AbstractDetailedFigure.COLOR_BLACK);
		final Label cargoDetail = new StandardLabel(shipModel.getCargoDetail());

		//- Add labels to this element for composition.
		this.append(passageLabel);
		this.append(passageDetail);
		this.append(cargoLabel);
		this.append(cargoDetail);
		//
		//		this.layout();
	}

	//	@Override
	//	protected Image createDetailedIcon() {
	//		//- Generate the image for the current element.
	//		final IconImageFactory imageFactory = new IconImageFactory();
	//		imageFactory.setShape(IconImageFactory.SHAPE_SHIP);
	//		if (shipModel.getFFStatus().equals(AssistantConstants.FFSTATUS_FOE)) imageFactory.setEnemy(true);
	//		return imageFactory.generateImage();
	//	}
}
// - UNUSED CODE ............................................................................................
