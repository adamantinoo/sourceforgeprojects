//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.actions;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.commands.Command;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;

import net.sf.util.FileUtilities;
import net.sf.vgap4.assistant.editor.MainMapPage;
import net.sf.vgap4.assistant.models.AssistantMap;
import net.sf.vgap4.assistant.models.AssistantTurnInfo;
import net.sf.vgap4.assistant.models.BaseInformation;
import net.sf.vgap4.assistant.models.PlanetInformation;
import net.sf.vgap4.assistant.models.helpers.Gamer;
import net.sf.vgap4.assistant.ui.AssistantConstants;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This command is responsible for reading the data structures that conform a VGA planets turn information in
 * CSV format. The class inherits the structure from the abstract <code>Command</code> class and add some
 * fields and methods to perform the processing operation. All the fields and the private
 * <code>openDialog</code> methods fall in that category.
 */
public class ImportTurnDataCommand extends Command {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger					logger									= Logger.getLogger("net.sf.vgap4.projecteditor.commands");
	private final static String[]	OPEN_FILTER_NAMES				= new String[] { "CSVInfo.txt (Turn descripcion information)" };
	private final static String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.txt" };

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the Workbench Part (in this case this result to be a MainPageEditor) that is associated with
	 * this action command. The part is allocated on the moment the action is created and then passed to the
	 * launched and executed command
	 */
	private final MainMapPage			editor;
	private String								projectName;
	private String								projectPath;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ImportTurnDataCommand(final String label, final MainMapPage editor) {
		super(label);
		this.editor = editor;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean canExecute() {
		// - This command can be executed anytime.
		return true;
	}

	@Override
	public boolean canUndo() {
		// - We do not store the previous model, so the command can not be undo
		return false;
	}

	/**
	 * Key method for a command, this is in charge to execute any actions that have to be done when the command
	 * is scheduled to be executed. In this particular implementation this is the next thread of actions:
	 * <ul>
	 * <li>Open a File Selection dialog to allow the user to select the turn descriptor and the reading point
	 * where the turn files should be located.</li>
	 * <li>Create a new Turn Processing Command to perform all the conplex tasks and to be used by the Progress
	 * Monitor.</li>
	 * <li>Create and open a new Process Dialog to inform the user of a long operation and also about the
	 * process of that action.</li>
	 * </ul>
	 */
	@Override
	public void execute() {
		// - Open a file selector to select the file to import into the model.
		IWorkbenchWindow currentWindow = editor.getSite().getWorkbenchWindow();
		final String dataFileName = this.openDialog(currentWindow);
		if (null != dataFileName) {
			//			//- The user selected the cancel. Do not open the file and abort the command.
			//			return;
			//		else {
			try {
				final TurnReader turnData = new TurnReader(dataFileName, projectPath, editor.getMapModel());
				new ProgressMonitorDialog(currentWindow.getShell()).run(true, true, turnData);
			} catch (InvocationTargetException ite) {
				ite.printStackTrace();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
			//[02]
		}
		editor.setDirty(true);
		//- Signal the diagram that there have been some model changes.
		editor.getMapModel().fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);

		//[03]
	}

	private String openDialog(final IWorkbenchWindow window) {
		// - Get the user to choose an scenery file.
		final FileDialog fileChooser = new FileDialog(window.getShell(), SWT.OPEN);
		fileChooser.setText("Select the player's turn CSVInfo.txt turn info file.");
		fileChooser.setFilterPath(null);
		fileChooser.setFilterExtensions(ImportTurnDataCommand.OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(ImportTurnDataCommand.OPEN_FILTER_NAMES);
		projectName = fileChooser.open();
		projectPath = fileChooser.getFilterPath();
		return projectName;
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
//class ReadTurnWorkspaceModifyOperation extends WorkspaceModifyOperation {
//	// - F I E L D - S E C T I O N ............................................................................
//	private final String				dataFileName;
//	private final String				projectPath;
//	private final AssistantMap	mainmap;
//
//	public ReadTurnWorkspaceModifyOperation(final String dataFileName, final String projectPath,
//			final AssistantMap diagram) {
//		this.dataFileName = dataFileName;
//		this.projectPath = projectPath;
//		mainmap = diagram;
//	}
//
//	@Override
//	protected void execute(final IProgressMonitor monitor) throws CoreException, InvocationTargetException,
//			InterruptedException {
//		// - Turn information is spread into several files. Create an instance to handle those
//		try {
//			monitor.beginTask("reading turn data", 40000);
//			final TurnReader turnData = new TurnReader(dataFileName, mainmap);
//			monitor.subTask("Reading turn definition");
//			final boolean valid = turnData.readTurnDescription();
//			monitor.worked(10000);
//			if (valid) {
//				turnData.readPlayerData(projectPath, monitor);
//				turnData.readPlanetData(projectPath, monitor);
//				turnData.readBaseData(projectPath, monitor);
//				monitor.worked(10000);
//				turnData.readShipData(projectPath, monitor);
//				monitor.worked(10000);
//				turnData.readPodData(projectPath, monitor);
//				turnData.readWingData(projectPath, monitor);
//
//				//- Updated rest of model structures.
//				mainmap.updateSpotData();
//				mainmap.calculatePlanetClassification();
//
//				//- Signal the diagram that there have been some model changes.
//				mainmap.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
//				monitor.done();
//			}
//		} catch (final FileNotFoundException fnfe) {
//			// - The command has failed for some cause, but the command is not deactivated.
//			System.err.println("File <" + dataFileName + "> cannot be found.");
//			fnfe.printStackTrace();
//		} catch (final IOException ioe) {
//			// TODO Notify the user the error during the read on the file contents.
//			ioe.printStackTrace();
//		}
//	}
//}

//- CLASS IMPLEMENTATION ...................................................................................
class TurnReader implements IRunnableWithProgress {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger							= Logger.getLogger("net.sf.vgap4.projecteditor.commands");
	private static String				TYPE_POD						= "TYPE_POD";
	private static String[]			supportedCodesList	= { "<BaseIndex>", "<PlanetIndex>", "<ShipIndex>", "<PodIndex>",
			"<WingIndex>", "<PlayerIndex>"							};
	//	private static Vector fileTurnConfiguration=new Vector(5);
	//	static {
	//		fileTurnConfiguration.add({"/Pod.csv","<PodIndex>",TYPE_POD});
	//	}

	// - F I E L D - S E C T I O N ............................................................................
	private final String				descriptionFileName;
	private String							projectPath;
	private final AssistantMap	diagram;

	private String							gameName;
	private String							gameId;
	private int									turn;
	private int									player;
	private int									numSubTasks;
	private int									subTaskValue;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TurnReader(final String dataFileName, String projectPath, final AssistantMap diagram) {
		descriptionFileName = dataFileName;
		this.projectPath = projectPath;
		this.diagram = diagram;

		//- Set the Progress Bar numbers for progressive tracking.
		numSubTasks = 8;
		subTaskValue = 1000;
	}

	/**
	 * This method is called from the execution of the Progress Dialog with the Progress Monitor created for the
	 * operation tracking. This code is actually moved from other methods and classes to this single location
	 * spot.
	 */
	public void run(IProgressMonitor monitor) {
		try {
			monitor.beginTask("Reading turn data from selected source.", numSubTasks * subTaskValue);
			//			final TurnReader turnData = new TurnReader(dataFileName, mainmap);
			monitor.subTask("Reading turn definition.");
			sleep(1000);
			final boolean valid = this.readTurnDescription();
			monitor.worked(subTaskValue);
			if (valid) {
				this.readPlayerData(projectPath, monitor);
				this.readPlanetData(projectPath, monitor);
				this.readBaseData(projectPath, monitor);
				monitor.worked(10000);
				this.readShipData(projectPath, monitor);
				monitor.worked(10000);
				this.readPodData(projectPath, monitor);
				this.readWingData(projectPath, monitor);

				//- Updated rest of model structures.
				monitor.subTask("Reading turn definition.");
				sleep(1000);
				diagram.updateSpotData();
				diagram.calculatePlanetClassification();
				monitor.worked(subTaskValue);
				monitor.done();

				//- Signal the diagram that there have been some model changes.
				//				diagram.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
			}
		} catch (final FileNotFoundException fnfe) {
			// - The command has failed for some cause, but the command is not deactivated.
			System.err.println("File <" + descriptionFileName + "> cannot be found.");
			fnfe.printStackTrace();
		} catch (final IOException ioe) {
			// TODO Notify the user the error during the read on the file contents.
			ioe.printStackTrace();
		}
		//		monitor.beginTask("Doing something timeconsuming here", 100);
		//		for (int i = 0; i < 10; i++) {
		//			if (monitor.isCanceled()) return;
		//			monitor.subTask("I'm doing something here " + i);
		//			sleep(1000);
		//			monitor.worked(i);
		//		}
		//		monitor.done();
	}

	private void sleep(Integer waitTime) {
		try {
			Thread.sleep(waitTime);
		} catch (Throwable t) {
			System.out.println("Wait time interrupted");
		}
	}

	/**
	 * Open the Base.csv and read and process the base data. Base information is split upon many .csv files. I
	 * should read all data that are affected by the data model.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 */
	public void readBaseData(final String projectPath, final IProgressMonitor monitor) {
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Base.csv"));
			//- Read the data into the model.
			//- The first line of the file contains the names on the data fields. 
			//- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			if (this.checkBaseFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				while (line != null) {
					//- Split the line into their components
					final String[] baseFields = line.split(",");
					//- Process the data creating a new Planet instance.
					final BaseInformation baseInfo = new BaseInformation(AssistantConstants.MODELTYPE_BASE);
					baseInfo.loadData(fieldNames, baseFields);

					//- Add the new instance to the model.
					diagram.addBaseInformation(turn, baseInfo);

					line = reader.readLine();
				}
			}
			reader.close();
		} catch (final IOException exc) {
			//TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	/**
	 * Open the Planet.csv file and read and process their contents to a set of <code>PlanetInformation</code>
	 * instances. Echa instance is then added to the corresponding <code>Planet</code> to have more historic
	 * detail.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 *          progress monitor to show while I am processing the file.
	 */
	public void readPlanetData(final String projectPath, final IProgressMonitor monitor) {
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Planet.csv"));
			// - Read the data into the model.
			//			final int lines = this.countLines(reader);
			//			final int units = 10000 / lines;
			//- The first line of the file contains the names on the data fields. 
			//--- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			if (this.checkPlanetFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				//				monitor.worked(units);
				while (line != null) {
					//- Split the line into their components
					final String[] planetFields = line.split(",");
					//- Process the data creating a new Planet instance.
					final PlanetInformation planetInfo = new PlanetInformation(AssistantConstants.MODELTYPE_PLANET);
					planetInfo.loadData(fieldNames, planetFields);

					//- Add the new instance to the model.
					diagram.addPlanetInformation(turn, planetInfo);

					line = reader.readLine();
					//					monitor.worked(units);
				}
			}
			reader.close();
		} catch (final IOException exc) {
			//TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		} catch (final ArrayIndexOutOfBoundsException aioobs) {
			//- The initialization of some array has failed.
			aioobs.printStackTrace();
		}

	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void readPlayerData(final String projectPath, final IProgressMonitor monitor) {
		int toAdvance = this.subTaskValue;
		try {
			monitor.subTask("Reading Player list data.");
			sleep(1000);
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Player.csv"));
			//- Count the number of lines to set the progress advance for each loop iteration.
			long noLines = FileUtilities.countLines(new BufferedReader(new FileReader(projectPath + "/Player.csv")));
			int iterationSpan = new Long(this.subTaskValue / noLines).intValue();
			//- Read the data into the model.
			//- The first line of the file contains the names on the data fields. 
			//- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			monitor.worked(iterationSpan);
			toAdvance -= iterationSpan;
			if (this.checkContentsFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				while (line != null) {
					//- Split the line into their components
					final String[] playerFields = line.split(",");
					//- Process the data creating a new Gamer instance.
					final Gamer player = new Gamer(AssistantConstants.MODELTYPE_GAMER);
					player.loadData(fieldNames, playerFields);

					//- Add the new instance to the model.
					diagram.addPlayerInformation(player);

					line = reader.readLine();
					monitor.worked(iterationSpan);
					toAdvance -= iterationSpan;
				}
			}
			reader.close();
		} catch (final IOException exc) {
			//TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
		//- Advance the rest of the estimated work share.
		monitor.worked(this.subTaskValue - toAdvance);
	}

	/**
	 * @param projectPath
	 *          Path to the directory where there is supposed to be found the .CSV files.
	 * @param monitor
	 *          Progress Monitor to inform the user of the activity and the expected progression.
	 */
	public void readPodData(final String projectPath, final IProgressMonitor monitor) {
		int toAdvance = this.subTaskValue;
		try {
			monitor.subTask("Reading Pod list data.");
			sleep(1000);
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Pod.csv"));
			//		//- Read the data into the model.
			//		try {
			//- The first line of the file contains the names on the data fields. 
			//--- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			if (this.checkContentsFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				while (line != null) {
					//- Split the line into their components
					final String[] dataFields = line.split(",");

					//TODO Create a new generic TurnInformationSet and add to it the type code.
					final AssistantTurnInfo turnInfo = new AssistantTurnInfo(AssistantConstants.MODELTYPE_POD);
					turnInfo.loadData(fieldNames, dataFields);

					//- Add the new instance to the model.
					diagram.addThingInformation(turn, turnInfo);

					line = reader.readLine();
				}
			}
			reader.close();
		} catch (final IOException exc) {
			//TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	/**
	 * Open the Ship.csv and read and process the ship data.
	 * 
	 * @param projectPath
	 *          the path to the directory where I should find the .csv files.
	 * @param monitor
	 */
	public void readShipData(final String projectPath, final IProgressMonitor monitor) {
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Ship.csv"));
			// - Read the data into the model.
			// - The first line of the file contains the names on the data fields. 
			//- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			if (this.checkShipFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				while (line != null) {
					// - Split the line into their components
					final String[] shipFields = line.split(",");
					// - Process the data creating a new Planet instance.
					final AssistantTurnInfo shipInfo = new AssistantTurnInfo(AssistantConstants.MODELTYPE_SHIP);
					shipInfo.loadData(fieldNames, shipFields);

					// - Add the new instance to the model.
					diagram.addShipInformation(turn, shipInfo);

					line = reader.readLine();
				}
			}
			reader.close();
		} catch (final IOException exc) {
			// TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	/**
	 * CSVInfo.txt file contains two source lines and four data fields. This method reads that file and
	 * processes its contents to extract those elements.<br>
	 * Also validates that this information matches the same fields on the diagram or that the diagram is not
	 * initialized, when then this method will load the fields with the read information.
	 * 
	 * @throws FileNotFoundException
	 */
	public boolean readTurnDescription() throws FileNotFoundException, IOException {
		// - Open the file for reading.
		final BufferedReader reader = new BufferedReader(new FileReader(descriptionFileName));

		// - Read the two lines
		final String line1 = reader.readLine();
		final String line2 = reader.readLine();

		// - Line 1 only contains the game name. Get it directly.
		gameName = line1.trim();
		TurnReader.logger.info("Reading data for Game Name=" + gameName);

		// - Line 2 is complex and there is no separator between fields. Scan it with a parser.
		// try {
		final Pattern pat = Pattern.compile("Game ID:(.+)Turn:(.+)Player:(.+)");
		final Matcher match = pat.matcher(line2);
		if (match.matches()) {
			gameId = match.group(1).trim();
			turn = Integer.parseInt(match.group(2).trim());
			player = Integer.parseInt(match.group(3).trim());
			TurnReader.logger.info("Reading data for Turn " + turn + " of player " + player);

			// - Check that this diagram contains data for the game and player.
			//- If diagram is not initialized then set those fields.
			if (diagram.isInitialized()) {
				// - Check that this information belongs to the right game, player and turn
				//TODO If the turn does not match the already stored identification throw an exception
				if ((gameId.equals(diagram.getGameId())) && (player == diagram.getPlayer())) {
					diagram.setReadTurn(turn);
					return true;
				} else
					return false;
			} else {
				// - Set the game identification into the diagram.
				diagram.setGameId(gameId);
				diagram.setPlayer(player);
				diagram.setGameName(gameName);
				diagram.setReadTurn(turn);
				return true;
			}
		} else
			return false;
	}

	public void readWingData(final String projectPath, final IProgressMonitor monitor) {
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(projectPath + "/Wing.csv"));
			//		//- Read the data into the model.
			//		try {
			//- The first line of the file contains the names on the data fields. 
			//--- Use this line to check that this file has the right format before trying to read.
			final String headerLine = reader.readLine();
			if (this.checkContentsFormat(headerLine)) {
				final String[] fieldNames = headerLine.split(",");
				String line = reader.readLine();
				while (line != null) {
					//- Split the line into their components
					final String[] dataFields = line.split(",");

					//TODO Create a new generic TurnInformationSet and add to it the type code.
					final AssistantTurnInfo turnInfo = new AssistantTurnInfo(AssistantConstants.MODELTYPE_WING);
					turnInfo.loadData(fieldNames, dataFields);

					//- Add the new instance to the model.
					diagram.addThingInformation(turn, turnInfo);

					line = reader.readLine();
				}
			}
			reader.close();
		} catch (final IOException exc) {
			//TODO Notify the user the error during the read on the file contents.
			exc.printStackTrace();
		}
	}

	private boolean checkBaseFormat(final String line) {
		final String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<BaseIndex>"))
			return true;
		else
			return false;
	}

	/**
	 * Check that the file going to be read contains a .CSV format file where the first line are the names of
	 * the fields to be read. This files will be recognized because the first field of the first line contains
	 * the type of the content and the <b><code>Index</code></b> suffix in between of angle brackets. IN the
	 * code generalization that is taking place there is a list of the valid type codes that will be checked and
	 * that generate an OK condition.<br>
	 * <br>
	 * <b>Supported Codes:</b>
	 * <ul>
	 * <li>BaseIndex</li>
	 * <li>PlanetIndex</li>
	 * <li>ShipIndex</li>
	 * <li>PodIndex</li>
	 * <li>WingIndex</li>
	 * </ul>
	 * <b>Valid Codes:</b>
	 * <ul>
	 * <li>ContactIndex</li>
	 * <li>MinefieldIndex</li>
	 * <li>PlayerIndex</li>
	 * </ul>
	 * 
	 * @param line
	 *          File line to be checked.
	 * @return <code>true</code> is the validation succesds.
	 */
	private boolean checkContentsFormat(final String line) {
		final String[] fieldNames = line.split(",");
		final String checkWord = fieldNames[0];

		//- Check the word against the list of supported codes.
		for (final String element : TurnReader.supportedCodesList) {
			if (checkWord.equals(element)) return true;
		}
		return false;
	}

	// - P R O T E C T E D - S E C T I O N
	// TODO This method has no implementation. I have to find a procedure to verify that we are reading a proper
	// formated file
	private boolean checkPlanetFormat(final String line) {
		final String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<PlanetIndex>"))
			return true;
		else
			return false;
	}

	private boolean checkShipFormat(final String line) {
		final String[] fieldNames = line.split(",");
		if (fieldNames[0].trim().equals("<ShipIndex>"))
			return true;
		else
			return false;
	}

	//	private int countLines(final BufferedReader reader) throws IOException {
	//		return 307;
	//		//		reader.mark(0);
	//		//		int lines = 0;
	//		//		String line = reader.readLine();
	//		//		while (null != line) {
	//		//			line = reader.readLine();
	//		//			lines++;
	//		//		}
	//		//		reader.reset();
	//		//		return lines;
	//	}
}
// - UNUSED CODE ............................................................................................
//[01]
//			// - Turn information is spread into several files. Create an instance to handle those
//			TurnReader turnData = new TurnReader(dataFileName, diagram);
//			if (turnData.readTurnDescription()) {
//				turnData.readPlanetData(projectPath);
//				turnData.readBaseData(projectPath);
//				turnData.readShipData(projectPath);
//
//				// - Signal the diagram that there have been some model changes
//				diagram.fireStructureChange(VGAP4Map.DATA_ADDED_PROP, null, null);
//			}
//		} catch (FileNotFoundException fnfe) {
//			// - The command has failed for some cause, but the command is not deactivated.
//			System.err.println("File <" + dataFileName + "> cannot be found.");
//			fnfe.printStackTrace();
//		} catch (IOException ioe) {
//			// TODO Notify the user the error during the read on the file contents.
//			ioe.printStackTrace();
//			} catch (InvocationTargetException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

//[02]
// Initialize the monitor and launch the Reader
//			final AssistantMap diagram = editor.getMapModel();
//			final AssistantMap mainmap = diagram;
//			final IProgressMonitor monitor = new NullProgressMonitor();

//			ProgressMonitorDialog dialog = new ProgressMonitorDialog(currentWindow.getShell());
//			try {
//				dialog.run(true, true, new IRunnableWithProgress() {
//					public void run(IProgressMonitor monitor) {
//						monitor.beginTask("Doing something timeconsuming here", 100);
//						for (int i = 0; i < 10; i++) {
//							if (monitor.isCanceled()) return;
//							monitor.subTask("I'm doing something here " + i);
//							sleep(1000);
//							monitor.worked(i);
//						}
//						monitor.done();
//					}
//				});
//			} catch (InvocationTargetException e) {
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

//			//FIXME Removed the code to use a progress indicator to show the loading of the turn data.
//			try {
//				//				monitor.beginTask("reading turn data", 40000);
//				final TurnReader turnData = new TurnReader(dataFileName, mainmap);
//				//				monitor.subTask("Reading turn definition");
//				final boolean valid = turnData.readTurnDescription();
//				//				monitor.worked(10000);
//				if (valid) {
//					turnData.readPlayerData(projectPath, monitor);
//					turnData.readPlanetData(projectPath, monitor);
//					turnData.readBaseData(projectPath, monitor);
//					//					monitor.worked(10000);
//					turnData.readShipData(projectPath, monitor);
//					//					monitor.worked(10000);
//					turnData.readPodData(projectPath, monitor);
//					turnData.readWingData(projectPath, monitor);
//
//					//- Updated rest of model structures.
//					mainmap.updateSpotData();
//					mainmap.calculatePlanetClassification();
//
//					//- Signal the diagram that there have been some model changes.
//					mainmap.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
//					//					monitor.done();
//				}
//			} catch (final FileNotFoundException fnfe) {
//				// - The command has failed for some cause, but the command is not deactivated.
//				System.err.println("File <" + dataFileName + "> cannot be found.");
//				fnfe.printStackTrace();
//			} catch (final IOException ioe) {
//				// TODO Notify the user the error during the read on the file contents.
//				ioe.printStackTrace();
//			}

//[03]

//		//			try {
//			//TODO Open a new operation to load the data
//			final WorkspaceModifyOperation operation = new ReadTurnWorkspaceModifyOperation(dataFileName, projectPath,
//					diagram);
//			Display.getCurrent().asyncExec(new Runnable() {
//				public void run() {
//					try {
//						operation.run(new ProgressMonitorPart(editor.getSite().getWorkbenchWindow().getShell(), null));
//					} catch (final InvocationTargetException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (final InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			});
//[01]		
//		}
