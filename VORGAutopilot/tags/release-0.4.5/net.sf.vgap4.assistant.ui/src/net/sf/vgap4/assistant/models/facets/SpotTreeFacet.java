//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.facets;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import net.sf.vgap4.assistant.models.AssistantNode;
import net.sf.vgap4.assistant.models.Spot;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotTreeFacet implements ITreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.viewers");

	// - F I E L D - S E C T I O N ............................................................................
	private Spot	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotTreeFacet(Spot target) {
		this.delegate = target;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Return the visible name identifier for this Planet to be shown on Views. This name follows the next
	 * structure:
	 * <ul>
	 * <li>object unique identifier.</li>
	 * <li>Planet name.</li>
	 * <li>Planet decorators - this is a list of codes to signal the Farming Level, Contraband Level, Mineral
	 * Level and the presence of natives.</li>
	 * </ul>
	 */
	public String getName() {
		return this.delegate.getIdentifier();
	}

	public Vector<AssistantNode> getContents() {
		return delegate.getContents();
	}

	public String getLocationString() {
		return delegate.getLocationString();
	}

	public AssistantNode getDelegate() {
		return delegate;
	}

	public String getRace() {
		AssistantNode representative = delegate.getRepresentative();
		return representative.getTreeFacet().getRace();
	}

	public String getFFCode() {
		return delegate.getFFStatus();
	}
}

// - UNUSED CODE ............................................................................................
