//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Hashtable;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpotRegistry implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger									logger						= Logger.getLogger("net.sf.vgap4.assistant.models");
	private static final long							serialVersionUID	= 1L;

	// - F I E L D - S E C T I O N ............................................................................
	private final Hashtable<Point, Spot>	locations					= new Hashtable<Point, Spot>();
	private int														minX							= 9000;
	private int														minY							= 9000;
	private int														maxX							= -1;
	private int														maxY							= -1;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpotRegistry() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void clear() {
		locations.clear();
		minX = 9000;
		minY = 9000;
		maxX = -1;
		maxY = -1;
	}

	//	public Enumeration<Point> getLocations() {
	//		return locations.keys();
	//	}
	public Hashtable<Point, Spot> getLocations() {
		return locations;
	}

	public Spot getSpot(final Point location) {
		return locations.get(location);
	}

	public void register(final Point location, final AssistantNode mapElement) {
		if ((null != location) && (null != mapElement)) {
			//- Check if this location has already been used. If so add this new element to the vector.
			Spot cluster = locations.get(location);
			if (null == cluster) {
				cluster = new Spot();
				cluster.add(mapElement);
				locations.put(location, cluster);
				logger.info("New registration: location [" + mapElement.getLocationString() + "] for node "
						+ mapElement.getIdentifier());

				//- Adjust map size from new locations additions.
				minX = Math.min(minX, location.x);
				minY = Math.min(minY, location.y);
				maxX = Math.max(maxX, location.x);
				maxY = Math.max(maxY, location.y);
			} else {
				cluster.add(mapElement);
			}
		}
	}

	public Rectangle getBoundaries() {
		return new Rectangle(minX, minY, maxX, maxY);
	}
}
// - UNUSED CODE ............................................................................................
