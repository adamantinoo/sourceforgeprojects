//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.models.database;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Properties;

import net.sf.vgap4.assistant.ui.AssistantConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class RaceDatabase {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String										FARMLEVEL_COLD		= "FARMLEVEL_COLD";
	public static final String										FARMLEVEL_HOT			= "FARMLEVEL_HOT";
	public static final String										FARMLEVEL_INRANGE	= "FARMLEVEL_INRANGE";
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.database");
	private static Hashtable<Integer, Properties>	raceData					= new Hashtable<Integer, Properties>(10);
	static {
		final Properties raceProps = new Properties();
		raceProps.setProperty("lowFarmLevel", "32");
		raceProps.setProperty("highFarmLevel", "70");
		RaceDatabase.raceData.put(AssistantConstants.RACE_CYBORG, raceProps);
	}

	public static String getFarmLevel(final int race, final int planetTemp) {
		final Properties data = RaceDatabase.raceData.get(race);
		if (null == data)
			return RaceDatabase.FARMLEVEL_COLD;
		else {
			final int low = RaceDatabase.convert2Integer(data.getProperty("lowFarmLevel"));
			final int high = RaceDatabase.convert2Integer(data.getProperty("highFarmLevel"));
			if (planetTemp >= high) return RaceDatabase.FARMLEVEL_HOT;
			if (planetTemp <= low) return RaceDatabase.FARMLEVEL_COLD;
			return RaceDatabase.FARMLEVEL_INRANGE;
		}
	}

	private static int convert2Integer(final String fieldValue) {
		try {
			return Integer.parseInt(fieldValue.trim());
		} catch (final Exception exc) {
			//- The field is null or not convertible to a number. Return the default ZERO.
			return 0;
		}
	}

	public static String getRaceDescriptor(int race) {
		switch (race) {
			case 101:
				return "The Solar Federation of Planets";
			case 103:
				return "The Birdman Republic";
			case 104:
				return "The Stormer Kingdom";
			case 106:
				return "The Cyborg Collective";
			case 108:
				return "The Evil Empire";
			case 109:
				return "The Robotic Imperium";
			case 110:
				return "The Rebels";
			default:
				return "No definida";
		}
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
}
// - UNUSED CODE ............................................................................................
