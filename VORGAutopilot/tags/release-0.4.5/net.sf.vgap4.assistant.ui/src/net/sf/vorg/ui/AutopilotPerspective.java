//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.ui;

// - IMPORT SECTION .........................................................................................
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vgap4.assistant.views.SelectionPropertiesView;
import net.sf.vorg.views.BoatTreeView;
import net.sf.vorg.views.PilotStatusView;

// - CLASS IMPLEMENTATION ...................................................................................
public class AutopilotPerspective implements IPerspectiveFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	protected static final String	VORGPERSPECTIVE_ID	= "net.sf.vorg.ui.AutopilotPerspective.id";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Creates the and configures the editor location and the views that appear on the workspace. This method
	 * defines the relative locations of the elements and the relative sizes. The total size of the workspace is
	 * decided by the user ot the initialization code of the application. The locations of the views and it
	 * sizes are hardcoded inside this method and cannot de declared as configuracle properties.<br>
	 * The elements defines in the Harpoon perspective are three:
	 * <ul>
	 * <li>The Editor panel where we load and display the scenery map.</li>
	 * <li>The Property and selection view, where we display selection properties or selection contents.</li>
	 * <li>The message are where we display game messages and commands results.</li>
	 * </ul>
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		// TODO Register the perspective
		Activator.addReference(VORGPERSPECTIVE_ID, this);

		// - Activate the Editor area. The maps are shown on editor parts.
		final String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);

		// - Define the two views that compose the main presentation window
		layout.addStandaloneView(BoatTreeView.ID, true, IPageLayout.LEFT, 0.20f, editorArea);
		layout.getViewLayout(SelectionPropertiesView.ID).setCloseable(false);
		layout.addStandaloneView(PilotStatusView.ID, true, IPageLayout.TOP, 0.80f, editorArea);
		layout.getViewLayout(PilotStatusView.ID).setCloseable(false);
	}
}

// - UNUSED CODE ............................................................................................
