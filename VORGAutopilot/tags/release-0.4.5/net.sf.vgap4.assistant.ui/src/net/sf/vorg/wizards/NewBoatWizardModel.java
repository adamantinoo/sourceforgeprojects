//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import com.xyz.article.wizards.HolidayModel;

// - CLASS IMPLEMENTATION ...................................................................................
public class NewBoatWizardModel extends HolidayModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg");

	// - F I E L D - S E C T I O N ............................................................................
	private String	name;
	private String	boatId;
	private String	clef;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NewBoatWizardModel() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBoatId() {
		return boatId;
	}

	public void setBoatId(String boatId) {
		this.boatId = boatId;
	}

	public String getClef() {
		return clef;
	}

	public void setClef(String clef) {
		this.clef = clef;
	}
}

// - UNUSED CODE ............................................................................................
