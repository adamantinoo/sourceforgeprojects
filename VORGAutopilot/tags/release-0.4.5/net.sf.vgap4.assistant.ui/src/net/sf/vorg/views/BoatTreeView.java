//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SelectionTreeView.java 184 2008-09-25 16:01:50Z boneymen $
//  LAST UPDATE:    $Date: 2008-09-25 18:01:50 +0200 (jue, 25 sep 2008) $
//  RELEASE:        $Revision: 184 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.logging.Logger;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.gef.core.model.IGEFNode;
import net.sf.gef.core.views.BaseTreeContentProvider;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.ui.Activator;
import net.sf.vorg.actions.BoatAction;
import net.sf.vorg.models.UIPilotModelStore;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;
import net.sf.vorg.wizards.NewBoatWizard;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This sample class demonstrates how to plug-in a new workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly, but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace). The view is connected to the model using
 * a content provider.
 * <p>
 * The view uses a label provider to define how model objects should be presented in the view. Each view can
 * present the same model objects using different labels and icons, if needed. Alternatively, a single label
 * provider can be shared between views in order to ensure that objects of the same type are presented in the
 * same way everywhere.
 * <p>
 */
public class BoatTreeView extends ViewPart implements PropertyChangeListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String			ID						= "net.sf.vorg.views.BoatTreeView.id";
	public static final String			TREEVIEWER_ID	= ID + "SelectionTreeProvider";
	public static final int					NAME_COLUMN		= 0;
	//	public static final int										LOCATION_COLUMN					= 1;
	//	public static final int										CLASS_COLUMN						= 2;
	//	private static final int									RESOURCES_COLUMN				= 2;
	private static Logger						logger				= Logger.getLogger("net.sf.vgap4.assistant.views");
	//	private static Vector<ColumnConfigurator>	treeColumnsConfigurator	= new Vector<ColumnConfigurator>();
	//	//	private static VORGFigureFactory					imageFactory						= new VORGFigureFactory();
	//	/**
	//	 * Build the list of columns and the code to get the contents for each of them depending on the object type.
	//	 */
	//	static {
	//		//- NAME Column 1.
	//		logger.info("Adding 'Name' configuration to Tree View columns.");
	//		treeColumnsConfigurator.add(new ColumnConfigurator(BoatTreeView.NAME_COLUMN, "Name", "Game Boat codename", 180,
	//				true, new TextAccessor() {
	//					@Override
	//					public String getText(final IPilotTreeFacet facet) {
	//						return facet.getName();
	//					}
	//				}, new ImageAccessor() {
	//					@Override
	//					public Image getImage(final IPilotTreeFacet facet) {
	//						return facet.getImage();
	//					}
	//				}));
	//	}

	//	public static ColumnConfigurator getColumnConfiguration(final int index) {
	//		return treeColumnsConfigurator.get(index);
	//	}

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Table structure to present the contents. Those contents are generated by the
	 * <code>ViewTreeContentProvider</code>.
	 */
	private TreeViewer							viewer;
	/** Menu actions that are available on this viewer. */
	private BoatAction							newBoatCard;
	private BoatAction							deleteBoatCard;
	private BoatAction							updateData;
	private Action									doubleClickAction;

	private PilotModelStore					modelServer;
	/**
	 * This class provides the data contents for the viewer. This is installed in a MDC pattern where this is
	 * the Model part. This class is feed from the editor selection where all model information is extracted and
	 * stored for access from the Controller that is implemented by the <code>TableViewer</code>.<br>
	 * This is the main model container for the application. All objects are references and created from this
	 * model storage.
	 */
	private BaseTreeContentProvider	contentProvider;
	/**
	 * This class is called to draw each cell contents. It will receive small pieces of the model and is
	 * responsible to map the columns to the different attributes of the model.
	 */
	private ViewTreeLabelProvider		labelProvider;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatTreeView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(BoatTreeView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		//- Create the content provider along with the factories to create the model objects
		contentProvider = new PilotTreeContentProvider(modelServer);
		labelProvider = new ViewTreeLabelProvider();

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(labelProvider);
		viewer.setInput(this.getViewSite());

		//- Configure the viewer columns
		//		this.configureTreeColumns(viewer.getTree());

		//TODO Configure the view menus and actions.
		this.makeActions();
		this.hookContextMenu();
		this.hookDoubleClickAction();
		this.contributeToActionBars();

		//- Register this as a selection provider.
		Activator.addReference(BoatTreeView.TREEVIEWER_ID, viewer);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) {
			viewer.refresh();
		}
	}

	private void makeActions() {
		//- New basic Boat action to create a new Boat. This is done with a wizard
		newBoatCard = new BoatAction() {
			@Override
			public void run() {
				//- Instantiates and initializes the wizard
				PilotBoat boat = new PilotBoat();
				NewBoatWizard wizard = new NewBoatWizard(boat);
				IWorkbench workBench = PlatformUI.getWorkbench();
				Shell shell = workBench.getActiveWorkbenchWindow().getShell();
				wizard.init(workBench, (IStructuredSelection) viewer.getSelection());

				// Instantiates the wizard container with the wizard and opens it
				WizardDialog dialog = new WizardDialog(shell, wizard);
				dialog.create();
				int result = dialog.open();
				if (result == Window.OK) this.addNewBoat(wizard.getModel());
			}

			public void addNewBoat(PilotBoat boat) {
				if (null != modelStore) modelStore.addBoat(boat);
			}
		};
		newBoatCard.setText("New Boat");
		newBoatCard.setToolTipText("Create a new Boat card to allow control of that game ship on the Pilot");
		newBoatCard.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
				ISharedImages.IMG_TOOL_NEW_WIZARD));
		newBoatCard.setModelStore(this.modelServer);

		deleteBoatCard = new BoatAction() {
			@Override
			public void run() {
				final ISelection selection = viewer.getSelection();
				if (null != selection) {
					this.deleteBoat(selection);
				}
			}

			public void deleteBoat(ISelection selection) {
				Iterator<Object> sit = ((IStructuredSelection) selection).iterator();
				while (sit.hasNext()) {
					Object element = sit.next();
					if (element instanceof PilotBoat) {
						if (null != modelStore) modelStore.removeBoat((IGEFNode) element);
					}
				}
			}

		};
		deleteBoatCard.setText("Delete Boat");
		deleteBoatCard.setToolTipText("Delete the selected boats on the selection.");
		deleteBoatCard.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
				ISharedImages.IMG_TOOL_DELETE));
		deleteBoatCard.setModelStore(this.modelServer);

		updateData = new BoatAction() {
			private boolean	running			= false;
			private Display	display;
			private int			refresh			= 2;
			//			private long		lastHour		= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			private long		lastMinute	= minuteOfDay() - 100;
			private int			timeDelay		= 15;

			private long minuteOfDay() {
				final Calendar now = Calendar.getInstance();
				final int hour = now.get(Calendar.HOUR_OF_DAY);
				final int minute = now.get(Calendar.MINUTE);
				return hour * 60 + minute;
			}

			private void wait4NextIteration() throws InterruptedException {
				//- Calculate last minute of day and current minute of day and then the fire minute.
				final long dayMinute = minuteOfDay();
				long waitUntil = lastMinute + refresh;
				//- Check for the 24 hours. This will give a time unreachable.
				if (waitUntil > 24 * 60) waitUntil -= 24 * 60;
				if (dayMinute > waitUntil) {
					lastMinute = dayMinute;
					return;
				}

				//- Calculate the time to wait
				final Calendar now = GregorianCalendar.getInstance();
				final int seconds = now.get(Calendar.SECOND);
				final long waitSeconds = (waitUntil - dayMinute) * 60 - seconds + timeDelay;
				try {
					Thread.yield();
					lastMinute = waitUntil;
					if (waitSeconds > 0) Thread.sleep(waitSeconds * 1000);
				} catch (final IllegalArgumentException iae) {
					// - The value to wait is not valid. Skip
				}
			}

			/**
			 * This methods starts or restarts the game processing loop and the game timers.<br>
			 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
			 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside
			 * that thread that requires access to the UI (updating, creating new model elements) will be posted as
			 * another Runnable instance to the Display created for the application.
			 */
			public void startProcessingLoop() {
				// - Mark the flag that keeps the main thread running.
				display = Display.getCurrent();
				running = true;
				//			final Scenery sce = this;
				// HarpoonLoop job = new HarpoonLoop(Display.getCurrent(), "GameProcessing loop");
				// job.schedule(2 * 1000);
				// job.runInUIThread(new NullProgressMonitor());
				// // TODO Launch a new thread to perform all the tasks
				// final Scenery scene = this;
				// - Create a new thread and keep it running the infinite loop
				if (null != this.modelStore) {
					final Thread gameProcessingThread = new Thread(new Runnable() {
						public void run() {
							//					checkMapReload();
							//					performOperation();
							//					modelStore.run(false);
							while (running) {
								try {
									wait4NextIteration();
									//					// - Sleep until the right number of seconds. Only 0 - 15 - 30 and 45
									//						Calendar now = GregorianCalendar.getInstance();
									//						int secs = now.get(Calendar.SECOND);
									//						if ((0 == secs) || (15 == secs) || (30 == secs) || (45 == secs)) {
									// - Run the job but this time inside the Display UI environment.
									logger.info("Starting UIJob");
									HarpoonLoop job = new HarpoonLoop(modelStore, "Main Pilot Loop");
									display.syncExec(job);
									//								// - Sleep for some time.
									//								Thread.yield();
									//								Thread.sleep(5 * 1000);
									//							} catch (InterruptedException ie) {
									//								// - If the thread is interrupted then exit it by turning the flag.
									//								allowRun = false;
								} catch (Exception e) {
									e.printStackTrace();
								}
								//						} else {
								//							try {
								//								// - Sleep for some time.
								//								Thread.yield();
								//								Thread.sleep(2 * 1000);
								//							} catch (InterruptedException ie) {
								//								// - If the thread is interrupted then exit it by turning the flag.
								//								allowRun = false;
								//							}
							}
							//					}
						};
					});
					gameProcessingThread.start();
					gameProcessingThread.setName("Main Pilot Loop");
				}
			}

			//		/**
			//		 * This methods starts or restarts the game processing loop and the game timers.<br>
			//		 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
			//		 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside that
			//		 * thread that requires access to the UI (updating, creating new model elements) will be posted as another
			//		 * Runnable instance to the Display created for the application.
			//		 * 
			//		 * @return
			//		 */
			//		public Thread run(final boolean RCPFlag) {
			//			// - Mark the flag that keeps the main thread running.
			//			allowRun = true;
			//
			//			while (allowRun)
			//				try {
			//					//- Wait to the next iteration time depending on external configuration.
			//					wait4NextIteration();
			//
			//					// - Sleep for some time.
			//					Thread.yield();
			//					//				Thread.sleep(PilotModelStore.SLEEP_MSECONDS);
			//				} catch (final InterruptedException ie) {
			//					System.out.println("Autopilot interrupted. Terminating current process.");
			//					allowRun = false;
			//				} catch (final Exception ex) {
			//					// - Any class of exception. Record it and continue.
			//					System.out.println("EEE EXCEPTION - " + ex.getLocalizedMessage());
			//				}
			//			notifyAll();
			//			return null;
			//		}
			@Override
			public void run() {
				//- Toggle the run status of the model
				if (running)
					running = false;
				else {
					running = true;
					startProcessingLoop();
				}
			}
		};
		updateData.setText("Start/stop loop run");
		updateData.setToolTipText("Toggles the state of the model from Stop to Run or again to Stop.");
		updateData.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
				ISharedImages.IMG_DEF_VIEW));
		updateData.setModelStore(this.modelServer);

		doubleClickAction = new Action() {
			@Override
			public void run() {
				final ISelection selection = viewer.getSelection();
				final Object obj = ((IStructuredSelection) selection).getFirstElement();
				BoatTreeView.this.showMessage("Double-click detected on " + obj.toString());
			}
		};
	}

	private void showMessage(final String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "Boat Information Panel", message);
	}

	private void hookContextMenu() {
		final MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(final IMenuManager manager) {
				BoatTreeView.this.fillContextMenu(manager);
			}
		});
		final Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		this.getSite().registerContextMenu(menuMgr, viewer);
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(final DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	private void contributeToActionBars() {
		final IActionBars bars = this.getViewSite().getActionBars();
		this.fillLocalPullDown(bars.getMenuManager());
		this.fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillContextMenu(final IMenuManager manager) {
		manager.add(newBoatCard);
		manager.add(deleteBoatCard);
		manager.add(updateData);
		//- Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalPullDown(final IMenuManager manager) {
		manager.add(newBoatCard);
		manager.add(deleteBoatCard);
		manager.add(updateData);
	}

	private void fillLocalToolBar(final IToolBarManager manager) {
		manager.add(newBoatCard);
		manager.add(deleteBoatCard);
		manager.add(updateData);
	}

	@Override
	public void dispose() {
		//		// - Remove this from the selection provider.
		//		final ISelectionService globalSelectionService = this.getSite().getWorkbenchWindow().getSelectionService();
		//		globalSelectionService.removeSelectionListener(this);
		modelServer.removePropertyChangeListener(this);
		super.dispose();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		super.init(site);
		//		contentProvider = new ViewTreeContentProvider(new VORGEditPartFactory(new VORGFigureFactory()));

		//TODO Connect this view with the content provider that contains the singleton model.
		modelServer = (UIPilotModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		if (null == modelServer) {
			modelServer = new UIPilotModelStore();
			//- Add this view to the listener list for this model.
			//- Connect this provider to the model to be informed of changes.
			modelServer.addPropertyChangeListener(this);
		}
		//		modelServer.loadModelContents();
	}

	//[03]

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	//[01]

	//	private void configureTreeColumns(final Tree targetTree) {
	//		//- Code to configure the columns that compose the tree.
	//		//This is configured as the ColumnConfigurator metadata.
	//		TreeColumn column = null;
	//		final Iterator<ColumnConfigurator> cit = BoatTreeView.treeColumnsConfigurator.iterator();
	//		while (cit.hasNext()) {
	//			final ColumnConfigurator configurator = cit.next();
	//			column = new TreeColumn(targetTree, SWT.NONE);
	//			column.setWidth(configurator.getPreferredWidth());
	//			column.setText(configurator.getTitle());
	//			column.setToolTipText(configurator.getToolTip());
	//			//				columns.add(column);
	//		}
	//		viewer.getTree().setLayout(new FillLayout());
	//		viewer.getTree().setLinesVisible(false);
	//		viewer.getTree().setHeaderVisible(false);
	//	}
}

class HarpoonLoop implements Runnable {
	private PilotModelStore	referenceStore;
	private String					name;

	public HarpoonLoop(PilotModelStore store, String name) {
		referenceStore = store;
		this.name = name;
	}

	public void run() {
		//		logger.info(">>> Entering Processing Enemy Detection Loop");
		if (null != referenceStore) {
			referenceStore.checkMapReload();
			referenceStore.performOperation();
		}
		//		logger.info("<<< Exiting Processing Enemy Detection Loop");
	}
}

class PilotTreeContentProvider extends BaseTreeContentProvider {
	public PilotTreeContentProvider(AbstractGEFNode modelServer) {
		super(modelServer);
	}

	@Override
	public Object[] getChildren(Object parent) {
		//		if(parent instanceof PilotModelStore)return new Object[] {parent};
		if (parent instanceof PilotBoat)
			return null;
		else
			return super.getChildren(parent);
	}

	@Override
	public boolean hasChildren(Object parent) {
		if (parent instanceof PilotBoat)
			return false;
		else
			return super.hasChildren(parent);
	}

	@Override
	public Object getParent(final Object element) {
		if (element instanceof PilotBoat) ((PilotBoat) element).getParent();
		return super.getParent(element);
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
///**
// * Class to encapsulate the metadata to compose a tree column. Has the name, the title, the tooltip text and
// * preferred width along the object to render it that has the code to get and compose the output.
// */
//class ColumnConfigurator {
//
//	// - F I E L D - S E C T I O N ............................................................................
//	private final int						columnId;
//	private final String				title;
//	private final String				toolTip;
//	private final int						preferredWidth;
//	private final boolean				hasImage;
//	private final TextAccessor	textGetter;
//	private ImageAccessor				imageGetter;
//
//	// - C O N S T R U C T O R - S E C T I O N ................................................................
//	public ColumnConfigurator(final int nameColumn, final String columnTitle, final String toolTipText, final int width,
//			final boolean hasImageFlag, final TextAccessor textAccessor) {
//		columnId = nameColumn;
//		title = columnTitle;
//		toolTip = toolTipText;
//		preferredWidth = width;
//		hasImage = hasImageFlag;
//		textGetter = textAccessor;
//	}
//
//	public ColumnConfigurator(final int nameColumn, final String columnTitle, final String toolTipText, final int width,
//			final boolean hasImageFlag, final TextAccessor textAccessor, final ImageAccessor imageAccessor) {
//		this(nameColumn, columnTitle, toolTipText, width, hasImageFlag, textAccessor);
//		imageGetter = imageAccessor;
//	}
//
//	// - M E T H O D - S E C T I O N ..........................................................................
//	public int getColumnId() {
//		return columnId;
//	}
//
//	public String getColumnText(final IPilotTreeFacet assistantTreeFacet) {
//		if (null != textGetter)
//			return textGetter.getText(assistantTreeFacet);
//		else
//			return "N/A";
//	}
//
//	public Image getImage(final IPilotTreeFacet node) {
//		if (null != imageGetter)
//			return imageGetter.getImage(node);
//		else
//			return null;
//	}
//
//	public int getPreferredWidth() {
//		return preferredWidth;
//	}
//
//	public String getTitle() {
//		return title;
//	}
//
//	public String getToolTip() {
//		return toolTip;
//	}
//
//}

//- CLASS IMPLEMENTATION ...................................................................................
//abstract class ImageAccessor {
//	public Image getImage(final IPilotTreeFacet node) {
//		return null;
//	}
//}
//
////- CLASS IMPLEMENTATION ...................................................................................
//abstract class TextAccessor {
//	public String getText(final IPilotTreeFacet node) {
//		return "";
//	}
//}

//- CLASS IMPLEMENTATION ...................................................................................
class TypeSorter extends ViewerSorter {
}

//- CLASS IMPLEMENTATION ...................................................................................
/* This class can forward the calls to the model if this implements the ITableLabelProvider interface */
class ViewTreeLabelProvider extends LabelProvider implements ITableLabelProvider {

	// - M E T H O D - S E C T I O N ..........................................................................
	/** Return the model icon only if the column is set to column 0 that is the key column. */
	public Image getColumnImage(final Object target, final int index) {
		return getImage(target);
		//		final ColumnConfigurator configurator = BoatTreeView.getColumnConfiguration(index);
		//		return configurator.getImage(((UIBoat) target).getTreeFacet());
	}

	public String getColumnText(final Object target, final int index) {
		return getText(target);
		//		final ColumnConfigurator configurator = BoatTreeView.getColumnConfiguration(index);
		//		return configurator.getColumnText(((UIBoat) target).getTreeFacet());
	}

	@Override
	public Image getImage(final Object target) {
		if (target instanceof PilotModelStore) return ImageFactory.getImage("icons/starship.gif");
		if (target instanceof PilotBoat) return ImageFactory.getImage("icons/boat.gif");
		return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}

	@Override
	public String getText(final Object element) {
		if (element instanceof PilotBoat) return ((PilotBoat) element).getName();
		if (element instanceof PilotModelStore) return "Autopilot List";
		return element.toString();
	}
}
//- UNUSED CODE ............................................................................................
//[01]
//private void contributeToActionBars() {
//	final IActionBars bars = this.getViewSite().getActionBars();
//	this.fillLocalPullDown(bars.getMenuManager());
//	this.fillLocalToolBar(bars.getToolBarManager());
//}
//
//private void fillContextMenu(final IMenuManager manager) {
//	manager.add(action1);
//	manager.add(action2);
//	// Other plug-ins can contribute there actions here
//	manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
//}
//
//private void fillLocalPullDown(final IMenuManager manager) {
//	manager.add(action1);
//	manager.add(new Separator());
//	manager.add(action2);
//}
//
//private void fillLocalToolBar(final IToolBarManager manager) {
//	manager.add(action1);
//	manager.add(action2);
//}
//
//private void hookContextMenu() {
//	final MenuManager menuMgr = new MenuManager("#PopupMenu");
//	menuMgr.setRemoveAllWhenShown(true);
//	menuMgr.addMenuListener(new IMenuListener() {
//		public void menuAboutToShow(final IMenuManager manager) {
//			SelectionTreeView.this.fillContextMenu(manager);
//		}
//	});
//	final Menu menu = menuMgr.createContextMenu(viewer.getControl());
//	viewer.getControl().setMenu(menu);
//	this.getSite().registerContextMenu(menuMgr, viewer);
//}
//
//private void hookDoubleClickAction() {
//	viewer.addDoubleClickListener(new IDoubleClickListener() {
//		public void doubleClick(final DoubleClickEvent event) {
//			doubleClickAction.run();
//		}
//	});
//}
//
//private void makeActions() {
//	action1 = new Action() {
//		@Override
//		public void run() {
//			SelectionTreeView.this.showMessage("Action 1 executed");
//		}
//	};
//	action1.setText("Action 1");
//	action1.setToolTipText("Action 1 tooltip");
//	action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
//			ISharedImages.IMG_OBJS_INFO_TSK));
//
//	action2 = new Action() {
//		@Override
//		public void run() {
//			SelectionTreeView.this.showMessage("Action 2 executed");
//		}
//	};
//	action2.setText("Action 2");
//	action2.setToolTipText("Action 2 tooltip");
//	action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(
//			ISharedImages.IMG_OBJS_INFO_TSK));
//	doubleClickAction = new Action() {
//		@Override
//		public void run() {
//			final ISelection selection = viewer.getSelection();
//			final Object obj = ((IStructuredSelection) selection).getFirstElement();
//			SelectionTreeView.this.showMessage("Double-click detected on " + obj.toString());
//		}
//	};
//}
//private void showMessage(final String message) {
//	MessageDialog.openInformation(viewer.getControl().getShell(), "Sample View", message);
//}

//[03]
//	/**
//	 * This event is fired any time the selection is modified. <br>
//	 * The parameters received are the new selection list on the <code>selection</code> parameter and the
//	 * <code>EditorPart</code> that points to the originating <i>Editor</i> on the <code>part</code> parameter.<br>
//	 * <br>
//	 * From this selection I may extract the selectable elements (the ones that are going to move to the
//	 * selection views) and the model information for each of the selected items. Filtering must be
//	 * parameterizable thought the user interface.<br>
//	 * <br>
//	 * TODO This release only supports the selection of part elements up to the level of
//	 * <code>AbstractGenericEditPart</code>. When routes or other object appear on the Map it has to be changed
//	 * to support them.
//	 * 
//	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
//	 *      org.eclipse.jface.viewers.ISelection)
//	 */
//	public void selectionChanged(final IWorkbenchPart editorPart, final ISelection selection) {
//		if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
//			final Vector<ISelectablePart> models = new Vector<ISelectablePart>(2);
//			final Iterator<Object> sit = ((StructuredSelection) selection).iterator();
//			while (sit.hasNext()) {
//				final Object part = sit.next();
//				//TODO Apply all the filters that are defined on the list.
//				//TODO Filter the EditParts that match the proper interface
//				//- Check that the selected object has the right interface before trying to get its model.
//				if (part instanceof ISelectablePart) models.add((ISelectablePart) part);
//			}
//			if (!models.isEmpty()) {
//				contentProvider.setModelContents(models);
//				viewer.refresh(true);
//			}
//		}
//	}
