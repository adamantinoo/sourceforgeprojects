//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

//- INTERFACE IMPLEMENTATION ...............................................................................
public interface PilotUIConstants {
	//- G L O B A L - S E C T I O N ..........................................................................
	//- D E F I N I T I O N   C O L O R S
	Color	COLOR_SHIP_NAME	= new Color(Display.getDefault(), 0, 0, 0);
	Color	COLOR_BLACK			= new Color(Display.getDefault(), 0, 0, 0);
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
