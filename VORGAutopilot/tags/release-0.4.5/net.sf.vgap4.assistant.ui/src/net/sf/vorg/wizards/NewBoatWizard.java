//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import com.xyz.article.wizards.HolidayWizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbench;

import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class NewBoatWizard extends HolidayWizard {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	//TODO Move the data from the HolidayWizard calss and no methods
	private PilotBoat								model;
	private BoatPropertiesMainPage	newBoatPage;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NewBoatWizard(PilotBoat modelBoat) {
		super();
		if (null != modelBoat) model = modelBoat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/*
	 * 
	 * reimplement createPageControls reimplement performCancel extend addPages reimplement performFinish extend
	 * dispose
	 */
	@Override
	public void addPages() {
		newBoatPage = new BoatPropertiesMainPage(workbench, selection);
		addPage(newBoatPage);
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
	}

	public PilotBoat getModel() {
		return model;
	}

	@Override
	public boolean canFinish() {
		newBoatPage.saveDataToModel();
		if (null != model.getName())
			return true;
		else
			return false;
	}

	@Override
	public boolean performFinish() {
		newBoatPage.saveDataToModel();
		return newBoatPage.verifyDataModel();
		//		return canFinish();
	}
}

// - UNUSED CODE ............................................................................................
