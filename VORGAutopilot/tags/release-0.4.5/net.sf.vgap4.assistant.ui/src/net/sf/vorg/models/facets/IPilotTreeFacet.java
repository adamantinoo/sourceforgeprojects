//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models.facets;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.graphics.Image;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IPilotTreeFacet {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public String getName();

	public Image getImage();
}
// - UNUSED CODE ............................................................................................
