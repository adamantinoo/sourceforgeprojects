//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SpotEditPart.java 179 2008-07-10 11:51:20Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-10 13:51:20 +0200 (jue, 10 jul 2008) $
//  RELEASE:        $Revision: 179 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;

import net.sf.gef.core.editparts.AbstractDirectedNodeEditPart;
import net.sf.vgap4.assistant.figures.IDetailedFigure;
import net.sf.vgap4.projecteditor.policies.GNodePolicy;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatEditPart extends AbstractDirectedNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public Boat getCastedModel() {
		return (Boat) this.getModel();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Boat.NAME_CHANGED.equals(prop)) {
			this.refreshVisuals();
			return;
		}
		//		if (Boat.LOCATION_CHANGED.equals(prop)) {
		//			this.refreshVisuals();
		//			return;
		//		}
		if (Boat.PROPERTY_CHANGED.equals(prop)) {
			this.refreshVisuals();
			return;
		}
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer('\n');
		buffer.append("[BoatEditPart:");
		buffer.append(this.getModel().toString()).append("-");
		buffer.append(this.getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	protected void createEditPolicies() {
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final IFigure fig = this.getFigure();
		if (fig instanceof IDetailedFigure) {
			((IDetailedFigure) fig).createContents();
			super.refreshVisuals();
		}
	}
}
// - UNUSED CODE ............................................................................................
