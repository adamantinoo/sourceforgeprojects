//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vgap4.assistant.figures.IDetailedFigure;
import net.sf.vgap4.assistant.figures.draw2d.MultiLabelLine;
import net.sf.vgap4.assistant.ui.AssistantConstants;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class WaypointFigure extends SelectableFigure implements IDetailedFigure {
	// - S T A T I C - S E C T I O N ..........................................................................
	public Font							FONT_BOATNAME	= new Font(Display.getDefault(), "Arial", 14, SWT.NORMAL);
	// - F I E L D - S E C T I O N ............................................................................
	private final Waypoint	waypointModel;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WaypointFigure(Waypoint model) {
		//		super(model);
		waypointModel = model;
		if (waypointModel.getType() == WaypointTypes.NOACTION) {
			this.setBackgroundColor(AssistantConstants.COLOR_GRAY);
			this.setForegroundColor(AssistantConstants.COLOR_GRAY);
		}
		if (waypointModel.getState() == WaypointStates.ACTIVE)
			this.setBackgroundColor(AssistantConstants.COLOR_DARK_GREEN);
		if (waypointModel.getState() == WaypointStates.NOACTIVE)
			this.setBackgroundColor(AssistantConstants.COLOR_DARK_GRAY);
		if (waypointModel.getState() == WaypointStates.ONHOLD) this.setBackgroundColor(AssistantConstants.COLOR_DARK_BLUE);
		if (waypointModel.getState() == WaypointStates.SURPASSED)
			this.setBackgroundColor(AssistantConstants.COLOR_DARK_YELLOW);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createContents() {
		this.removeAll();

		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 10;
		grid.marginHeight = 0;
		grid.marginWidth = 1;
		grid.numColumns = 2;
		grid.verticalSpacing = 0;
		this.setLayoutManager(grid);
		this.setBorder(new LineBorder(1));

		Label iconCell = new Label(ImageFactory.getImage("icons/Waypoint.jpg"));
		iconCell.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(5);
		this.add(iconCell);
		addWaypointContents();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	protected void addWaypointContents() {
		Figure container = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 2;
		grid.numColumns = 2;
		grid.verticalSpacing = 0;
		container.setLayoutManager(grid);
		if (waypointModel.getType() == WaypointTypes.NOACTION) {
			container.setBackgroundColor(AssistantConstants.COLOR_GRAY);
			container.setForegroundColor(AssistantConstants.COLOR_GRAY);
		}
		if (waypointModel.getState() == WaypointStates.ACTIVE)
			container.setBackgroundColor(AssistantConstants.COLOR_DARK_GREEN);
		if (waypointModel.getState() == WaypointStates.NOACTIVE)
			container.setBackgroundColor(AssistantConstants.COLOR_DARK_GRAY);
		if (waypointModel.getState() == WaypointStates.ONHOLD)
			container.setBackgroundColor(AssistantConstants.COLOR_DARK_BLUE);
		if (waypointModel.getState() == WaypointStates.SURPASSED)
			container.setBackgroundColor(AssistantConstants.COLOR_DARK_YELLOW);

		Label waypointName = new Label(waypointModel.getName());
		waypointName.setFont(FONT_BOATNAME);
		container.add(waypointName);

		final MultiLabelLine heading = new MultiLabelLine();
		heading.setDefaultFont("Tahoma", 10);
		heading.addColumn("Type:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		heading.addColumn(waypointModel.getType().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		final MultiLabelLine awd = new MultiLabelLine();
		awd.setDefaultFont("Tahoma", 10);
		awd.addColumn("AWD: ", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		awd.addColumn(waypointModel.getMaxAWD() + "/" + waypointModel.getMaxAWD(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//		final MultiLabelLine speed = new MultiLabelLine();
		//		speed.setDefaultFont("Tahoma", 10);
		//		speed.addColumn("Speed:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		//		speed.addColumn(nf2.format(boatModel.getSpeed()) + " knts", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		//		final MultiLabelLine sail = new MultiLabelLine();
		//		sail.setDefaultFont("Tahoma", 10);
		//		sail.addColumn("Sail:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		//		sail.addColumn(boatModel.getSail().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		final MultiLabelLine location = new MultiLabelLine();
		location.setDefaultFont("Tahoma", 10);
		location.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		location.addColumn(waypointModel.getLocation().toReport().replace('\t', '-'), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		final MultiLabelLine range = new MultiLabelLine();
		range.setDefaultFont("Tahoma", 10);
		range.addColumn("Range:", SWT.NORMAL, PilotUIConstants.COLOR_BLACK);
		range.addColumn(new Double(waypointModel.getRange()).toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		container.add(heading);
		container.add(awd);

		//		container.add(speed);
		//		container.add(sail);

		container.add(location);
		container.add(range);
		this.add(container);
	}

	public void addExecutionData(String newValue) {
		createContents();
		Label executionData = new Label(newValue);
		executionData.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		this.add(executionData);
	}
}
// - UNUSED CODE ............................................................................................
