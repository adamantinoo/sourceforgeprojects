//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SpotTreeFacet.java 184 2008-09-25 16:01:50Z boneymen $
//  LAST UPDATE:    $Date: 2008-09-25 18:01:50 +0200 (jue, 25 sep 2008) $
//  RELEASE:        $Revision: 184 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models.facets;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.graphics.Image;

import net.sf.vgap4.assistant.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotTreeFacet implements IPilotTreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.assistant.models.viewers");

	// - F I E L D - S E C T I O N ............................................................................
	private PilotBoat	delegate;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotTreeFacet(PilotBoat target) {
		this.delegate = target;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getName() {
		return delegate.getName();
	}

	public PilotBoat getDelegate() {
		return delegate;
	}

	public Image getImage() {
		return ImageFactory.getImage("icons/starship.gif");
	}
}

// - UNUSED CODE ............................................................................................
