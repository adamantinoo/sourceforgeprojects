//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import com.xyz.article.wizards.HolidayMainPage;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatPropertiesMainPage extends HolidayMainPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	private Text	boatNameText;
	private Text	boatIdText;
	private Text	boatClefText;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatPropertiesMainPage(IWorkbench workbench, IStructuredSelection selection) {
		super(workbench, selection);
		setTitle("Enter Boat Identification");
		setDescription("Enter the Boat indentification data such as the name, the boat number id\n and the access key to the game site identified as the clef item.");
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void createControl(Composite parent) {
		//- Create the composite to hold the widgets
		GridData gd;
		Composite composite = new Composite(parent, SWT.NULL);

		//- Create the desired layout for this wizard page
		GridLayout gl = new GridLayout();
		int ncol = 2;
		gl.numColumns = ncol;
		composite.setLayout(gl);

		//- Boat Name
		new Label(composite, SWT.NONE).setText("Boat Name:");
		boatNameText = new Text(composite, SWT.BORDER);
		//- Boat Identification number				
		new Label(composite, SWT.NONE).setText("Boat Code Number:");
		boatIdText = new Text(composite, SWT.BORDER);
		//- Boat clef
		new Label(composite, SWT.NONE).setText("Boat Clef:");
		boatClefText = new Text(composite, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		boatClefText.setLayoutData(gd);

		//- Set the composite as the control for this page
		setControl(composite);
		boatNameText.addListener(SWT.KeyUp, this);
		boatIdText.addListener(SWT.KeyUp, this);
		boatClefText.addListener(SWT.KeyUp, this);

		//- Load interface values with model values.
		onEnterPage();
		int d = 0;
	}

	private void onEnterPage() {
		NewBoatWizard wiz = (NewBoatWizard) getWizard();
		PilotBoat model = wiz.getModel();

		if (null != model.getName()) boatNameText.setText(model.getName());
		if (null != model.getBoatId()) boatIdText.setText(model.getBoatId());
		if (null != model.getBoatClef()) boatClefText.setText(model.getBoatClef());
	}

	@Override
	public IWizardPage getNextPage() {
		return null;
	}

	@Override
	public boolean canFlipToNextPage() {
		return false;
	}

	@Override
	public void saveDataToModel() {
		// Gets the model
		NewBoatWizard wizard = (NewBoatWizard) getWizard();
		PilotBoat model = wizard.getModel();

		model.setName(boatNameText.getText());
		model.setBoatId(boatIdText.getText());
		model.setBoatClef(boatClefText.getText());
		//		try {
		//			model.updateBoatData();
		//		} catch (DataLoadingException dle) {
		//			Status status = new Status(IStatus.ERROR, "not_used", 0, dle.getLocalizedMessage(), null);
		//			applyToStatusLine(findMostSevere());
		//			getWizard().getContainer().updateButtons();
		//		} catch (BoatNotFoundException bnfe) {
		//			Status status = new Status(IStatus.ERROR, "not_used", 0, bnfe.getLocalizedMessage(), null);
		//			applyToStatusLine(findMostSevere());
		//			getWizard().getContainer().updateButtons();
		//		}
	}

	public boolean verifyDataModel() {
		// Gets the model
		NewBoatWizard wizard = (NewBoatWizard) getWizard();
		PilotBoat model = wizard.getModel();

		model.setName(boatNameText.getText());
		model.setBoatId(boatIdText.getText());
		model.setBoatClef(boatClefText.getText());
		try {
			model.updateBoatData();
		} catch (DataLoadingException dle) {
			Status status = new Status(IStatus.ERROR, "not_used", 0, dle.getLocalizedMessage(), null);
			applyToStatusLine(findMostSevere());
			getWizard().getContainer().updateButtons();
			return false;
		} catch (BoatNotFoundException bnfe) {
			Status status = new Status(IStatus.ERROR, "not_used", 0, bnfe.getLocalizedMessage(), null);
			applyToStatusLine(findMostSevere());
			getWizard().getContainer().updateButtons();
			return false;
		}
		return true;
	}
}

// - UNUSED CODE ............................................................................................
