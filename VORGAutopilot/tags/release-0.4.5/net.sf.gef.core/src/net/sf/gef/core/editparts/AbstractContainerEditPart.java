//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import net.sf.gef.core.model.AbstractGEFNode;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractContainerEditPart extends AbstractGenericEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.gef.core.editparts");
	/** Property ID to use when new model data is added to the diagram. */
	public static final String	DATA_ADDED_PROP				= "AbstractContainerEditPart.DATA_ADDED_PROP";
	public static final String	DATA_REMOVED_PROP			= "AbstractContainerEditPart.DATA_REMOVED_PROP";
	public static final String	CHILD_ADDED_PROP			= "AbstractContainerEditPart.CHILD_ADDED_PROP";
	public static final String	CHILD_REMOVED_PROP		= "AbstractContainerEditPart.CHILD_REMOVED_PROP";
	public static final String	STRUCTURE_CHANGE_PROP	= "AbstractContainerEditPart.STRUCTURE_CHANGE_PROP";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractContainerEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AbstractContainerEditPart.DATA_ADDED_PROP.equals(prop)) refreshChildren();
		if (AbstractContainerEditPart.DATA_REMOVED_PROP.equals(prop)) refreshChildren();
		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) refreshChildren();
		//		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) refreshChildren();
		if (AbstractContainerEditPart.STRUCTURE_CHANGE_PROP.equals(prop)) refreshChildren();
	}
}

// - UNUSED CODE ............................................................................................
