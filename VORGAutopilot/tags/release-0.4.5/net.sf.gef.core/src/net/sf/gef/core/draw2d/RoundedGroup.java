//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: RoundedGroup.java 211 2009-03-18 14:44:40Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-18 15:44:40 +0100 (mié, 18 mar 2009) $
//  RELEASE:        $Revision: 211 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.draw2d;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;

// - CLASS IMPLEMENTATION ...................................................................................
public class RoundedGroup extends RoundedRectangle {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger		= Logger.getLogger("net.sf.vgap4.assistant.figures.draw2d");
	private static final int	DEFAULT_ROUNDSIZE	= 6;

	// - F I E L D - S E C T I O N ............................................................................
	private GridLayout				grid							= null;
	public Label							groupName					= new StandardLabel();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RoundedGroup() {
		setRoundSize(DEFAULT_ROUNDSIZE);
		grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 3;
		grid.marginHeight = 4;
		grid.marginWidth = 4;
		grid.numColumns = 1;
		setLayoutManager(grid);
		groupName.setText("indefinido");
		this.add(groupName);

		this.setSize(getPreferredSize());
	}

	public RoundedGroup(int size) {
		this();
		setRoundSize(size);
		this.setSize(getPreferredSize());
	}

	private void setRoundSize(int size) {
		setCornerDimensions(new Dimension(size, size));
	}

	public RoundedGroup(String text, int bold) {
		this();
		groupName.setText(text);
		if (SWT.BOLD == bold) groupName.setFont(Draw2DConstants.FONT_MAP_BOLD);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setText(String text) {
		groupName.setText(text);
		this.setSize(getPreferredSize());
		this.repaint();
	}

	public void reset() {
		removeAll();
		this.add(groupName);
	}
}

// - UNUSED CODE ............................................................................................
