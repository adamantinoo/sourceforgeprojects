//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.core;

//- IMPORT SECTION .........................................................................................
import net.sf.vorg.core.enums.InputHandlerStates;
import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IInputHandler {

	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public String getFilePath();

	public InputTypes getType();

	public boolean loadContents();

	public void setStore(final PilotModelStore newStore);

	public InputHandlerStates getState();
}

// - UNUSED CODE ............................................................................................
