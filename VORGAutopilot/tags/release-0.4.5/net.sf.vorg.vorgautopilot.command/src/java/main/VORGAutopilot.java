//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

// - IMPORT SECTION .........................................................................................
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;
import net.sf.vorg.vorgautopilot.parsers.VRToolNavParser;
import net.sf.vorg.vorgautopilot.parsers.XMLFileParser;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGAutopilot {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger					= Logger.getLogger("net");
	private static final String		APPLICATIONNAME	= "VORGAutopilot";
	private static VORGAutopilot	singleton;
	private static boolean				onDebug					= false;
	private static PrintWriter		printer;
	private static int						refresh					= 10;
	public static int							timeDelay				= 15;
	static {
		VORGAutopilot.logger.setLevel(Level.OFF);
	}

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	public static int getRefresh() {
		return VORGAutopilot.refresh;
	}

	// - M A I N - S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		VORGAutopilot.singleton = new VORGAutopilot(args);
		VORGAutopilot.singleton.execute();
		VORGAutopilot.exit(0);
	}

	public static boolean onDebug() {
		return VORGAutopilot.onDebug;
	}

	public static void output(final String message) {
		System.out.println(message);
		if (null == VORGAutopilot.printer)
			try {
				VORGAutopilot.printer = new PrintWriter("VORGAutopilot.output.txt");
				VORGAutopilot.printer.println(message);
				VORGAutopilot.printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			VORGAutopilot.printer.println(message);
			VORGAutopilot.printer.flush();
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	private String	configurationFileName	= null;
	private boolean	activateVRTool				= false;
	private boolean	activateXMLFile				= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/*
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are: <ul> <li><b>-conf<font
	 * color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where the application will
	 * expect the configuration files and data.</li> <li><b>-res<font color="GREY">[ourcesLocation</font></b>
	 * ${RESOURCEDIR} - is the directory where the application is going to locate the files that contains the
	 * SQL statements and other application resources.
	 */
	public VORGAutopilot(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		banner();

		// - Process parameters and store them into the instance fields
		processParameters(args, VORGAutopilot.APPLICATIONNAME);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/*
	 * Starts a new thread with the autopilot configuration file. From that file the pilot gets all the date
	 * needed to run the boat.
	 */
	public void execute() {
		// - Detect the type of configuration file and then use the right configuration reader.
		final PilotModelStore model = new PilotModelStore();
		model.setRefreshInterval(refresh);
		model.setTimeDeviation(timeDelay);
		if (activateVRTool) {
			model.setInputHandler(new VRToolNavParser(configurationFileName));
			model.run();
		}
		if (activateXMLFile) {
			model.setInputHandler(new XMLFileParser(configurationFileName));
			model.run();
		}
	}

	public void processParameters(final String[] args, final String ApplicationName) {
		// super.processParameters(args, ApplicationName);
		for (int i = 0; i < args.length; i++) {
			VORGAutopilot.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-conf")) {
					// - Get and open the file with the autopilot configuration
					configurationFileName = argumentStringValue(args, i);
					activateXMLFile = true;
					//					if (validateConfiguration(configurationFileName))
					//						continue;
					//					else
					//						VORGAutopilot.exit(VORGConstants.INVALIDCONFIGURATION);
				}
				if (args[i].startsWith("-nav")) {
					// - Get and open the file with the autopilot configuration
					configurationFileName = argumentStringValue(args, i);
					activateVRTool = true;
				}
				if (args[i].startsWith("-refr")) { //$NON-NLS-1$
					VORGAutopilot.refresh = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-time")) { //$NON-NLS-1$
					VORGAutopilot.timeDelay = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-debug")) { //$NON-NLS-1$
					VORGAutopilot.onDebug = true;
					VORGAutopilot.logger.setLevel(Level.ALL);
					continue;
				}
				if (args[i].startsWith("-help")) { //$NON-NLS-1$
					help();
					VORGAutopilot.exit(0);
				}
			}
		}
		// ... Check that required parameters have values.
		if (null == configurationFileName)
			VORGAutopilot.exit(VORGConstants.NOCONFIG);
	}

	protected double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	protected int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	protected String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			VORGAutopilot.exit(VORGConstants.NOCONFIG);
		return "";
	}

	//	/*
	//	 * Checks if the configuration file exists and has content. It does not check the content format or the
	//	 * content structure, at least on this release
	//	 */
	//	private boolean validateConfiguration(final String configurationFileName) {
	//		try {
	//			final BufferedReader creader = new BufferedReader(new FileReader(configurationFileName));
	//			creader.close();
	//		} catch (final FileNotFoundException fnfe) {
	//			// TODO Auto-generated catch block
	//			fnfe.printStackTrace();
	//			return false;
	//		} catch (final IOException ioe) {
	//			// TODO Auto-generated catch block
	//			ioe.printStackTrace();
	//			return false;
	//		}
	//		return true;
	//	}

	private void banner() {
		System.out.println("__     _____  ____   ____    _         _              _ _       _   ");
		System.out.println("\\ \\   / / _ \\|  _ \\ / ___|  / \\  _   _| |_ ___  _ __ (_) | ___ | |_ ");
		System.out.println(" \\ \\ / / | | | |_) | |  _  / _ \\| | | | __/ _ \\| '_ \\| | |/ _ \\| __|");
		System.out.println("  \\ \\ /| |_| |  _ <| |_| |/ ___ \\ |_| | || (_) | |_) | | | (_) | |_ ");
		System.out.println("   \\_/  \\___/|_| \\_\\\\____/_/   \\_\\__,_|\\__\\___/| .__/|_|_|\\___/ \\__|");
		System.out.println("                                               |_|                  ");
		System.out.println();
		System.out.println(VORGConstants.VERSION);
		System.out.println();
	}

	private void help() {
		System.out.println("Description:");
		System.out
				.println("   Aplication to run the boat based on a configuration file with the commands and points to follow.");
		System.out.println();
		System.out.println("Command API for the RouteFinder:");
		System.out.println("   java -classpath vorgautopilot019.jar net.sf.vorg.vorgautopilot.command.main.VORGAutopilot ");
		System.out.println("Allowed parameters:");
		System.out.println("    -config <file name>  -- configuration file with the autentication and the route");
		System.out.println("    -nav <.NAV file path>  -- path to the VRTool file that defines pilot routes for boats.");
		System.out
				.println("    -time <seconds>  -- number of seconds to delay from the minute to sinchrinize with server refresh.");
		System.out.println("    -refresh <minutes> number of minutes between each configurtation refresh.");
		System.out.println();
	}
}

// - UNUSED CODE ............................................................................................
