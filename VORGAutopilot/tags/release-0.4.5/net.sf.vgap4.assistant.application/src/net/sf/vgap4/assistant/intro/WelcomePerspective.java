//  PROJECT:        net.sf.vgap4.assistant.application
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vgap4.assistant.intro;

//- IMPORT SECTION .........................................................................................
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

//- CLASS IMPLEMENTATION ...................................................................................
public class WelcomePerspective implements IPerspectiveFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	PERSPECTIVE_ID	= "net.sf.vgap4.assistant.application.welcome.perspective";

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createInitialLayout(final IPageLayout layout) {
	}
}
