//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;

import main.RouteCalculator;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;

//- CLASS IMPLEMENTATION ...................................................................................
public class Route {
	private static final String	HEADER							= "Name\tLat\tLon\tLat Deg\tLonDeg\tAlpha\tSpeed\tSail\tDiff Lat\tDiff Lon\tDistance\tTTC\tW Speed\tW Dir\tWind Time\tDistance\tElapsed time\tETA\tVRTool Data\tAWD";
	protected static double			ITERATION_INCREMENT	= 0.01;
	private static int					defaultIterations		= 100;

	public static void setDeepLevel(final int level) {
		Route.defaultIterations = level;
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected Vector<RouteCell>					route									= new Vector<RouteCell>();
	protected Vector<RouteControl>			controls							= new Vector<RouteControl>();
	protected Vector<GeoLocation>				savedConfiguration		= new Vector<GeoLocation>();
	protected RouteState								creationState					= RouteState.EMPTY;
	protected RouteCell									lastNode							= null;
	private final Vector<RouteControl>	windChangeReferences	= new Vector<RouteControl>();
	private final Calendar							creationTime					= Calendar.getInstance();
	private long												iterationCounter			= 0;
	private long												lastIteration					= 0;
	private double											bestTime							= Double.POSITIVE_INFINITY;
	private String											name;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Route() {
	}

	public void add(final RouteCell newCell, final Directions direction) throws LocationNotInMap {
		// - Check for the start formation of the route.
		if (creationState == RouteState.EMPTY) {
			route.add(newCell);
			lastNode = newCell;
			creationState = RouteState.MIDDLE;
			return;
		}
		if (creationState == RouteState.MIDDLE) {
			route.add(newCell);
			final RouteControl theControl = new RouteControl(newCell.getEntryLocation(), direction, lastNode, newCell);
			controls.add(theControl);
			lastNode = newCell;
			return;
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Adds a new wind cell to the Route. This procedure checks for the formation of the Route and determines if
	 * this is a Start cell or a middle cell and also connect the controls.
	 * 
	 * @param direction
	 * @throws LocationNotInMap
	 */
	public void add(final WindCell routeCell, final Directions direction, final GeoLocation entry, final GeoLocation exit)
			throws LocationNotInMap {
		// - If the start and end locations are the same we can skip this cell.
		if (entry.isEquivalent(exit)) return;
		final RouteCell newCell = new RouteCell(routeCell, entry, exit);
		this.add(newCell, direction);
	}

	/**
	 * Update the wind data from the maps but by evaluating each cell each time to detect wind shifts and
	 * recalculate all cell data.
	 * 
	 * @throws LocationNotInMap
	 */
	public void adjustWindChanges() throws LocationNotInMap {
		final Vector<RouteCell> newRouteList = new Vector<RouteCell>();
		double elapsed = 0.0;
		final Iterator<RouteCell> rit = route.iterator();
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			final double cellTTC = routeCell.getTTC();
			if (this.checkIfWindChange(elapsed, cellTTC)) {
				// - Check if this cell is already a
				// - New algorithm updates the list of cells and controls on the route.
				final WindChangeCell leg = new WindChangeCell(routeCell, elapsed);

				final RouteControl currentControl = routeCell.getRightControl();
				final RouteControl nextControl = routeCell.getLeftControl();
				// - Connect the new leg in the position and reconnect the controls
				if (null == currentControl) {
					// - This is the first cell so does not have right control.
					nextControl.setLeft(leg.getRight());
					controls.insertElementAt(leg.getControl(), 0);
				} else if (null == nextControl) {
					currentControl.setRight(leg.getLeft());
					controls.add(leg.getControl());
				} else {
					currentControl.setRight(leg.getLeft());
					controls.insertElementAt(leg.getControl(), controls.indexOf(currentControl) + 1);
					nextControl.setLeft(leg.getRight());
				}
				// - Add both cells to the new list.
				newRouteList.add(leg.getLeft());
				newRouteList.add(leg.getRight());
				if (leg.getLeft().getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
				elapsed += leg.getLeft().getTTC();
				if (leg.getRight().getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
				elapsed += leg.getRight().getTTC();
			} else {
				final WindCell windData = WindMapHandler.cellWithPoint(routeCell.getCell().getLocation(), this.newSearchTime(
						elapsed).getTime());
				routeCell.setWindData(windData);
				newRouteList.add(routeCell);
				if (routeCell.getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
				elapsed += routeCell.getTTC();
			}
		}
		route = newRouteList;
	}

	public Calendar getCreationTime() {
		return (Calendar) creationTime.clone();
	}

	public Calendar getEndTime() {
		final Calendar start = this.getCreationTime();
		start.add(Calendar.MINUTE, new Double(this.getRouteTTC() * VORGConstants.TOMINUTES).intValue());
		return start;
	}

	public double getRouteTTC() {
		final Iterator<RouteCell> rit = route.iterator();
		double elapsed = 0.0;
		while (rit.hasNext())
			elapsed += rit.next().getTTC();
		return elapsed;
	}

	public Calendar getSearchTime() {
		final Calendar now = (Calendar) creationTime.clone();
		now.add(Calendar.HOUR, -2); // Adjust for the two hour difference with wind cell stamp time
		final int elapsed = new Double(this.getRouteTTC() * VORGConstants.TOMINUTES).intValue();
		now.add(Calendar.MINUTE, elapsed);
		return now;
	}

	public GeoLocation getWindChange(final int position) {
		try {
			if (position < windChangeReferences.size())
				if (null != windChangeReferences.get(position - 1))
					return windChangeReferences.get(position - 1).getCurrentLocation();
			if (null != windChangeReferences.firstElement()) return windChangeReferences.firstElement().getCurrentLocation();
			return new GeoLocation();
		} catch (final NoSuchElementException nsee) {
			return null;
		}
	}

	/**
	 * Starts the process to calculate a new optimum route based on the current route setup. New route
	 * calculation will be performed by exploring the values possible for the route controls.
	 */
	public void optimizeRoute() {
		// - Start optimization with the initial control and the recursively.
		this.optimizeControl(0);

		// - Update the route with the stored avlues of the best route.
		for (int index = 0; index < controls.size(); index++) {
			final RouteControl control = controls.get(index);
			control.storeLocation(savedConfiguration.get(index));
		}
	}

	public String printReport() {
		// - Before printing the route data
		final StringBuffer buffer = new StringBuffer();
		buffer.append(name).append(" - [");
		buffer.append(creationTime.get(Calendar.DAY_OF_MONTH)).append("/");
		buffer.append(creationTime.get(Calendar.MONTH)).append(" ");
		buffer.append(creationTime.get(Calendar.HOUR_OF_DAY)).append(":");
		buffer.append(creationTime.get(Calendar.MINUTE)).append("]");
		buffer.append('\n');
		buffer.append(Route.HEADER).append('\n');
		final Iterator<RouteCell> rit = route.iterator();
		int routeCounter = 1;
		final RouteCell startNode = route.firstElement();
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			// - Check if this cell is the start of the route.
			if (startNode.equals(routeCell)) buffer.append(routeCell.printStartReport()).append('\n');
			buffer.append(routeCell.printReport(routeCounter++)).append('\n');
		}
		return buffer.toString();
	}

	public void setName(final String newName) {
		name = newName;
	}

	public void setWindChange() {
		windChangeReferences.add(controls.lastElement());
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(route.toString()).append("");
		buffer.append(VORGConstants.NEWLINE);
		buffer.append("").append(controls.toString()).append("");
		return buffer.toString();
	}

	public String vrtoolReport() {
		// - Before printing the route data
		final StringBuffer buffer = new StringBuffer();
		final Iterator<RouteCell> rit = route.iterator();
		int routeCounter = 0;
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			// - Check if this cell is the start of the route.
			// if (startNode.equals(routeCell)) buffer.append(routeCell.vrtoolStartReport()).append('\n');
			buffer.append(routeCell.vrtoolReport(routeCounter++));
		}
		// - Use that last control to get the last cell data.
		return buffer.toString();
	}

	private void evaluateRoute(final int controlId) {
		final double currentElapsed = this.getRouteTTC();

		// - Check if this is a better time.
		if (currentElapsed < bestTime) {
			bestTime = currentElapsed;
			if (RouteCalculator.onDebug())
				System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
			this.saveControlConfiguration();
		}
	}

	private RouteControl getControl(final int controlId) {
		return controls.elementAt(controlId);
	}

	private boolean lastControl(final int controlId) {
		if (controlId + 1 == controls.size())
			return true;
		else
			return false;
	}

	private long levelIterations(final int controlId) {
		if (controlId == 0) return Route.defaultIterations;
		if (controlId < 4) return Route.defaultIterations / 10;
		if (controlId < 8) return Route.defaultIterations / 20;
		return 3;
	}

	/**
	 * Main recursive calculation method. This release divides the scan in two sections and then applies
	 * different optimization procedures depending on the deep level of the control.
	 * 
	 * @param elapsed
	 */
	private void optimizeControl(final int controlId) {
		this.optimizerLevel2(controlId);
	}

	/**
	 * This optimizer trims the number of possible iterations based on the control deep level. For initial
	 * levels it scans the 50 per cent of the point while for higher levels this numer is reduced.
	 * 
	 * @param elapsed
	 */
	private void optimizerLevel2(final int controlId) {
		final RouteControl control = this.getControl(controlId);
		control.reset();

		final long iterations = this.levelIterations(controlId);
		boolean doUp = true;
		boolean doDown = true;
		for (int counter = 0; counter < iterations; counter++) {
			iterationCounter++;
			if (doUp) {
				doUp = control.adjustUp(controlId);
				if (doUp) if (this.lastControl(controlId))
					this.evaluateRoute(controlId);
				else
					this.optimizeControl(controlId + 1);
			}
			iterationCounter++;
			if (doDown) {
				doDown = control.adjustDown(controlId);
				if (doDown) if (this.lastControl(controlId))
					this.evaluateRoute(controlId);
				else
					this.optimizeControl(controlId + 1);
			}
		}
	}

	private void saveControlConfiguration() {
		savedConfiguration = new Vector<GeoLocation>(controls.size());
		for (int index = 0; index < controls.size(); index++) {
			final RouteControl targetControl = controls.get(index);
			savedConfiguration.add(index, targetControl.getCurrentLocation());
			targetControl.setOptimizedLocation(targetControl.getCurrentLocation());
		}
		lastIteration = iterationCounter;
	}

	/**
	 * Tests if this cell lies inside a wind change area. If the wind change time happens during the run on this
	 * cell the result of this method is <code>true</code>
	 */
	protected boolean checkIfWindChange(final double elapsed, final double runTTC) {
		final Calendar searchTime = Calendar.getInstance();
		searchTime.add(Calendar.MINUTE, new Double(elapsed * VORGConstants.TOMINUTES).intValue());
		final int elapsedHour = searchTime.get(Calendar.HOUR_OF_DAY);
		final int elapsedMinute = searchTime.get(Calendar.MINUTE);

		// elapsedDate = WindMapHandler.addElapsed(elapsedDate, elapsed);
		final double endHour = elapsedHour + elapsedMinute / 60.0 + runTTC;
		if (elapsedHour < 11) if (endHour > 11) return true;
		if (elapsedHour < 23) if (endHour > 23) return true;
		if (elapsedHour == 23) if (endHour > 35) return true;
		// else if (endHour > 23) return true;
		return false;
	}

	protected Calendar newSearchTime(final double elapsed) {
		final Calendar now = Calendar.getInstance();
		now.add(Calendar.HOUR, -2); // Adjust for the two hour difference with wind cell stamp time
		final int elapsedMinutes = new Double(elapsed * VORGConstants.TOMINUTES).intValue();
		now.add(Calendar.MINUTE, elapsedMinutes);
		return now;
	}

}

enum RouteState {
	EMPTY, MIDDLE, CLONED;
}
// - UNUSED CODE ............................................................................................
