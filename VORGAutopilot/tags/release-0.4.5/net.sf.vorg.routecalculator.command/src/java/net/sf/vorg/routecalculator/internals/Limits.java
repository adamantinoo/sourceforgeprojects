package net.sf.vorg.routecalculator.internals;

public class Limits {
	public static final String	NORTH_SOUTH			= "NS";
	public static final String	EAST_WEST				= "EW";
	private String							type						= Limits.NORTH_SOUTH;
	private double							westSouthlimit	= 0.0;
	private double							eastNorthLimit	= 0.0;

	public Limits(String type, final double left, final double right) {
		this.type = type;
		this.westSouthlimit = left;
		this.eastNorthLimit = right;
	}

	public Limits(final double left, final double right) {
		this.westSouthlimit = left;
		this.eastNorthLimit = right;
	}

	public double getWest() {
		return this.westSouthlimit;
	}

	public double getSouth() {
		return this.westSouthlimit;
	}

	public double getEast() {
		return this.eastNorthLimit;
	}

	public double getNorth() {
		return this.eastNorthLimit;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("[WindCell ");
		buffer.append("type=").append(this.type).append(",");
		buffer.append("WestSouth=").append(this.westSouthlimit).append(",");
		buffer.append("EastNorth=").append(this.eastNorthLimit).append("]");
		return buffer.toString();
	}
}
