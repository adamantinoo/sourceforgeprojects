//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.models.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
public class VMCData {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected final int				targetDirection;
	protected final int				windDirection;
	protected final double		windSpeed;
	private double						leftVMC							= 0.0;
	private int								leftAngle;
	private SailConfiguration	leftConfiguration		= new SailConfiguration();
	private double						rightVMC						= 0.0;
	private int								rightAngle;
	private SailConfiguration	rightConfiguration	= new SailConfiguration();
	private double						maxSpeed						= 0.0;
	private int								maxAngle;
	private SailConfiguration	maxConfiguration		= new SailConfiguration();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VMCData(double heading, WindCell startCell) {
		this(new Double(heading).intValue(), startCell);
	}

	public VMCData(int heading, WindCell startCell) {
		this(heading, startCell.getWindDir(), startCell.getWindSpeed());
	}

	public VMCData(int heading, int windDirection, double windSpeed) {
		this.targetDirection = heading;
		this.windDirection = windDirection;
		this.windSpeed = windSpeed;
		calculateVMC();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void calculateVMC() {
		// SailConfiguration rightConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		for (int rotation = 1; rotation < 180; rotation++) {
			int angle = GeoLocation.adjustAngleTo360(windDirection + rotation);
			SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
			double speed = configuration.getSpeed();
			double vmcSpeed = speed * Math.cos(Math.toRadians(targetDirection - angle));
			if (vmcSpeed > rightVMC) {
				rightVMC = vmcSpeed;
				rightAngle = angle;
				rightConfiguration = configuration;
			}
			if (speed > maxSpeed) {
				maxSpeed = speed;
				maxAngle = angle;
				maxConfiguration = configuration;
			}
		}
		// SailConfiguration leftConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		for (int rotation = 1; rotation < 180; rotation++) {
			int angle = GeoLocation.adjustAngleTo360(windDirection - rotation);
			SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
			double speed = configuration.getSpeed();
			double vmcSpeed = speed * Math.cos(Math.toRadians(targetDirection - angle));
			if (vmcSpeed > leftVMC) {
				leftVMC = vmcSpeed;
				leftAngle = angle;
				leftConfiguration = configuration;
			}
			if (speed > maxSpeed) {
				maxSpeed = speed;
				maxAngle = angle;
				maxConfiguration = configuration;
			}
		}
	}

	public void addLeftData(double leftVMC, int leftAngle, SailConfiguration leftConfiguration) {
		this.leftVMC = leftVMC;
		this.leftAngle = leftAngle;
		this.leftConfiguration = leftConfiguration;
	}

	public void addRightData(double rightVMC, int rightAngle, SailConfiguration rightConfiguration) {
		this.rightVMC = rightVMC;
		this.rightAngle = rightAngle;
		this.rightConfiguration = rightConfiguration;
	}

	public int getBestAngle() {
		if (leftConfiguration.getSpeed() >= rightConfiguration.getSpeed())
			return leftAngle;
		else
			return rightAngle;
	}

	public int getWorstAngle() {
		if (leftConfiguration.getSpeed() < rightConfiguration.getSpeed())
			return leftAngle;
		else
			return rightAngle;
	}

	public String printReport() {
		StringBuffer buffer = new StringBuffer();
		// NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		// nf.setMaximumFractionDigits(3);
		// nf.setMinimumFractionDigits(3);
		int awd = GeoLocation.calculateAWD(this.windDirection, maxAngle);
		buffer.append("Max Speed=").append(maxConfiguration);
		buffer.append(" course=").append(maxAngle).append(" - AWD=").append(awd).append(VORGConstants.NEWLINE);
		buffer.append("[VMC results").append(VORGConstants.NEWLINE);
		buffer.append("   Projection heading=").append(targetDirection).append(VORGConstants.NEWLINE);
		buffer.append("   Wind direction=").append(windDirection).append(VORGConstants.NEWLINE);
		buffer.append("   Wind speed=").append(FormatSingletons.nf3.format(windSpeed)).append(" knots").append(
				VORGConstants.NEWLINE);
		buffer.append("   Port VMC [boat speed=").append(FormatSingletons.nf3.format(leftConfiguration.getSpeed()));
		buffer.append("-").append(leftConfiguration.getSail()).append("] [VMC=");
		buffer.append(FormatSingletons.nf3.format(leftVMC)).append(" knots - heading=");
		buffer.append(leftAngle);
		awd = GeoLocation.calculateAWD(windDirection, leftAngle);
		buffer.append(" AWD=").append(awd * Integer.signum(awd));
		buffer.append("]").append(VORGConstants.NEWLINE);
		buffer.append("   Starboard VMC [boat speed=").append(FormatSingletons.nf3.format(rightConfiguration.getSpeed()));
		buffer.append("-").append(rightConfiguration.getSail());
		buffer.append("] [VMC=").append(FormatSingletons.nf3.format(rightVMC)).append(" knots - heading=");
		buffer.append(rightAngle);
		awd = GeoLocation.calculateAWD(windDirection, rightAngle);
		buffer.append(" AWD=").append(awd * Integer.signum(awd));
		buffer.append("]").append(VORGConstants.NEWLINE).append("]");
		return buffer.toString();
	}

	public String printRecord() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("VM PORT").append("\t\t\t\t\t");
		buffer.append(leftAngle).append("\t");
		buffer.append(leftConfiguration.getSpeed()).append("\t").append(leftConfiguration.getSail()).append("\t");
		buffer.append(windSpeed).append("\t").append(windDirection).append("\t");
		buffer.append(leftVMC).append("\t");
		buffer.append("\t").append(targetDirection).append("\n");/* .append("GMP+1-0H").append("\n"); */

		buffer.append("VM STAR").append("\t\t\t\t\t");
		buffer.append(rightAngle).append("\t");
		buffer.append(rightConfiguration.getSpeed()).append("\t").append(rightConfiguration.getSail()).append("\t");
		buffer.append(windSpeed).append("\t").append(windDirection).append("\t");
		buffer.append(rightVMC).append("\t");
		buffer.append("\t").append(targetDirection).append("\n");/* .append("GMP+1-0H").append("\n"); */
		return buffer.toString();
	}

	public SailConfiguration getBestSailConfiguration() {
		if (leftConfiguration.getSpeed() >= rightConfiguration.getSpeed())
			return leftConfiguration;
		else
			return rightConfiguration;
	}

	public int getMaxAngle() {
		return this.maxAngle;
	}
}

// - UNUSED CODE ............................................................................................
