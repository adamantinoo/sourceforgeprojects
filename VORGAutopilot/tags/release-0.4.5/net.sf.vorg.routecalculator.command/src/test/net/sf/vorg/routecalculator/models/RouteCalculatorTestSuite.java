//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import junit.framework.Test;
import junit.framework.TestSuite;
import net.sf.vorg.routecalculator.internals.TestGeoLocation;
import net.sf.vorg.routecalculator.internals.TestPolars;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCalculatorTestSuite {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");
	// private static WindMap testMap;
	// private static GeoLocation start;
	// private static GeoLocation end;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteCalculatorTestSuite() {
		// testMap = new WindMap();
		// testMap.addCell(new WindCell(3, 100, 180, 10.0));
		// testMap.addCell(new WindCell(3, 101, 90, 8.0));
		// testMap.addCell(new WindCell(3, 102, 0, 12.0));
		// start = new GeoLocation(2, 40, 100, 0);
		// end = new GeoLocation(3, 20, 102, 20);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public static Test suite() {
		TestSuite suite = new TestSuite("Test for net.sf.vorg.routecalculator.models");
		// $JUnit-BEGIN$
		suite.addTestSuite(TestGeoLocation.class);
		// suite.addTestSuite(TestWindMap.class);
		suite.addTestSuite(TestPolars.class);
		// suite.addTestSuite(TestRoute.class);
		// suite.addTestSuite(WindCellTest.class);
		// $JUnit-END$
		return suite;
	}
}

// - UNUSED CODE ............................................................................................
