//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: Application.java 184 2008-09-25 16:01:50Z boneymen $
//  LAST UPDATE:    $Date: 2008-09-25 18:01:50 +0200 (jue, 25 sep 2008) $
//  RELEASE:        $Revision: 184 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.app;

//- IMPORT SECTION .........................................................................................
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

//- CLASS IMPLEMENTATION ...................................................................................
public class WelcomePerspective implements IPerspectiveFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String	PERSPECTIVE_ID	= "net.sf.vorg.ui.WelcomePerspective.id";

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createInitialLayout(final IPageLayout layout) {
	}
}
