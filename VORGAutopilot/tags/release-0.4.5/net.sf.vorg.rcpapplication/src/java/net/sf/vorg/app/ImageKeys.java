/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License. 
 *  
 */

package net.sf.vorg.app;

/**
 * This class is used to define path for images
 * 
 * @author <a href="mailto:dev@directory.apache.org">Apache Directory Project</a>
 * @version $Rev: 38 $, $Date: 2008-03-17 15:45:56 +0100 (lun, 17 mar 2008) $
 */
public class ImageKeys {
	//- Images for Actions
	public static final String	EXIT									= "icons/exit.png";													//$NON-NLS-1$
	public static final String	ABOUT									= "resources/icons/about.png";								//$NON-NLS-1$
	public static final String	INTRO									= "resources/icons/intro.gif";								//$NON-NLS-1$
	public static final String	MANAGE_CONFIGURATION	= "resources/icons/manage-configuration.png"; //$NON-NLS-1$
	public static final String	REPORT_BUG						= "resources/icons/bug-report.png";					//$NON-NLS-1$
	public static final String	SEARCH_UPDATES				= "resources/icons/search-updates.png";			//$NON-NLS-1$
	public static final String	SHOW_PREFERENCES			= "resources/icons/preferences.png";					//$NON-NLS-1$

	//- Images for graphical icons
	public static final String	DISABLED_BOAT					= "icons/boatinvalid_16.bmp";
	public static final String	READY_BOAT						= "icons/boat_16.bmp";
	public static final String	ONHOLD_BOAT						= "icons/boathold_16.bmp";
	public static final String	ERROR_BOAT						= "icons/cannotrun.gif";
	public static final String	WAYPOINT_SMALL				= "icons/waypoint_16.gif";
	public static final String	WAYPOINT_BIG					= "icons/waypoint_30.gif";
	public static final String	ACTIVEWAYPOINT_BIG		= "icons/activewaypoint_30.gif";
	public static final String	BOAT_BIG							= "icons/boat_60.bmp";
	public static final String	WIND_CELL							= "icons/WindCell.jpg";
}
