//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

import net.sf.gef.core.wizards.AbstractWizard;
import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.wizards.PilotStoreWizardModel;

// - CLASS IMPLEMENTATION ...................................................................................
public class StoreConfigurationWizard extends AbstractWizard {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Reference to the model container that contains a copy of the model data and methods to mode this
	 * information to and from the original model.
	 */
	private PilotStoreWizardModel		model	= null;
	/** Reference to the workbench used to display some elements. */
	protected IWorkbench						workbench;
	/**
	 * The workbench selection when this wizard was started. Not used internally because we use the model to
	 * access the data.
	 */
	protected IStructuredSelection	selection;
	/**
	 * The page where the properties components will be drawn and operated. Can not be initialized to an empty
	 * element to avoid a failure of the wizard but in that case the output will be lost. If this field is not
	 * received at the creation of this instance then we should return an exception.
	 */
	private StorePropertiesPage			newStorePage;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public StoreConfigurationWizard(final PilotStoreWizardModel newWizardModel) {
		super();
		if (null == newWizardModel)
			throw new NullPointerException("The wizards should receive a valid WizardModel.");
		model = newWizardModel;
	}

	public StoreConfigurationWizard() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void addPages() {
		newStorePage = new StorePropertiesPage(model, workbench, selection);
		addPage(newStorePage);
	}

	@Override
	public boolean canFinish() {
		final IStatus status = newStorePage.getStatus();
		if (status.getSeverity() == IStatus.ERROR)
			return false;
		else
			return true;
	}

	public PilotStoreWizardModel getModel() {
		return model;
	}

	@Override
	public void init(final IWorkbench workbench, final IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
	}

	@Override
	public boolean performFinish() {
		newStorePage.saveDataToModel();
		return true;
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
class StorePropertiesPage extends WizardPage implements Listener {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	private final PilotStoreWizardModel	model;
	protected IWorkbench								workbench;
	protected IStructuredSelection			selection;
	protected IStatus										pageStatus;

	//- Widgets on the page
	private Text												inputLocationPath;
	private Spinner											refrestMinutes;
	private Spinner											timeSeconds;
	private InputTypes									inputType	= InputTypes.NONE;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public StorePropertiesPage(final PilotStoreWizardModel modelRef, final IWorkbench workbench,
			final IStructuredSelection selection) {
		super("Page1");
		setTitle("Configure Autopilot parameters.");
		setDescription("Set the location of the input file that contains the Routes. The Input File may be a VRTool .NAV file\n or a user configured .XML file. Other configuration parameters are the refresh attributes for the Autopilot operation.");
		setImageDescriptor(ImageFactory.getImageDescriptor("icons/configurestore_wiz.png"));
		model = modelRef;
		this.workbench = workbench;
		this.selection = selection;
		pageStatus = new Status(IStatus.OK, "not_used", 0, "", null);
	}

	@Override
	public boolean canFlipToNextPage() {
		return false;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void createControl(final Composite parent) {
		//- Create the composite to hold the widgets
		final Composite composite = new Composite(parent, SWT.NULL);
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.makeColumnsEqualWidth = false;
		composite.setLayout(grid);

		//- Create the page widgets from SWT components.
		//- Line for the document path and the selector button.
		new Label(composite, SWT.NONE).setText("Input File Location:");

		final Composite pathSelection = new Composite(composite, SWT.NULL);
		final GridLayout gridPath = new GridLayout();
		gridPath.numColumns = 2;
		gridPath.makeColumnsEqualWidth = false;
		pathSelection.setLayout(grid);
		inputLocationPath = new Text(pathSelection, SWT.BORDER);
		if (null != model.getInputPath()) {
			inputLocationPath.setText(model.getInputPath());
		}
		final GridData grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 300;
		inputLocationPath.setLayoutData(grdata);
		inputLocationPath.addListener(SWT.KeyUp, this);
		final Button clear = new Button(pathSelection, SWT.NONE);
		clear.setText("...");
		clear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent e) {
				//- Open the file selection dialog to select the file
				inputLocationPath.setText(StorePropertiesPage.this.selectFile());
				if (null == inputLocationPath.getText()) {
					pageStatus = new Status(IStatus.WARNING, "not_used", 0,
							"If the Configuration File is empty the Autopilot can not run.", null);
				} else {
					try {
						pageStatus = new Status(IStatus.OK, "not_used", 0, "", null);
						@SuppressWarnings("unused")
						final FileInputStream testFile = new FileInputStream(inputLocationPath.getText());
						//- Add the code to detect the file type.
						updateFileType(inputLocationPath.getText());
					} catch (final FileNotFoundException fnfe) {
						pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The Configuration File does not exist.", null);
					}
				}

				//- Show the most serious error
				StorePropertiesPage.this.applyToStatusLine(pageStatus);
				StorePropertiesPage.this.getWizard().getContainer().updateButtons();
			}
		});

		//- Line with the refresh time.
		new Label(composite, SWT.NONE).setText("Refresh in minutes:");
		refrestMinutes = new Spinner(composite, SWT.NONE);
		refrestMinutes.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.BOLD));
		refrestMinutes.setMaximum(90);
		refrestMinutes.setMinimum(1);
		refrestMinutes.setIncrement(1);
		refrestMinutes.setSelection(model.getRefreshInterval());
		refrestMinutes.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				model.setRefreshInterval(refrestMinutes.getSelection());
			}
		});

		//- Tine woth the time delay.
		new Label(composite, SWT.NONE).setText("Delay in seconds:");
		timeSeconds = new Spinner(composite, SWT.NONE);
		timeSeconds.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.BOLD));
		timeSeconds.setMaximum(90);
		timeSeconds.setMinimum(0);
		timeSeconds.setIncrement(1);
		timeSeconds.setSelection(model.getTimeDeviation());
		timeSeconds.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				model.setTimeDeviation(timeSeconds.getSelection());
			}
		});
		setControl(composite);
	}

	@Override
	public IWizardPage getNextPage() {
		return null;
	}

	public IStatus getStatus() {
		return pageStatus;
	}

	public void handleEvent(final Event event) {
		//- Initialize a variable with the no error status
		pageStatus = new Status(IStatus.OK, "not_used", 0, "", null);

		//- Check the event widget to perform the validation
		if (event.widget == inputLocationPath)
			if (null == inputLocationPath.getText()) {
				pageStatus = new Status(IStatus.WARNING, "not_used", 0,
						"If the Configuration File is empty the Autopilot can not run.", null);
			} else {
				try {
					@SuppressWarnings("unused")
					final FileInputStream testFile = new FileInputStream(inputLocationPath.getText());
					updateFileType(inputLocationPath.getText());
				} catch (final FileNotFoundException fnfe) {
					pageStatus = new Status(IStatus.ERROR, "not_used", 0,
							"The Configuration File does not exist. Key a valid .NAV or .XML file.", null);
				}
			}

		//- Show the most serious error
		applyToStatusLine(pageStatus);
		getWizard().getContainer().updateButtons();
	}

	private void updateFileType(String testFile) {
		if (testFile.toLowerCase().endsWith(".nav")) {
			inputType = InputTypes.NAV;
		} else if (testFile.toLowerCase().endsWith(".xml")) {
			inputType = InputTypes.XML;
		} else {
			inputType = InputTypes.NONE;
			pageStatus = new Status(IStatus.WARNING, "not_used", 0, "If the Configuration File is not of the right type.",
					null);
			//- Show the most serious error
			applyToStatusLine(pageStatus);
			getWizard().getContainer().updateButtons();
		}
	}

	public void saveDataToModel() {
		model.setInputPath(inputLocationPath.getText());
		model.setInputType(inputType);
		model.setRefreshInterval(refrestMinutes.getSelection());
		model.setTimeDeviation(timeSeconds.getSelection());
	}

	/**
	 * Applies the status to the status line of a dialog page.
	 */
	protected void applyToStatusLine(final IStatus status) {
		String message = status.getMessage();
		if (message.length() == 0) {
			message = null;
		}
		switch (status.getSeverity()) {
			case IStatus.OK:
				setErrorMessage(null);
				this.setMessage(message);
				break;
			case IStatus.WARNING:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.WARNING);
				break;
			case IStatus.INFO:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.INFORMATION);
				break;
			default:
				setErrorMessage(message);
				this.setMessage(null);
				break;
		}
	}

	//	protected IStatus findMostSevere() {
	//		return pageStatus;
	//	}

	//	/*
	//	 * Saves the uses choices from this page to the model. Called on exit of the page
	//	 */
	//	public void saveDataToModel() {
	//		// Gets the model
	//		HolidayWizard wizard = (HolidayWizard) getWizard();
	//		HolidayModel model = wizard.model;
	//
	//		//		model.resetFlights = true;
	//		//		if ((model.departure != null) && (model.destination != null))
	//		//		    if (model.departure.equals(fromText.getText()) &&
	//		//		    	model.destination.equals(toText.getText()))
	//		//	    		model.resetFlights = false;
	//		//		
	//		//	    // Saves the user choices in the model
	//		//		model.departure = fromText.getText();
	//		//		model.destination = toText.getText();
	//		//		model.departureDate = travelDate.getText() + " "+ 
	//		//			travelMonth.getText()+ " "+ travelYear.getText();
	//		//		model.returnDate = returnDate.getText() + " "+ 
	//		//			returnMonth.getText()+ " "+ returnYear.getText();
	//		//
	//		//		model.usePlane = planeButton.getSelection();
	//
	//		model.rentalCompany = boatNameText.getText();
	//		model.carPrice = boatIdText.getText();
	//		model.destination = boatClefText.getText();
	//
	//	}

	//	private String getExtension(final String newPath) {
	//		final int point = newPath.indexOf('.');
	//		if (point > 0)
	//			return newPath.substring(point);
	//		else
	//			return "NONE";
	//	}

	private String selectFile() {
		final FileDialog dialog = new FileDialog(workbench.getActiveWorkbenchWindow().getShell(), SWT.OPEN);
		dialog.setFilterNames(new String[] { "VRTool Nav files", "XML Configuration files" });
		dialog.setFilterExtensions(new String[] { "*.nav", "*.xml" }); //Windows wild cards
		//		dialog.setFilterPath ("c:\\"); //Windows path
		if (null != inputLocationPath.getText()) {
			dialog.setFileName(inputLocationPath.getText());
		}
		return dialog.open();
	}

	//	private static boolean isTextNonEmpty(Text t) {
	//		String s = t.getText();
	//		if ((s != null) && (s.trim().length() > 0)) return true;
	//		return false;
	//	}
	//
	//	private void createLine(Composite parent, int ncol) {
	//		Label line = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL | SWT.BOLD);
	//		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
	//		gridData.horizontalSpan = ncol;
	//		line.setLayoutData(gridData);
	//	}

	//	/**
	//	 * @return true iff all fields of the return dates are set
	//	 */
	//	private boolean isReturnDateSet() {
	//		if ((returnDate.getSelectionIndex() >= 0) && (returnMonth.getSelectionIndex() >= 0)
	//				&& (returnYear.getSelectionIndex() >= 0)) return true;
	//		return false;
	//	}
	//
	//	/*
	//	 * @return true if the travel and return dates are valid i.e. if the return date is the either the same or
	//	 * after the travel date
	//	 */
	//	private boolean validDates() {
	//		if (isReturnDateSet()) {
	//			// compare the years first, if not equal, we have an answer		
	//			int i = Integer.parseInt(returnYear.getText());
	//			int j = Integer.parseInt(travelYear.getText());
	//			if (i > j) return true;
	//			if (i < j) return false;
	//
	//			// if the years are equal, look at the month
	//			i = returnMonth.getSelectionIndex();
	//			j = travelMonth.getSelectionIndex();
	//			if (j == -1) {
	//				// if the travel month still contains the initial value the selection index is still -1
	//				// we need to find the index of the current month
	//				String month1 = travelMonth.getText();
	//				for (int k = 0; k < 12; k++)
	//					if (months[k].equals(month1)) {
	//						j = k;
	//						break;
	//					}
	//			}
	//			if (i > j) return true;
	//			if (i < j) return false;
	//
	//			// if the months are also equal, comparing the ddays we have the answer			
	//			i = Integer.parseInt(returnDate.getText());
	//			j = Integer.parseInt(travelDate.getText());
	//			if (i < j) return false;
	//			return true;
	//		}
	//		return false;
	//	}

}
// - UNUSED CODE ............................................................................................
