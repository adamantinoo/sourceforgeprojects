//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: GraphicalDetailedView.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.app.Activator;
import net.sf.vorg.factories.PilotEditPartFactory;
import net.sf.vorg.factories.PilotFigureFactory;
import net.sf.vorg.models.UIPilotModelStore;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
@SuppressWarnings("deprecation")
public class PilotStatusView extends ViewPart implements PropertyChangeListener/* ,ISelectionChangedListener */{
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String		ID						= "net.sf.vorg.views.PilotStatusView.id";
	//	private static Logger					logger				= Logger.getLogger("net.sf.vgap4.assistant.views");

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	/** The view cannot be an editor at the same time, so delegate all editor actions to this editor. */
	LocalGraphicalDetailedEditor	detailEditor	= null;
	/** This is the root of the editor's model. */
	private PilotModelStore				editorContainer;

	@SuppressWarnings("unused")
	private Composite							viewerRoot;
	@SuppressWarnings("unused")
	private IViewSite							viewSite;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotStatusView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(PilotStatusView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.<br>
	 * This class will set as the top presentation element a new <code>GraphicalDetailedEditor</code> that will
	 * present the selection received as a new MVC pattern
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// - Create a new editor and initialize it based on this view.
		viewerRoot = parent;
		detailEditor = new LocalGraphicalDetailedEditor(parent, this);
	}

	@Override
	public void dispose() {
		if (null != editorContainer) {
			editorContainer.removePropertyChangeListener(this);
		}
		super.dispose();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		viewSite = site;
		super.init(site);

		//- Connect this view with the content provider that contains the singleton model.
		editorContainer = (UIPilotModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		if (null == editorContainer) {
			editorContainer = new UIPilotModelStore();
			//		//- Configure the input location by hand.
			//		UIVRToolNavParser input = new UIVRToolNavParser("L:\\VORG\\Leg5-ChinaRio.nav");
			//		editorContainer.setInputHandler(input);
			//		//FIXME need to load the model
			//		input.loadContents();
		}
		editorContainer.addPropertyChangeListener(this);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) {
			editorContainer.fireStructureChange(PilotModelStore.MODEL_STRUCTURE_CHANGED, null, null);
		}
	}

	//	/**
	//	 * This event is fired any time the selection in the <code>SelectionInfoView</code> is changed. This method
	//	 * should get the selection parts that match a <code>ISelectablePart</code> and then create their
	//	 * visualization page to be added to the presentation list.<br>
	//	 * The parameter is a selection event that contains the final selection.<br>
	//	 * If the selection is a single object then visualize all their contents, but if the selection are multiple
	//	 * object, present them in the reduced form and let the user to click on them to expand their contents.
	//	 * 
	//	 * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
	//	 */
	//	public void selectionChanged(final IWorkbenchPart editorPart, final ISelection selection) {
	//		if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
	//			processSelection((StructuredSelection) selection);
	//			//			//TODO Apply all the filters that are defined on the list.
	//			//			//TODO Filter the EditParts that match the proper interface
	//			//			//			final StructuredSelection selectionContent = ;
	//			//			//			final StructuredSelection sel = (StructuredSelection) selection;
	//			//			final Iterator<Object> sit = ((StructuredSelection) selection).iterator();
	//			//			//TODO Check if the selection are EditParts or Model objects. Depending on this generate the new model.
	//			//			//- Get the model elements of the selection and move them to the Editor container.
	//			//			this.editorContainer.clear();
	//			//			//			final Vector<AssistantNode> models = new Vector<AssistantNode>();
	//			//
	//			//			while (sit.hasNext()) {
	//			//				final Object element = sit.next();
	//			//				if (element instanceof ISelectablePart) {
	//			//					//- Get the model of this part.
	//			//					Object model = ((AbstractEditPart) element).getModel();
	//			//					if (model instanceof Spot) {
	//			//						this.editorContainer.addChild(((Spot) model).getContents());
	//			//						continue;
	//			//					}
	//			//					if (model instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) model);
	//			//				}
	//			//				if (element instanceof Spot) {
	//			//					this.editorContainer.addChild(((Spot) element).getContents());
	//			//					continue;
	//			//				}
	//			//				if (element instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) element);
	//			//			}
	//			//			//TODO Notify the editor that the model has changed.
	//			//			this.editorContainer.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
	//		}
	//	}

	//		public void selectionChanged(final SelectionChangedEvent event) {
	//			//- Get the selection from the event.
	//			final ISelection selection = event.getSelection();
	//			if ((!selection.isEmpty()) && (selection instanceof StructuredSelection)) {
	//						 Iterator<Object> sit = ((StructuredSelection) selection).iterator();
	//						while (sit.hasNext()) {
	//							Object selected = sit.next();
	//							if(selected instanceof ActiveWaypointEditPart)((ActiveWaypointEditPart) selected).setSelected(value)
	//						}
	//			}
	//		}
	//
	//	private void processSelection(StructuredSelection selection) {
	//		//TODO Apply all the filters that are defined on the list.
	//		//TODO Filter the EditParts that match the proper interface
	//		//			final StructuredSelection selectionContent = ;
	//		//			final StructuredSelection sel = (StructuredSelection) selection;
	//		final Iterator<Object> sit = (selection).iterator();
	//		//TODO Check if the selection are EditParts or Model objects. Depending on this generate the new model.
	//		//- Get the model elements of the selection and move them to the Editor container.
	//		this.editorContainer.clear();
	//		//			final Vector<AssistantNode> models = new Vector<AssistantNode>();
	//
	//		while (sit.hasNext()) {
	//			final Object element = sit.next();
	//			if (element instanceof ISelectablePart) {
	//				//- Get the model of this part.
	//				Object model = ((AbstractEditPart) element).getModel();
	//				if (model instanceof Spot) {
	//					this.editorContainer.addChild(((Spot) model).getContents());
	//					continue;
	//				}
	//				if (model instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) model);
	//			}
	//			if (element instanceof Spot) {
	//				this.editorContainer.addChild(((Spot) element).getContents());
	//				continue;
	//			}
	//			if (element instanceof AssistantNode) this.editorContainer.addChild((AssistantNode) element);
	//		}
	//		//TODO Notify the editor that the model has changed.
	//		this.editorContainer.fireStructureChange(AssistantMap.DATA_ADDED_PROP, null, null);
	//	}

	public PilotModelStore getContainer() {
		return editorContainer;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		detailEditor.setFocus();
	}

}

class LocalGraphicalDetailedEditor extends BaseGraphicalEditor {
	//	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.views");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	ID	= "net.sf.vorg.editors.LocalGraphicalDetailedEditor.id";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private PilotStatusView			detailedView;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public LocalGraphicalDetailedEditor(Composite parent, PilotStatusView detailedView) {
		try {
			setEditDomain(new DefaultEditDomain(this));
			this.detailedView = detailedView;
			// - Register the view. This will remove the requirement to have the view declared as a static singleton
			Activator.addReference(ID, this);

			//- Access the selection editor to copy the initialization to this view editor.
			init(this.detailedView.getSite());
			createGraphicalViewer(parent);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public PilotModelStore getContents() {
		if (null != detailedView)
			return detailedView.getContainer();
		else
			return new PilotModelStore();
	}

	public void init(IWorkbenchPartSite site) throws PartInitException {
		LocalEmptyEditorSite editorSite = new LocalEmptyEditorSite(site);
		setSite(editorSite);
		setInput(null);
		getCommandStack().addCommandStackListener(this);
		//		// - Add this editor selection listener to the list of listeners of this window
		//		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
		initializeActionRegistry();
	}

	@Override
	protected void initializeGraphicalViewer() {
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(getContents()); // Set the contents of this graphical.
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setRootEditPart(new ScalableRootEditPart());
		viewer.setEditPartFactory(new PilotEditPartFactory(new PilotFigureFactory()));
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		//		// - Register this as a listener to the SelectionInfoView selection provider.
		//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		//		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);

		//		// configure the context menu provider
		//		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		//		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		//		viewer.setContextMenu(cmProvider);
		//		getSite().registerContextMenu(cmProvider, viewer);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Do nothing or we can bypass signaling to the main editor

	}

	//	@Override
	//	public void dispose() {
	//		// - Unregister this from the SelectionInfoView selection provider.
	//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
	//		if (null != provider) ((ISelectionProvider) provider).removeSelectionChangedListener(this);
	//		super.dispose();
	//	}

}

class LocalEmptyEditorSite implements IEditorSite {

	private final IWorkbenchPartSite	workbenchSite;

	public LocalEmptyEditorSite(IWorkbenchPartSite site) {
		workbenchSite = site;
	}

	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
		return workbenchSite.getAdapter(adapter);
	}

	public String getId() {
		return workbenchSite.getId();
	}

	@SuppressWarnings("deprecation")
	public IKeyBindingService getKeyBindingService() {
		return workbenchSite.getKeyBindingService();
	}

	public IWorkbenchPage getPage() {
		return workbenchSite.getPage();
	}

	public IWorkbenchPart getPart() {
		return workbenchSite.getPart();
	}

	public String getPluginId() {
		return workbenchSite.getPluginId();
	}

	public String getRegisteredName() {
		return workbenchSite.getRegisteredName();
	}

	public ISelectionProvider getSelectionProvider() {
		return workbenchSite.getSelectionProvider();
	}

	@SuppressWarnings("unchecked")
	public Object getService(Class api) {
		return workbenchSite.getService(api);
	}

	public Shell getShell() {
		return workbenchSite.getShell();
	}

	public IWorkbenchWindow getWorkbenchWindow() {
		return workbenchSite.getWorkbenchWindow();
	}

	@SuppressWarnings("unchecked")
	public boolean hasService(Class api) {
		return workbenchSite.hasService(api);
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuManager, selectionProvider);
	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuId, menuManager, selectionProvider);
	}

	public void setSelectionProvider(ISelectionProvider provider) {
		workbenchSite.setSelectionProvider(provider);
	}

	public IEditorActionBarContributor getActionBarContributor() {
		// TODO Auto-generated method stub
		return null;
	}

	public IActionBars getActionBars() {
		// TODO Auto-generated method stub
		return null;
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}
}
// - UNUSED CODE ............................................................................................
