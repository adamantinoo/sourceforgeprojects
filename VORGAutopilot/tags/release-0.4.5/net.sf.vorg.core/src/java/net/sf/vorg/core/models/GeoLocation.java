//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id:$
//  LAST UPDATE:    $Date: $
//  RELEASE:        $Revision: $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

import java.text.NumberFormat;
import java.util.Formatter;
import java.util.Locale;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Quadrants;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.internals");

	public static double adjustAngleTo360(final double heading) {
		if (heading == 0.0) return 360.0;
		if (heading > 360.0) return GeoLocation.adjustAngleTo360(heading - 360.0);
		if (heading < 0.0) return GeoLocation.adjustAngleTo360(heading + 360.0);
		return heading;
	}

	public static int adjustAngleTo360(final int heading) {
		if (heading == 0) return 360;
		if (heading > 360) return GeoLocation.adjustAngleTo360(heading - 360);
		if (heading < 0) return GeoLocation.adjustAngleTo360(heading + 360);
		return heading;
	}

	/**
	 * Calculate the AWd between the course of the boat and the wind direction. We have to take care of the
	 * limit when the boat angle and the wind are at different sides of the 0-360 course.
	 */
	public static int calculateAWD(final double windAngle, final double boatAngle) {
		double angle = windAngle - boatAngle;
		if (angle < 0) {
			if (angle < -180) angle += 360;
		} else if (angle > 180) angle -= 360;
		return new Long(Math.round(angle)).intValue();
	}

	/** Calculate the difference in grades between two angles taking on account the 0-360 rose wind. */
	public static double angleDifference(final double alpha1, final double alpha2) {
		double dalpha = Math.toRadians(alpha1) - Math.toRadians(alpha2);
		double continuous_alpha = Math.acos(Math.cos(dalpha));

		//		double a1 = GeoLocation.adjustAngleTo360(angle1);
		//		double a2 = GeoLocation.adjustAngleTo360(angle2);
		//		if(a1>180.0)a1=360.0-a1;
		//		if(a2>180.0)a2=360.0-a2;
		return GeoLocation.adjustAngleTo360(Math.toDegrees(continuous_alpha));
	}

	public static int angleDifference(final int angle1, final int angle2) {
		return new Double(angleDifference(new Double(angle1), new Double(angle2))).intValue();
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected double	latitude	= 0.0;
	protected double	longitude	= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GeoLocation() {
	}

	public GeoLocation(final double lat, final double lon) {
		latitude = lat;
		longitude = lon;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public GeoLocation(final int latGrade, final int latMinute, final int lonGrade, final int lonMinute) {
		latitude = latGrade * 1.0 + latMinute / 60.0;
		longitude = lonGrade * 1.0 + lonMinute / 60.0;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This method calculates the angle that forms a destination point with a start point. The resulting value
	 * may be a positive or a negative number. 0-360 normalization should be performed at the call level.
	 */
	public double angleTo(final GeoLocation destination) {
		double alpha = 0.0;
		double deltaLon = destination.getLon() - getLon();
		// - Control the change over the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon = 360.0 - (Math.abs(deltaLon) * Math.signum(deltaLon));
		deltaLon *= 60.0;

		// - Calculate the exaggerated latitude and then the loxodromic distance and angle.
		final double Bs = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + getLat() / 2.0))) - 23.0
				* Math.sin(Math.toRadians(getLat()));
		final double Be = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + destination.getLat() / 2.0))) - 23.0
				* Math.sin(Math.toRadians(destination.getLat()));
		final double deltaLat = Be - Bs;

		// // - Adjusted loxodromic distance.
		// double Ls = Math.toRadians(this.getLat());
		// double Ld = Math.toRadians(destination.getLat());
		// double ldelta = Math.toRadians(deltaLon);
		// double loxDistance = VORGConstants.EARTHRADIUS
		// * Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));

		// - And now the loxodromic angle.
		alpha = Math.toDegrees(Math.atan(deltaLon / deltaLat));
		if (deltaLat > 0) alpha = alpha + 360.0;
		if (deltaLat < 0) alpha = alpha + 180.0;

		return GeoLocation.adjustAngleTo360(alpha);
	}

	/**
	 * This method calculates the angle that forms a destination point with a start point. The resulting value
	 * may be a positive or a negative number. 0-360 normalization should be performed at the call level.
	 */
	public double angleTo2(final GeoLocation destination) {
		double alpha = 0.0;
		final double deltaLat = destination.getLat() - getLat();
		double deltaLon = destination.getLon() - getLon();
		// - Control the change over the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon = 360.0 - Math.abs(deltaLon);
		final double hyp = Math.hypot(deltaLat, deltaLon);

		// - In the case the origin and destination are the same we return by default the angle 0.
		if ((deltaLat == 0.0) && (deltaLon == 0.0)) return 0.0;

		Quadrants quadrant = Quadrants.QUADRANT_I;
		// - Detect the angle quadrant.
		if ((deltaLat >= 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_I;
		if ((deltaLat < 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_II;
		if ((deltaLat < 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_III;
		if ((deltaLat >= 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_IV;

		switch (quadrant) {
			case QUADRANT_I:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_II:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.PI - Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_III:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.PI - Math.asin(deltaLon / hyp);
				else
					alpha = 2.0 * Math.PI - Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_IV:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp) * -1.0;
				break;

			default:
				break;
		}

		return GeoLocation.adjustAngleTo360(Math.toDegrees(alpha));
	}

	public GeoLocation directEstimation(final double seconds, final double speed, final double angle) {
		final double time = seconds / (60.0 * 60.0);
		final double distance = speed * time;
		// double deltaLat = distance * Math.cos(Math.toRadians(angle));
		final double endLat = getLat() * 60 + distance * Math.cos(Math.toRadians(angle));

		final double Bs = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + getLat() / 2.0))) - 23.0
				* Math.sin(Math.toRadians(getLat()));
		final double Be = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + endLat / 2.0))) - 23.0
				* Math.sin(Math.toRadians(endLat));
		final double deltaLat = Be - Bs;
		double dlo;
		if ((angle == 90) || (angle == 270)) {
			// double apartamiento = distance;
			dlo = distance / Math.cos(Math.abs(Math.toRadians(getLat())));
			if (angle == 270) dlo = -dlo;
		} else
			dlo = deltaLat * Math.tan(Math.toRadians(angle));

		final double endLon = getLon() * 60 + dlo;
		return new GeoLocation(endLat, endLon);
	}

	public GeoLocation directEstimation(double distance, final double angle) {
		//	final double distance = speed * time;
		// double deltaLat = distance * Math.cos(Math.toRadians(angle));
		final double endLat = getLat() * 60 + distance * Math.cos(Math.toRadians(angle));

		final double Bs = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + getLat() / 2.0))) - 23.0
				* Math.sin(Math.toRadians(getLat()));
		final double Be = 3437.746 * Math.log(Math.tan(Math.toRadians(45 + (endLat / 60.0) / 2.0))) - 23.0
				* Math.sin(Math.toRadians(endLat / 60.0));
		final double deltaLat = Be - Bs;
		double dlo;
		if ((angle == 90) || (angle == 270)) {
			// double apartamiento = distance;
			dlo = distance / Math.cos(Math.abs(Math.toRadians(getLat())));
			if (angle == 270) dlo = -dlo;
		} else
			dlo = deltaLat * Math.tan(Math.toRadians(angle));

		final double endLon = getLon() * 60 + dlo;
		return new GeoLocation(endLat / 60.0, endLon / 60.0);
	}

	public double distance(final GeoLocation destination) {
		final double deltaLat = destination.getLat() - getLat();
		final double deltaLon = destination.getLon() - getLon();
		final double hyp = Math.hypot(deltaLat, deltaLon);
		final double Ls = Math.toRadians(getLat());
		final double Ld = Math.toRadians(destination.getLat());
		final double ldelta = Math.toRadians(deltaLon);
		final double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		return loxDistance;
	}

	public double getLat() {
		return latitude;
	}

	public double getLon() {
		return longitude;
	}

	public boolean isEquivalent(final GeoLocation targetLocation) {
		if (Math.abs(targetLocation.getLat() - getLat()) < VORGConstants.DIFFERENTIAL)
			if (Math.abs(targetLocation.getLon() - getLon()) < VORGConstants.DIFFERENTIAL) return true;
		return false;
	}

	public void setLat(final double lat) {
		latitude = lat;
	}

	public void setLat(final String newLatitude) {
		if (null == newLatitude) {
			this.setLat(0.0);
			return;
		}
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = newLatitude.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1)
				this.setLat(new Double(newLatitude).doubleValue());
			else {
				final int degree = new Integer(newLatitude.substring(0, pos)).intValue();
				final int minute = new Integer(newLatitude.substring(pos + 1, newLatitude.length())).intValue();
				// - Test for negative values.
				if (degree < 0)
					this.setLat(degree * 1.0 - minute / 60.0);
				else
					this.setLat(degree * 1.0 + minute / 60.0);
			}
		} catch (final Exception ex) {
			this.setLat(0.0);
		}
	}

	public void setLocation(final GeoLocation newLocation) {
		latitude = newLocation.getLat();
		longitude = newLocation.getLon();
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public void setLon(final double lon) {
		longitude = lon;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public void setLon(final String newLongitude) {
		if (null == newLongitude) {
			this.setLon(0.0);
			return;
		}
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = newLongitude.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1)
				this.setLon(new Double(newLongitude).doubleValue());
			else {
				final int degree = new Integer(newLongitude.substring(0, pos)).intValue();
				final int minute = new Integer(newLongitude.substring(pos + 1, newLongitude.length())).intValue();
				// - Test for negative values.
				if (degree < 0)
					this.setLon(degree * 1.0 - minute / 60.0);
				else
					this.setLon(degree * 1.0 + minute / 60.0);
			}
		} catch (final Exception ex) {
			this.setLon(0.0);
		}
	}

	public String toReport() {
		final StringBuffer buffer = new StringBuffer();
		// Formatter formatter = new Formatter(Locale.ENGLISH);
		int latDegree;
		long latMinute;
		// - Generate the numbers.
		if (latitude < 0) {
			latDegree = new Double(Math.floor(latitude * -1.0)).intValue();
			latMinute = Math.round(((latitude * -1.0) - Math.floor(latitude * -1.0)) * 60.0);
		} else {
			latDegree = new Double(Math.floor(latitude)).intValue();
			latMinute = Math.round((latitude - Math.floor(latitude)) * 60.0);
		}
		int lonDegree;
		long lonMinute;
		if (longitude < 0) {
			lonDegree = new Double(Math.floor(longitude * -1.0)).intValue();
			lonMinute = Math.round(((longitude * -1.0) - Math.floor(longitude * -1.0)) * 60.0);
		} else {
			lonDegree = new Double(Math.floor(longitude)).intValue();
			lonMinute = Math.round((longitude - Math.floor(longitude)) * 60.0);
		}

		// - Generate the output.
		Formatter formatter = new Formatter(Locale.ENGLISH);
		formatter.format("%1$02d� %2$02d'", latDegree, latMinute);
		if (latitude > 0)
			buffer.append(formatter).append(" N");
		else
			buffer.append(formatter).append(" S");
		buffer.append('\t');
		formatter = new Formatter(Locale.ENGLISH);
		formatter.format("%1$03d� %2$02d'", lonDegree, lonMinute);
		if (longitude > 0)
			buffer.append(formatter).append(" E");
		else
			buffer.append(formatter).append(" W");
		return buffer.toString();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[GeoLocation ");
		final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(4);
		nf.setMinimumFractionDigits(4);
		buffer.append("lat=").append(nf.format(latitude)).append(",");
		buffer.append("lon=").append(nf.format(longitude)).append("]");
		buffer.append(" [").append(toReport().replace('\t', ' ').replace('�', ' ')).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
