//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractGEFNode extends AbstractPropertyChanger implements IGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long		serialVersionUID	= 4649865295860939719L;
	/** Property ID to use when a child is added to this diagram. */
	public static final String	CHILD_ADDED_PROP	= "AbstractNode.CHILD_ADDED_PROP";

	// - F I E L D - S E C T I O N ............................................................................
	public IGEFNode							parent						= null;
	public Vector<IGEFNode>			children					= new Vector<IGEFNode>();

	// - M E T H O D - S E C T I O N ..........................................................................
	public IGEFNode getParent() {
		return parent;
	}

	public void addChild(IGEFNode child) {
		if (null != child) {
			children.add(child);
			child.setParent(this);
			firePropertyChange(AbstractGEFNode.CHILD_ADDED_PROP, null, child);
		}
	}

	public void removeChild(IGEFNode child) {
		if (null != child) {
			children.remove(child);
			firePropertyChange(AbstractGEFNode.CHILD_ADDED_PROP, child, null);
		}
	}

	public void setParent(IGEFNode newParent) {
		parent = newParent;
	}

	public Vector<IGEFNode> getChildren() {
		return children;
	}

	public String quote(String value) {
		if (null == value) return '"' + "" + '"';
		return '"' + value + '"';
	}

	public String quote(double value) {
		return '"' + new Double(value).toString() + '"';
	}

	public String quote(int value) {
		return '"' + new Integer(value).toString() + '"';
	}
}
// - UNUSED CODE ............................................................................................
