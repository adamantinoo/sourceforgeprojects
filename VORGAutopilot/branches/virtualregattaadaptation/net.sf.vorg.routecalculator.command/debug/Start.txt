[Route [
[RouteCell cell=[WindCell direction=80,speed=6.47948164152
          location=[GeoLocation lat=-3.0000,lon=158.0000] [3  0' S 158  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=153,distance=16.91941493602736,speed=9.64,ttc=1.7551260307082321
                entrylocation=[GeoLocation lat=-3.2493,lon=158.0400] [3  15' S 158  2' E]
                exitlocation=[GeoLocation lat=-3.5000,lon=158.1689] [3  30' S 158  10' E]], 
[RouteCell cell=[WindCell direction=47,speed=5.3995680346
          location=[GeoLocation lat=-4.0000,lon=158.0000] [4  0' S 158  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=137,distance=29.118127371409678,speed=9.01,ttc=3.231756644995525
                entrylocation=[GeoLocation lat=-3.5000,lon=158.1689] [3  30' S 158  10' E]
                exitlocation=[GeoLocation lat=-3.8550,lon=158.5000] [3  51' S 158  30' E]], 
[RouteCell cell=[WindCell direction=63,speed=6.47948164152
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=143,distance=26.16144282213496,speed=10.01,ttc=2.613530751462034
                entrylocation=[GeoLocation lat=-3.8550,lon=158.5000] [3  51' S 158  30' E]
                exitlocation=[GeoLocation lat=-4.2037,lon=158.7620] [4  12' S 158  46' E]], 
[RouteCell cell=[WindCell direction=63,speed=6.47948164152
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=145,distance=2.8520595386513525,speed=10.078,ttc=0.28299856505768534
                entrylocation=[GeoLocation lat=-4.2037,lon=158.7620] [4  12' S 158  46' E]
                exitlocation=[GeoLocation lat=-4.2427,lon=158.7892] [4  15' S 158  47' E]], 
[RouteCell cell=[WindCell direction=73,speed=9.71922246228
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=145,distance=18.84617678228949,speed=11.658000000000001,ttc=1.6165874748918758
                entrylocation=[GeoLocation lat=-4.2427,lon=158.7892] [4  15' S 158  47' E]
                exitlocation=[GeoLocation lat=-4.5000,lon=158.9695] [4  30' S 158  58' E]], 
[RouteCell cell=[WindCell direction=59,speed=10.259179265739998
          location=[GeoLocation lat=-5.0000,lon=159.0000] [5  0' S 159  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=143,distance=52.854933139089496,speed=12.708,ttc=4.1591857994247325
                entrylocation=[GeoLocation lat=-4.5000,lon=158.9695] [4  30' S 158  58' E]
                exitlocation=[GeoLocation lat=-5.2040,lon=159.5000] [5  12' S 159  30' E]], 
[RouteCell cell=[WindCell direction=59,speed=10.7991360692
          location=[GeoLocation lat=-5.0000,lon=160.0000] [5  0' S 160  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=143,distance=22.218618209408707,speed=12.708,ttc=1.748396144901535
                entrylocation=[GeoLocation lat=-5.2040,lon=159.5000] [5  12' S 159  30' E]
                exitlocation=[GeoLocation lat=-5.5000,lon=159.7231] [5  30' S 159  43' E]], 
[RouteCell cell=[WindCell direction=42,speed=11.33909287266
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=136,distance=55.163214202072176,speed=13.088,ttc=4.2147932611607715
                entrylocation=[GeoLocation lat=-5.5000,lon=159.7231] [5  30' S 159  43' E]
                exitlocation=[GeoLocation lat=-6.1643,lon=160.3611] [6  10' S 160  22' E]], 
[RouteCell cell=[WindCell direction=42,speed=11.33909287266
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=135,distance=3.4662162735559767,speed=13.065999999999999,ttc=0.26528518854706695
                entrylocation=[GeoLocation lat=-6.1643,lon=160.3611] [6  10' S 160  22' E]
                exitlocation=[GeoLocation lat=-6.2053,lon=160.4020] [6  12' S 160  24' E]], 
[RouteCell cell=[WindCell direction=43,speed=9.71922246228
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Thu Feb 26 09:00:00 CET 2009]
                alpha=135,distance=8.289999882534858,speed=12.406,ttc=0.668225042925589
                entrylocation=[GeoLocation lat=-6.2053,lon=160.4020] [6  12' S 160  24' E]
                exitlocation=[GeoLocation lat=-6.3031,lon=160.5000] [6  18' S 160  30' E]], 
[RouteCell cell=[WindCell direction=42,speed=11.33909287266
          location=[GeoLocation lat=-6.0000,lon=161.0000] [6  0' S 161  0' E]
          timeStamp=Thu Feb 26 09:00:00 CET 2009]
                alpha=136,distance=16.382451999957954,speed=13.088,ttc=1.2517154645444648
                entrylocation=[GeoLocation lat=-6.3031,lon=160.5000] [6  18' S 160  30' E]
                exitlocation=[GeoLocation lat=-6.5000,lon=160.6901] [6  30' S 160  41' E]]]
[
[RouteControl direction=EW, setup location=[GeoLocation lat=-3.5000,lon=158.1689] [3  30' S 158  10' E]
              optimizedLocation=[GeoLocation lat=-3.5000,lon=158.1689] [3  30' S 158  10' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-3.8550,lon=158.5000] [3  51' S 158  30' E]
              optimizedLocation=[GeoLocation lat=-3.8550,lon=158.5000] [3  51' S 158  30' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-4.2037,lon=158.7620] [4  12' S 158  46' E]
              optimizedLocation=[GeoLocation lat=-4.2037,lon=158.7620] [4  12' S 158  46' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-4.2427,lon=158.7892] [4  15' S 158  47' E]
              optimizedLocation=[GeoLocation lat=-4.2427,lon=158.7892] [4  15' S 158  47' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-4.5000,lon=158.9695] [4  30' S 158  58' E]
              optimizedLocation=[GeoLocation lat=-4.5000,lon=158.9695] [4  30' S 158  58' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-5.2040,lon=159.5000] [5  12' S 159  30' E]
              optimizedLocation=[GeoLocation lat=-5.2040,lon=159.5000] [5  12' S 159  30' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-5.5000,lon=159.7231] [5  30' S 159  43' E]
              optimizedLocation=[GeoLocation lat=-5.5000,lon=159.7231] [5  30' S 159  43' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.1643,lon=160.3611] [6  10' S 160  22' E]
              optimizedLocation=[GeoLocation lat=-6.1643,lon=160.3611] [6  10' S 160  22' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.2053,lon=160.4020] [6  12' S 160  24' E]
              optimizedLocation=[GeoLocation lat=-6.2053,lon=160.4020] [6  12' S 160  24' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.3031,lon=160.5000] [6  18' S 160  30' E]
              optimizedLocation=[GeoLocation lat=-6.3031,lon=160.5000] [6  18' S 160  30' E]]]
