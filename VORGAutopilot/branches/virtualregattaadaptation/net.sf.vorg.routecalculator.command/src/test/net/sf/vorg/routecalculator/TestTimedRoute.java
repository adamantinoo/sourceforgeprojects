//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.models.TimedRoute;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestTimedRoute extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestTimedRoute() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testRouteCellTimePartitioning() {
		TimedRoute testRoute = new TimedRoute();
		testRoute.generateRoute(new GeoLocation(49, 05, -42, 26), 53, 61);
	}
}

// - UNUSED CODE ............................................................................................
