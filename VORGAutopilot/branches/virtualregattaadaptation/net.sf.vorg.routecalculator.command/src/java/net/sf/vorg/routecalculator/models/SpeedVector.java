//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class SpeedVector {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	location;

	private final int					heading;

	private final double			speed;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpeedVector(GeoLocation newLocation, int heading, double boatSpeed) {
		this.location = newLocation;
		this.heading = heading;
		this.speed = boatSpeed;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getLocation() {
		return location;
	}

	public int getHeading() {
		return heading;
	}

	public double getSpeed() {
		return speed;
	}

}

// - UNUSED CODE ............................................................................................
