package net.sf.vorg.core.enums;

public enum Quadrants {
	QUADRANT_I, QUADRANT_II, QUADRANT_III, QUADRANT_IV;
	public static Quadrants q4Angle(final double angle) {
		if (angle < 0.0) return q4Angle(angle + 360.0);
		if (angle > 360.0) return q4Angle(angle - 360.0);
		if (angle <= 90.0) return Quadrants.QUADRANT_I;
		if (angle <= 180.0) return Quadrants.QUADRANT_II;
		if (angle <= 270.0) return Quadrants.QUADRANT_III;
		return QUADRANT_IV;
	}
}
