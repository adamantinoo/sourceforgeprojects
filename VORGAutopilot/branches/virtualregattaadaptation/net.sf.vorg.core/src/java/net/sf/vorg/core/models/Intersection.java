//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.core.enums.Directions;

// - CLASS IMPLEMENTATION ...................................................................................
public class Intersection {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................
	protected GeoLocation	location;
	protected Directions	direction;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Intersection(final GeoLocation location, final Directions direction) {
		this.location = location;
		this.direction = direction;
	}

	public Directions getDirection() {
		return direction;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getLocation() {
		return location;
	}

	public void setDirection(final Directions direction) {
		this.direction = direction;
	}

	public void setLocation(final GeoLocation location) {
		this.location = location;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Intersection ");
		buffer.append("location=").append(location).append(",");
		buffer.append("direction=").append(direction).append("]");
		return buffer.toString();
	}

}
// - UNUSED CODE ............................................................................................
