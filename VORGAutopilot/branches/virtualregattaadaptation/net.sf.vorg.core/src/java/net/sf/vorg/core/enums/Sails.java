package net.sf.vorg.core.enums;

public enum Sails {
	JIB, SPI, JIBN2, GENOA, CZ, LSPI, HSPI;

	public static Sails minSail(Sails lowSail, Sails highSail) {
		int comparison = lowSail.compareTo(highSail);
		if (comparison < 0) return lowSail;
		if (comparison > 0) return highSail;
		return lowSail;
	}

	public static Sails decodeSail(String sailCode) {
		if (null == sailCode) return Sails.JIB;
		final int code = new Integer(sailCode).intValue();
		if (code == 1) return Sails.SPI;
		if (code == 2) return Sails.JIB;
		if (code == 4) return Sails.JIBN2;
		if (code == 8) return Sails.GENOA;
		if (code == 16) return Sails.CZ;
		if (code == 32) return Sails.LSPI;
		if (code == 64) return Sails.HSPI;
		return Sails.JIB;
	}

	public static int encodeSail(Sails sail) {
		if (sail == JIB) return 1;
		if (sail == SPI) return 2;
		if (sail == JIBN2) return 4;
		if (sail == GENOA) return 8;
		if (sail == CZ) return 16;
		if (sail == LSPI) return 32;
		if (sail == HSPI) return 64;
		return 1;
	}
}
