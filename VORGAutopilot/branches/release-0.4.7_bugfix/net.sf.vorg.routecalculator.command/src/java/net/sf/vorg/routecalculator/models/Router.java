//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.RouterType;
import net.sf.vorg.routecalculator.parsers.RouteParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class Router /* extends WindMapHandler */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger	= Logger.getLogger("net.sf.vorg.routecalculator");

	// - F I E L D - S E C T I O N ............................................................................
	private final RouterType	routerType;
	private GeoLocation				start;
	private GeoLocation				end;
	private Calendar					now;
	private final double			elapsed	= 0.0;
	private double						angle;
	private int								heading;
	private GeoLocation				currentLocation;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Router(RouterType type) {
		this.routerType = type;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Route generateRoute(GeoLocation start, GeoLocation end) {
		try {
			ExtendedRoute buildRoute = new ExtendedRoute();
			Route directRoute = buildRoute.calculateDirectRoute(start, end, 0);
			// - Before the report we have to update all the route with the new wind information.
			directRoute.adjustWindChanges();
			return directRoute;
		} catch (LocationNotInMap lnime) {
			lnime.printStackTrace();
		}
		return null;
	}

	// /**
	// * Calculate the route in the apparent direction and the best VMC route projecting on that same angle.
	// *
	// * @throws LocationNotInMap
	// */
	// public void evaluateCell(GeoLocation location, int apparent) throws LocationNotInMap {
	// int alpha;
	// if (null != end)
	// alpha = new Double(this.start.angleTo(end)).intValue();
	// else
	// alpha = apparent;
	// // start = location;
	// // - Direct route
	// now = GregorianCalendar.getInstance();
	// now.add(Calendar.HOUR, -2);
	// // loadWinds(location);
	// final WindCell startCell = this.cellWithPoint(location, now.getTime());
	//
	// // - Extend this line to calculate intersections with the cell.
	// double endLat = location.getLat() + 10.0 * Math.cos(Math.toRadians(alpha));
	// double endLon = location.getLon() + 10.0 * Math.sin(Math.toRadians(alpha));
	// GeoLocation endPoint = new GeoLocation(endLat, endLon);
	//
	// // - Buildup the route to the end from the start cell.
	// final Route directRoute = new Route();
	// Vector<Intersection> intersections = this.calculateIntersection(location, endPoint, startCell);
	// directRoute.add(startCell, intersections.lastElement().getDirection(), location,
	// intersections.lastElement()
	// .getLocation());
	// RouteCell routeElement = directRoute.getLast();
	// System.out.println("Adding cell to route: " + routeElement.toString());
	//
	// StringBuffer buffer = new StringBuffer("[Wind Cell evaluation").append('\n');
	// buffer.append("").append(routeElement.getCell()).append('\n');
	// buffer.append("Direct route").append('\n');
	// printRouteData(routeElement, buffer);
	//
	// // - Calculate now the VMC projection.
	// // - Get the VMC from this start point to the end of the cell.
	// ExtendedLocation extended = new ExtendedLocation(start.getLat(), start.getLon());
	// VMCData vmc = extended.getVMC(startCell, alpha);
	// int bestAngle = vmc.getBestAngle();
	//
	// // - Extend this line to calculate intersections with the cell.
	// endLat = start.getLat() + 10.0 * Math.cos(Math.toRadians(bestAngle));
	// endLon = start.getLon() + 10.0 * Math.sin(Math.toRadians(bestAngle));
	// endPoint = new GeoLocation(endLat, endLon);
	//
	// // - Buildup the route to the end from the start cell.
	// // directRoute = new Route();
	// intersections = this.calculateIntersection(start, endPoint, startCell);
	// directRoute.add(startCell, intersections.lastElement().getDirection(), location,
	// intersections.lastElement()
	// .getLocation());
	// routeElement = directRoute.getLast();
	//
	// buffer.append("VMC route").append('\n');
	// printRouteData(routeElement, buffer);
	// buffer.append("]\n]");
	// System.out.println(buffer.toString());
	// }

	private void printRouteData(RouteCell routeElement, StringBuffer buffer) {
		buffer.append("").append(routeElement.printStartReport()).append('\n');
		buffer.append("").append(routeElement.printReport(1)).append('\n');
	}

	/**
	 * Optimize a direct route from the start location to the end location. get the cells that have to be passed
	 * and the optimize the resulting route.
	 */
	public void optimizeRoute(GeoLocation startLocation, GeoLocation endLocation) {
		try {
			System.out.println("Starting calculation for Route. - " + GregorianCalendar.getInstance().getTime());
			System.out.println("Direct Path.");
			System.out.println("START\t" + startLocation.toReport());
			System.out.println("END\t" + endLocation.toReport());
			System.out.println();
			// this.loadWinds(startLocation);
			// this.loadWinds(endLocation);
			// System.out.println();
			// Route directRoute = this.selectDirectRoute(startLocation, endLocation);
			ExtendedRoute buildRoute = new ExtendedRoute();
			Route directRoute = buildRoute.calculateDirectRoute(startLocation, endLocation, 0);
			// - Before the report we have to update all the route with the new wind information.
			directRoute.adjustWindChanges();
			System.out.println("Direct Route");
			System.out.println(directRoute.printReport());
			System.out.println();

			System.out.println("Optimized Route");
			directRoute.optimizeRoute();
			System.out.println(directRoute.printReport());
		} catch (LocationNotInMap lnime) {
			lnime.printStackTrace();
		}
	}

	/** Read a route from a file and then optimize the resulting route to get the shortest way. */
	public void optimizeRoute(String routeDefinitionFile) {
		// - Parse the input route configuration file.
		ExtendedRoute directRoute = new ExtendedRoute();
		InputStream stream;
		try {
			stream = new BufferedInputStream(new FileInputStream(routeDefinitionFile));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final RouteParserHandler handler = new RouteParserHandler(this, directRoute);
			parser.parse(stream, handler);

			// - If we reached this point we have the new route configured
			System.out.println("Programmed Route");
			// - Before the report we have to update all the route with the new wind information.
			directRoute.adjustWindChanges();
			System.out.println(directRoute.printReport());
			System.out.println();
			// if (!RouteCalculator.onlyDirect()) {
			System.out.println("Optimized Route");
			directRoute.optimizeRoute();
			System.out.println(directRoute.printReport());
			// }
		} catch (final FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			// TODO Auto-generated catch block
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		} catch (LocationNotInMap lnime) {
			// TODO Auto-generated catch block
			lnime.printStackTrace();
		}
	}

	public void startElement(String name, Attributes attributes, ExtendedRoute route) throws SAXException {
		try {
			if (name.toLowerCase().equals("waypoint")) {
				GeoLocation waypoint = new GeoLocation();
				waypoint.setLat(attributes.getValue("latitude"));
				waypoint.setLon(attributes.getValue("longitude"));
				route.addWaypoint(waypoint);
			}
		} catch (final LocationNotInMap lnime) {
			throw new SAXException(lnime);

		}
	}
}

// - UNUSED CODE ............................................................................................
