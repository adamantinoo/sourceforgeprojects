//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.util.Formatter;
import java.util.Locale;

import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.vorgautopilot.core.WaypointTypes;

// - CLASS IMPLEMENTATION ...................................................................................
public class ActiveWaypoint extends Waypoint {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final long	serialVersionUID	= -4269757840312603475L;

	// - F I E L D - S E C T I O N ............................................................................
	private final Waypoint		originalReference;
	private double						estimatedCourse;
	private int								estimatedAWD;
	private double						selectedCourse;
	private int								selectedResultingAWD;
	private double						distance;
	private VMCData						vmc;
	private SailConfiguration	sails;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ActiveWaypoint(final Waypoint targetWaypoint) {
		super(targetWaypoint.getType());
		originalReference = targetWaypoint;

		//- Copy the parameter waypoint data to this internal structure
		boatParent = targetWaypoint.boatParent;
		name = targetWaypoint.getName();
		range = targetWaypoint.getRange();
		course = targetWaypoint.getCourse();
		minAWD = targetWaypoint.minAWD;
		maxAWD = targetWaypoint.maxAWD;
		state = targetWaypoint.getState();
		selectedAWD = targetWaypoint.getSelectedAWD();
		setLocation(targetWaypoint.getLocation());

		activeWaypoint = this;
		firePropertyChange(Waypoint.WAYPOINT_ACTIVATED, null, "ACTIVE");
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Checks the state of this point with respect to the boat location or the current time or the list of
	 * walked points.
	 */
	@Override
	public boolean checkSurpassed(final GeoLocation boatLocation) {
		//		//- Check the time point
		//		if (null != endTimePoint) {
		//			final Calendar now = Calendar.getInstance();
		//			endTimePoint.before(now);
		//			setState(WaypointStates.SURPASSED);
		//			return true;
		//		}
		// - Calculate distance to waypoint. Discard it if the distance is less than control.
		final GeoLocation waypointLocation = getLocation();
		final double distance = waypointLocation.distance(boatLocation);
		if (distance < getRange()) {
			System.out.println("--- Discarded waypoint " + waypointLocation + ". Distance " + distance + " below "
					+ getRange() + " miles.");
			setState(WaypointStates.SURPASSED);
			return true;
		}
		//- Calculate the angle form the boat to the point. If crossed the perpendicular then the point is out
		double referenceAngle = getCourse();
		if (type == WaypointTypes.POINT) referenceAngle = boatParent.getHeading();
		if (type == WaypointTypes.VMGLOX) referenceAngle = boatParent.getHeading();
		if (type == WaypointTypes.AWD) referenceAngle = boatParent.getHeading();
		if (type == WaypointTypes.MAX) referenceAngle = boatParent.getHeading();
		//		if (referenceAngle >= 0.0) {
		final double boatAngle = GeoLocation.adjustAngleTo360(boatLocation.angleTo(getLocation()) - referenceAngle);
		if ((boatAngle >= 90.0) && (boatAngle <= 270.0)) {
			System.out.println("--- Discarded waypoint " + getLocation() + ". Crossed line with angle " + boatAngle);
			setState(WaypointStates.SURPASSED);
			return true;
		}
		return false;
	}

	public void deactivate() {
		originalReference.setState(WaypointStates.NOACTIVE);
		firePropertyChange(Waypoint.WAYPOINT_DEACTIVATED, null, this);
	}

	public GeoLocation getBoatLocation() {
		return boatParent.getLocation();
	}

	public double getDistance() {
		return distance;
	}

	public int getEstimatedAWD() {
		return estimatedAWD;
	}

	public double getEstimatedCourse() {
		return estimatedCourse;
	}

	public String getPower() {
		if (null == vmc)
			return boatParent.getPower();
		else {
			final int maxAWD = vmc.getMaxAWD();
			SailConfiguration configuration = Polars.lookup(maxAWD, getWindSpeed());
			final double maxSpeed = configuration.getSpeed();
			configuration = Polars.lookup(getSelectedResultingAWD(), getWindSpeed());
			return FormatSingletons.nf2.format(configuration.getSpeed() * 100.0 / maxSpeed) + "%";
		}
	}

	public SailConfiguration getSails() {
		return sails;
	}

	public double getSelectedCourse() {
		return selectedCourse;
	}

	public int getSelectedResultingAWD() {
		return selectedResultingAWD;
	}

	public void setDistance(final double newDistance) {
		distance = newDistance;
	}

	public void setEstimatedCourse(final double angle) {
		estimatedCourse = angle;
		estimatedAWD = GeoLocation.calculateAWD(getWindDir(), angle);
	}

	public void setSelectedCourse(final double newHeading) {
		selectedCourse = newHeading;
		selectedResultingAWD = GeoLocation.calculateAWD(getWindDir(), newHeading);
	}

	public void setVMC(final VMCData newVMC) {
		vmc = newVMC;
	}

	protected void sendCommand(final Boat boat, final double newHeading) {
		final Formatter formatter = new Formatter(Locale.ENGLISH);
		//- Get the AWD and polars that path this angle.
		// - Round the angle to get the AWD. This can change the result.
		final double apparentHeading = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), Math.round(newHeading));
		sails = Polars.lookup(new Double(apparentHeading).intValue(), getCurrentWindCell().getWindSpeed());
		//		System.out.println();
		if (apparentHeading < 0.0)
			formatter.format("[Adjusted angle=%1$03d - AWD=%2$03d Port]", Math.round(newHeading), Math.round(Math
					.abs(apparentHeading)));
		else formatter.format("[Adjusted angle=%1$03d - AWD=%2$03d Starboard]", Math.round(newHeading), Math.round(Math
				.abs(apparentHeading)));
		//	System.out.println("[Selected angle= " + FormatSingletons.nf3.format(newHeading) + " - AWD= "
		//				+ Math.round(apparentHeading) + "]");
		System.out.println(formatter);
		System.out.println(sails);
		System.out.println();
		final BoatCommand newBoatCommand = new BoatCommand();
		newBoatCommand.setHeading(boat.getHeading(), new Double(Math.round(newHeading)).intValue());
		newBoatCommand.setSails(sails);
		newBoatCommand.sendCommand(boat);
		firePropertyChange(Waypoint.WAYPOINT_EXECUTED, null, this);
	}
}

// - UNUSED CODE ............................................................................................
