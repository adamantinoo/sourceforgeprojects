//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ITreeFacet.java 190 2009-03-10 16:45:12Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-10 17:45:12 +0100 (mar, 10 mar 2009) $
//  RELEASE:        $Revision: 190 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This interface defines the methods to be declared on any element that is to be shown inside a tree display
 * view. The API declared is for the columns declared ion the tree.
 */
public interface ITreeFacet {

	// - F I E L D - S E C T I O N ............................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	//	public ITreeFacet getTreeFacet();
}

// - UNUSED CODE ............................................................................................
