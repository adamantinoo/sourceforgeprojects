//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface VORGConstants {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final double	TOMINUTES							= 60.0;
	public static final double	EARTHRADIUS						= 3437.74;
	public static final String	VERSION								= "Version Beta 0.4.4b 10/04";
	public static final int			NOCONFIG							= 10;
	public static final int			INVALIDCONFIGURATION	= 11;
	public static final int			GENERICERROR					= 12;
	public static final double	DIFFERENTIAL					= 0.0001;
	public static final String	NEWLINE								= System.getProperty("line.separator");
}

// - UNUSED CODE ............................................................................................
