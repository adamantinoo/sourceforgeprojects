//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: WindCell.java 307 2009-05-08 11:03:44Z boneymen $
//  LAST UPDATE:    $Date: 2009-05-08 13:03:44 +0200 (vie, 08 may 2009) $
//  RELEASE:        $Revision: 307 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.enums.Quadrants;
import net.sf.vorg.core.exceptions.LocationNotInMap;

// - CLASS IMPLEMENTATION ...................................................................................
public class WindCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	location;
	private int								direction	= 0;
	private double						speed			= 0.0;
	/** Time reference for this cell to be able to locate cell ahead on the future. */
	private Date							timeStamp;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/** Create a new cell from the latitude and longitude points. */
	public WindCell(final int latitude, final int longitude, final int windDirection, final double windSpeed) {
		location = new GeoLocation(latitude, 0, longitude, 0);
		direction = windDirection;
		speed = windSpeed;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Return a list of the intersections with the cell boundaries of a line that connect the two reference
	 * points. The intersections are calculated by testing the crossing of the four wall lines with this two
	 * point line and sensing which of this crossings lie inside the cell.
	 */
	public Vector<Intersection> calculateIntersection(final GeoLocation start, final GeoLocation end)
			throws LocationNotInMap {
		final Vector<Intersection> intersections = new Vector<Intersection>(4);
		// - Calculate a new latitude using the loxodromic correction
		double deltaLat = end.getLat() - start.getLat();
		double intermediateLat = start.getLat() + deltaLat / 2.0;
		double deltaLatCorrected = deltaLat * Math.cos(Math.toRadians(intermediateLat));
		double deltaLon = end.getLon() - start.getLon();
		// - Control the change over the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon = 360.0 - Math.abs(deltaLon);
		double course = start.angleTo(end);
		final Quadrants quad = Quadrants.q4Angle(course);

		// - Calculate cell limits.
		final Boundaries boundaries = getBoundaries();
		double lat;
		double lon;
		GeoLocation intersection;
		if (Quadrants.QUADRANT_I == quad) {
			// - Solve the line intersections with the direct line.
			// - Check the crossing of the date line.
			double boundWE = boundaries.getWest() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			// - Check the crossing of the date line.
			boundWE = boundaries.getEast() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.E));
		}
		if (Quadrants.QUADRANT_II == quad) {
			// - Solve the line intersections with the direct line.
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			// - Check the crossing of the date line.
			double boundWE = boundaries.getWest() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			// - Check the crossing of the date line.
			boundWE = boundaries.getEast() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.E));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.S));
		}
		if (Quadrants.QUADRANT_III == quad) {
			// - Solve the line intersections with the direct line.
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			// - Check the crossing of the date line.
			double boundWE = boundaries.getEast() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.E));
			// - Check the crossing of the date line.
			boundWE = boundaries.getWest() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.S));
		}
		if (Quadrants.QUADRANT_IV == quad) {
			// - Solve the line intersections with the direct line.
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			// - Check the crossing of the date line.
			double boundWE = boundaries.getEast() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.E));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			// - Check the crossing of the date line.
			boundWE = boundaries.getWest() - start.getLon();
			if (Math.abs(boundWE) > 180.0) boundWE -= 360.0 * Math.signum(boundWE);
			lat = (deltaLat / deltaLon) * boundWE + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (doesContain(intersection)) intersections.add(new Intersection(intersection, Directions.W));
		}
		return intersections;
	}

	/**
	 * Check if this cell contains the parameter location. This test is done checking if the location has a
	 * latitude between the cell latitudes and the longitude is also between the cell longitudes.
	 */
	public boolean doesContain(final GeoLocation target) {
		// - BUG detected when the date crossing is near the location.
		if (Math.floor(target.getLon()) == 180.0) target.setLon(target.getLon() * -1.0);
		boolean contained = false;
		// - Calculate min and max latitudes for this cell.
		final double minLat = location.getLat() - 0.5;
		final double maxLat = location.getLat() + .5;
		if ((minLat <= target.getLat()) && (maxLat >= target.getLat())) {
			// - Calculate min and max longitudes to perform the same test.
			double minLon = location.getLon() - .5;
			// - BUG Related to the crossing of the date line. Adapt to positives.
			if (minLon < 0) minLon += 360.0;
			double maxLon = location.getLon() + .5;
			if (maxLon < 0) maxLon += 360.0;
			double targetLon = target.getLon();
			//BUG Detect the case of a 0 crossing in longitude
			if (minLon > maxLon)
				minLon -= 360.0;
			else if (targetLon < 0) targetLon += 360.0;
			if ((minLon <= targetLon) && (maxLon >= targetLon)) contained = true;
		}
		return contained;
	}

	public Boundaries getBoundaries() {
		final Boundaries limits = new Boundaries();
		limits.setNorth(location.getLat() + .5);
		limits.setSouth(location.getLat() - .5);
		limits.setWest(location.getLon() - .5);
		limits.setEast(location.getLon() + .5);
		return limits;
	}

	// public boolean contains(final GeoLocation intersection) {
	// return this.doesContain(intersection);
	// }

	/** Return the south and north limits for this cell. */
	public Limits getCeiling() {
		return new Limits(Limits.NORTH_SOUTH, location.getLat() - .5, location.getLat() + .5);
	}

	public GeoLocation getLocation() {
		return location;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	/** Return the west and east limits for this cell. */
	public Limits getWalls() {
		return new Limits(Limits.EAST_WEST, location.getLon() - .5, location.getLon() + .5);
	}

	public int getWindDir() {
		return direction;
	}

	public double getWindSpeed() {
		return speed;
	}

	public boolean isEquivalent(final WindCell target) {
		return location.isEquivalent(target.location);
	}

	public void timeStamp(final Date time) {
		timeStamp = time;
	}

	/** Returns the location of a cell located at the indicated direction of this cell. */
	public GeoLocation cellAtDirection(Directions direction) {
		if (direction == Directions.N) return new GeoLocation(location.getLat() + 1.0, location.getLon());
		if (direction == Directions.E) return new GeoLocation(location.getLat(), location.getLon() + 1.0);
		if (direction == Directions.S) return new GeoLocation(location.getLat() - 1.0, location.getLon());
		if (direction == Directions.W) return new GeoLocation(location.getLat(), location.getLon() - 1.0);
		return location;
	}

	@Override
	public String toString() {
		final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(3);
		nf.setMinimumFractionDigits(3);
		final StringBuffer buffer = new StringBuffer("[WindCell ");
		buffer.append("direction=").append(direction).append(",");
		buffer.append("speed=").append(nf.format(speed)).append("");
		buffer.append("\n          location=").append(location).append("");
		buffer.append("\n          timeStamp=").append(getTimeStamp()).append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
