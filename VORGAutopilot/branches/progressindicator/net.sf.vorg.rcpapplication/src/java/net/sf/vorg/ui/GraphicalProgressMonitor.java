//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.ui;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;

import net.sf.vorg.vorgautopilot.core.IVORGProgressMonitor;

// - CLASS IMPLEMENTATION ...................................................................................
public class GraphicalProgressMonitor implements IVORGProgressMonitor {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger	= Logger.getLogger("net.sf.vorg.ui");

	// - F I E L D - S E C T I O N ............................................................................
	private final IProgressMonitor	progressMonitor;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GraphicalProgressMonitor(IProgressMonitor monitor) {
		progressMonitor = monitor;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void beginTask(String name, int totalWork) {
		progressMonitor.beginTask(name, totalWork);
	}

	public void done() {
		progressMonitor.done();
	}

	public void internalWorked(double work) {
		progressMonitor.internalWorked(work);
	}

	public boolean isCanceled() {
		return progressMonitor.isCanceled();
	}

	public void setCanceled(boolean value) {
		progressMonitor.setCanceled(value);
	}

	public void setTaskName(String name) {
		progressMonitor.setTaskName(name);
	}

	public void subTask(String name) {
		progressMonitor.subTask(name);
	}

	public void worked(int work) {
		progressMonitor.worked(work);
	}
}

// - UNUSED CODE ............................................................................................
