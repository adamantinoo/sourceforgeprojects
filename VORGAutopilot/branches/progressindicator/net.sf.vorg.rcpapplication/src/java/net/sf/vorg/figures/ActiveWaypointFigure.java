//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.Disposable;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;

import net.sf.gef.core.draw2d.MultiLabelLine;
import net.sf.gef.core.draw2d.RoundedGroup;
import net.sf.gef.core.figures.ISelectableFigure;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.core.WaypointTypes;
import net.sf.vorg.vorgautopilot.models.ActiveWaypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class ActiveWaypointFigure extends RoundedGroup implements IDetailedFigure, ISelectableFigure, Disposable {
	class SingleColumnFigure extends Figure {
		public SingleColumnFigure() {
			final GridLayout grid = new GridLayout();
			grid.horizontalSpacing = 0;
			grid.verticalSpacing = 0;
			grid.marginHeight = 0;
			grid.marginWidth = 0;
			grid.numColumns = 1;
			setLayoutManager(grid);
		}
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int										selected						= EditPart.SELECTED_NONE;
	private final ActiveWaypoint	waypointModel;
	private final Figure					mainContent					= new Figure();
	private final Figure					content							= new SingleColumnFigure();
	private Figure								compressedContainer	= new Figure();
	//	private final Figure					standardContainer		= new Figure();
	private Figure								extendedContainer		= new Figure();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ActiveWaypointFigure(final ActiveWaypoint model) {
		super(8);
		waypointModel = model;
		setOpaque(true);
		groupName.setFont(PilotUIConstants.FONT_GROUPNAME);
		groupName.setForegroundColor(PilotUIConstants.COLOR_GROUPNAME);

		//- Set the object's graphical icon. This uses the same common code for tree views.
		groupName.setIcon(ImageFactory.getImage(ImageKeys.ACTIVEWAYPOINT_SMALL));
		groupName.setIconTextGap(3);
		setText("Active Waypoint");
		addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(final MouseEvent me) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(final MouseEvent me) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(final MouseEvent me) {
				ActiveWaypointFigure.this.setSelected(EditPart.SELECTED_NONE);
				ActiveWaypointFigure.this.refreshContents();
			}

			@Override
			public void mouseHover(final MouseEvent me) {
				ActiveWaypointFigure.this.setSelected(EditPart.SELECTED_PRIMARY);
				ActiveWaypointFigure.this.refreshContents();
			}

			@Override
			public void mouseMoved(final MouseEvent me) {
				// TODO Auto-generated method stub

			}
		});
		final GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;

		//- Set the layout and contents of the main area
		setLayout();
		setIcon();
		mainContent.add(content, gridData);
		this.add(mainContent);

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return selected;
	}

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == getSelected()) || (EditPart.SELECTED == getSelected()))
			return true;
		else
			return false;
	}

	public void refreshContents() {
		content.removeAll();
		addCompressedContents();
		addStandardContents();
		if (isSelected()) {
			addExtendedContants();
		}
		updateBackgroundColor();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(final int value) {
		selected = value;
	}

	protected void addStandardContents() {
	}

	private void addCompressedContents() {
		compressedContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 2;
		compressedContainer.setLayoutManager(grid);

		//- Compose the first line with the name and the type
		final MultiLabelLine name = new MultiLabelLine();
		name.setDefaultFont("Arial", 12);
		if (null == waypointModel.getName()) {
			name.addColumn("<>", SWT.NORMAL, PilotUIConstants.COLOR_NAMES);
		} else {
			name.addColumn(waypointModel.getName(), SWT.NORMAL, PilotUIConstants.COLOR_NAMES);
		}
		final MultiLabelLine typeLabel = new MultiLabelLine();
		typeLabel.setDefaultFont("Arial", 12);
		typeLabel.addColumn("    Type:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		typeLabel.addColumn(waypointModel.getType().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//- Compose the second line with the selected course data
		final MultiLabelLine selectedCourse = new MultiLabelLine();
		selectedCourse.setDefaultFont("Tahoma", 11);
		selectedCourse.addColumn("Selected Course:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		selectedCourse.addColumn(new Double(waypointModel.getSelectedCourse()).intValue() + "�", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		selectedCourse.addColumn("   Selected AWD:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		//- Get the sign of the AWD by comparing it to the current course.
		int course = new Double(waypointModel.getSelectedCourse()).intValue();
		int data = GeoLocation.angleDifference(waypointModel.getWindDir(), course);
		if (data > 0) {
			selectedCourse.addColumn(new Integer(waypointModel.getSelectedResultingAWD()).toString() + "� Starboard",
					SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		} else {
			selectedCourse.addColumn(new Integer(waypointModel.getSelectedResultingAWD()).toString() + "� Port", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}

		final MultiLabelLine powerResult = new MultiLabelLine();
		powerResult.setDefaultFont("Tahoma", 11);
		powerResult.addColumn("   Power:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		powerResult.addColumn(waypointModel.getPower(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		compressedContainer.add(name);
		compressedContainer.add(typeLabel);
		compressedContainer.add(selectedCourse);
		compressedContainer.add(powerResult);
		content.add(compressedContainer);
	}

	private void addExtendedContants() {
		extendedContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		extendedContainer.setLayoutManager(grid);

		//- Compose the second line with the waypoint characteristics.
		final MultiLabelLine waypointData = new MultiLabelLine();
		waypointData.setDefaultFont("Tahoma", 10);
		waypointData.addColumn("AWD Limits: ", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		waypointData.addColumn(waypointModel.getMinAWD() + "/" + waypointModel.getMaxAWD(), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		if (waypointModel.getType() == WaypointTypes.ANGLE) {
			waypointData.addColumn("    Angle: ", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			waypointData.addColumn(new Double(waypointModel.getCourse()).toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.AWD) {
			waypointData.addColumn("    Fixed AWD: ", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			waypointData.addColumn(FormatSingletons.nf1.format(waypointModel.getSelectedAWD()).toString() + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.VMG) {
			waypointData.addColumn("    Angle: ", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			waypointData.addColumn(FormatSingletons.nf1.format(waypointModel.getCourse()).toString() + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		waypointData.addColumn("    Waypoint Range:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		waypointData.addColumn(FormatSingletons.nf1.format(waypointModel.getRange()).toString() + " NM", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		//- Compose the third line with the waypoint location.
		final MultiLabelLine waypointLocation = new MultiLabelLine();
		waypointLocation.setDefaultFont("Tahoma", 10);
		waypointLocation.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		waypointLocation.addColumn(waypointModel.getLocation().toReport().replace('\t', '-'), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		waypointLocation.addColumn("   Distance:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		waypointLocation.addColumn(FormatSingletons.nf3.format(waypointModel.getDistance()) + " NM", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		waypointLocation.addColumn("   Wpt Angle:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		waypointLocation.addColumn(FormatSingletons.nf1.format(waypointModel.getBoatLocation().angleTo(
				waypointModel.getLocation()))
				+ "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//- Compose the second line with the power information
		final MultiLabelLine powerInfo = new MultiLabelLine();
		powerInfo.setDefaultFont("Tahoma", 10);
		powerInfo.addColumn("Selected Sail:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		powerInfo.addColumn(waypointModel.getSails().getSail().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		powerInfo.addColumn("   Result Speed:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		powerInfo.addColumn(FormatSingletons.nf2.format(waypointModel.getSails().getSpeed()) + " knts", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		powerInfo.addColumn("   Power:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		powerInfo.addColumn(waypointModel.getPower(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//- Compose the third line with the estimated course data
		final MultiLabelLine estimatedCourse = new MultiLabelLine();
		estimatedCourse.setDefaultFont("Tahoma", 10);
		estimatedCourse.addColumn("Estimated Course:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		estimatedCourse.addColumn(FormatSingletons.nf1.format(waypointModel.getEstimatedCourse()).toString() + "�",
				SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		estimatedCourse.addColumn("   Estimated AWD:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		//- Get the sign of the AWD by comparing it to the current course.
		int course = new Double(waypointModel.getEstimatedCourse()).intValue();
		int data = GeoLocation.angleDifference(waypointModel.getWindDir(), course);
		if (data > 0) {
			estimatedCourse.addColumn(new Double(waypointModel.getEstimatedAWD()).toString() + "� Starboard", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		} else {
			estimatedCourse.addColumn(new Double(waypointModel.getEstimatedAWD()).toString() + "� Port", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}

		extendedContainer.add(waypointData);
		extendedContainer.add(waypointLocation);
		extendedContainer.add(powerInfo);
		extendedContainer.add(estimatedCourse);
		content.add(extendedContainer);
	}

	private void setIcon() {
		final Label iconCell = new Label(ImageFactory.getImage(ImageKeys.ACTIVEWAYPOINT_BIG));
		iconCell.setFont(PilotUIConstants.FONT_DEFAULT);
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(3);
		final GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.BEGINNING;
		//				gridData.grabExcessVerticalSpace = true;
		mainContent.add(iconCell, gridData);
	}

	private void setLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 2;
		mainContent.setLayoutManager(grid);
	}

	private void updateBackgroundColor() {
		//		if (waypointModel.getType() == WaypointTypes.NOACTION) {
		//			groupName.setBackgroundColor(PilotUIConstants.COLOR_GRAY);
		//		}
		//		if (waypointModel.getState() == WaypointStates.ACTIVE) {
		setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_ACTIVE);
		content.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_ACTIVE);
		//	}
		//		if (waypointModel.getState() == WaypointStates.NOACTIVE) {
		//			groupName.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_NOACTION);
		//		}
		//		if (waypointModel.getState() == WaypointStates.ONHOLD) {
		//			groupName.setBackgroundColor(PilotUIConstants.COLOR_LIGHTEST_GRAY);
		//		}
		//		if (waypointModel.getState() == WaypointStates.SURPASSED) {
		//			groupName.setBackgroundColor(PilotUIConstants.COLOR_DARK_GRAY);
		//		}
	}

	@Override
	public void dispose() {
		if (mainContent instanceof Disposable) {
			((Disposable) mainContent).dispose();
		}
		if (content instanceof Disposable) {
			((Disposable) content).dispose();
		}
		if (compressedContainer instanceof Disposable) {
			((Disposable) compressedContainer).dispose();
		}
		if (extendedContainer instanceof Disposable) {
			((Disposable) extendedContainer).dispose();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		dispose();
		super.finalize();
	}
}
// - UNUSED CODE ............................................................................................
