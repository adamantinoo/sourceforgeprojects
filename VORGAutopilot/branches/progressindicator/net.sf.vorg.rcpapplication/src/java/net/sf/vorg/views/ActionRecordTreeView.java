//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: GraphicalDetailedView.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.IProgressService;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.gef.core.views.BaseTreeContentProvider;
import net.sf.vorg.app.Activator;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.models.UIPilotModelStore;
import net.sf.vorg.views.facets.AbstractImageAccesor;
import net.sf.vorg.views.facets.AbstractTextAccesor;
import net.sf.vorg.views.facets.TreeColumnConfigurator;
import net.sf.vorg.vorgautopilot.models.ActionRecord;
import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.BoatActionList;
import net.sf.vorg.vorgautopilot.models.ITreeFacet;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

//class ActionRecordTextAccesor extends AbstractTextAccesor{
//	
//}
// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class implements a Multicolumn Tree View that gets the data from some part of the model for
 * presentation. Initial versions will not add filter functionalities but newer version should add a tool menu
 * to control de detail level of the presentation and the type and number of records to present.<br>
 * Implementation will refactor any code to make this class as configurable as possible by the creation of a
 * metamodel structure to control the translation of the model to the presentation using the Facet pattern..
 * <p>
 */
public class ActionRecordTreeView extends ViewPart implements PropertyChangeListener, ISelectionChangedListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String										ID											= "net.sf.vorg.views.ActionRecordTreeView.id";
	public static final String										TREEVIEWER_ID						= ActionRecordTreeView.ID
																																						+ "SelectionTreeProvider";
	private static Logger													logger									= Logger.getLogger("net.sf.vorg.views");
	private static Vector<TreeColumnConfigurator>	treeColumnsConfigurator	= new Vector<TreeColumnConfigurator>();
	static {
		//- DATETIME Column 1.
		ActionRecordTreeView.logger.info("Adding 'DATETIME' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.DATETIME_COLUMN,
				"Boat/Date", "The boat name or the reference Data/Time if the Boat is expanded.", 155,
				new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						//						return facet.getText(facet,ActionRecord.DATETIME_COLUMN);
						if (facet instanceof BoatActionList)
							return ((BoatActionList) facet).getBoatName();
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getTimeStamp();
						return super.getText(facet);
					}
				}, new AbstractImageAccesor() {
					@Override
					public Image getImage(final ITreeFacet facet) {
						if (facet instanceof BoatActionList)
							return ImageFactory.getImage(ImageKeys.READY_BOAT);
						if (facet instanceof ActionRecord)
							return ImageFactory.getImage(((ActionRecord) facet).getImageName());
						return ImageFactory.getImage(null);
					}
				}));
		//- BOATNAME Column 2.
		ActionRecordTreeView.logger.info("Adding 'BOATNAME' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.BOAT_COLUMN, "Boat name",
				"Boat name as declared in the game", 80, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTNOOK))
								return ((ActionRecord) facet).getErrorMessage();
							else
								return ((ActionRecord) facet).getBoatName();
						return super.getText(facet);
					}
				}));
		//- LATITUDE Column 3.
		ActionRecordTreeView.logger.info("Adding 'LATITUDE' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LATITUDE_COLUMN, "Lat",
				"Latitude location of the event", 80, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getEventLatitude();
						return super.getText(facet);
					}
				}));
		//- LONGITUDE Column 4.
		ActionRecordTreeView.logger.info("Adding 'LONGITUDE' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "Lon",
				"Longitude location of the event", 80, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getEventLongitude();
						return super.getText(facet);
					}
				}));
		//- HEADING Column 4.
		ActionRecordTreeView.logger.info("Adding 'HEADING' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN,
				"Heading", "Boat heading course", 45, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getHeading();
						return super.getText(facet);
					}
				}));
		//- SPEED Column 5.
		ActionRecordTreeView.logger.info("Adding 'SPEED' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "Speed",
				"Boat current speed", 60, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getSpeed();
						return super.getText(facet);
					}
				}));
		//- SAIL Column 6.
		ActionRecordTreeView.logger.info("Adding 'SAIL' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "Sail",
				"Boat current sail", 60, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getSail();
						return super.getText(facet);
					}
				}));
		//- AWD Column 7.
		ActionRecordTreeView.logger.info("Adding 'AWD' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "AWD",
				"Boat current Apparent Wind Direction", 50, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getAwd();
						return super.getText(facet);
					}
				}));
		//- DISTANCERUN Column 8.
		ActionRecordTreeView.logger.info("Adding 'DISTANCERUN' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "Run",
				"Distance run by the boat since last update", 50, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getDistanceRun();
						return super.getText(facet);
					}
				}));
		//- RANKING Column 9.
		ActionRecordTreeView.logger.info("Adding 'RANKING' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN,
				"Ranking", "Boat position in game ranking", 50, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getRanking();
						return super.getText(facet);
					}
				}));
		//- WINDIR Column 10.
		ActionRecordTreeView.logger.info("Adding 'WINDIR' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "WDir",
				"Current cell wind direction", 45, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getWindDirection();
						return super.getText(facet);
					}
				}));
		//- WINSPEED Column 11.
		ActionRecordTreeView.logger.info("Adding 'WINSPEED' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN,
				"Wind Spd", "Current cell wind SPEED", 60, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord)
							return ((ActionRecord) facet).getWindSpeed();
						return super.getText(facet);
					}
				}));
		//- NEWHEADING Column 12.
		ActionRecordTreeView.logger.info("Adding 'NEWHEADING' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "New H",
				"Command new heading for boat", 50, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord) {
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTOK))
								return ((ActionRecord) facet).getNewHeading();
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTNOOK))
								return ((ActionRecord) facet).getNewHeading();
						}
						return super.getText(facet);
					}
				}));
		//- NEWSPEED Column 13.
		ActionRecordTreeView.logger.info("Adding 'NEWSPEED' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN, "New S",
				"Command new speed for boat", 60, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord) {
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTOK))
								return ((ActionRecord) facet).getNewSpeed();
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTNOOK))
								return ((ActionRecord) facet).getNewSpeed();
						}
						return super.getText(facet);
					}
				}));
		//- NEWAWD Column 14.
		ActionRecordTreeView.logger.info("Adding 'NEWAWD' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN,
				"New AWD", "New AWD for the boat", 50, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord) {
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTOK))
								return ((ActionRecord) facet).getNewAWD();
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTNOOK))
								return ((ActionRecord) facet).getNewAWD();
						}
						return super.getText(facet);
					}
				}));
		//- NEWSAIL Column 15.
		ActionRecordTreeView.logger.info("Adding 'NEWSAIL' configuration to Tree View columns.");
		ActionRecordTreeView.treeColumnsConfigurator.add(new TreeColumnConfigurator(ActionRecord.LONGITUDE_COLUMN,
				"New Sail", "Command new sail for boat", 60, new AbstractTextAccesor() {
					@Override
					public String getText(final ITreeFacet facet) {
						if (facet instanceof ActionRecord) {
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTOK))
								return ((ActionRecord) facet).getNewSail();
							if (((ActionRecord) facet).getType().equals(Boat.BOAT_COMMANDSENTNOOK))
								return ((ActionRecord) facet).getNewSail();
						}
						return super.getText(facet);
					}
				}));
	}

	public static TreeColumnConfigurator getColumnConfiguration(final int index) {
		return ActionRecordTreeView.treeColumnsConfigurator.get(index);
	}

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Table structure to present the contents. Those contents are generated by the
	 * <code>ViewTreeContentProvider</code>.
	 */
	private TreeViewer									viewer;
	//	/** Menu actions that are available on this viewer. */
	//	private Action											action1;
	//	private Action										action2;
	//	private Action										doubleClickAction;
	private PilotModelStore							modelServer;

	/**
	 * This class provides the data contents for the viewer. This is installed in a MDC pattern where this is
	 * the Model part. This class is feeded from the editor selection where all model information is extracted
	 * and stored for access from the Controller that is implemented by the <code>TableViewer</code>.
	 */
	private ActionRecordContentProvider	contentProvider;
	/**
	 * This class is called to draw each cell contents. It will receive small pieces of the model and is
	 * responsible to map the columns to the different attributes of the model.
	 */
	private ViewTreeLabelProvider				labelProvider;

	//	/**
	//	 * Stores the viewer column elements. It is only required it we like to change the column properties after
	//	 * their creation.
	//	 */
	//	private final Vector<TreeColumn>					columns									= new Vector<TreeColumn>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ActionRecordTreeView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(ActionRecordTreeView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is a callback that will allow us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		contentProvider = new ActionRecordContentProvider(modelServer);
		labelProvider = new ViewTreeLabelProvider();

		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(labelProvider);
		//- Configure the viewer columns before populating the tree
		configureTreeColumns(viewer.getTree());
		viewer.setInput(getViewSite());
		viewer.addSelectionChangedListener(this);

		//- Configure the view menus and actions.
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();

		//- Register this as a selection provider.
		Activator.addReference(BoatTreeView.TREEVIEWER_ID, viewer);
	}

	/**
	 * This is the entry point to detect any change on the state of the model and update the menu appareance and
	 * the model data with respect to the new model state.
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		// - Update the model when we have finished with the addition and processing of the turn data.
		if (PilotModelStore.RECORD_CHANGED.equals(prop)) {
			viewer.refresh();
		}
	}

	private void makeActions() {
	}

	/**
	 * This event is fired any time the selection is modified. <br>
	 * The parameters received are the new selection list on the <code>selection</code> parameter and the
	 * <code>EditorPart</code> that points to the originating <i>Editor</i> on the <code>part</code> parameter.<br>
	 * <br>
	 * From this selection I may extract the selectable elements (the ones that are going to move to the
	 * selection views) and the model information for each of the selected items. Filtering must be
	 * parameterizable thought the user interface.<br>
	 * <br>
	 * TODO This release only supports the selection of part elements up to the level of
	 * <code>AbstractGenericEditPart</code>. When routes or other object appear on the Map it has to be changed
	 * to support them.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		//- Scan the selection for PilotBoat instances to change the menus.
		ISelection selection = event.getSelection();
		if ((!selection.isEmpty()) && (selection instanceof TreeSelection)) {
			//TODO Apply all the filters that are defined on the list.
			//TODO Filter the EditParts that match the proper interface
			//			final StructuredSelection sel = (StructuredSelection) selection;
			final Iterator<Object> sit = ((IStructuredSelection) selection).iterator();
			//			final Vector<ISelectablePart> models = new Vector<ISelectablePart>();
			while (sit.hasNext()) {
				final Object selected = sit.next();
				//- Check that the selected object has the right interface before trying to get its model.
				if (selected instanceof PilotBoat) {
					//TODO Implement the code to perform the operations
				}
			}
		}
		getViewSite().getActionBars().getToolBarManager().update(true);
	}

	private void hookContextMenu() {
		final MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(final IMenuManager manager) {
				fillContextMenu(manager);
			}
		});
		final Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void hookDoubleClickAction() {
		//		viewer.addDoubleClickListener(new IDoubleClickListener() {
		//			public void doubleClick(final DoubleClickEvent event) {
		//				doubleClickAction.run();
		//			}
		//		});
	}

	private void contributeToActionBars() {
		final IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillContextMenu(final IMenuManager manager) {
		//		manager.add(newBoatCard);
		//		manager.add(deleteBoatCard);
		//		manager.add(runAutopilot);
		//		//- Other plug-ins can contribute their actions here
		//		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalPullDown(final IMenuManager manager) {
		//		manager.add(newBoatCard);
		//		manager.add(deleteBoatCard);
		//		manager.add(runAutopilot);
	}

	private void fillLocalToolBar(final IToolBarManager manager) {
		//		manager.add(newBoatCard);
		//		manager.add(deleteBoatCard);
		//		manager.add(runAutopilot);
	}

	@Override
	public void dispose() {
		// - Remove the view listening form the model.
		modelServer.removePropertyChangeListener(this);
		super.dispose();
	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		super.init(site);

		//- Connect this view with the content provider that contains the singleton model.
		modelServer = (UIPilotModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		if (null == modelServer) {
			//- Create the Progress Indicator where to run the model processing
			IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
			try {
				progressService.busyCursorWhile(new IRunnableWithProgress() {
					public void run(IProgressMonitor monitor) {
						//- Create and initialize a new model.
						modelServer = new UIPilotModelStore(monitor);
					}
				});
			} catch (InvocationTargetException ite) {
				// TODO Auto-generated catch block
				ite.printStackTrace();
			} catch (InterruptedException ie) {
				// TODO Auto-generated catch block
				ie.printStackTrace();
			}
		}
		//- Add this view to the listener list for this model.
		modelServer.addPropertyChangeListener(this);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private void configureTreeColumns(final Tree targetTree) {
		//TODO Column configuration is got from a fixed metadata that is defined inside the code.
		//		int columnCount = treeColumnsConfigurator.size();
		TreeColumn column = null;
		final Iterator<TreeColumnConfigurator> cit = ActionRecordTreeView.treeColumnsConfigurator.iterator();
		while (cit.hasNext()) {
			final TreeColumnConfigurator configurator = cit.next();
			column = new TreeColumn(targetTree, SWT.NONE);
			column.setWidth(configurator.getPreferredWidth());
			column.setText(configurator.getTitle());
			column.setToolTipText(configurator.getToolTip());
		}
		viewer.getTree().setLayout(new FillLayout());
		viewer.getTree().setLinesVisible(true);
		viewer.getTree().setHeaderVisible(true);
	}

	//- CLASS IMPLEMENTATION ...................................................................................
	private class ActionRecordContentProvider extends BaseTreeContentProvider {

		public ActionRecordContentProvider(AbstractGEFNode modelServer) {
			super(modelServer);
		}

		@Override
		public Object[] getElements(final Object parent) {
			if (null != parent) {
				if (parent instanceof BoatActionList)
					return ((BoatActionList) parent).getActionRecords();
				if (parent instanceof IViewSite)
					return ((PilotModelStore) providerContents).getActionRecords();
			}
			//			}else
			return null;
		}

		@Override
		public Object[] getChildren(final Object parent) {
			if (null != parent)
				if (parent instanceof BoatActionList)
					return ((BoatActionList) parent).getActionRecords();
			return null;
		}

		@Override
		public boolean hasChildren(final Object target) {
			if (null != target)
				if (target instanceof BoatActionList)
					return true;
			return false;
		}
	}

	//- CLASS IMPLEMENTATION ...................................................................................
	/* This class can forward the calls to the model if this implements the ITableLabelProvider interface */
	private class ViewTreeLabelProvider extends LabelProvider implements ITableLabelProvider {

		/** Return the model icon only if the column is set to column 0 that is the key column. */
		public Image getColumnImage(final Object target, final int index) {
			final TreeColumnConfigurator configurator = ActionRecordTreeView.getColumnConfiguration(index);
			if (target instanceof ITreeFacet)
				return configurator.getImage(((ITreeFacet) target));
			else
				return getImage(target);
		}

		public String getColumnText(final Object target, final int index) {
			final TreeColumnConfigurator configurator = ActionRecordTreeView.getColumnConfiguration(index);
			if (target instanceof ITreeFacet)
				return configurator.getColumnText(((ITreeFacet) target));
			else
				return getText(target);
		}

		@Override
		public Image getImage(final Object target) {
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}

		@Override
		public String getText(final Object target) {
			return target.toString();
		}
	}
}
//- UNUSED CODE ............................................................................................
