//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: SpotEditPart.java 179 2008-07-10 11:51:20Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-10 13:51:20 +0200 (jue, 10 jul 2008) $
//  RELEASE:        $Revision: 179 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Vector;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;

import net.sf.gef.core.editparts.AbstractDirectedNodeEditPart;
import net.sf.gef.core.model.IGEFNode;
import net.sf.vorg.policies.GNodePolicy;
import net.sf.vorg.vorgautopilot.models.PilotCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotCommandEditPart extends AbstractDirectedNodeEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vgap4.projecteditor.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public PilotCommand getCastedModel() {
		return (PilotCommand) getModel();
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (PilotCommand.WAYPOINT_ACTIVATED.equals(prop))
			//- Send this event up in the chain for a complete model refresh.
			((PropertyChangeListener) getParent()).propertyChange(evt);
	}

	@Override
	public String toString() {
		// TODO Add the field isSelected to the information that is displayed.
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[PilotCommandEditPart:");
		buffer.append(getModel().toString()).append("-");
		buffer.append(getFigure().toString()).append("-");
		buffer.append(super.toString()).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		IFigure child = ((GraphicalEditPart) childEditPart).getFigure();
		getContentPane().add(child);
	}

	@Override
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new GNodePolicy());
		super.createEditPolicies();
	}

	@Override
	protected List<IGEFNode> getModelChildren() {
		final Vector<IGEFNode> childList = new Vector<IGEFNode>(2);
		childList.addAll(getCastedModel().getWaypoints());
		return childList;
	}
}
// - UNUSED CODE ............................................................................................
