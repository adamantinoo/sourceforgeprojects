//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models;

// - IMPORT SECTION .........................................................................................
import org.eclipse.core.runtime.IProgressMonitor;

import net.sf.vorg.app.Activator;
import net.sf.vorg.ui.GraphicalProgressMonitor;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class UIPilotModelStore extends PilotModelStore {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.models");
	private static final long		serialVersionUID	= -2252280374360378912L;
	public static final String	PILOTMODELID			= "net.sf.vorg.models.PilotModelStore.id";

	//	private IProgressMonitor		progress;

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public UIPilotModelStore() {
	//		//- Create the progress indicator to receive the progress messages to be displayed
	//		super();
	//		Activator.addReference(UIPilotModelStore.PILOTMODELID, this);
	//	}

	/**
	 * Starts processing by setting the Progress Indicator to be used to display the state of the processing.
	 * This is only valid for graphical interfaces, so the stand alone processing version should not rely on
	 * such structures. The presentation will then be decoupled from the processing through a set of methods
	 * that will do not do anything in the stand alone version but that will report to the display on the
	 * graphical version.
	 */
	public UIPilotModelStore(IProgressMonitor monitor) {
		super(new GraphicalProgressMonitor(monitor));
		Activator.addReference(UIPilotModelStore.PILOTMODELID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean runningUI() {
		return true;
	}
}

// - UNUSED CODE ............................................................................................
