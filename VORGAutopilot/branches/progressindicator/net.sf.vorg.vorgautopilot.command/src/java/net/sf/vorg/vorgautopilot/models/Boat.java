//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.vorgautopilot.core.PilotLocation;
import net.sf.vorg.vorgautopilot.core.VORGURLRequest;
import net.sf.vorg.vorgautopilot.core.WaypointTypes;
import net.sf.vorg.vorgautopilot.parsers.BoatDataParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
@SuppressWarnings("deprecation")
public class Boat extends AbstractGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long				serialVersionUID				= 3537901291871922124L;
	private static Logger						logger									= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final int				BOTTOM_RANKING					= 999999;
	//	public static final String			PROPERTY_CHANGED		= "Boat.PROPERTY_CHANGED";
	//	public static final String			LOCATION_CHANGED		= "Boat.LOCATION_CHANGED";
	public static final String			NAME_CHANGED						= "Boat.NAME_CHANGED";
	public static final String			BOAT_UPDATE							= "Boat.BOAT_UPDATE";
	public static final String			IDENTIFICATION_CHANGED	= "Boat.IDENTIFICATION_CHANGED";
	public static final String			BOAT_COMMANDSENTOK			= "Boat.BOAT_COMMANDSENTOK";
	public static final String			BOAT_COMMANDSENTNOOK		= "Boat.BOAT_COMMANDSENTNOOK";

	// - F I E L D - S E C T I O N ............................................................................
	/** Date and time of the last data reload from the game servers. */
	private Date										lastBoatUpdate					= null;
	/** Flag that reports if the last update request really updated the Boat data or not */
	private boolean									updated									= false;
	private final Vector<Waypoint>	surpassedWaypoints			= new Vector<Waypoint>();
	private WindCell								currentWindCell					= null;
	// - BOAT data.
	private String									name										= null;
	private final PilotLocation			location								= new PilotLocation();
	private Sails										sail										= Sails.JIB;
	private double									speed										= 0.0;
	private double									windSpeed								= 0.0;
	private int											windAngle								= 0;
	private int											heading									= 0;
	private int											ranking									= Boat.BOTTOM_RANKING;
	//- INCREMENTAL DATA
	private int											diffRanking							= 0;
	private double									diffSpeed								= 0.0;
	private GeoLocation							previousLocation				= null;
	private double									distanceRun							= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Boat() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Return true or false depending if the waypoint is or not inside the list of points already walked. The
	 * test is made based on the distance between the target test waypoint and the waypoint in the list. If the
	 * distance is less than one mile then we consider that we have reached that point and a match is found.
	 * 
	 * @param waypoint
	 *          the waypoint to be tested
	 * @return true or <code>false</code> depending of this waypoint if found on the current list.
	 */
	public boolean checkSurpassed(final Waypoint waypoint) {
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		while (wit.hasNext()) {
			final Waypoint testWaypoint = wit.next();
			final double distance = testWaypoint.getLocation().distance(waypoint.getLocation());
			if (distance < 1.0) return true;
		}
		return false;
	}

	public void clearUpdate() {
		lastBoatUpdate = null;
		updated = false;
	}

	/**
	 * Generate the piece of XML to write down the information that is not persistent on the game server. Any
	 * other information can be retrieved when this boat is loaded, but the list of points that conform the
	 * route already walked.
	 * 
	 * @return the serialized string that contains the XML piece that is able to return the Boat to its current
	 *         state.
	 */
	public String generatePersistentXML() {
		final StringBuffer buffer = new StringBuffer();
		//- Iterate though the children to dump also all other persistent information.
		buffer.append("    <bypassedpointlist>").append(VORGConstants.NEWLINE);
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		while (wit.hasNext()) {
			final Waypoint waypoint = wit.next();
			if (waypoint.getType() == WaypointTypes.BYPASSED) {
				continue;
			}
			if (waypoint.getType() == WaypointTypes.NOACTION) {
				continue;
			}
			buffer.append(waypoint.generatePersistentXML("bypassedwaypoint"));
		}
		buffer.append("    </bypassedpointlist>").append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	public int getDiffRanking() {
		return diffRanking;
	}

	public double getDiffSpeed() {
		return diffSpeed;
	}

	public int getHeading() {
		return heading;
	}

	public Date getLastBoatUpdate() {
		return lastBoatUpdate;
	}

	public GeoLocation getLocation() {
		return location.getLocation();
	}

	public String getName() {
		return name;
	}

	/**
	 * Return the percentage of the current boat speed relative to the max speed that can be reached on the
	 * current wind cell.
	 */
	public String getPower() {
		final WindCell cell = getWindCell();
		final VMCData vmg = new VMCData(heading, cell);
		final int maxAWD = vmg.getMaxAWD();
		final SailConfiguration configuration = Polars.lookup(maxAWD, cell.getWindSpeed());
		final double maxSpeed = configuration.getSpeed();
		return FormatSingletons.nf2.format(speed * 100.0 / maxSpeed) + "%";
	}

	public int getRanking() {
		return ranking;
	}

	public Sails getSail() {
		return sail;
	}

	public double getSpeed() {
		return speed;
	}

	public int getWindAngle() {
		return windAngle;
	}

	public WindCell getWindCell() {
		if (null == currentWindCell) {
			try {
				currentWindCell = WindMapHandler.getWindCell(getLocation());
			} catch (final LocationNotInMap e) {
				final int cellLocationLat = new Long(Math.round(getLocation().getLat())).intValue();
				final int cellLocationLon = new Long(Math.round(getLocation().getLon())).intValue();
				currentWindCell = new WindCell(cellLocationLat, cellLocationLon, windAngle, windSpeed);
			}
		}
		return currentWindCell;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public String printReport() {
		final StringBuffer buffer = new StringBuffer(toString());
		final WindCell cell = getWindCell();
		buffer.append("[Wind Cell Data ").append("direction=").append(cell.getWindDir()).append(", ");
		buffer.append("speed=").append(FormatSingletons.nf3.format(cell.getWindSpeed())).append(", ");
		buffer.append("\n                location=").append(cell.getLocation()).append("]\n");

		// - Add current information for course VMG
		final VMCData vmg = new VMCData(heading, cell);
		buffer.append("\n").append(vmg.printReport());
		return buffer.toString();
	}

	public void setHeading(final int newHeading) {
		heading = newHeading;
	}

	public boolean setLastUpdateDate(final String value) {
		if (null != value) {
			final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			try {
				final Date newDate = formatter.parse(value);
				if (null == newDate) {
					lastBoatUpdate = null;
					updated = false;
					return updated;
				}
				//- Detect if this updated has changed the date value.
				if (null != lastBoatUpdate) {
					final int compare = lastBoatUpdate.compareTo(newDate);
					if (compare != 0) {
						lastBoatUpdate = newDate;
						updated = true;
					} else {
						updated = false;
					}
				} else {
					lastBoatUpdate = newDate;
					updated = true;
				}
			} catch (final ParseException pe) {
				lastBoatUpdate = null;
				updated = false;
			}
		}
		return updated;
	}

	public void setLatitude(final String latitude) {
		location.setLatitude(latitude);
	}

	public void setLocation(final GeoLocation newLocation) {
		location.setLocation(newLocation);
	}

	public void setLongitude(final String longitude) {
		location.setLongitude(longitude);
	}

	public void setName(final String newName) {
		final String oldName = name;
		if (newName.toLowerCase().equals(name)) return;
		name = newName;
		firePropertyChange(Boat.NAME_CHANGED, oldName, newName);
	}

	public void setRanking(final int classification) {
		if (wasUpdated()) {
			if (ranking < Boat.BOTTOM_RANKING) if (ranking != classification) {
				diffRanking = classification - ranking;
			}
			ranking = classification;

			//- Then process the new location and calculate the distance run
			if (null == previousLocation) {
				previousLocation = getLocation();
			} else {
				distanceRun = previousLocation.distance(getLocation());
				previousLocation = getLocation();
			}
		} else {
			ranking = classification;
		}
	}

	public void setSail(final Sails newSails) {
		if (null != newSails) {
			sail = newSails;
		}
	}

	public void setSail(final String sailCode) {
		sail = Sails.decodeSail(sailCode);
	}

	public void setSpeed(final double newSpeed) {
		if (speed != newSpeed) {
			diffSpeed = newSpeed - speed;
			speed = newSpeed;
		}
	}

	public void setWindAngle(final int windAngle) {
		this.windAngle = windAngle;
	}

	public void setWindSpeed(final double windSpeed) {
		this.windSpeed = windSpeed;
	}

	/**
	 * Add a new point to the list of walked points. This call also updates the persistent file that keeps track
	 * of this list of points.
	 * 
	 * @param waypoint
	 *          the waypoint to be added to the list of already walked waypoints.
	 */
	public void surpassWaypoint(final Waypoint waypoint) {
		if (surpassWaypointDirect(waypoint)) {
			setDirty(true);
		}
	}

	/**
	 * Just add this point to the list of walked points. During the addition process, check for duplicates and
	 * do not add a point already on the list or close to the point. The addition is done without altering the
	 * persistent repository where the points are stored.
	 * 
	 * @param waypoint
	 *          the waypoint to be added to the current list.
	 * @return <code>true</code> if the point is added to the list or <code>false</code> if the point is already
	 *         there and was not added to the list. This will help signaling of dirty states.
	 */
	public boolean surpassWaypointDirect(final Waypoint waypoint) {
		// - Check for duplicated before adding this point.
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		boolean found = false;
		while (wit.hasNext()) {
			final Waypoint testWp = wit.next();
			if (testWp.isEquivalent(waypoint)) {
				found = true;
			}
		}
		if (!found) {
			surpassedWaypoints.add(waypoint);
			waypoint.setState(WaypointStates.SURPASSED);
			//			if (null != parent) {
			//				((PilotBoat) parent).recordChange(new ActionRecord(Waypoint.WAYPOINT_SURPASSED, this));
			//			}			
			return true;
		} else return false;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Boat ");
		buffer.append("name=").append(getName()).append(", ");
		buffer.append("ranking=").append(ranking).append("");
		buffer.append("\n      location=").append(location.getLocation()).append("");
		buffer.append("\n      heading=").append(heading).append(", ");
		buffer.append("AWD=").append(windAngle).append(", ");
		buffer.append("speed=").append(FormatSingletons.nf3.format(speed)).append(" knots, ");
		buffer.append("sail=").append(sail).append("");
		buffer.append("\n      windSpeed=").append(FormatSingletons.nf3.format(windSpeed)).append(" knots");
		buffer.append("]\n");
		return buffer.toString();
	}

	/**
	 * Reads the Boat information form the game servers and processes it with a XML parser to update the Boat
	 * contents.<br>
	 * But this is only executed if the boat information is older than 9 minutes. There is no need to update
	 * this information more frequently. We use the server date information compared with the last update date
	 * to detect if the elapsed time is greater than nine minutes. Then we force a reload that may or may not
	 * update the information. If the boat information was changed on the last reload is signaled on the field
	 * <code>updated</code>.
	 * 
	 * @return returns the state of the <code>updated</code> field that represents if the content data of the
	 *         boat was or not really updated.
	 * @throws DataLoadingException
	 *           if any exception is found during the processing of this data. This exception encloses all other
	 *           SAX and XML exceptions.
	 * @throws BoatNotFoundException
	 *           thrown if the Boat is detected to not be found on the server databases. This is a especial
	 *           detected XML message.
	 */
	public boolean updateBoatData() throws DataLoadingException, BoatNotFoundException {
		// - First check if this is a configured Boat instance.
		if (null != name) {
			if (needsUpdate()) {
				Boat.logger.fine("Processing boat data for boat: " + name);
				final StringBuffer request = new StringBuffer("/get_user.php?");
				try {
					request.append("pseudo=" + URLEncoder.encode(getName(), "UTF-8"));
				} catch (final UnsupportedEncodingException uee) {
					throw new DataLoadingException("Some Exception while loading the boat data. " + uee.getMessage());
				}

				final VORGURLRequest boatRequest = new VORGURLRequest(request.toString());
				try {
					boatRequest.executeGET(null);
					//- Clear the current wind cell to be updated with fresh data.
					currentWindCell = null;
					parseBoatData(boatRequest.getData());
					// - Add current boat location point to the list of surpassed points.
					final Waypoint waypoint = new Waypoint(WaypointTypes.BYPASSED);
					waypoint.setLocation(getLocation());
					surpassWaypoint(waypoint);

					//- Check if the boat data was updated by the date detection
					if (updated) {
						firePropertyChange(Boat.BOAT_UPDATE, null, this);
						//- Register this new update in the ActionRecordings
						if (null != parent) {
							((PilotBoat) parent).recordChange(new ActionRecord(Boat.BOAT_UPDATE, this));
						}
					}
					return true;
				} catch (final ParserConfigurationException pce) {
					System.out.println("EEE - " + pce.getLocalizedMessage());
					throw new DataLoadingException("Some Exception while loading the boat data. " + pce.getMessage());
				} catch (final SAXException saxe) {
					if (saxe instanceof BoatNotFoundException)
						//- Process this special case throwing again the exception
						throw new BoatNotFoundException(saxe.getLocalizedMessage());
					else {
						System.out.println("EEE - " + saxe.getLocalizedMessage());
						throw new DataLoadingException("Some Exception while loading the boat data. " + saxe.getMessage());
					}
				} catch (final MalformedURLException mue) {
					System.out.println("EEE - " + mue.getLocalizedMessage());
					throw new DataLoadingException("Some Exception while loading the boat data. " + mue.getMessage());
				} catch (final ProtocolException pe) {
					System.out.println("EEE - " + pe.getLocalizedMessage());
					throw new DataLoadingException("Some Exception while loading the boat data. " + pe.getMessage());
				} catch (final IOException ioe) {
					System.out.println("EEE - " + ioe.getLocalizedMessage());
					throw new DataLoadingException("Some Exception while loading the boat data. " + ioe.getMessage());
				}
			} else {
				updated = false;
				return false;
			}
		} else throw new DataLoadingException("Invalid update call. Instance is not initialized");
	}

	public boolean wasUpdated() {
		return updated;
	}

	private boolean needsUpdate() {
		if (null == lastBoatUpdate) return true;
		final Calendar now = Calendar.getInstance();
		now.add(Calendar.MINUTE, -9);
		final Date nowTime = now.getTime();
		final boolean test = nowTime.after(lastBoatUpdate);
		if (nowTime.after(lastBoatUpdate)) return true;
		return false;
	}

	/**
	 * Opens a StringReader to process the data being received. Uses an XML parser to detect the model elements
	 * and then updates the Boat data with the new server read data. Detects the "Boat not Found" exception to
	 * signal that this boat is not present on the game server.
	 * 
	 * @param data
	 *          the stream contents to be processed for parsing
	 * @throws ParserConfigurationException
	 *           thrown if the parser founds any internal exception.
	 * @throws SAXException
	 *           thrown for any parsing and format exception detected by the DefaultHandlers.
	 * @throws IOException
	 *           if any IO error happens. This is for compatibility to the version where the reading is
	 *           performed directly to the HTTP stream.
	 */
	private void parseBoatData(final String data) throws ParserConfigurationException, SAXException, IOException {
		//		try {
		//		lastException = null;
		final InputStream stream = new BufferedInputStream(new StringBufferInputStream(data));
		final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		//- Create our parser and pass it the reference to the Boat to update their contents.
		final BoatDataParserHandler handler = new BoatDataParserHandler(this);
		parser.parse(stream, handler);
	}

	/** Promotes the dirty state to the parent and up to the final container. */
	private void setDirty(final boolean dirtyState) {
		if (null != parent) if (parent instanceof PilotBoat) {
			((PilotBoat) parent).setDirty(dirtyState);
		}
	}

	public double getDistanceRun() {
		return distanceRun;
	}
}
// - UNUSED CODE ............................................................................................
