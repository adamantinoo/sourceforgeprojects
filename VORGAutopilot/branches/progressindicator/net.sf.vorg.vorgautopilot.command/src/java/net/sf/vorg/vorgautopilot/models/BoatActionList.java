//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatActionList implements ITreeFacet {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger								logger		= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final Vector<ActionRecord>	records		= new Vector<ActionRecord>();
	private String											boatName	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatActionList(String name) {
		boatName = name;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getBoatName() {
		return boatName;
	}

	public void add(ActionRecord record) {
		records.add(record);
	}

	public Object[] getActionRecords() {
		return records.toArray();
	}
}

// - UNUSED CODE ............................................................................................
