//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.ui;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.vorgautopilot.core.IVORGProgressMonitor;

// - CLASS IMPLEMENTATION ...................................................................................
public class StandAloneProgressMonitor implements IVORGProgressMonitor {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.ui");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public StandAloneProgressMonitor() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void beginTask(String name, int totalWork) {
		System.out.println();
		System.out.print(name);
	}

	@Override
	public void done() {
		// TODO Auto-generated method stub

	}

	@Override
	public void internalWorked(double work) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isCanceled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setCanceled(boolean value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTaskName(String name) {
		System.out.println();
		System.out.print(name);
	}

	@Override
	public void subTask(String name) {
		System.out.println();
		System.out.print(name);
	}

	@Override
	public void worked(int work) {
		System.out.print(".");
	}
}

// - UNUSED CODE ............................................................................................
