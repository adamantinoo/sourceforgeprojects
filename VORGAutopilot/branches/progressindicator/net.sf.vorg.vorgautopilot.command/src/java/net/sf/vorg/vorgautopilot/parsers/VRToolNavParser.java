//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.exceptions.InvalidContentException;
import net.sf.vorg.core.enums.InputHandlerStates;
import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.core.NavAttributes;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class VRToolNavParser extends ABoatRouteParser {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VRToolNavParser(final String targetInput) {
		super(targetInput);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public InputTypes getType() {
		return InputTypes.NAV;
	}

	/**
	 * Before loading the content of the pointed file, this method checks if the file really need to be read.
	 * This is done by comparing if the current file message digest is the same as the stored message digest.<br>
	 * Process the data contents of the configured VRTool file and parse the contents to load and update the
	 * data inside the Pilot Model.
	 */
	public boolean loadContents() {
		if (needsReload()) {
			try {
				System.out
						.println("------------------------------------------------------------------------------------------");
				System.out.println(Calendar.getInstance().getTime() + " - new scan run for configuration: " + getFilePath());
				logger.fine("Model needs reload. Parsing again NAV data.");
				parse();
				final byte[] localHash = computeHash();
				logger.fine("Computed hash for file [" + inputReference + "] is " + hexEncode(localHash));
				hash = localHash;
				lastUpdate = Calendar.getInstance();
				return true;
			} catch (final FileNotFoundException fnfe) {
				System.out.println("EEE - " + fnfe.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, fnfe);
				return false;
			} catch (final DataLoadingException dle) {
				System.out.println("EEE - " + dle.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, dle);
				return false;
			} catch (final IOException ioe) {
				System.out.println("EEE - " + ioe.getLocalizedMessage());
				setState(InputHandlerStates.ERROR, ioe);
				return false;
			}
		} else {
			System.out.println("------------------------------------------------------------------------------------------");
			System.out.println(Calendar.getInstance().getTime() + " - input route configuration not reloaded.");
			return false;
		}
	}

	@Override
	public void startElement(final String name, final Attributes attributes) throws SAXException {
		// - Get the boat information to be managed.
		if (name.toLowerCase().equals("boat")) {
			boatConstruction = new PilotBoat();
			boatConstruction.setBoatName(validateNotNull(attributes, "name"));
			boatConstruction.setBoatId(validateNull(attributes, "userid"));
			boatConstruction.setBoatEmail(validateNull(attributes, "email"));
			boatConstruction.setBoatClef(validateNull(attributes, "clef"));
			boatConstruction.updateBoatData();
		}
		super.startElement(name, attributes);
	}

	protected boolean detectPilotRoute(final String line) {
		final String[] sections = line.split(";");
		final String type = sections[0];
		if (type.equals("O")) {
			final String name = sections[1];
			if (name.equals("Route")) {
				// - Get the name of the boat from the suffix.
				final String boatName = sections[2];
				if (boatName.toUpperCase().startsWith("AP_")) return true;
			}
		}
		return false;
	}

	/**
	 * Read the input file and convert each line to a list of attributes like the XML parsers. Then send that
	 * information to the <code>startElement</code> method for processing and data acquisition.
	 * 
	 * @throws FileNotFoundException
	 */
	protected void parse() throws FileNotFoundException, DataLoadingException, IOException {
		//- Check we are properly configured before starting.
		if ((null == boatStore) || (null == inputReference))
			throw new DataLoadingException("Invalid call. Instance is not initialized");
		else {
			// - Open the input file.
			final BufferedReader input = new BufferedReader(new FileReader(inputReference));
			String line = input.readLine();
			while (null != line) {
				try {
					if (detectPilotRoute(line)) if (parseBoatName(line)) {
						processWaypoints(input);
					}
				} catch (final RuntimeException exc) {
					//- Detect any type of runtime exception and bypass it.
					System.out.println("EEE - " + exc.getLocalizedMessage());
				}
				line = input.readLine();
			}
		}
	}

	/**
	 * After a boat route definition line is found and the name processed, the next lines define route
	 * waypoints. So keep reading and generate the waypoint list.
	 * 
	 * @throws IOException
	 */
	protected void processWaypoints(final BufferedReader input) throws IOException {
		//		try {
		String line = input.readLine();
		while (null != line) {
			try {
				final String[] sections = line.split(";");
				final String type = sections[0];
				if (type.equals("P")) {
					boolean waypointPresent = false;
					final NavAttributes attr = new NavAttributes();
					final String name = "waypoint";
					attr.add("latitude", sections[1]);
					// - Coordinate system on VRTool is not the same as the game. Change the sign of the longitude.
					attr.add("longitude", changeSign(sections[2]));

					// - Detect and parse the section 3 that contains the waypoint attributes
					if (sections.length > 3) {
						waypointPresent = parseWaypointAttributes(sections[3], attr);
					}
					if (sections.length > 4) {
						attr.add("date", sections[4]);
					}
					if (waypointPresent) {
						try {
							this.startElement(name, attr);
						} catch (final InvalidContentException ice) {
							//- Continue processing the file
							System.out.println("EEE - " + ice.getLocalizedMessage());
						} catch (final BoatNotFoundException bnfe) {
							//- Continue processing the file
							System.out.println("EEE - " + bnfe.getLocalizedMessage());
						} catch (final DataLoadingException dle) {
							//- Continue processing the file
							System.out.println("EEE - " + dle.getLocalizedMessage());
						} catch (final SAXException saxe) {
							//- Continue processing the file
							System.out.println("EEE - " + saxe.getLocalizedMessage());
						}
					}
				}
			} catch (final RuntimeException exc) {
				//- Detect any type of runtime exception and continue reading.
				System.out.println("EEE - " + exc.getLocalizedMessage());
			}
			line = input.readLine();
			if (line.equals("")) {
				line = null;
				boatStore.progress(1);
			}
		}
		// - Close the pilot command.
		try {
			this.endElement("pilotcommand");
			this.endElement("boat");
		} catch (final SAXException saxe) {
			//- Detect any type of runtime exception and continue reading.
			System.out.println("EEE - " + saxe.getLocalizedMessage());
		}
	}

	private String changeSign(final String value) {
		double detectedValue = 0.0;
		if (null == value) return "0.0";
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = value.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1) {
				detectedValue = new Double(value).doubleValue();
			} else {
				final int degree = new Integer(value.substring(0, pos)).intValue();
				final int minute = new Integer(value.substring(pos + 1, value.length())).intValue();
				// - Test for negative values.
				if (degree < 0) {
					detectedValue = degree * 1.0 - minute / 60.0;
				} else {
					detectedValue = degree * 1.0 + minute / 60.0;
				}
			}
		} catch (final Exception ex) {
			detectedValue = 0.0;
		}
		return new Double(detectedValue * -1.0).toString();
	}

	/**
	 * Processed a line that is known to contain a boat identification route. Extract the boat name from this
	 * route
	 */
	private boolean parseBoatName(final String line) {
		try {
			//- Split the NAV line into its components.
			final String[] sections = line.split(";");
			String boatName = sections[2];
			boatName = boatName.substring(3);

			if (null != boatName) {
				// - Read the file with the identification information.
				final Properties idprops = new Properties();
				try {
					idprops.load(new BufferedInputStream(new FileInputStream(boatName + ".ident")));
					System.out.println("Detected configuration Route for boat name=" + boatName);
				} catch (final FileNotFoundException e) {
					System.out.println("Identification file for " + boatName + " not found.");
					return false;
				} catch (final IOException e) {
					System.out.println("Identification file for " + boatName + " load error.");
					return false;
				}

				try {
					//- Create the XML lines to create the structure.
					NavAttributes attr = new NavAttributes();
					attr.add("name", boatName);
					attr.add("userid", idprops.getProperty("userid"));
					attr.add("email", idprops.getProperty("email"));
					attr.add("clef", idprops.getProperty("clef"));

					//- Check if this boat is already on the store and the clear the list of commands.
					final PilotBoat existent = boatStore.searchBoatName(boatName);
					if (null == existent) {
						this.startElement("boat", attr);
					} else {
						//- Because we do not create the boat we have to assign it directly to the boatConstuction.
						boatConstruction = existent;
						boatStore.getProgressManager().subTask(
								"Processing route waypoint for boat [" + boatConstruction.getBoatName() + "].");
						boatConstruction.clearCommands();

						//- Update the boat information from the game server. This update is required for interface presentation
						boatConstruction.updateBoatData();
						//- Clear the update date flag to force a new update on the processing.
						boatConstruction.getBoat().clearUpdate();
					}

					attr = new NavAttributes();
					attr.add("type", "ROUTE");
					this.startElement("pilotcommand", attr);
					attr.add("border", "NOBORDER");
					attr.add("action", "connect");
					this.startElement("limit", attr);
					this.startElement("waypointlist", new NavAttributes());
				} catch (final InvalidContentException ice) {
					//- Continue processing the file
					System.out.println("EEE - " + ice.getLocalizedMessage());
					return false;
				} catch (final BoatNotFoundException bnfe) {
					//- Continue processing the file
					System.out.println("EEE - " + bnfe.getLocalizedMessage());
					return false;
				} catch (final DataLoadingException dle) {
					//- Continue processing the file
					System.out.println("EEE - " + dle.getLocalizedMessage());
					return false;
				} catch (final SAXException saxe) {
					//- Continue processing the file
					System.out.println("EEE - " + saxe.getLocalizedMessage());
					return false;
				}
				return true;
			} else return false;
		} catch (final RuntimeException exc) {
			//- Detect any type of runtime exception and bypass it.
			System.out.println("EEE - " + exc.getLocalizedMessage());
			return false;
		}
	}

	/** Parses the Text field of the VRT point where there is the waypoint configuration information. */
	private boolean parseWaypointAttributes(final String text, final NavAttributes attr) {
		// - Check that the string contains a valid list of attributes. Search for the key type.
		if (text.toLowerCase().contains("type")) {
			final String[] parameters = text.split(" ");
			for (final String parameter : parameters) {
				if ((null == parameter) || (parameter.equals(""))) {
					continue;
				}
				final String[] values = parameter.split("=");
				// - Check for bad formed parameters.
				if (values.length < 2) {
					continue;
				}
				if (null == values[0]) {
					continue;
				}
				if (null == values[1]) {
					continue;
				}
				attr.add(values[0], values[1].replace('"', ' ').trim());
			}
			System.out.println("Waypoint detected and OK. Contents. " + text);
			return true;
		} else {
			System.out.println("Waypoint detected but not properly configured. Skipped. Contents=" + text);
		}
		return false;
	}
}
// - UNUSED CODE ............................................................................................
