//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.AbstractXMLHandler;
import net.sf.core.exceptions.InvalidContentException;
import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.core.WaypointTypes;
import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatModelParserHandler extends AbstractXMLHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private PilotModelStore	modelContainer	= null;
	private PilotBoat				boatRef					= null;
	private PilotCommand		buildUpCommand	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatModelParserHandler(final PilotModelStore modelServer) {
		modelContainer = modelServer;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/** Parse the configuration file and store the parameters into the single boat instance. */
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		// - Get the boat name to be managed.
		if (name.toLowerCase().equals("configuration")) {
			modelContainer.getProgressManager().subTask("Analyzing Waypoint Configuration definition");
			String fileLocation = (validateNull(attributes, "location"));
			String inputType = (validateNull(attributes, "type"));
			//- Create the input handler if properly configured.
			if ((null != fileLocation) && (null != inputType) && (null != modelContainer)) {
				modelContainer.setRefreshInterval(validateNull(attributes, "refresh"));
				modelContainer.setTimeDeviation(validateNull(attributes, "time"));
				InputTypes inputCode = InputTypes.decode(inputType);
				//- Special case. If we are running the user interface the model is of class UIPilotModelStore
				if (modelContainer.runningUI()) {
					if (inputCode == InputTypes.NAV) {
						modelContainer.setInputHandler(new UIVRToolNavParser(fileLocation));
					}
				} else if (inputCode == InputTypes.NAV) {
					modelContainer.setInputHandler(new VRToolNavParser(fileLocation));
				}
				if (inputCode == InputTypes.XML) {
					modelContainer.setInputHandler(new XMLFileParser(fileLocation));
				}
			}
		}
		if (name.toLowerCase().equals("boat")) {
			boatRef = new PilotBoat();
			try {
				boatRef.setBoatName(validateNotNull(attributes, "name"));
				modelContainer.getProgressManager().subTask("Processing Boat data for boat [" + boatRef.getBoatName() + "].");
				boatRef.setBoatId(validateNull(attributes, "boatid"));
				boatRef.setBoatEmail(validateNull(attributes, "email"));
				boatRef.setBoatClef(validateNull(attributes, "clef"));
				modelContainer.buildBoat(boatRef);
				boatRef.updateBoatData();
				modelContainer.progress(1);
				modelContainer.getProgressManager().subTask(
						"Processing Boat commands and waypoints for boat [" + boatRef.getBoatName() + "].");
			} catch (InvalidContentException ice) {
				throw new SAXException(ice);
			} catch (DataLoadingException dle) {
				//- No nothing so this boat is removed from the processing.
			} catch (BoatNotFoundException bnfe) {
				throw new SAXException(bnfe);
			}
		}
		if (name.toLowerCase().equals("bypassedwaypoint")) if (null != boatRef) {
			Boat currentBoat = boatRef.getBoat();
			if (null != currentBoat) {
				try {
					//				Waypoint waypoint = new Waypoint(WaypointTypes.BYPASSED);
			WaypointTypes type = WaypointTypes.decodeType(AbstractXMLHandler.svalidateNotNull(attributes, "type"));
			if (type == WaypointTypes.NOACTION) {
				type = WaypointTypes.BYPASSED;
			}
			final Waypoint waypoint = new Waypoint(type);
			waypoint.setName(AbstractXMLHandler.svalidateNull(attributes, "name"));
			waypoint.setLatitude(AbstractXMLHandler.svalidateNotNull(attributes, "latitude"));
			waypoint.setLongitude(AbstractXMLHandler.svalidateNotNull(attributes, "longitude"));
			waypoint.setRange(AbstractXMLHandler.svalidateNull(attributes, "range"));
			waypoint.setMinAWD(AbstractXMLHandler.svalidateNull(attributes, "minAWD"));
			waypoint.setMaxAWD(AbstractXMLHandler.svalidateNull(attributes, "maxAWD"));
			waypoint.setAngleParameter(AbstractXMLHandler.svalidateNull(attributes, "angle"));
			waypoint.setSelectedAWD(AbstractXMLHandler.svalidateNull(attributes, "angle"));
			currentBoat.surpassWaypoint(waypoint);
		} catch (SAXException saxe) {
			System.out.println("EEE - " + saxe.getLocalizedMessage());
		}
	}
}

		// - Read and create the list of commands to be processed for the boat.
		if (name.toLowerCase().equals("pilotcommand")) {
			buildUpCommand = new PilotCommand(attributes.getValue("type"));
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("pilotlimits")) if (null != buildUpCommand) {
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("limit")) if (null != buildUpCommand) {
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("waypointlist")) if (null != buildUpCommand) {
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("waypoint")) if (null != buildUpCommand) {
			buildUpCommand.startElement(name, attributes);
		}
	}

	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {
		super.endElement(uri, localName, name);
		if (name.toLowerCase().equals("pilotcommand")) {
			boatRef.addCommand(buildUpCommand);
			modelContainer.progress(1);
		}
	}
}

// - UNUSED CODE ............................................................................................
