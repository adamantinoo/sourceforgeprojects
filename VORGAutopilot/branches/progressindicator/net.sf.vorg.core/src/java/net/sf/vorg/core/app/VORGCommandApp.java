//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.app;

// - IMPORT SECTION .........................................................................................
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	protected static Logger			logger			= Logger.getLogger("net.sf.vorg");
	private static PrintWriter	printer			= null;
	protected static boolean		onDebug			= false;
	protected static String			outputName	= "VORGCommandApp.output.txt";
	protected static String			NEWLINE			= System.getProperty("line.separator");
	static {
		VORGCommandApp.logger.setLevel(Level.OFF);
	}

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	public static boolean onDebug() {
		return VORGCommandApp.onDebug;
	}

	public static void output() {
		VORGCommandApp.output(VORGConstants.NEWLINE);
	}

	public static void output(final String message) {
		if (null == message) {
			VORGCommandApp.output();
			return;
		}
		System.out.println(message);
		if (null == VORGCommandApp.printer) {
			try {
				VORGCommandApp.printer = new PrintWriter(new FileOutputStream(VORGCommandApp.outputName, true), true);
				VORGCommandApp.printer.println(message);
				VORGCommandApp.printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			VORGCommandApp.printer.println(message);
			VORGCommandApp.printer.flush();
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	private final String[]	parameters;
	private StringBuffer		debugBuffer;
	//- P A R A M E T E R S
	protected GeoLocation		startLocation		= null;
	protected String				navigationFile	= null;
	protected int						cellsToScan			= 20;
	private int							nextArgument		= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public VORGCommandApp() {
	//	}

	public VORGCommandApp(final String[] args) {
		// - Store the parameters received on this invocation into the instance for method availability.
		parameters = args;
		nextArgument = 0;
	}

	/**
	 * Scans and process the application arguments. The processing expects that all key arguments start with the
	 * character "-" and that are followed by the exact number of parameters for the key if required.
	 */
	public void processParameters(final String[] args, final String ApplicationName) {
		debugBuffer = new StringBuffer();
		for (int i = 0; i < args.length; i++) {
			//- Get the next argument. It should be a key, if not skip it until next key.
			final String argument = this.getNextArgument();
			//- Store debugger output because the state of the debugging flag is still unknown.
			this.buffer2debug("Application argument: args[" + i + "] = " + argument);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) if (args[i].toLowerCase().startsWith("-sta")) { //$NON-NLS-1$
					startLocation = new GeoLocation();
					startLocation.setLat(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					startLocation.setLon(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					continue;
				}
			if (args[i].toLowerCase().startsWith("-nav")) { //$NON-NLS-1$
				navigationFile = this.argumentStringValue(args, i);
				i++;
				continue;
			}
			if (args[i].toLowerCase().startsWith("-cell")) { //$NON-NLS-1$
				cellsToScan = this.argumentIntegerValue(args, i);
				i++;
				continue;
			}
			if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
				VORGCommandApp.onDebug = true;
				VORGCommandApp.logger.setLevel(Level.ALL);
				continue;
			}
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	protected double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = this.argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	protected double argumentGeopositionValue(final String positionData) {
		if (null == positionData) return 0.0;
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = positionData.indexOf(":");
			// - The latitude comes in double format
			if (pos < 1)
				return new Double(positionData).doubleValue();
			else {
				final int degree = new Integer(positionData.substring(0, pos)).intValue();
				final int minute = new Integer(positionData.substring(pos + 1, positionData.length())).intValue();
				// - Test for negative values.
				// - Check for negative values but in the range from 0 to 1 degree.
				if (positionData.substring(0, 1).equals("-"))
					return degree * 1.0 - minute / 60.0;
				else if (degree < 0)
					return degree * 1.0 - minute / 60.0;
				else
					return degree * 1.0 + minute / 60.0;
			}
		} catch (final Exception ex) {
			return 0.0;
		}
	}

	protected int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = this.argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	protected String argumentStringValue(final String value) {
		return value;
	}

	protected String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else {
			// - Exit point 10. There are no enough arguments in the list to find a value.
			VORGCommandApp.exit(VORGConstants.NOCONFIG);
		}
		return "";
	}

	protected void buffer2debug(final String message) {
		debugBuffer.append(message).append(VORGCommandApp.NEWLINE);
	}

	protected String getNextArgument() {
		if (nextArgument >= parameters.length) {
			// - Exit point 10. There are no enough arguments in the list to find a value.
			VORGCommandApp.exit(VORGConstants.NOCONFIG);
		}
		return parameters[nextArgument++];
	}
}

// - UNUSED CODE ............................................................................................
