//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Locale;
import java.util.logging.Logger;

import net.sf.vorg.core.enums.Sails;

// - CLASS IMPLEMENTATION ...................................................................................
public class SailConfiguration {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................
	protected double			speed		= 0.0;
	protected Sails				sail		= Sails.JIB;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SailConfiguration() {
	}

	public SailConfiguration(final Sails sail, final double newSpeed) {
		this.sail = sail;
		speed = newSpeed;
	}

	public Sails getSail() {
		return sail;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double getSpeed() {
		return speed;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[SailConfiguration ");
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(3);
		nf.setMinimumFractionDigits(3);
		buffer.append(sail).append(" - ");
		buffer.append("speed=").append(nf.format(speed)).append("]");
		return buffer.toString();
	}

	public void setSail(final Sails sail) {
		this.sail = sail;
	}

	public void setSpeed(final double speed) {
		this.speed = speed;
	}

}
// - UNUSED CODE ............................................................................................
