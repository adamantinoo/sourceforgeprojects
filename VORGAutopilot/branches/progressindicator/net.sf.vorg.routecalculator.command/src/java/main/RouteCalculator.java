//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

//- IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.app.VORGCommandApp;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.routecalculator.core.RouterType;
import net.sf.vorg.routecalculator.models.Finder;
import net.sf.vorg.routecalculator.models.IsochroneRouter;
import net.sf.vorg.routecalculator.models.IsochroneStepRouter;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.Router;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCalculator extends VORGCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator");
	private static final String			APPLICATIONNAME	= "RouteCalculator";
	private static final String			APPDESCRIPTION	= "VORG Route Calculator Application";
	private static final String			VERSION					= RouteCalculator.APPDESCRIPTION + "0.2.0";
	/** Reference to the static part of the application */
	private static RouteCalculator	singleton;
	static {
		RouteCalculator.logger.setLevel(Level.OFF);
	}
	static {
		VORGCommandApp.outputName = RouteCalculator.APPLICATIONNAME + ".output.txt";
	}

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherited code
		// to be executed instead making a lot of calls.
		RouteCalculator.singleton = new RouteCalculator(args);
		RouteCalculator.singleton.execute();
		VORGCommandApp.exit(0);
	}

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation	endLocation		= null;
	private GeoLocation	waypoint			= null;
	private boolean			printVMC			= false;
	private boolean			printVMG			= false;
	private boolean			onlyDirect		= false;
	private RouterType	routerType		= RouterType.NONE;
	private int					heading				= 0;
	private int					windDir				= 0;
	private double			windSpeed			= 0.0;
	private double			boatSpeed;
	private boolean			optimize			= false;
	private double			ortoAngle			= -1.0;
	private int					maxAWD				= 120;
	private boolean			activeMax			= false;
	private boolean			activeAWD			= false;
	private boolean			activeVMG			= false;
	private int					VMGAngle			= 90;
	private int					searchAngle		= 90;
	private boolean			activeSearch	= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are:
	 * <ul>
	 * <li><b>-conf<font color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where
	 * the application will expect the configuration files and data.</li>
	 * <li><b>-res<font color="GREY">[ourcesLocation</font></b> ${RESOURCEDIR} - is the directory where the
	 * application is going to locate the files that contains the SQL statements and other application
	 * resources.
	 */
	public RouteCalculator(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherited.
		super(args);

		// - Initialize log and print out the banner
		this.banner();

		// - Process parameters and store them into the instance fields
		this.processParameters(args, RouteCalculator.APPLICATIONNAME);
	}

	/** Runs the optimizer or the route calculator depending on the specified parameters. */
	public void execute() {
		if (printVMC) if (null != startLocation) {
			WindCell cell;
			try {
				cell = WindMapHandler.getWindCell(startLocation);
				final VMCData vmc = new VMCData(heading, cell.getWindDir(), cell.getWindSpeed());
				VORGCommandApp.output(vmc.printReport());
			} catch (final LocationNotInMap e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		if (printVMG) {
			final VMCData vmc = new VMCData(heading, windDir, windSpeed);
			VORGCommandApp.output(vmc.printReport());
		}
		if (routerType == RouterType.ISO) {
			// - Check for required parameters that are the start and the waypoint
			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);

			final IsochroneRouter theRouter = new IsochroneRouter(routerType);
			theRouter.setOutputFile(navigationFile);
			theRouter.setOrtoAngle(ortoAngle);
			theRouter.generateIsochrone(startLocation, waypoint);
			// if (optimize) {
			// Route bestRoute = theRouter.getBestRoute();
			// // - Extract the first part of the route until the first wind change for optimization.
			// Route optimRoute = bestRoute.extractSubRoute(1);
			// optimRoute.optimizeRoute();
			// }
		}
		if (routerType == RouterType.ISOSTEP) {
			// - Check for required parameters that are the start and the waypoint
			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			if (this.ortoAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);

			IsochroneStepRouter theRouter = new IsochroneStepRouter(routerType);
			theRouter.setOutputFile(navigationFile);
			theRouter.setOrtoAngle(ortoAngle);
			theRouter.generateIsochroneStep(startLocation, 3 * 60);

		}
		if (routerType == RouterType.DIRECT) if ((null != startLocation) && (null != endLocation)) {
			final Router theRouter = new Router(routerType);
			final Route directRoute = theRouter.generateRoute(startLocation, endLocation);
			VORGCommandApp.output("Direct Route");
			VORGCommandApp.output(directRoute.printReport());
			VORGCommandApp.output();
		}
		if (routerType == RouterType.OPTIMDIRECT) if ((null != startLocation) && (null != endLocation)) {
			final Router theRouter = new Router(routerType);
			theRouter.optimizeRoute(startLocation, endLocation);
		}
		final Finder theRouter = new Finder();
		if (activeMax) {
			// - Generate the max speed route
			final Route maxSpeedRoute = theRouter.getMaxSpeed(startLocation, cellsToScan);
			// RouteFinder.output("Max Speed Route");
			output(maxSpeedRoute.printReport());
			output();

			// - If the VRTool output file is present, then send the information to the tail of the file.
			if (null != navigationFile) send2VRTool(maxSpeedRoute, "MAX");
		}

		if (activeAWD) {
			// - Generate the AWD route if the AWD parameter is present on the input.
			final Route awdRoute = theRouter.getAWDRoute(startLocation, cellsToScan, maxAWD);
			awdRoute.setName("AWD Route - " + maxAWD);
			// Calendar now = GregorianCalendar.getInstance();
			// output("AWD Route - " + maxAWD + " [" + now.getTime() + "]");
			output(awdRoute.printReport());
			output();

			// - If the VRTool output file is present, then send the information to the tail of the file.
			final String title = "AWD " + maxAWD;
			if (null != navigationFile) send2VRTool(awdRoute, title);
		}
		if (activeVMG) {
			// - Generate the VMG route if requested.
			final Route vmgRoute = theRouter.getVMGRoute(startLocation, cellsToScan, VMGAngle);
			vmgRoute.setName("VMG Route - " + VMGAngle);
			output(vmgRoute.printReport());
			output();

			// - If the VRTool output file is present, then send the information to the tail of the file.
			if (null != navigationFile) send2VRTool(vmgRoute, "VMG " + VMGAngle);
		}
		if (activeSearch) searchSolution();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Scans and process the application arguments. The processing expects that all key arguments start with the
	 * character "-" and that are followed by the exact number of parameters for the key if required.
	 */
	@Override
	public void processParameters(final String[] args, final String appName) {
		logger.finer("Start parameter processing for application: " + appName);
		super.processParameters(args, appName);
		for (int i = 0; i < args.length; i++) {
			// - Get the next argument. It should be a key, if not skip it until next key.
			String argument = args[i];
			// - Store debugger output because the state of the debugging flag is still unknown.
			buffer2debug("Application argument: args[" + i + "] = " + argument);

			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-help")) {
					this.help();
					VORGCommandApp.exit(0);
				}
				if (args[i].toLowerCase().startsWith("-boat")) { //$NON-NLS-1$
					final String boatName = this.argumentStringValue(args, i);
					i++;
					final Boat boat = new Boat();
					try {
						boat.setName(boatName);
						boat.updateBoatData();
						startLocation = boat.getLocation();
						heading = boat.getHeading();
						boatSpeed = boat.getSpeed();
					} catch (final DataLoadingException dle) {
						System.out.println(dle.getMessage());
						VORGCommandApp.exit(VORGConstants.GENERICERROR);
					} catch (final BoatNotFoundException bnfe) {
						System.out.println(bnfe.getMessage());
						VORGCommandApp.exit(VORGConstants.GENERICERROR);
					}
					continue;
				}
				if (args[i].toLowerCase().startsWith("-end")) { //$NON-NLS-1$
					endLocation = new GeoLocation();
					endLocation.setLat(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					endLocation.setLon(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-way")) { //$NON-NLS-1$
					waypoint = new GeoLocation();
					waypoint.setLat(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					waypoint.setLon(this.argumentGeopositionValue(this.argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-orto")) { //$NON-NLS-1$
					ortoAngle = this.argumentDoubleValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-optim")) { //$NON-NLS-1$
					optimize = true;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-VMC")) { //$NON-NLS-1$
					heading = this.argumentIntegerValue(args, i);
					i++;
					printVMC = true;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-VMG")) { //$NON-NLS-1$
					heading = this.argumentIntegerValue(args, i);
					i++;
					windDir = this.argumentIntegerValue(args, i);
					i++;
					windSpeed = this.argumentDoubleValue(args, i);
					i++;
					printVMG = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-max")) { //$NON-NLS-1$
					activeMax = true;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-AWD")) { //$NON-NLS-1$
					maxAWD = argumentIntegerValue(args, i);
					activeAWD = true;
					i++;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-VMG")) { //$NON-NLS-1$
					VMGAngle = argumentIntegerValue(args, i);
					activeVMG = true;
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-search")) { //$NON-NLS-1$
					searchAngle = argumentIntegerValue(args, i);
					activeSearch = true;
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-polar")) { //$NON-NLS-1$
					final int apparent = this.argumentIntegerValue(args, i);
					i++;
					final double windSpeed = this.argumentDoubleValue(args, i);
					i++;
					final SailConfiguration conf = Polars.lookup(apparent, windSpeed);
					final StringBuffer buffer = new StringBuffer("[Polar search results").append('\n');
					buffer.append("Sail=").append(conf.getSail()).append(VORGConstants.NEWLINE);
					buffer.append("Speed=").append(conf.getSpeed()).append(VORGConstants.NEWLINE);
					VORGCommandApp.output(buffer.toString());
					VORGCommandApp.exit(0);
				}
				if (args[i].toLowerCase().startsWith("-direct")) { //$NON-NLS-1$
					onlyDirect = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-deep")) { //$NON-NLS-1$
					final int deepLevel = this.argumentIntegerValue(args, i);
					i++;
					Route.setDeepLevel(deepLevel);
					continue;
				}
				if (args[i].toLowerCase().startsWith("-router")) {
					// - Get the router type. Can be DIRECT or FILE or ISO.
					routerType = RouterType.encode(this.argumentStringValue(args, i));
					i++;
					continue;
				}
			}
		}
	}

	private void banner() {
		VORGCommandApp.output(" ____             _        ____      _            _       _             ");
		VORGCommandApp.output("|  _ \\ ___  _   _| |_ ___ / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
		VORGCommandApp.output("| |_) / _ \\| | | | __/ _ \\ |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
		VORGCommandApp.output("|  _ < (_) | |_| | ||  __/ |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
		VORGCommandApp.output("|_| \\_\\___/ \\__,_|\\__\\___|\\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
		VORGCommandApp.output();
		VORGCommandApp.output(RouteCalculator.VERSION);
		VORGCommandApp.output();
	}

	private void help() {
		System.out.println("Command API for the RouteCalcualtor:");
		System.out.println("java -classpath routecalculator.jar net.sf.vorg.routecalculator.command.main.RouteCalculator ");
		System.out.println("Allowed parameters:");
		System.out.println("-sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-way[point] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println();
		System.out.println("Allowed commands");
		System.out.println();
		System.out.println("Allowed toggles");
		System.out.println("-cells   -- number of levels to process in the VMG routing. Not used.");
		// System.out.println("-wind   -- if present then consider wind shift cell for splitting and recalculation.");
		System.out.println("-debug  -- if defined opens the ouput for verbose debugging information.");
		System.out.println();
		System.out.println("Allowed commands");
		System.out
				.println("-VMC <direction> -- calculates the VMC for the cell that contains the -start location over the parameter direction.");
		System.out
				.println("-VMG <direction> <winddir> <windspeed>  -- calculates the VMC for the parameter values. Does not need a cell location.");
		System.out
				.println("-polar <AWD> <windspeed>  -- calculates the exact polars for this AWD on the selected wind speed.");
		System.out
				.println("-router  -- optimizes a route. The next parameters specify the route and the optimizer operation.");
		System.out.println("    <type>  -- if there is a direct route of a loaded one.");
		// System.out.println("    <deep>  -- number of iterations to process between 10 and 60.");
		// System.out.println("    <iterations> -- number of thousands of iterations to descend a deep level.");
		System.out.println();
		System.out.println("Description:");
		System.out.println("   Utility to generate a route from a start point or from the current location of a boat");
		System.out.println("   following the max speed route or the AWD for the angle specified.");
		System.out.println();
		System.out.println("Command API for the RouteFinder:");
		System.out.println("   java -classpath routecalculator.jar main.RouteScanner ");
		System.out.println("Allowed parameters:");
		System.out.println("-sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-boat <boat name>   -- name of the game boat where to extract the current location");
		System.out.println("-nav <.NAV file path>  -- path to the VRTool file where to store the results");
		System.out.println("-scan <number>  -- number of cells to route");
		System.out.println("-AWD <number>  -- AWD angle to follow in the alternative route");
		System.out.println("Allowed toggles");
		System.out.println("-port -- gets the route using the port side angle");
		System.out.println();
	}

	private void searchSolution() {
		try {
			final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
			nf.setMaximumFractionDigits(6);
			nf.setMinimumFractionDigits(6);
			final Finder theRouter = new Finder();
			int searchVMGAngle = searchAngle;
			final WindCell boatCell = WindMapHandler.getWindCell(startLocation);
			final VMCData vmc = new VMCData(searchVMGAngle, boatCell);
			final int bestAngle = vmc.getBestAngle();
			final int searchAWD = GeoLocation.calculateAWD(boatCell.getWindDir(), bestAngle);
			StringBuffer buffer = new StringBuffer();
			buffer.append("Search List for VMG\n");
			buffer.append("Name\tStart l\tStart L\tEnd l\tEnd L\tAngle\tDistance\tDiff Lat\tDiff Lon\n");
			double maxDistance = 0.0;
			final Vector<Route> records = new Vector<Route>();
			// - Search in the VMG list
			for (int i = -10; i < 11; i++) {
				searchVMGAngle = searchAngle + i;
				final Route vmgRoute = theRouter.getVMGRoute(startLocation, 15, searchVMGAngle);
				vmgRoute.setName("VMG Route - " + searchVMGAngle);
				buffer.append("VMG " + searchVMGAngle).append("\t");
				buffer.append(startLocation.toReport()).append("\t");
				// buffer.append(startLocation.getLat()).append("\t").append(startLocation.getLon()).append("\t");
				final GeoLocation target = vmgRoute.getWindChange(2);
				if (null != target) {
					buffer.append(target.toReport()).append("\t");
					// buffer.append(target.getLat()).append("\t").append(target.getLon()).append("\t");
					final double angle = startLocation.angleTo(target);
					buffer.append(angle).append("\t");
					final double distance = startLocation.distance(target);
					buffer.append(distance).append("\t");
					buffer.append(nf.format(target.getLat() - startLocation.getLat())).append("\t");
					buffer.append(nf.format(target.getLon() - startLocation.getLon())).append("\n");
					if (distance > maxDistance) {
						maxDistance = distance;
						records.add(0, vmgRoute);
					}
				} else
					buffer.append("\n");
			}
			output(buffer.toString());

			// - Search in the AWD list
			buffer = new StringBuffer();
			buffer.append("\nSearch List for AWD\n");
			buffer.append("Name\tStart l\tStart L\tEnd l\tEnd L\tAngle\tDistance\tDiff Lat\tDiff Lon\n");
			for (int i = -10; i < 11; i++) {
				final int searchAWDAngle = searchAWD + i;
				final Route awdRoute = theRouter.getAWDRoute(startLocation, 15, searchAWDAngle);
				buffer.append("AWD " + searchAWDAngle).append("\t");
				buffer.append(startLocation.toReport()).append("\t");
				// buffer.append(startLocation.getLat()).append("\t").append(startLocation.getLon()).append("\t");
				final GeoLocation target = awdRoute.getWindChange(2);
				if (null != target) {
					buffer.append(target.toReport()).append("\t");
					// buffer.append(target.getLat()).append("\t").append(target.getLon()).append("\t");
					final double angle = startLocation.angleTo(target);
					buffer.append(angle).append("\t");
					final double distance = startLocation.distance(target);
					buffer.append(distance).append("\t");
					buffer.append(nf.format(target.getLat() - startLocation.getLat())).append("\t");
					buffer.append(nf.format(target.getLon() - startLocation.getLon())).append("\n");
					if (distance > maxDistance) {
						maxDistance = distance;
						records.add(0, awdRoute);
					}
				} else
					buffer.append("\n");
			}
			output(buffer.toString());

			// - Output the two more long routes
			output(records.get(0).printReport());
			output(records.get(1).printReport());
		} catch (final LocationNotInMap e) {
			e.printStackTrace();
		}
	}

	private void send2VRTool(final Route awdRoute, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(navigationFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CCFF").append(maxAWD).append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			buffer.append(awdRoute.vrtoolReport());
			buffer.append(VORGConstants.NEWLINE);
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
// - UNUSED CODE ............................................................................................
