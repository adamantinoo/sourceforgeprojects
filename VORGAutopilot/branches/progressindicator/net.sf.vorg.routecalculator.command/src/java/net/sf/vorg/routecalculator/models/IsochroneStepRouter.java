//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.RouterType;

// - CLASS IMPLEMENTATION ...................................................................................
public class IsochroneStepRouter extends IsochroneRouter {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger							logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	// private TimedRoute route;
	private Vector<NamedGeoLocation>	isochrone;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsochroneStepRouter(final RouterType type) {
		super(type);
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public void generateIsochroneStep(GeoLocation startLocation, int runMinutes) {
		isochrone = new Vector<NamedGeoLocation>(50);

		// - Generate the routes using the Time route generator
		for (int deviation = -45; deviation <= 45; deviation++) {
			final int course = new Double(this.ortoAngle + deviation).intValue();
			TimedRoute route = new TimedRoute();
			route.generateRoute(startLocation, runMinutes, course);
			route.setName("ISO STEP - " + course);
			if (null != route.getRouteEnd()) {
				System.out.println(route.printReport());
				NamedGeoLocation endLocation = new NamedGeoLocation(route.getRouteEnd());
				endLocation.setName("ISO STEP - " + course);
				isochrone.add(endLocation);
			}
		}
		if (null != outputFile) {
			send2VRTool(isochrone, "ISO STEP " + new Double(ortoAngle).intValue());
		}
	}
}

// - UNUSED CODE ............................................................................................
