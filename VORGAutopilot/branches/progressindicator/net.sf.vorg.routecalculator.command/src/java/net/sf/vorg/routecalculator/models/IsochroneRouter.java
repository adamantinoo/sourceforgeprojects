//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.app.VORGCommandApp;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.core.RouterType;

// - CLASS IMPLEMENTATION ...................................................................................
public class IsochroneRouter extends Router {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger			= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected String			outputFile	= null;
	private Route					firstRoute;

	protected double			ortoAngle;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsochroneRouter(final RouterType type) {
		super(type);
	}

	/**
	 * With the start point and the final waypoint, this method calculates the routes for the different VMGs to
	 * both sides of the resulting angle. Those routes are divided on the wind change points and those points
	 * are recorded to remember the route that has advanced more in the general direction of the start-waypoint
	 * angle.
	 */
	public void generateIsochrone(final GeoLocation start, final GeoLocation waypoint) {
		// - Calculate the base angle
		double baseCourse = start.angleTo(waypoint);
		if (ortoAngle >= 0.0) baseCourse = ortoAngle;
		final Vector<NamedGeoLocation> isochrone1 = new Vector<NamedGeoLocation>(50);
		final Vector<NamedGeoLocation> isochrone2 = new Vector<NamedGeoLocation>(50);
		double maxFirstDistance = 0.0;
		firstRoute = null;
		double maxSecondDistance = 0.0;
		Route secondRoute = null;

		// - Generate the routes using another Router
		for (int deviation = -45; deviation <= 45; deviation++) {
			// if (activeVMG) {
			// - Generate the VMG route if requested.
			final Finder theRouter = new Finder();
			final int course = new Double(baseCourse + deviation).intValue();
			final Route vmgRoute = theRouter.getVMGRoute(start, 30, course);
			vmgRoute.setName("VMG Route - " + course);

			// - Get the first and second wind change locations
			final GeoLocation firstLocation = vmgRoute.getWindChange(1);
			final GeoLocation secondLocation = vmgRoute.getWindChange(2);
			if (null != firstLocation) {
				NamedGeoLocation location = new NamedGeoLocation(firstLocation);
				location.setName("ISO STEP - " + course);
				isochrone1.add(location);
			}
			if (null != secondLocation) {
				NamedGeoLocation location = new NamedGeoLocation(secondLocation);
				location.setName("ISO STEP - " + course);
				isochrone2.add(location);
			}

			// - Calculate the distances from the start to this points to get the isochrones.
			double destAngle = start.angleTo(firstLocation);
			double destDistance = start.distance(firstLocation);
			double projectionAngle = Math.abs(destAngle - baseCourse);
			double projectedDistance = destDistance * Math.cos(Math.toRadians(projectionAngle));
			if (projectedDistance > maxFirstDistance) {
				maxFirstDistance = projectedDistance;
				firstRoute = vmgRoute;
			}
			destAngle = start.angleTo(secondLocation);
			destDistance = start.distance(secondLocation);
			projectionAngle = Math.abs(destAngle - baseCourse);
			projectedDistance = destDistance * Math.cos(Math.toRadians(projectionAngle));
			if (projectedDistance > maxSecondDistance) {
				maxSecondDistance = projectedDistance;
				secondRoute = vmgRoute;
			}
		}
		VORGCommandApp.output(firstRoute.printReport());
		VORGCommandApp.output(secondRoute.printReport());
		// - Send the collected data to the NAV file
		if (null != outputFile) {
			this.send2VRTool(isochrone1, "ISO1 " + new Double(baseCourse).intValue());
			this.send2VRTool(isochrone2, "ISO2 " + new Double(baseCourse).intValue());
			firstRoute.setName(firstRoute.getName() + " FIRST");
			secondRoute.setName(secondRoute.getName() + " SECOND");
			if (null != firstRoute) this.send2VRTool(firstRoute, firstRoute.getName());
			if (null != secondRoute) this.send2VRTool(secondRoute, secondRoute.getName());
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// public void generateIsochrone(final SpeedVector vector) {
	// this.generateIsochroneData(vector, 1.0 * 60.0, "ISO+1h");
	// this.generateIsochroneData(vector, 2.0 * 60.0, "ISO+2h");
	// this.generateIsochroneData(vector, 3.0 * 60.0, "ISO+3h");
	// }

	public Route getBestRoute() {
		return firstRoute;
	}

	// public String vrtoolReport(GeoLocation point) {
	// final StringBuffer buffer = new StringBuffer("P; ");
	// buffer.append(FormatSingletons.nf4.format(point.getLat())).append(";");
	// buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
	// return buffer.toString();
	// }

	public void setOrtoAngle(final double angle) {
		ortoAngle = angle;
	}

	public void setOutputFile(final String outputFile) {
		this.outputFile = outputFile;
	}

	// private void generateIsochroneData(final SpeedVector point, final double time, final String name) {
	// final Vector<GeoLocation> isochrone = new Vector<GeoLocation>(50);
	// // GeoLocation target = new GeoLocation();
	// // - Iterate for 50 degrees, 25 each side of heading
	// for (int deviation = -60; deviation <= 60; deviation++) {
	// final int course = GeoLocation.adjustAngleTo360(point.getHeading() + deviation);
	// // double maxSpeed = 0.0;
	// try {
	// final WindCell cell = WindMapHandler.getWindCell(point.getLocation());
	// final SailConfiguration polar = Polars.lookup(GeoLocation.calculateAWD(cell.getWindDir(), course), cell
	// .getWindSpeed());
	// final GeoLocation newLocation = point.getLocation().directEstimation(time * 60.0, polar.getSpeed(),
	// course);
	// // if (polar.getSpeed() > maxSpeed) {
	// // target = newLocation;
	// // maxSpeed = polar.getSpeed();
	// // }
	// isochrone.add(newLocation);
	// } catch (final LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// // - Send the collected data to the NAV file
	// if (null != outputFile) this.send2VRTool(isochrone, name);
	// // return target;
	// }

	private void send2VRTool(final Route awdRoute, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CCFF").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			buffer.append(awdRoute.vrtoolReport());
			buffer.append(VORGConstants.NEWLINE);
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void send2VRTool(final Vector<NamedGeoLocation> isochrone, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			for (final NamedGeoLocation point : isochrone) {
				buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLat())).append(";");
				buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
				buffer.append(point.getName());
				buffer.append(VORGConstants.NEWLINE);
			}
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// private void send2VRTool(final Vector<GeoLocation> isochrone, final String title) {
	// try {
	// final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));
	//
	// // - Compose the report.
	// final StringBuffer buffer = new StringBuffer();
	// buffer.append(VORGConstants.NEWLINE);
	// buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
	// buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
	// buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
	// buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
	// buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
	// buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
	// for (final GeoLocation point : isochrone) {
	// buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLat())).append(";");
	// buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
	// buffer.append(VORGConstants.NEWLINE);
	// }
	// writer.write(buffer.toString());
	// writer.close();
	// } catch (final IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
}

// - UNUSED CODE ............................................................................................
