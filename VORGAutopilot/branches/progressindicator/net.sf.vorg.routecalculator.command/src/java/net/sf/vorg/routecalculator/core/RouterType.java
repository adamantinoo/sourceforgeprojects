//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum RouterType {
	NONE, DIRECT, FILE, OPTIMDIRECT, OPTIMFILE, ISO, ISOSTEP;

	public static RouterType encode(String argument) {
		if (argument.toUpperCase().equals("NONE")) return NONE;
		if (argument.toUpperCase().equals("DIRECT")) return DIRECT;
		if (argument.toUpperCase().equals("FILE")) return FILE;
		if (argument.toUpperCase().equals("OPTIMDIRECT")) return OPTIMDIRECT;
		if (argument.toUpperCase().equals("OPTIMFILE")) return OPTIMFILE;
		if (argument.toUpperCase().startsWith("ISOSTEP")) return ISOSTEP;
		if (argument.toUpperCase().startsWith("ISO")) return ISO;
		return NONE;
	}
}
// - UNUSED CODE ............................................................................................
