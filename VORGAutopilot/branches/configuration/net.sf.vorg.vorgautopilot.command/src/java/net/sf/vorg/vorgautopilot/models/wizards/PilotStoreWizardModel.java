//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models.wizards;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.enums.InputTypes;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;
import net.sf.vorg.vorgautopilot.parsers.HTTPInputParser;
import net.sf.vorg.vorgautopilot.parsers.UIVRToolNavParser;
import net.sf.vorg.vorgautopilot.parsers.XMLFileParser;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotStoreWizardModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final PilotModelStore	target;
	private int										refreshInterval;
	private int										timeDeviation;
	private String								inputLocation;
	private InputTypes						inputType;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotStoreWizardModel(final PilotModelStore targetDestination) {
		//- Copy target model data to this internal structure for publishing.
		target = targetDestination;
		//		refreshInterval = target.getRefreshInterval();
		//		timeDeviation = target.getTimeDeviation();
		//		inputLocation = target.getFilePath();
		//		inputType = target.getInputType();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getInputPath() {
		return inputLocation;
	}

	public int getRefreshInterval() {
		return refreshInterval;
	}

	public int getTimeDeviation() {
		return timeDeviation;
	}

	public void setInputPath(final String newPath) {
		inputLocation = newPath;
		//		if (null != newPath) {
		//			//- Detect the file type from the file extension.
		//			//			String extension = getExtension(newPath);
		//			if (newPath.toLowerCase().endsWith(".nav")) {
		//				target.setInputHandler(new VRToolNavParser(newPath));
		//				return;
		//			}
		//			if (newPath.toLowerCase().endsWith(".xml")) {
		//				target.setInputHandler(new XMLFileParser(newPath));
		//				return;
		//			}
		//			//- The selected file is not valid. Reset the path back to null
		//			PilotStoreWizardModel.logger.finer("The user selected configuration file is not valid. Cancelled.");
		//		}
	}

	public void setInputType(final InputTypes newType) {
		inputType = newType;
	}

	public void setRefreshInterval(final int refreshTime) {
		refreshInterval = refreshTime;
	}

	//	private String getExtension(String newPath) {
	//		int point = newPath.indexOf('.');
	//		newPath.endsWith(suffix)
	//		if (point > 0)
	//			return newPath.substring(point);
	//		else
	//			return "NONE";
	//	}

	public void setTimeDeviation(final int seconds) {
		timeDeviation = seconds;
	}

	public void updateTarget() {
		target.setRefreshInterval(refreshInterval);
		target.setTimeDeviation(timeDeviation);
		//-  Set the InputHandler
		if (null != inputLocation) {
			if (inputType == InputTypes.HTTP) {
				target.setInputHandler(new HTTPInputParser(inputLocation));
			}
			if (inputType == InputTypes.NAV) {
				target.setInputHandler(new UIVRToolNavParser(inputLocation));
			}
			if (inputType == InputTypes.XML) {
				target.setInputHandler(new XMLFileParser(inputLocation));
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
