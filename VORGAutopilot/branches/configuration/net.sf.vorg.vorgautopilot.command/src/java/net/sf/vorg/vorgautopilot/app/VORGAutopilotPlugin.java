//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.app;

// - IMPORT SECTION .........................................................................................
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import main.VORGAutopilot;
import net.sf.vorg.core.VORGConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGAutopilotPlugin implements IApplication {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger								logger					= Logger.getLogger("net.sf.vorg.vorgautopilot");
	//	public static final String		VORG											= "VORG";
	//
	//	public static final Font			FONT_GROUPNAME						= new Font(Display.getDefault(), "Tahoma", 10, SWT.NORMAL);
	//	public static final Color			COLOR_BRILLIANT_BLUE			= new Color(Display.getDefault(), 0x00, 0x00, 0xFF);

	//	//- Property names and references
	//	public static final String		PLUGIN_ID									= "net.sf.vorg.core.app.VORGCorePlugin";										//$NON-NLS-1$
	//	public static final String		PREFIX										= VORGAutopilotPlugin.PLUGIN_ID + ".";													//$NON-NLS-1$
	//	public static final String		FIRST_WIND_CHANGE					= VORGAutopilotPlugin.PREFIX + "first_wind_change";
	//	public static final String		WIND_CHANGE_COUNT					= VORGAutopilotPlugin.PREFIX + "wind_change_count";
	//	public static final String		LIMIT_SPEED								= VORGAutopilotPlugin.PREFIX + "limit_speed";
	//	public static final String		POLAR_DATABASE_ID					= VORGAutopilotPlugin.PREFIX + "polar_database_id";
	//	public static final String		WINDPOINT_ANGLE						= VORGAutopilotPlugin.PREFIX + "windpoint_angle";
	//	public static final String		WINDMAP2GMP_DISPLACEMENT	= VORGAutopilotPlugin.PREFIX + "windmap2gmt_displacement";
	//	public static final String		WINDMAP_HOST							= VORGAutopilotPlugin.PREFIX + "windmap_host";
	//	public static final String		WINDMAP_PREFIX						= VORGAutopilotPlugin.PREFIX + "windmap_prefix";
	//	public static final String		PREFERENCE_PAGE_CONTEXT		= PREFIX + "preference_page_context";											//$NON-NLS-1$

	private static VORGAutopilotPlugin	pluginReference	= null;
	private static VORGAutopilot				singleton;

	public static IApplication getPlugin() {
		VORGAutopilotPlugin ref = VORGAutopilotPlugin.pluginReference;
		return VORGAutopilotPlugin.pluginReference;
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGAutopilotPlugin() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	public void start(final BundleContext context) throws Exception {
	//		super.start(context);
	//		VORGAutopilotPlugin.pluginReference = this;
	//	}
	//
	//	@Override
	//	public void stop(final BundleContext context) throws Exception {
	//		VORGAutopilotPlugin.pluginReference = null;
	//		super.stop(context);
	//	}

	public Object start(IApplicationContext context) throws Exception {
		//- Get access to the application arguments.
		Map<String, Object> contextArguments = context.getArguments();
		Object argsData = contextArguments.get("application.args");
		if (null == argsData) VORGAutopilot.exit(VORGConstants.NOCONFIG);
		if (argsData instanceof String[]) {
			String[] args = (String[]) argsData;
			singleton = new VORGAutopilot(args);
			singleton.execute();
		}

		//- Code from the Main method of the former command line application
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		//exit(0);

		VORGAutopilotPlugin.pluginReference = this;
		return null;
	}

	public void stop() {
		VORGAutopilotPlugin.pluginReference = null;
	}

	//	private void processParameters(IApplicationContext context) {
	//		//- Get access to the application arguments.
	//		Map<String, Object> contextArguments = context.getArguments();
	//		Object argsData = contextArguments.get("application.args");
	//		if (null == argsData) VORGAutopilot.exit(VORGConstants.NOCONFIG);
	//		if (argsData instanceof String[]) {
	//			String[] args = (String[]) argsData;
	//			//		Iterator<String> ait = args.keySet().iterator();
	//			for (int i = 0; i < args.length; i++) {
	//				//		while (ait.hasNext()) {
	//				//			String argumentName = ait.next();
	//
	//				//		}
	//				//		for (int i = 0; i < args.length; i++) {
	//				logger.info("Processing aplication argument: " + args[i]);
	//				// - Test all parameters that start with the character '-'. They are the parameter codes
	//				if (args[i].startsWith("-")) {
	//					if (args[i].toLowerCase().startsWith("-conf")) {
	//						// - Get and open the file with the autopilot configuration
	//						VORGAutopilot.configurationFileName = argumentStringValue(args, i);
	//						VORGAutopilot.activateXMLFile = true;
	//					}
	//					if (args[i].toLowerCase().startsWith("-nav")) {
	//						// - Get and open the file with the autopilot configuration
	//						VORGAutopilot.configurationFileName = argumentStringValue(args, i);
	//						VORGAutopilot.activateVRTool = true;
	//					}
	//					if (args[i].toLowerCase().startsWith("-url")) {
	//						// - Get and open the file with the autopilot configuration
	//						VORGAutopilot.configurationFileName = argumentStringValue(args, i);
	//						VORGAutopilot.activateHTTPResource = true;
	//					}
	//
	//					//				if (args[i].toLowerCase().startsWith("-refr")) { //$NON-NLS-1$
	//					//					VORGAutopilot.refresh = argumentIntegerValue(args, i);
	//					//					i++;
	//					//					continue;
	//					//				}
	//					//				if (args[i].toLowerCase().startsWith("-time")) { //$NON-NLS-1$
	//					//					VORGAutopilot.timeDelay = argumentIntegerValue(args, i);
	//					//					i++;
	//					//					continue;
	//					//				}
	//					//				if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
	//					//					VORGAutopilot.onDebug = true;
	//					//					VORGAutopilot.logger.setLevel(Level.ALL);
	//					//					continue;
	//					//				}
	//					if (args[i].toLowerCase().startsWith("-help")) { //$NON-NLS-1$
	//						VORGAutopilot.help();
	//						VORGAutopilot.exit(0);
	//					}
	//					//			} else {
	//					//				Object value = args.get(argumentName);
	//					//				int d = 0;
	//				}
	//			}
	//			// ... Check that required parameters have values.
	//			if (null == VORGAutopilot.configurationFileName) {
	//				VORGAutopilot.exit(VORGConstants.NOCONFIG);
	//			}
	//		}
	//	}
	//
	//	protected String argumentStringValue(String argumentValue) {
	//		// - Check argument array size before trying to get the argument value
	//		if (null == argumentValue) {
	//			// - Exit point 10. There are no enough arguments in the list to find a value.
	//			VORGAutopilot.exit(VORGConstants.NOCONFIG);
	//		}
	//		return argumentValue;
	//	}
	//
	//	protected String argumentStringValue(final String[] args, final int position) {
	//		// - Check argument array size before trying to get the argument value
	//		if (position + 1 < args.length)
	//			return args[position + 1];
	//		else {
	//			// - Exit point 10. There are no enough arguments in the list to find a value.
	//			VORGAutopilot.exit(VORGConstants.NOCONFIG);
	//		}
	//		return "";
	//	}
}
// - UNUSED CODE ............................................................................................
