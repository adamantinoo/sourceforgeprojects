//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

//- INTERFACE IMPLEMENTATION ...............................................................................
public interface PilotUIConstants {
	//- G L O B A L - S E C T I O N ..........................................................................
	//- D E F I N I T I O N   C O L O R S
	//	Color	COLOR_SHIP_NAME							= new Color(Display.getDefault(), 0, 0, 0);

	//- D E F I N I T I O N   C O L O R S
	Color	COLOR_STANDARD_RED					= ColorConstants.red;
	Color	COLOR_STANDARD_ORANGE				= ColorConstants.orange;
	Color	COLOR_STANDARD_DARKBLUE			= ColorConstants.darkBlue;
	Color	COLOR_STANDARD_BLUE					= ColorConstants.blue;
	Color	COLOR_STANDARD_DARKGREN			= ColorConstants.darkGreen;
	Color	COLOR_STANDARD_GREEN				= ColorConstants.green;
	Color	COLOR_BLACK									= new Color(Display.getDefault(), 0, 0, 0);
	Color	COLOR_WHITE									= new Color(Display.getDefault(), 255, 255, 255);

	Color	COLOR_BROWN									= new Color(Display.getCurrent(), 204, 102, 0);

	Color	COLOR_BRILLIANT_RED					= new Color(Display.getDefault(), 0xFF, 0x33, 0x00);
	Color	COLOR_LIGHT_RED							= new Color(Display.getDefault(), 0xFF, 0x80, 0x80);
	Color	COLOR_MEDIUM_RED						= new Color(Display.getDefault(), 0xE0, 0x33, 0x00);
	Color	COLOR_DARK_RED							= new Color(Display.getDefault(), 0x33, 0x00, 0x00);
	Color	COLOR_BRILLIANT_GREEN				= new Color(Display.getDefault(), 0x00, 0xFF, 0x00);
	Color	COLOR_LIGHT_GREEN						= new Color(Display.getDefault(), 0x80, 0xFF, 0x80);
	Color	COLOR_MEDIUM_GREEN					= new Color(Display.getDefault(), 0x00, 0xE0, 0x00);
	Color	COLOR_DARK_GREEN						= new Color(Display.getDefault(), 0x00, 0xA0, 0x00);
	Color	COLOR_BRILLIANT_BLUE				= new Color(Display.getDefault(), 0x00, 0x00, 0xFF);
	Color	COLOR_LIGHT_BLUE						= new Color(Display.getDefault(), 0x80, 0x80, 0xFF);
	Color	COLOR_MEDIUM_BLUE						= new Color(Display.getDefault(), 0x00, 0x10, 0xE0);
	Color	COLOR_DARK_BLUE							= new Color(Display.getDefault(), 0x00, 0x00, 0x33);
	Color	COLOR_BRILLIANT_YELLOW			= new Color(Display.getDefault(), 0xFF, 0xFF, 0x00);
	Color	COLOR_LIGHTEST_YELLOW				= new Color(Display.getDefault(), 0xFE, 0xFE, 0xEE);
	Color	COLOR_LIGHT_YELLOW					= new Color(Display.getDefault(), 0x80, 0x80, 0x00);
	Color	COLOR_MEDIUM_YELLOW					= new Color(Display.getDefault(), 0xE0, 0xE0, 0x00);
	Color	COLOR_DARK_YELLOW						= new Color(Display.getDefault(), 0xF8, 0xBB, 0x05);

	Color	COLOR_DARK_GRAY							= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);
	Color	COLOR_GRAY									= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);
	Color	COLOR_LIGHT_GRAY						= ColorConstants.lightGray;
	Color	COLOR_LIGHTEST_GRAY					= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);

	//- F O N T   C O L O R S
	Color	COLOR_GROUPNAME							= new Color(Display.getDefault(), 147, 122, 243);
	Color	COLOR_NAMES									= new Color(Display.getDefault(), 128, 128, 255);
	Color	COLOR_FIELDNAMES						= new Color(Display.getDefault(), 49, 174, 13);

	//- B A C K G R O U N D   C O L O R S
	Color	COLOR_READY									= new Color(Display.getDefault(), 0xD1, 0xFC, 0xD7);
	Color	COLOR_NOTREADY							= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
	Color	COLOR_CONFIGURED						= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
	Color	COLOR_ONHOLD								= new Color(Display.getDefault(), 206, 206, 206);
	Color	COLOR_WAYPOINT_ACTIVE				= new Color(Display.getDefault(), 209, 214, 252);
	Color	COLOR_WAYPOINT_SURPASSED		= new Color(Display.getDefault(), 255, 219, 170);
	Color	COLOR_WAYPOINT_NOACTION			= COLOR_READY;

	//- B A C K G R O U N D   C O L O R S
	Color	COLOR_BACKGROUND_MAP				= ColorConstants.white;
	Color	COLOR_BACKGROUND_VIEW				= ColorConstants.white;
	Color	COLOR_TIP										= ColorConstants.tooltipBackground;
	Color	COLOR_UNDEFINED_BACKGROUND	= COLOR_LIGHTEST_YELLOW;
	Color	COLOR_FRIEND_BACKGROUND			= new Color(Display.getDefault(), 0xD1, 0xFC, 0xD7);
	Color	COLOR_FOE_BACKGROUND				= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
	Color	COLOR_NEUTRAL_BACKGROUND		= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);

	//- F O N T S
	Font	FONT_DEFAULT								= new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL);
	//	Font		FONT_MAP_DEFAULT						= new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL);
	//	Font		FONT_MAP_BOLD								= new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD);
	Font	FONT_BOATNAME								= new Font(Display.getDefault(), "Arial", 14, SWT.NORMAL);
	Font	FONT_GROUPNAME							= new Font(Display.getDefault(), "Tahoma", 9, SWT.BOLD);

	//	//- G L O B A L   C O N S T A N T S
	//	int			DEFAULT_ZOOM_FACTOR					= 10;
	//	int			SIZE_ICONIMAGE							= 16;
	//
	//	// - I N F O R M A T I O N - R A C E S
	//	int			RACE_DEFAULT								= 101;
	//	int			RACE_CYBORG									= 106;
	//	int			RACE_EVILEMPIRE							= 108;

	//	//- E N U M E R A T E D   C O N S T A N T S
	//	String	MODELTYPE_BASE							= "MODELTYPE_BASE";
	//	String	MODELTYPE_PLANET						= "MODELTYPE_PLANET";
	//	String	MODELTYPE_SHIP							= "MODELTYPE_SHIP";
	//	String	MODELTYPE_THING							= "TYPE_THING";
	//	String	MODELTYPE_POD								= "MODELTYPE_POD";
	//	String	MODELTYPE_WING							= "MODELTYPE_WING";
	//	String	MODELTYPE_GAMER							= "MODELTYPE_GAMER";
	//
	//	// - I N F O R M A T I O N - L E V E L S
	//	String	INFOLEVEL_UNEXPLORED				= "INFOLEVEL_UNEXPLORED";
	//	String	INFOLEVEL_NOINFO						= "INFOLEVEL_NOINFO";
	//	String	INFOLEVEL_INFO							= "INFOLEVEL_INFO";
	//	String	INFOLEVEL_OBSOLETE					= "INFOLEVEL_OBSOLETE";
	//	String	INFOLEVEL_NATIVES						= "INFOLEVEL_NATIVES";
	//	String	FFSTATUS_UNDEFINED					= "FFSTATUS_UNDEFINED";
	//	String	FFSTATUS_FRIEND							= "FFSTATUS_FRIEND";
	//	String	FFSTATUS_NEUTRAL						= "FFSTATUS_NEUTRAL";
	//	String	FFSTATUS_FOE								= "FFSTATUS_FOE";
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
