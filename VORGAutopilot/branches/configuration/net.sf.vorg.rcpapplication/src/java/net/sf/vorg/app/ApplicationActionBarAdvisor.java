//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: Application.java 184 2008-09-25 16:01:50Z boneymen $
//  LAST UPDATE:    $Date: 2008-09-25 18:01:50 +0200 (jue, 25 sep 2008) $
//  RELEASE:        $Revision: 184 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

// - CLASS IMPLEMENTATION ...................................................................................
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	//	private IWorkbenchAction	newAction;
	//	private IWorkbenchAction	closeAction;
	//	private IWorkbenchAction	closeAllAction;
	//	private IWorkbenchAction	saveAction;
	//	private IWorkbenchAction	saveAsAction;
	//	private IWorkbenchAction	saveAllAction;
	//	private IWorkbenchAction	printAction;
	//	private IWorkbenchAction	moveAction;
	//	private IWorkbenchAction	renameAction;
	//	private IWorkbenchAction	refreshAction;
	//	private IWorkbenchAction	propertiesAction;
	private IWorkbenchAction	exitAction;
	//	private IWorkbenchAction	undoAction;
	//	private IWorkbenchAction	redoAction;
	//	private IWorkbenchAction	cutAction;
	//	private IWorkbenchAction	copyAction;
	//	private IWorkbenchAction	pasteAction;
	//	private IWorkbenchAction	selectAllAction;
	private IWorkbenchAction	closePerspectiveAction;
	private IWorkbenchAction	closeAllPerspectivesAction;
	private IWorkbenchAction	aboutAction;
	private IWorkbenchAction	preferencesAction;
	private IContributionItem	viewsList;
	private IContributionItem	perspectivesList;
	private IWorkbenchAction	introAction;

	private IWorkbenchAction	showHelpAction;						// NEW
	private IWorkbenchAction	searchHelpAction;					// NEW
	private IWorkbenchAction	dynamicHelpAction;					// NEW

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ApplicationActionBarAdvisor(final IActionBarConfigurer configurer) {
		super(configurer);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Populates the Menu Bar
	 */
	@Override
	protected void fillMenuBar(final IMenuManager menuBar) {
		final MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE); //$NON-NLS-1$
		final MenuManager windowMenu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW); //$NON-NLS-1$
		final MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP); //$NON-NLS-1$

		//- Adding menus
		menuBar.add(fileMenu);
		//- Add a group marker indicating where action set menus will appear.
		menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menuBar.add(windowMenu);
		menuBar.add(helpMenu);

		//- Populating File Menu
		fileMenu.add(exitAction);

		//- Populating Window Menu
		final MenuManager perspectiveMenu = new MenuManager("Open Perspective", "openPerspective");
		perspectiveMenu.add(perspectivesList);
		windowMenu.add(perspectiveMenu);
		final MenuManager viewMenu = new MenuManager("Show View");
		viewMenu.add(viewsList);
		windowMenu.add(viewMenu);
		windowMenu.add(new Separator());
		windowMenu.add(closePerspectiveAction);
		windowMenu.add(closeAllPerspectivesAction);
		windowMenu.add(new Separator());
		windowMenu.add(preferencesAction);

		//- Populating Help Menu
		helpMenu.add(introAction);
		helpMenu.add(aboutAction);
		helpMenu.add(showHelpAction); // NEW
		helpMenu.add(searchHelpAction); // NEW
		helpMenu.add(dynamicHelpAction); // NEW
	}

	/**
	 * Creates the actions and registers them. Registering is needed to ensure that key bindings work. The
	 * corresponding commands keybindings are defined in the plugin.xml file. Registering also provides
	 * automatic disposal of the actions when the window is closed.
	 */
	@Override
	protected void makeActions(final IWorkbenchWindow window) {
		//		//TODO Create our applications actions and register them.
		//		newAction = new NewVGAP4FileAction(window);
		//		register(newAction);
		//		newAction.setText("New...");

		// - Creates the default common actions and registers them.
		exitAction = ActionFactory.QUIT.create(window);
		//		exitAction.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(VAPApplication.PLUGIN_ID, ImageKeys.EXIT));
		register(exitAction);

		closePerspectiveAction = ActionFactory.CLOSE_PERSPECTIVE.create(window);
		register(closePerspectiveAction);
		closeAllPerspectivesAction = ActionFactory.CLOSE_ALL_PERSPECTIVES.create(window);
		register(closeAllPerspectivesAction);

		aboutAction = ActionFactory.ABOUT.create(window);
		//		aboutAction.setImageDescriptor(AbstractUIPlugin
		//				.imageDescriptorFromPlugin(VAPApplication.PLUGIN_ID, ImageKeys.ABOUT));
		register(aboutAction);
		preferencesAction = ActionFactory.PREFERENCES.create(window);
		//		preferencesAction.setImageDescriptor(AbstractUIPlugin.imageDescriptorFromPlugin(VAPApplication.PLUGIN_ID,
		//				ImageKeys.SHOW_PREFERENCES));
		register(preferencesAction);
		viewsList = ContributionItemFactory.VIEWS_SHORTLIST.create(window);
		perspectivesList = ContributionItemFactory.PERSPECTIVES_SHORTLIST.create(window);

		introAction = ActionFactory.INTRO.create(window);
		//		introAction.setImageDescriptor(AbstractUIPlugin
		//				.imageDescriptorFromPlugin(VAPApplication.PLUGIN_ID, ImageKeys.INTRO));
		register(introAction);

		showHelpAction = ActionFactory.HELP_CONTENTS.create(window); // NEW
		register(showHelpAction); // NEW
		searchHelpAction = ActionFactory.HELP_SEARCH.create(window); // NEW
		register(searchHelpAction); // NEW
		dynamicHelpAction = ActionFactory.DYNAMIC_HELP.create(window); // NEW
		register(dynamicHelpAction); // NEW

		// - My user interface additions
	}
}
// - UNUSED CODE ............................................................................................
