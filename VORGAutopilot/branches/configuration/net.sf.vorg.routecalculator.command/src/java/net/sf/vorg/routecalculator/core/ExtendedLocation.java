//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
public class ExtendedLocation extends GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ExtendedLocation(final double lat, final double lon) {
		super(lat, lon);
	}

	public ExtendedLocation(GeoLocation location) {
		super(location.getLat(), location.getLon());
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public VMCData getVMC(final WindCell startCell, final double heading) {
		return this.getVMC(startCell, new Double(heading).intValue());
	}

	public VMCData getVMC(final WindCell startCell, final int heading) {
		// final VMCData vmc = new VMCData(heading, startCell);
		return new VMCData(heading, startCell);
		// double leftVMC = -1.0;
		// double rightVMC = -1.0;
		// double leftBoatSpeed = -1.0;
		// double rightBoatSpeed = -1.0;
		// int leftAngle = 0;
		// int rightAngle = 0;
		// final int windDirection = startCell.getWindDir();
		// final double windSpeed = startCell.getWindSpeed();
		//
		// SailConfiguration rightConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		// for (int rotation = 1; rotation < 180; rotation++) {
		// final int angle = GeoLocation.adjustAngle(windDirection + rotation);
		// final SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
		// final double speed = configuration.getSpeed();
		// // Sails sail = configuration.getSail();
		// final double vmcSpeed = speed * Math.cos(Math.toRadians(heading - angle));
		// if (vmcSpeed > rightVMC) {
		// rightVMC = vmcSpeed;
		// rightAngle = angle;
		// rightBoatSpeed = speed;
		// rightConfiguration = configuration;
		// }
		// }
		// SailConfiguration leftConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		// for (int rotation = 1; rotation < 180; rotation++) {
		// final int angle = GeoLocation.adjustAngle(windDirection - rotation);
		// final SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
		// final double speed = configuration.getSpeed();
		// // Sails sail = configuration.getSail();
		// final double vmcSpeed = speed * Math.cos(Math.toRadians(heading - angle));
		// if (vmcSpeed > leftVMC) {
		// leftVMC = vmcSpeed;
		// leftAngle = angle;
		// leftBoatSpeed = speed;
		// leftConfiguration = configuration;
		// }
		// }
		// vmc.addLeftData(leftVMC, leftAngle, leftConfiguration);
		// vmc.addRightData(rightVMC, rightAngle, rightConfiguration);
		// return vmc;
	}

	public void incrementLat(final double incLat) {
		latitude += incLat;
	}

	public void incrementLon(final double incLon) {
		longitude += incLon;
	}

	public void printVMC(final WindCell startCell, final int heading) {
		double leftVMC = -1.0;
		double rightVMC = -1.0;
		double leftBoatSpeed = -1.0;
		double rightBoatSpeed = -1.0;
		int leftAngle = 0;
		int rightAngle = 0;
		final int windDirection = startCell.getWindDir();
		final double windSpeed = startCell.getWindSpeed();

		SailConfiguration rightConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		for (int rotation = 1; rotation < 180; rotation++) {
			final int angle = GeoLocation.adjustAngleTo360(windDirection + rotation);
			final SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
			final double speed = configuration.getSpeed();
			// Sails sail = configuration.getSail();
			final double vmcSpeed = speed * Math.cos(Math.toRadians(heading - angle));
			if (vmcSpeed > rightVMC) {
				rightVMC = vmcSpeed;
				rightAngle = angle;
				rightBoatSpeed = speed;
				rightConfiguration = configuration;
			}
		}
		SailConfiguration leftConfiguration = new SailConfiguration(Sails.JIB, 0.0);
		for (int rotation = 1; rotation < 180; rotation++) {
			final int angle = GeoLocation.adjustAngleTo360(windDirection - rotation);
			final SailConfiguration configuration = Polars.lookup(rotation, windSpeed);
			final double speed = configuration.getSpeed();
			// Sails sail = configuration.getSail();
			final double vmcSpeed = speed * Math.cos(Math.toRadians(heading - angle));
			if (vmcSpeed > leftVMC) {
				leftVMC = vmcSpeed;
				leftAngle = angle;
				leftBoatSpeed = speed;
				leftConfiguration = configuration;
			}
		}
		final StringBuffer buffer = new StringBuffer("[VMC results").append('\n');
		buffer.append("Cell Location=").append(startCell.getLocation().formattedLocation()).append('\n');
		buffer.append("Projection heading=").append(heading).append('\n');
		buffer.append("Wind direction=").append(windDirection).append('\n');
		buffer.append("Wind speed=").append(windSpeed).append('\n');
		buffer.append("Left VMC [boat=").append(leftBoatSpeed);
		buffer.append("-").append(leftConfiguration.getSail()).append(" - VMC=").append(leftVMC).append(" - ");
		buffer.append(leftAngle).append("]\n");
		buffer.append("Right VMC [boat=").append(rightBoatSpeed);
		buffer.append("-").append(rightConfiguration.getSail()).append(" - VMC=").append(rightVMC).append(" - ");
		buffer.append(rightAngle).append("]\n]");
		System.out.println(buffer.toString());
	}
}

// - UNUSED CODE ............................................................................................
