//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

//- IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import net.sf.vorg.core.exceptions.LocationNotInMap;

//- CLASS IMPLEMENTATION ...................................................................................
public class WindMap {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger							logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");
	// - F I E L D - S E C T I O N ............................................................................
	protected final Vector<WindCell>	cells			= new Vector<WindCell>(10);
	protected String									reference	= "UNDEFINED";
	private Date											timeStamp	= GregorianCalendar.getInstance().getTime();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void addCell(final WindCell newCell) {
		//- Check if this cell is already on the map.
		if (!cells.contains(newCell)) {
			//- Timestamp this cell.
			newCell.timeStamp(timeStamp);
			cells.add(newCell);
		}
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setRef(final String mapRef) {
		reference = mapRef;
	}

	public void setTimeStamp(final Date mapDate) {
		timeStamp = mapDate;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[WindMap ");
		buffer.append(reference).append(",");
		buffer.append("nroCells=").append(cells.size()).append("]");
		return buffer.toString();
	}

	/**
	 * Locate the wind cell in the map that contains the specified location.
	 * 
	 * @throws LocationNotInMap
	 */
	protected WindCell cellWithPoint(final GeoLocation start) throws LocationNotInMap {
		final Iterator<WindCell> wit = cells.iterator();
		while (wit.hasNext()) {
			final WindCell cell = wit.next();
			if (cell.doesContain(start)) return cell;
		}
		// - No cell contains this location we have to throw an exception
		throw new LocationNotInMap("The " + start.toString() + " location is not contained inside the Route Map.");
	}

	protected WindCell cellWithPoint(final GeoLocation start, final WindCell skip) throws LocationNotInMap {
		final Iterator<WindCell> wit = cells.iterator();
		while (wit.hasNext()) {
			final WindCell cell = wit.next();
			// - Skip identities have to be checked with coordinates because cells in different time maps also are different.
			if (cell.doesContain(start)) if (cell.isEquivalent(skip))
				continue;
			else
				return cell;
		}
		// - No cell contains this location we have to throw an exception
		throw new LocationNotInMap("The " + start.toString() + " location is not contained inside the Route Map.");
	}
}
// - UNUSED CODE ............................................................................................
