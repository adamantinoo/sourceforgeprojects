//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.ui;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.osgi.service.prefs.BackingStoreException;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

import net.sf.vorg.core.app.VORGCorePlugin;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class implements a sample preference page that is added to the preference dialog based on the
 * registration.
 */
public class VORGCorePreferencePage extends PreferencePage implements IWorkbenchPreferencePage {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core"); //$NON-NLS-1$
	// - F I E L D - S E C T I O N ............................................................................
	private Group					windDataGroup;
	private Spinner				firstChangeHour;
	private Spinner				numberOfWindChanges;
	private Group					polarDataGroup;
	private Spinner				sailSpeedLimit;
	private Spinner				minimumWindpointAngle;
	private Group					windMapDataGroup;
	private Spinner				windMapDisplacement;
	private Text					windMapHost;
	private Text					windMapURLPrefix;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	//[01]

	/*
	 * (non-Javadoc) Method declared on IWorkbenchPreferencePage
	 */
	public void init(final IWorkbench workbench) {
		// do nothing
	}

	/*
	 * (non-Javadoc) Method declared on PreferencePage
	 */
	@Override
	public boolean performOk() {
		final IPreferenceStore store = getPreferenceStore();
		store.setValue(VORGCorePlugin.FIRST_WIND_CHANGE, firstChangeHour.getSelection());
		store.setValue(VORGCorePlugin.WIND_CHANGE_COUNT, numberOfWindChanges.getSelection());
		store.setValue(VORGCorePlugin.LIMIT_SPEED, sailSpeedLimit.getSelection());
		store.setValue(VORGCorePlugin.WINDPOINT_ANGLE, minimumWindpointAngle.getSelection());
		store.setValue(VORGCorePlugin.WINDMAP2GMP_DISPLACEMENT, windMapDisplacement.getSelection());
		store.setValue(VORGCorePlugin.WINDMAP_HOST, windMapHost.getText());
		store.setValue(VORGCorePlugin.WINDMAP_PREFIX, windMapURLPrefix.getText());
		try {
			new InstanceScope().getNode(VORGCorePlugin.PLUGIN_ID).flush();
		} catch (final BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//		VORGCorePlugin.getPlugin().savePluginPreferences();
		return true;
	}

	/**
	 * Creates the components contents of the Preference Page. This is the set of controls and data boxes that
	 * allow to change all the configuration properties for this plug-in. The control is now simple because
	 * there is no packaging between the game set and the different properties to be consistent.
	 */
	@Override
	protected Control createContents(final Composite parent) {
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, VORGCorePlugin.PREFERENCE_PAGE_CONTEXT);
		final IPreferenceStore store = getPreferenceStore();

		//- Create the canvas control to store the page contents.
		final Composite page = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		page.setLayout(layout);
		GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		page.setLayoutData(data);

		//- Create the wind data configuration group.
		windDataGroup = createGroup(page, MessagesVORGCore.VORGCorePreferencePage_windchangetimes, 3);
		createLabel(windDataGroup, MessagesVORGCore.VORGCorePreferencePage_firstwindchange);
		firstChangeHour = new Spinner(windDataGroup, SWT.NONE);
		firstChangeHour.addModifyListener(new ModifyListener() {
			public void modifyText(final ModifyEvent e) {
				e.getSource();
			}
		});
		firstChangeHour.setValues(store.getInt(VORGCorePlugin.FIRST_WIND_CHANGE), 0, 24, 0, 1, 4);
		createLabel(windDataGroup, MessagesVORGCore.VORGCorePreferencePage_windchangecount);
		numberOfWindChanges = new Spinner(windDataGroup, SWT.NONE);
		numberOfWindChanges.setValues(store.getInt(VORGCorePlugin.WIND_CHANGE_COUNT), 1, 6, 0, 1, 1);

		//- Create the polar data configuration group.
		polarDataGroup = createGroup(page, MessagesVORGCore.VORGCorePreferencePage_polarparameters, 3);
		createLabel(polarDataGroup, MessagesVORGCore.VORGCorePreferencePage_speedlimit);
		sailSpeedLimit = new Spinner(polarDataGroup, SWT.NONE);
		sailSpeedLimit.setValues(store.getInt(VORGCorePlugin.LIMIT_SPEED), 20, 35, 0, 1, 2);
		createLabel(polarDataGroup, MessagesVORGCore.VORGCorePreferencePage_windpointangle);
		minimumWindpointAngle = new Spinner(polarDataGroup, SWT.NONE);
		minimumWindpointAngle.setValues(store.getInt(VORGCorePlugin.WINDPOINT_ANGLE), 1, 60, 0, 1, 5);

		//- Create the wind map data configuration group.
		windMapDataGroup = createGroup(page, MessagesVORGCore.VORGCorePreferencePage_windmapsparameters, 3);
		createLabel(windMapDataGroup, MessagesVORGCore.VORGCorePreferencePage_wind2gmtdisplacement);
		windMapDisplacement = new Spinner(windMapDataGroup, SWT.NONE);
		windMapDisplacement.setValues(store.getInt(VORGCorePlugin.WINDMAP2GMP_DISPLACEMENT), -6, 6, 0, 1, 1);
		createLabel(windMapDataGroup, MessagesVORGCore.VORGCorePreferencePage_windmaphost);
		windMapHost = createTextField(windMapDataGroup);
		windMapHost.setText(store.getString(VORGCorePlugin.WINDMAP_HOST));
		createLabel(windMapDataGroup, MessagesVORGCore.VORGCorePreferencePage_windmapprefix);
		windMapURLPrefix = createTextField(windMapDataGroup);
		windMapURLPrefix.setText(store.getString(VORGCorePlugin.WINDMAP_PREFIX));
		windMapURLPrefix.setEditable(false);

		//		this.initializeValues();
		return page;
		//return new Composite(parent, SWT.NULL);
	}

	@Override
	protected Point doComputeSize() {
		//		windMapDisplacement.setSize(width, height);
		return super.doComputeSize();
	}

	/**
	 * The <code>ReadmePreferencePage implementation of this
	 * <code>PreferencePage method 
	 * returns preference store that belongs to the our plugin.
	 * This is important because we want to store
     * our preferences separately from the workbench.
	 */
	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return VORGCorePlugin.getPlugin().getPreferenceStore();
	}

	/*
	 * (non-Javadoc) Method declared on PreferencePage
	 */
	@Override
	protected void performDefaults() {
		super.performDefaults();
		final IPreferenceStore store = getPreferenceStore();
		firstChangeHour.setSelection(store.getDefaultInt(VORGCorePlugin.FIRST_WIND_CHANGE));
		numberOfWindChanges.setSelection(store.getDefaultInt(VORGCorePlugin.WIND_CHANGE_COUNT));
		sailSpeedLimit.setSelection(store.getDefaultInt(VORGCorePlugin.LIMIT_SPEED));
		minimumWindpointAngle.setSelection(store.getDefaultInt(VORGCorePlugin.WINDPOINT_ANGLE));
		windMapDisplacement.setSelection(store.getDefaultInt(VORGCorePlugin.WINDMAP2GMP_DISPLACEMENT));
		windMapHost.setText(store.getDefaultString(VORGCorePlugin.WINDMAP_HOST));
		windMapURLPrefix.setText(store.getDefaultString(VORGCorePlugin.WINDMAP_PREFIX));
	}

	private Group createGroup(final Composite parent, final String title, final int columns) {
		final Group newGroup = new Group(parent, SWT.NONE);
		newGroup.setText(title);
		newGroup.setFont(VORGCorePlugin.FONT_GROUPNAME);
		newGroup.setForeground(VORGCorePlugin.COLOR_BRILLIANT_BLUE);

		//- Set the layout to the specified number of columns.
		final GridLayout layout = new GridLayout();
		layout.numColumns = columns;
		layout.makeColumnsEqualWidth = false;
		newGroup.setLayout(layout);

		final GridData data = new GridData();
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		newGroup.setLayoutData(data);
		return newGroup;
	}

	/**
	 * Utility method that creates a label instance and sets the default layout data.
	 * 
	 * @param parent
	 *          the parent for the new label
	 * @param text
	 *          the text for the new label
	 * @return the new label
	 */
	private Label createLabel(final Composite parent, final String text) {
		final Label label = new Label(parent, SWT.LEFT);
		label.setText(text);
		final GridData data = new GridData();
		data.horizontalSpan = 2;
		data.horizontalAlignment = GridData.FILL;
		label.setLayoutData(data);
		return label;
	}

	/**
	 * Create a text field specific for this application
	 * 
	 * @param parent
	 *          the parent of the new text field
	 * @return the new text field
	 */
	private Text createTextField(final Composite parent) {
		final Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		//		text.addModifyListener(this);
		final GridData data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		data.verticalAlignment = GridData.CENTER;
		data.grabExcessVerticalSpace = false;
		text.setLayoutData(data);
		return text;
	}

}
//- UNUSED CODE ............................................................................................
//[01]
//	/**
//	 * Creates an new checkbox instance and sets the default layout data.
//	 * 
//	 * @param group
//	 *          the composite in which to create the checkbox
//	 * @param label
//	 *          the string to set into the checkbox
//	 * @return the new checkbox
//	 */
//	private Button createCheckBox(Composite group, String label) {
//		Button button = new Button(group, SWT.CHECK | SWT.LEFT);
//		button.setText(label);
//		button.addSelectionListener(this);
//		GridData data = new GridData();
//		button.setLayoutData(data);
//		return button;
//	}
//
//	/**
//	 * Creates composite control and sets the default layout data.
//	 * 
//	 * @param parent
//	 *          the parent of the new composite
//	 * @param numColumns
//	 *          the number of columns for the new composite
//	 * @return the newly-created coposite
//	 */
//	private Composite createComposite(Composite parent, int numColumns) {
//		Composite composite = new Composite(parent, SWT.NULL);
//
//		//GridLayout
//		GridLayout layout = new GridLayout();
//		layout.numColumns = numColumns;
//		composite.setLayout(layout);
//
//		//GridData
//		GridData data = new GridData();
//		data.verticalAlignment = GridData.FILL;
//		data.horizontalAlignment = GridData.FILL;
//		composite.setLayoutData(data);
//		return composite;
//	}
//	/**
//	 * Utility method that creates a push button instance and sets the default layout data.
//	 * 
//	 * @param parent
//	 *          the parent for the new button
//	 * @param label
//	 *          the label for the new button
//	 * @return the newly-created button
//	 */
//	private Button createPushButton(Composite parent, String label) {
//		Button button = new Button(parent, SWT.PUSH);
//		button.setText(label);
//		button.addSelectionListener(this);
//		GridData data = new GridData();
//		data.horizontalAlignment = GridData.FILL;
//		button.setLayoutData(data);
//		return button;
//	}

//	/**
//	 * Utility method that creates a radio button instance and sets the default layout data.
//	 * 
//	 * @param parent
//	 *          the parent for the new button
//	 * @param label
//	 *          the label for the new button
//	 * @return the newly-created button
//	 */
//	private Button createRadioButton(Composite parent, String label) {
//		Button button = new Button(parent, SWT.RADIO | SWT.LEFT);
//		button.setText(label);
//		button.addSelectionListener(this);
//		GridData data = new GridData();
//		button.setLayoutData(data);
//		return button;
//	}
//	/**
//	 * Creates a tab of one horizontal spans.
//	 * 
//	 * @param parent
//	 *          the parent in which the tab should be created
//	 */
//	private void tabForward(Composite parent) {
//		Label vfiller = new Label(parent, SWT.LEFT);
//		GridData gridData = new GridData();
//		gridData = new GridData();
//		gridData.horizontalAlignment = GridData.BEGINNING;
//		gridData.grabExcessHorizontalSpace = false;
//		gridData.verticalAlignment = GridData.CENTER;
//		gridData.grabExcessVerticalSpace = false;
//		vfiller.setLayoutData(gridData);
//	}

//	/**
//	 * (non-Javadoc) Method declared on SelectionListener
//	 */
//	public void widgetDefaultSelected(SelectionEvent event) {
//		//Handle a default selection. Do nothing in this example
//	}
//
//	/**
//	 * (non-Javadoc) Method declared on SelectionListener
//	 */
//	public void widgetSelected(SelectionEvent event) {
//		//Do nothing on selection in this example;
//	}
//	/**
//	 * (non-Javadoc) Method declared on ModifyListener
//	 */
//	public void modifyText(ModifyEvent event) {
//		//Do nothing on a modification in this example
//	}

//	/**
//	 * Initializes states of the controls using default values in the preference store.
//	 */
//	private void initializeDefaults() {
//		final IPreferenceStore store = this.getPreferenceStore();
//		checkBox1.setSelection(store.getDefaultBoolean(IReadmeConstants.PRE_CHECK1));
//		checkBox2.setSelection(store.getDefaultBoolean(IReadmeConstants.PRE_CHECK2));
//		checkBox3.setSelection(store.getDefaultBoolean(IReadmeConstants.PRE_CHECK3));
//
//		radioButton1.setSelection(false);
//		radioButton2.setSelection(false);
//		radioButton3.setSelection(false);
//		final int choice = store.getDefaultInt(IReadmeConstants.PRE_RADIO_CHOICE);
//		switch (choice) {
//			case 1:
//				radioButton1.setSelection(true);
//				break;
//			case 2:
//				radioButton2.setSelection(true);
//				break;
//			case 3:
//				radioButton3.setSelection(true);
//				break;
//		}
//		textField.setText(store.getDefaultString(IReadmeConstants.PRE_TEXT));
//	}

//	/**
//	 * Initializes states of the controls from the preference store.
//	 */
//	private void initializeValues() {
//		final IPreferenceStore store = this.getPreferenceStore();
//		checkBox1.setSelection(store.getBoolean(IReadmeConstants.PRE_CHECK1));
//		checkBox2.setSelection(store.getBoolean(IReadmeConstants.PRE_CHECK2));
//		checkBox3.setSelection(store.getBoolean(IReadmeConstants.PRE_CHECK3));
//
//		final int choice = store.getInt(IReadmeConstants.PRE_RADIO_CHOICE);
//		switch (choice) {
//			case 1:
//				radioButton1.setSelection(true);
//				break;
//			case 2:
//				radioButton2.setSelection(true);
//				break;
//			case 3:
//				radioButton3.setSelection(true);
//				break;
//		}
//		textField.setText(store.getString(IReadmeConstants.PRE_TEXT));
//	}

//	/**
//	 * Stores the values of the controls back to the preference store.
//	 */
//	private void storeValues() {
//		final IPreferenceStore store = getPreferenceStore();
//		store.setValue(IReadmeConstants.PRE_CHECK1, checkBox1.getSelection());
//		store.setValue(IReadmeConstants.PRE_CHECK2, checkBox2.getSelection());
//		store.setValue(IReadmeConstants.PRE_CHECK3, checkBox3.getSelection());
//
//		int choice = 1;
//
//		if (radioButton2.getSelection())
//			choice = 2;
//		else if (radioButton3.getSelection()) choice = 3;
//
//		store.setValue(IReadmeConstants.PRE_RADIO_CHOICE, choice);
//		store.setValue(IReadmeConstants.PRE_TEXT, textField.getText());
//	}

