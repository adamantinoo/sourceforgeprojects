//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.app;

// - IMPORT SECTION .........................................................................................
import org.osgi.framework.BundleContext;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGCorePlugin extends AbstractUIPlugin {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core.app");
	public static final String		VORG											= "VORG";

	public static final Font			FONT_GROUPNAME						= new Font(Display.getDefault(), "Tahoma", 10, SWT.NORMAL);
	public static final Color			COLOR_BRILLIANT_BLUE			= new Color(Display.getDefault(), 0x00, 0x00, 0xFF);

	//- Property names and references
	public static final String		PLUGIN_ID									= "net.sf.vorg.core.app.VORGCorePlugin";										//$NON-NLS-1$
	public static final String		PREFIX										= VORGCorePlugin.PLUGIN_ID + ".";													//$NON-NLS-1$
	public static final String		FIRST_WIND_CHANGE					= VORGCorePlugin.PREFIX + "first_wind_change";
	public static final String		WIND_CHANGE_COUNT					= VORGCorePlugin.PREFIX + "wind_change_count";
	public static final String		LIMIT_SPEED								= VORGCorePlugin.PREFIX + "limit_speed";
	public static final String		POLAR_DATABASE_ID					= VORGCorePlugin.PREFIX + "polar_database_id";
	public static final String		WINDPOINT_ANGLE						= VORGCorePlugin.PREFIX + "windpoint_angle";
	public static final String		WINDMAP2GMP_DISPLACEMENT	= VORGCorePlugin.PREFIX + "windmap2gmt_displacement";
	public static final String		WINDMAP_HOST							= VORGCorePlugin.PREFIX + "windmap_host";
	public static final String		WINDMAP_PREFIX						= VORGCorePlugin.PREFIX + "windmap_prefix";
	public static final String		PREFERENCE_PAGE_CONTEXT		= PREFIX + "preference_page_context";											//$NON-NLS-1$

	private static VORGCorePlugin	pluginReference						= null;

	public static AbstractUIPlugin getPlugin() {
		VORGCorePlugin ref = VORGCorePlugin.pluginReference;
		return VORGCorePlugin.pluginReference;
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGCorePlugin() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		VORGCorePlugin.pluginReference = this;
	}

	@Override
	public void stop(final BundleContext context) throws Exception {
		VORGCorePlugin.pluginReference = null;
		super.stop(context);
	}
}
// - UNUSED CODE ............................................................................................
