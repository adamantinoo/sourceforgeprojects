//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.ui;

// - IMPORT SECTION .........................................................................................
import org.eclipse.osgi.util.NLS;

// - CLASS IMPLEMENTATION ...................................................................................
public class MessagesVORGCore extends NLS {
	private static final String	BUNDLE_NAME	= "net.sf.vorg.core.ui.messages"; //$NON-NLS-1$
	public static String				VORGCorePreferencePage_firstwindchange;
	public static String	VORGCorePreferencePage_polarparameters;
	public static String	VORGCorePreferencePage_speedlimit;
	public static String	VORGCorePreferencePage_wind2gmtdisplacement;
	public static String				VORGCorePreferencePage_windchangecount;
	public static String				VORGCorePreferencePage_windchangetimes;
	public static String	VORGCorePreferencePage_windmaphost;
	public static String	VORGCorePreferencePage_windmapprefix;
	public static String	VORGCorePreferencePage_windmapsparameters;
	public static String	VORGCorePreferencePage_windpointangle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, MessagesVORGCore.class);
	}

	private MessagesVORGCore() {
	}
}

// - UNUSED CODE ............................................................................................
