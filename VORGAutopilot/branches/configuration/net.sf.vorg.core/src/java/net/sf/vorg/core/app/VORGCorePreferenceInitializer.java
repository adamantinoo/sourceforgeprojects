//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGCorePreferenceInitializer extends AbstractPreferenceInitializer {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core.app");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGCorePreferenceInitializer() {
		super();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void initializeDefaultPreferences() {
		//- Define the values for preferences to be used for the first time.
		final IPreferenceStore store = VORGCorePlugin.getPlugin().getPreferenceStore();
		store.setDefault(VORGCorePlugin.FIRST_WIND_CHANGE, 8);
		store.setDefault(VORGCorePlugin.WIND_CHANGE_COUNT, 2);
		store.setDefault(VORGCorePlugin.LIMIT_SPEED, 23);
		store.setDefault(VORGCorePlugin.POLAR_DATABASE_ID, VORGCorePlugin.VORG);
		store.setDefault(VORGCorePlugin.WINDPOINT_ANGLE, 10);
		store.setDefault(VORGCorePlugin.WINDMAP2GMP_DISPLACEMENT, -2);
		store.setDefault(VORGCorePlugin.WINDMAP_HOST, "volvogame.virtualregatta.com");
		store.setDefault(VORGCorePlugin.WINDMAP_PREFIX, "/resources/winds/meteo_");

	}
}
// - UNUSED CODE ............................................................................................
