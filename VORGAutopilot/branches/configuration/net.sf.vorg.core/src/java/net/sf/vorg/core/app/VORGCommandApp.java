//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.app;

// - IMPORT SECTION .........................................................................................
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger			= Logger.getLogger("net.sf.vorg.core.app");
	private static PrintWriter	printer;
	protected static boolean		onDebug			= false;
	protected static String			outputName	= "VORGCommandApp.output.txt";

	// - F I E L D - S E C T I O N ............................................................................

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	public static boolean onDebug() {
		return onDebug;
	}

	public static void output() {
		output(VORGConstants.NEWLINE);
	}

	public static void output(final String message) {
		System.out.println(message);
		if (null == printer)
			try {
				printer = new PrintWriter(new FileOutputStream(outputName, true), true);
				printer.println(message);
				printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			printer.println(message);
			printer.flush();
		}
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGCommandApp() {
	}

	protected double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	protected double argumentGeopositionValue(final String positionData) {
		if (null == positionData)
			return 0.0;
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = positionData.indexOf(":");
			// - The latitude comes in double format
			if (pos < 1)
				return new Double(positionData).doubleValue();
			else {
				final int degree = new Integer(positionData.substring(0, pos)).intValue();
				final int minute = new Integer(positionData.substring(pos + 1, positionData.length())).intValue();
				// - Test for negative values.
				// - Check for negative values but in the range from 0 to 1 degree.
				if (positionData.substring(0, 1).equals("-"))
					return degree * 1.0 - minute / 60.0;
				else if (degree < 0)
					return degree * 1.0 - minute / 60.0;
				else
					return degree * 1.0 + minute / 60.0;
			}
		} catch (final Exception ex) {
			return 0.0;
		}
	}

	protected int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	protected String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			exit(VORGConstants.NOCONFIG);
		return "";
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
