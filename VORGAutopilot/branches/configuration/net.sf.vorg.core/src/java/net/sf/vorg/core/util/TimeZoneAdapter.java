//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.util;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class TimeZoneAdapter {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core.util");

	public static Calendar changeTimeZone(Calendar targetDate, String newTimeZoneID) {
		int localTimeZoneOffset = targetDate.getTimeZone().getOffset(targetDate.getTimeInMillis());
		int gmtTimeZoneOffset = TimeZone.getTimeZone(newTimeZoneID).getOffset(targetDate.getTimeInMillis());
		int savings = targetDate.getTimeZone().getDSTSavings();
		int localDisplacement = localTimeZoneOffset * -1 + gmtTimeZoneOffset * -1;
		targetDate.add(Calendar.MILLISECOND, localTimeZoneOffset * -1);
		targetDate.add(Calendar.MILLISECOND, gmtTimeZoneOffset * -1);
		targetDate.add(Calendar.MILLISECOND, savings);
		return targetDate;
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TimeZoneAdapter() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
