//  PROJECT:        net.sf.vorg.map.perspective
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import net.sf.vorg.views.GlobalMapView;

// - CLASS IMPLEMENTATION ...................................................................................
public class AutopilotPerspective implements IPerspectiveFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String	MAPPERSPECTIVE_ID	= "net.sf.vorg.ui.MapPerspective.id";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Creates the map views and sets the initial proposed locations and structure. The method defines the
	 * relative locations of the different elements that compose the Map perspective and their relative sizes.<br>
	 * The total size of the workspace is decided by the user of the initialization code of the application. The
	 * locations of the views and it sizes are hard coded inside this method and cannot be declared as
	 * configurable properties.<br>
	 * The order in the creation is important to position the views in an appropriate manner.<br>
	 * The elements defined for the Map Perspective are:
	 * <ul>
	 * <li>The Global Map where we draw the coast lines and any other map graphical data.</li>
	 * <li>The Property view where we show the property data for the selected element or elements.</li>
	 * </ul>
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		//- Register the perspective
		Activator.addReference(MAPPERSPECTIVE_ID, this);

		// - Activate the Editor area. The maps are shown on editor parts.
		final String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);

		// - Define the two views that compose the main presentation window
		layout.addStandaloneView(GlobalMapView.ID, true, IPageLayout.LEFT, 0.25f, editorArea);
		layout.getViewLayout(GlobalMapView.ID).setCloseable(false);
	}
}

// - UNUSED CODE ............................................................................................
