//  PROJECT:        net.sf.vorg.map.perspective
//  FILE NAME:      $Id: GraphicalDetailedView.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.gef.model.IGEFModel;
import es.ftgroup.gef.model.IWireModel;
import es.ftgroup.ui.figures.IFigureFactory;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import net.sf.gef.core.editors.BaseGraphicalEditor;
import net.sf.gef.core.editors.LocalEmptyEditorSite;
import net.sf.gef.core.editparts.AbstractEditPartFactory;
import net.sf.gef.core.editparts.AbstractGenericEditPart;
import net.sf.vorg.app.Activator;

// - CLASS IMPLEMENTATION ...................................................................................
public class GlobalMapView extends ViewPart implements PropertyChangeListener {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final String		ID							= "net.sf.vorg.views.GlobalMapView.id";

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * The view cannot be an editor at the same time, so delegate all editor actions to this editor that is
	 * created during the <code>createPartControl</code> creation phase.
	 */
	LocalGraphicalDetailedEditor	detailEditor		= null;
	/** This is the root of the editor's model. */
	private final MapDataStore		editorContainer	= null;

	//	@SuppressWarnings("unused")
	//	private Composite							viewerRoot;
	//	@SuppressWarnings("unused")
	//	private IViewSite							viewSite;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GlobalMapView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		Activator.addReference(GlobalMapView.ID, this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.<br>
	 * This class will set as the top presentation element a new <code>GraphicalDetailedEditor</code> that will
	 * present the selection received as a new MVC pattern
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// - Create a new editor and initialize it based on this view.
		//		viewerRoot = parent;
		detailEditor = new LocalGraphicalDetailedEditor(parent, this);
	}

	//	@Override
	//	public void dispose() {
	//		if (null != editorContainer) {
	//			editorContainer.removePropertyChangeListener(this);
	//		}
	//		super.dispose();
	//	}

	@Override
	public void init(final IViewSite site) throws PartInitException {
		//		viewSite = site;
		super.init(site);

		//- Connect this view with the content provider that contains the singleton model.
		//		editorContainer = (UIPilotModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		//		if (null == editorContainer) {
		//			editorContainer = new UIPilotModelStore();
		//			//		//- Configure the input location by hand.
		//			//		UIVRToolNavParser input = new UIVRToolNavParser("L:\\VORG\\Leg5-ChinaRio.nav");
		//			//		editorContainer.setInputHandler(input);
		//			//		//FIXME need to load the model
		//			//		input.loadContents();
		//		}
		//		editorContainer.addPropertyChangeListener(this);
	}

	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();

		//		// - Update the model when we have finished with the addition and processing of the turn data.
		//		if (AbstractGEFNode.CHILD_ADDED_PROP.equals(prop)) {
		//			editorContainer.fireStructureChange(PilotModelStore.MODEL_STRUCTURE_CHANGED, null, null);
		//		}
	}

	public MapDataStore getContainer() {
		return editorContainer;
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	@Override
	public void setFocus() {
		detailEditor.setFocus();
	}
}

class MapDataStore {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger						= Logger.getLogger("net.sf.vorg");
	// - F I E L D - S E C T I O N ............................................................................
	private String				coastLineDataFile	= "E:\\Docs\\ldediego\\Workstage\\ProjectsSourceforge\\VORGAutopilot\\net.sf.vorg.map.perspective\\mapdata\\coast\\30240.dat";
	private final Vector	coastLines				= new Vector();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public void readCoastLineData(String coastLineFile) {
		if (null != coastLineFile) coastLineDataFile = coastLineFile;
		try {
			FileSequenceProcessor processor = new FileSequenceProcessor(coastLineDataFile);
			processor.process(new CoastLineProcessor());
			//			BufferedReader dataInput = new BufferedReader(new FileReader(coastLineDataFile));
			//			String dataLine = dataInput.readLine();
			//			while (null != dataLine) {
			//				try {
			//					if (detectPilotRoute(line)) if (parseBoatName(line)) {
			//						processWaypoints(input);
			//					}
			//				} catch (final RuntimeException exc) {
			//					//- Detect any type of runtime exception and bypass it.
			//					System.out.println("EEE - " + exc.getLocalizedMessage());
			//				}
			//				dataLine = dataInput.readLine();
			//			}
		} catch (final FileNotFoundException fnfe) {
			//- Report the exception to the user console but do not change anything.
			logger.severe("Coast Data lines file does not exist. [" + fnfe.getLocalizedMessage() + "]");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class FileSequenceProcessor {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger					= Logger.getLogger("net.sf.vorg");
	// - F I E L D - S E C T I O N ............................................................................
	private String								currentFilePath	= null;
	private final BufferedReader	dataInput;
	private ILineProcessor				processor;
	private String								dataLine;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FileSequenceProcessor(String filePath) throws IOException {
		if (null == filePath)
			throw new FileNotFoundException("The file path received is not valid. Cannot open this file [" + filePath + "]");
		currentFilePath = filePath;
		dataInput = new BufferedReader(new FileReader(currentFilePath));
		dataLine = dataInput.readLine();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setProcessor(ILineProcessor newProcessor) {
		processor = newProcessor;
	}

	public void process(ILineProcessor newProcessor) throws IOException {
		if (null != newProcessor) setProcessor(newProcessor);
		if (null == processor) throw new IOException("No line processor set to process the file.");
		while (null != dataLine) {
			processor.process(dataLine);
			dataLine = dataInput.readLine();
		}
	}
}

class CoastLineProcessor implements ILineProcessor {

	public void process(String dataLine) {
		// TODO Auto-generated method stub

	}

}

interface ILineProcessor {

	void process(String dataLine);

}

class LocalGraphicalDetailedEditor extends BaseGraphicalEditor {
	//	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.views");
	// - S T A T I C - S E C T I O N ..........................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	ID	= "net.sf.vorg.editors.LocalGraphicalDetailedEditor.id";

	// - F I E L D - S E C T I O N ............................................................................
	// - F I E L D S
	private GlobalMapView				detailedView;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public LocalGraphicalDetailedEditor(Composite parent, GlobalMapView detailedView) {
		try {
			setEditDomain(new DefaultEditDomain(this));
			this.detailedView = detailedView;
			// - Register the view. This will remove the requirement to have the view declared as a static singleton
			Activator.addReference(ID, this);

			//- Access the selection editor to copy the initialization to this view editor.
			init(this.detailedView.getSite());
			createGraphicalViewer(parent);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public MapDataStore getContents() {
		if (null != detailedView)
			return detailedView.getContainer();
		else
			return new MapDataStore();
	}

	public void init(IWorkbenchPartSite site) throws PartInitException {
		LocalEmptyEditorSite editorSite = new LocalEmptyEditorSite(site);
		setSite(editorSite);
		setInput(null);
		getCommandStack().addCommandStackListener(this);
		//		// - Add this editor selection listener to the list of listeners of this window
		//		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(this);
		initializeActionRegistry();
	}

	@Override
	protected void initializeGraphicalViewer() {
		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setContents(getContents()); // Set the contents of this graphical.
	}

	@Override
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();

		GraphicalViewer viewer = getGraphicalViewer();
		viewer.setRootEditPart(new ScalableRootEditPart());
		viewer.setEditPartFactory(new MapEditPartFactory(new MapFigureFactory()));
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		//		// - Register this as a listener to the SelectionInfoView selection provider.
		//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
		//		if (null != provider) ((ISelectionProvider) provider).addSelectionChangedListener(this);

		//		// configure the context menu provider
		//		ContextMenuProvider cmProvider = new VGAP4MapViewerContextMenuProvider(viewer, getActionRegistry());
		//		((VGAP4MapViewerContextMenuProvider) cmProvider).registerAction(new ImportTurnDataAction(this));
		//		viewer.setContextMenu(cmProvider);
		//		getSite().registerContextMenu(cmProvider, viewer);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Do nothing or we can bypass signaling to the main editor

	}

	//	@Override
	//	public void dispose() {
	//		// - Unregister this from the SelectionInfoView selection provider.
	//		final Object provider = Activator.getByID("SelectionInfoView.SelectionProvider");
	//		if (null != provider) ((ISelectionProvider) provider).removeSelectionChangedListener(this);
	//		super.dispose();
	//	}

}

class MapEditPartFactory extends AbstractEditPartFactory {

	public MapEditPartFactory(IFigureFactory factory) {
		super(factory);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected AbstractGenericEditPart getPartForElement(Object modelElement) {
		// TODO Auto-generated method stub
		return null;
	}

}

class MapFigureFactory implements IFigureFactory {

	public PolylineConnection createConnection(IWireModel newWire) {
		// TODO Auto-generated method stub
		return null;
	}

	public Figure createFigure(EditPart part, IGEFModel unit) {
		// TODO Auto-generated method stub
		return null;
	}

	public Figure createFigure(EditPart part, IGEFModel unit, String subType) {
		// TODO Auto-generated method stub
		return null;
	}

}
// - UNUSED CODE ............................................................................................
