//  PROJECT:        net.sf.vorg.map.perspective
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.views;

// - IMPORT SECTION .........................................................................................
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Geometry;

import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;

// - CLASS IMPLEMENTATION ...................................................................................
public class ProofMapView extends GlobalMapView {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger							= Logger.getLogger("net.sf.vorg.views");

	// - F I E L D - S E C T I O N ............................................................................
	private final String	mapBordersShapeFile	= "./resources/TM_WORLD_BORDERS-0.2.shp";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ProofMapView() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void init(IViewSite site) throws PartInitException {
		//- Do any parent initialization stuff.
		super.init(site);
		try {
			//- Connection parameters to create a DataStore from a shape file.
			logger.fine("Creating connection parameters to the Shape file: " + mapBordersShapeFile);
			Map<String, Serializable> connectParameters = new HashMap<String, Serializable>();
			File file = new File(mapBordersShapeFile);
			connectParameters.put("url", file.toURI().toURL());
			connectParameters.put("create spatial index", true);
			DataStore dataStore = DataStoreFinder.getDataStore(connectParameters);

			String[] typeNames = dataStore.getTypeNames();
			String typeName = typeNames[0];

			logger.info("Reading content " + typeName);
			FeatureSource<SimpleFeatureType, SimpleFeature> featureSource;
			FeatureCollection<SimpleFeatureType, SimpleFeature> collection;
			FeatureIterator<SimpleFeature> iterator;

			featureSource = dataStore.getFeatureSource(typeName);
			collection = featureSource.getFeatures();
			iterator = collection.features();

			double totalLength = 0.0;
			try {
				while (iterator.hasNext()) {
					SimpleFeature feature = iterator.next();

					Geometry geometry = (Geometry) feature.getDefaultGeometry();
					totalLength += geometry.getLength();
				}
			} finally {
				if (iterator != null) {
					// YOU MUST CLOSE THE ITERATOR!
					iterator.close();
				}
			}
			logger.info("Total Length " + totalLength);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
