//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editors;

// - IMPORT SECTION .........................................................................................
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IKeyBindingService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.IWorkbenchWindow;

// - CLASS IMPLEMENTATION ...................................................................................
@SuppressWarnings("deprecation")
public class LocalEmptyEditorSite implements IEditorSite {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	private final IWorkbenchPartSite	workbenchSite;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public LocalEmptyEditorSite(IWorkbenchPartSite site) {
		workbenchSite = site;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@SuppressWarnings("unchecked")
	public Object getAdapter(Class adapter) {
		return workbenchSite.getAdapter(adapter);
	}

	public String getId() {
		return workbenchSite.getId();
	}

	public IKeyBindingService getKeyBindingService() {
		return workbenchSite.getKeyBindingService();
	}

	public IWorkbenchPage getPage() {
		return workbenchSite.getPage();
	}

	public IWorkbenchPart getPart() {
		return workbenchSite.getPart();
	}

	public String getPluginId() {
		return workbenchSite.getPluginId();
	}

	public String getRegisteredName() {
		return workbenchSite.getRegisteredName();
	}

	public ISelectionProvider getSelectionProvider() {
		return workbenchSite.getSelectionProvider();
	}

	@SuppressWarnings("unchecked")
	public Object getService(Class api) {
		return workbenchSite.getService(api);
	}

	public Shell getShell() {
		return workbenchSite.getShell();
	}

	public IWorkbenchWindow getWorkbenchWindow() {
		return workbenchSite.getWorkbenchWindow();
	}

	@SuppressWarnings("unchecked")
	public boolean hasService(Class api) {
		return workbenchSite.hasService(api);
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuManager, selectionProvider);
	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider) {
		workbenchSite.registerContextMenu(menuId, menuManager, selectionProvider);
	}

	public void setSelectionProvider(ISelectionProvider provider) {
		workbenchSite.setSelectionProvider(provider);
	}

	public IEditorActionBarContributor getActionBarContributor() {
		// TODO Auto-generated method stub
		return null;
	}

	public IActionBars getActionBars() {
		// TODO Auto-generated method stub
		return null;
	}

	public void registerContextMenu(MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}

	public void registerContextMenu(String menuId, MenuManager menuManager, ISelectionProvider selectionProvider,
			boolean includeEditorInput) {
		// TODO Auto-generated method stub

	}
}
// - UNUSED CODE ............................................................................................
