//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.views;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.gef.core.model.IGEFNode;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The content provider will have at the top level a list of selected Spots from the Map. Each Spot may or may
 * not have children and all then share the same parent that is an abstract element. The content provider
 * class is responsible for providing objects to the view. It can wrap existing objects in adapters or simply
 * return objects as-is. These objects may be sensitive to the current input of the view, or ignore it and
 * always show the same content (like Task List, for example).
 */
public class BaseTreeContentProvider implements ITreeContentProvider {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.gef.core.views");

	// - F I E L D - S E C T I O N ............................................................................
	protected final AbstractGEFNode	providerContents;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseTreeContentProvider(AbstractGEFNode modelServer) {
		providerContents = modelServer;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Object[] getChildren(final Object parent) {
		if ((null != parent) && (parent instanceof AbstractGEFNode))
			return ((AbstractGEFNode) parent).getChildren().toArray();
		else
			return null;
	}

	public boolean hasChildren(final Object target) {
		if ((null != target) && (target instanceof AbstractGEFNode)) {
			Vector<IGEFNode> childs = ((AbstractGEFNode) target).getChildren();
			if (childs.size() > 0)
				return true;
			else
				return false;
		} else
			return false;
	}

	public Object[] getElements(final Object parent) {
		if (null != parent)
			return new Object[] { providerContents };
		//		return providerContents.getChildren().toArray();
		else
			return null;
	}

	public Object getParent(final Object element) {
		return null;
	}

	/** It is possible to use this method to signal the model of the selection change. */
	public void inputChanged(final Viewer v, final Object oldInput, final Object newInput) {
	}

	public void dispose() {
	}
}

// - UNUSED CODE ............................................................................................
