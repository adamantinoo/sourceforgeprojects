//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.figures;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface ISelectableFigure {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected();

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected();

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(final int value);
}

// - UNUSED CODE ............................................................................................
