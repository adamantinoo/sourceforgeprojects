//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.SAXException;

import net.sf.core.exceptions.InvalidContentException;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.vorgautopilot.core.NavAttributes;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class UIVRToolNavParser extends VRToolNavParser {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.parsers");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public UIVRToolNavParser(String targetInput) {
		super(targetInput);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Read the input file and convert each line to a list of attributes like the XML parsers. Then send that
	 * information to the <code>startElement</code> method for processing and data acquisition.
	 * 
	 * @throws FileNotFoundException
	 */
	@Override
	protected void parse() throws FileNotFoundException, DataLoadingException, IOException {
		//- Check we are properly configured before starting.
		if ((null == boatStore) || (null == inputReference))
			throw new DataLoadingException("Invalid call. Instance is not initialized");
		else {
			// - Open the input file.
			final BufferedReader input = new BufferedReader(new FileReader(inputReference));
			String line = input.readLine();
			while (null != line) {
				try {
					if (detectPilotRoute(line)) if (parseBoatName(line)) {
						processWaypoints(input);
					}
				} catch (final RuntimeException exc) {
					//- Detect any type of runtime exception and bypass it.
					System.out.println("EEE - " + exc.getLocalizedMessage());
				}
				line = input.readLine();
			}
		}
	}

	/**
	 * Processed a line that is known to contain a boat identification route. Extract the boat name from this
	 * route
	 */
	private boolean parseBoatName(final String line) {
		try {
			//- Split the NAV line into its components.
			final String[] sections = line.split(";");
			String boatName = sections[2];
			boatName = boatName.substring(3);

			if (null != boatName) {
				// - Read the file with the identification information.
				//				final Properties idprops = new Properties();
				//				try {
				//					idprops.load(new BufferedInputStream(new FileInputStream(boatName + ".ident")));
				//					System.out.println("Detected configuration Route for boat name=" + boatName);
				//				} catch (final FileNotFoundException e) {
				//					System.out.println("Identification file for " + boatName + " not found.");
				//					return false;
				//				} catch (final IOException e) {
				//					System.out.println("Identification file for " + boatName + " load error.");
				//					return false;
				//				}

				try {
					//- Check if this boat is already on the store and the clear the list of commands.
					PilotBoat existent = boatStore.searchBoatName(boatName);
					if (null == existent) {
						NavAttributes attr = new NavAttributes();
						attr.add("name", boatName);
						this.startElement("boat", attr);
					} else {
						//- Because we do not create the boat we have to assign it directly to the boatConstuction.
						boatConstruction = existent;
						boatConstruction.clearCommands();

						//- Update the boat information from the game server. This update is required for interface presentation
						boatConstruction.updateBoatData();
						//- Clear the update date flag to force a new update on the processing.
						boatConstruction.getBoat().clearUpdate();
					}

					NavAttributes attr = new NavAttributes();
					attr.add("type", "ROUTE");
					this.startElement("pilotcommand", attr);
					attr.add("border", "NOBORDER");
					attr.add("action", "connect");
					this.startElement("limit", attr);
					this.startElement("waypointlist", new NavAttributes());
				} catch (final InvalidContentException ice) {
					//- Continue processing the file
					System.out.println("EEE - " + ice.getLocalizedMessage());
					return false;
				} catch (final BoatNotFoundException bnfe) {
					//- Continue processing the file
					System.out.println("EEE - " + bnfe.getLocalizedMessage());
					return false;
				} catch (final DataLoadingException dle) {
					//- Continue processing the file
					System.out.println("EEE - " + dle.getLocalizedMessage());
					return false;
				} catch (final SAXException saxe) {
					//- Continue processing the file
					System.out.println("EEE - " + saxe.getLocalizedMessage());
					return false;
				}
				return true;
			} else return false;
		} catch (final RuntimeException exc) {
			//- Detect any type of runtime exception and bypass it.
			System.out.println("EEE - " + exc.getLocalizedMessage());
			return false;
		}
	}
}

// - UNUSED CODE ............................................................................................
