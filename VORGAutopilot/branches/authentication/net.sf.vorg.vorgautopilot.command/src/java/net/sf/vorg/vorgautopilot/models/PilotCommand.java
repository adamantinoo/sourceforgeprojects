//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.core.AbstractXMLHandler;
import net.sf.core.exceptions.InvalidContentException;
import net.sf.gef.core.model.AbstractGEFNode;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.CommandStatus;
import net.sf.vorg.core.enums.CommandTypes;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.enums.WaypointTypes;
import net.sf.vorg.core.models.GeoLocation;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This class processes the pilot command section of the boat configuration file and generates the set of
 * commands and waypoints that describe the new boat instructions.
 */
public class PilotCommand extends AbstractGEFNode {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger											logger							= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final long									serialVersionUID		= -5791829360139561174L;
	public static final String								WAYPOINT_ACTIVATED	= "PilotCommand.WAYPOINT_ACTIVATED";
	public static final String								STRUCTURE_CHANGED		= "PilotCommand.STRUCTURE_CHANGED";
	private static final GeoLocation					GALWAY							= new GeoLocation(53, 0, -9, 55);
	private static Hashtable<Integer, Double>	ortoTable						= new Hashtable<Integer, Double>(50);
	static {
		PilotCommand.ortoTable.put(70, 53.0);
		PilotCommand.ortoTable.put(69, 54.0);
		PilotCommand.ortoTable.put(68, 55.0);
		PilotCommand.ortoTable.put(67, 55.0);
		PilotCommand.ortoTable.put(66, 56.0);
		PilotCommand.ortoTable.put(65, 57.0);
		PilotCommand.ortoTable.put(64, 58.0);
		PilotCommand.ortoTable.put(63, 58.0);
		PilotCommand.ortoTable.put(62, 59.0);
		PilotCommand.ortoTable.put(61, 60.0);
		PilotCommand.ortoTable.put(60, 60.0);
		PilotCommand.ortoTable.put(59, 61.0);
		PilotCommand.ortoTable.put(58, 62.0);
		PilotCommand.ortoTable.put(57, 63.0);
		PilotCommand.ortoTable.put(56, 63.0);
		PilotCommand.ortoTable.put(55, 64.0);
		PilotCommand.ortoTable.put(54, 65.0);
		PilotCommand.ortoTable.put(53, 66.0);
		PilotCommand.ortoTable.put(52, 66.0);
		PilotCommand.ortoTable.put(51, 67.0);
		PilotCommand.ortoTable.put(50, 68.0);
		PilotCommand.ortoTable.put(49, 69.0);
		PilotCommand.ortoTable.put(48, 70.0);
		PilotCommand.ortoTable.put(47, 70.0);
		PilotCommand.ortoTable.put(46, 71.0);
		PilotCommand.ortoTable.put(45, 72.0);
		PilotCommand.ortoTable.put(44, 73.0);
		PilotCommand.ortoTable.put(43, 73.0);
		PilotCommand.ortoTable.put(42, 74.0);
		PilotCommand.ortoTable.put(41, 75.0);
		PilotCommand.ortoTable.put(40, 76.0);
		PilotCommand.ortoTable.put(39, 77.0);
		PilotCommand.ortoTable.put(38, 77.0);
		PilotCommand.ortoTable.put(37, 78.0);
		PilotCommand.ortoTable.put(36, 79.0);
		PilotCommand.ortoTable.put(35, 80.0);
		PilotCommand.ortoTable.put(34, 81.0);
		PilotCommand.ortoTable.put(33, 81.0);
		PilotCommand.ortoTable.put(32, 82.0);
		PilotCommand.ortoTable.put(31, 83.0);
		PilotCommand.ortoTable.put(30, 84.0);
		PilotCommand.ortoTable.put(29, 85.0);
		PilotCommand.ortoTable.put(28, 85.0);
		PilotCommand.ortoTable.put(27, 86.0);
		PilotCommand.ortoTable.put(26, 87.0);
		PilotCommand.ortoTable.put(25, 88.0);
		PilotCommand.ortoTable.put(24, 89.0);
		PilotCommand.ortoTable.put(23, 89.0);
		PilotCommand.ortoTable.put(22, 90.0);
		PilotCommand.ortoTable.put(21, 91.0);
		PilotCommand.ortoTable.put(20, 92.0);
		PilotCommand.ortoTable.put(19, 93.0);
		PilotCommand.ortoTable.put(18, 93.0);
		PilotCommand.ortoTable.put(17, 94.0);
		PilotCommand.ortoTable.put(16, 95.0);
		PilotCommand.ortoTable.put(15, 96.0);
		PilotCommand.ortoTable.put(14, 97.0);
		PilotCommand.ortoTable.put(13, 97.0);
		PilotCommand.ortoTable.put(12, 98.0);
		PilotCommand.ortoTable.put(11, 99.0);
		PilotCommand.ortoTable.put(10, 99.0);
		PilotCommand.ortoTable.put(9, 100.0);
		PilotCommand.ortoTable.put(8, 100.0);
	}
	// - F I E L D - S E C T I O N ............................................................................
	private CommandTypes											type								= CommandTypes.NOCOMMAND;
	private Vector<PilotLimits>								limits							= new Vector<PilotLimits>();
	private Vector<Waypoint>									waypointList				= new Vector<Waypoint>();
	private ActiveWaypoint										activeWaypoint			= null;
	private final Calendar										lastGeneratedRoute	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommand(final String typeName) {
		type = CommandTypes.decodeType(typeName);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addLimit(final PilotLimits newLimit) {
		if (null != newLimit) {
			limits.add(newLimit);
		}
	}

	public void addWaypoint(final Waypoint newWaypoint) {
		waypointList.add(newWaypoint);
		addChild(newWaypoint);
		newWaypoint.setState(WaypointStates.NOACTIVE);
	}

	public void cleanActiveWaypoint() {
		if (null != activeWaypoint) {
			activeWaypoint.deactivate();
		}
		activeWaypoint = null;
	}

	public void clearWaypoints() {
		final Vector<Waypoint> oldWaypoints = waypointList;
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint waypoint = wit.next();
			removeChild(waypoint);
		}
		waypointList = new Vector<Waypoint>();
		fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, waypointList);
		((PilotBoat) parent).setDirty(true);
	}

	public String generatePersistentXML() {
		final StringBuffer buffer = new StringBuffer();
		//- Iterate though the children to dump also all other persistent information.
		buffer.append("      <pilotcommand").append(" type=").append(this.quote(type.toString())).append(">").append(
				VORGConstants.NEWLINE);
		buffer.append("      <pilotlimits>").append(VORGConstants.NEWLINE);
		final Iterator<PilotLimits> lit = limits.iterator();
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			buffer.append(limit.generatePersistentXML());
		}
		buffer.append("      </pilotlimits>").append(VORGConstants.NEWLINE);
		buffer.append("      <waypointlist>").append(VORGConstants.NEWLINE);
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint waypoint = wit.next();
			buffer.append(waypoint.generatePersistentXML("waypoint"));
		}
		buffer.append("      </waypointlist>").append(VORGConstants.NEWLINE);
		buffer.append("      </pilotcommand>").append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	public ActiveWaypoint getActiveWaypoint() {
		return activeWaypoint;
	}

	public String getType() {
		return type.toString();
	}

	public Vector<Waypoint> getWaypoints() {
		return waypointList;
	}

	/**
	 * Perform the pilot activity on the associated Boat. If the command has an active waypoint, choose it for
	 * execution. Otherwise scan to select the next waypoint.
	 */
	public void pilot() {
		//		//- Generate the printing report.
		//		StringBuffer reportBuffer = new StringBuffer(VORGConstants.NEWLINE);
		System.out.println();
		if (type == CommandTypes.NOCOMMAND) {
			System.out.println(Calendar.getInstance().getTime() + " - " + "Not processing. Command not Active" + this);
			return;
		}
		if (type == CommandTypes.ROUTE) if ((null != parent) && (parent instanceof PilotBoat)) {
			pilotWaypoint();
			//			printReport();
			//			//- If there is an active waypoint. Update the contents and perform the calculations.
			//			if (null != activeWaypoint) {
			//				//- Check if this point has been surpassed. Is so cancel it and search for a new one.
			//				if (activeWaypoint.isActive()) {
			//					final Boat refBoat = ((PilotBoat) parent).getBoat();
			//					// - Make the waypoint to check its own surpassed algorithm.
			//					if (activeWaypoint.checkSurpassed(refBoat.getLocation())) {
			//						refBoat.surpassWaypoint(activeWaypoint);
			//						activeWaypoint = null;
			//						pilot();
			//						fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
			//					} else {
			//						activeWaypoint.activateWaypoint();
			//					}
			//				} else {
			//					//- The active waypoint is no more active. Clear it and for for another waypoint.
			//					activeWaypoint = null;
			//					pilot();
			//					fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
			//				}
			//			} else {
			//				evaluateWaypoints(((PilotBoat) parent).getBoat());
			//			}
			//		if (type == CommandTypes.AUTO) if ((null != parent) && (parent instanceof PilotBoat)) {
			//			//- New type of command to set the automatic waypoint generator bases on isochrones
			//			//			printReport();
			//			//- If current time are the ones to update routes enter the new route generator
			//			if (null == lastGeneratedRoute) generateNewIsoRoute(this);
			//			final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
			//			if ((now.get(Calendar.HOUR_OF_DAY) == 11) || (now.get(Calendar.HOUR_OF_DAY) == 23))
			//				if (now.get(Calendar.MINUTE) > 2) {
			//					//- Check if the route was updated less than six hours ago
			//					now.add(Calendar.HOUR, -3);
			//					if (now.after(lastGeneratedRoute)) generateNewIsoRoute(this);
			//				}
			//
			//			//- After the route is generated, use the waypoints as ever.
			//			pilotWaypoint();
		} else {
			System.out.println(Calendar.getInstance().getTime() + " - " + "Invalid data for command processing");
		}
	}

	public void startElement(final String name, final Attributes attributes) throws SAXException {
		if (name.toLowerCase().equals("pilotlimits")) {
			limits = new Vector<PilotLimits>();
		}
		if (name.toLowerCase().equals("pilotlimit")) {
			final PilotLimits newLimit = new PilotLimits();
			try {
				newLimit.setBorder(AbstractXMLHandler.svalidateNotNull(attributes, "border"));
				newLimit.setLatitude(AbstractXMLHandler.svalidateNull(attributes, "latitude"));
				newLimit.setLongitude(AbstractXMLHandler.svalidateNull(attributes, "longitude"));
				newLimit.setAction(AbstractXMLHandler.svalidateNull(attributes, "action"));
				limits.add(newLimit);
			} catch (final InvalidContentException ice) {
				System.out.println("EEE - " + ice.getLocalizedMessage());
			}
		}
		if (name.toLowerCase().equals("waypointlist")) {
			waypointList = new Vector<Waypoint>();
		}
		if (name.toLowerCase().equals("waypoint")) {
			try {
				final WaypointTypes type = WaypointTypes.decodeType(AbstractXMLHandler.svalidateNotNull(attributes, "type"));
				final Waypoint waypoint = new Waypoint(type);
				waypoint.setName(AbstractXMLHandler.svalidateNull(attributes, "name"));
				waypoint.setLatitude(AbstractXMLHandler.svalidateNotNull(attributes, "latitude"));
				waypoint.setLongitude(AbstractXMLHandler.svalidateNotNull(attributes, "longitude"));
				waypoint.setRange(AbstractXMLHandler.svalidateNull(attributes, "range"));
				waypoint.setMinAWD(AbstractXMLHandler.svalidateNull(attributes, "minAWD"));
				waypoint.setMaxAWD(AbstractXMLHandler.svalidateNull(attributes, "maxAWD"));
				waypoint.setAngleParameter(AbstractXMLHandler.svalidateNull(attributes, "angle"));
				if (type == WaypointTypes.ANGLE) {
					waypoint.setAngleParameter(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				}
				if (type == WaypointTypes.VMG) {
					waypoint.setAngleParameter(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				}
				if (type == WaypointTypes.AWD) {
					waypoint.setSelectedAWD(AbstractXMLHandler.svalidateNotNull(attributes, "angle"));
				}
				addWaypoint(waypoint);
			} catch (final InvalidContentException ice) {
				System.out.println("EEE - " + ice.getLocalizedMessage());
			}
		}
	}

	/** Checks if this command is active or not depending on the state and the actions of its limits. */
	public CommandStatus testActivity() {
		// - Check if the command is active or inactive by limits.
		CommandStatus status = CommandStatus.NOCHANGE;
		//- If a command has no limits it is considered active.
		if (limits.size() == 0) return CommandStatus.GO;
		final Iterator<PilotLimits> lit = limits.iterator();
		GeoLocation boatLocation = null;
		if (null != parent) if (parent instanceof PilotBoat) {
			boatLocation = ((PilotBoat) parent).getBoat().getLocation();
		}
		if (null == boatLocation) return CommandStatus.NOGO;
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			//- Check this limit to the current boat location, that was obtained before.
			final CommandStatus test = limit.testLimit(boatLocation);
			if (test != CommandStatus.NOCHANGE) {
				status = test;
			}
		}
		return status;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[PilotCommand ");
		buffer.append("type=").append(type).append("\n");
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			buffer.append("").append(wp).append("\n");
		}
		return buffer.toString();
	}

	private void evaluateWaypoints(final Boat boat) {
		// - Get the current boat location.
		final GeoLocation boatLocation = boat.getLocation();
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			//- Send the boat to the waypoint for internal reference.
			wp.setBoat(boat);
			// - Test if the waypoint has been surpassed by checking the points of the boat route
			if (boat.checkSurpassed(wp)) {
				wp.setState(WaypointStates.SURPASSED);
			}
			// - Check if the waypoint is still active.
			if (wp.isActive()) {
				//- Make the waypoint to check its own surpassed algorithm.
				if (wp.checkSurpassed(boatLocation)) {
					boat.surpassWaypoint(wp);
					continue;
				}
				activeWaypoint = wp.activateWaypoint();
				firePropertyChange(PilotCommand.WAYPOINT_ACTIVATED, null, activeWaypoint);
				break;
			}
		}
	}

	//	/**
	//	 * Uses the RouteCalculator to generate a new isochrone route and then gets the first wind change waypoint
	//	 * to set the route path.
	//	 */
	//	private void generateNewIsoRoute(final PilotCommand pilotCommand) {
	//		//- Get the orthodromic angle for the projection.
	//		final GeoLocation boatLocation = ((PilotBoat) parent).getBoat().getLocation();
	//		final int orthoAngle = getOrtodromicAngle(boatLocation) + 1;
	//		final IsochroneRouter router = new IsochroneRouter(RouterType.ISO);
	//		router.setOrtoAngle(orthoAngle);
	//		router.generateIsochrone(boatLocation, PilotCommand.GALWAY);
	//		final Route route = router.getBestRoute();
	//		final GeoLocation waypointLocation = route.getWindChange(1);
	//
	//		//- Create the list of waypoint for a new command
	//		cleanActiveWaypoint();
	//		clearWaypoints();
	//		final Waypoint vmgWaypoint = new Waypoint(WaypointTypes.VMG);
	//		vmgWaypoint.setName("AUTO VMG " + orthoAngle);
	//		vmgWaypoint.setLatitude(new Double(waypointLocation.getLat()).toString());
	//		vmgWaypoint.setLongitude(new Double(waypointLocation.getLon()).toString());
	//		vmgWaypoint.setRange("3");
	//		vmgWaypoint.setMinAWD("45");
	//		vmgWaypoint.setMaxAWD("150");
	//		vmgWaypoint.setAngleParameter(new Integer(orthoAngle).toString());
	//		addWaypoint(vmgWaypoint);
	//
	//		final Waypoint targetWaypoint = new Waypoint(WaypointTypes.VMGLOX);
	//		targetWaypoint.setName("GALWAY-VMGLOX");
	//		targetWaypoint.setLatitude(new Double(PilotCommand.GALWAY.getLat()).toString());
	//		targetWaypoint.setLongitude(new Double(PilotCommand.GALWAY.getLon()).toString());
	//		targetWaypoint.setRange("3");
	//		targetWaypoint.setMinAWD("45");
	//		targetWaypoint.setMaxAWD("150");
	//		addWaypoint(targetWaypoint);
	//
	//		lastGeneratedRoute = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
	//		fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, waypointList);
	//		((PilotBoat) parent).setDirty(true);
	//	}

	/** Lookups the current longitude in the ortodromic table to get the best projection angle */
	private int getOrtodromicAngle(final GeoLocation location) {
		final int longitude = new Double(Math.abs(Math.round(location.getLon()))).intValue();
		final Double ortho = PilotCommand.ortoTable.get(longitude);
		if (null == ortho) return 90;
		return ortho.intValue();
	}

	private void pilotWaypoint() {
		printReport();
		//- If there is an active waypoint. Update the contents and perform the calculations.
		if (null != activeWaypoint) {
			//- Check if this point has been surpassed. Is so cancel it and search for a new one.
			if (activeWaypoint.isActive()) {
				final Boat refBoat = ((PilotBoat) parent).getBoat();
				// - Make the waypoint to check its own surpassed algorithm.
				if (activeWaypoint.checkSurpassed(refBoat.getLocation())) {
					refBoat.surpassWaypoint(activeWaypoint);
					activeWaypoint = null;
					pilot();
					fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
				} else {
					activeWaypoint.activateWaypoint();
				}
			} else {
				//- The active waypoint is no more active. Clear it and for for another waypoint.
				activeWaypoint = null;
				pilot();
				fireStructureChange(PilotCommand.STRUCTURE_CHANGED, this, activeWaypoint);
			}
		} else {
			evaluateWaypoints(((PilotBoat) parent).getBoat());
		}
	}

	private void printReport() {
		System.out.println("==========================================================================================");
		System.out.println(Calendar.getInstance().getTime() + " - " + "Processing Boat -- "
				+ ((PilotBoat) parent).getBoatName().toUpperCase());
		System.out.println(((PilotBoat) parent).getBoat().printReport());
		System.out.println();
		System.out.println("Processing configuration command" + this);
	}
}
// - UNUSED CODE ............................................................................................
