//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class AutopilotRunner implements Runnable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.core");

	// - F I E L D - S E C T I O N ............................................................................
	private final PilotModelStore	referenceStore;
	private final String					name;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AutopilotRunner(final PilotModelStore store, final String name) {
		referenceStore = store;
		this.name = name;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void run() {
		AutopilotRunner.logger.info(">>> Entering Processing Autopilot Loop");
		if (null != referenceStore) {
			referenceStore.checkMapReload();
			referenceStore.performOperation();
		}
		AutopilotRunner.logger.info("<<< Exiting Processing Autopilot Loop");
	}
}

// - UNUSED CODE ............................................................................................
