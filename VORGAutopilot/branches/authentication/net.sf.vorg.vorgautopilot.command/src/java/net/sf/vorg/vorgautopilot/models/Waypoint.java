//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.enums.WaypointTypes;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.singletons.FormatSingletons;

// - CLASS IMPLEMENTATION ...................................................................................
public class Waypoint extends PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final long		serialVersionUID			= -1058670010401234442L;
	private static double				DEFAULT_RANGE					= 4.0;
	private static final int		DEFAULT_MINAWD				= 30;
	private static final int		DEFAULT_MAXAWD				= 180;
	public static final String	STATE_CHANGED					= "Waypoint.STATE_CHANGED";
	public static final String	WAYPOINT_ACTIVATED		= "Waypoint.WAYPOINT_ACTIVATED";
	public static final String	WAYPOINT_EXECUTED			= "Waypoint.WAYPOINT_EXECUTED";
	public static final String	WAYPOINT_DEACTIVATED	= "Waypoint.WAYPOINT_DEACTIVATED";
	public static final String	WAYPOINT_SURPASSED		= "Waypoint.WAYPOINT_SURPASSED";

	// - F I E L D - S E C T I O N ............................................................................
	protected WaypointTypes			type									= WaypointTypes.NOACTION;
	protected WaypointStates		state									= WaypointStates.NOACTIVE;
	protected Boat							boatParent						= null;
	protected ActiveWaypoint		activeWaypoint				= null;

	//- COMMON DATA
	protected String						name									= null;
	protected double						range									= Waypoint.DEFAULT_RANGE;
	protected int								selectedAWD						= Waypoint.DEFAULT_MINAWD;
	//- WIND DATA
	protected double						course								= 0.0;
	protected int								minAWD								= Waypoint.DEFAULT_MINAWD;
	protected int								maxAWD								= Waypoint.DEFAULT_MAXAWD;
	//- TIME DATA
	private final int						minutesActive					= 0;
	private final Calendar			endTimePoint					= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Waypoint(final WaypointTypes newType) {
		setType(newType);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public ActiveWaypoint activateWaypoint() {
		if (null != boatParent) {
			if (null == activeWaypoint) {
				activeWaypoint = new ActiveWaypoint(this);
			}
			setState(WaypointStates.ACTIVE);
			final StringBuffer buffer = new StringBuffer();
			buffer.append("Activating route to waypoint:").append(VORGConstants.NEWLINE);
			buffer.append(toString()).append(VORGConstants.NEWLINE);
			if (getType() == WaypointTypes.POINT) {
				// - Get the direction to the waypoint and adjust the course and polars.
				final double angle = boatParent.getLocation().angleTo(getLocation());
				final double newHeading = adjustSailAngle(angle);
				final double awd = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), angle);
				buffer.append("[direct angle= ").append(FormatSingletons.nf2.format(angle)).append(" - ");
				buffer.append("AWD= ").append(FormatSingletons.nf2.format(awd)).append(" ");
				buffer.append("distance= ");
				buffer.append(FormatSingletons.nf4.format(getDistance(boatParent.getLocation()))).append("]").append(
						VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(angle);
				activeWaypoint.setSelectedCourse(newHeading);
				activeWaypoint.setVMC(new VMCData(newHeading, getCurrentWindCell()));
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newHeading);
			}
			if (getType() == WaypointTypes.ANGLE) {
				//- The direction is configures in the waypoint.
				final double angle = getCourse();
				final double newHeading = adjustSailAngle(angle);
				final double awd = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), angle);
				buffer.append("[direct angle= ").append(FormatSingletons.nf2.format(angle)).append(" - ");
				buffer.append("AWD= ").append(FormatSingletons.nf2.format(awd)).append(" ");
				buffer.append("distance= ");
				buffer.append(FormatSingletons.nf4.format(getDistance(boatParent.getLocation()))).append("]").append(
						VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(angle);
				activeWaypoint.setSelectedCourse(newHeading);
				activeWaypoint.setVMC(new VMCData(newHeading, getCurrentWindCell()));
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newHeading);
			}
			if (getType() == WaypointTypes.VMG) {
				final double angle = getCourse();
				final VMCData vmc = new VMCData(angle, getCurrentWindCell());
				final double newHeading = adjustSailAngle(vmc.getBestAngle());
				buffer.append(vmc.printReport()).append(VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(angle);
				activeWaypoint.setSelectedCourse(newHeading);
				activeWaypoint.setVMC(vmc);
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newHeading);
			}
			if (getType() == WaypointTypes.VMGLOX) {
				final double angle = boatParent.getLocation().angleTo(getLocation());
				final VMCData vmc = new VMCData(angle, getCurrentWindCell());
				final double newHeading = adjustSailAngle(vmc.getBestAngle());
				buffer.append(vmc.printReport()).append(VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(angle);
				activeWaypoint.setSelectedCourse(newHeading);
				activeWaypoint.setVMC(vmc);
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newHeading);
			}
			if (getType() == WaypointTypes.AWD) {
				double boatAngle = boatParent.getHeading();
				final double windDir = getCurrentWindCell().getWindDir();
				final int currentAWD = GeoLocation.calculateAWD(windDir, boatAngle);
				boatAngle = GeoLocation.adjustAngleTo360(windDir - getSelectedAWD());
				final double newHeading = adjustSailAngle(boatAngle);
				buffer.append("[calculated course= ").append(FormatSingletons.nf2.format(boatAngle)).append(" - ");
				buffer.append("AWD= ").append(FormatSingletons.nf2.format(getSelectedAWD())).append(" ");
				buffer.append("distance= ");
				buffer.append(FormatSingletons.nf4.format(getDistance(boatParent.getLocation()))).append("]").append(
						VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(boatAngle);
				activeWaypoint.setSelectedCourse(newHeading);
				activeWaypoint.setVMC(new VMCData(newHeading, getCurrentWindCell()));
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newHeading);
			}
			if (getType() == WaypointTypes.MAX) {
				final int angle = boatParent.getHeading();
				final VMCData vmc = new VMCData(angle, getCurrentWindCell());
				final int localMaxAWD = vmc.getMaxAWD();
				//- Calculate the Port and Starboard angles.
				final int portAngle = GeoLocation.adjustAngleTo360(getCurrentWindCell().getWindDir() + localMaxAWD);
				final int starboardAngle = GeoLocation.adjustAngleTo360(getCurrentWindCell().getWindDir() - localMaxAWD);
				double diffPort = GeoLocation.angleDifference(angle, portAngle);
				//- Convert a 360.0 back to 0.0 for this comparison.
				if (diffPort == 360.0) {
					diffPort = 0.0;
				}
				double diffStarboard = GeoLocation.angleDifference(angle, starboardAngle);
				//- Check for 360.0 values to be converted back to 0.0
				if (diffPort == 360.0) {
					diffPort = 0.0;
				}
				if (diffStarboard == 360.0) {
					diffStarboard = 0.0;
				}
				double newCourse;
				if (diffPort < diffStarboard) {
					newCourse = adjustSailAngle(portAngle);
				} else {
					newCourse = adjustSailAngle(starboardAngle);
				}
				buffer.append(vmc.printReport()).append(VORGConstants.NEWLINE);
				System.out.print(buffer.toString());
				activeWaypoint.setEstimatedCourse(boatParent.getHeading());
				activeWaypoint.setSelectedCourse(newCourse);
				activeWaypoint.setVMC(vmc);
				activeWaypoint.setDistance(getDistance(boatParent.getLocation()));
				activeWaypoint.sendCommand(boatParent, newCourse);
			}
			return activeWaypoint;
		}
		return null;
	}

	/**
	 * Checks the state of this point with respect to the boat location or the current time or the list of
	 * walked points.
	 */
	public boolean checkSurpassed(final GeoLocation boatLocation) {
		//		//- Check the time point
		//		if (null != endTimePoint) {
		//			final Calendar now = Calendar.getInstance();
		//			endTimePoint.before(now);
		//			setState(WaypointStates.SURPASSED);
		//			return true;
		//		}
		// - Calculate distance to waypoint. Discard it if the distance is less than control.
		final GeoLocation waypointLocation = getLocation();
		final double distance = waypointLocation.distance(boatLocation);
		if (distance < getRange()) {
			System.out.println("--- Discarded waypoint " + waypointLocation + ". Distance " + distance + " below "
					+ getRange() + " miles.");
			setState(WaypointStates.SURPASSED);
			return true;
		}
		//- Check the angle only for this type of waypoints.
		if ((type == WaypointTypes.ANGLE) || (type == WaypointTypes.VMG)) {
			//- Calculate the angle form the boat to the point. If crossed the perpendicular then the point is out
			double referenceAngle = getCourse();
			//		if (null == boatParent) boatParent = ((PilotBoat) ((PilotCommand) parent).parent).getBoat();
			if (type == WaypointTypes.POINT) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.VMGLOX) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.AWD) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.MAX) {
				referenceAngle = boatParent.getHeading();
			}
			//		if (referenceAngle >= 0.0) {
			final double boatAngle = GeoLocation.adjustAngleTo360(boatLocation.angleTo(getLocation()) - referenceAngle);
			if ((boatAngle >= 90.0) && (boatAngle <= 270.0)) {
				System.out.println("--- Discarded waypoint " + getLocation() + ". Crossed line with angle " + boatAngle);
				setState(WaypointStates.SURPASSED);
				return true;
			}
		}
		return false;
	}

	public String generatePersistentXML(final String typecast) {
		final StringBuffer buffer = new StringBuffer();
		//- Compose the waypoint data line.
		buffer.append("        <").append(typecast).append(" ");
		if (null != name) {
			buffer.append("name=").append(this.quote(name)).append(" ");
		}
		buffer.append("type=").append(this.quote(type.toString())).append(" ");
		if (type == WaypointTypes.ANGLE) {
			buffer.append("angle=").append(this.quote(course)).append(" ");
		}
		if (type == WaypointTypes.VMG) {
			buffer.append("angle=").append(this.quote(course)).append(" ");
		}
		if (type == WaypointTypes.AWD) {
			buffer.append("angle=").append(this.quote(getSelectedAWD())).append(" ");
		}
		buffer.append("range=").append(this.quote(range)).append(" ");
		buffer.append("minAWD=").append(this.quote(minAWD)).append(" ");
		buffer.append("maxAWD=").append(this.quote(maxAWD)).append(" ");
		buffer.append("latitude=").append(this.quote(getLocation().getLat())).append(" ");
		buffer.append("longitude=").append(this.quote(getLocation().getLon())).append(" ");
		if (null != endTimePoint) {
			buffer.append("endTime=").append(this.quote(endTimePoint.getTime().toString())).append(" ");
		}
		buffer.append("/>").append(VORGConstants.NEWLINE);
		return buffer.toString();
	}

	public double getCourse() {
		return course;
	}

	public double getDistance(final GeoLocation boatLocation) {
		return getLocation().distance(boatLocation);
	}

	public int getMaxAWD() {
		return maxAWD;
	}

	public int getMinAWD() {
		return minAWD;
	}

	public String getName() {
		return name;
	}

	public double getRange() {
		return range;
	}

	public int getSelectedAWD() {
		return selectedAWD;
	}

	public WaypointStates getState() {
		return state;
	}

	public WaypointTypes getType() {
		return type;
	}

	public int getWindDir() {
		//	  if(null==boatWindCell);
		return getCurrentWindCell().getWindDir();
	}

	public double getWindSpeed() {
		//	  if(null==boatWindCell)getCurrentWindCell();
		return getCurrentWindCell().getWindSpeed();
	}

	/**
	 * Checks the state of the waypoint to follow the logic to just activate the waypoint not surpassed, with an
	 * active type and not deactivated by manual intervention.
	 */
	public boolean isActive() {
		if (type == WaypointTypes.NOACTION) return false;
		if (state == WaypointStates.NOACTIVE) return true;
		if (state == WaypointStates.ONHOLD) return false;
		if (state == WaypointStates.SURPASSED) return false;
		return true;
	}

	public boolean isEquivalent(final Waypoint targetWaypoint) {
		return targetWaypoint.getLocation().isEquivalent(getLocation());
	}

	public void setAngleParameter(final String value) {
		if (null != value) {
			try {
				course = GeoLocation.adjustAngleTo360(new Double(value).intValue());
			} catch (final Exception e) {
				//- If the assignment fails set the angle to the current heading
				if (null != boatParent) {
					course = boatParent.getHeading();
				}
			}
		}
	}

	public void setBoat(final Boat boat) {
		if (null != boat) {
			boatParent = boat;
		}
	}

	public void setMaxAWD(final int value) {
		maxAWD = Math.abs(value);
		if (Math.abs(value) > Waypoint.DEFAULT_MAXAWD) {
			maxAWD = Waypoint.DEFAULT_MAXAWD;
		}
		if (Math.abs(value) < Waypoint.DEFAULT_MINAWD) {
			maxAWD = Waypoint.DEFAULT_MINAWD;
		}
		if (Math.abs(value) < minAWD) {
			maxAWD = minAWD;
		}
	}

	public void setMaxAWD(final String value) {
		if (null == value) {
			this.setMaxAWD(Waypoint.DEFAULT_MAXAWD);
			return;
		}
		try {
			this.setMaxAWD(new Double(value).intValue());
			if (Math.abs(maxAWD) < Math.abs(minAWD)) {
				maxAWD = minAWD;
			}
		} catch (final Exception e) {
			this.setMaxAWD(Waypoint.DEFAULT_MAXAWD);
		}
	}

	public void setMinAWD(final int value) {
		minAWD = Math.abs(value);
		if (Math.abs(value) < Waypoint.DEFAULT_MINAWD) {
			minAWD = Waypoint.DEFAULT_MINAWD;
		}
		if (Math.abs(value) > Waypoint.DEFAULT_MAXAWD) {
			minAWD = Waypoint.DEFAULT_MAXAWD;
		}
		if (Math.abs(value) > maxAWD) {
			minAWD = maxAWD;
		}
	}

	public void setMinAWD(final String value) {
		if (null == value) {
			this.setMinAWD(Waypoint.DEFAULT_MINAWD);
			return;
		}
		try {
			this.setMinAWD(new Double(value).intValue());
		} catch (final Exception e) {
			this.setMinAWD(Waypoint.DEFAULT_MINAWD);
		}
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setRange(final String value) {
		if (null == value) {
			range = Waypoint.DEFAULT_RANGE;
			return;
		}
		try {
			range = new Double(value);
		} catch (final Exception e) {
			range = Waypoint.DEFAULT_RANGE;
		}
	}

	public void setSelectedAWD(final String value) {
		if (null != value) {
			try {
				selectedAWD = new Double(value).intValue();
			} catch (final Exception e) {
			}
		}
	}

	public void setState(final WaypointStates newState) {
		final WaypointStates oldState = state;
		state = newState;
		firePropertyChange(Waypoint.STATE_CHANGED, oldState, newState);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Waypoint ");
		buffer.append(type).append(" ");
		if (null != name) {
			buffer.append('"').append(name).append('"');
		}
		buffer.append("  ");
		if (type == WaypointTypes.ANGLE) {
			buffer.append("angle=").append(course).append(", ");
		}
		if (type == WaypointTypes.AWD) {
			buffer.append("angle=").append(getSelectedAWD()).append(", ");
		}
		if (type == WaypointTypes.MAX) {
			buffer.append("range=").append(range).append(", ");
		}
		if (type == WaypointTypes.POINT) {
			buffer.append("range=").append(range).append(", ");
		}
		if (type == WaypointTypes.VMG) {
			buffer.append("angle=").append(course).append(", ");
		}
		buffer.append("AWD=").append(minAWD).append("/").append(maxAWD).append(", ");
		buffer.append(getLocation()).append("");
		if (null != boatParent) {
			double referenceAngle = getCourse();
			if (type == WaypointTypes.POINT) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.VMGLOX) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.AWD) {
				referenceAngle = boatParent.getHeading();
			}
			if (type == WaypointTypes.MAX) {
				referenceAngle = boatParent.getHeading();
			}
			//		if (referenceAngle >= 0.0) {
			final double boatAngle = GeoLocation.adjustAngleTo360(boatParent.getLocation().angleTo(getLocation())
					- referenceAngle);
			buffer.append(", boat/ref Angle=").append(FormatSingletons.nf2.format(boatAngle));
			buffer.append("/").append(FormatSingletons.nf2.format(referenceAngle));
		}
		buffer.append("]");
		return buffer.toString();
	}

	protected WindCell getCurrentWindCell() {
		return boatParent.getWindCell();
	}

	protected void setType(final WaypointTypes newType) {
		type = newType;
		if (newType == WaypointTypes.NOACTION) {
			setState(WaypointStates.ONHOLD);
		}
	}

	private int absInt(final double value) {
		return new Double(Math.abs(value)).intValue();
	}

	/**
	 * Adjusts the sailing angle to not to be up to the wind. The angle is changes until the speed is different
	 * from 0.0 and the angle is between the minimum AWD and max AWD angles.
	 */
	private double adjustSailAngle(final double angle) {
		double newHeading = angle;
		double apparentHeading = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), newHeading);
		//- Adjust AWD to be between the limits.
		if (apparentHeading < 0) {
			//- Process AWD to Port
			if (absInt(apparentHeading) > maxAWD) {
				newHeading -= absInt(apparentHeading) - maxAWD;
			}
			if (absInt(apparentHeading) < minAWD) {
				newHeading += minAWD - absInt(apparentHeading);
			}
		} else {
			if (absInt(apparentHeading) > maxAWD) {
				newHeading += absInt(apparentHeading) - maxAWD;
			}
			if (absInt(apparentHeading) < minAWD) {
				newHeading -= minAWD - absInt(apparentHeading);
			}
		}
		apparentHeading = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), newHeading);
		SailConfiguration sails = Polars
				.lookup(new Double(apparentHeading).intValue(), getCurrentWindCell().getWindSpeed());

		// - Adjust if the speed = 0.0
		boolean awdAboveMin = Math.abs(Math.round(apparentHeading)) < Math.abs(minAWD);
		boolean awdBelowMax = Math.abs(Math.round(apparentHeading)) > Math.abs(maxAWD);
		boolean search = (sails.getSpeed() == 0.0) | awdAboveMin | awdBelowMax;
		int displacement = 1;
		if (apparentHeading > 0) {
			displacement = -1;
		} else {
			displacement = 1;
		}
		while (search) {
			newHeading = GeoLocation.adjustAngleTo360(newHeading + displacement);
			apparentHeading = GeoLocation.calculateAWD(getCurrentWindCell().getWindDir(), newHeading);
			sails = Polars.lookup(new Double(apparentHeading).intValue(), getCurrentWindCell().getWindSpeed());
			awdAboveMin = Math.abs(Math.round(apparentHeading)) < Math.abs(minAWD);
			awdBelowMax = Math.abs(Math.round(apparentHeading)) > Math.abs(maxAWD);
			search = (sails.getSpeed() == 0.0) | awdAboveMin | awdBelowMax;
		}
		return newHeading;
	}
}
// - UNUSED CODE ............................................................................................
