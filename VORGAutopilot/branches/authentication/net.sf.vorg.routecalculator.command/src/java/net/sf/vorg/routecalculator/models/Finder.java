//  PROJECT:        net.sf.vorg.routescanner.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Vector;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Intersection;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class Finder extends ExtendedRoute {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routescanner.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Finder() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/*
	 * Creates a new Route starting at a specific point and going for a number of cells following the
	 * connections that lie at specific apparent angles that match the parameter. The side to be selected is
	 * something that still is not tested, but it tries to follow the starboard side.
	 * 
	 * @param startLocation The location where the boats start the route
	 * 
	 * @param cellsToScan number of cells to run
	 * 
	 * @param maxAWD max apparent wind direction angle to follow. This is the exact route to follow.
	 * 
	 * @return the route to follow
	 */
	public Route getAWDRoute(final GeoLocation startLocation, final int cellsToScan, final int selectedAWD) {
		final Route foundRoute = new Route();
		try {
			// - Initialize the timing system to locate the wind cells.
			double elapsed = 0.0;
			Calendar now = foundRoute.getSearchTime();
			final WindCell startCell = WindMapHandler.cellWithPoint(startLocation, now.getTime());
			// - Get the starboard angle. This is the wind direction plus the AWD received
			int starboardAngle = GeoLocation.adjustAngleTo360(startCell.getWindDir() - selectedAWD);

			// - Get the intersection with the cell border and add this cell to the new route.
			Vector<Intersection> intersections = this.getIntersection(startLocation, startCell, starboardAngle);
			Directions nextDirection = intersections.lastElement().getDirection();
			GeoLocation currentRouteLocation = intersections.lastElement().getLocation();
			RouteCell newCell = new RouteCell(startCell, startLocation, currentRouteLocation);
			double cellTTC = newCell.getTTC();
			if (this.checkIfWindChange(elapsed, cellTTC)) {
				final Vector<Object> changeCells = this.splitRoute(foundRoute, newCell, selectedAWD);
				final Directions lastDirection = (Directions) changeCells.lastElement();
				foundRoute.add((RouteCell) changeCells.get(0), nextDirection);
				foundRoute.add((RouteCell) changeCells.get(1), lastDirection);
				foundRoute.setWindChange();

				final RouteCell newLastCell = (RouteCell) changeCells.get(1);
				currentRouteLocation = newLastCell.getExitLocation();
				nextDirection = lastDirection;
			} else
				foundRoute.add(newCell, nextDirection);
			elapsed = foundRoute.getRouteTTC();
			WindCell nextCell = startCell;

			for (int i = 1; i <= cellsToScan; i++) {
				// - Get the next cell in the line using the direction and the connection with the cell boundaries.
				now = foundRoute.getSearchTime();
				final GeoLocation nextLocation = nextCell.cellAtDirection(nextDirection);
				nextCell = WindMapHandler.cellWithPoint(nextLocation, now.getTime());
				starboardAngle = GeoLocation.adjustAngleTo360(nextCell.getWindDir() - selectedAWD);
				intersections = this.getIntersection(currentRouteLocation, nextCell, starboardAngle);
				nextDirection = intersections.lastElement().getDirection();
				// If the next location in the route is just the same entering point we have been reflected
				// and the AWD can not continue. Log this and mar the waypoint as the last one.
				if (currentRouteLocation.isEquivalent(intersections.lastElement().getLocation())) {
					System.out.println("Reflection point. We can not enter the cell because we are reflected. End of route.");
					return foundRoute;
				}
				newCell = new RouteCell(nextCell, currentRouteLocation, intersections.lastElement().getLocation());
				currentRouteLocation = intersections.lastElement().getLocation();
				cellTTC = newCell.getTTC();
				if (this.checkIfWindChange(elapsed, cellTTC)) {
					final Vector<Object> changeCells = this.splitRoute(foundRoute, newCell, selectedAWD);
					final Directions lastDirection = (Directions) changeCells.lastElement();
					foundRoute.add((RouteCell) changeCells.get(0), nextDirection);
					foundRoute.add((RouteCell) changeCells.get(1), lastDirection);
					foundRoute.setWindChange();

					final RouteCell newLastCell = (RouteCell) changeCells.get(1);
					currentRouteLocation = newLastCell.getExitLocation();
					nextDirection = lastDirection;
				} else
					foundRoute.add(newCell, nextDirection);
				elapsed = foundRoute.getRouteTTC();
			}
		} catch (final LocationNotInMap e) {
			e.printStackTrace();
		}
		return foundRoute;
	}

	public Route getMaxSpeed(final GeoLocation startLocation, final int cellsToScan) {
		final Route directRoute = new Route();
		try {
			GeoLocation nextLocation = startLocation;
			Calendar now = this.getSearchTime();
			final WindCell startCell = WindMapHandler.cellWithPoint(startLocation, now.getTime());
			VMCData vmc = new VMCData(0, startCell);
			final int course = 0;
			int maxSpeedAngle = new Double(this.getMaxAngle4Course(course, vmc, startCell.getWindDir())).intValue();
			Vector<Intersection> intersections = this.getIntersection(startLocation, startCell, maxSpeedAngle);
			directRoute.add(startCell, intersections.lastElement().getDirection(), startLocation, intersections.lastElement()
					.getLocation());
			now = this.getSearchTime();
			GeoLocation thisLocation = intersections.lastElement().getLocation();
			nextLocation = startCell.cellAtDirection(intersections.lastElement().getDirection());
			WindCell nextCell = WindMapHandler.cellWithPoint(nextLocation, now.getTime());

			for (int i = 1; i <= cellsToScan; i++) {
				vmc = new VMCData(0, nextCell);
				maxSpeedAngle = new Double(this.getMaxAngle4Course(maxSpeedAngle, vmc, nextCell.getWindDir())).intValue();
				intersections = this.getIntersection(thisLocation, nextCell, maxSpeedAngle);
				directRoute.add(nextCell, intersections.firstElement().getDirection(), intersections.firstElement()
						.getLocation(), intersections.lastElement().getLocation());
				now = this.getSearchTime();
				thisLocation = intersections.lastElement().getLocation();
				nextLocation = nextCell.cellAtDirection(intersections.lastElement().getDirection());
				nextCell = WindMapHandler.cellWithPoint(nextLocation, now.getTime());
			}
		} catch (final LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return directRoute;
	}

	/*
	 * Creates a new Route starting at a specific point and going for a number of cells following the best VMG
	 * based on a direction received as a parameter.
	 * 
	 * @param startLocation The location where the boats start the route
	 * 
	 * @param cellsToScan number of cells to run
	 * 
	 * @param maxAWD max apparent wind direction angle to follow. This is the exact route to follow.
	 * 
	 * @return the route to follow
	 */
	public Route getVMGRoute(final GeoLocation startLocation, final int cellsToScan, final int vmgAngle) {
		final Route foundRoute = new Route();
		try {
			// - Initialize the timing system to locate the wind cells.
			double elapsed = 0.0;
			Calendar now = foundRoute.getSearchTime();
			final WindCell startCell = WindMapHandler.cellWithPoint(startLocation, now.getTime());
			// - Get the best VMG angle
			VMCData vmc = new VMCData(vmgAngle, startCell);
			int bestAngle = vmc.getBestAngle();

			// - Get the intersection with the cell border and add this cell to the new route.
			Vector<Intersection> intersections = this.getIntersection(startLocation, startCell, bestAngle);
			Directions nextDirection = intersections.lastElement().getDirection();
			GeoLocation currentRouteLocation = intersections.lastElement().getLocation();
			RouteCell newCell = new RouteCell(startCell, startLocation, currentRouteLocation);
			double cellTTC = newCell.getTTC();
			if (this.checkIfWindChange(elapsed, cellTTC)) {
				final Vector<Object> changeCells = this.splitVMGRoute(foundRoute, newCell, vmgAngle);
				final Directions lastDirection = (Directions) changeCells.lastElement();
				foundRoute.add((RouteCell) changeCells.get(0), nextDirection);
				foundRoute.add((RouteCell) changeCells.get(1), lastDirection);
				foundRoute.setWindChange();

				final RouteCell newLastCell = (RouteCell) changeCells.get(1);
				currentRouteLocation = newLastCell.getExitLocation();
				nextDirection = lastDirection;
			} else
				foundRoute.add(newCell, nextDirection);
			elapsed = foundRoute.getRouteTTC();
			WindCell nextCell = startCell;

			for (int i = 1; i <= cellsToScan; i++) {
				// - Get the next cell in the line using the direction and the connection with the cell boundaries.
				now = foundRoute.getSearchTime();
				final GeoLocation nextLocation = nextCell.cellAtDirection(nextDirection);
				nextCell = WindMapHandler.cellWithPoint(nextLocation, now.getTime());
				vmc = new VMCData(vmgAngle, nextCell);
				bestAngle = vmc.getBestAngle();
				// bestAngle = GeoLocation.adjustAngle(nextCell.getWindDir() + vmgAngle);
				intersections = this.getIntersection(currentRouteLocation, nextCell, bestAngle);
				try {
					nextDirection = intersections.lastElement().getDirection();
				} catch (NoSuchElementException e) {
					// - No intersection found. This should not happen but take correction action
					return foundRoute;
				}
				// - Bug detected. If this two locations are the same we cannot continue.
				// - This is the case of entering a cell that will reflect us. We have to choose another
				// route and the only available is the worst route.
				// - Warning. Do not update the content of "curentRouteLocation" until this check is completed.
				if (currentRouteLocation.isEquivalent(intersections.lastElement().getLocation())) {
					bestAngle = vmc.getWorstAngle();
					intersections = this.getIntersection(currentRouteLocation, nextCell, bestAngle);
					nextDirection = intersections.lastElement().getDirection();
				}
				newCell = new RouteCell(nextCell, currentRouteLocation, intersections.lastElement().getLocation());
				currentRouteLocation = intersections.lastElement().getLocation();
				cellTTC = newCell.getTTC();
				if (this.checkIfWindChange(elapsed, cellTTC)) {
					final Vector<Object> changeCells = this.splitVMGRoute(foundRoute, newCell, vmgAngle);
					final Directions lastDirection = (Directions) changeCells.lastElement();
					foundRoute.add((RouteCell) changeCells.get(0), nextDirection);
					foundRoute.add((RouteCell) changeCells.get(1), lastDirection);
					foundRoute.setWindChange();

					final RouteCell newLastCell = (RouteCell) changeCells.get(1);
					currentRouteLocation = newLastCell.getExitLocation();
					nextDirection = lastDirection;
				} else
					foundRoute.add(newCell, nextDirection);
				elapsed = foundRoute.getRouteTTC();
			}
		} catch (final LocationNotInMap e) {
			e.printStackTrace();
		}
		return foundRoute;
	}

	/*
	 * Calculate the point for a wind change. The angle is the angle between the start and end. The distance to
	 * run depends on the speed and the time to elapse until the wind change time.
	 */
	private GeoLocation calculateMidPoint(final GeoLocation start, final GeoLocation end, final double minutes,
			final double speed) {
		// - Test to find a conversion factor between loxodromic and trigonometric distances.
		final double deltaLat = end.getLat() - start.getLat();
		double deltaLon = end.getLon() - start.getLon();

		// - Correct the difference if the points are at different sides of the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon -= 360.0 * Math.signum(deltaLon);
		final double Ls = Math.toRadians(start.getLat());
		final double Ld = Math.toRadians(end.getLat());
		final double ldelta = Math.toRadians(deltaLon);
		final double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		final double TTC = loxDistance / speed;
		final double t11 = minutes / VORGConstants.TOMINUTES;
		final double targetLat = t11 * deltaLat / TTC;
		final double targetLon = t11 * deltaLon / TTC;
		final GeoLocation lastLocation = new GeoLocation(start.getLat() + targetLat, start.getLon() + targetLon);

		return lastLocation;
	}

	private Vector<Intersection> getIntersection(final GeoLocation startLocation, final WindCell startWindCell,
			final int direction) throws LocationNotInMap {
		// - With both angles calculate the start direction of two routes to be evaluated.
		// - Extend this line to calculate intersections with the cell.
		// - Use the loxodromic calculations to get an end point.
		final GeoLocation endPoint = startLocation.directEstimation(120.0, direction);
		// final double endLat = startLocation.getLat() + 2.0 * Math.cos(Math.toRadians(direction));
		// final double endLon = startLocation.getLon() + 2.0 * Math.sin(Math.toRadians(direction));
		// endPoint = new GeoLocation(endLat, endLon);
		final Vector<Intersection> intersections = startWindCell.calculateIntersection(startLocation, endPoint);
		return intersections;
	}

	private double getMaxAngle4Course(final int course, final VMCData vmc, final int windDirection) {
		final int localMaxAWD = vmc.getMaxAWD();
		// - Calculate the Port and Starboard angles.
		final int portAngle = GeoLocation.adjustAngleTo360(windDirection + localMaxAWD);
		final int starboardAngle = GeoLocation.adjustAngleTo360(windDirection - localMaxAWD);
		double diffPort = GeoLocation.angleDifference(course, portAngle);
		// - Convert a 360.0 back to 0.0 for this comparison.
		if (diffPort == 360.0) diffPort = 0.0;
		double diffStarboard = GeoLocation.angleDifference(course, starboardAngle);
		// - Check for 360.0 values to be converted back to 0.0
		if (diffPort == 360.0) diffPort = 0.0;
		if (diffStarboard == 360.0) diffStarboard = 0.0;
		double newCourse;
		if (diffPort < diffStarboard)
			newCourse = portAngle;
		else
			newCourse = starboardAngle;
		return newCourse;
	}

	private double nextElevenDifference(final Calendar entryDate) {
		final int entryHours = entryDate.get(Calendar.HOUR_OF_DAY);
		final Calendar elevenTime = Calendar.getInstance();
		if (entryHours < 11)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 11, 0,
					0);
		else if (entryHours < 23)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 23, 0,
					0);
		else
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE) + 1, 11,
					0, 0);
		final long diff = elevenTime.getTimeInMillis() - entryDate.getTimeInMillis();
		// - Convert to minutes.
		final double mins = diff / (60 * 1000);
		return mins;
	}

	private Vector<Object> splitRoute(final Route theRoute, final RouteCell reference, final int maxAWD)
			throws LocationNotInMap {
		final Vector<Object> cells = new Vector<Object>();
		// - Get the time start form the creation time of the route to keep all times in sych.
		final Calendar nowSearchTime = theRoute.getSearchTime();

		final WindCell windCell = WindMapHandler.cellWithPoint(reference.getCell().getLocation(), nowSearchTime.getTime());
		final GeoLocation entry = reference.getEntryLocation();
		final GeoLocation exit = reference.getExitLocation();

		// - Calculate an intermediate initial point.
		final GeoLocation midPoint = this.calculateMidPoint(entry, exit, this.nextElevenDifference(theRoute.getEndTime()),
				reference.getSpeed());
		final RouteCell preRoute = new RouteCell(windCell, entry, midPoint);

		// DEBUG Start of debug section
		// final Calendar entryTime = theRoute.getCreationTime();
		// entryTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() *
		// VORGConstants.TOMINUTES).intValue());
		// final Calendar endTime = theRoute.getCreationTime();
		// endTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() * VORGConstants.TOMINUTES).intValue());
		// endTime.add(Calendar.MINUTE, new Double(preRoute.getTTC() * VORGConstants.TOMINUTES).intValue());
		// System.out.println("Start time=" + entryTime.getTime());
		// System.out.println("End time=" + endTime.getTime());
		// DEBUG End of debug section

		cells.add(preRoute);

		// - Locate the next future wind cell.
		final WindCell currentWindCell = preRoute.getCell();
		final Date currentTime = currentWindCell.getTimeStamp();
		final Calendar futureTime = Calendar.getInstance();
		futureTime.setTime(currentTime);
		futureTime.add(Calendar.HOUR, 13);
		final WindCell futureWindCell = WindMapHandler.cellWithPoint(currentWindCell.getLocation(), futureTime.getTime());
		final int starboardAngle = GeoLocation.adjustAngleTo360(futureWindCell.getWindDir() - maxAWD);
		final Vector<Intersection> intersections = this.getIntersection(midPoint, futureWindCell, starboardAngle);
		final GeoLocation currentRouteLocation = intersections.lastElement().getLocation();

		final RouteCell postRoute = new RouteCell(futureWindCell, midPoint, currentRouteLocation);
		cells.add(postRoute);
		cells.add(intersections.lastElement().getDirection());
		return cells;
	}

	private Vector<Object> splitVMGRoute(final Route theRoute, final RouteCell reference, final int vmgAngle)
			throws LocationNotInMap {
		final Vector<Object> cells = new Vector<Object>();
		final Calendar nowSearchTime = theRoute.getSearchTime();

		final WindCell windCell = WindMapHandler.cellWithPoint(reference.getCell().getLocation(), nowSearchTime.getTime());
		final GeoLocation entry = reference.getEntryLocation();
		final GeoLocation exit = reference.getExitLocation();

		// - Calculate an intermediate initial point.
		final GeoLocation midPoint = this.calculateMidPoint(entry, exit, this.nextElevenDifference(theRoute.getEndTime()),
				reference.getSpeed());
		final RouteCell preRoute = new RouteCell(windCell, entry, midPoint);

		// DEBUG Start of debug section
		// final Calendar entryTime = theRoute.getCreationTime();
		// entryTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() *
		// VORGConstants.TOMINUTES).intValue());
		// final Calendar endTime = theRoute.getCreationTime();
		// endTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() * VORGConstants.TOMINUTES).intValue());
		// endTime.add(Calendar.MINUTE, new Double(preRoute.getTTC() * VORGConstants.TOMINUTES).intValue());
		// System.out.println("Start time=" + entryTime.getTime());
		// System.out.println("End time=" + endTime.getTime());
		// DEBUG End of debug section

		cells.add(preRoute);

		// - Locate the next future wind cell.
		final WindCell currentWindCell = preRoute.getCell();
		final Date currentTime = currentWindCell.getTimeStamp();
		final Calendar futureTime = Calendar.getInstance();
		futureTime.setTime(currentTime);
		futureTime.add(Calendar.HOUR, 13);
		final WindCell futureWindCell = WindMapHandler.cellWithPoint(currentWindCell.getLocation(), futureTime.getTime());
		final VMCData vmc = new VMCData(vmgAngle, futureWindCell);
		final int bestAngle = vmc.getBestAngle();
		final Vector<Intersection> intersections = this.getIntersection(midPoint, futureWindCell, bestAngle);
		final GeoLocation currentRouteLocation = intersections.lastElement().getLocation();

		final RouteCell postRoute = new RouteCell(futureWindCell, midPoint, currentRouteLocation);
		cells.add(postRoute);
		cells.add(intersections.lastElement().getDirection());
		return cells;
	}
}

// - UNUSED CODE ............................................................................................
