//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.core.exceptions;

//- IMPORT SECTION .........................................................................................
import org.xml.sax.SAXException;

// - CLASS IMPLEMENTATION ...................................................................................
public class InvalidContentException extends SAXException {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long	serialVersionUID	= -8228699753177913838L;

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public InvalidContentException(String message) {
		super(message);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}
// - UNUSED CODE ............................................................................................
