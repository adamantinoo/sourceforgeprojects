//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGURLRequest {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger											logger		= Logger.getLogger("net.sf.vorg");
	private static String											protocol	= "http";
	private static Hashtable<String, String>	cookies		= new Hashtable<String, String>();

	public static void clearProxy() {
		//- Clear proxy settings
		System.getProperties().remove("proxySet");
		System.getProperties().remove("proxyHost");
		System.getProperties().remove("proxyPort");
	}

	public static String getResourceData(final HttpURLConnection conn) throws MalformedURLException,
			FileNotFoundException, IOException {
		conn.setDoInput(true);
		final StringBuffer data = new StringBuffer();
		//		try {
		final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line = null;
		while ((line = in.readLine()) != null)
			data.append(line).append("\n");

		// - Process the headers.
		String headerName = null;
		for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
			if (headerName.equals("Set-Cookie")) {
				String cookie = conn.getHeaderField(i);
				cookie = cookie.substring(0, cookie.indexOf(";"));
				final String cookieName = cookie.substring(0, cookie.indexOf("="));
				final String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
			}

		in.close();
		//		} catch (MalformedURLException ex) {
		//			System.err.println(ex);
		//		} catch (FileNotFoundException ex) {
		//			System.err.println("Failed to open stream to URL: " + ex);
		//		} catch (IOException ex) {
		//			System.err.println("Error reading URL content: " + ex);
		//		}
		return data.toString();
	}

	public static void setProxy(final String hostName, final int port) {
		if (null != hostName) {
			System.getProperties().put("proxySet", "true");
			System.getProperties().put("proxyHost", hostName);
			System.getProperties().put("proxyPort", port);
		}
		//		//- Authentication to the proxy
		//		URLConnection connection = url.openConnection();
		//		String password = "username:password";
		//		String encodedPassword = base64Encode(password);
		//		connection.setRequestProperty("Proxy-Authorization", encodedPassword);

	}

	// - F I E L D - S E C T I O N ............................................................................
	private String													host		= "volvogame.virtualregatta.com";
	private String													request	= "";
	private URL															url;
	private HttpURLConnection								conn;
	private final Hashtable<String, String>	headers	= new Hashtable<String, String>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGURLRequest(final String request) {
		VORGURLRequest.logger.info("Preparing URL: " + VORGURLRequest.protocol + "://" + host + request);
		this.request = request;
	}

	public void addCookies(final Hashtable<String, String> newCookies) {
		while (newCookies.keys().hasMoreElements()) {
			final String key = newCookies.keys().nextElement();
			VORGURLRequest.cookies.put(key, newCookies.get(key));
		}
	}

	public void executeGET(final Hashtable<String, String> parameters) throws MalformedURLException, ProtocolException,
			IOException {
		//- Compose the request with the parameters.
		url = new URL(VORGURLRequest.protocol, host, request + composeHttpHeading(parameters, "&"));
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		setConnectionHeaders(conn);
		conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");

		conn.setUseCaches(false);
		conn.setDoInput(true);
	}

	public void executePOST(final Hashtable<String, String> parameters) {
		try {
			// - Process the POST parameters and encode them for output.
			final String urlParameters = composeHttpHeading(parameters, "&");
			url = new URL(VORGURLRequest.protocol, host, request);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			setConnectionHeaders(conn);
			conn.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.length() + 2));
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			final Writer out = new OutputStreamWriter(conn.getOutputStream());
			out.write(urlParameters);
			out.write("\r\n");
			out.flush();
			out.close();
		} catch (final MalformedURLException mue) {
			mue.printStackTrace();
		} catch (final UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public String getData() {
		final StringBuffer data = new StringBuffer();
		try {
			conn.connect();
			final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final int nHttpResponseCode = conn.getResponseCode();
			if (nHttpResponseCode != HttpURLConnection.HTTP_OK) {
				System.out.println("Http error:" + nHttpResponseCode);
				in.close();
				return nHttpResponseCode + conn.getResponseMessage();
			}

			String line = null;
			while ((line = in.readLine()) != null)
				data.append(line).append("\n");

			// - Process the headers searching for cookies.
			String headerName = null;
			for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
				if (headerName.equals("Set-Cookie")) {
					String cookie = conn.getHeaderField(i);
					cookie = cookie.substring(0, cookie.indexOf(";"));
					final String cookieName = cookie.substring(0, cookie.indexOf("="));
					final String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
					VORGURLRequest.cookies.put(cookieName, cookieValue);
				}

			in.close();
		} catch (final MalformedURLException ex) {
			System.err.println(ex);
		} catch (final FileNotFoundException ex) {
			System.err.println("Failed to open stream to URL: " + ex);
		} catch (final IOException ex) {
			System.err.println("Error reading URL content: " + ex);
		}
		return data.toString();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setDefaultHeaders() {
		headers.clear();
		//	headers.put("Host", "www.volvooceanracegame.org");
		headers.put("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		headers.put("Accept", "text/javascript, text/html, application/xml, text/xml, */*");
		//		headers.put("Accept-Language", "en-GB,en;q=0.8,es-ES;q=0.5,es;q=0.3");
		//		headers.put("Accept-Encoding", "gzip,deflate");
		//	headers.put("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		//	headers.put("Keep-Alive", "300");
		//	headers.put("Proxy-Connection", "keep-alive");
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		headers.put("Content-Language", "en-US");
		//		headers.put("Pragma", "no-cache");
		//	headers.put("Cache-Control", "no-cache");
		//	Host	www.volvooceanracegame.org
		//	User-Agent	Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5
		//	Accept	text/javascript, text/html, application/xml, text/xml, */*
		//	Accept-Language	en-GB,en;q=0.8,es-ES;q=0.5,es;q=0.3
		//	Accept-Encoding	gzip,deflate
		//	Accept-Charset	ISO-8859-1,utf-8;q=0.7,*;q=0.7
		//	Keep-Alive	300
		//	Proxy-Connection	keep-alive
		//	X-Requested-With	XMLHttpRequest
		//	X-Prototype-Version	1.6.0.1
		//	Content-Type	application/x-www-form-urlencoded; charset=UTF-8
		//	Referer	http://www.volvooceanracegame.org/home.php
		//	Content-Length	52
		//	Cookie	__utma=192196909.1001457302.1235038643.1241441512.1241453256.228; __utmz=192196909.1235038644.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __qca=499e8936-352f1-64ab8-7393d; forum_order=down; forum_topicid=13554; useremail=boneysp%40orangemail.es; PHPSESSID=0f9c4d33b99642deb0978a8ece647890; last_online=1241453258; __utmc=192196909; __utmb=192196909.2.10.1241453256
		//	Pragma	no-cache
		//	Cache-Control	no-cache
	}

	public void setHeaders(final Hashtable<String, String> newHeaders) {
		setDefaultHeaders();
		final Enumeration<String> henu = newHeaders.keys();
		while (henu.hasMoreElements()) {
			final String key = henu.nextElement();
			headers.put(key, newHeaders.get(key));
		}
	}

	public void setHost(final String newHost) {
		host = newHost;
	}

	private String composeHttpHeading(final Hashtable<String, String> data, final String separator)
			throws UnsupportedEncodingException {
		if (null == data) return "";
		final StringBuffer composedHeading = new StringBuffer();
		final Enumeration<String> pit = data.keys();
		int dataCounter = 0;
		while (pit.hasMoreElements()) {
			final String name = pit.nextElement();
			final String value = data.get(name);
			if (dataCounter > 0) composedHeading.append(separator);
			composedHeading.append(name).append("=").append(URLEncoder.encode(value, "UTF-8"));
			dataCounter++;
		}
		return composedHeading.toString();
	}

	private void setConnectionHeaders(final HttpURLConnection conx) throws UnsupportedEncodingException {
		if (headers.size() < 1) setDefaultHeaders();
		final Enumeration<String> henu = headers.keys();
		while (henu.hasMoreElements()) {
			final String key = henu.nextElement();
			conn.setRequestProperty(key, headers.get(key));
		}
		//- If there are cookies, add them
		if (VORGURLRequest.cookies.size() > 0)
			conn.setRequestProperty("Cookie", composeHttpHeading(VORGURLRequest.cookies, "; "));
	}
}

// - UNUSED CODE ............................................................................................
