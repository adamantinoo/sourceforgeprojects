//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.singletons;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Locale;

// - CLASS IMPLEMENTATION ...................................................................................
public class FormatSingletons {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public static final NumberFormat	nf1	= NumberFormat.getInstance(Locale.ENGLISH);
	static {
		nf1.setMaximumFractionDigits(1);
		nf1.setMinimumFractionDigits(1);
	}
	public static final NumberFormat	nf2	= NumberFormat.getInstance(Locale.ENGLISH);
	static {
		nf2.setMaximumFractionDigits(2);
		nf2.setMinimumFractionDigits(2);
	}
	public static final NumberFormat	nf3	= NumberFormat.getInstance(Locale.ENGLISH);
	static {
		nf3.setMaximumFractionDigits(3);
		nf3.setMinimumFractionDigits(3);
	}
	public static final NumberFormat	nf4	= NumberFormat.getInstance(Locale.ENGLISH);
	static {
		nf4.setMaximumFractionDigits(4);
		nf4.setMinimumFractionDigits(4);
	}
	public static final NumberFormat	nf5	= NumberFormat.getInstance(Locale.ENGLISH);
	static {
		nf5.setMaximumFractionDigits(5);
		nf5.setMinimumFractionDigits(5);
	}
}

// - UNUSED CODE ............................................................................................
