//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.enums;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum CommandTypes {
	NOCOMMAND, ROUTE, AUTO;
	public static CommandTypes decodeType(String typeName) {
		if (null == typeName) return NOCOMMAND;
		if (typeName.toUpperCase().equals("ROUTE")) return CommandTypes.ROUTE;
		if (typeName.toUpperCase().equals("AUTO")) return CommandTypes.AUTO;
		return NOCOMMAND;
	}
}
// - UNUSED CODE ............................................................................................
