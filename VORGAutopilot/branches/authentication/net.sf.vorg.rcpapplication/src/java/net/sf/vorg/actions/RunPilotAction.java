//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.actions;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import org.eclipse.swt.widgets.Display;

import net.sf.vorg.core.enums.ModelStates;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class RunPilotAction extends BoatAction {
	class AutopilotJob implements Runnable {
		private final PilotModelStore	referenceStore;

		//		private final String					name;

		public AutopilotJob(final PilotModelStore store, final String name) {
			referenceStore = store;
		}

		public void run() {
			RunPilotAction.logger.info(">>> Entering Processing Autopilot Loop");
			if (null != referenceStore) {
				referenceStore.checkMapReload();
				referenceStore.performOperation();
			}
			RunPilotAction.logger.info("<<< Exiting Processing Autopilot Loop");
		}
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger			= Logger.getLogger("net.sf.vorg.actions");
	// - F I E L D - S E C T I O N ............................................................................
	private boolean				running			= false;																		;
	private Display				display			= Display.getCurrent();
	private int						refresh			= PilotModelStore.DEFAULT_REFRESH_MINUTES;
	private int						timeDelay		= PilotModelStore.DEFAULT_TIME_DELAY;
	private long					lastMinute	= minuteOfDay() - 100;
	private Thread				pilotLoopProcessingThread;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This methods starts or restarts the game processing loop and the game timers.<br>
	 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
	 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside that
	 * thread that requires access to the UI (updating, creating new model elements) will be posted as another
	 * Runnable instance to the Display created for the application.
	 * 
	 * @return
	 */
	@Override
	public void run() {
		//- Check the current status of the model.
		//- Toggle the run status of the model
		if (running) {
			stopLoop();
			setImageDescriptor(ImageFactory.getImageDescriptor("icons/run.gif"));
			//- Send a message to the Store to clean up all active waypoints
			modelStore.cleanActiveWaypoints();
			//- Reset timer
			lastMinute = minuteOfDay() - 100;
		} else {
			setImageDescriptor(ImageFactory.getImageDescriptor("icons/running.gif"));
			//- Send a message to the Store to clean up all active waypoints
			//		modelStore.cleanActiveWaypoints();
			//		modelStore.clearUpdate();
			startLoop();
		}
	}

	/**
	 * This methods starts or restarts the game processing loop and the game timers.<br>
	 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
	 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside that
	 * thread that requires access to the UI (updating, creating new model elements) will be posted as another
	 * Runnable instance to the Display created for the application.
	 */
	public void startLoop() {
		// - Mark the flag that keeps the main thread running.
		display = Display.getCurrent();
		running = true;
		modelStore.setState(ModelStates.RUNNING);

		// - Create a new thread and keep it running the infinite loop
		if (null != modelStore) {
			//- Read the timing configuration from the store.
			//			refresh = modelStore.getRefreshInterval();
			//			timeDelay = modelStore.getTimeDeviation();
			pilotLoopProcessingThread = new Thread(new Runnable() {
				public void run() {
					while (running) {
						try {
							//- Re read the time configuration because it can be modified on the interface.
							refresh = modelStore.getRefreshInterval();
							timeDelay = modelStore.getTimeDeviation();

							//- Sleep until the right time.
							RunPilotAction.this.wait4NextIteration();

							if (running) {
								// - Run the job but this time inside the Display UI environment.
								RunPilotAction.logger.info("Starting UIJob");
								final AutopilotJob job = new AutopilotJob(modelStore, "Main Pilot Loop");
								if (null != display)
									if (!display.isDisposed()) {
										display.syncExec(job);
									}
							}
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}
				};
			});
			pilotLoopProcessingThread.start();
			pilotLoopProcessingThread.setName("Main Pilot Loop");
		}
	}

	public void stopLoop() {
		running = false;
		pilotLoopProcessingThread.interrupt();
		modelStore.setState(ModelStates.READY);
	}

	private long minuteOfDay() {
		final Calendar now = Calendar.getInstance();
		final int hour = now.get(Calendar.HOUR_OF_DAY);
		final int minute = now.get(Calendar.MINUTE);
		return hour * 60 + minute;
	}

	private void wait4NextIteration() throws InterruptedException {
		//- Calculate last minute of day and current minute of day and then the fire minute.
		final long dayMinute = minuteOfDay();
		long waitUntil = lastMinute + refresh;
		//- Check for the 24 hours. This will give a time unreachable.
		if (waitUntil > 24 * 60) {
			waitUntil -= 24 * 60;
		}
		if (dayMinute > waitUntil) {
			lastMinute = dayMinute;
			return;
		}

		//- Calculate the time to wait
		final Calendar now = GregorianCalendar.getInstance();
		final int seconds = now.get(Calendar.SECOND);
		final long waitSeconds = (waitUntil - dayMinute) * 60 - seconds + timeDelay;
		try {
			Thread.yield();
			lastMinute = waitUntil;
			if (waitSeconds > 0) {
				Thread.sleep(waitSeconds * 1000);
			}
		} catch (final InterruptedException ie) {
			// - We have been interrupted.
			running = false;
			lastMinute -= refresh;
		} catch (final IllegalArgumentException iae) {
			// - The value to wait is not valid. Skip
		}
	}
}
// - UNUSED CODE ............................................................................................
