//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridData;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MouseEvent;
import org.eclipse.draw2d.MouseMotionListener;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.Disposable;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.SWT;

import net.sf.gef.core.draw2d.MultiLabelLine;
import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.enums.WaypointStates;
import net.sf.vorg.core.enums.WaypointTypes;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class WaypointFigure extends SelectableFigure implements IDetailedFigure, Disposable {
	class SingleColumnFigure extends Figure {
		public SingleColumnFigure() {
			final GridLayout grid = new GridLayout();
			grid.horizontalSpacing = 0;
			grid.verticalSpacing = 0;
			grid.marginHeight = 0;
			grid.marginWidth = 0;
			grid.numColumns = 1;
			setLayoutManager(grid);
		}
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	private final Waypoint	waypointModel;
	private final Figure		content							= new SingleColumnFigure();
	private Figure					compressedContainer	= new Figure();
	//	@SuppressWarnings("unused")
	//	private final Figure		standardContainer		= new Figure();
	private Figure					extendedContainer		= new Figure();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WaypointFigure(final Waypoint model) {
		waypointModel = model;
		setOpaque(true);
		initialize();
		addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(final MouseEvent me) {
			}

			public void mouseEntered(final MouseEvent me) {
			}

			@Override
			public void mouseExited(final MouseEvent me) {
				WaypointFigure.this.setSelected(EditPart.SELECTED_NONE);
				WaypointFigure.this.refreshContents();
			}

			@Override
			public void mouseHover(final MouseEvent me) {
				WaypointFigure.this.setSelected(EditPart.SELECTED_PRIMARY);
				WaypointFigure.this.refreshContents();
			}

			public void mouseMoved(final MouseEvent me) {
			}
		});
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void refreshContents() {
		content.removeAll();
		addCompressedContents();
		addStandardContents();
		if (isSelected()) {
			addExtendedContants();
		}
		updateBackgroundColor();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	protected void addCompressedContents() {
		compressedContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		compressedContainer.setLayoutManager(grid);

		//- Compose the first line with the name and the type
		final MultiLabelLine name = new MultiLabelLine();
		name.setDefaultFont("Arial", 12);
		name.addColumn(waypointModel.getName(), SWT.NORMAL, PilotUIConstants.COLOR_NAMES);
		name.addColumn("    Type:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		name.addColumn(waypointModel.getType().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		if (waypointModel.getType() == WaypointTypes.ANGLE) {
			name.addColumn("    Angle:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			name.addColumn(FormatSingletons.nf1.format(waypointModel.getCourse()) + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.AWD) {
			name.addColumn("    AWD:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			name.addColumn(FormatSingletons.nf1.format(waypointModel.getSelectedAWD()) + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.VMG) {
			name.addColumn("    Angle:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			name.addColumn(FormatSingletons.nf1.format(waypointModel.getCourse()) + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		//		if (waypointModel.getType() == WaypointTypes.VMGLOX) {
		//			final double angle = this.waypointModel.get.getLocation().angleTo(getLocation());
		//			name.addColumn("    Angle:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		//			name.addColumn(FormatSingletons.nf1.format(waypointModel.getCourse()) + "�", SWT.BOLD,
		//					PilotUIConstants.COLOR_BLACK);
		//		}
		compressedContainer.add(name);
		content.add(compressedContainer);
	}

	protected void addStandardContents() {
	}

	private void addExtendedContants() {
		extendedContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 1;
		extendedContainer.setLayoutManager(grid);

		//- Compose the second line with the waypoint characteristics.
		final MultiLabelLine waypointData = new MultiLabelLine();
		waypointData.setDefaultFont("Tahoma", 10);
		waypointData.addColumn("AWD: ", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
		waypointData.addColumn(waypointModel.getMinAWD() + "/" + waypointModel.getMaxAWD(), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		if (waypointModel.getType() == WaypointTypes.ANGLE) {
			waypointData.addColumn("    Angle: ", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
			waypointData.addColumn(new Double(waypointModel.getCourse()).toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.AWD) {
			waypointData.addColumn("    Angle: ", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
			waypointData.addColumn(FormatSingletons.nf1.format(waypointModel.getSelectedAWD()) + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		if (waypointModel.getType() == WaypointTypes.VMG) {
			waypointData.addColumn("    Angle: ", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
			waypointData.addColumn(FormatSingletons.nf1.format(waypointModel.getCourse()) + "�", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		waypointData.addColumn("    Range:", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
		waypointData.addColumn(new Double(waypointModel.getRange()).toString() + " NM", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		//- Compose the third line with the boat location
		final MultiLabelLine boatLocation = new MultiLabelLine();
		boatLocation.setDefaultFont("Tahoma", 11);
		boatLocation.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_DARK_GREEN);
		boatLocation.addColumn(waypointModel.getLocation().formattedLocation().replace('\t', '-'), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		extendedContainer.add(waypointData);
		extendedContainer.add(boatLocation);
		content.add(extendedContainer);
	}

	private void initialize() {
		removeAll();
		content.removeAll();
		content.setOpaque(true);
		final GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;
		setLayout();
		setBorder(new LineBorder(1));
		setIcon();
		this.add(content, gridData);
		updateBackgroundColor();
	}

	private void setIcon() {
		final Label iconCell = new Label(ImageFactory.getImage(ImageKeys.WAYPOINT_BIG));
		iconCell.setFont(PilotUIConstants.FONT_DEFAULT);
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(5);
		this.add(iconCell);
	}

	private void setLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		grid.numColumns = 2;
		setLayoutManager(grid);
	}

	private void updateBackgroundColor() {
		//		@SuppressWarnings("unused")
		//		final WaypointStates state = waypointModel.getState();
		//		final WaypointTypes type = waypointModel.getType();
		if (waypointModel.getState() == WaypointStates.ACTIVE) {
			content.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_ACTIVE);
		}
		if (waypointModel.getState() == WaypointStates.NOACTIVE) {
			content.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_NOACTION);
		}
		if (waypointModel.getState() == WaypointStates.ONHOLD) {
			content.setBackgroundColor(PilotUIConstants.COLOR_ONHOLD);
		}
		if (waypointModel.getState() == WaypointStates.SURPASSED) {
			content.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_SURPASSED);
		}
		if (waypointModel.getType() == WaypointTypes.NOACTION) {
			content.setBackgroundColor(PilotUIConstants.COLOR_WAYPOINT_NOACTION);
		}
	}

	@Override
	public void dispose() {
		if (content instanceof Disposable) {
			((Disposable) content).dispose();
		}
		if (compressedContainer instanceof Disposable) {
			((Disposable) compressedContainer).dispose();
		}
		if (extendedContainer instanceof Disposable) {
			((Disposable) extendedContainer).dispose();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		dispose();
		super.finalize();
	}
}
// - UNUSED CODE ............................................................................................
