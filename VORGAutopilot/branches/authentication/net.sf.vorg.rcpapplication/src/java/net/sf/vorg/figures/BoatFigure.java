//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.gef.Disposable;
import org.eclipse.swt.SWT;

import net.sf.gef.core.draw2d.MultiLabelLine;
import net.sf.gef.core.figures.SelectableFigure;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class BoatFigure extends SelectableFigure implements IDetailedFigure, Disposable {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	private Boat		boatModel			= null;
	private Figure	boatContainer	= new Figure();
	private Figure	windContainer	= new Figure();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatFigure(final Boat model) {
		boatModel = model;
		setOpaque(false);
		initialize();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void refreshContents() {
		initialize();

		addCompressedContents();
		addStandardContents();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	protected void addCompressedContents() {
		addBoatInformation();
	}

	protected void addStandardContents() {
		addWindInformation();
	}

	private void addBoatInformation() {
		//- Add the Boat icon
		final Label iconCell = new Label(ImageFactory.getImage(ImageKeys.BOAT_BIG));
		iconCell.setFont(PilotUIConstants.FONT_DEFAULT);
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(3);
		this.add(iconCell);

		//- Add the container with the Boat information
		boatContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 1;
		grid.verticalSpacing = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.numColumns = 2;
		boatContainer.setLayoutManager(grid);

		//- Compose the first line with the name and the ranking.
		final MultiLabelLine name = new MultiLabelLine();
		name.setDefaultFont("Arial", 14);
		name.addColumn(boatModel.getName(), SWT.NORMAL, PilotUIConstants.COLOR_NAMES);
		final MultiLabelLine ranking = new MultiLabelLine();
		ranking.setDefaultFont("Arial", 14);
		ranking.addColumn("Ranking:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		int diffRanking = boatModel.getDiffRanking();
		if (boatModel.getDiffRanking() < 0) {
			ranking.addColumn(new Integer(boatModel.getRanking()).toString() + " [Gain "
					+ new Integer(Math.abs(diffRanking)).toString() + "]", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		} else {
			ranking.addColumn(new Integer(boatModel.getRanking()).toString() + " [Loss "
					+ new Integer(Math.abs(diffRanking)).toString() + "]", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		}

		//- Compose the second line with the boat course
		final MultiLabelLine boatComponents = new MultiLabelLine();
		boatComponents.setDefaultFont("Tahoma", 10);
		boatComponents.addColumn("Heading:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		boatComponents.addColumn(new Integer(boatModel.getHeading()).toString() + "�", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		//- Get the sign of the AWD by comparing it to the current course.
		int course = boatModel.getHeading();
		int data = GeoLocation.angleDifference(boatModel.getWindCell().getWindDir(), course);
		boatComponents.addColumn("    AWD:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		//TODO Adjust the AWD color depending on the angle turning it red when approaching bad values
		if (data > 0) {
			boatComponents.addColumn(new Integer(boatModel.getWindAngle()).toString() + "� Starboard", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		} else {
			boatComponents.addColumn(new Integer(boatModel.getWindAngle()).toString() + "� Port", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}
		final MultiLabelLine boatComponents2 = new MultiLabelLine();
		boatComponents2.setDefaultFont("Tahoma", 10);
		boatComponents2.addColumn("Speed:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		boatComponents2.addColumn(FormatSingletons.nf2.format(boatModel.getSpeed()) + " knts", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		boatComponents2.addColumn("    Sail:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		boatComponents2.addColumn(boatModel.getSail().toString(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//- Compose the third line with the boat location
		final MultiLabelLine boatLocation = new MultiLabelLine();
		boatLocation.setDefaultFont("Tahoma", 11);
		boatLocation.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		boatLocation.addColumn(boatModel.getLocation().formattedLocation().replace('\t', '-'), SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		final MultiLabelLine powerData = new MultiLabelLine();
		powerData.setDefaultFont("Tahoma", 11);
		powerData.addColumn("Power:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		powerData.addColumn(boatModel.getPower(), SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		if (boatModel.getDistanceRun() > 0.0) {
			powerData.addColumn("    Dist Run:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
			powerData.addColumn(FormatSingletons.nf2.format(boatModel.getDistanceRun()) + "NM", SWT.BOLD,
					PilotUIConstants.COLOR_BLACK);
		}

		//		//		container.
		//		final GridData gridData = new GridData();
		//		gridData.horizontalSpan = 2;

		boatContainer.add(name);
		boatContainer.add(ranking);
		boatContainer.add(boatComponents);
		boatContainer.add(boatComponents2);
		boatContainer.add(boatLocation);
		boatContainer.add(powerData);
		this.add(boatContainer);
	}

	private void addWindInformation() {
		//- Add the Wind icon
		final Label iconCell = new Label(ImageFactory.getImage(ImageKeys.WIND_CELL));
		iconCell.setFont(PilotUIConstants.FONT_DEFAULT);
		iconCell.setIconAlignment(PositionConstants.TOP);
		iconCell.setIconTextGap(3);
		this.add(iconCell);

		//- Add the container with the Boat information
		windContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 1;
		grid.verticalSpacing = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.numColumns = 1;
		windContainer.setLayoutManager(grid);

		final WindCell cell = boatModel.getWindCell();
		final VMCData vmg = new VMCData(boatModel.getHeading(), cell);

		final MultiLabelLine heading = new MultiLabelLine();
		heading.setDefaultFont("Tahoma", 10);
		heading.addColumn("Wind Direction:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		heading.addColumn(new Integer(cell.getWindDir()).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		heading.addColumn("    Speed:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		heading.addColumn(FormatSingletons.nf2.format(cell.getWindSpeed()) + " knots", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		//		final MultiLabelLine location = new MultiLabelLine();
		//		location.setDefaultFont("Tahoma", 10);
		//		location.addColumn("Location:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		//		location.addColumn(cell.getLocation().toReport().replace('\t', '-'), SWT.BOLD, PilotUIConstants.COLOR_BLACK);

		//- Add max power configuration
		int portAngle = GeoLocation.adjustAngleTo360(cell.getWindDir() + vmg.getMaxAWD());
		int starboardAngle = GeoLocation.adjustAngleTo360(cell.getWindDir() - vmg.getMaxAWD());

		final MultiLabelLine maxLabel = new MultiLabelLine();
		maxLabel.setDefaultFont("Tahoma", 10);
		maxLabel.addColumn("XPerformance 100%:", SWT.NORMAL, PilotUIConstants.COLOR_BRILLIANT_RED);

		final MultiLabelLine vmgHeading = new MultiLabelLine();
		vmgHeading.setDefaultFont("Tahoma", 10);
		vmgHeading.addColumn("Port:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		vmgHeading.addColumn(new Integer(portAngle).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		vmgHeading.addColumn("   Starboard:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		vmgHeading.addColumn(new Integer(starboardAngle).toString() + "�", SWT.BOLD, PilotUIConstants.COLOR_BLACK);
		vmgHeading.addColumn("   AWD:+-", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		vmgHeading.addColumn(new Integer(Math.abs(vmg.getMaxAWD())).toString() + "�", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);
		//		final MultiLabelLine vmgSpeed = new MultiLabelLine();
		//		vmgSpeed.setDefaultFont("Tahoma", 10);
		vmgHeading.addColumn("   Speed:", SWT.NORMAL, PilotUIConstants.COLOR_FIELDNAMES);
		vmgHeading.addColumn(FormatSingletons.nf2.format(vmg.getMaxSpeed()) + " knts", SWT.BOLD,
				PilotUIConstants.COLOR_BLACK);

		Figure windDataContainer = new SingleColumnFigure();
		Figure maxPowerContainer = new SingleColumnFigure();
		maxPowerContainer.setBorder(new LineBorder(1));

		windDataContainer.add(heading);
		//		windDataContainer.add(location);

		maxPowerContainer.add(maxLabel);
		maxPowerContainer.add(vmgHeading);
		//		maxPowerContainer.add(vmgSpeed);

		windContainer.add(windDataContainer);
		windContainer.add(maxPowerContainer);
		this.add(windContainer);
	}

	private void initialize() {
		removeAll();
		setLayout();
		setBorder(new LineBorder(1));
	}

	private void setLayout() {
		final GridLayout grid = new GridLayout();
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.numColumns = 2;
		setLayoutManager(grid);
	}

	class SingleColumnFigure extends Figure {
		public SingleColumnFigure() {
			final GridLayout grid = new GridLayout();
			grid.horizontalSpacing = 1;
			grid.verticalSpacing = 1;
			grid.marginHeight = 0;
			grid.marginWidth = 0;
			grid.numColumns = 1;
			setLayoutManager(grid);
		}
	}

	@Override
	public void dispose() {
		if (boatContainer instanceof Disposable) {
			((Disposable) boatContainer).dispose();
		}
		if (windContainer instanceof Disposable) {
			((Disposable) windContainer).dispose();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		dispose();
		super.finalize();
	}
}
// - UNUSED CODE ............................................................................................
