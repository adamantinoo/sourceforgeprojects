//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedContainerEditPart.java 137 2008-05-09 07:10:00Z boneymen $
//  LAST UPDATE:    $Date: 2008-05-09 09:10:00 +0200 (vie, 09 may 2008) $
//  RELEASE:        $Revision: 137 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.editparts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FreeformLayer;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;

import net.sf.gef.core.editparts.AbstractDirectedEditPart;
import net.sf.gef.core.model.IGEFNode;
import net.sf.vorg.policies.PilotGraphLayoutManager;
import net.sf.vorg.vorgautopilot.models.PilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotModelContainerEditPart extends AbstractDirectedEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger				logger	= Logger.getLogger("net.sf.vgap4.assistant.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (PilotModelStore.STATE_CHANGED.equals(prop)) {
			PilotModelStore theModel = getCastedModel();
			theModel.updatePersistentStorage();
		}
		if (PilotModelStore.MODEL_STRUCTURE_CHANGED.equals(prop)) {
			refreshChildren();
		}
		super.propertyChange(evt);
	}

	@Override
	protected IFigure createFigure() {
		// - Create an empty figure to control the scroll and the sizing.
		final Figure fig = new FreeformLayer();
		fig.setOpaque(true);
		fig.setBorder(new MarginBorder(1));
		fig.setLayoutManager(new PilotGraphLayoutManager(this));
		return fig;
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	protected List<IGEFNode> getModelChildren() {
		final PilotModelStore model = getCastedModel();
		return model.getChildren();
	}

	@Override
	protected void refreshChildren() {
		//FIXME Remove this method in final release.
		try {
			super.refreshChildren();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private PilotModelStore getCastedModel() {
		return (PilotModelStore) getModel();
	}
}
// - UNUSED CODE ............................................................................................
