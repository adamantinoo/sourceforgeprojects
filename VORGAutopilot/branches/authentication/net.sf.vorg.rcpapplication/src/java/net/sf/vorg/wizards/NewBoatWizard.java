//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

import net.sf.gef.core.wizards.AbstractWizard;
import net.sf.vorg.core.enums.ModelStates;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class NewBoatWizard extends AbstractWizard {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the workbench used to display some elements. */
	protected IWorkbench						workbench;
	/**
	 * The workbench selection when this wizard was started. Not used internally because we use the model to
	 * access the data.
	 */
	protected IStructuredSelection	selection;
	private final PilotBoat					model;
	private BoatPropertiesMainPage	newBoatPage;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NewBoatWizard(final PilotBoat newWizardModel) {
		super();
		if (null == newWizardModel)
			throw new NullPointerException("The wizards should receive a valid WizardModel.");
		model = newWizardModel;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void addPages() {
		newBoatPage = new BoatPropertiesMainPage(workbench, selection);
		addPage(newBoatPage);
	}

	@Override
	public boolean canFinish() {
		final IStatus status = newBoatPage.getStatus();
		if (status.getSeverity() == IStatus.OK)
			return true;
		else
			return false;
	}

	public PilotBoat getModel() {
		return model;
	}

	@Override
	public void init(final IWorkbench workbench, final IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
	}

	@Override
	public boolean performFinish() {
		newBoatPage.saveDataToModel();
		return true;
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This is the class that defines the page contents. It has the code to continually control the user input and
 * handle the errors and the storage of the Boat configuration data.
 */
class BoatPropertiesMainPage extends WizardPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the workbench used to display some elements. */
	//	protected IWorkbench						workbench;
	//	protected IStructuredSelection	selection;
	/**
	 * This is the current wizards status. It will be changed by any user action that creates an invalid or
	 * erroneous configuration.
	 */
	protected IStatus	pageStatus;

	//- Widgets on the page
	private Text			boatNameText;
	private Text			boatEmailText;
	private Text			boatPasswordText;
	private Button		validateLogin;
	private Text			boatIdText;
	private Text			boatClefText;
	private PilotBoat	boatModel;
	private boolean		validated	= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatPropertiesMainPage(final IWorkbench workbench, final IStructuredSelection selection) {
		super("Page1");
		setTitle("Enter Boat Login Identification");
		setDescription("Enter the Boat identification data such as the name, \nthe email used to register the Boat and the password used to login in the game to control this boat.");
		setImageDescriptor(ImageFactory.getImageDescriptor("icons/addboat_wiz.png"));
		//		this.workbench = workbench;
		//		this.selection = selection;
		pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat name can not be left empty.", null);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean canFlipToNextPage() {
		return false;
	}

	/**
	 * This methods creates the structure of components and containers in the page. The current contents
	 * structure is:<br>
	 * <ul>
	 * <li><b>composite</b> - this is the root container of all the other elements. It has two columns of
	 * different sizes, one for the text and the other for the buttons.</li>
	 * <li><b>nameValidation</b> - another Composite that contains the boat name Label and the Boat Validation
	 * button.</li>
	 * <li><b>controlValidation</b> - a new Composite to include the Login check before accepting the current
	 * Boat configuration.</li>
	 * </ul>
	 * 
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(final Composite parent) {
		//- Create the composite to hold the widgets
		final Composite composite = new Composite(parent, SWT.NULL);
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.makeColumnsEqualWidth = false;
		composite.setLayout(grid);

		//- Boat Name
		new Label(composite, SWT.NONE).setText("Boat Name:");

		final Composite nameValidation = new Composite(composite, SWT.NONE);
		GridLayout gridPath = new GridLayout();
		gridPath.numColumns = 3;
		gridPath.makeColumnsEqualWidth = false;
		gridPath.marginRight = 8;
		gridPath.marginWidth = 0;
		nameValidation.setLayout(gridPath);

		boatNameText = new Text(nameValidation, SWT.BORDER);
		GridData grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 200;
		boatNameText.setLayoutData(grdata);
		boatNameText.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(final KeyEvent e) {
			}

			@Override
			public void keyReleased(final KeyEvent e) {
				//- Check if the name is empty or still not validated.
				//				if (boatNameText.getText().isEmpty()) {
				//					pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat name can not be left empty.", null);
				//				} else {
				//					pageStatus = new Status(IStatus.WARNING, "not_used", 0, "The Boat name has to be validated before accepted.",
				//							null);
				validated = false;
				//				}
				applyToStatusLine(pageStatus);
			}
		});
		final Button validate = new Button(nameValidation, SWT.NONE);
		validate.setText("Validate Boat");
		validate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent e) {
				//- Validate this boat against the game database
				BoatPropertiesMainPage.this.validateBoatName(boatNameText.getText());
			}
		});

		//- Boat Identification email
		new Label(composite, SWT.NONE).setText("Boat Email:");
		boatEmailText = new Text(composite, SWT.BORDER);
		grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 200;
		boatEmailText.setLayoutData(grdata);
		boatEmailText.setEditable(false);
		boatEmailText.addKeyListener(new AuthenticationKeyListener());

		//- Boat Identification number				
		new Label(composite, SWT.NONE).setText("Boat Password:");

		final Composite controlValidation = new Composite(composite, SWT.NONE);
		gridPath = new GridLayout();
		gridPath.numColumns = 2;
		gridPath.makeColumnsEqualWidth = false;
		gridPath.marginRight = 8;
		gridPath.marginWidth = 0;
		controlValidation.setLayout(gridPath);

		boatPasswordText = new Text(controlValidation, SWT.BORDER + SWT.PASSWORD);
		grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 200;
		boatPasswordText.setLayoutData(grdata);
		boatPasswordText.setEditable(false);
		boatPasswordText.addKeyListener(new AuthenticationKeyListener());
		//
		//			@Override
		//			public void keyPressed(final KeyEvent e) {
		//			}
		//
		//			@Override
		//			public void keyReleased(final KeyEvent e) {
		//				//- Check if the name is empty or still not validated.
		//				if (!boatNameText.getText().isEmpty()) {
		//					//- Show the validation button and activate it
		//					validateLogin.setVisible(true);
		//					boatIdText.setEditable(true);
		//					boatClefText.setEditable(true);
		//					pageStatus = new Status(IStatus.WARNING, "not_used", 0, "Validate the Login configuration before saving.",
		//							null);
		//				} else {
		//					validateLogin.setVisible(false);
		//					pageStatus = new Status(IStatus.OK, "not_used", 0,
		//							"Boat validated. Enter Email & Password to allow control.", null);
		//				}
		//				applyToStatusLine(pageStatus);
		//			}
		//		});
		validateLogin = new Button(controlValidation, SWT.NONE);
		validateLogin.setText("Validate Login");
		validateLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent e) {
				//- Validate this boat against the game database
				BoatPropertiesMainPage.this.validateBoatLogin();
			}
		});
		validateLogin.setVisible(false);

		//- Boat Identification number				
		new Label(composite, SWT.NONE).setText("Boat Code Number:");
		boatIdText = new Text(composite, SWT.BORDER);
		boatIdText.setEditable(false);
		boatIdText.addKeyListener(new AuthenticationKeyListener());

		//- Boat clef
		new Label(composite, SWT.NONE).setText("Boat Clef:");
		boatClefText = new Text(composite, SWT.BORDER);
		grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 300;
		boatClefText.setLayoutData(grdata);
		boatClefText.setEditable(false);
		boatClefText.addKeyListener(new AuthenticationKeyListener());

		//- Set the composite as the control for this page
		setControl(composite);

		//- Load interface values with model values.
		onEnterPage();
	}

	private PilotBoat getBoatModel() {
		if (null == boatModel) {
			final NewBoatWizard wizard = (NewBoatWizard) getWizard();
			boatModel = wizard.getModel();
		}
		return boatModel;
	}

	class AuthenticationKeyListener implements KeyListener {

		@Override
		public void keyPressed(final KeyEvent e) {
		}

		@Override
		public void keyReleased(final KeyEvent event) {
			//- Check if the Boat identifier has been validated.
			PilotBoat model = getBoatModel();
			if (model.getState() == ModelStates.INVALID) {
				pageStatus = new Status(IStatus.WARNING, "not_used", 0, "The Boat name has to be validated before accepted.",
						null);
			} else {
				//- Check if the authentication is valid before activating the validation button
				if ((boatEmailText.getText().isEmpty()) && (boatPasswordText.getText().isEmpty())) {
					//				if (!boatNameText.getText().isEmpty()) {
					//					//- Show the validation button and activate it
					//					validateLogin.setVisible(true);
					//					boatIdText.setEditable(true);
					//					boatClefText.setEditable(true);
					//					pageStatus = new Status(IStatus.WARNING, "not_used", 0, "Validate the Login configuration before saving.",
					//							null);
					//				} else {
					validateLogin.setVisible(false);
					boatIdText.setEditable(false);
					boatClefText.setEditable(false);
					pageStatus = new Status(IStatus.OK, "not_used", 0,
							"Boat validated. Enter Email & Password to allow control.", null);
				} else if ((boatEmailText.getText().isEmpty()) || (boatPasswordText.getText().isEmpty())
						|| (boatIdText.getText().isEmpty()) || (boatClefText.getText().isEmpty())) {
					//					validateLogin.setVisible(true);
					boatIdText.setEditable(true);
					boatClefText.setEditable(true);
					pageStatus = new Status(IStatus.WARNING, "not_used", 0,
							"Validate authentication or enter authentication data before saving.", null);
				} else {
					pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat authentication confirmed. Boat under control.", null);
				}
			}
			if ((!boatEmailText.getText().isEmpty()) && (!boatPasswordText.getText().isEmpty())) {
				validateLogin.setVisible(true);
			} else {
				validateLogin.setVisible(false);
			}
			applyToStatusLine(pageStatus);
		}
	}

	@Override
	public IWizardPage getNextPage() {
		return null;
	}

	public IStatus getStatus() {
		return pageStatus;
	}

	public void saveDataToModel() {
		// Gets the model
		//		final NewBoatWizard wizard = (NewBoatWizard) getWizard();
		final PilotBoat model = getBoatModel();
		model.setBoatName(boatNameText.getText());
		model.setBoatEmail(boatEmailText.getText());
		model.setBoatPassword(boatPasswordText.getText());
		model.setBoatId(boatIdText.getText());
		model.setBoatClef(boatClefText.getText());
	}

	/**
	 * Applies the status to the status line of a dialog page.
	 */
	protected void applyToStatusLine(final IStatus status) {
		final PilotBoat model = getBoatModel();
		//- Perform any checks and set the right status and message.
		//- EMPTY
		if (boatNameText.getText().isEmpty()) {
			pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat name can not be left empty.", null);
			boatNameText.setEditable(true);
			boatEmailText.setEditable(false);
			boatPasswordText.setEditable(false);
			boatIdText.setEditable(false);
			boatClefText.setEditable(false);
			validateLogin.setVisible(false);
			//			return;
		}
		//- VALIDATE NAME. This is checked on the BoatName key listener
		if (validated) {
			pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat validated. Enter Email & Password to allow control.",
					null);
			boatEmailText.setEditable(true);
			boatPasswordText.setEditable(true);
		}
		if (!boatNameText.getText().isEmpty() && !validated) {
			pageStatus = new Status(IStatus.WARNING, "not_used", 0, "The Boat name has to be validated before accepted.",
					null);
		}
		//- NAME VALID
		//		if (model.getState() != ModelStates.INVALID) {
		//			pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat validated. Enter Email & Password to allow control.",
		//					null);
		//			boatEmailText.setEditable(true);
		//			boatPasswordText.setEditable(true);
		//		}
		//- ENTER EMAIL PASSWD
		if ((boatEmailText.getText().isEmpty()) && (boatPasswordText.getText().isEmpty())) {
			validateLogin.setVisible(false);
			boatIdText.setEditable(false);
			boatClefText.setEditable(false);
			//			pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat validated. Enter Email & Password to allow control.",
			//					null);
		}
		//- VALIDATE AUTHENTICATION
		if (!boatEmailText.getText().isEmpty() && !boatPasswordText.getText().isEmpty()) {
			//				|| (boatIdText.getText().isEmpty()) || (boatClefText.getText().isEmpty())) {
			//					validateLogin.setVisible(true);
			pageStatus = new Status(IStatus.WARNING, "not_used", 0,
					"Validate authentication or enter authentication data before saving.", null);
			boatIdText.setEditable(true);
			boatClefText.setEditable(true);
			validateLogin.setVisible(true);
		}
		if (!boatIdText.getText().isEmpty() && !boatClefText.getText().isEmpty()) {
			if ((boatEmailText.getText().isEmpty()) || (boatPasswordText.getText().isEmpty())) {
				pageStatus = new Status(IStatus.WARNING, "not_used", 0,
						"Boat not complete. Enter Email & Password to allow control.", null);
			} else {
				pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat authentication confirmed. Boat under control.", null);
			}
		}
		//else {
		//			pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat authentication confirmed. Boat under control.", null);
		//		}
		//		if ((!boatEmailText.getText().isEmpty()) && (!boatPasswordText.getText().isEmpty())) {
		//			validateLogin.setVisible(true);
		//		} else {
		//			validateLogin.setVisible(false);
		//		}

		String message = pageStatus.getMessage();
		if (message.length() == 0) {
			message = null;
		}
		switch (pageStatus.getSeverity()) {
			case IStatus.OK:
				setErrorMessage(null);
				this.setMessage(message);
				break;
			case IStatus.WARNING:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.WARNING);
				break;
			case IStatus.INFO:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.INFORMATION);
				break;
			default:
				setErrorMessage(message);
				this.setMessage(null);
				break;
		}
		getWizard().getContainer().updateButtons();
	}

	private boolean checkNull(String value) {
		if (null == value)
			return true;
		if (value.equals(""))
			return true;
		if (value.length() == 0)
			return true;
		return false;
	}

	/**
	 * This method is called when we enter the page and used to load the fields with the current values if we
	 * are not creating a new Boat but editing an existing one.
	 */
	private void onEnterPage() {
		//final NewBoatWizard wiz = (NewBoatWizard) getWizard();
		final PilotBoat model = getBoatModel();

		//- Detect then when the name is not empty we are editing a Boat. Set it not editable
		if ((null != model.getBoatName()) && (!model.getBoatName().equals(""))) {
			boatNameText.setText(model.getBoatName());
			boatNameText.setEditable(false);
			validateBoatName(model.getBoatName());
		}
		if (null != model.getBoatEmail()) {
			boatEmailText.setText(model.getBoatEmail());
			//			boatEmailText.setEditable(true);
			//			boatPasswordText.setEditable(true);
		}
		if (null != model.getBoatPassword()) {
			boatPasswordText.setText(model.getBoatPassword());
			//			boatPasswordText.setEditable(true);
			//			boatEmailText.setEditable(true);
			//			validateLogin.setVisible(true);
			//			boatIdText.setEditable(true);
			//			boatClefText.setEditable(true);
		}
		if (null != model.getBoatId()) {
			boatIdText.setText(model.getBoatId());
			//			boatIdText.setEditable(true);
		}
		if (null != model.getBoatClef()) {
			boatClefText.setText(model.getBoatClef());
			//			boatClefText.setEditable(true);
		}
		applyToStatusLine(pageStatus);
	}

	private void validateBoatName(final String boatName) {
		//- Get access to the model
		final PilotBoat model = getBoatModel();

		model.setBoatName(boatNameText.getText());
		try {
			model.updateBoatData();
			validated = true;
		} catch (final Exception exc) {
			pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat " + boatNameText.getText()
					+ " was not found on the game database.", null);
			model.setState(ModelStates.INVALID);
			validated = false;
			return;
		}
		applyToStatusLine(pageStatus);
	}

	private void validateBoatLogin() {
		//- Save current model data before attemptimg to check the login
		saveDataToModel();
		//- Get access to the model
		//		final NewBoatWizard wizard = (NewBoatWizard) getWizard();
		final PilotBoat model = getBoatModel();
		//		model.setBoatName(boatNameText.getText());
		//		if (!boatEmailText.getText().isEmpty()) {
		//			model.setBoatEmail(boatEmailText.getText());
		//		}
		//		if (!boatPasswordText.getText().isEmpty()) {
		//			model.setBoatPassword(boatPasswordText.getText());
		//		}
		try {
			model.loginBoat();
			//- Get back the authentication data from the request
			if (null != model.getBoatId()) {
				boatIdText.setText(model.getBoatId());
				boatIdText.setEditable(true);
			}
			if (null != model.getBoatClef()) {
				boatClefText.setText(model.getBoatClef());
				boatClefText.setEditable(true);
			}

			pageStatus = new Status(IStatus.OK, "not_used", 0, "Boat authentication confirmed. Boat under control.", null);
		} catch (final Exception exc) {
			pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat " + boatNameText.getText()
					+ " was not found on the game database.", null);
			model.setState(ModelStates.INVALID);
		}
		applyToStatusLine(pageStatus);
	}
}
// - UNUSED CODE ............................................................................................
