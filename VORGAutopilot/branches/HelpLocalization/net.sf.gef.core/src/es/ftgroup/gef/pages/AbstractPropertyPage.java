//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.gef.pages;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.Assert;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import net.sf.gef.core.model.AbstractPropertyChanger;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractPropertyPage implements PropertyChangeListener, IPropertyPage {
	//	private static Logger						logger				= Logger.getLogger("es.ftgorup.gef.pages");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S

	// - F I E L D S
	/**
	 * A value of <code>true</code> in this field signals that this instance is registered as a listener to the
	 * <code>unitModel</code> property change notification mechanism.
	 */
	protected boolean								active	= false;
	/**
	 * Reference to the link Control where all page composition will be linked. This element is located in the
	 * <code>SelectionView</code> control contents and it is received by a sentence.
	 */
	protected transient Composite		container;
	/** Group element that will contain all the unit properties to be presented on selection. */
	public Group										page;
	/** Reference to the model. But limited to the interface level of a Unit. */
	private AbstractPropertyChanger	anyModel;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the
	 * <code>SelectionView</code> at runtime. Other fields may be <code>null</code> and in those situation the
	 * code will generate a informational message.
	 */
	public AbstractPropertyPage(final Composite top) {
		container = top;
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Get the associated model element. At this class this model will be subclassed to the <code>Unit</code>
	 * interface. If the model is null then we should throw an exception to signal some internal code error.
	 * 
	 * @return the referenced model at the lower level. This is required by the
	 *         <code>PropertyChangeListener</code> interface.
	 */
	public AbstractPropertyChanger getModel() {
		Assert.isNotNull(anyModel, "The page unit model instance is not set up but it is being accessed.");
		return anyModel;
	}

	/**
	 * Store a reference to the model inside the page. Check also if a value existed previously to call the
	 * notification registration mechanism so this instance will receive any property modification to the model
	 * reference.
	 */
	public void setModel(final AbstractPropertyChanger model) {
		if (null != model) deactivate();
		anyModel = model;
		activate();
	}

	/** Changes the parent container where to hang the new SQL interface elements. */
	public void setContainer(final Composite parent) {
		Assert.isNotNull(parent, "The page view contained that is received is null.");
		container = parent;
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Return the constructed page with the interface SWL components. If the <code>build()</code> has not been
	 * called then if will return a fake page with a single Label.
	 */
	public Group getPropertyPage() {
		if (null == page) {
			page = new Group(container, SWT.NONE);
			page.setText("Building ERROR");
		}
		return page;
	}

	// - P R O T E C T E D - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - P R O P E R T Y C H A N G E L I S T E N E R

	public abstract void propertyChange(PropertyChangeEvent evt);

	// - I N T E R F A C E - I P R O P E R T Y P A G E
	/**
	 * Activates or deactivates the connection to the property listener. Any change on a target property will
	 * fire a call on the <code>propertyChange</code> method.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#activate()
	 */
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		anyModel.addPropertyChangeListener(this);
		active = true;
	}

	protected abstract void createContents();

	/**
	 * Deactivates the property listening to the model changes and clears the activity flag.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#deactivate()
	 */
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		getModel().removePropertyChangeListener(this);
		active = false;
	}

	/** Accessor to the <code>active</code> notification listener flag. */
	private boolean isActive() {
		return active;
	}

	/**
	 * Called when the page is no longer accessible and the SWT elements have been disposed from the viewer.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#dispose()
	 */
	public void dispose() {
		deactivate();
		anyModel = null;
		container = null;
		// location.dispose();
		page.dispose();
	}
}

// - UNUSED CODE ............................................................................................
