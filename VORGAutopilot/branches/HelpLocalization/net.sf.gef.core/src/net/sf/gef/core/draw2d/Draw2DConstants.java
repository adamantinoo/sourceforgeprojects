//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: AssistantConstants.java 200 2009-03-16 15:08:46Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-16 16:08:46 +0100 (lun, 16 mar 2009) $
//  RELEASE:        $Revision: 200 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.draw2d;

//- IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface Draw2DConstants {
	//	//- G L O B A L - S E C T I O N ..........................................................................
	//	//- D E F I N I T I O N   C O L O R S
	//	Color		COLOR_STANDARD_RED					= ColorConstants.red;
	//	Color		COLOR_STANDARD_ORANGE				= ColorConstants.orange;
	//	Color		COLOR_STANDARD_DARKBLUE			= ColorConstants.darkBlue;
	//	Color		COLOR_STANDARD_BLUE					= ColorConstants.blue;
	//	Color		COLOR_STANDARD_DARKGREN			= ColorConstants.darkGreen;
	//	Color		COLOR_STANDARD_GREEN				= ColorConstants.green;
	//
	//	Color		COLOR_BROWN									= new Color(Display.getCurrent(), 204, 102, 0);
	//
	//	Color		COLOR_BRILLIANT_RED					= new Color(Display.getDefault(), 0xFF, 0x33, 0x00);
	//	Color		COLOR_LIGHT_RED							= new Color(Display.getDefault(), 0xFF, 0x80, 0x80);
	//	Color		COLOR_MEDIUM_RED						= new Color(Display.getDefault(), 0xE0, 0x33, 0x00);
	//	Color		COLOR_DARK_RED							= new Color(Display.getDefault(), 0x33, 0x00, 0x00);
	//	Color		COLOR_BRILLIANT_GREEN				= new Color(Display.getDefault(), 0x00, 0xFF, 0x00);
	//	Color		COLOR_LIGHT_GREEN						= new Color(Display.getDefault(), 0x80, 0xFF, 0x80);
	//	Color		COLOR_MEDIUM_GREEN					= new Color(Display.getDefault(), 0x00, 0xE0, 0x00);
	//	Color		COLOR_DARK_GREEN						= new Color(Display.getDefault(), 0x00, 0xA0, 0x00);
	//	Color		COLOR_BRILLIANT_BLUE				= new Color(Display.getDefault(), 0x00, 0x00, 0xFF);
	//	Color		COLOR_LIGHT_BLUE						= new Color(Display.getDefault(), 0x80, 0x80, 0xFF);
	//	Color		COLOR_MEDIUM_BLUE						= new Color(Display.getDefault(), 0x00, 0x10, 0xE0);
	//	Color		COLOR_DARK_BLUE							= new Color(Display.getDefault(), 0x00, 0x00, 0x33);
	//	Color		COLOR_BRILLIANT_YELLOW			= new Color(Display.getDefault(), 0xFF, 0xFF, 0x00);
	//	Color		COLOR_LIGHTEST_YELLOW				= new Color(Display.getDefault(), 0xFE, 0xFE, 0xEE);
	//	Color		COLOR_LIGHT_YELLOW					= new Color(Display.getDefault(), 0x80, 0x80, 0x00);
	//	Color		COLOR_MEDIUM_YELLOW					= new Color(Display.getDefault(), 0xE0, 0xE0, 0x00);
	//	Color		COLOR_DARK_YELLOW						= new Color(Display.getDefault(), 0xF8, 0xBB, 0x05);
	//
	//	Color		COLOR_DARK_GRAY							= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);
	//	Color		COLOR_GRAY									= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);
	//	Color		COLOR_LIGHT_GRAY						= ColorConstants.lightGray;
	//	Color		COLOR_LIGHTEST_GRAY					= new Color(Display.getDefault(), 0xE4, 0xE4, 0xE4);
	//
	//	//- M A P   C O L O R S
	//	Color		COLOR_BASE_OWNED						= COLOR_DARK_GREEN;
	//	Color		COLOR_BASE_FRIEND						= COLOR_DARK_GREEN;
	//	Color		COLOR_BASE_ENEMY						= COLOR_BRILLIANT_RED;
	//	Color		COLOR_BASE_NEUTRAL					= COLOR_DARK_YELLOW;
	//	Color		COLOR_BASE_RING							= ColorConstants.lightGray;
	//	Color		COLOR_PLANET_UNEXPLORED			= ColorConstants.lightGray;
	//	Color		COLOR_PLANET_NOINFO					= ColorConstants.darkGray;
	//	Color		COLOR_PLANET_INFO						= COLOR_MEDIUM_BLUE;
	//	Color		COLOR_PLANET_OBSOLETE				= new Color(Display.getDefault(), 0xE4, 0x81, 0xE4);
	//	Color		COLOR_PLANET_CLASS_A				= COLOR_BRILLIANT_RED;
	//	Color		COLOR_PLANET_CLASS_B				= COLOR_BRILLIANT_RED;
	//	Color		COLOR_PLANET_CLASS_C				= ColorConstants.darkGray;
	//	Color		COLOR_PLANET_NATIVES				= ColorConstants.orange;
	//
	//	Color		COLOR_SHIP_DEFAULT					= COLOR_DARK_GREEN;
	//	Color		COLOR_SHIP_FRIEND						= COLOR_DARK_GREEN;
	//	Color		COLOR_SHIP_ENEMY						= COLOR_BRILLIANT_RED;
	//	Color		COLOR_SHIP_NEUTRAL					= COLOR_BRILLIANT_RED;
	//
	//	//- F F   C O M B I N A T I O N   C O L O R S
	//	Color		COLOR_FF_UNDEFINED					= COLOR_GRAY;
	//	Color		COLOR_FF_FOE								= COLOR_BRILLIANT_RED;
	//	Color		COLOR_FF_FRIEND							= COLOR_DARK_GREEN;
	//	Color		COLOR_FF_FOEFRIEND					= new Color(Display.getDefault(), 0xB0, 0x0D, 0xF0);
	//	Color		COLOR_FF_NEUTRAL						= COLOR_DARK_YELLOW;
	//	Color		COLOR_FF_FOENEUTRAL					= new Color(Display.getDefault(), 0x80, 0xC0, 0xEE);
	//	Color		COLOR_FF_FRIENDNEUTRAL			= new Color(Display.getDefault(), 0xC8, 0xE0, 0x8D);
	//	Color		COLOR_FF_FOEFRIENDNEUTRAL		= ColorConstants.black;
	//
	//	//- R E S O U R C E S   C O L O R S
	//	Color		COLOR_NEUTRONIUM_MINERAL		= COLOR_BRILLIANT_RED;
	//	Color		COLOR_NEUTRONIUM_ORE				= COLOR_STANDARD_ORANGE;
	//	Color		COLOR_DURANIUM_MINERAL			= COLOR_STANDARD_DARKBLUE;
	//	Color		COLOR_DURANIUM_ORE					= COLOR_STANDARD_BLUE;
	//	Color		COLOR_TRITANIUM_MINERAL			= COLOR_STANDARD_DARKGREN;
	//	Color		COLOR_TRITANIUM_ORE					= COLOR_STANDARD_GREEN;
	//	Color		COLOR_MOLYBDENUM_MINERAL		= COLOR_BROWN;
	//	Color		COLOR_MOLYBDENUM_ORE				= COLOR_STANDARD_ORANGE;
	//	Color		COLOR_NATIVES_DECORATOR			= COLOR_BROWN;																							//ColorConstants.orange;
	//	Color		COLOR_NATIVES_DEFAULT				= COLOR_BROWN;																							//ColorConstants.orange;
	//	Color		COLOR_CONTRABAND_DECORATOR	= new Color(Display.getDefault(), 0xB0, 0x0D, 0xF0);
	//	Color		COLOR_FARMING_NORMAL				= COLOR_DARK_GREEN;
	//	Color		COLOR_FARMING_HOT						= COLOR_BRILLIANT_RED;
	//	Color		COLOR_FARMING_COLD					= COLOR_DARK_BLUE;																					//new Color(Display.getDefault(),0x80,0xC0,0xEE);
	//
	//	//- B A C K G R O U N D   C O L O R S
	//	Color		COLOR_BACKGROUND_MAP				= ColorConstants.white;
	//	Color		COLOR_BACKGROUND_VIEW				= ColorConstants.white;
	//	Color		COLOR_TIP										= ColorConstants.tooltipBackground;
	//	Color		COLOR_UNDEFINED_BACKGROUND	= COLOR_LIGHTEST_YELLOW;
	//	Color		COLOR_FRIEND_BACKGROUND			= new Color(Display.getDefault(), 0xD1, 0xFC, 0xD7);
	//	Color		COLOR_FOE_BACKGROUND				= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
	//	Color		COLOR_NEUTRAL_BACKGROUND		= new Color(Display.getDefault(), 0xFD, 0xDB, 0xD5);
	//
	//- F O N T S
	Font	FONT_ROUNDEDGROUP_DEFAULT		= new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL);
	Font	FONT_ROUNDEDGROUP_BOLD			= new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD);
	Font	FONT_STANDARDLABEL_DEFAULT	= new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL);
	Font	FONT_STANDARDLABEL_BOLD			= new Font(Display.getDefault(), "Tahoma", 8, SWT.BOLD);
	//
	//	//- G L O B A L   C O N S T A N T S
	//	int			DEFAULT_ZOOM_FACTOR					= 10;
	//	int			SIZE_ICONIMAGE							= 16;
	//
	//	// - I N F O R M A T I O N - R A C E S
	//	int			RACE_DEFAULT								= 101;
	//	int			RACE_CYBORG									= 106;
	//	int			RACE_EVILEMPIRE							= 108;
	//
	//	//- E N U M E R A T E D   C O N S T A N T S
	//	String	MODELTYPE_BASE							= "MODELTYPE_BASE";
	//	String	MODELTYPE_PLANET						= "MODELTYPE_PLANET";
	//	String	MODELTYPE_SHIP							= "MODELTYPE_SHIP";
	//	String	MODELTYPE_THING							= "TYPE_THING";
	//	String	MODELTYPE_POD								= "MODELTYPE_POD";
	//	String	MODELTYPE_WING							= "MODELTYPE_WING";
	//	String	MODELTYPE_GAMER							= "MODELTYPE_GAMER";
	//
	//	// - I N F O R M A T I O N - L E V E L S
	//	String	INFOLEVEL_UNEXPLORED				= "INFOLEVEL_UNEXPLORED";
	//	String	INFOLEVEL_NOINFO						= "INFOLEVEL_NOINFO";
	//	String	INFOLEVEL_INFO							= "INFOLEVEL_INFO";
	//	String	INFOLEVEL_OBSOLETE					= "INFOLEVEL_OBSOLETE";
	//	String	INFOLEVEL_NATIVES						= "INFOLEVEL_NATIVES";
	//	String	FFSTATUS_UNDEFINED					= "FFSTATUS_UNDEFINED";
	//	String	FFSTATUS_FRIEND							= "FFSTATUS_FRIEND";
	//	String	FFSTATUS_NEUTRAL						= "FFSTATUS_NEUTRAL";
	//	String	FFSTATUS_FOE								= "FFSTATUS_FOE";
}

// - UNUSED CODE ............................................................................................
