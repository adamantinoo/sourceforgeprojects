//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: DetailedPlanetFigure.java 177 2008-07-03 07:11:53Z boneymen $
//  LAST UPDATE:    $Date: 2008-07-03 09:11:53 +0200 (jue, 03 jul 2008) $
//  RELEASE:        $Revision: 177 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;

import net.sf.gef.core.draw2d.RoundedGroup;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.core.enums.ModelStates;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotBoatFigure extends RoundedGroup {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	private final PilotBoat	pilot;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotBoatFigure(final PilotBoat pilotBoat) {
		super(8);
		pilot = pilotBoat;
		groupName.setFont(PilotUIConstants.FONT_GROUPNAME);
		groupName.setForegroundColor(PilotUIConstants.COLOR_GROUPNAME);
		setOpaque(true);
		refreshContents();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void refreshContents() {
		//- Show any error or warning information.
		if (pilot.hasErrors()) {
			setText(pilot.getBoatName() + " [" + pilot.getErrorMessage() + "]");
			groupName.setIcon(ImageFactory.getImage(ImageKeys.ERROR_BOAT));
		} else {
			setStateIcon(pilot.getState());
			setText(pilot.getBoatName() + " - " + pilot.getLastBoatUpdate());
		}
		groupName.setIconTextGap(3);
		updateBackgroundColor();

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	@Override
	public void add(IFigure figure, Object constraint, int index) {
		super.add(figure, constraint, index);
	}

	private void setStateIcon(ModelStates state) {
		//- Set the default icon to a disabled boat
		groupName.setIcon(ImageFactory.getImage(ImageKeys.DISABLED_BOAT));
		if (state == ModelStates.READY) {
			groupName.setIcon(ImageFactory.getImage(ImageKeys.READY_BOAT));
		}
		if (state == ModelStates.ONHOLD) {
			groupName.setIcon(ImageFactory.getImage(ImageKeys.ONHOLD_BOAT));
		}
	}

	private void updateBackgroundColor() {
		setBackgroundColor(PilotUIConstants.COLOR_NOTREADY);
		ModelStates state = pilot.getState();
		if (state == ModelStates.READY) {
			setBackgroundColor(PilotUIConstants.COLOR_READY);
		}
		if (state == ModelStates.CONFIGURED) {
			setBackgroundColor(PilotUIConstants.COLOR_CONFIGURED);
		}
		if (state == ModelStates.ONHOLD) {
			setBackgroundColor(PilotUIConstants.COLOR_ONHOLD);
		}
	}
}
// - UNUSED CODE ............................................................................................
