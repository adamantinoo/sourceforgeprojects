//  PROJECT:        net.sf.vorg.rcpapplication
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core;

// - IMPORT SECTION .........................................................................................
import org.eclipse.osgi.util.NLS;

// - CLASS IMPLEMENTATION ...................................................................................
public class Externalizer extends NLS {
	private static final String	BUNDLE_NAME							= "net.sf.vorg.resources.externalized"; //$NON-NLS-1$
	public static String				INITIAL_SEPARATOR				= "";
	public static String				INTERMEDIATE_SEPARATOR	= "   ";
	public static String				ActiveWaypointFigure_activewaypoint;
	public static String				ActiveWaypointFigure_angle;
	public static String				ActiveWaypointFigure_awdlimits;
	public static String				ActiveWaypointFigure_distance;
	public static String				ActiveWaypointFigure_estimatedawd;
	public static String				ActiveWaypointFigure_estimatedcourse;
	public static String				ActiveWaypointFigure_fixedawd;
	public static String				ActiveWaypointFigure_knts;
	public static String				ActiveWaypointFigure_location;
	public static String				ActiveWaypointFigure_nm;
	public static String				ActiveWaypointFigure_port;
	public static String				ActiveWaypointFigure_power;
	public static String				ActiveWaypointFigure_resultspeed;
	public static String				ActiveWaypointFigure_selectedawd;
	public static String				ActiveWaypointFigure_selectedcourse;
	public static String				ActiveWaypointFigure_selectedsail;
	public static String				ActiveWaypointFigure_starboard;
	public static String				ActiveWaypointFigure_type;
	public static String				ActiveWaypointFigure_waypointrange;
	public static String				ActiveWaypointFigure_wptangle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Externalizer.class);
	}

	private Externalizer() {
	}
}

// - UNUSED CODE ............................................................................................
