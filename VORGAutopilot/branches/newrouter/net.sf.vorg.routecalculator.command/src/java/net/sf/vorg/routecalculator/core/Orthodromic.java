//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class Orthodromic {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	origin;
	private final GeoLocation	destination;
	private double						distance;
	private double						incourse	= 0.0;
	private double						constantx;
	private double						constantk;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Orthodromic(GeoLocation start, GeoLocation end) {
		origin = start;
		destination = end;
		calculateGreatestCircle();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getOrthoLatitude(double targetLongitude) {
		double lat = waylat(targetLongitude);
		return new GeoLocation(lat, targetLongitude);
	}

	public double waylat(double targetLongitude) {
		double winlon = GeoLocation.adjustAngleTo360(targetLongitude);
		double wlat = 0;
		double wrinlon = 0;
		double wrlat = 0;
		// double x = constantx;
		// double k = constantk;
		double z = 0;
		double wlatsign = 0;
		double wlatdeg = 0;
		double wlatmin = 0;
		double walat = 0;

		// wrinlon = Math.toRadians(winlon);
		z = Math.toRadians(winlon) - constantx;
		if (z < 0) {
			z = z + (2 * Math.PI);
		}

		wrlat = Math.atan(Math.sin(z) * constantk);
		wlat = Math.toDegrees(wrlat);
		return wlat;
		// walat = Math.abs(wlat);
		// wlatsign = 0;
		// if (wlat != 0) {
		// wlatsign = wlat / walat;
		// }
		// wlatdeg = Math.floor(walat);
		// wlatmin = (walat - wlatdeg) * 60.0;
		// wlatmin = (Math.floor(wlatmin * 1000)) / 1000;
		//
		// targetLatDegree = wlatdeg;
		// targetLatMinunte = wlatmin;
	}

	private void calculateGreatestCircle() {
		// origin = new GeoLocation(ilatdeg, ilatmin, ilondeg, ilonmin);
		// destination = new GeoLocation(flatdeg, flatmin, flondeg, flonmin);
		double dlon = (destination.getLon() - origin.getLon());
		double sgndlon = 0.0 - (dlon / Math.abs(dlon));
		if (Math.abs(dlon) > 180) {
			dlon = (360.0 - Math.abs(dlon)) * sgndlon;
		}
		double rinlat = Math.toRadians(origin.getLat());
		double rfnlat = Math.toRadians(destination.getLat());
		double rinlon = Math.toRadians(origin.getLon());
		double rdlon = Math.toRadians(dlon);
		double rgcdist = (Math.sin(rinlat) * Math.sin(rfnlat) + Math.cos(rinlat) * Math.cos(rfnlat) * Math.cos(rdlon));

		// double rgcdist = (Math.sin(Math.toRadians(origin.getLat())) *
		// Math.sin(Math.toRadians(destination.getLat())) + Math
		// .cos(Math.toRadians(origin.getLat()))
		// * Math.cos(Math.toRadians(destination.getLat()))
		// * Math.cos(Math.toRadians((destination.getLon() - origin.getLon()))));
		rgcdist = Math.acos(rgcdist);
		double gcdist = Math.toDegrees(rgcdist);
		double rincourse = (Math.sin(rfnlat) - Math.cos(rgcdist) * Math.sin(rinlat))
				/ (Math.sin(rgcdist) * Math.cos(rinlat));

		// double rincourse = (Math.sin(Math.toRadians(destination.getLat())) - Math.cos(rgcdist)
		// * Math.sin(Math.toRadians(origin.getLat())))
		// / (Math.sin(rgcdist) * Math.cos(Math.toRadians(origin.getLat())));
		rincourse = Math.acos(rincourse);
		incourse = Math.toDegrees(rincourse);
		if (dlon < 0) {
			incourse = 360.0 - incourse;
		}
		distance = Math.abs(gcdist) * 60.0;
		double rcontx = Math.atan(Math.sin(rinlat) * Math.tan(rincourse));
		if (Math.tan(rincourse) < 0) {
			rcontx = rcontx + Math.PI;
		}
		double rcontn = rinlon;
		if (rcontn < 0) {
			rcontn = rcontn + (2 * Math.PI);
		}
		rcontx = rcontn - rcontx;
		if (rcontx < 0) {
			rcontx = rcontx + (2 * Math.PI);
		}
		if (rcontx > 2 * Math.PI) {
			rcontx = rcontx - (2 * Math.PI);
		}
		constantx = rcontx;
		double rcontk = Math.acos(Math.sin(rincourse) * Math.cos(rinlat));
		rcontk = Math.abs(Math.tan(rcontk));
		constantk = rcontk;
	}

	public double getInitialCourse() {
		return incourse;
	}
}

// - UNUSED CODE ............................................................................................
