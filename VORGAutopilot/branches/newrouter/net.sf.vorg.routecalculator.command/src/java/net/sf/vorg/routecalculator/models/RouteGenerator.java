//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.Intersection;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class has static methods to build any type of route or composition of routes. It will store all the
 * knowledge about the way to interconnect legs and to generate new route cells to build up the new routes.
 * This class will interface with the outside world through the RouteEnvironment classes that define how to
 * generate or compose a new route.
 */
public class RouteGenerator {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	public static Route generateRoute(RouteEnvironment environment) throws LocationNotInMap, RoutingException {
		// - Create a new Route and set the initial position and time form the environment data.
		GeoTimeLocation entryLocation = environment.getStartLocation();
		final Route genRoute = new Route();
		genRoute.setName(generateRouteCodeName(environment));
		genRoute.setStartPoint(environment.getStartLocation());
		int cellCounter = 0;
		boolean windChange = false;
		// - Generate a route leg for every wind change counter selected
		// - Get into the loop to generate cells and connect them until the stop condition is reached.
		while (!environment.getGenerationLimit().limitReached(cellCounter, windChange)) {
			// - Locate the initial wind cell for this location and time coordinates
			WindCell actualWindCell = WindMapHandler.cellWithPoint(entryLocation, entryLocation.getWindTime().getTime());
			// - Get the movement angle. This parameter is calculated by the environment algorithm.
			int movementAngle = environment.getAlgorithm().getMovementAngle(actualWindCell.getWindDir(),
					actualWindCell.getWindSpeed());
			// - Create a new route element based on the current route location and the movement angle.
			// This will access the correct wind cell and calculate the right intersections.
			RouteCell newCell = new RouteCell(entryLocation, movementAngle);
			actualWindCell = newCell.getCell();
			movementAngle = environment.getAlgorithm().getMovementAngle(actualWindCell.getWindDir(),
					actualWindCell.getWindSpeed());

			// - Calculate the intersection with the borders of the resulting wind cell.
			Vector<Intersection> intersections = actualWindCell.calculateIntersection(entryLocation, movementAngle);
			Directions nextDirection = intersections.lastElement().getDirection();
			GeoTimeLocation exitLocation = new GeoTimeLocation(intersections.lastElement().getLocation());
			newCell.setExitLocation(exitLocation);
			// - Update the time point of the new route point because while adding to the route is cloned.
			exitLocation.addTTC(entryLocation, newCell.getTTC());
			windChange = genRoute.add(newCell, nextDirection);
			cellCounter++;
			entryLocation = exitLocation;
		}

		// - Check if we have reached the count limit on the wind change counter.
		return genRoute;
	}

	private static String generateRouteCodeName(RouteEnvironment environment) throws RoutingException {
		String name = environment.getAlgorithmType() + " " + environment.getAngleParameter();
		return name;
	}

	public static void send2VRTool(String destinationFile, final Route targetRoute, final String title) {
		if (null != destinationFile) {
			try {
				final BufferedWriter writer = new BufferedWriter(new FileWriter(destinationFile, true));

				// - Compose the report.
				final StringBuffer buffer = new StringBuffer();
				buffer.append(VORGConstants.NEWLINE);
				buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
				// buffer.append("A;Color=$0000CCFF").append(VORGConstants.NEWLINE);
				// buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
				// buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
				// buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
				// buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
				buffer.append(targetRoute.vrtoolReport());
				buffer.append(VORGConstants.NEWLINE);
				writer.write(buffer.toString());
				writer.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void send2VRTool(String destinationFile, final Vector<IsoPoint> isochrone, final String title) {
		if (null != destinationFile) {
			try {
				final BufferedWriter writer = new BufferedWriter(new FileWriter(destinationFile, true));

				// - Compose the report.
				final StringBuffer buffer = new StringBuffer();
				buffer.append(VORGConstants.NEWLINE);
				buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
				buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
				buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
				buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
				buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
				buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
				for (final IsoPoint point : isochrone) {
					buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLocation().getLat())).append(";");
					buffer.append(FormatSingletons.nf4.format(point.getLocation().getLon() * -1.0)).append(";");
					buffer.append(point.getName());
					buffer.append(VORGConstants.NEWLINE);
				}
				writer.write(buffer.toString());
				writer.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static Route optimizeRoute(Route targetRoute) {
		GeoTimeLocation end = targetRoute.getEndLocation();
		return optimizeRoute(targetRoute, end);
	}

	public static Route optimizeRoute(Route targetRoute, GeoTimeLocation end) {
		Route winnerRoute = new Route();
		try {
			while (targetRoute.getStepCount() > 1) {
				RouteCell firstCell = targetRoute.getFirsCell();
				int direction = firstCell.getNextDirection();
				winnerRoute.add(firstCell, direction);
				// limit.setCellLimit(cellCount - winnerRoute.getStepCount());
				targetRoute = IsochroneOptimizer.optimizeIsochroneStep(firstCell.getExitLocation(), end, null);
			}
			RouteCell firstCell = targetRoute.getFirsCell();
			int direction = firstCell.getNextDirection();
			winnerRoute.add(firstCell, direction);
			return winnerRoute;
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteGenerator() {
	}
	// - M E T H O D - S E C T I O N ..........................................................................

}

// - UNUSED CODE ............................................................................................
