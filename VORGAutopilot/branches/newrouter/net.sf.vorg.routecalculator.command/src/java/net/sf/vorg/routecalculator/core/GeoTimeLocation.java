//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.util.TimeZoneAdapter;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class extends the geographical location adding the new time axis to identify a location in a time
 * point. This new addition is needed to control the generation of routes because of the time dependency of a
 * route to the start location and start time.
 */
public class GeoTimeLocation extends GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private Calendar			time			= GregorianCalendar.getInstance();
	private Calendar			windTime	= TimeZoneAdapter.changeTimeZone(GregorianCalendar.getInstance(), "Etc/GMT+1");

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GeoTimeLocation() {
	}

	public GeoTimeLocation(GeoLocation location) {
		if (null != location) setLocation(location);
	}

	public GeoTimeLocation(double lat, double lon, Calendar newTime) {
		this(new GeoLocation(lat, lon));
		time = newTime;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Return a clone of the time point related to the Game Time Zone and with the adjust necessary to compare
	 * it to the Wind Time Zone that is currently -2 hours displaced from GTZ.
	 */
	public Calendar getWindTime() {
		if (null != windTime) {
			Calendar now = (Calendar) windTime.clone();
			now.add(Calendar.HOUR, -2);
			return now;
		} else {
			windTime = TimeZoneAdapter.changeTimeZone(GregorianCalendar.getInstance(), "Etc/GMT+1");
			Calendar now = (Calendar) windTime.clone();
			now.add(Calendar.HOUR, -2);
			return now;
		}
	}

	public Calendar getTime() {
		return time;
	}

	public void setTime(Calendar newTime) {
		time = newTime;
		windTime = TimeZoneAdapter.changeTimeZone(newTime, "Etc/GMT+1");
	}

	public Calendar addTTC(GeoTimeLocation base, double hours) {
		time = (Calendar) base.time.clone();
		time.add(Calendar.SECOND, new Double(hours * VORGConstants.H2S).intValue());
		windTime = TimeZoneAdapter.changeTimeZone(time, "Etc/GMT+1");
		return time;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[GeoTimeLocation ");
		buffer.append("time=").append(time.getTime().toString()).append(",");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
