//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Intersection;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class TimedRoute extends Route {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected Date				initialTime;
	private int						timeLimit	= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TimedRoute() {
		super();
		initialTime = Calendar.getInstance().getTime();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Generates a partial route with VMG projection to the parameter angle that runs for the selected time. The
	 * start point is a general location that is the route base. The resulting route will traverses one or more
	 * cells up to the run time scheduled.
	 * 
	 * @throws LocationNotInMap
	 */
	public Route generateRoute(GeoLocation start, int minutes, int angle) {
		try {
			// - Initialize the timing system to locate the wind cells.
			timeLimit = minutes;
			double elapsed = 0.0;
			Calendar searchTime = this.getSearchTime();
			final WindCell startCell = WindMapHandler.cellWithPoint(start, searchTime.getTime());
			// - Get the best VMG angle
			VMCData vmc = new VMCData(angle, startCell);
			int bestAngle = vmc.getBestAngle();
			// bestAngle = angle;

			// - Get the intersection with the cell border and add this cell to the new route.
			Vector<Intersection> intersections = this.getIntersection(start, startCell, bestAngle);
			Directions nextDirection = intersections.lastElement().getDirection();
			GeoLocation currentRouteLocation = intersections.lastElement().getLocation();
			RouteCell newCell = new RouteCell(startCell, start, currentRouteLocation);
			double cellTTC = newCell.getTTC();
			if (Double.isInfinite(cellTTC)) throw new LocationNotInMap("InvalidRoute");
			if (this.checkIfArrivalTime(elapsed, cellTTC)) {
				// - Calculate the new end point by running the seconds left from the limit time.
				double seconds = this.timeLimit * 60.0 - ((elapsed) * 3600.0);
				GeoLocation endLocation = start.directEstimation(seconds, vmc.getBestSailConfiguration().getSpeed(), bestAngle);
				newCell.setExitLocation(endLocation);
				this.add(newCell, nextDirection);

				// - We have reached the end time limit so the sub route is complete.
				return this;
			} else
				this.add(newCell, nextDirection);

			// - Then calculate the next cells until the run time is reached. Set the iteration counter to a
			// expected limit.
			elapsed = this.getRouteTTC();
			WindCell nextCell = startCell;

			for (int i = 1; i <= 10; i++) {
				// - Get the next cell in the line using the direction and the connection with the cell boundaries.
				searchTime = this.getSearchTime();
				final GeoLocation nextLocation = nextCell.cellAtDirection(nextDirection);
				nextCell = WindMapHandler.cellWithPoint(nextLocation, searchTime.getTime());
				vmc = new VMCData(angle, nextCell);
				bestAngle = vmc.getBestAngle();
				// bestAngle = angle;
				intersections = this.getIntersection(currentRouteLocation, nextCell, bestAngle);
				nextDirection = intersections.lastElement().getDirection();
				// - Bug detected. If this two locations are the same we cannot continue.
				// - This is the case of entering a cell that will reflect us. We have to choose another
				// route and the only available is the worst route.
				// - Warning. Do not update the content of "curentRouteLocation" until this check is completed.
				if (currentRouteLocation.isEquivalent(intersections.lastElement().getLocation())) {
					bestAngle = vmc.getWorstAngle();
					// bestAngle = angle;
					intersections = this.getIntersection(currentRouteLocation, nextCell, bestAngle);
					nextDirection = intersections.lastElement().getDirection();
				}
				newCell = new RouteCell(nextCell, currentRouteLocation, intersections.lastElement().getLocation());
				currentRouteLocation = intersections.lastElement().getLocation();
				cellTTC = newCell.getTTC();
				if (this.checkIfArrivalTime(elapsed, cellTTC)) {
					// - Calculate the new end point by running the seconds left from the limit time.
					double seconds = this.timeLimit * 60.0 - ((elapsed) * 3600.0);
					GeoLocation endLocation = newCell.getEntryLocation().directEstimation(seconds, newCell.getSpeed(), bestAngle);
					newCell.setExitLocation(endLocation);
					this.add(newCell, nextDirection);

					// - We have reached the end time limit so the sub route is complete.
					return this;
				} else
					this.add(newCell, nextDirection);
				elapsed = this.getRouteTTC();
			}
		} catch (final LocationNotInMap lnime) {
			lnime.printStackTrace();
		}
		return this;
	}

	private Vector<Intersection> getIntersection(final GeoLocation startLocation, final WindCell startWindCell,
			final int direction) throws LocationNotInMap {
		// - Use the loxodromic calculations to get an end point.
		final GeoLocation endPoint = startLocation.directEstimation(70.0, direction);
		final Vector<Intersection> intersections = startWindCell.calculateIntersection(startLocation, endPoint);
		return intersections;
	}

	/**
	 * Tests if the route reaches the time elapsed scheduled to run. If this time is reached or exceeded then
	 * <code>true</code> is the value returned.
	 */
	protected boolean checkIfArrivalTime(final double elapsed, final double runTTC) {
		// - Convert times to minutes for comparison with limit
		int minutesRun = new Double(Math.round((elapsed + runTTC) * 60.0)).intValue();
		if (minutesRun > timeLimit) return true;
		return false;
	}

	public GeoLocation getRouteEnd() {
		try {
			RouteCell lastCell = this.route.lastElement();
			if (null != lastCell) return lastCell.getExitLocation();
		} catch (NoSuchElementException nsee) {
			return null;
		}
		return null;
	}
}

// - UNUSED CODE ............................................................................................
