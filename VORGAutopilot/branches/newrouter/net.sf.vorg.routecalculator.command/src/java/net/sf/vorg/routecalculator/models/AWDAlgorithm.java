//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class AWDAlgorithm extends AngleAlgorithm {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AWDAlgorithm(int angle) {
		super(angle);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public int getMovementAngle(int windDirection, double windSpeed) {
		// - Starboard AWD are positive and Port are negative.
		return GeoLocation.adjustAngleTo360(windDirection - angle);
	}
}

// - UNUSED CODE ............................................................................................
