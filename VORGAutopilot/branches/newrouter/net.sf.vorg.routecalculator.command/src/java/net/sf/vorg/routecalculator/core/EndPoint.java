//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.models.Boundaries;
import net.sf.vorg.core.models.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Represents the internal end points for a route leg inside a RouteCell. End points are Geo Time locations
 * that can also have other properties for identification or classification.
 */
public class EndPoint extends GeoTimeLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger		logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	// private final GeoTimeLocation pointLocation = null;
	private EndPointAspect	aspect		= EndPointAspect.INTERMEDIATE;
	private int							direction	= EndPointDirection.UNCLASSIFIED;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public EndPoint() {
	}

	public EndPoint(GeoTimeLocation location) {
		setLocation(location);
		setTime(location.getTime());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setAspect(EndPointAspect newAspect) {
		aspect = newAspect;
	}

	/**
	 * Detects if this point lies in a wind cell border or in a cell corner. By default any point is inside the
	 * parameter wind cell.
	 * 
	 * @return
	 */
	public int classify(WindCell targetWindCell) {
		// - Test the location of the point.
		Boundaries boundaries = targetWindCell.getBoundaries();
		int resultFlag = EndPointDirection.UNCLASSIFIED;
		if (Math.abs(getLat() - boundaries.getNorth()) < VORGConstants.MIN_POINT_DISTANCE) {
			resultFlag += EndPointDirection.N;
		}
		if (Math.abs(getLat() - boundaries.getSouth()) < VORGConstants.MIN_POINT_DISTANCE) {
			resultFlag += EndPointDirection.S;
		}
		if (Math.abs(getLon() - boundaries.getWest()) < VORGConstants.MIN_POINT_DISTANCE) {
			resultFlag += EndPointDirection.W;
		}
		if (Math.abs(getLon() - boundaries.getEast()) < VORGConstants.MIN_POINT_DISTANCE) {
			resultFlag += EndPointDirection.E;
		}
		if (resultFlag == EndPointDirection.UNCLASSIFIED) resultFlag = EndPointDirection.INTERNAL;
		direction = resultFlag;
		return direction;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[EndPoint ");
		buffer.append(super.toString()).append(",");
		buffer.append("aspect=").append(aspect.toString()).append(",");
		buffer.append("direction=").append(EndPointDirection.decode(direction)).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
