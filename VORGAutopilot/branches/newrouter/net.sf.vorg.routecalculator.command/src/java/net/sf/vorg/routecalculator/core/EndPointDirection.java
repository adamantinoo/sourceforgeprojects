//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

import net.sf.vorg.core.enums.Directions;

// - IMPORT SECTION .........................................................................................

//- CLASS IMPLEMENTATION ...................................................................................
public class EndPointDirection {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final int	UNCLASSIFIED	= 0x00;
	public static final int	N							= 0x01;
	public static final int	NE						= 0x03;
	public static final int	E							= 0x02;
	public static final int	SE						= 0x06;
	public static final int	S							= 0x04;
	public static final int	SW						= 0x0C;
	public static final int	W							= 0x08;
	public static final int	NW						= 0x09;
	// public static final int CENTER = 0x10;
	public static final int	INTERNAL			= 0x10;

	public static boolean detectCorner(int direction) {
		if (direction == NE) return true;
		if (direction == NW) return true;
		if (direction == SE) return true;
		if (direction == SW) return true;
		return false;
	}

	public static boolean detectBorder(int direction) {
		if (direction != INTERNAL) return true;
		return false;
	}

	public static String decode(int direction) {
		if (direction == UNCLASSIFIED) return "UNCLASSIFIED";
		if (direction == N) return "N";
		if (direction == NE) return "NE";
		if (direction == E) return "E";
		if (direction == SE) return "SE";
		if (direction == S) return "S";
		if (direction == SW) return "SW";
		if (direction == W) return "W";
		if (direction == NW) return "NW";
		if (direction == INTERNAL) return "INTERNAL";
		return "UNCLASSIFIED";
	}

	public static Directions transform(int direction) {
		if (direction == N) return Directions.N;
		if (direction == E) return Directions.E;
		if (direction == S) return Directions.S;
		if (direction == W) return Directions.W;
		if (direction == NE) return Directions.NE;
		if (direction == SE) return Directions.SE;
		if (direction == SW) return Directions.SW;
		if (direction == NW) return Directions.NW;
		return Directions.N;
	}
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
