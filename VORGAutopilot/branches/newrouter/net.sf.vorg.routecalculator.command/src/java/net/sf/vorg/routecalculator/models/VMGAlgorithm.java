//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.VMCData;

// - CLASS IMPLEMENTATION ...................................................................................
public class VMGAlgorithm extends AngleAlgorithm {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VMGAlgorithm(int angle, int minAWD, int maxAWD) {
		super(angle, minAWD, maxAWD);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public int getMovementAngle(int windDirection, double windSpeed) {
		VMCData vmc = new VMCData(angle, windDirection, windSpeed);
		int bestAngle = vmc.getBestAngle();
		return bestAngle;
	}
}

// - UNUSED CODE ............................................................................................
