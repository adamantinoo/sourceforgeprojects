//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class NamedGeoLocation extends GeoTimeLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private String				name		= "GEO";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NamedGeoLocation(GeoLocation location) {
		latitude = location.getLat();
		longitude = location.getLon();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[NamedGeoLocation ");
		buffer.append(getName()).append(VORGConstants.NEWLINE);
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
