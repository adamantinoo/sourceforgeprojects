//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;
import net.sf.vorg.routecalculator.models.AWDAlgorithm;
import net.sf.vorg.routecalculator.models.AngleAlgorithm;
import net.sf.vorg.routecalculator.models.VMGAlgorithm;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Defines properties to determinate the algorithm and the process to build a new Route. This class will grow
 * up with any new property that helps to create new types of routes.
 */
public class RouteEnvironment {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger		logger						= Logger.getLogger("net.sf.vorg.routecalculator.core");

	// - F I E L D - S E C T I O N ............................................................................
	private GeoTimeLocation	startLocation			= null;
	private int							angleParameter		= -1;
	private int							minAWD						= 30;
	private int							maxAWD						= 170;
	private GenerationLimit	generationLimit		= new GenerationLimit();
	private RouterType			algorithm					= RouterType.ANGLE;
	private RouterAlgorithm	algorithmInstance	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteEnvironment() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public RouterAlgorithm getAlgorithm() throws RoutingException {
		if (angleParameter == -1) throw new RoutingException("The environment is not initialized or is invalid.");
		if (null == algorithmInstance) {
			if (algorithm == RouterType.ANGLE) algorithmInstance = new AngleAlgorithm(angleParameter);
			if (algorithm == RouterType.AWD) algorithmInstance = new AWDAlgorithm(angleParameter);
			if (algorithm == RouterType.VMG) algorithmInstance = new VMGAlgorithm(angleParameter, minAWD, maxAWD);
		}
		return algorithmInstance;
	}

	public int getAngleParameter() throws RoutingException {
		if (angleParameter == -1) throw new RoutingException("The environment is not initialized or is invalid.");
		return angleParameter;
	}

	public GenerationLimit getGenerationLimit() {
		return generationLimit;
	}

	public int getMaxAWD() {
		return maxAWD;
	}

	public int getMinAWD() {
		return minAWD;
	}

	public GeoTimeLocation getStartLocation() throws RoutingException {
		if (null == startLocation) throw new RoutingException("The environment is not initialized or is invalid.");
		return startLocation;
	}

	public void setAlgorithmType(final RouterType algorithm) {
		this.algorithm = algorithm;
		algorithmInstance = null;
	}

	public RouterType getAlgorithmType() {
		return algorithm;
	}

	public void setAngleParameter(final int angleParameter) {
		this.angleParameter = GeoLocation.adjustAngleTo360(angleParameter);
		algorithmInstance = null;
	}

	public void setGenerationLimit(final GenerationLimit limit) {
		generationLimit = limit;
	}

	public void setMaxAWD(final int maxAWD) {
		this.maxAWD = maxAWD;
	}

	public void setMinAWD(final int minAWD) {
		this.minAWD = minAWD;
	}

	public void setStartLocation(final GeoTimeLocation startLocation) {
		this.startLocation = startLocation;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[RouteEnvironment ");
		buffer.append("start=").append(startLocation.toString()).append(VORGConstants.NEWLINE);
		buffer.append("                 ,").append("angle=").append(angleParameter).append(",");
		buffer.append("AWD=").append(minAWD).append("/").append(maxAWD).append(",");
		buffer.append("algorithm=").append(algorithm.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
