//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractRouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.core.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractRouteCell() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public abstract GeoLocation getEntryLocation();

	public abstract GeoLocation getExitLocation();

	public abstract WindCell getCell();

	public abstract double getTTC();

	public abstract void setEntryLocation(GeoLocation entryLocation);

	public abstract void setExitLocation(GeoLocation exitLocation);

	public abstract void setWindData(final WindCell windData);

	public abstract String printReport(final int waypointId);

	public abstract String printStartReport();

	@Override
	public abstract String toString();

	public abstract void optimize();

	public abstract String vrtoolReport(int index);
}

// - UNUSED CODE ............................................................................................
