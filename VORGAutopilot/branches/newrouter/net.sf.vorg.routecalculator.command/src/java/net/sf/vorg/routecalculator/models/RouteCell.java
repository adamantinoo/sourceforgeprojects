//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Quadrants;
import net.sf.vorg.core.enums.Sails;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.core.models.WindMapHandler;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.core.EndPoint;
import net.sf.vorg.routecalculator.core.EndPointDirection;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * A RouteCell defines the geographical part of a route that compresses a single wind cell. Contains the
 * reference points and the wind cell that will be used at the time the cell is crossed. And performs all the
 * mathematical calculations needed to time the route and manage the angles.<br>
 * This type of cell only controls the calculations when the route leg fits completely on a single wind cell.
 * If there is a wind change during the time used to cross the cell, it will be replaced at the ProxyCell by
 * another version for multi-wind cells.
 */
public class RouteCell /* extends AbstractRouteCell */{
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	/** The wind cell contained on this route cell. Contains the wind information and location. */
	private WindCell			cell;
	/** Geographical location when entering the cell. */
	private EndPoint			entryLocation;
	/** Geographical location when exiting the cell. */
	private EndPoint			exitLocation;
	/** Difference between the exit and entry latitudes. */
	private double				deltaLat;
	/** Difference between the exit and entry longitudes. */
	private double				deltaLon;
	/**
	 * Heading angle to run inside this cell. There is a single course line inside a single cell so the path is
	 * not optimized
	 */
	private int						alpha;
	private int						AWD;
	/**
	 * Time To Cross the cell. The elapsed time to move from the entry to the exit at the current speed. This
	 * time is expressed in hours.
	 */
	private double				ttc;
	/** The speed reached depending on the wind angle, wind direction and heading. */
	private double				speed;
	/** The recommended sail to reach the speed. */
	private Sails					sail;
	/** This field stores the loxodromic distance in miles from the entry location to the exit location. */
	private double				loxDistance;
	/**
	 * References to connections for the adjacent route controls. This is only used in route optimizations when
	 * the connection points can be moved.
	 */
	private RouteControl	leftControl;
	private RouteControl	rightControl;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/** Empty constructor needed to create empty cells before entry and exit point initializations. */
	public RouteCell() {
	}

	/**
	 * Load the cell data with the required wind information and the entry and exit locations. Update the time
	 * on the exit location based on the current cell calculations and evaluation.
	 */
	public RouteCell(final WindCell windCell, GeoTimeLocation entry, GeoTimeLocation exit) {
		cell = windCell;
		entryLocation = new EndPoint(entry);
		entryLocation.classify(cell);
		exitLocation = new EndPoint(exit);
		exitLocation.classify(cell);
		// - Perform the calculations for all the other cell parameters.
		calculateCell();
		exitLocation.addTTC(entry, getTTC());
	}

	/**
	 * With this constructor we can identify the right wind cell when the entry location point lies in the
	 * intersection of two or more wind cells and also use the internal intersection algorithm that is more
	 * robust and faster.<br>
	 * If the entryLocation is on a border or on a corner, then we have to identify the correct wind cell to
	 * select and this depends on the current movement direction.<br>
	 * The generation algorithm then will request the final wind cell to the Route cell before performing any
	 * further calculations.
	 * 
	 * @throws LocationNotInMap
	 *           is the wind location is not found on the wind map service.
	 */
	public RouteCell(GeoTimeLocation entry, int movementAngle) throws LocationNotInMap {
		// - Get the initial wind cell for this location. This may not be the end wind cell.
		WindCell actualWindCell = WindMapHandler.cellWithPoint(entry, entry.getWindTime().getTime());
		// - Classify this point from the point of view of the wind cell borders.
		entryLocation = new EndPoint(entry);
		int direction = entryLocation.classify(actualWindCell);

		// - Adjust to the correct wind cell before starting the calculations.
		if (EndPointDirection.detectCorner(direction)) {
			Quadrants quad = Quadrants.q4Angle(movementAngle);
			if (quad == Quadrants.QUADRANT_I)
				actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() + 0.1, entry.getLon() + 0.1),
						entry.getWindTime().getTime());
			if (quad == Quadrants.QUADRANT_II)
				actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() - 0.1, entry.getLon() + 0.1),
						entry.getWindTime().getTime());
			if (quad == Quadrants.QUADRANT_III)
				actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() - 0.1, entry.getLon() - 0.1),
						entry.getWindTime().getTime());
			if (quad == Quadrants.QUADRANT_IV)
				actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() + 0.1, entry.getLon() - 0.1),
						entry.getWindTime().getTime());
		} else if (EndPointDirection.detectBorder(direction)) {
			Quadrants quad = Quadrants.q4Angle(movementAngle);
			if ((direction == EndPointDirection.N) || (direction == EndPointDirection.S)) {
				if ((quad == Quadrants.QUADRANT_I) || (quad == Quadrants.QUADRANT_IV)) {
					actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() + 0.1, entry.getLon()), entry
							.getWindTime().getTime());
				}
				if ((quad == Quadrants.QUADRANT_II) || (quad == Quadrants.QUADRANT_III)) {
					actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat() - 0.1, entry.getLon()), entry
							.getWindTime().getTime());
				}
			}
			if ((direction == EndPointDirection.W) || (direction == EndPointDirection.E)) {
				if ((quad == Quadrants.QUADRANT_I) || (quad == Quadrants.QUADRANT_II)) {
					actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat(), entry.getLon() + 0.1), entry
							.getWindTime().getTime());
				}
				if ((quad == Quadrants.QUADRANT_III) || (quad == Quadrants.QUADRANT_IV)) {
					actualWindCell = WindMapHandler.cellWithPoint(new GeoLocation(entry.getLat(), entry.getLon() - 0.1), entry
							.getWindTime().getTime());
				}
			}
		}
		cell = actualWindCell;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// [01]
	public void calculateCell() {
		// - Calculate time to traverse this cell.
		// Delta movement is calculated in degrees and not used for distance calculations.
		deltaLat = exitLocation.getLat() - entryLocation.getLat();
		deltaLon = exitLocation.getLon() - entryLocation.getLon();
		// - Control the change over the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon = 360.0 - (Math.abs(deltaLon) * Math.signum(deltaLon));

		// - If destination is the same as the origin the TTC is ZERO.
		if ((deltaLat == 0.0) && (deltaLon == 0.0)) {
			alpha = 0;
			loxDistance = 0.0;
			ttc = 0.0;
			return;
		}
		loxDistance = entryLocation.distance(exitLocation);
		alpha = (int) Math.round(entryLocation.angleTo(exitLocation));
		AWD = GeoLocation.calculateAWD(cell.getWindDir(), alpha);

		// - Get the sail type and the speed from the Polars.
		final SailConfiguration configuration = Polars.lookup(AWD, cell.getWindSpeed());
		speed = configuration.getSpeed();
		sail = configuration.getSail();
		// - Calculate the time in hours to cross the cell
		if (0.0 == speed)
			ttc = Double.POSITIVE_INFINITY;
		else
			ttc = loxDistance / speed;
	}

	public int getAlpha() {
		return alpha;
	}

	public int getApparent() {
		return AWD;
	}

	public WindCell getCell() {
		return cell;
	}

	public double getDistance() {
		return loxDistance;
	}

	public GeoTimeLocation getEntryLocation() {
		return entryLocation;
	}

	public GeoTimeLocation getExitLocation() {
		return exitLocation;
	}

	public double getSpeed() {
		return speed;
	}

	/**
	 * Return the time to cross the cell from the start to end to the selected speed and polars expressed in
	 * hours.
	 */
	public double getTTC() {
		return ttc;
	}

	public int getWindDirection() {
		return cell.getWindDir();
	}

	public double getWindSpeed() {
		return cell.getWindSpeed();
	}

	public String printReport(final int waypointId) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("W").append(waypointId).append('\t');
		buffer.append(exitLocation.formatLatitude()).append("\t").append(exitLocation.formatLongitude()).append("\t");
		buffer.append(exitLocation.getLat()).append('\t');
		buffer.append(exitLocation.getLon()).append('\t');
		buffer.append(alpha).append('\t');
		buffer.append(speed).append('\t');
		buffer.append(sail).append('\t');
		buffer.append(deltaLat).append('\t');
		buffer.append(deltaLon).append('\t');
		buffer.append(loxDistance).append('\t');
		if (ttc == Double.POSITIVE_INFINITY)
			buffer.append(100).append('\t');
		else
			buffer.append(ttc).append('\t');
		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir()).append('\t');
		buffer.append(cell.getTimeStamp()).append('\t');

		buffer.append(FormatSingletons.nf2.format(loxDistance) + " miles").append('\t');
		SimpleDateFormat formatter1 = new SimpleDateFormat("HH'H' mm'M'");
		Calendar tmp = GregorianCalendar.getInstance();
		tmp.set(Calendar.HOUR_OF_DAY, 0);
		tmp.set(Calendar.MINUTE, 0);
		tmp.set(Calendar.SECOND, 0);
		tmp.add(Calendar.MINUTE, new Double(Math.round(getTTC() * VORGConstants.H2M)).intValue());
		buffer.append(formatter1.format(tmp.getTime())).append('\t');
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		buffer.append(formatter.format(exitLocation.getTime().getTime()));
		return buffer.toString();
	}

	public String printStartReport() {
		final StringBuffer buffer = new StringBuffer("START").append('\t');
		buffer.append(entryLocation.formatLatitude()).append("\t").append(entryLocation.formatLongitude()).append("\t");
		buffer.append(entryLocation.getLat()).append('\t');
		buffer.append(entryLocation.getLon()).append('\t');

		buffer.append("\t\t\t\t\t\t\t");

		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir()).append('\t');
		buffer.append(cell.getTimeStamp());
		buffer.append("\t\t\t");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		buffer.append(formatter.format(entryLocation.getTime().getTime()));

		return buffer.toString();
	}

	public void setEntryLocation(final GeoTimeLocation newEntryLocation) {
		entryLocation = new EndPoint(newEntryLocation);
		entryLocation.classify(getCell());
		calculateCell();
	}

	public void setExitLocation(final GeoTimeLocation newExitLocation) {
		exitLocation = new EndPoint(newExitLocation);
		exitLocation.classify(getCell());
		calculateCell();
		exitLocation.addTTC(entryLocation, getTTC());
	}

	public void setWindData(final WindCell windData) {
		cell = windData;
		calculateCell();
	}

	public int getNextDirection() {
		return exitLocation.classify(getCell());
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(VORGConstants.NEWLINE).append("[RouteCell ");
		buffer.append("cell=").append(cell).append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("alpha=").append(alpha).append(",");
		buffer.append("distance=").append(loxDistance).append(",");
		buffer.append("speed=").append(speed).append(",");
		buffer.append("ttc=").append(ttc).append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("entrylocation=").append(entryLocation)
				.append("");
		buffer.append(VORGConstants.NEWLINE).append("                ").append("exitlocation=").append(exitLocation)
				.append("");
		buffer.append("]");
		return buffer.toString();
	}

	public String vrtoolReport(int index) {
		final StringBuffer buffer = new StringBuffer("P; ");
		if (index == 0) {
			buffer.append(FormatSingletons.nf5.format(entryLocation.getLat())).append(";");
			buffer.append(FormatSingletons.nf5.format(entryLocation.getLon() * -1.0)).append(";");
			buffer.append("START").append(VORGConstants.NEWLINE);
		} else {
			buffer.append(FormatSingletons.nf5.format(exitLocation.getLat())).append(";");
			buffer.append(FormatSingletons.nf5.format(exitLocation.getLon() * -1.0)).append(";");
			buffer.append("W").append(index).append(VORGConstants.NEWLINE);
		}
		return buffer.toString();
	}

	public RouteControl getLeftControl() {
		return leftControl;
	}

	public RouteControl getRightControl() {
		return rightControl;
	}

	public void registerControlLeft(RouteControl control) {
		leftControl = control;
	}

	public void registerControlRight(RouteControl control) {
		rightControl = control;
	}
}
// - UNUSED CODE ............................................................................................
// [01]
// public int calcApparent() {
// int angle = Math.abs(cell.getWindDir() - alpha);
// if (angle > 180) angle = angle - 360;
// return Math.abs(angle);
// }

// public Vector<Intersection> calculateIntersection(WindCell windData, final int heading){
// final GeoLocation endPoint = this.entryLocation.directEstimation(120.0, heading);
// }

// @Override
// public void optimize() {
// }
