//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.singletons.FormatSingletons;
import net.sf.vorg.routecalculator.core.NamedGeoLocation;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class uses the RouteGenerator to create a set of routes based on the same Environment but calculated
 * for a spread of angles. The resulting set is stored with all the routes completed and the final points.
 * With those final points this generator will perform additional calculations to detect the best routes or to
 * shot result inside the graphical output.
 */
public class IsochroneRouter /* extends Router */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger							= Logger.getLogger("net.sf.vorg.routecalculator.models");
	private static final int	DEFAULT_SCAN_RANGE	= 60;

	public static Vector<IsoPoint> generateIsoStep(final RouteEnvironment newEnvironment) throws RoutingException {
		IsochroneRouter router = new IsochroneRouter(RouterType.NONE);
		return router.generateIsochrone(newEnvironment);
	}

	// - F I E L D - S E C T I O N ............................................................................
	private RouteEnvironment								environment;
	private int															initialAngle	= -1;
	private Vector<IsoPoint>								isochrone			= new Vector<IsoPoint>(1);
	private final Hashtable<Integer, Route>	routes				= new Hashtable<Integer, Route>();
	protected String												outputFile		= null;

	// private Route firstRoute;

	// protected double ortoAngle;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsochroneRouter(final RouterType type) {
		// super(type);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * With the start point and the final waypoint, this method calculates the routes for the different VMGs to
	 * both sides of the resulting angle. Those routes are divided on the wind change points and those points
	 * are recorded to remember the route that has advanced more in the general direction of the start-waypoint
	 * angle.
	 * 
	 * @return
	 * @throws RoutingException
	 */
	public Vector<IsoPoint> generateIsochrone(final RouteEnvironment newEnvironment) throws RoutingException {
		// - Initialize storage structures
		environment = newEnvironment;
		initialAngle = environment.getAngleParameter();
		isochrone = new Vector<IsoPoint>(50);
		// final double maxFirstDistance = 0.0;
		// firstRoute = null;

		// - Generate the routes using the configured Router and the RouteGenerator
		for (int deviation = -IsochroneRouter.DEFAULT_SCAN_RANGE; deviation <= IsochroneRouter.DEFAULT_SCAN_RANGE; deviation++)
			try {
				// - Generate the Route based on the parameters configured in the Environment
				final int course = new Double(initialAngle + deviation).intValue();
				// - Generate the VMG route if requested.
				environment.setAngleParameter(course);
				final Route targetRoute = RouteGenerator.generateRoute(environment);

				// - Get the first and second wind change locations
				final GeoLocation firstLocation = targetRoute.getEndLocation();
				if (null != firstLocation) {
					final NamedGeoLocation location = new NamedGeoLocation(firstLocation);
					location.setName("ISO STEP - " + course);
					isochrone.add(new IsoPoint(location, course, environment.getAlgorithmType()));
					routes.put(course, targetRoute);
				}
			} catch (final Exception e) {
				// - If the locations are not found, skip the route
				continue;
			}
		return isochrone;
	}

	// public Route getBestRoute() {
	// return firstRoute;
	// }

	public void sendDataVRT() {
		// - Send the collected data to the NAV file
		if (null != outputFile) {
			this.send2VRTool(isochrone, "ISO1 " + initialAngle);
			// if (null != firstRoute) this.send2VRTool(firstRoute, firstRoute.getName());
		}
	}

	public void setOutputFile(final String outputFile) {
		this.outputFile = outputFile;
	}

	// [01]

	protected void send2VRTool(final Vector<IsoPoint> isochrone, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			for (final IsoPoint point : isochrone) {
				buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLocation().getLat())).append(";");
				buffer.append(FormatSingletons.nf4.format(point.getLocation().getLon() * -1.0)).append(";");
				buffer.append(point.getName());
				buffer.append(VORGConstants.NEWLINE);
			}
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void send2VRTool(final Route awdRoute, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append(VORGConstants.NEWLINE);
			buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
			buffer.append("A;Color=$0000CCFF").append(VORGConstants.NEWLINE);
			buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
			buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
			buffer.append(awdRoute.vrtoolReport());
			buffer.append(VORGConstants.NEWLINE);
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Vector<IsoPoint> getIsochrone() {
		return isochrone;
	}

	public Route getRoute(int angle) {
		return routes.get(angle);
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// public String vrtoolReport(GeoLocation point) {
// final StringBuffer buffer = new StringBuffer("P; ");
// buffer.append(FormatSingletons.nf4.format(point.getLat())).append(";");
// buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
// return buffer.toString();
// }

// public void setOrtoAngle(final double angle) {
// ortoAngle = angle;
// }
// private void generateIsochroneData(final SpeedVector point, final double time, final String name) {
// final Vector<GeoLocation> isochrone = new Vector<GeoLocation>(50);
// // GeoLocation target = new GeoLocation();
// // - Iterate for 50 degrees, 25 each side of heading
// for (int deviation = -60; deviation <= 60; deviation++) {
// final int course = GeoLocation.adjustAngleTo360(point.getHeading() + deviation);
// // double maxSpeed = 0.0;
// try {
// final WindCell cell = WindMapHandler.getWindCell(point.getLocation());
// final SailConfiguration polar = Polars.lookup(GeoLocation.calculateAWD(cell.getWindDir(), course), cell
// .getWindSpeed());
// final GeoLocation newLocation = point.getLocation().directEstimation(time * 60.0, polar.getSpeed(),
// course);
// // if (polar.getSpeed() > maxSpeed) {
// // target = newLocation;
// // maxSpeed = polar.getSpeed();
// // }
// isochrone.add(newLocation);
// } catch (final LocationNotInMap e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
// // - Send the collected data to the NAV file
// if (null != outputFile) this.send2VRTool(isochrone, name);
// // return target;
// }
// private void send2VRTool(final Vector<GeoLocation> isochrone, final String title) {
// try {
// final BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));
//
// // - Compose the report.
// final StringBuffer buffer = new StringBuffer();
// buffer.append(VORGConstants.NEWLINE);
// buffer.append("O;Route;").append(title).append(VORGConstants.NEWLINE);
// buffer.append("A;Color=$0000CC99").append(VORGConstants.NEWLINE);
// buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
// buffer.append("A;Linewidth=1").append(VORGConstants.NEWLINE);
// buffer.append("A;ShowText=FALSE").append(VORGConstants.NEWLINE);
// buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
// for (final GeoLocation point : isochrone) {
// buffer.append("P; ").append(FormatSingletons.nf4.format(point.getLat())).append(";");
// buffer.append(FormatSingletons.nf4.format(point.getLon() * -1.0)).append(";");
// buffer.append(VORGConstants.NEWLINE);
// }
// writer.write(buffer.toString());
// writer.close();
// } catch (final IOException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }
