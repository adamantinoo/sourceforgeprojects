//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Limits;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;

//- CLASS IMPLEMENTATION ...................................................................................
public class RouteControl {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected GeoLocation	location	= new GeoLocation();
	protected Directions	direction	= Directions.NS;
	protected RouteCell		left;
	protected RouteCell		right;
	protected GeoLocation	upLocation;
	protected GeoLocation	downLocation;
	/** Stores the last optimized location to be used as the start point for next iterations. */
	private GeoLocation		optimizedLocation;
	private Limits				controlLimits;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * Creates a new control that connect two route cells, setting a direction and the initial control location.
	 */
	public RouteControl(final GeoLocation location, final Directions setDirection, final RouteCell leftNode,
			final RouteCell rightNode) {
		this.location = location;
		upLocation = location;
		downLocation = location;
		optimizedLocation = location;
		direction = setDirection;
		if (setDirection == Directions.N) direction = Directions.EW;
		if (setDirection == Directions.S) direction = Directions.EW;
		if (setDirection == Directions.E) direction = Directions.NS;
		if (setDirection == Directions.W) direction = Directions.NS;
		left = leftNode;
		right = rightNode;
		if (direction == Directions.NS) controlLimits = left.getCell().getCeiling();
		if (direction == Directions.EW) controlLimits = left.getCell().getWalls();

		// - Register the control inside the route nodes.
		leftNode.registerControlLeft(this);
		rightNode.registerControlRight(this);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Increments the downLocation for latitude or longitude depending on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustDown(final int controlId) {
		if (Directions.NS == direction) {
			// - Move the up location one step left.
			downLocation.setLat(downLocation.getLat() + Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (downLocation.getLat() > iterationLimits.getNorth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ downLocation.formattedLocation().replace('\t', ' '));
			storeLocation(downLocation);
			// this.updateLeft(downLocation);
			// this.updateRight(downLocation);
			// location = downLocation;

			final double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			downLocation.setLon(downLocation.getLon() + Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (downLocation.getLon() > iterationLimits.getEast()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ downLocation.formattedLocation().replace('\t', ' '));
			storeLocation(downLocation);
			// this.updateLeft(downLocation);
			// this.updateRight(downLocation);
			// location = downLocation;

			final double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustDown(controlId);
			return true;
		}
		return false;
	}

	/**
	 * Decrements the upLocation for latitude or longitude depending on the control type until it reaches the
	 * cell limit. It this limit is reached the method returns <code>false</code> to avoid being called again.
	 */
	public boolean adjustUp(final int controlId) {
		if (Directions.NS == direction) {
			// FIXME Move the up location one step up.
			upLocation.setLat(upLocation.getLat() - Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (upLocation.getLat() < iterationLimits.getSouth()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ upLocation.formattedLocation().replace('\t', ' '));
			storeLocation(upLocation);
			// this.updateLeft(upLocation);
			// this.updateRight(upLocation);
			// location = upLocation;

			final double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		if (Directions.EW == direction) {
			// - Move the up location one step left.
			upLocation.setLon(upLocation.getLon() - Route.ITERATION_INCREMENT);
			final Limits iterationLimits = getLimits();
			if (upLocation.getLon() < iterationLimits.getWest()) return false;
			if (controlId < 1)
				System.out.println("Iterating level " + controlId + " with "
						+ upLocation.formattedLocation().replace('\t', ' '));
			storeLocation(upLocation);
			// this.updateLeft(upLocation);
			// this.updateRight(upLocation);
			// location = upLocation;

			final double leftTTC = getLeftTTC();
			if (Double.POSITIVE_INFINITY == leftTTC) return adjustUp(controlId);
			return true;
		}
		return false;
	}

	public GeoLocation getCurrentLocation() {
		return location;
	}

	public Directions getDirection() {
		return direction;
	}

	// public double getFixedLat() {
	// return left.getExitLocation().getLat();
	// }
	public double getFixedLon() {
		return left.getExitLocation().getLon();
	}

	public double getLeftTTC() {
		return left.getTTC();
	}

	public Limits getLimits() {
		return controlLimits;
	}

	// public GeoLocation getOptimizedLocation() {
	// return optimizedLocation;
	// }

	/** Sets the start point for the up and down locations to the rounded value of the optimized location. */
	public void reset() {
		upLocation = new GeoLocation(Math.round(optimizedLocation.getLat() * 100.0) / 100.0, Math.round(optimizedLocation
				.getLon() * 100.0) / 100.0);
		downLocation = new GeoLocation(Math.round(optimizedLocation.getLat() * 100.0) / 100.0, Math.round(optimizedLocation
				.getLon() * 100.0) / 100.0);
	}

	public void setLeft(final RouteCell newLeft) {
		left = newLeft;
		left.registerControlLeft(this);
	}

	public void setOptimizedLocation(final GeoLocation newLocation) {
		optimizedLocation = newLocation;
	}

	public void setRight(final RouteCell newRigth) {
		right = newRigth;
		right.registerControlRight(this);
	}

	public void storeLocation(final GeoLocation newLocation) {
		updateLeft(newLocation);
		updateRight(newLocation);
		location = newLocation;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[RouteControl ");
		buffer.append("direction=").append(direction).append(", ");
		buffer.append("setup location=").append(location).append("");
		buffer.append("\n              ").append("optimizedLocation=").append(optimizedLocation).append("]");
		return buffer.toString();
	}

	private double updateLeft(final GeoLocation newLocation) {
		left.setExitLocation(new GeoTimeLocation(newLocation));
		return left.getTTC();
	}

	private double updateRight(final GeoLocation newLocation) {
		right.setEntryLocation(new GeoTimeLocation(newLocation));
		return right.getTTC();
	}
}
// - UNUSED CODE ............................................................................................
