//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.util.TimeZoneAdapter;
import net.sf.vorg.routecalculator.core.EndPoint;
import net.sf.vorg.routecalculator.core.EndPointAspect;
import net.sf.vorg.routecalculator.core.EndPointDirection;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * Una ruta en un conjunto de una o mas subrutas o de simples celdas. La ruta minima estara entonces compuesta
 * por una unica celda, mientras que puede haber rutas compuestas por subrutas de multiples celdas cada una.<br>
 * A route is a set of legs that are interconnected. Routes can be constructed by a Route Generator of by the
 * connection of free waypoints that will define each of the legs. A Route may be part of another route and in
 * those cases it is considered a subroute.
 */
public class Route {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger							= Logger.getLogger("net.sf.vorg.routecalculator.models");
	private static final String	HEADER							= "Name\tLat\tLon\tLat Deg\tLonDeg\tAlpha\tSpeed\tSail\tDiff Lat\tDiff Lon\tDistance\tTTC\tW Speed\tW Dir\tWind Time\tDistance\tElapsed time\tETA\tVRTool Data\tAWD";
	protected static double			ITERATION_INCREMENT	= 0.01;
	private static int					defaultIterations		= 100;

	public static void setDeepLevel(final int level) {
		Route.defaultIterations = level;
	}

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * Unique identifier that allows to give a semantic reference to the Route. Usually is composed with some
	 * keys during the construction of the Route.
	 */
	private String											name									= "ROUTE";
	/**
	 * Vector with the cells that define the route. Any cell is the piece of the route that is inside a game
	 * wind cell unit.
	 */
	protected Vector<RouteCell>					route									= new Vector<RouteCell>();
	/**
	 * The list of controls that connect route cells and that can be moved to adjust the route to the better
	 * wind conditions.
	 */
	protected Vector<RouteControl>			controls							= new Vector<RouteControl>();
	protected RouteState								creationState					= RouteState.EMPTY;
	protected RouteCell									lastNode							= null;
	/**
	 * These are the controls that are set inside the cells and that are located at the points where the wind
	 * change affect the route.
	 */
	private final Vector<RouteControl>	windChangeReferences	= new Vector<RouteControl>();
	// /**
	// * Represents the time when this route starts to define. Used to locate the right wind cells depending on
	// * the time elapsed during the run over the route.
	// */
	// protected Calendar startRouteTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(),
	// "Etc/GMT+1");
	/**
	 * The initial start point of the Route. If this point connects to another route then it should be
	 * equivalent to the EndPoint on that Route.
	 */
	private EndPoint										startPoint						= null;
	/**
	 * The end point of the Route. If this point connects to another route then it should be equivalent to the
	 * EndPoint on that Route.
	 */
	private EndPoint										endPoint							= null;
	/**
	 * This stores the point in time that represents the start point of the route. It can be the route creation
	 * time (the default) or any other time selected by the user or calculated by the generator. This time is in
	 * Game Time Coordinates because all wind cells are related to that time coordinate system
	 */
	private Calendar										startTime							= TimeZoneAdapter.changeTimeZone(Calendar.getInstance(),
																																"Etc/GMT+1");
	/** If this Route is not a simple route this this vector contains the pieces or legs of the composing route. */
	private final Vector<Route>					legs									= new Vector<Route>();

	// - OPTIMIZATION
	/**
	 * This is the list of connections points that generate an optimized route. This vector contains the control
	 * points for a found optimization.
	 */
	protected Vector<GeoLocation>				savedConfiguration		= new Vector<GeoLocation>();
	private long												iterationCounter			= 0;
	private long												lastIteration					= 0;
	private double											bestTime							= Double.POSITIVE_INFINITY;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Route() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Add a new route leg to the end of the route and sets the direction of the border that connects the two
	 * cells. This direction can be one of the four sides of the cell, N, E, S or W.
	 * 
	 * @throws RoutingException
	 */
	public boolean add(final RouteCell newCell, final Directions direction) throws LocationNotInMap, RoutingException {
		// - Bug detected. If this two locations are the same we cannot continue.
		// - This is the case of entering a cell that will reflect us.
		if (newCell.getEntryLocation().isEquivalent(newCell.getExitLocation())) {
			Route.logger.severe("Reflection point. We can not enter the cell because we are reflected. End of route.");
			throw new RoutingException("Reflection point. We can not enter the cell because we are reflected. End of route.");
		}

		// - Detect if there is a wind change inside this leg. If so divide the leg.
		if (this.checkIfWindChange(newCell.getEntryLocation().getTime(), newCell.getExitLocation().getTime())) {
			Route.logger.fine("Detected wind change while adding cell: " + newCell.toString());
			// - Calculate an intermediate initial point.
			final GeoTimeLocation splitPoint = new GeoTimeLocation(calculateSplitPoint(newCell.getEntryLocation(), newCell
					.getExitLocation(), nextElevenDifference(newCell.getEntryLocation().getTime()), newCell.getSpeed()));
			Route.logger.fine("New split point at: " + splitPoint.toString());
			final RouteCell newChangeCell = new RouteCell(newCell.getCell(), newCell.getEntryLocation(), splitPoint);
			Route.logger.finer("Current cell TTC: " + newChangeCell.getTTC());
			Route.logger.finer("Current cell End Time Point: "
					+ newChangeCell.getExitLocation().getTime().getTime().toString());
			append(newChangeCell, direction);
			return true;
		}
		append(newCell, direction);
		return false;
	}

	public boolean add(final RouteCell newCell, int direction) throws LocationNotInMap, RoutingException {
		return add(newCell, EndPointDirection.transform(direction));
	}

	// /**
	// * Adds a new wind cell to the Route. This procedure checks for the formation of the Route and determines
	// if
	// * this is a start cell or a middle cell and also connects the controls.
	// *
	// * @param direction
	// * @throws LocationNotInMap
	// * @throws RoutingException
	// */
	// public void add(final WindCell routeCell, final Directions direction, final GeoTimeLocation entry, final
	// GeoTimeLocation exit)
	// throws LocationNotInMap, RoutingException {
	// // - If the start and end locations are the same we can skip this cell.
	// if (entry.isEquivalent(exit)) {
	// Route.logger.severe("Reflection point. We can not enter the cell because we are reflected. End of route.");
	// throw new
	// RoutingException("Reflection point. We can not enter the cell because we are reflected. End of route.");
	// }
	// final RouteCell newCell = new RouteCell(routeCell, entry, exit);
	// append(newCell, direction);
	// }

	// /**
	// * Update the wind data from the maps but by evaluating each cell each time to detect wind shifts and
	// * recalculate all cell data.
	// *
	// * @throws LocationNotInMap
	// */
	// @Deprecated
	// public void adjustWindChanges() throws LocationNotInMap {
	// final Vector<RouteCell> newRouteList = new Vector<RouteCell>();
	// double elapsed = 0.0;
	// final Iterator<RouteCell> rit = route.iterator();
	// while (rit.hasNext()) {
	// final RouteCell routeCell = rit.next();
	// final double cellTTC = routeCell.getTTC();
	// if (this.checkIfWindChange(elapsed, cellTTC)) {
	// // - Check if this cell is already a
	// // - New algorithm updates the list of cells and controls on the route.
	// final WindChangeCell leg = new WindChangeCell(routeCell, elapsed);
	//
	// final RouteControl currentControl = routeCell.getRightControl();
	// final RouteControl nextControl = routeCell.getLeftControl();
	// // - Connect the new leg in the position and reconnect the controls
	// if (null == currentControl) {
	// // - This is the first cell so does not have right control.
	// nextControl.setLeft(leg.getRight());
	// controls.insertElementAt(leg.getControl(), 0);
	// } else if (null == nextControl) {
	// currentControl.setRight(leg.getLeft());
	// controls.add(leg.getControl());
	// } else {
	// currentControl.setRight(leg.getLeft());
	// controls.insertElementAt(leg.getControl(), controls.indexOf(currentControl) + 1);
	// nextControl.setLeft(leg.getRight());
	// }
	// // - Add both cells to the new list.
	// newRouteList.add(leg.getLeft());
	// newRouteList.add(leg.getRight());
	// if (leg.getLeft().getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
	// elapsed += leg.getLeft().getTTC();
	// if (leg.getRight().getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
	// elapsed += leg.getRight().getTTC();
	// } else {
	// final WindCell windData = WindMapHandler.cellWithPoint(routeCell.getCell().getLocation(),
	// newSearchTime(elapsed).getTime());
	// routeCell.setWindData(windData);
	// newRouteList.add(routeCell);
	// if (routeCell.getTTC() == Double.POSITIVE_INFINITY) elapsed += 100.0;
	// elapsed += routeCell.getTTC();
	// }
	// }
	// route = newRouteList;
	// }

	/**
	 * Generates a new partial route from this route contents but only to reach the selected wind change point.
	 * The wind change counter starts with 1 for the first wind change, 2 for the second...
	 */
	@Deprecated
	public Route extractSubRoute(final int windChangeCounter) {
		int counter = 0;
		final Route subRoute = new Route();
		final Iterator<RouteCell> rit = route.iterator();
		final Iterator<RouteControl> cit = controls.iterator();
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			final RouteControl control = cit.next();
			try {
				subRoute.add(routeCell, control.getDirection());

				// - Detect the wind change
				if (control.equals(windChangeReferences.get(counter))) {
					counter++;
					if (counter >= windChangeCounter) break;
				}
			} catch (final LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (final RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return subRoute;
	}

	public Calendar getCreationTime() {
		return (Calendar) startTime.clone();
	}

	public Calendar getEndTime() {
		final Calendar start = getCreationTime();
		start.add(Calendar.MINUTE, new Double(getRouteTTC() * VORGConstants.H2M).intValue());
		return start;
	}

	public RouteCell getFirsCell() {
		if (route.size() > 0)
			return route.firstElement();
		else
			return null;
	}

	public String getName() {
		return name;
	}

	public double getRouteTTC() {
		final Iterator<RouteCell> rit = route.iterator();
		double elapsed = 0.0;
		while (rit.hasNext())
			elapsed += rit.next().getTTC();
		return elapsed;
	}

	public Calendar getSearchTime() {
		final Calendar now = (Calendar) startTime.clone();
		now.add(Calendar.HOUR, -2); // Adjust for the two hour difference with wind cell stamp time
		final int elapsed = new Double(getRouteTTC() * VORGConstants.TOMINUTES).intValue();
		now.add(Calendar.MINUTE, elapsed);
		return now;
	}

	public GeoLocation getWindChange(final int position) {
		try {
			if (position < windChangeReferences.size())
				if (null != windChangeReferences.get(position - 1))
					return windChangeReferences.get(position - 1).getCurrentLocation();
			if (null != windChangeReferences.firstElement()) return windChangeReferences.firstElement().getCurrentLocation();
			return new GeoLocation();
		} catch (final NoSuchElementException nsee) {
			return null;
		}
	}

	/**
	 * Starts the process to calculate a new optimum route based on the current route setup. New route
	 * calculation will be performed by exploring the values possible for the route controls.
	 */
	public void optimizeRoute() {
		// - Start optimization with the initial control and the recursively.
		optimizeControl(0);

		// - Update the route with the stored values of the best route.
		for (int index = 0; index < controls.size(); index++) {
			final RouteControl control = controls.get(index);
			control.storeLocation(savedConfiguration.get(index));
		}
	}

	public String printReport() {
		// - Before printing the route data
		final StringBuffer buffer = new StringBuffer();
		buffer.append(name).append(" - [");
		buffer.append(startTime.get(Calendar.DAY_OF_MONTH)).append("/");
		buffer.append(startTime.get(Calendar.MONTH) + 1).append(" ");
		buffer.append(startTime.get(Calendar.HOUR_OF_DAY)).append(":");
		buffer.append(startTime.get(Calendar.MINUTE)).append("]");
		buffer.append('\n');
		buffer.append(Route.HEADER).append('\n');
		final Iterator<RouteCell> rit = route.iterator();
		int routeCounter = 1;
		final RouteCell startNode = route.firstElement();
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			// - Check if this cell is the start of the route.
			if (startNode.equals(routeCell)) buffer.append(routeCell.printStartReport()).append(VORGConstants.NEWLINE);
			buffer.append(routeCell.printReport(routeCounter++)).append(VORGConstants.NEWLINE);
		}
		return buffer.toString();
	}

	public void setName(final String newName) {
		name = newName;
	}

	public void setStartPoint(final GeoTimeLocation startLocation) {
		startPoint = new EndPoint(startLocation);
		startPoint.setAspect(EndPointAspect.START);
		// - Update and synch the start time.
		startTime = startPoint.getTime();
	}

	public void setWindChange() {
		windChangeReferences.add(controls.lastElement());
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(getName()).append(" - [").append(startTime.getTime().toString()).append("]").append(
				VORGConstants.NEWLINE);
		buffer.append(route.toString()).append("");
		buffer.append(VORGConstants.NEWLINE);
		buffer.append("").append(controls.toString()).append("");
		return buffer.toString();
	}

	public String vrtoolReport() {
		final StringBuffer buffer = new StringBuffer();
		// buffer.append(VORGConstants.NEWLINE);
		// buffer.append("O;Route;").append(name).append(VORGConstants.NEWLINE);
		buffer.append("A;Color=$0000CCFF").append(VORGConstants.NEWLINE);
		buffer.append("A;Visible=TRUE").append(VORGConstants.NEWLINE);
		buffer.append("A;Linewidth=2").append(VORGConstants.NEWLINE);
		buffer.append("A;ShowText=TRUE").append(VORGConstants.NEWLINE);
		buffer.append("A;ShowBoat=FALSE").append(VORGConstants.NEWLINE);
		final Iterator<RouteCell> rit = route.iterator();
		int routeCounter = 1;
		final RouteCell startNode = route.firstElement();
		while (rit.hasNext()) {
			final RouteCell routeCell = rit.next();
			// - Check if this cell is the start of the route.
			if (startNode.equals(routeCell)) buffer.append(routeCell.vrtoolReport(0));
			buffer.append(routeCell.vrtoolReport(routeCounter++));
		}
		// - Use that last control to get the last cell data.
		return buffer.toString();
	}

	/**
	 * Tests if this cell lies inside a wind change area. If the wind change time happens during the run on this
	 * cell the result of this method is <code>true</code>
	 * 
	 * @param exitTime
	 */
	protected boolean checkIfWindChange(final Calendar enterTime, final Calendar exitTime) {
		// - Test if this time is after the 11 hour or the 23 hours and enter time before.
		// Calendar testTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		// testTime.setTime(enterTime);
		final int enterHour = enterTime.get(Calendar.HOUR_OF_DAY);
		// testTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		// testTime.setTime(exitTime);
		final int exitHour = exitTime.get(Calendar.HOUR_OF_DAY);

		if ((enterHour < 11) && (exitHour >= 11)) return true;
		if ((enterHour < 23) && (exitHour >= 23)) return true;
		if ((enterHour < 23) && (exitHour < enterHour)) return true;
		return false;
	}

	/**
	 * Tests if this cell lies inside a wind change area. If the wind change time happens during the run on this
	 * cell the result of this method is <code>true</code>
	 */
	protected boolean checkIfWindChange(final double elapsed, final double runTTC) {
		// final Calendar searchTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		final Calendar searchTime = Calendar.getInstance();
		searchTime.add(Calendar.MINUTE, new Double(elapsed * VORGConstants.TOMINUTES).intValue());
		final int elapsedHour = searchTime.get(Calendar.HOUR_OF_DAY);
		final int elapsedMinute = searchTime.get(Calendar.MINUTE);

		// elapsedDate = WindMapHandler.addElapsed(elapsedDate, elapsed);
		final double endHour = elapsedHour + elapsedMinute / 60.0 + runTTC;
		if (elapsedHour < 11) if (endHour > 11) return true;
		if (elapsedHour < 23) if (endHour > 23) return true;
		if (elapsedHour == 23) if (endHour > 35) return true;
		// else if (endHour > 23) return true;
		return false;
	}

	protected Calendar newSearchTime(final double elapsed) {
		final Calendar now = Calendar.getInstance();
		now.add(Calendar.HOUR, -2); // Adjust for the two hour difference with wind cell stamp time
		final int elapsedMinutes = new Double(elapsed * VORGConstants.TOMINUTES).intValue();
		now.add(Calendar.MINUTE, elapsedMinutes);
		return now;
	}

	/**
	 * Add a new route cell to the end of the route and sets the direction of the border that connects the two
	 * cells. This direction can be one of the four sides of the cell, N, E, S or W.
	 */
	private void append(final RouteCell newCell, final Directions direction) throws LocationNotInMap {
		// - Check for the start formation of the route.
		if (creationState == RouteState.EMPTY) {
			route.add(newCell);
			lastNode = newCell;
			creationState = RouteState.MIDDLE;
			return;
		}
		if (creationState == RouteState.MIDDLE) {
			route.add(newCell);
			final RouteControl theControl = new RouteControl(newCell.getEntryLocation(), direction, lastNode, newCell);
			controls.add(theControl);
			lastNode = newCell;
			return;
		}
	}

	/**
	 * Calculate the time point for route split on a wind change. The angle is the angle between the start and
	 * end. The distance to run depends on the speed and the time to elapse from the entry location time and the
	 * next wind change time.
	 */
	private GeoLocation calculateSplitPoint(final GeoLocation start, final GeoLocation end, final double minutes,
			final double speed) {
		// - Test to find a conversion factor between loxodromic and trigonometric distances.
		final double deltaLat = end.getLat() - start.getLat();
		double deltaLon = end.getLon() - start.getLon();

		// - Correct the difference if the points are at different sides of the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon -= 360.0 * Math.signum(deltaLon);
		final double Ls = Math.toRadians(start.getLat());
		final double Ld = Math.toRadians(end.getLat());
		final double ldelta = Math.toRadians(deltaLon);
		final double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		final double TTC = loxDistance / speed;
		// final double t11 = minutes / VORGConstants.H2M;
		final double t12 = minutes / VORGConstants.H2S;
		final double targetLat = t12 * deltaLat / TTC;
		final double targetLon = t12 * deltaLon / TTC;
		final GeoLocation splitLocation = new GeoLocation(start.getLat() + targetLat, start.getLon() + targetLon);

		return splitLocation;
	}

	private void evaluateRoute(final int controlId) {
		final double currentElapsed = getRouteTTC();

		// - Check if this is a better time.
		if (currentElapsed < bestTime) {
			bestTime = currentElapsed;
			// if (onDebug())
			// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
			saveControlConfiguration();
		}
	}

	private RouteControl getControl(final int controlId) {
		return controls.elementAt(controlId);
	}

	private boolean lastControl(final int controlId) {
		if (controlId + 1 == controls.size())
			return true;
		else
			return false;
	}

	private long levelIterations(final int controlId) {
		if (controlId == 0) return Route.defaultIterations;
		if (controlId < 4) return Route.defaultIterations / 10;
		if (controlId < 8) return Route.defaultIterations / 20;
		return 3;
	}

	private double nextElevenDifference(final Calendar entryDate) {
		final int entryHours = entryDate.get(Calendar.HOUR_OF_DAY);
		final Calendar elevenTime = Calendar.getInstance();
		// final Calendar elevenTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		if (entryHours < 11)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 11, 0,
					0);
		else if (entryHours < 23)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 23, 0,
					0);
		else
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE) + 1, 11,
					0, 0);
		final long diff = elevenTime.getTimeInMillis() - entryDate.getTimeInMillis();
		// - Convert to minutes.
		// - Convert to seconds
		// final double mins = diff / (60 * 1000);
		final double mins = diff / 1000;
		return mins;
	}

	/**
	 * Main recursive calculation method. This release divides the scan in two sections and then applies
	 * different optimization procedures depending on the deep level of the control.
	 * 
	 * @param elapsed
	 */
	private void optimizeControl(final int controlId) {
		optimizerLevel2(controlId);
	}

	/**
	 * This optimizer trims the number of possible iterations based on the control deep level. For initial
	 * levels it scans the 50 per cent of the point while for higher levels this numer is reduced.
	 * 
	 * @param elapsed
	 */
	private void optimizerLevel2(final int controlId) {
		final RouteControl control = getControl(controlId);
		control.reset();

		final long iterations = levelIterations(controlId);
		boolean doUp = true;
		boolean doDown = true;
		for (int counter = 0; counter < iterations; counter++) {
			iterationCounter++;
			if (doUp) {
				doUp = control.adjustUp(controlId);
				if (doUp) if (lastControl(controlId))
					evaluateRoute(controlId);
				else
					optimizeControl(controlId + 1);
			}
			iterationCounter++;
			if (doDown) {
				doDown = control.adjustDown(controlId);
				if (doDown) if (lastControl(controlId))
					evaluateRoute(controlId);
				else
					optimizeControl(controlId + 1);
			}
		}
	}

	private void saveControlConfiguration() {
		savedConfiguration = new Vector<GeoLocation>(controls.size());
		for (int index = 0; index < controls.size(); index++) {
			final RouteControl targetControl = controls.get(index);
			savedConfiguration.add(index, targetControl.getCurrentLocation());
			targetControl.setOptimizedLocation(targetControl.getCurrentLocation());
		}
		lastIteration = iterationCounter;
	}

	public GeoTimeLocation getEndLocation() {
		if (null == endPoint) {
			endPoint = new EndPoint(getLastLeg().getExitLocation());
		}
		return new GeoTimeLocation(endPoint.getLat(), endPoint.getLon(), endPoint.getTime());
	}

	private RouteCell getLastLeg() {
		// - Locate the last cell in the last leg.
		if (legs.size() > 0) {
			return legs.lastElement().getLastLeg();
		} else if (route.size() > 0) return route.lastElement();
		return null;
	}

	public int getStepCount() {
		return route.size();
	}

	public GeoTimeLocation getStartLocation() {
		return new GeoTimeLocation(startPoint.getLat(), startPoint.getLon(), startPoint.getTime());
	}
}

enum RouteState {
	EMPTY, MIDDLE, CLONED;
}
// - UNUSED CODE ............................................................................................
