//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface RouterAlgorithm {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public int getMovementAngle(int windDirection, double windSpeed);
}

// - UNUSED CODE ............................................................................................
