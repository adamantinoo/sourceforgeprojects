//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.GenerationLimit;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.Orthodromic;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;

// - CLASS IMPLEMENTATION ...................................................................................
public class IsochroneOptimizer /* extends IsochroneRouter */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	public static Route optimizeIsochrone(IsochroneRouter router, GeoLocation dest) {
		Vector<IsoPoint> isoWorkset = router.getIsochrone();
		// - With the result of this isochrone classify the end points in minute size boxes.
		Iterator<IsoPoint> iit = isoWorkset.iterator();
		double betterDistance = Double.POSITIVE_INFINITY;
		IsoPoint winnerPoint = null;
		while (iit.hasNext()) {
			IsoPoint targetPoint = iit.next();
			// classify(targetPoint);
			// - Project each point to the orthodromic route
			double destDistance = dest.distance(targetPoint.getLocation());
			if (destDistance < betterDistance) {
				betterDistance = destDistance;
				winnerPoint = targetPoint;
			}
		}
		return router.getRoute(winnerPoint.getAngle());
	}

	// - F I E L D - S E C T I O N ............................................................................
	private Vector<IsoPoint>																					isochrone				= new Vector<IsoPoint>(1);
	private GeoTimeLocation																						origin;
	private GeoTimeLocation																						destination;
	private Orthodromic																								orthodromic;
	// private Vector<NamedGeoLocation> iso;
	private Hashtable<Integer, Hashtable<Integer, Vector<IsoPoint>>>	registry				= new Hashtable<Integer, Hashtable<Integer, Vector<IsoPoint>>>();
	private final String																							navigationFile	= "L:\\VORG\\RouterTesting.nav";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsochroneOptimizer() {
	}

	public IsochroneOptimizer(Vector<IsoPoint> targetIsochrone) {
		isochrone = targetIsochrone;
	}

	public static Route optimizeIsochroneStep(final GeoTimeLocation start, final GeoTimeLocation end,
			GenerationLimit limit) {
		IsochroneOptimizer optim = new IsochroneOptimizer();
		return optim.optimizeIsochrone(start, end, limit);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double getInitialAngle() {
		if (null != orthodromic)
			return orthodromic.getInitialCourse();
		else
			return 0.0;
	}

	public Route optimizeIsochrone(final GeoTimeLocation start, final GeoTimeLocation end, GenerationLimit limit) {
		origin = start;
		destination = end;
		registry = new Hashtable<Integer, Hashtable<Integer, Vector<IsoPoint>>>();

		// - Calculate the orthodromic between this two points as the baseline where to project the route
		orthodromic = new Orthodromic(start, end);

		// - Calculate the set of isochrones for the start angle of this orthodromic.
		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(origin);
		environment.setAlgorithmType(RouterType.VMG);
		environment.setAngleParameter(new Double(Math.round(orthodromic.getInitialCourse())).intValue());
		if (null != limit) environment.setGenerationLimit(limit);

		final IsochroneRouter baseRouter = new IsochroneRouter(RouterType.VMG);
		try {
			isochrone = baseRouter.generateIsochrone(environment);
			// - With the result of this isochrone classify the end points in minute size boxes.
			Iterator<IsoPoint> iit = isochrone.iterator();
			double betterDistance = Double.POSITIVE_INFINITY;
			IsoPoint winnerPoint = null;
			while (iit.hasNext()) {
				IsoPoint targetPoint = iit.next();
				classify(targetPoint);
				// - Project each point to the orthodromic route
				double destDistance = destination.distance(targetPoint.getLocation());
				if (destDistance < betterDistance) {
					betterDistance = destDistance;
					winnerPoint = targetPoint;
				}
			}

			// - Return the winner as the target for this method.
			if (null != winnerPoint) return baseRouter.getRoute(winnerPoint.getAngle());
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Route winnerRoute(final GeoTimeLocation start, final GeoTimeLocation end) {
		Route winnerRoute = new Route();
		try {
			// - Create the calculation limit with the distance to the destination
			int cellCount = new Double(Math.hypot(end.getLat() - start.getLat(), end.getLon() - start.getLat())).intValue();
			GenerationLimit limit = new GenerationLimit();
			// limit.setWindChangeLimit(2);
			// - Get the first point and the initial winner route.
			Route targetRoute = optimizeIsochrone(start, end, null);

			while (targetRoute.getStepCount() > 1) {
				RouteCell firstCell = targetRoute.getFirsCell();
				int direction = firstCell.getNextDirection();
				winnerRoute.add(firstCell, direction);
				// limit.setCellLimit(cellCount - winnerRoute.getStepCount());
				targetRoute = optimizeIsochrone(firstCell.getExitLocation(), end, null);
			}
			RouteCell firstCell = targetRoute.getFirsCell();
			int direction = firstCell.getNextDirection();
			winnerRoute.add(firstCell, direction);
			return winnerRoute;
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Route isoStepOptimizer(final GeoTimeLocation start, final GeoTimeLocation end, int minutes) {
		Route optimRoute = new Route();
		optimRoute.setStartPoint(start);
		double targetAngle = start.angleTo(end);
		try {
			// // - Calculate time up to the next hour boundary.
			// Calendar now = start.getTime();
			// Calendar nextTime = (Calendar) now.clone();
			// nextTime.add(Calendar.HOUR, 1);
			// nextTime.set(Calendar.MINUTE, 0);
			// nextTime.set(Calendar.SECOND, 0);
			// nextTime.set(Calendar.MILLISECOND, 0);
			// int secondsToMove = (int) ((nextTime.getTimeInMillis() - now.getTimeInMillis()) / 1000);

			// - Get the first isochrone step for this number of seconds.
			RouteEnvironment environment = new RouteEnvironment();

			// - Set the start time to the next wind change less the number of minutes of step.
			Calendar targetTime = GregorianCalendar.getInstance();
			if (targetTime.get(Calendar.HOUR_OF_DAY) < 11) {
				targetTime.set(Calendar.HOUR_OF_DAY, 11);
				targetTime.set(Calendar.MINUTE, 0);
				targetTime.set(Calendar.SECOND, 0);
				targetTime.set(Calendar.MILLISECOND, 0);
				targetTime.add(Calendar.MINUTE, -minutes);
			} else if (targetTime.get(Calendar.HOUR_OF_DAY) < 23) {
				targetTime.set(Calendar.HOUR_OF_DAY, 23);
				targetTime.set(Calendar.MINUTE, 0);
				targetTime.set(Calendar.SECOND, 0);
				targetTime.set(Calendar.MILLISECOND, 0);
				targetTime.add(Calendar.MINUTE, -minutes);
			}
			environment.setStartLocation(new GeoTimeLocation(start.getLat(), start.getLon(), targetTime));
			environment.setAlgorithmType(RouterType.ANGLE);
			environment.setAngleParameter((int) Math.round(targetAngle));
			// environment.setMinAWD(minAWD);
			// environment.setMaxAWD(maxAWD);
			IsochroneRouter isoRouter = new IsochroneRouter(RouterType.ANGLE);
			Vector<IsoPoint> firstIso = isoRouter.generateIsochrone(environment);
			RouteGenerator.send2VRTool(navigationFile, firstIso, "ISOP " + targetTime.get(Calendar.HOUR_OF_DAY));

			// - Optimize the isochrone searching for the closest point to destination.
			Route targetRoute = IsochroneOptimizer.optimizeIsochrone(isoRouter, end);

			// - Terminate loop when distance to destination is below 5 miles
			double targetDistance = targetRoute.getEndLocation().distance(end);
			while (targetDistance > 1.0) {

				// // - Create the calculation limit with the distance to the destination
				// int cellCount = new Double(Math.hypot(end.getLat() - start.getLat(), end.getLon() -
				// start.getLat())).intValue();
				// GenerationLimit limit = new GenerationLimit();
				// // limit.setWindChangeLimit(2);
				// // - Get the first point and the initial winner route.
				// Route targetRoute = optimizeIsochrone(start, end, null);

				// while (targetRoute.getStepCount() > 1) {
				RouteCell firstCell = targetRoute.getFirsCell();
				int direction = firstCell.getNextDirection();
				optimRoute.add(firstCell, direction);

				// - Calculate the next environment for the next step.
				environment = new RouteEnvironment();
				// targetTime.add(Calendar.MINUTE, -minutes);
				environment.setStartLocation(new GeoTimeLocation(targetRoute.getEndLocation().getLat(), targetRoute
						.getEndLocation().getLon(), targetTime));
				environment.setAlgorithmType(RouterType.ANGLE);
				environment.setAngleParameter((int) Math.round(targetRoute.getEndLocation().angleTo(end)));
				Vector<IsoPoint> nextIso = isoRouter.generateIsochrone(environment);
				RouteGenerator.send2VRTool(navigationFile, nextIso, "ISOP " + targetTime.get(Calendar.HOUR_OF_DAY) + ":"
						+ targetTime.get(Calendar.MINUTE));
				targetRoute = IsochroneOptimizer.optimizeIsochrone(isoRouter, end);
				targetDistance = targetRoute.getEndLocation().distance(end);
			}
			RouteCell firstCell = targetRoute.getFirsCell();
			int direction = firstCell.getNextDirection();
			optimRoute.add(firstCell, direction);
			return optimRoute;
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Route isoStepOptimizer2(final GeoTimeLocation start, final GeoTimeLocation end) {
		Route optimRoute = new Route();
		optimRoute.setStartPoint(start);
		double targetAngle = start.angleTo(end);
		try {
			// - Calculate time up to the next hour boundary.
			Calendar now = start.getTime();
			Calendar nextTime = (Calendar) now.clone();
			nextTime.add(Calendar.HOUR, 1);
			nextTime.set(Calendar.MINUTE, 0);
			nextTime.set(Calendar.SECOND, 0);
			nextTime.set(Calendar.MILLISECOND, 0);
			int secondsToMove = (int) ((nextTime.getTimeInMillis() - now.getTimeInMillis()) / 1000);

			// - Get the first isochrone step for this number of seconds.
			RouteEnvironment environment = new RouteEnvironment();

			// - Set the start time to the next wind change less the number of seconds to run.
			Calendar targetTime = GregorianCalendar.getInstance();
			if (now.get(Calendar.HOUR_OF_DAY) < 11) {
				targetTime.set(Calendar.HOUR_OF_DAY, 11);
				targetTime.set(Calendar.MINUTE, 0);
				targetTime.set(Calendar.SECOND, 0);
				targetTime.set(Calendar.MILLISECOND, 0);
				targetTime.add(Calendar.SECOND, -secondsToMove);
			} else if (now.get(Calendar.HOUR_OF_DAY) < 23) {
				targetTime.set(Calendar.HOUR_OF_DAY, 23);
				targetTime.set(Calendar.MINUTE, 0);
				targetTime.set(Calendar.SECOND, 0);
				targetTime.set(Calendar.MILLISECOND, 0);
				targetTime.add(Calendar.SECOND, -secondsToMove);
			}
			environment.setStartLocation(new GeoTimeLocation(start.getLat(), start.getLon(), targetTime));
			environment.setAlgorithmType(RouterType.ANGLE);
			environment.setAngleParameter((int) Math.round(targetAngle));
			// environment.setMinAWD(minAWD);
			// environment.setMaxAWD(maxAWD);
			IsochroneRouter isoRouter = new IsochroneRouter(RouterType.ANGLE);
			Vector<IsoPoint> firstIso = isoRouter.generateIsochrone(environment);
			RouteGenerator.send2VRTool(navigationFile, firstIso, "ISOP " + targetTime.get(Calendar.HOUR_OF_DAY));

			// - Optimize the isochrone searching for the closest point to destination.
			Route targetRoute = IsochroneOptimizer.optimizeIsochrone(isoRouter, end);

			// - Terminate loop when distance to destination is below 5 miles
			double targetDistance = targetRoute.getEndLocation().distance(end);
			while (targetDistance > 5.0) {

				// // - Create the calculation limit with the distance to the destination
				// int cellCount = new Double(Math.hypot(end.getLat() - start.getLat(), end.getLon() -
				// start.getLat())).intValue();
				// GenerationLimit limit = new GenerationLimit();
				// // limit.setWindChangeLimit(2);
				// // - Get the first point and the initial winner route.
				// Route targetRoute = optimizeIsochrone(start, end, null);

				// while (targetRoute.getStepCount() > 1) {
				RouteCell firstCell = targetRoute.getFirsCell();
				int direction = firstCell.getNextDirection();
				optimRoute.add(firstCell, direction);

				// - Calculate the next environment for the next step.
				environment = new RouteEnvironment();
				targetTime.add(Calendar.HOUR, -1);
				environment.setStartLocation(new GeoTimeLocation(targetRoute.getEndLocation().getLat(), targetRoute
						.getEndLocation().getLon(), targetTime));
				environment.setAlgorithmType(RouterType.ANGLE);
				environment.setAngleParameter((int) Math.round(targetRoute.getEndLocation().angleTo(end)));
				Vector<IsoPoint> nextIso = isoRouter.generateIsochrone(environment);
				RouteGenerator.send2VRTool(navigationFile, nextIso, "ISOP " + targetTime.get(Calendar.HOUR_OF_DAY));
				targetRoute = IsochroneOptimizer.optimizeIsochrone(isoRouter, end);
				targetDistance = targetRoute.getEndLocation().distance(end);
			}
			RouteCell firstCell = targetRoute.getFirsCell();
			int direction = firstCell.getNextDirection();
			optimRoute.add(firstCell, direction);
			return optimRoute;
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private Vector<IsoPoint> searchPoint(IsoPoint targetPoint) throws RoutingException {
		int lat = new Double(Math.round(targetPoint.getLocation().getLat() * VORGConstants.TOMINUTES)).intValue();
		int lon = new Double(Math.round(targetPoint.getLocation().getLon() * VORGConstants.TOMINUTES)).intValue();
		Hashtable<Integer, Vector<IsoPoint>> box = registry.get(lat);
		if (null == box) throw new RoutingException("The target point is not located in the Registry database.");
		Vector<IsoPoint> target = box.get(lon);
		if (null == target) throw new RoutingException("The target point is not located in the Registry database.");
		return target;
	}

	private void classify(IsoPoint targetPoint) {
		int lat = new Double(Math.round(targetPoint.getLocation().getLat() * VORGConstants.TOMINUTES)).intValue();
		int lon = new Double(Math.round(targetPoint.getLocation().getLon() * VORGConstants.TOMINUTES)).intValue();
		// - Search for this latitude minute inside the registry.
		Hashtable<Integer, Vector<IsoPoint>> box = registry.get(lat);
		if (null == box) {
			// - Create a new box and register this point inside.
			box = new Hashtable<Integer, Vector<IsoPoint>>();
			registry.put(lat, box);
		}
		Vector<IsoPoint> target = box.get(lon);
		if (null == target) {
			// - Create the box content list
			target = new Vector<IsoPoint>();
			box.put(lon, target);
		}
		target.add(targetPoint);
	}

	public Vector<IsoPoint> getIsochrone() {
		return isochrone;
	}
}

// - UNUSED CODE ............................................................................................
