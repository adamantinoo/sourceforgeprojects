//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Directions;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.util.TimeZoneAdapter;
import net.sf.vorg.routecalculator.core.EndPoint;
import net.sf.vorg.routecalculator.core.EndPointAspect;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Una ruta en un conjunto de una o mas subrutas o de simples celdas. La ruta minima estara entonces compuesta
 * por una unica celda, mientras que puede haber rutas compuestas por subrutas de multiples celdas cada una.<br>
 * A route is a set of legs that are interconnected. Routes can be constructed by a Route Generator of by the
 * connection of free waypoints that will define each of the legs. A Route may be part of another route and in
 * those cases it is considered a subroute.
 */
public class Route2 extends Route {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger			= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	/**
	 * The initial start point of the Route. If this point connects to another route then it should be
	 * equivalent to the EndPoint on that Route.
	 */
	private EndPoint						startPoint	= null;
	/**
	 * The end point of the Route. If this point connects to another route then it should be equivalent to the
	 * EndPoint on that Route.
	 */
	private final EndPoint			endPoint		= null;
	/**
	 * This stores the point in time that represents the start point of the route. It can be the route creation
	 * time (the default) or any other time selected by the user or calculated by the generator. This time is in
	 * Game Time Coordinates because all wind cells are related to that time coordinate system
	 */
	private Calendar						startTime		= TimeZoneAdapter
																							.changeTimeZone(GregorianCalendar.getInstance(), "Etc/GMT+1");
	/** If this Route is not a simple route this this vector contains the pieces or legs of the composing route. */
	private final Vector<Route>	legs				= new Vector<Route>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Route2() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setStartPoint(GeoTimeLocation startLocation) {
		startPoint = new EndPoint(startLocation);
		startPoint.setAspect(EndPointAspect.START);
		// - Update and synch the start time.
		startTime = startPoint.getTime();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(startTime.getTime().toString()).append(VORGConstants.NEWLINE);
		buffer.append(route.toString()).append("");
		buffer.append(VORGConstants.NEWLINE);
		buffer.append("").append(controls.toString()).append("");
		return buffer.toString();
	}

	/**
	 * Add a new route leg to the end of the route and sets the direction of the border that connects the two
	 * cells. This direction can be one of the four sides of the cell, N, E, S or W.
	 * 
	 * @throws RoutingException
	 */
	public boolean add(final RouteCell newCell, final Directions direction) throws LocationNotInMap, RoutingException {
		// - Bug detected. If this two locations are the same we cannot continue.
		// - This is the case of entering a cell that will reflect us.
		if (newCell.getEntryLocation().isEquivalent(newCell.getExitLocation())) {
			logger.severe("Reflection point. We can not enter the cell because we are reflected. End of route.");
			throw new RoutingException("Reflection point. We can not enter the cell because we are reflected. End of route.");
		}

		// - Detect if there is a wind change inside this leg. If so divide the leg.
		if (checkIfWindChange(newCell.getEntryLocation().getTime(), newCell.getExitLocation().getTime())) {
			logger.fine("Detected wind change while adding cell: " + newCell.toString());
			// - Calculate an intermediate initial point.
			final GeoTimeLocation splitPoint = new GeoTimeLocation(calculateSplitPoint(newCell.getEntryLocation(), newCell
					.getExitLocation(), nextElevenDifference(newCell.getEntryLocation().getTime()), newCell.getSpeed()));
			logger.fine("New split point at: " + splitPoint.toString());
			RouteCell newChangeCell = new RouteCell(newCell.getCell(), newCell.getEntryLocation(), splitPoint);
			logger.finer("Current cell TTC: " + newChangeCell.getTTC());
			logger.finer("Current cell End Time Point: " + newChangeCell.getExitLocation().getTime().getTime().toString());
			append(newChangeCell, direction);
			return true;
		}
		append(newCell, direction);
		return false;
	}

	/**
	 * Add a new route cell to the end of the route and sets the direction of the border that connects the two
	 * cells. This direction can be one of the four sides of the cell, N, E, S or W.
	 */
	private void append(final RouteCell newCell, final Directions direction) throws LocationNotInMap {
		// - Check for the start formation of the route.
		if (creationState == RouteState.EMPTY) {
			route.add(newCell);
			lastNode = newCell;
			creationState = RouteState.MIDDLE;
			return;
		}
		if (creationState == RouteState.MIDDLE) {
			route.add(newCell);
			final RouteControl theControl = new RouteControl(newCell.getEntryLocation(), direction, lastNode, newCell);
			controls.add(theControl);
			lastNode = newCell;
			return;
		}
	}

	// // private Vector<Object> splitAtomicLeg(final Route theRoute, final RouteCell reference, final int
	// maxAWD)
	// // throws LocationNotInMap {
	// private RouteCell splitAtomicLeg(RouteCell reference) {
	// // final Vector<Object> cells = new Vector<Object>();
	// // // - Get the time start form the creation time of the route to keep all times in sych.
	// // final Calendar nowSearchTime = theRoute.getSearchTime();
	// //
	// // final WindCell windCell = WindMapHandler.cellWithPoint(reference.getCell().getLocation(),
	// // nowSearchTime.getTime());
	// // final GeoLocation entry = reference.getEntryLocation();
	// // final GeoLocation exit = reference.getExitLocation();
	//
	// // - Calculate an intermediate initial point.
	// final GeoTimeLocation splitPoint = new GeoTimeLocation(calculateSplitPoint(reference.getEntryLocation(),
	// reference
	// .getExitLocation(), nextElevenDifference(reference.getExitLocation().getWindTime()),
	// reference.getSpeed()));
	// return new RouteCell(reference.getCell(), reference.getEntryLocation(), splitPoint);
	//
	// // DEBUG Start of debug section
	// // final Calendar entryTime = theRoute.getCreationTime();
	// // entryTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() *
	// // VORGConstants.TOMINUTES).intValue());
	// // final Calendar endTime = theRoute.getCreationTime();
	// // endTime.add(Calendar.MINUTE, new Double(theRoute.getRouteTTC() * VORGConstants.TOMINUTES).intValue());
	// // endTime.add(Calendar.MINUTE, new Double(preRoute.getTTC() * VORGConstants.TOMINUTES).intValue());
	// // System.out.println("Start time=" + entryTime.getTime());
	// // System.out.println("End time=" + endTime.getTime());
	// // DEBUG End of debug section
	//
	// // cells.add(preRoute);
	// //
	// // // - Locate the next future wind cell.
	// // final WindCell currentWindCell = preRoute.getCell();
	// // final Date currentTime = currentWindCell.getTimeStamp();
	// // // final Calendar futureTime = Calendar.getInstance(TimeZone.getTimeZone("GMT+01"));
	// // final Calendar futureTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
	// // futureTime.setTime(currentTime);
	// // futureTime.add(Calendar.HOUR, 13);
	// // final WindCell futureWindCell = WindMapHandler.cellWithPoint(currentWindCell.getLocation(),
	// // futureTime.getTime());
	// // final int starboardAngle = GeoLocation.adjustAngleTo360(futureWindCell.getWindDir() - maxAWD);
	// // final Vector<Intersection> intersections = getIntersection(splitPoint, futureWindCell,
	// starboardAngle);
	// // final GeoLocation currentRouteLocation = intersections.lastElement().getLocation();
	// //
	// // final RouteCell postRoute = new RouteCell(futureWindCell, splitPoint, currentRouteLocation);
	// // cells.add(postRoute);
	// // cells.add(intersections.lastElement().getDirection());
	// // return cells;
	// }

	private double nextElevenDifference(final Calendar entryDate) {
		final int entryHours = entryDate.get(Calendar.HOUR_OF_DAY);
		final Calendar elevenTime = Calendar.getInstance();
		// final Calendar elevenTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		if (entryHours < 11)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 11, 0,
					0);
		else if (entryHours < 23)
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE), 23, 0,
					0);
		else
			elevenTime.set(entryDate.get(Calendar.YEAR), entryDate.get(Calendar.MONTH), entryDate.get(Calendar.DATE) + 1, 11,
					0, 0);
		final long diff = elevenTime.getTimeInMillis() - entryDate.getTimeInMillis();
		// - Convert to minutes.
		final double mins = diff / (60 * 1000);
		return mins;
	}

	/**
	 * Calculate the time point for route split on a wind change. The angle is the angle between the start and
	 * end. The distance to run depends on the speed and the time to elapse from the entry location time and the
	 * next wind change time.
	 */
	private GeoLocation calculateSplitPoint(final GeoLocation start, final GeoLocation end, final double minutes,
			final double speed) {
		// - Test to find a conversion factor between loxodromic and trigonometric distances.
		final double deltaLat = end.getLat() - start.getLat();
		double deltaLon = end.getLon() - start.getLon();

		// - Correct the difference if the points are at different sides of the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon -= 360.0 * Math.signum(deltaLon);
		final double Ls = Math.toRadians(start.getLat());
		final double Ld = Math.toRadians(end.getLat());
		final double ldelta = Math.toRadians(deltaLon);
		final double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		final double TTC = loxDistance / speed;
		final double t11 = minutes / VORGConstants.H2M;
		final double targetLat = t11 * deltaLat / TTC;
		final double targetLon = t11 * deltaLon / TTC;
		final GeoLocation splitLocation = new GeoLocation(start.getLat() + targetLat, start.getLon() + targetLon);

		return splitLocation;
	}

	/**
	 * Tests if this cell lies inside a wind change area. If the wind change time happens during the run on this
	 * cell the result of this method is <code>true</code>
	 * 
	 * @param exitTime
	 */
	protected boolean checkIfWindChange(Calendar enterTime, Calendar exitTime) {
		// - Test if this time is after the 11 hour or the 23 hours and enter time before.
		// Calendar testTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		// testTime.setTime(enterTime);
		int enterHour = enterTime.get(Calendar.HOUR_OF_DAY);
		// testTime = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		// testTime.setTime(exitTime);
		int exitHour = exitTime.get(Calendar.HOUR_OF_DAY);

		if ((enterHour < 11) && (exitHour >= 11)) return true;
		if ((enterHour < 23) && (exitHour >= 23)) return true;
		if ((enterHour < 23) && (exitHour < enterHour)) return true;
		return false;
	}
}
// - UNUSED CODE ............................................................................................
