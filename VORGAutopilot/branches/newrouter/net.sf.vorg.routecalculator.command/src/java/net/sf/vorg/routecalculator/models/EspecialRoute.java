//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class EspecialRoute extends ExtendedRoute {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	initialLocation;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public EspecialRoute(Calendar initialTime, GeoLocation initialLocation, double ttc) {
		startRouteTime = initialTime;
		this.initialLocation = initialLocation;
		startRouteTime.add(Calendar.SECOND, new Double(Math.round(ttc * 3600.0)).intValue());
	}
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
