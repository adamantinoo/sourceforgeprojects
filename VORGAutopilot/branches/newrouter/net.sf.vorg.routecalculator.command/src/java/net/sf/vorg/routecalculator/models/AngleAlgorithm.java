//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.routecalculator.core.RouterAlgorithm;

// - CLASS IMPLEMENTATION ...................................................................................
public class AngleAlgorithm implements RouterAlgorithm {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected int					angle		= -1;
	private int						minAWD	= 30;
	private int						maxAWD	= 170;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AngleAlgorithm(int angle) {
		this.angle = angle;
	}

	public AngleAlgorithm(int angle, int minAWD, int maxAWD) {
		this(angle);
		this.minAWD = minAWD;
		this.maxAWD = maxAWD;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getMovementAngle(int windDirection, double windSpeed) {
		final int newHeading = adjustSailAngle(angle, windDirection, windSpeed);
		return newHeading;
	}

	/**
	 * Adjusts the sailing angle to not to be up to the wind. The angle is changes until the speed is different
	 * from 0.0 and the angle is between the minimum AWD and max AWD angles.
	 * 
	 * @param windDirection
	 * @param windSpeed
	 */
	private int adjustSailAngle(final int angle, int windDirection, double windSpeed) {
		int newHeading = angle;
		int apparentHeading = GeoLocation.calculateAWD(windDirection, newHeading);
		// - Adjust AWD to be between the limits.
		if (apparentHeading < 0) {
			// - Process AWD to Port
			if (Math.abs(apparentHeading) > maxAWD) {
				newHeading -= Math.abs(apparentHeading) - maxAWD;
			}
			if (Math.abs(apparentHeading) < minAWD) {
				newHeading += minAWD - Math.abs(apparentHeading);
			}
		} else {
			if (Math.abs(apparentHeading) > maxAWD) {
				newHeading += Math.abs(apparentHeading) - maxAWD;
			}
			if (Math.abs(apparentHeading) < minAWD) {
				newHeading -= minAWD - Math.abs(apparentHeading);
			}
		}
		apparentHeading = GeoLocation.calculateAWD(windDirection, newHeading);
		SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windSpeed);

		// - Adjust if the speed = 0.0
		boolean awdAboveMin = Math.abs(Math.round(apparentHeading)) < Math.abs(minAWD);
		boolean awdBelowMax = Math.abs(Math.round(apparentHeading)) > Math.abs(maxAWD);
		boolean search = (sails.getSpeed() == 0.0) | awdAboveMin | awdBelowMax;
		int displacement = 1;
		if (apparentHeading > 0) {
			displacement = -1;
		} else {
			displacement = 1;
		}
		while (search) {
			newHeading = GeoLocation.adjustAngleTo360(newHeading + displacement);
			apparentHeading = GeoLocation.calculateAWD(windDirection, newHeading);
			sails = Polars.lookup(new Double(apparentHeading).intValue(), windSpeed);
			awdAboveMin = Math.abs(Math.round(apparentHeading)) < Math.abs(minAWD);
			awdBelowMax = Math.abs(Math.round(apparentHeading)) > Math.abs(maxAWD);
			search = (sails.getSpeed() == 0.0) | awdAboveMin | awdBelowMax;
		}
		return newHeading;
	}
}

// - UNUSED CODE ............................................................................................
