//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.models.WindCell;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCell2 extends RouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * Load the cell data with the required wind information and the entry and exit locations. Update the time
	 * on the exit location based on the current cell calculations and evaluation.
	 */
	public RouteCell2(final WindCell windCell, GeoTimeLocation entry, GeoTimeLocation exit) {
		cell = windCell;
		entryLocation = entry;
		exitLocation = exit;
		// - Perform the calculations for all the other cell parameters.
		calculateCell();
		exit.addTTC(entry, getTTC() * VORGConstants.H2S);
	}
	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
