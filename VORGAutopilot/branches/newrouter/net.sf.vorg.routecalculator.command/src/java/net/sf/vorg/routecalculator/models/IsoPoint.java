//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.routecalculator.core.NamedGeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class IsoPoint {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private NamedGeoLocation	location;
	private int								angle;
	private RouterType				algorithm;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public IsoPoint(NamedGeoLocation newLocation, int course, RouterType routerGeneratorType) {
		location = newLocation;
		angle = course;
		algorithm = routerGeneratorType;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public NamedGeoLocation getLocation() {
		return location;
	}

	public void setLocation(NamedGeoLocation location) {
		this.location = location;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

	public RouterType getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(RouterType algorithm) {
		this.algorithm = algorithm;
	}

	public String getName() {
		if (null != location)
			return location.getName();
		else
			return "UNDEFINED";
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[IsoPoint ");
		buffer.append("location=").append(location.toString()).append(",");
		buffer.append("angle=").append(angle).append(",");
		buffer.append("algorithm=").append(algorithm.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
