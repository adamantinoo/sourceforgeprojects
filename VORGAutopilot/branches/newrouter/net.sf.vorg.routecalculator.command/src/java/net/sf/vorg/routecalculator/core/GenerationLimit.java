//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.core;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class GenerationLimit {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger						= Logger.getLogger("net.sf.vorg.routecalculator.core");

	// - F I E L D - S E C T I O N ............................................................................
	private int						cellLimit					= 5;
	private boolean				detectWindChanges	= true;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GenerationLimit() {
	}

	public GenerationLimit(int cellCount) {
		setCellLimit(cellCount);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public boolean limitReached(int currentCellsNo, boolean windChange) {
		if (detectWindChanges) return windChange;
		if (currentCellsNo >= cellLimit) return true;
		return false;
	}

	public void setCellLimit(int newCellLimit) {
		cellLimit = newCellLimit;
		detectWindChanges = false;
	}

	// public void setWindChangeLimit(int i) {
	// // TODO Auto-generated method stub
	//
	// }
}

// - UNUSED CODE ............................................................................................
