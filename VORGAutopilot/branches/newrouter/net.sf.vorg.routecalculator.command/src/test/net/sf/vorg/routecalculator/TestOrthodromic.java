//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.Orthodromic;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestOrthodromic extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestOrthodromic() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testGetOrthoLatitude() {
		Orthodromic ortho = new Orthodromic(new GeoLocation(46, 0, -42, 0), new GeoLocation(49, 0, -37, 0));
		GeoLocation lat40W = ortho.getOrthoLatitude(-40.0);
	}
}

// - UNUSED CODE ............................................................................................
