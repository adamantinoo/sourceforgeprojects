//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;
import net.sf.vorg.routecalculator.models.IsochroneOptimizer;
import net.sf.vorg.routecalculator.models.IsochroneRouter;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.RouteGenerator;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestIsochroneRouter extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger					= Logger.getLogger("net.sf.vorg.routecalculator");
	private static String	navigationFile	= "L:\\VORG\\RouterTesting.nav";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestIsochroneRouter() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testGenerateIsochroneVMG() {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 04, -41, 30));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 19);
		now.set(Calendar.MINUTE, 11);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(start);
		environment.setAlgorithmType(RouterType.VMG);
		environment.setAngleParameter(53);

		final IsochroneRouter theRouter = new IsochroneRouter(RouterType.VMG);
		try {
			theRouter.generateIsochrone(environment);
			Route better = theRouter.getRoute(53);
			System.out.println(better.printReport());
			RouteGenerator.send2VRTool(navigationFile, better, better.getName());
			RouteGenerator.send2VRTool(navigationFile, theRouter.getIsochrone(), "ISO 53");
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testGenerateIsochroneANGLE() {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 04, -41, 30));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 19);
		now.set(Calendar.MINUTE, 11);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(start);
		environment.setAlgorithmType(RouterType.ANGLE);
		environment.setAngleParameter(61);

		IsochroneRouter theRouter = new IsochroneRouter(RouterType.ANGLE);
		theRouter.setOutputFile("L:\\VORG\\Leg7-BostonGalway.nav");
		try {
			theRouter.generateIsochrone(environment);
			theRouter.sendDataVRT();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testIsochroneOptimizer() {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 0, -42, 0));
		GeoTimeLocation end = new GeoTimeLocation(new GeoLocation(49, 0, -37, 0));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 11);
		now.set(Calendar.MINUTE, 3);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		IsochroneOptimizer optimizer = new IsochroneOptimizer();
		Route winner = optimizer.optimizeIsochrone(start, end, null);
		System.out.println(winner.printReport());
		RouteGenerator.send2VRTool("L:\\VORG\\Leg7-BostonGalway.nav", winner, winner.getName());

		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(start);
		environment.setAlgorithmType(RouterType.VMG);
		environment.setAngleParameter(new Double(Math.round(optimizer.getInitialAngle())).intValue());

		final IsochroneRouter theRouter = new IsochroneRouter(RouterType.VMG);
		theRouter.setOutputFile("L:\\VORG\\Leg7-BostonGalway.nav");
		try {
			theRouter.generateIsochrone(environment);
			theRouter.sendDataVRT();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testWinnerRoute() {
		// - Draw the isochrone for the first route
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 0, -42, 0));
		GeoTimeLocation end = new GeoTimeLocation(new GeoLocation(49, 0, -37, 0));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 11);
		now.set(Calendar.MINUTE, 3);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		IsochroneOptimizer optimizer = new IsochroneOptimizer();
		Route bestVMG = optimizer.optimizeIsochrone(start, end, null);
		System.out.println(bestVMG.printReport());
		RouteGenerator.send2VRTool(navigationFile, bestVMG, bestVMG.getName());
		RouteGenerator.send2VRTool(navigationFile, optimizer.getIsochrone(), "ISO x");
		Route winner = optimizer.winnerRoute(start, end);
		System.out.println(bestVMG.printReport());
		RouteGenerator.send2VRTool(navigationFile, winner, winner.getName());
	}

	public void testWinnerRoute2() {
		// - Draw the isochrone for the first route
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 1, -41, 59));
		GeoTimeLocation end = new GeoTimeLocation(new GeoLocation(49, 0, -37, 0));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 13);
		now.set(Calendar.MINUTE, 20);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		IsochroneOptimizer optimizer = new IsochroneOptimizer();
		Route bestVMG = optimizer.optimizeIsochrone(start, end, null);
		System.out.println(bestVMG.printReport());
		RouteGenerator.send2VRTool(navigationFile, bestVMG, bestVMG.getName());
		RouteGenerator.send2VRTool(navigationFile, optimizer.getIsochrone(), "ISO x");
		Route winner = optimizer.winnerRoute(start, end);
		System.out.println(bestVMG.printReport());
		RouteGenerator.send2VRTool(navigationFile, winner, winner.getName());
	}
}

// - UNUSED CODE ............................................................................................
