//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.RouteGenerator;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestRouteGenerator extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger					= Logger.getLogger("net.sf.vorg.routecalculator");
	private static String	navigationFile	= "L:\\VORG\\RouterTesting.nav";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestRouteGenerator() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testGenerateRouteAWD() {
		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(new GeoTimeLocation(new GeoLocation(46, 0, -42, 00)));
		environment.setAlgorithmType(RouterType.AWD);
		environment.setAngleParameter(-150);
		try {
			Route testAWDRoute = RouteGenerator.generateRoute(environment);
			System.out.println(testAWDRoute.printReport());
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testGenerateRouteVMG() {
		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(new GeoTimeLocation(new GeoLocation(46, 0, -42, 00)));
		environment.setAlgorithmType(RouterType.VMG);
		environment.setAngleParameter(34);
		try {
			Route testVMGRoute = RouteGenerator.generateRoute(environment);
			System.out.println(testVMGRoute.printReport());
			RouteGenerator.send2VRTool("L:\\VORG\\Leg7-BostonGalway.nav", testVMGRoute, "VMG "
					+ environment.getAngleParameter());
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testGenerateRouteANGLE() {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(46, 04, -41, 30));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 19);
		now.set(Calendar.MINUTE, 7);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);

		RouteEnvironment environment = new RouteEnvironment();
		environment.setStartLocation(start);
		environment.setAlgorithmType(RouterType.ANGLE);
		environment.setAngleParameter(60);
		try {
			Route testANGLERoute = RouteGenerator.generateRoute(environment);
			System.out.println(testANGLERoute.printReport());
			RouteGenerator.send2VRTool(navigationFile, testANGLERoute, "ANGLE " + environment.getAngleParameter());
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		environment = new RouteEnvironment();
		environment.setStartLocation(new GeoTimeLocation(new GeoLocation(46, 0, -42, 00)));
		environment.setAlgorithmType(RouterType.ANGLE);
		environment.setAngleParameter(58);
		try {
			Route testANGLERoute = RouteGenerator.generateRoute(environment);
			System.out.println(testANGLERoute.printReport());
			RouteGenerator.send2VRTool(navigationFile, testANGLERoute, "ANGLE " + environment.getAngleParameter());
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		environment = new RouteEnvironment();
		environment.setStartLocation(new GeoTimeLocation(new GeoLocation(46, 0, -42, 00)));
		environment.setAlgorithmType(RouterType.ANGLE);
		environment.setAngleParameter(108);
		try {
			Route testANGLERoute = RouteGenerator.generateRoute(environment);
			System.out.println(testANGLERoute.printReport());
			RouteGenerator.send2VRTool(navigationFile, testANGLERoute, "ANGLE " + environment.getAngleParameter());
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RoutingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

// - UNUSED CODE ............................................................................................
