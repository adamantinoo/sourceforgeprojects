//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.models.IsochroneOptimizer;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.RouteGenerator;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestOptimizers extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger					= Logger.getLogger("net.sf.vorg.routecalculator");
	private static final String	navigationFile	= "L:\\VORG\\RouterTesting.nav";

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestOptimizers() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testIsoStepOptimizer() throws Exception {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(53, 0, -9, 45));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 11);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);
		GeoTimeLocation end = new GeoTimeLocation(new GeoLocation(53, 30, -10, 30));
		IsochroneOptimizer optimizer = new IsochroneOptimizer();
		Route winner = optimizer.isoStepOptimizer2(start, end);
		System.out.println(winner.printReport());
		RouteGenerator.send2VRTool(navigationFile, winner, "OPTIM");
	}

	public void testIsoStepOptimizer2() throws Exception {
		GeoTimeLocation start = new GeoTimeLocation(new GeoLocation(53, 0, -9, 45));
		Calendar now = GregorianCalendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 11);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);
		start.setTime(now);
		GeoTimeLocation end = new GeoTimeLocation(new GeoLocation(53, 30, -10, 30));
		IsochroneOptimizer optimizer = new IsochroneOptimizer();
		Route winner = optimizer.isoStepOptimizer(start, end, 20);
		System.out.println(winner.printReport());
		RouteGenerator.send2VRTool(navigationFile, winner, "OPTIM");
	}
}

// - UNUSED CODE ............................................................................................
