//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.gef.EditPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class SelectableFigure extends Figure implements ISelectableFigure {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int	selected	= EditPart.SELECTED_NONE;

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return selected;
	}

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == getSelected()) || (EditPart.SELECTED == getSelected()))
			return true;
		else
			return false;
	}

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(final int value) {
		selected = value;
	}
}

// - UNUSED CODE ............................................................................................
