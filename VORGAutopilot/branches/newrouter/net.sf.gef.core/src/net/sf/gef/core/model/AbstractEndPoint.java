//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package net.sf.gef.core.model;

// - IMPORT SECTION .........................................................................................
import java.util.List;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractEndPoint extends AbstractGEFNode implements RouteEndPoint {
	private static final long	serialVersionUID	= 2014543229905972868L;

	// - F I E L D S

	// - C O N S T R U C T O R S
	public AbstractEndPoint() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public abstract void addConnection(Route wire);

	public List<Route> getSourceConnections() {
		return null;
	}

	public List<Route> getTargetConnections() {
		return null;
	}

	public abstract void removeConnection(Route conn);

	public abstract void setSource(Route wire);

	public abstract void setTarget(Route wire);
}

// - UNUSED CODE ............................................................................................
