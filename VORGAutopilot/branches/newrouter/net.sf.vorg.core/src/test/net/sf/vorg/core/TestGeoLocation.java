package net.sf.vorg.core;

import java.util.Formatter;
import java.util.Locale;

import junit.framework.TestCase;
import net.sf.vorg.core.models.GeoLocation;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;

public class TestGeoLocation extends TestCase {

	/**
	 * Check the calculations for all the main angles. The resulting value may be a positive or a negative
	 * number. 0-360 normalization should be performed at the call level.
	 */
	public void testAngleTo() {
		GeoLocation start = new GeoLocation(10, 0, 10, 0);
		// GeoLocation end = new GeoLocation(15, 0, 10, 0);
		double alpha = start.angleTo(new GeoLocation(12, 0, 10, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 11, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(11, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(10, 0, 12, 0));

		alpha = start.angleTo(new GeoLocation(9, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 11, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 10, 0));

		alpha = start.angleTo(new GeoLocation(8, 0, 9, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(9, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(10, 0, 8, 0));

		alpha = start.angleTo(new GeoLocation(11, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 9, 0));
		alpha = start.angleTo(new GeoLocation(120, 0, 10, 0));

		start = new GeoLocation(-2.500000, 157.295949);
		alpha = start.angleTo(new GeoLocation(-2.500000, 157.517088));

		// - Test angles to point at the other side of the date change line.
		start = new GeoLocation(-25.6778, 175.5190);
		alpha = start.angleTo(new GeoLocation(-47.0000, -155.0000));

		// - Test angles to point at the other side of the date change line.
		start = new GeoLocation(-34.8642, 179.6180);
		alpha = start.angleTo(new GeoLocation(-34.5000, 179.5833));
	}

	public void testReport() {
		final StringBuffer buffer = new StringBuffer();
		GeoLocation start = new GeoLocation(-2.500000, 157.295949);
		double latitude = start.getLat();
		Formatter formatter = new Formatter(Locale.ENGLISH);
		Formatter result = formatter.format("%1$03d", new Double(Math.floor(latitude * -1.0)).intValue());
		int dum = 0;
	}

	public void testDirectestimation() throws Exception {
		GeoLocation start = new GeoLocation(53, 0, -9, 45);
		double alpha = 348.0;
		VMCData vmc = new VMCData(318, 110, 6.5);

		GeoLocation w1 = start.directEstimation(2 * 60 * 60 + 30 * 60, vmc.getBestSailConfiguration().getSpeed(), 348.0);
		GeoLocation w2 = w1.directEstimation(3 * 60 * 60 + 38 * 60, 5.528, 266.0);
		GeoLocation w3 = w2.directEstimation(0 * 60 * 60 + 55 * 60, vmc.getBestSailConfiguration().getSpeed(), 348.0);
		int tiempoTotalH = 2 + 3 + 0;
		int tiempoTotalM = 30 + 38 + 55;
		//7H 03M

		SailConfiguration sails = Polars.lookup(330, 110, 6.5);
		GeoLocation w1t = start.directEstimation(3 * 60 * 60 + 40 * 60, sails.getSpeed(), 330.0);
		sails = Polars.lookup(255, 110, 6.5);
		GeoLocation w2t = w1t.directEstimation(1 * 60 * 60 + 10 * 60, sails.getSpeed(), 255.0);
		sails = Polars.lookup(327, 110, 6.5);
		GeoLocation w3t = w2t.directEstimation(1 * 60 * 60 + 20 * 60, sails.getSpeed(), 327.0);
		tiempoTotalH = 3 + 1 + 1;
		tiempoTotalM = 40 + 10 + 20;
		//6H 10M

	}

	public void testDiffAngle() throws Exception {
		double alpha1 = GeoLocation.angleDifference(5.0, 160.0);
		alpha1 = GeoLocation.angleDifference(5.0, 180.0);
		alpha1 = GeoLocation.angleDifference(5.0, 270.0);
		alpha1 = GeoLocation.angleDifference(5.0, 355.0);
		alpha1 = GeoLocation.angleDifference(355.0, 15.0);
		alpha1 = GeoLocation.angleDifference(355.0, 130.0);
		alpha1 = GeoLocation.angleDifference(355.0, 180.0);
		alpha1 = GeoLocation.angleDifference(355.0, 270.0);
	}
}
