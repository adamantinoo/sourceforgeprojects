//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.enums;

import java.util.Iterator;
import java.util.Vector;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum Directions {
	N, NE, E, SE, S, SW, W, NW, NS, EW, CENTER, OUTSIDE;
	static Vector<Directions>	baseVector	= new Vector<Directions>();
	static {
		baseVector.add(N);
		baseVector.add(NE);
		baseVector.add(E);
		baseVector.add(SE);
		baseVector.add(S);
		baseVector.add(SW);
		baseVector.add(W);
		baseVector.add(NW);
		baseVector.add(CENTER);
	}

	public static boolean detectCorner(Directions direction) {
		if (direction == NE) return true;
		if (direction == NW) return true;
		if (direction == SE) return true;
		if (direction == SW) return true;
		return false;
	}

	public static boolean detectBorder(Directions direction) {
		if (direction == CENTER) return false;
		if (direction == OUTSIDE) return false;
		return true;
	}

	public static Directions encode(int direction) {
		if (direction == 0x01) return Directions.N;
		if (direction == 0x02) return Directions.E;
		if (direction == 0x04) return Directions.S;
		if (direction == 0x08) return Directions.W;
		if (direction == 0x03) return Directions.NE;
		if (direction == 0x06) return Directions.SE;
		if (direction == 0x0C) return Directions.SW;
		if (direction == 0x09) return Directions.NW;
		return Directions.OUTSIDE;
	}

	public static Iterator<Directions> getIterator() {
		return baseVector.iterator();
	}
}
// - UNUSED CODE ............................................................................................
