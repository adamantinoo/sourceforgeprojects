//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface VORGConstants {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static final double	TOMINUTES							= 60.0;
	public static final double	EARTHRADIUS						= 3437.74;
	public static final String	VERSION								= "Version Beta 0.4.4b 10/04";
	public static final int			NOCONFIG							= 10;
	public static final int			INVALIDCONFIGURATION	= 11;
	public static final int			GENERICERROR					= 12;
	public static final double	MIN_POINT_DISTANCE			= 0.0001;
	public static final String	NEWLINE								= System.getProperty("line.separator");
	public static final double	H2S										= 3600.0;
	public static final double	M2H										= 1.0 / 60.0;
	public static final double	H2M										= 60.0;
}

// - UNUSED CODE ............................................................................................
