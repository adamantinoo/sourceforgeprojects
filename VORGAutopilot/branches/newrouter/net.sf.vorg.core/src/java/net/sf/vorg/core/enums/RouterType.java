//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.enums;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum RouterType {
	NONE, DIRECT, FILE, OPTIMDIRECT, OPTIMFILE, ISO, ISOOPTIM, ISOVMG, ISOANGLE, ANGLE, AWD, VMG, ISOSTEP, ISOSTEPVMG, ISOSTEPANGLE, ISOSTEPPOINT, OPTIMROUTE;

	public static RouterType encode(String argument) {
		if (argument.toUpperCase().equals("NONE")) return NONE;
		if (argument.toUpperCase().equals("DIRECT")) return DIRECT;
		if (argument.toUpperCase().equals("FILE")) return FILE;
		if (argument.toUpperCase().equals("OPTIMDIRECT")) return OPTIMDIRECT;
		if (argument.toUpperCase().equals("OPTIMFILE")) return OPTIMFILE;
		//		if (argument.toUpperCase().startsWith("ISOOPTIM")) return ISOOPTIM;
		if (argument.toUpperCase().equals("ISO")) return ISO;
		if (argument.toUpperCase().equals("ISOVMG")) return ISO;
		if (argument.toUpperCase().equals("ISOANGLE")) return ISO;
		if (argument.toUpperCase().equals("ANGLE")) return ANGLE;
		if (argument.toUpperCase().equals("AWD")) return AWD;
		if (argument.toUpperCase().equals("VMG")) return VMG;
		if (argument.toUpperCase().equals("ISOSTEP")) return ISOSTEP;
		if (argument.toUpperCase().equals("ISOSTEPVMG")) return ISOSTEPVMG;
		if (argument.toUpperCase().equals("ISOSTEPANGLE")) return ISOSTEPANGLE;
		if (argument.toUpperCase().equals("ISOSTEPPOINT")) return ISOSTEPPOINT;
		if (argument.toUpperCase().equals("OPTIMROUTE")) return OPTIMROUTE;
		return NONE;
	}
}
// - UNUSED CODE ............................................................................................
