//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.parsers.MapParserHandler;
import net.sf.vorg.core.util.TimeZoneAdapter;

// - CLASS IMPLEMENTATION ...................................................................................
public class WindMapHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	/** Stores the wind maps for the different prevision hours. */
	private static Hashtable<Date, WindMap>	windMaps	= null;

	/** Gets the wind cell that corresponds to the target location and to the target time. */
	public static WindCell cellWithPoint(final GeoLocation target, final Date selectTime) throws LocationNotInMap {
		if (null == WindMapHandler.windMaps) {
			// - The map is empty so load the first point.
			WindMapHandler.windMaps = new Hashtable<Date, WindMap>();
			WindMapHandler.loadWinds(target);
		}

		//- Check if the wind maps need a reload. This test is only done at the wind change times.
		if (WindMapHandler.needReload()) {
			WindMapHandler.clear();
			return WindMapHandler.cellWithPoint(target, selectTime);
		}
		final WindMap activeMap = WindMapHandler.getActiveMap(selectTime);
		// - If the map is null it can be because of error or that the cell data is not loaded.
		if (null != activeMap)
			try {
				return activeMap.cellWithPoint(target);
			} catch (final LocationNotInMap lnime) {
				WindMapHandler.loadWinds(target);
				return activeMap.cellWithPoint(target);
			}
		else
			throw new LocationNotInMap("The " + target.toString() + " location is not contained inside the Wind Map.");
	}

	public static void clear() {
		WindMapHandler.windMaps = null;
	}

	//	public static WindCell cellWithPoint(final GeoLocation target, final WindCell skip, final Date selectTime)
	//			throws LocationNotInMap {
	//		if (null == WindMapHandler.windMaps) {
	//			// - The map is empty so load the first point.
	//			WindMapHandler.windMaps = new Hashtable<Date, WindMap>();
	//			WindMapHandler.loadWinds(target);
	//		}
	//		final WindMap activeMap = WindMapHandler.getActiveMap(selectTime);
	//		if (null != activeMap)
	//			try {
	//				return activeMap.cellWithPoint(target, skip);
	//			} catch (final LocationNotInMap lnime) {
	//				WindMapHandler.loadWinds(target);
	//				return activeMap.cellWithPoint(target, skip);
	//			}
	//		else
	//			throw new LocationNotInMap("The " + target.toString() + " location is not contained inside the Route Map.");
	//	}

	public static WindCell getWindCell(final GeoLocation location) throws LocationNotInMap {
		final Calendar now = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		now.add(Calendar.HOUR, -2);
		return WindMapHandler.cellWithPoint(location, now.getTime());
	}

	public static void loadWinds(final GeoLocation location) {
		final String protocol = "http";
		final String host = "volvogame.virtualregatta.com";
		final String prefix = "/resources/winds/meteo_";

		// - Calculate the wind box that matches this location.
		final double latitude = Math.floor(location.getLat() + 0.5);
		final double l2 = Math.round(latitude / 10.0) * 10.0;
		int mapLat;
		if (latitude > l2)
			mapLat = new Double(l2).intValue() + 10;
		else
			mapLat = new Double(l2).intValue();

		// - Check if the longitude is below or after the 0.5 limit.
		final double minutes = location.getLon() - Math.floor(location.getLon());
		long longitude;
		if (minutes >= 0.5)
			longitude = Math.round(location.getLon() + 0.5);
		else
			longitude = Math.round(location.getLon());
		int mapLon = new Double(Math.floor(Math.floor(longitude) / 10.0) * 10.0).intValue();
		if (mapLon == 180) mapLon = -180;
		final String suffix = mapLon + "_" + mapLat + ".xml?rnd=9165";

		try {
			final URL mapReference = new URL(protocol, host, prefix + suffix);
			System.out.println("Loading map data for box " + suffix);
			final InputStream stream = new BufferedInputStream(mapReference.openStream());
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			// - Check for an empty wind map table.
			if (null == WindMapHandler.windMaps) WindMapHandler.windMaps = new Hashtable<Date, WindMap>();
			final MapParserHandler handler = new MapParserHandler(WindMapHandler.windMaps);
			parser.parse(stream, handler);
		} catch (final MalformedURLException mue) {
			mue.printStackTrace();
		} catch (final IOException ioe) {
			// - Code to create a fake wind map to be used while debugging without line.
			Date mapDate = Calendar.getInstance().getTime();

			// - Check if we have registered a map with that reference.
			WindMap currentMap = new WindMap();
			currentMap.setRef(mapDate.toString());
			WindMapHandler.windMaps.put(mapDate, currentMap);

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(-23, -43, 218, 3.8));
			currentMap.addCell(new WindCell(-24, -43, 211, 10.3));
			currentMap.addCell(new WindCell(-24, -42, 203, 11.3));
			currentMap.addCell(new WindCell(-23, -42, 204, 7.6));

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(32, 123, 40, 12.5));
			currentMap.addCell(new WindCell(32, 124, 330, 9.8));
			currentMap.addCell(new WindCell(32, 125, 70, 11.3));
			currentMap.addCell(new WindCell(33, 125, 32, 11.3));
			currentMap.addCell(new WindCell(33, 126, 32, 11.3));
			currentMap.addCell(new WindCell(34, 126, 349, 8.6));

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(1, 104, 16, 9.2));
			currentMap.addCell(new WindCell(1, 105, 3, 23.6));
			currentMap.addCell(new WindCell(2, 105, 10, 21.0));
			currentMap.addCell(new WindCell(2, 106, 9, 22.0));
			currentMap.addCell(new WindCell(3, 106, 13, 19.2));

			final Calendar nextMap = Calendar.getInstance();
			nextMap.add(Calendar.HOUR, 12);
			mapDate = nextMap.getTime();

			// - Check if we have registered a map with that reference.
			currentMap = new WindMap();
			currentMap.setRef(mapDate.toString());
			WindMapHandler.windMaps.put(mapDate, currentMap);

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(32, 123, 40, 12.5));
			currentMap.addCell(new WindCell(32, 124, 330, 9.8));
			currentMap.addCell(new WindCell(32, 125, 120, 11.3));
			currentMap.addCell(new WindCell(33, 125, 32, 11.3));
			currentMap.addCell(new WindCell(33, 126, 32, 11.3));
			currentMap.addCell(new WindCell(34, 126, 349, 8.6));

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(1, 104, 16, 9.2));
			currentMap.addCell(new WindCell(1, 105, 3, 23.6));
			currentMap.addCell(new WindCell(2, 105, 10, 21.0));
			currentMap.addCell(new WindCell(2, 106, 9, 22.0));
			currentMap.addCell(new WindCell(3, 106, 13, 19.2));
		} catch (final ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (final SAXException se) {
			se.printStackTrace();
		}
	}

	//[01]

	private static WindMap getActiveMap(final Date selectTime) {
		// - Select the right map depending on passing time by the cell.
		final Vector<Date> keyVector = new Vector<Date>(8);
		keyVector.addAll(WindMapHandler.windMaps.keySet());
		Collections.sort(keyVector);
		final Iterator<Date> mit = keyVector.iterator();
		Date lastReference = null;
		Date selectedReference = null;
		WindMap activeMap = null;
		while (mit.hasNext()) {
			final Date mapReference = mit.next();
			lastReference = mapReference;
			if (null == selectedReference) selectedReference = lastReference;
			if (selectTime.after(mapReference)) if (mapReference.before(selectedReference)) {
				if (lastReference.before(selectedReference)) selectedReference = lastReference;
			} else {
				activeMap = WindMapHandler.windMaps.get(mapReference);
				selectedReference = mapReference;
			}
		}
		if (null == activeMap) activeMap = WindMapHandler.windMaps.get(lastReference);
		return activeMap;
	}

	private static boolean needReload() {
		final Calendar now = TimeZoneAdapter.changeTimeZone(Calendar.getInstance(), "Etc/GMT+1");
		final int hour = now.get(Calendar.HOUR_OF_DAY);
		if ((hour == 11) || (hour == 23)) {
			//- Check that the last update time is close enough.
			final WindMap activeMap = WindMapHandler.getActiveMap(now.getTime());
			final Date mapTime = activeMap.getTimeStamp();
			now.add(Calendar.HOUR, -3);
			if (mapTime.before(now.getTime())) return true;
		}
		return false;
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
}
// - UNUSED CODE ............................................................................................
