//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.core;

// - IMPORT SECTION .........................................................................................
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Logger;

import org.xml.sax.Attributes;

// - CLASS IMPLEMENTATION ...................................................................................
public class NavAttributes implements Attributes {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger										logger		= Logger.getLogger("net.sf.vorg.vorgautopilot.core");

	// - F I E L D - S E C T I O N ............................................................................
	private final Hashtable<String, String>	contents	= new Hashtable<String, String>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NavAttributes() {
		// TODO Auto-generated constructor stub
	}

	public NavAttributes(final Properties props) {
		final Enumeration<?> penu = props.propertyNames();
		while (penu.hasMoreElements()) {
			final String propName = (String) penu.nextElement();
			contents.put(propName, props.getProperty(propName));
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public void add(final String name, final String value) {
		contents.put(name, value);
	}

	public int getIndex(final String name) {
		return 0;
	}

	public int getIndex(final String uri, final String localName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLength() {
		return contents.size();
	}

	public String getLocalName(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getQName(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(final String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(final String uri, final String localName) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getURI(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getValue(final int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getValue(final String name) {
		return contents.get(name);
	}

	public String getValue(final String uri, final String localName) {
		// TODO Auto-generated method stub
		return null;
	}
}

// - UNUSED CODE ............................................................................................
