//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

//- IMPORT SECTION .........................................................................................
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.app.VORGCommandApp;
import net.sf.vorg.core.enums.RouterType;
import net.sf.vorg.core.exceptions.BoatNotFoundException;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.core.models.Polars;
import net.sf.vorg.core.models.SailConfiguration;
import net.sf.vorg.core.models.VMCData;
import net.sf.vorg.routecalculator.core.GeoTimeLocation;
import net.sf.vorg.routecalculator.core.RouteEnvironment;
import net.sf.vorg.routecalculator.core.exceptions.RoutingException;
import net.sf.vorg.routecalculator.models.IsochroneOptimizer;
import net.sf.vorg.routecalculator.models.IsochroneRouter;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.RouteGenerator;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCalculator extends VORGCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger					= Logger.getLogger("net.sf.vorg.routecalculator");
	private static final String			APPLICATIONNAME	= "RouteCalculator";
	private static final String			APPDESCRIPTION	= "VORG Route Calculator Application";
	private static final String			VERSION					= RouteCalculator.APPDESCRIPTION + " 0.3.0";
	/** Reference to the static part of the application */
	private static RouteCalculator	singleton;
	static {
		RouteCalculator.logger.setLevel(Level.OFF);
	}
	static {
		VORGCommandApp.outputName = RouteCalculator.APPLICATIONNAME + ".output.txt";
	}

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherited code
		// to be executed instead making a lot of calls.
		RouteCalculator.singleton = new RouteCalculator(args);
		RouteCalculator.singleton.execute();
		VORGCommandApp.exit(0);
	}

	// - F I E L D - S E C T I O N ............................................................................
	private GeoTimeLocation	startLocation		= null;
	private GeoTimeLocation	endLocation			= null;
	private GeoTimeLocation	waypoint				= null;
	/** Boat heading direction when the start location is set from a game boat */
	private int							heading					= -1;
	private double					boatSpeed;
	private String					navigationFile	= null;
	private int							minAWD					= 30;
	private int							maxAWD					= 170;
	private int							targetAngle			= -1;
	private int							cellsToSpan			= 8;
	private Calendar				targetTime;
	private RouterType			routerType			= RouterType.NONE;
	private int							hours						= -1;
	private int							minutes					= 10;
	private RouterType			algorithm				= RouterType.VMG;

	private final boolean		printVMC				= false;
	private boolean					printVMG				= false;
	//	private final boolean		onlyDirect			= false;
	private int							windDir					= 0;
	private double					windSpeed				= 0.0;

	//private final boolean		optimize				= false;

	//private final double		ortoAngle				= -1.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are:
	 * <ul>
	 * <li><b>-conf<font color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where
	 * the application will expect the configuration files and data.</li>
	 * <li><b>-res<font color="GREY">[ourcesLocation</font></b> ${RESOURCEDIR} - is the directory where the
	 * application is going to locate the files that contains the SQL statements and other application
	 * resources.
	 */
	public RouteCalculator(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		banner();

		// - Process parameters and store them into the instance fields
		processParameters(args, RouteCalculator.APPLICATIONNAME);
	}

	private RouteEnvironment processEnvironment() {
		// - Check for required parameters that are the start and the waypoint
		if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);

		//- Get the initial target angle depending on the parameters
		if (heading >= 0)
			targetAngle = heading;
		else if (targetAngle < 0) {
			if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			targetAngle = new Double(Math.round(startLocation.angleTo(endLocation))).intValue();
		}

		RouteEnvironment environment = new RouteEnvironment();
		if (null != targetTime) startLocation.setTime(targetTime);
		environment.setStartLocation(startLocation);
		environment.setAlgorithmType(RouterType.VMG);
		if (checkValidAlgorithm(algorithm)) environment.setAlgorithmType(algorithm);
		environment.setAngleParameter(targetAngle);
		environment.setMinAWD(minAWD);
		environment.setMaxAWD(maxAWD);
		return environment;
	}

	/** Runs the optimizer or the route calculator depending on the specified parameters. */
	public void execute() {
		//		if (printVMC) if (null != startLocation) {
		//			WindCell cell;
		//			try {
		//				cell = WindMapHandler.getWindCell(startLocation);
		//				final VMCData vmc = new VMCData(heading, cell.getWindDir(), cell.getWindSpeed());
		//				VORGCommandApp.output(vmc.printReport());
		//			} catch (final LocationNotInMap e) {
		//				e.printStackTrace();
		//			}
		//		}
		if (printVMG) {
			final VMCData vmc = new VMCData(heading, windDir, windSpeed);
			output(vmc.printReport());
		}
		if (routerType == RouterType.ISO) {
			//			// - Check for required parameters that are the start and the waypoint
			//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//
			//			//- Get the initial target angle depending on the parameters
			//			if (heading >= 0)
			//				targetAngle = heading;
			//			else if (targetAngle < 0) {
			//				if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//				targetAngle = new Double(Math.round(startLocation.angleTo(waypoint))).intValue();
			//			}

			RouteEnvironment environment = processEnvironment();
			//			if (null != targetTime) startLocation.setTime(targetTime);
			//			environment.setStartLocation(startLocation);
			//			environment.setAlgorithmType(RouterType.VMG);
			//			if(checkValidAlgorithm(algorithm))environment.setAlgorithmType(algorithm);
			//			environment.setAngleParameter(targetAngle);
			//			environment.setMinAWD(minAWD);
			//			environment.setMaxAWD(maxAWD);

			final IsochroneRouter theRouter = new IsochroneRouter(algorithm);
			try {
				theRouter.generateIsochrone(environment);
				Route better = theRouter.getRoute(targetAngle);
				if (null != better) {
					output(better.printReport());
					RouteGenerator.send2VRTool(navigationFile, better, better.getName());
				}
				RouteGenerator.send2VRTool(navigationFile, theRouter.getIsochrone(), "ISO" + algorithm + " " + targetAngle);
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//		if (routerType == RouterType.ISOANGLE) {
		//			// - Check for required parameters that are the start and the waypoint
		//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//
		//			//- Get the initial target angle depending on the parameters
		//			if (heading >= 0)
		//				targetAngle = heading;
		//			else if (targetAngle < 0) {
		//				if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//				targetAngle = new Double(Math.round(startLocation.angleTo(waypoint))).intValue();
		//			}
		//
		//			RouteEnvironment environment = new RouteEnvironment();
		//			if (null != targetTime) startLocation.setTime(targetTime);
		//			environment.setStartLocation(startLocation);
		//			environment.setAlgorithmType(RouterType.ANGLE);
		//			environment.setAngleParameter(targetAngle);
		//			environment.setMinAWD(minAWD);
		//			environment.setMaxAWD(maxAWD);
		//
		//			final IsochroneRouter theRouter = new IsochroneRouter(RouterType.VMG);
		//			try {
		//				theRouter.generateIsochrone(environment);
		//				Route better = theRouter.getRoute(targetAngle);
		//				if (null != better) {
		//					output(better.printReport());
		//					RouteGenerator.send2VRTool(navigationFile, better, better.getName());
		//				}
		//				RouteGenerator.send2VRTool(navigationFile, theRouter.getIsochrone(), "ISOANG " + targetAngle);
		//			} catch (RoutingException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//		}
		if (routerType == RouterType.OPTIMROUTE) {
			//			// - Check for required parameters that are the start and the waypoint
			//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//			//- Get the initial target angle depending on the parameters
			//			if (heading >= 0) {
			//				if (targetAngle < 0) targetAngle = heading;
			//			}
			//			if (targetAngle < 0) {
			//				if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//				targetAngle = new Double(Math.round(startLocation.angleTo(endLocation))).intValue();
			//			}
			//			if (targetAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//
			//			RouteEnvironment environment = new RouteEnvironment();
			//			if (null != targetTime) startLocation.setTime(targetTime);
			//			environment.setStartLocation(startLocation);
			//			environment.setAlgorithmType(RouterType.VMG);
			//			environment.setAngleParameter(targetAngle);
			//			environment.setMinAWD(minAWD);
			//			environment.setMaxAWD(maxAWD);
			if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			RouteEnvironment environment = processEnvironment();
			try {
				Route targetRoute = RouteGenerator.generateRoute(environment);
				output(targetRoute.printReport());
				RouteGenerator.send2VRTool(navigationFile, targetRoute, targetRoute.getName());
				Route winner = RouteGenerator.optimizeRoute(targetRoute, endLocation);
				output(winner.printReport());
				RouteGenerator.send2VRTool(navigationFile, winner, winner.getName());
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (routerType == RouterType.ANGLE) {
			//			// - Check for required parameters that are the start and the waypoint
			//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//			if (heading >= 0) {
			//				if (targetAngle < 0) targetAngle = heading;
			//			}
			//			if (targetAngle < 0) {
			//				if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//				targetAngle = new Double(Math.round(startLocation.angleTo(endLocation))).intValue();
			//			}
			//			if (targetAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//
			//			RouteEnvironment environment = new RouteEnvironment();
			//			if (null != targetTime) startLocation.setTime(targetTime);
			//			environment.setStartLocation(new GeoTimeLocation(startLocation));
			//			environment.setAlgorithmType(RouterType.ANGLE);
			//			environment.setAngleParameter(targetAngle);
			//			environment.setMinAWD(minAWD);
			//			environment.setMaxAWD(maxAWD);
			RouteEnvironment environment = processEnvironment();
			environment.setAlgorithmType(RouterType.ANGLE);
			try {
				Route targetRoute = RouteGenerator.generateRoute(environment);
				output(targetRoute.printReport());
				RouteGenerator.send2VRTool(navigationFile, targetRoute, "ANGLE " + targetAngle);
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (routerType == RouterType.AWD) {
			// - Check for required parameters that are the start and the waypoint
			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			if (targetAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);

			RouteEnvironment environment = new RouteEnvironment();
			if (null != targetTime) startLocation.setTime(targetTime);
			environment.setStartLocation(new GeoTimeLocation(startLocation));
			environment.setAlgorithmType(RouterType.AWD);
			environment.setAngleParameter(targetAngle);
			try {
				Route targetRoute = RouteGenerator.generateRoute(environment);
				output(targetRoute.printReport());
				RouteGenerator.send2VRTool(navigationFile, targetRoute, "AWD " + targetAngle);
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (routerType == RouterType.VMG) {
			//			// - Check for required parameters that are the start and the waypoint
			//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//			if (heading >= 0) {
			//				if (targetAngle < 0) targetAngle = heading;
			//			}
			//			if (targetAngle < 0) {
			//				if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//				targetAngle = new Double(Math.round(startLocation.angleTo(endLocation))).intValue();
			//			}
			//			if (targetAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//
			//			RouteEnvironment environment = new RouteEnvironment();
			//			if (null != targetTime) startLocation.setTime(targetTime);
			//			environment.setStartLocation(new GeoTimeLocation(startLocation));
			//			environment.setAlgorithmType(RouterType.VMG);
			//			environment.setAngleParameter(targetAngle);
			//			environment.setMinAWD(minAWD);
			//			environment.setMaxAWD(maxAWD);
			RouteEnvironment environment = processEnvironment();
			environment.setAlgorithmType(RouterType.VMG);
			try {
				Route targetRoute = RouteGenerator.generateRoute(environment);
				output(targetRoute.printReport());
				RouteGenerator.send2VRTool(navigationFile, targetRoute, "VMG " + targetAngle);
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (routerType == RouterType.ISOSTEP) {
			//			// - Check for required parameters that are the start and the waypoint
			//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//			if (hours < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//
			//			//- Get the initial target angle depending on the parameters
			//			if (heading >= 0)
			//				targetAngle = heading;
			//			else if (targetAngle < 0) {
			//				if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			//				targetAngle = new Double(Math.round(startLocation.angleTo(waypoint))).intValue();
			//			}

			//			RouteEnvironment environment = new RouteEnvironment();
			RouteEnvironment environment = processEnvironment();
			//- Set the start time to eleven less the number of hours.
			Calendar time = GregorianCalendar.getInstance();
			int targetHour = 10;
			if (time.get(Calendar.HOUR_OF_DAY) < 11) {
				targetHour = 11 - hours;
			} else if (time.get(Calendar.HOUR_OF_DAY) < 23) {
				targetHour = 23 - hours;
			}
			time.set(Calendar.HOUR_OF_DAY, targetHour);
			time.set(Calendar.MINUTE, 0);
			time.set(Calendar.SECOND, 0);
			time.set(Calendar.MILLISECOND, 0);
			startLocation.setTime(time);
			environment.setStartLocation(startLocation);
			//			environment.setAlgorithmType(RouterType.VMG);
			//			environment.setAngleParameter(targetAngle);
			//			environment.setMinAWD(minAWD);
			//			environment.setMaxAWD(maxAWD);

			final IsochroneRouter theRouter = new IsochroneRouter(RouterType.VMG);
			try {
				theRouter.generateIsochrone(environment);
				Route better = theRouter.getRoute(targetAngle);
				if (null != better) {
					output(better.printReport());
					RouteGenerator.send2VRTool(navigationFile, better, better.getName());
				}
				RouteGenerator.send2VRTool(navigationFile, theRouter.getIsochrone(), "ISOSTEP" + algorithm + " " + hours + "H");
			} catch (RoutingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//		if (routerType == RouterType.ISOSTEPANGLE) {
		//			// - Check for required parameters that are the start and the waypoint
		//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//			if (hours < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//
		//			//- Get the initial target angle depending on the parameters
		//			if (heading >= 0)
		//				targetAngle = heading;
		//			else if (targetAngle < 0) {
		//				if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//				targetAngle = new Double(Math.round(startLocation.angleTo(waypoint))).intValue();
		//			}
		//
		//			RouteEnvironment environment = new RouteEnvironment();
		//			//- Set the start time to eleven less the number of hours.
		//			Calendar time = GregorianCalendar.getInstance();
		//			int targetHour = 10;
		//			if (time.get(Calendar.HOUR_OF_DAY) < 11) {
		//				targetHour = 11 - hours;
		//			} else if (time.get(Calendar.HOUR_OF_DAY) < 23) {
		//				targetHour = 23 - hours;
		//			}
		//			time.set(Calendar.HOUR_OF_DAY, targetHour);
		//			time.set(Calendar.MINUTE, 0);
		//			time.set(Calendar.SECOND, 0);
		//			time.set(Calendar.MILLISECOND, 0);
		//			if (minutes > 0) time.add(Calendar.MINUTE, -minutes);
		//			startLocation.setTime(time);
		//			environment.setStartLocation(startLocation);
		//			environment.setAlgorithmType(RouterType.ANGLE);
		//			environment.setAngleParameter(targetAngle);
		//			environment.setMinAWD(minAWD);
		//			environment.setMaxAWD(maxAWD);
		//
		//			final IsochroneRouter theRouter = new IsochroneRouter(RouterType.ANGLE);
		//			try {
		//				theRouter.generateIsochrone(environment);
		//				Route better = theRouter.getRoute(targetAngle);
		//				if (null != better) {
		//					output(better.printReport());
		//					RouteGenerator.send2VRTool(navigationFile, better, better.getName());
		//				}
		//				RouteGenerator.send2VRTool(navigationFile, theRouter.getIsochrone(), "ISOSTEPANGLE " + hours + ":" + minutes);
		//			} catch (RoutingException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
		//		}
		if (routerType == RouterType.ISOSTEPPOINT) {
			// - Check for required parameters that are the start and the waypoint
			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			if (null == endLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
			if (null != targetTime) startLocation.setTime(targetTime);
			IsochroneOptimizer optimizer = new IsochroneOptimizer();
			Route winner = optimizer.isoStepOptimizer(startLocation, endLocation, minutes);
			output(winner.printReport());
			RouteGenerator.send2VRTool(navigationFile, winner, "OPTIM " + hours + ":" + minutes);
		}
		//		if (routerType == RouterType.ISOOPTIM) {
		//			// - Check for required parameters that are the start and the waypoint
		//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//			if (null == waypoint) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//
		//			RouteEnvironment environment = new RouteEnvironment();
		//			environment.setStartLocation(new GeoTimeLocation(startLocation));
		//			environment.setAlgorithmType(RouterType.VMG);
		//			environment.setAngleParameter(new Double(Math.round(startLocation.angleTo(waypoint))).intValue());
		//
		//			final IsochroneRouter theRouter = new IsochroneRouter(RouterType.VMG);
		//			theRouter.setOutputFile(navigationFile);
		//			try {
		//				theRouter.generateIsochrone(environment);
		//				theRouter.sendDataVRT();
		//
		//				//- Optimize the set of routes by searching for the best heading
		//				//			Route baseRoute = theRouter.getBestRoute();
		//				IsochroneOptimizer optimizer = new IsochroneOptimizer(theRouter.getIsochrone());
		//			} catch (RoutingException re) {
		//				re.printStackTrace();
		//			}
		//		}
		//		if (routerType == RouterType.ISOSTEP) {
		//			// - Check for required parameters that are the start and the waypoint
		//			if (null == startLocation) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//			if (ortoAngle < 0) VORGCommandApp.exit(VORGConstants.INVALIDCONFIGURATION);
		//
		//			IsochroneStepRouter theRouter = new IsochroneStepRouter(routerType);
		//			theRouter.setOutputFile(navigationFile);
		//			theRouter.setOrtoAngle(ortoAngle);
		//			theRouter.generateIsochroneStep(startLocation, hours * 60);
		//
		//		}
		//		if (routerType == RouterType.DIRECT) if ((null != startLocation) && (null != endLocation)) {
		//			final Router theRouter = new Router(routerType);
		//			final Route directRoute = theRouter.generateRoute(startLocation, endLocation);
		//			VORGCommandApp.output("Direct Route");
		//			VORGCommandApp.output(directRoute.printReport());
		//			VORGCommandApp.output();
		//		}
		//		if (routerType == RouterType.OPTIMDIRECT) if ((null != startLocation) && (null != endLocation)) {
		//			final Router theRouter = new Router(routerType);
		//			theRouter.optimizeRoute(startLocation, endLocation);
		//		}
	}

	private boolean checkValidAlgorithm(RouterType value) {
		if (value == RouterType.VMG) return true;
		if (value == RouterType.ANGLE) return true;
		return false;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void processParameters(final String[] args, final String ApplicationName) {
		for (int i = 0; i < args.length; i++) {
			RouteCalculator.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-help")) {
					help();
					VORGCommandApp.exit(0);
				}
				if (args[i].toLowerCase().startsWith("-sta")) { //$NON-NLS-1$
					startLocation = new GeoTimeLocation();
					startLocation.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					startLocation.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-boat")) { //$NON-NLS-1$
					final String boatName = argumentStringValue(args, i);
					i++;
					final Boat boat = new Boat();
					try {
						boat.setName(boatName);
						boat.updateBoatData();
						startLocation = new GeoTimeLocation(boat.getLocation());
						heading = boat.getHeading();
						targetAngle = heading;
						boatSpeed = boat.getSpeed();
					} catch (final DataLoadingException dle) {
						System.out.println(dle.getMessage());
						VORGCommandApp.exit(VORGConstants.GENERICERROR);
					} catch (final BoatNotFoundException bnfe) {
						System.out.println(bnfe.getMessage());
						VORGCommandApp.exit(VORGConstants.GENERICERROR);
					}
					continue;
				}
				if (args[i].toLowerCase().startsWith("-end")) { //$NON-NLS-1$
					endLocation = new GeoTimeLocation();
					endLocation.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					endLocation.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-way")) { //$NON-NLS-1$
					waypoint = new GeoTimeLocation();
					waypoint.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					waypoint.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-nav")) { //$NON-NLS-1$
					navigationFile = argumentStringValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-minawd")) { //$NON-NLS-1$
					minAWD = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-maxawd")) { //$NON-NLS-1$
					maxAWD = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-hour")) {
					hours = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-minu")) {
					minutes = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-angle")) { //$NON-NLS-1$
					targetAngle = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-algo")) { //$NON-NLS-1$
					algorithm = RouterType.encode(argumentStringValue(args, i));
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-cell")) { //$NON-NLS-1$
					cellsToSpan = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-time")) { //$NON-NLS-1$
					targetTime = argumentDateValue(args, i);
					i++;
					continue;
				}
				//				if (args[i].toLowerCase().startsWith("-optim")) { //$NON-NLS-1$
				//					optimize = true;
				//					continue;
				//				}
				if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
					VORGCommandApp.onDebug = true;
					RouteCalculator.logger.setLevel(Level.ALL);
					continue;
				}
				//				if (args[i].toUpperCase().startsWith("-VMC")) { //$NON-NLS-1$
				//					heading = argumentIntegerValue(args, i);
				//					i++;
				//					// if (null == startLocation) {
				//					// System.out.println("Start location not specified in the right order. -start before -VMC.");
				//					// RouteCalculator.exit(1);
				//					// }
				//					printVMC = true;
				//					continue;
				//				}
				if (args[i].toUpperCase().startsWith("-VMG")) { //$NON-NLS-1$
					heading = argumentIntegerValue(args, i);
					i++;
					windDir = argumentIntegerValue(args, i);
					i++;
					windSpeed = argumentDoubleValue(args, i);
					i++;
					printVMG = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-polar")) { //$NON-NLS-1$
					final int apparent = argumentIntegerValue(args, i);
					i++;
					final double windSpeed = argumentDoubleValue(args, i);
					i++;
					final SailConfiguration conf = Polars.lookup(apparent, windSpeed);
					final StringBuffer buffer = new StringBuffer("[Polar search results").append('\n');
					buffer.append("Sail=").append(conf.getSail()).append(VORGConstants.NEWLINE);
					buffer.append("Speed=").append(conf.getSpeed()).append(VORGConstants.NEWLINE);
					VORGCommandApp.output(buffer.toString());
					VORGCommandApp.exit(0);
				}
				//				if (args[i].toLowerCase().startsWith("-direct")) { //$NON-NLS-1$
				//					onlyDirect = true;
				//					continue;
				//				}
				//				if (args[i].toLowerCase().startsWith("-deep")) { //$NON-NLS-1$
				//					final int deepLevel = argumentIntegerValue(args, i);
				//					i++;
				//					Route.setDeepLevel(deepLevel);
				//					continue;
				//				}
				if (args[i].toLowerCase().startsWith("-route")) {
					// - Get the router type. Can be DIRECT or FILE or ISO.
					routerType = RouterType.encode(argumentStringValue(args, i));
					i++;
					continue;
				}
			}
		}
		// ... Check that required parameters have values.
		// if ((null == startLocation) || (null == endLocation)) RouteCalculator.exit(VORGConstants.NOCONFIG);
	}

	private Calendar argumentDateValue(String[] args, int position) {
		final String argument = argumentStringValue(args, position);
		final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			final Date targetDate = formatter.parse(argument);
			if (null == targetDate)
				return GregorianCalendar.getInstance();
			else {
				Calendar target = GregorianCalendar.getInstance();
				target.setTimeInMillis(targetDate.getTime());
				return target;
			}
		} catch (final ParseException pe) {
			return GregorianCalendar.getInstance();
		}
	}

	private void banner() {
		VORGCommandApp.output(" ____             _        ____      _            _       _             ");
		VORGCommandApp.output("|  _ \\ ___  _   _| |_ ___ / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
		VORGCommandApp.output("| |_) / _ \\| | | | __/ _ \\ |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
		VORGCommandApp.output("|  _ < (_) | |_| | ||  __/ |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
		VORGCommandApp.output("|_| \\_\\___/ \\__,_|\\__\\___|\\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
		VORGCommandApp.output();
		VORGCommandApp.output(RouteCalculator.VERSION);
		VORGCommandApp.output();
	}

	private void help() {
		System.out.println("Command API for the RouteCalculator:");
		System.out.println("java -classpath routecalculator031.jar main.RouteCalculator <parameters>");
		System.out.println("Allowed generic parameters:");
		System.out.println("-sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out
				.println("-boat <boatname>  -- this is an alternate way to set the start location for the desired route.");
		System.out.println("-angle  -- Sets the angle for navigation or route proyection.");
		System.out.println("-time <DD/MM/YYYY HH:MM>  -- the start date/time for the route point.");
		System.out.println("-end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-way[point] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-nav[igation] <.NAV file path>  -- Path to the output file for all route lines generated.");
		System.out
				.println("-route[r] OPTIMROUTE  -- Generates the optimized incremental route from start to end based on the orthodromic projection.");
		System.out.println("   -sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("   -end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out
				.println("-route[r] ISO  -- Generates the isochrone to the first wind change based on a spread of angles from the start/waypoint heading but using VMG proyections.");
		System.out.println("   -angle  -- target angle for the routes");
		System.out.println("   -algo[rithm]  VMG/ANGLE -- algorith type to use in the route generation.");
		System.out.println("   -minAWD <degree>");
		System.out.println("   -maxAWD <degree>");
		//System.out
		//.println("-route[r] ISOANGLE  -- Generates the isochrone to the first wind change based on a spread of angles from the start/waypoint heading but using direct angles to calculate the route.");
		//System.out.println("   -angle  -- target angle for the routes");
		//System.out.println("   -minAWD <degree>");
		//System.out.println("   -maxAWD <degree>");
		System.out
				.println("-route[r] ISOSTEP  -- Generates the set of isochrones to end point separated by the elapsed number of minutes set on the parameters.");
		System.out.println("   -minAWD <degree>");
		System.out.println("   -maxAWD <degree>");
		System.out.println("   -algo[rithm]  VMG/ANGLE -- algorith type to use in the route generation.");
		System.out.println("   -minu[tes] <separation to next iso>");
		System.out.println("   -sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("   -end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out
				.println("-route[r] ISOSTEPPOINT  -- Generates the set of isochrones to end point separated by the elapsed number of minutes set on the parameters.");
		System.out.println("   -sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("   -end <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("   -minu[tes] <separation to next iso>");
		System.out
				.println("-route[r] ANGLE  -- Get the route for the specified number of cells or the first wind change in the angle heading.");
		System.out.println("   -angle  -- target angle for the routes");
		System.out.println("   -cells  -- number of cells to run to generate the route.");
		System.out.println("   -minAWD <degree>");
		System.out.println("   -maxAWD <degree>");
		System.out.println("-route[r] AWD  -- Route trying to follow this AWD settings.");
		System.out.println("   -angle AWD  -- value to set to the route");
		System.out.println("   -cells  -- number of cells to run to generate the route.");
		System.out.println("-route[r] VMG  -- VMG route for the specified number of cells or to the first wind change.");
		System.out.println("   -angle VMG  -- projection angle");
		System.out.println("   -cells  -- number of cells to run to generate the route.");
		System.out.println("   -minAWD <degree>");
		System.out.println("   -maxAWD <degree>");
		System.out.println();
		//		System.out.println("Allowed commands");
		//		System.out.println();
		System.out.println("Allowed toggles");
		//		System.out.println("-cells   -- number of levels to process in the VMG routing. Not used.");
		// System.out.println("-wind   -- if present then consider wind shift cell for splitting and recalculation.");
		System.out.println("-debug  -- if defined opens the ouput for verbose debugging information.");
		System.out.println();
		System.out.println("Allowed commands");
		System.out
				.println("-VMC <direction> -- calculates the VMC for the cell that contains the -start location over the parameter direction.");
		System.out
				.println("-VMG <direction> <winddir> <windspeed>  -- calculates the VMC for the parameter values. Does not need a cell location.");
		System.out
				.println("-polar <AWD> <windspeed>  -- calculates the exact polars for this AWD on the selected wind speed.");
		System.out
				.println("-router  -- optimizes a route. The next parameters specify the route and the optimizer operation.");
		System.out.println("    <type>  -- if there is a direct route of a loaded one.");
		// System.out.println("    <deep>  -- number of iterations to process between 10 and 60.");
		// System.out.println("    <iterations> -- number of thousands of iterations to descend a deep level.");
		System.out.println();
	}
}
// - UNUSED CODE ............................................................................................
