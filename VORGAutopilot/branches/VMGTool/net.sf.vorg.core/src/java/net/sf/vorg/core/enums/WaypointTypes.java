//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.enums;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum WaypointTypes {
	NOACTION, BYPASSED, POINT, ANGLE, VMG, VMGLOX, AWD, MAX;
	public static WaypointTypes decodeType(String typeName) {
		if (null == typeName) return WaypointTypes.NOACTION;
		if (typeName.toUpperCase().equals("BYPASSED")) return WaypointTypes.BYPASSED;
		if (typeName.toUpperCase().equals("POINT")) return WaypointTypes.POINT;
		if (typeName.toUpperCase().equals("DIRECT")) return WaypointTypes.POINT;
		if (typeName.toUpperCase().equals("ANGLE")) return WaypointTypes.ANGLE;
		if (typeName.toUpperCase().equals("VMG")) return WaypointTypes.VMG;
		if (typeName.toUpperCase().equals("VMGLOX")) return WaypointTypes.VMGLOX;
		if (typeName.toUpperCase().equals("AWD")) return WaypointTypes.AWD;
		if (typeName.toUpperCase().equals("MAX")) return WaypointTypes.MAX;
		return NOACTION;
	}
}

// - UNUSED CODE ............................................................................................
