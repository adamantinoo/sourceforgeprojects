//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models.facets;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This interface defines the methods to be declared on any element that is to be shown inside a tree display
 * view. The API declared is for the columns declared ion the tree.
 */
public interface ITreeFacet {

	// - F I E L D - S E C T I O N ............................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	//	public ITreeFacet getTreeFacet();
}

// - UNUSED CODE ............................................................................................
