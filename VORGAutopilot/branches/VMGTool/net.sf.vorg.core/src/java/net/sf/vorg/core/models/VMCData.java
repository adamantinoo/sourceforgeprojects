//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface VMCData {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	public static Logger			logger							= Logger.getLogger("net.sf.vorg.core");

	// - F I E L D - S E C T I O N ............................................................................
	//	protected final int				targetDirection;
	//	protected final int				windDirection;
	//	protected final double		windSpeed;
	//	private double						leftVMC							= 0.0;
	//	private int								leftAngle;
	//	private SailConfiguration	leftConfiguration		= new SailConfiguration();
	//	private double						rightVMC						= 0.0;
	//	private int								rightAngle;
	//	private SailConfiguration	rightConfiguration	= new SailConfiguration();
	//	private double						maxSpeed						= 0.0;
	//	private int								maxAngle;
	//	private SailConfiguration	maxConfiguration		= new SailConfiguration();
	//	private int								maxAWD;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	//	public VMCData(double heading, WindCell startCell) {
	//		this(new Double(heading).intValue(), startCell);
	//	}
	//
	//	public VMCData(int heading, WindCell startCell) {
	//		this(heading, startCell.getWindDir(), startCell.getWindSpeed());
	//	}
	//
	//	public VMCData(int heading, int windDirection, double windSpeed) {
	//		targetDirection = heading;
	//		this.windDirection = windDirection;
	//		this.windSpeed = windSpeed;
	//		calculateVMC();
	//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/** Scans all angles to store in the internal fields the values for the port, starboard and max speed VMC. */
	public void calculateVMC();

	public void addLeftData(double leftVMC, int leftAngle, SailConfiguration leftConfiguration);

	public void addRightData(double rightVMC, int rightAngle, SailConfiguration rightConfiguration);

	public int getBestAngle();

	public int getWorstAngle();

	public String printReport();

	public String printRecord();

	public SailConfiguration getBestSailConfiguration();

	public int getMaxAWD();

	public Object getMaxSpeed();

	public String toString();
}

// - UNUSED CODE ............................................................................................
