//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.enums.BoatModels;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class factory is a static class that will create instances of defined elements depending on the
 * different boat models available. There is a default option but if a boat identifies itself as a valid and
 * supported model the factory returned will always generate instances configured for that model
 * characteristics such as Polars and VMCData instances.
 */
public class BoatModelFactory {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.core.models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatModelFactory() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public static IBoatModel instanceOf(BoatModels baseModel) {
		if (BoatModels.DEFAULT == baseModel) return new BoatModelDefaultVORG();
		if (BoatModels.SUNSHINE == baseModel) return new BoatModelSunshine();
		return new BoatModelDefaultVORG();
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
class BoatModelDefaultVORG implements IBoatModel {

	public VMCData getVMCData(double heading, WindCell startCell) {
		return new DefaultVMCData(heading, startCell);
	}

	public VMCData getVMCData(int heading, WindCell startCell) {
		return new DefaultVMCData(heading, startCell);
	}

	public IPolars getPolars() {
		return new DefaultPolars();
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
class BoatModelSunshine implements IBoatModel {
	public VMCData getVMCData(double heading, WindCell startCell) {
		return new SunshineVMCData(heading, startCell);
	}

	public VMCData getVMCData(int heading, WindCell startCell) {
		return new SunshineVMCData(heading, startCell);
	}

	public IPolars getPolars() {
		return new DefaultPolars();
	}
}
// - UNUSED CODE ............................................................................................
