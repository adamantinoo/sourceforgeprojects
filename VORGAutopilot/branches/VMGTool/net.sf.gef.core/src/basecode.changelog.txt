-- thread.QueuedThreadPool.java
--1.2
- The public class QueuedThreadPoolConfig is made internal and local to the
  QueuedThreadPool class.
- Removed most of the obsolete code and performed a full review of the code
  and the funcionalities.
- The number of initial threads is set to 1 and thre is no need for
  configuration on this aspect. We currently create the first thread at the
  class creation.
- Added a new constructor to the interface IThreadFactory that does not require
  to pass the IQueue because the funcionality is obtained from the
  QueuedThreadPool.
- Added code to control the number of threads still working and thus simplify
  the control when the queue has finished processing.
- Optimization of some of the meyhods.  
- Added Javadoc documentation to some of the methods.
- Added new protected operations to manage the thread management from a single
  set of methods.
- A�adir una llamada periodica al QueuedThread para ajustar el numero de threads
  de trabajo y hacer asi adaptable el proceso de forma independiente al encolamiento
  de nuevas unidades de trabajo. Una llamada periodica o el proceso de desencolamiento
  son las implementaciones candidatas. Se ha optado por ambas soluciones poniendo
  una funcion de ajuste tanto en la funcion <<isEmpty>> como en <<dequeue>>.
