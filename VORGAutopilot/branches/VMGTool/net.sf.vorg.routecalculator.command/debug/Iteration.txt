[Route [
[RouteCell cell=[WindCell direction=80,speed=6.47948164152
          location=[GeoLocation lat=-3.0000,lon=158.0000] [3  0' S 158  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=154,distance=16.681921139747946,speed=9.7,ttc=1.7197856845100976
                entrylocation=[GeoLocation lat=-3.2493,lon=158.0400] [3  15' S 158  2' E]
                exitlocation=[GeoLocation lat=-3.5000,lon=158.1600] [3  30' S 158  10' E]], 
[RouteCell cell=[WindCell direction=47,speed=5.3995680346
          location=[GeoLocation lat=-4.0000,lon=158.0000] [4  0' S 158  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=137,distance=30.140822759665266,speed=9.01,ttc=3.3452633473546354
                entrylocation=[GeoLocation lat=-3.5000,lon=158.1600] [3  30' S 158  10' E]
                exitlocation=[GeoLocation lat=-3.8700,lon=158.5000] [3  52' S 158  30' E]], 
[RouteCell cell=[WindCell direction=63,speed=6.47948164152
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 09:00:00 CET 2009]
                alpha=143,distance=22.67547067188502,speed=10.01,ttc=2.265281785403099
                entrylocation=[GeoLocation lat=-3.8700,lon=158.5000] [3  52' S 158  30' E]
                exitlocation=[GeoLocation lat=-4.1700,lon=158.7300] [4  10' S 158  44' E]], 
[RouteCell cell=[WindCell direction=73,speed=9.71922246228
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=146,distance=2.162974311423758,speed=11.712,ttc=0.1846801836939684
                entrylocation=[GeoLocation lat=-4.1700,lon=158.7300] [4  10' S 158  44' E]
                exitlocation=[GeoLocation lat=-4.2000,lon=158.7500] [4  12' S 158  45' E]], 
[RouteCell cell=[WindCell direction=73,speed=9.71922246228
          location=[GeoLocation lat=-4.0000,lon=159.0000] [4  0' S 159  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=145,distance=21.96538468526784,speed=11.658000000000001,ttc=1.8841469107280697
                entrylocation=[GeoLocation lat=-4.2000,lon=158.7500] [4  12' S 158  45' E]
                exitlocation=[GeoLocation lat=-4.5000,lon=158.9600] [4  30' S 158  58' E]], 
[RouteCell cell=[WindCell direction=59,speed=10.259179265739998
          location=[GeoLocation lat=-5.0000,lon=159.0000] [5  0' S 159  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=140,distance=50.662440127319314,speed=12.552,ttc=4.036204599053483
                entrylocation=[GeoLocation lat=-4.5000,lon=158.9600] [4  30' S 158  58' E]
                exitlocation=[GeoLocation lat=-5.1500,lon=159.5000] [5  9' S 159  30' E]], 
[RouteCell cell=[WindCell direction=59,speed=10.7991360692
          location=[GeoLocation lat=-5.0000,lon=160.0000] [5  0' S 160  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=142,distance=26.49713735677282,speed=12.656,ttc=2.093642332235526
                entrylocation=[GeoLocation lat=-5.1500,lon=159.5000] [5  9' S 159  30' E]
                exitlocation=[GeoLocation lat=-5.5000,lon=159.7700] [5  30' S 159  46' E]], 
[RouteCell cell=[WindCell direction=42,speed=11.33909287266
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Wed Feb 25 21:00:00 CET 2009]
                alpha=134,distance=45.73820853543851,speed=13.044,ttc=3.5064557294877727
                entrylocation=[GeoLocation lat=-5.5000,lon=159.7700] [5  30' S 159  46' E]
                exitlocation=[GeoLocation lat=-6.0300,lon=160.3200] [6  2' S 160  19' E]], 
[RouteCell cell=[WindCell direction=43,speed=9.71922246228
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Thu Feb 26 09:00:00 CET 2009]
                alpha=158,distance=6.4614283139567945,speed=12.61,ttc=0.5124051002344802
                entrylocation=[GeoLocation lat=-6.0300,lon=160.3200] [6  2' S 160  19' E]
                exitlocation=[GeoLocation lat=-6.1300,lon=160.3600] [6  8' S 160  22' E]], 
[RouteCell cell=[WindCell direction=43,speed=9.71922246228
          location=[GeoLocation lat=-6.0000,lon=160.0000] [6  0' S 160  0' E]
          timeStamp=Thu Feb 26 09:00:00 CET 2009]
                alpha=135,distance=11.852465304307781,speed=12.406,ttc=0.9553816946886813
                entrylocation=[GeoLocation lat=-6.1300,lon=160.3600] [6  8' S 160  22' E]
                exitlocation=[GeoLocation lat=-6.2700,lon=160.5000] [6  16' S 160  30' E]], 
[RouteCell cell=[WindCell direction=42,speed=11.33909287266
          location=[GeoLocation lat=-6.0000,lon=161.0000] [6  0' S 161  0' E]
          timeStamp=Thu Feb 26 09:00:00 CET 2009]
                alpha=140,distance=17.87089689155059,speed=13.176,ttc=1.3563218648717812
                entrylocation=[GeoLocation lat=-6.2700,lon=160.5000] [6  16' S 160  30' E]
                exitlocation=[GeoLocation lat=-6.5000,lon=160.6901] [6  30' S 160  41' E]]]
[
[RouteControl direction=EW, setup location=[GeoLocation lat=-3.5000,lon=158.1600] [3  30' S 158  10' E]
              optimizedLocation=[GeoLocation lat=-3.5000,lon=158.1600] [3  30' S 158  10' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-3.8700,lon=158.5000] [3  52' S 158  30' E]
              optimizedLocation=[GeoLocation lat=-3.8700,lon=158.5000] [3  52' S 158  30' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-4.1700,lon=158.7300] [4  10' S 158  44' E]
              optimizedLocation=[GeoLocation lat=-4.1700,lon=158.7300] [4  10' S 158  44' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-4.2000,lon=158.7500] [4  12' S 158  45' E]
              optimizedLocation=[GeoLocation lat=-4.2000,lon=158.7500] [4  12' S 158  45' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-4.5000,lon=158.9600] [4  30' S 158  58' E]
              optimizedLocation=[GeoLocation lat=-4.5000,lon=158.9600] [4  30' S 158  58' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-5.1500,lon=159.5000] [5  9' S 159  30' E]
              optimizedLocation=[GeoLocation lat=-5.2500,lon=159.5000] [5  15' S 159  30' E]], 
[RouteControl direction=EW, setup location=[GeoLocation lat=-5.5000,lon=159.7700] [5  30' S 159  46' E]
              optimizedLocation=[GeoLocation lat=-5.5000,lon=159.7200] [5  30' S 159  43' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.0300,lon=160.3200] [6  2' S 160  19' E]
              optimizedLocation=[GeoLocation lat=-6.0800,lon=160.3200] [6  5' S 160  19' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.1300,lon=160.3600] [6  8' S 160  22' E]
              optimizedLocation=[GeoLocation lat=-6.1600,lon=160.3600] [6  10' S 160  22' E]], 
[RouteControl direction=NS, setup location=[GeoLocation lat=-6.2700,lon=160.5000] [6  16' S 160  30' E]
              optimizedLocation=[GeoLocation lat=-6.3000,lon=160.5000] [6  18' S 160  30' E]]]