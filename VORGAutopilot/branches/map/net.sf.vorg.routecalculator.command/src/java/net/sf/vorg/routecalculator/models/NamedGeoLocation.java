//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class NamedGeoLocation extends GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private String				name		= "GEO";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NamedGeoLocation(GeoLocation location) {
		this.latitude = location.getLat();
		this.longitude = location.getLon();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

// - UNUSED CODE ............................................................................................
