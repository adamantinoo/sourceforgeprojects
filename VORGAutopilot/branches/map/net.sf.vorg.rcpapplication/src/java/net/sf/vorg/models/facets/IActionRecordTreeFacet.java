//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ITreeFacet.java 190 2009-03-10 16:45:12Z boneymen $
//  LAST UPDATE:    $Date: 2009-03-10 17:45:12 +0100 (mar, 10 mar 2009) $
//  RELEASE:        $Revision: 190 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.models.facets;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.graphics.Image;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This interface defines the methods to be declared on any element that is to be shown inside a tree display
 * view. The API declared is for the columns declared ion the tree.
 */
public interface IActionRecordTreeFacet extends ITreeFacet {
	// - G L O B A L - S E C T I O N ..........................................................................
	static Logger						logger						= Logger.getLogger("net.sf.vorg.models.facets");
	public static final int	DATETIME_COLUMN		= 0;
	public static final int	BOAT_COLUMN				= DATETIME_COLUMN + 1;
	public static final int	LATITUDE_COLUMN		= DATETIME_COLUMN + 2;
	public static final int	LONGITUDE_COLUMN	= DATETIME_COLUMN + 3;

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getTimeStamp();

	public String getBoatName();

	public Image getImage4Type();

	public String getEventLatitude();

	public String getEventLongitude();
}

// - UNUSED CODE ............................................................................................
