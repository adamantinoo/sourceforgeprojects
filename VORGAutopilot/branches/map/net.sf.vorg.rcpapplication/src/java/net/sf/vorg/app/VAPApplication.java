//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: Application.java 184 2008-09-25 16:01:50Z boneymen $
//  LAST UPDATE:    $Date: 2008-09-25 18:01:50 +0200 (jue, 25 sep 2008) $
//  RELEASE:        $Revision: 184 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.app;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import net.sf.gef.core.app.AbstractRCPApplication;
import net.sf.vorg.models.UIPilotModelStore;

// - CLASS IMPLEMENTATION ...................................................................................
public class VAPApplication extends AbstractRCPApplication {
	// - I N N E R C L A S S - S E C T I O N ..................................................................
	// - CLASS IMPLEMENTATION .................................................................................
	/** Creates the initial windows and sets the default perspective. There should be a default perspective. */
	class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

		// - M E T H O D - S E C T I O N ........................................................................
		@Override
		public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer configurer) {
			return new ApplicationWorkbenchWindowAdvisor(configurer);
		}

		@Override
		public void eventLoopIdle(final Display display) {
			// FIXME This is the point where I can set my event loop without interfering with other threads.
			super.eventLoopIdle(display);
		}

		@Override
		public String getInitialWindowPerspectiveId() {
			return WelcomePerspective.PERSPECTIVE_ID;
		}

		@Override
		public void initialize(final IWorkbenchConfigurer configurer) {
			super.initialize(configurer);
			configurer.setSaveAndRestore(true);
		}
	}

	// - CLASS IMPLEMENTATION .................................................................................
	/** Configures the application window. */
	class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

		// - C O N S T R U C T O R - S E C T I O N ..............................................................
		public ApplicationWorkbenchWindowAdvisor(final IWorkbenchWindowConfigurer configurer) {
			super(configurer);
		}

		// - M E T H O D - S E C T I O N ........................................................................
		@Override
		public ActionBarAdvisor createActionBarAdvisor(final IActionBarConfigurer configurer) {
			return new ApplicationActionBarAdvisor(configurer);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
		 */
		@Override
		public void postWindowOpen() {
			super.postWindowOpen();
			// - Set the window position
			Display.getCurrent().getShells()[1].setLocation(210, 0);
		}

		/**
		 * This method is called before any window is open. This is the list of options that can be activated:
		 * 
		 * <pre>
		 * configurer.setShowCoolBar(false);
		 * configurer.setShowFastViewBars(false);
		 * configurer.setShowMenuBar(true); // Show the application menu bar
		 * configurer.setShowPerspectiveBar(false);
		 * configurer.setShowProgressIndicator(false);
		 * configurer.setShowStatusLine(false);
		 * </pre>
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#preWindowOpen()
		 */
		@Override
		public void preWindowOpen() {
			final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
			configurer.setInitialSize(new Point(800, 600));
			configurer.setShowCoolBar(false);
			configurer.setShowStatusLine(true);
			configurer.setShowProgressIndicator(true);
			// - Set the title of the main window
			configurer.setTitle(APPLICATION_NAME + " - RCP Application. " + "Version Beta 0.4.8b 01/05");
		}
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	/** The application ID. */
	private static final String	APPLICATION_ID		= "net.sf.vorg.app.id";
	public static final String	PLUGIN_ID					= APPLICATION_ID;
	public static final String	APPLICATION_NAME	= "VORG Autopilot User Interface";
	private static Logger				logger						= Logger.getLogger("net.sf.vorg.app");

	// - F I E L D - S E C T I O N ............................................................................
	// - C O N S T R U C T O R - S E C T I O N ................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public Object start(final IApplicationContext context) {
		// - Register application
		Activator.addReference(APPLICATION_ID, this);

		// - This line fires the creation of the perspective and all its contents. Also enters main loop
		logger.log(Level.INFO, "Starting and creating the " + APPLICATION_NAME
				+ ". Running workbench ApplicationWorkbenchAdvisor");
		Object returnValue = super.start(context, new ApplicationWorkbenchAdvisor());
		stop();
		return returnValue;
	}

	@Override
	public void stop() {
		//- Terminate the processing loop and save any dirty state.
		UIPilotModelStore store = (UIPilotModelStore) Activator.getByID(UIPilotModelStore.PILOTMODELID);
		if (null != store) {
			store.stop();
			store.updatePersistentStorage();
		}
		//		super.stop();
	}
}

// - UNUSED CODE ............................................................................................
