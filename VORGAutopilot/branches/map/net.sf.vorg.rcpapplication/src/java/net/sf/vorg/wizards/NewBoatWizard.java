//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.wizards;

// - IMPORT SECTION .........................................................................................
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

import net.sf.gef.core.wizards.AbstractWizard;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotBoat;

// - CLASS IMPLEMENTATION ...................................................................................
public class NewBoatWizard extends AbstractWizard {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the workbench used to display some elements. */
	protected IWorkbench						workbench;
	/**
	 * The workbench selection when this wizard was started. Not used internally because we use the model to
	 * access the data.
	 */
	protected IStructuredSelection	selection;
	private final PilotBoat					model;
	private BoatPropertiesMainPage	newBoatPage;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public NewBoatWizard(final PilotBoat newWizardModel) {
		super();
		if (null == newWizardModel)
			throw new NullPointerException("The wizards should receive a valid WizardModel.");
		model = newWizardModel;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void addPages() {
		newBoatPage = new BoatPropertiesMainPage(workbench, selection);
		addPage(newBoatPage);
	}

	@Override
	public boolean canFinish() {
		final IStatus status = newBoatPage.getStatus();
		if (status.getSeverity() == IStatus.OK)
			return true;
		else
			return false;
	}

	public PilotBoat getModel() {
		return model;
	}

	@Override
	public void init(final IWorkbench workbench, final IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
	}

	@Override
	public boolean performFinish() {
		newBoatPage.saveDataToModel();
		return true;
	}
}

//- CLASS IMPLEMENTATION ...................................................................................
class BoatPropertiesMainPage extends WizardPage {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.vorg.wizards");

	// - F I E L D - S E C T I O N ............................................................................
	//		private final PilotBoatWizardModel	model;
	protected IWorkbench						workbench;
	protected IStructuredSelection	selection;
	protected IStatus								pageStatus;

	//- Widgets on the page
	private Text										boatNameText;
	private Text										boatIdText;
	private Text										boatEmailText;
	private Text										boatClefText;

	//	private final boolean						validBoat	= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BoatPropertiesMainPage(final IWorkbench workbench, final IStructuredSelection selection) {
		super("Page1");
		setTitle("Enter Boat Identification");
		setDescription("Enter the Boat identification data such as the name, the boat number id\n and the access key to the game site identified as the 'clef' item.");
		setImageDescriptor(ImageFactory.getImageDescriptor("icons/addboat_wiz.png"));
		this.workbench = workbench;
		this.selection = selection;
		pageStatus = new Status(IStatus.WARNING, "not_used", 0, "The boat name can not be left empty.", null);
	}

	@Override
	public boolean canFlipToNextPage() {
		return false;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void createControl(final Composite parent) {
		//- Create the composite to hold the widgets
		final Composite composite = new Composite(parent, SWT.NULL);
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.makeColumnsEqualWidth = false;
		composite.setLayout(grid);

		//- Boat Name
		new Label(composite, SWT.NONE).setText("Boat Name:");

		final Composite nameValidation = new Composite(composite, SWT.NULL);
		final GridLayout gridPath = new GridLayout();
		gridPath.numColumns = 2;
		gridPath.makeColumnsEqualWidth = false;
		nameValidation.setLayout(grid);

		boatNameText = new Text(nameValidation, SWT.BORDER);
		GridData grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 200;
		boatNameText.setLayoutData(grdata);
		boatNameText.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				pageStatus = new Status(IStatus.WARNING, "not_used", 0, "The Boat name has to be validated before accepted..",
						null);
			}
		});
		final Button validate = new Button(nameValidation, SWT.NONE);
		validate.setText("Validate Boat");
		validate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(final MouseEvent e) {
				//- Validate this boat against the game database
				BoatPropertiesMainPage.this.validateBoatName(boatNameText.getText());
			}
		});

		//- Boat Identification number				
		new Label(composite, SWT.NONE).setText("Boat Code Number:");
		boatIdText = new Text(composite, SWT.BORDER);

		//- Boat Identification email
		new Label(composite, SWT.NONE).setText("Boat Email:");
		boatEmailText = new Text(composite, SWT.BORDER);
		grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 300;
		boatEmailText.setLayoutData(grdata);

		//- Boat clef
		new Label(composite, SWT.NONE).setText("Boat Clef:");
		boatClefText = new Text(composite, SWT.BORDER);
		grdata = new GridData(GridData.FILL_HORIZONTAL);
		grdata.widthHint = 300;
		boatClefText.setLayoutData(grdata);

		//- Set the composite as the control for this page
		setControl(composite);

		//- Load interface values with model values.
		onEnterPage();
	}

	@Override
	public IWizardPage getNextPage() {
		return null;
	}

	public IStatus getStatus() {
		return pageStatus;
	}

	public void saveDataToModel() {
		// Gets the model
		final NewBoatWizard wizard = (NewBoatWizard) getWizard();
		final PilotBoat model = wizard.getModel();

		model.setBoatName(boatNameText.getText());
		model.setBoatId(boatIdText.getText());
		model.setBoatEmail(boatEmailText.getText());
		model.setBoatClef(boatClefText.getText());
	}

	/**
	 * Applies the status to the status line of a dialog page.
	 */
	protected void applyToStatusLine(final IStatus status) {
		String message = status.getMessage();
		if (message.length() == 0) {
			message = null;
		}
		switch (status.getSeverity()) {
			case IStatus.OK:
				setErrorMessage(null);
				this.setMessage(message);
				break;
			case IStatus.WARNING:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.WARNING);
				break;
			case IStatus.INFO:
				setErrorMessage(null);
				this.setMessage(message, WizardPage.INFORMATION);
				break;
			default:
				setErrorMessage(message);
				this.setMessage(null);
				break;
		}
	}

	private void onEnterPage() {
		final NewBoatWizard wiz = (NewBoatWizard) getWizard();
		final PilotBoat model = wiz.getModel();

		//- Detect then when the name is not empty we are editing a Boat. Not editable
		if (null != model.getBoatName()) {
			boatNameText.setText(model.getBoatName());
			boatNameText.setEditable(false);
			validateBoatName(model.getBoatName());
		}
		if (null != model.getBoatId()) {
			boatIdText.setText(model.getBoatId());
		}
		if (null != model.getBoatEmail()) {
			boatEmailText.setText(model.getBoatEmail());
		}
		if (null != model.getBoatClef()) {
			boatClefText.setText(model.getBoatClef());
		}
		applyToStatusLine(pageStatus);
		getWizard().getContainer().updateButtons();
	}

	private void validateBoatName(final String boatName) {
		//- Gets the model
		final NewBoatWizard wizard = (NewBoatWizard) getWizard();
		final PilotBoat model = wizard.getModel();

		model.setBoatName(boatNameText.getText());
		//		model.setBoatId(boatIdText.getText());
		//		model.setBoatClef(boatClefText.getText());
		try {
			model.updateBoatData();
			//			model.validateClef();
			//			model.setState(ModelStates.READY);
			pageStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		} catch (final Exception exc) {
			pageStatus = new Status(IStatus.ERROR, "not_used", 0, "The boat " + boatNameText.getText()
					+ " was not found on hte game database.", null);
		}
		applyToStatusLine(pageStatus);
		getWizard().getContainer().updateButtons();
	}
}
// - UNUSED CODE ............................................................................................
