//  PROJECT:        net.sf.vgap4.assistant.ui
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.figures;

// - IMPORT SECTION .........................................................................................
import net.sf.gef.core.draw2d.RoundedGroup;
import net.sf.vorg.app.ImageKeys;
import net.sf.vorg.core.PilotUIConstants;
import net.sf.vorg.factories.ImageFactory;
import net.sf.vorg.vorgautopilot.models.PilotBoat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotCommandFigure extends RoundedGroup {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	public Font									FONT_BOATNAME	= new Font(Display.getDefault(), "Arial", 14, SWT.NORMAL);
	// - F I E L D - S E C T I O N ............................................................................
	//	private final PilotCommand	command;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommandFigure(final PilotCommand pilotCommand) {
		super(8);
		//		command = pilotCommand;
		setOpaque(true);
		groupName.setFont(PilotUIConstants.FONT_BOATNAME);
		groupName.setForegroundColor(PilotUIConstants.COLOR_GROUPNAME);
		setBackgroundColor(PilotUIConstants.COLOR_WHITE);

		//- Set the object's graphical icon. This uses the same common code for tree views.
		groupName.setIcon(ImageFactory.getImage(ImageKeys.WAYPOINT_SMALL));
		groupName.setIconTextGap(3);
		String text = ((PilotBoat) pilotCommand.getParent()).getBoatName() + " Waypoints ["
				+ pilotCommand.getWaypoints().size() + "]      Type: " + pilotCommand.getType();
		setText(text);

		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	//	@Override
	//	public void add(IFigure figure) {
	//		
	//	}
}
// - UNUSED CODE ............................................................................................
