//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.CommandStatus;
import net.sf.vorg.core.models.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotLimits extends PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static final long	serialVersionUID	= -7209807731220871559L;
	// - F I E L D - S E C T I O N ............................................................................
	private LimitAction				action						= LimitAction.NONE;
	private LimitBorder				border						= LimitBorder.NOBORDER;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotLimits() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public void setBorder(String value) {
		border = LimitBorder.decode(value);
	}

	public void setAction(String value) {
		action = LimitAction.decode(value);
	}

	public CommandStatus testLimit(GeoLocation boatLocation) {
		if (border == LimitBorder.NOBORDER) {
			if (action == LimitAction.NONE) return CommandStatus.NOCHANGE;
			if (action == LimitAction.CONNECT) return CommandStatus.GO;
			if (action == LimitAction.DISCONNECT) return CommandStatus.NOGO;
		}
		if (border == LimitBorder.NORTH) if (boatLocation.getLat() > getLocation().getLat()) {
			if (action == LimitAction.NONE) return CommandStatus.NOCHANGE;
			if (action == LimitAction.CONNECT) return CommandStatus.GO;
			if (action == LimitAction.DISCONNECT) return CommandStatus.NOGO;
		}
		if (border == LimitBorder.SOUTH) if (boatLocation.getLat() < getLocation().getLat()) {
			if (action == LimitAction.NONE) return CommandStatus.NOCHANGE;
			if (action == LimitAction.CONNECT) return CommandStatus.GO;
			if (action == LimitAction.DISCONNECT) return CommandStatus.NOGO;
		}
		if (border == LimitBorder.EAST) if (boatLocation.getLon() > getLocation().getLon()) {
			if (action == LimitAction.NONE) return CommandStatus.NOCHANGE;
			if (action == LimitAction.CONNECT) return CommandStatus.GO;
			if (action == LimitAction.DISCONNECT) return CommandStatus.NOGO;
		}
		if (border == LimitBorder.WEST) if (boatLocation.getLon() < getLocation().getLon()) {
			if (action == LimitAction.NONE) return CommandStatus.NOCHANGE;
			if (action == LimitAction.CONNECT) return CommandStatus.GO;
			if (action == LimitAction.DISCONNECT) return CommandStatus.NOGO;
		}
		return CommandStatus.NOCHANGE;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[PilotLimits ");
		buffer.append("action=").append(action).append(", ");
		buffer.append("border=").append(border).append(", ");
		buffer.append(getLocation()).append("");
		buffer.append("]");
		return buffer.toString();
	}

	public String generatePersistentXML() {
		final StringBuffer buffer = new StringBuffer();
		//- Compose the waypoint data line.
		buffer.append("          <pilotlimit ");
		buffer.append("border=").append(this.quote(border.toString())).append(" ");
		buffer.append("action=").append(this.quote(action.toString())).append(" ");
		buffer.append("latitude=").append(this.quote(getLocation().getLat())).append(" ");
		buffer.append("longitude=").append(this.quote(getLocation().getLon())).append(" ");
		buffer.append("/>").append(VORGConstants.NEWLINE);
		return buffer.toString();
	}
}

enum LimitAction {
	NONE, CONNECT, DISCONNECT;

	public static LimitAction decode(String value) {
		if (null == value) return NONE;
		if (value.toLowerCase().equals("connect")) return CONNECT;
		if (value.toLowerCase().equals("disconnect")) return DISCONNECT;
		return NONE;
	}
}

enum LimitBorder {
	NOBORDER, NORTH, EAST, SOUTH, WEST;
	public static LimitBorder decode(String value) {
		if (null == value) return NOBORDER;
		if (value.toLowerCase().equals("north")) return NORTH;
		if (value.toLowerCase().equals("east")) return EAST;
		if (value.toLowerCase().equals("south")) return SOUTH;
		if (value.toLowerCase().equals("west")) return WEST;
		return NOBORDER;
	}
}
// - UNUSED CODE ............................................................................................
