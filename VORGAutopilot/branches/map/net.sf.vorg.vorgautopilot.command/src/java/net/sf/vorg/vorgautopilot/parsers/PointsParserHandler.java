//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.sf.vorg.core.enums.WaypointTypes;
import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.Waypoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class PointsParserHandler extends DefaultHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat	boatRef;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PointsParserHandler(final Boat boat) {
		boatRef = boat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		if (name.toLowerCase().equals("waypoint")) {
			final Waypoint waypoint = new Waypoint(WaypointTypes.POINT);
			waypoint.setLatitude(attributes.getValue("latitude"));
			waypoint.setLongitude(attributes.getValue("longitude"));
			boatRef.surpassWaypointDirect(waypoint);
		}
	}
}

// - UNUSED CODE ............................................................................................
