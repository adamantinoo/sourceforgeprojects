//  PROJECT:        net.sf.vorg.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.core.enums;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public enum InputTypes {
	NONE, NAV, XML, HTTP;

	public static InputTypes decode(final String inputType) {
		if (inputType.toUpperCase().equals("NAV")) return InputTypes.NAV;
		if (inputType.toUpperCase().equals("XML")) return InputTypes.XML;
		if (inputType.toUpperCase().equals("HTTP")) return InputTypes.HTTP;
		return InputTypes.NONE;
	}
}

// - UNUSED CODE ............................................................................................
