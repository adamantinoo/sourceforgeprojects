//  PROJECT:        net.sf.gef.core
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2008 by LDD Game Development Spain, all rights reserved.

package net.sf.gef.core.editparts;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractDirectedEditPart extends AbstractContainerEditPart {
	// - S T A T I C - S E C T I O N ..........................................................................
	//	private static Logger	logger	= Logger.getLogger("net.sf.gef.core.editparts");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AbstractDirectedEditPart() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void applyGraphResults(final DirectedGraph graph, final Map<AbstractDirectedNodeEditPart, Node> map) {
		this.applyChildrenResults(graph, map);
	}

	public void contributeNodesToGraph(final DirectedGraph graph, final Map<AbstractDirectedNodeEditPart, Node> map) {
		//- Scan all the children for this diagram and add all them to the graph.
		Node previousNode = null;
		final int counter = 0;
		boolean firstNode = true;
		final List<AbstractDirectedNodeEditPart> contents = this.getChildren();
		final Iterator<AbstractDirectedNodeEditPart> cit = contents.iterator();
		while (cit.hasNext()) {
			//- Get the part, the figure and the model.
			final AbstractDirectedNodeEditPart part = cit.next();

			//- Create the node and fill its fields.
			final Node node = new Node(part);
			node.setPadding(AbstractDirectedNodeEditPart.PADDING);
			node.setRowConstraint(counter);
			node.setSize(part.getSize());
			if (firstNode) {
				//- Do not connect this node with an edge.
				previousNode = node;
				firstNode = false;
				graph.nodes.add(node);
				map.put(part, node);
			} else {
				//- Connect this node and the previous one.
				final Edge connectingEdge = new Edge(previousNode, node);
				previousNode = node;
				graph.nodes.add(node);
				graph.edges.add(connectingEdge);
				map.put(part, node);
			}
		}
	}

	protected void applyChildrenResults(final DirectedGraph graph, final Map<AbstractDirectedNodeEditPart, Node> map) {
		//TODO Calculate the max width of all children.
		Iterator<Node> mit = map.values().iterator();
		int maxWidth = 0;
		while (mit.hasNext()) {
			final int nodeWidth = mit.next().width;
			if (nodeWidth > maxWidth) {
				maxWidth = nodeWidth;
			}
		}
		//- Update the widths of all nodes to the max width.
		mit = map.values().iterator();
		while (mit.hasNext()) {
			final Node node = mit.next();
			node.width = maxWidth;
			node.x = 2;
		}

		for (int i = 0; i < this.getChildren().size(); i++) {
			final AbstractDirectedNodeEditPart part = (AbstractDirectedNodeEditPart) this.getChildren().get(i);
			part.applyGraphResults(graph, map);
		}
	}

	@Override
	protected void createEditPolicies() {
		// - Disallows the removal of this edit part from its parent
		this.installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		this.installEditPolicy(EditPolicy.NODE_ROLE, null);
		this.installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, null);
		this.installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
	}
}

// - UNUSED CODE ............................................................................................
