package net.sf.vorg.vorgautopilot.internals;

public enum CommandTypes {
	NOCOMMAND, VMG, VMGROUTE, ROUTE, DIRECT;
	public static CommandTypes decodeType(String typeName) {
		if (typeName.toUpperCase().equals("VMG")) return CommandTypes.VMG;
		if (typeName.toUpperCase().equals("VMGROUTE")) return CommandTypes.VMGROUTE;
		if (typeName.toUpperCase().equals("ROUTE")) return CommandTypes.ROUTE;
		if (typeName.toUpperCase().equals("DIRECT")) return CommandTypes.DIRECT;
		return DIRECT;
	}
}
