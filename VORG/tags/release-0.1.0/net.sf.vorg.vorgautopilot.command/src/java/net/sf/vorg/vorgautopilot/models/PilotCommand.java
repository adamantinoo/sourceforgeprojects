//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.xml.sax.Attributes;

import net.sf.vorg.routecalculator.command.main.VMCRouter;
import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.models.VMCData;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.CommandStatus;
import net.sf.vorg.vorgautopilot.internals.CommandTypes;

//- CLASS IMPLEMENTATION ...................................................................................
public class PilotCommand {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger				= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat					boatRef;
	private CommandTypes				type					= CommandTypes.NOCOMMAND;
	// private final GeoLocation waypoint = new GeoLocation();
	private Vector<Waypoint>		waypointList	= new Vector<Waypoint>();
	private Vector<PilotLimits>	limits				= new Vector<PilotLimits>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotCommand(final String typeName, final Boat parentBoat) {
		type = CommandTypes.decodeType(typeName);
		boatRef = parentBoat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public CommandTypes getType() {
		return type;
	}

	public void pilot() {
		System.out.println(Calendar.getInstance().getTime() + " - " + "Processing command: " + this);
		// if (type == CommandTypes.NOCOMMAND) return;
		// // if (type == CommandTypes.VMG) pilotMaxVMG(boatRef, waypointList.firstElement().getLocation());
		// if (type == CommandTypes.VMGROUTE) pilotFollowVMG(boatRef);
		/* if (type == CommandTypes.ROUTE) */
		pilotFollowRoute(boatRef);
	}

	public void setWaypointList(final Vector<Waypoint> buildWaypointList) {
		waypointList = buildWaypointList;
	}

	public void startElement(final String name, final Attributes attributes) {
		// if (name.toLowerCase().equals("pilotcommand")) if (type == CommandTypes.VMG) {
		// waypointList = new Vector<Waypoint>();
		// Waypoint waypoint = new Waypoint();
		// waypoint.setLatitude(attributes.getValue("latitude"));
		// waypoint.setLongitude(attributes.getValue("longitude"));
		// waypointList.add(waypoint);
		// // setLatitude(attributes.getValue("waypointlatitude"));
		// // setLongitude(attributes.getValue("waypointlongitude"));
		// }
		if (name.toLowerCase().equals("pilotlimits")) limits = new Vector<PilotLimits>();
		if (name.toLowerCase().equals("limit")) {
			final PilotLimits newLimit = new PilotLimits();
			newLimit.setBorder(attributes.getValue("border"));
			newLimit.setLatitude(attributes.getValue("latitude"));
			newLimit.setLongitude(attributes.getValue("longitude"));
			newLimit.setAction(attributes.getValue("action"));
			limits.add(newLimit);
		}
		if (name.toLowerCase().equals("waypointlist")) waypointList = new Vector<Waypoint>();
		if (name.toLowerCase().equals("waypoint")) {
			final Waypoint waypoint = new Waypoint(attributes.getValue("type"));
			waypoint.setLatitude(attributes.getValue("latitude"));
			waypoint.setLongitude(attributes.getValue("longitude"));
			waypoint.setRange(attributes.getValue("range"));
			waypoint.setMinAWD(attributes.getValue("minAWD"));
			waypointList.add(waypoint);
		}
	}

	public CommandStatus testActivity() {
		// - Check if the command is active or inactive by limits.
		CommandStatus status = CommandStatus.NOCHANGE;
		final Iterator<PilotLimits> lit = limits.iterator();
		while (lit.hasNext()) {
			final PilotLimits limit = lit.next();
			final CommandStatus test = limit.testLimit(boatRef.getLocation());
			if (test != CommandStatus.NOCHANGE) status = test;
			// System.out.println("Testing limit " + limit + " with the result of: " + status);
		}
		return status;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("\n[PilotCommand ");
		buffer.append("type=").append(type).append("");
		// if (type == CommandTypes.VMG) buffer.append("waypoint=").append(waypoint).append("]");
		/* if (type == CommandTypes.TRACK) */
		buffer.append("\n").append("waypoints=").append(waypointList).append("]");
		buffer.append("\n").append("limits=").append(limits).append("]");
		buffer.append("]");
		return buffer.toString();
	}

	private double adjustSailAngle(final GeoLocation boatLocation, final Waypoint waypoint) throws LocationNotInMap {
		final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
		double angle = boatLocation.angleTo(waypoint.getLocation());
		double apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
		SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());

		// - If angle is less that the AWD adjust it. Also adjust id speed < 0.0
		boolean search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
		int displacement = 1;
		if (apparentHeading > 0)
			displacement = -1;
		else
			displacement = 1;
		while (search) {
			angle += displacement;
			apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
			sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());
			search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
			if (displacement < 0)
				displacement--;
			else
				displacement++;
		}
		return angle;
	}

	private void pilotFollowRoute(final Boat boat) {
		// - Get the current boat location.
		final ExtendedLocation boatLocation = new ExtendedLocation(boat.getLocation());
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			// - Test if this waypoint has been surpassed.
			if (boatRef.isSurpassed(wp)) continue;

			// - Calculate distance to waypoint. Discard it if the distance is less than control.
			final GeoLocation waypointLocation = wp.getLocation();
			final double distance = waypointLocation.distance(boatLocation);
			if (distance < wp.getRange()) {
				System.out.println("--- Discarded waypoint " + waypointLocation + ". Distance " + distance + " below "
						+ wp.getRange() + " miles.");
				boatRef.surpassWaypoint(wp);
				continue;
			}

			System.out.println("\nActivating route to waypoint: " + wp + " - [distance= " + distance + "]");
			// - Depending on waypoint type use the code to generate the command.
			try {
				if (wp.getType() == CommandTypes.VMG) {
					final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
					final double angle = boatLocation.angleTo(wp.getLocation());
					// - Get the VMC from this start point to the end of the cell.
					final VMCData vmc = boatLocation.getVMC(windCell, angle);

					// - Check if current boat configuration is the same as requested.
					System.out.println();
					System.out.println(vmc.printReport());
					System.out.println();
					final BoatCommand newBoatCommand = new BoatCommand();
					newBoatCommand.setHeading(boat.getHeading(), vmc.getBestAngle());
					newBoatCommand.setSails(vmc.getBestSailConfiguration());
					newBoatCommand.sendCommand(boat);
					return;
				} else {
					// - Get the direction to the waypoint and adjust the course and polars.
					double angle = adjustSailAngle(boatLocation, wp);
					final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
					double apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
					SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());
					System.out.println();
					System.out.println("[Selected angle= " + Math.round(angle * 100.0) / 100.0 + " - AWD= "
							+ Math.abs(Math.round(apparentHeading)) + "]");
					System.out.println(sails);
					System.out.println();
					final BoatCommand newBoatCommand = new BoatCommand();
					newBoatCommand.setHeading(boat.getHeading(), new Double(Math.round(angle * 100.0) / 100.0).intValue());
					newBoatCommand.setSails(sails);
					newBoatCommand.sendCommand(boat);
					return;
				}
			} catch (final LocationNotInMap lnime) {
				System.out.println("EEE " + lnime.getMessage());
			}
		} /* END while */
	}

	private void pilotFollowVMG(final Boat boat) {
		// // - Check if the command is active or inactive by limits.
		// CommandStatus active = CommandStatus.NOCHANGE;
		// Iterator<PilotLimits> lit = limits.iterator();
		// while (lit.hasNext()) {
		// PilotLimits limit = lit.next();
		// CommandStatus test = limit.testLimit(boat.getLocation());
		// if (test != CommandStatus.NOCHANGE) active = test;
		// System.out.println("Testing limit " + limit + " with the result of: " + active);
		// }
		// if (active == CommandStatus.GO) {
		// - Get the current boat location
		final GeoLocation boatLocation = boat.getLocation();
		final Iterator<Waypoint> wit = waypointList.iterator();
		while (wit.hasNext()) {
			final Waypoint wp = wit.next();
			if (boatRef.isSurpassed(wp)) continue;

			// - Calculate distance to waypoint. If less than 80 miles discard it.
			final GeoLocation waypointLocation = wp.getLocation();
			final double distance = waypointLocation.distance(boatLocation);
			if (distance < wp.getRange()) {
				System.out.println("Waypoint " + waypointLocation + " discarded. Distance " + distance + " below 60 miles.");
				boatRef.surpassWaypoint(wp);
				continue;
			} else {
				System.out.println("\nActivating route to waypoint: " + wp);
				pilotMaxVMG(boat, waypointLocation);
				return;
			}
		}
	}

	private void pilotMaxVMG(final Boat boat, final GeoLocation destination) {
		// CommandStatus active = CommandStatus.NOCHANGE;
		// Iterator<PilotLimits> lit = limits.iterator();
		// while (lit.hasNext()) {
		// PilotLimits limit = lit.next();
		// CommandStatus test = limit.testLimit(boat.getLocation());
		// if (test != CommandStatus.NOCHANGE) active = test;
		// System.out.println("Testing limit " + limit + " with the result of: " + active);
		// }
		// if (active == CommandStatus.GO) {
		// - Get the wind cell of the current boat location.
		final VMCRouter route = new VMCRouter(destination);
		try {
			final VMCData vmc = route.getBestVMG(boat.getLocation());
			// - Check if current boat configuration is the same as requested.
			// System.out.println("VMG");
			System.out.println();
			System.out.println(vmc.printReport());
			System.out.println();
			// System.out.println(vmc.printRecord());
			final BoatCommand newBoatCommand = new BoatCommand();
			newBoatCommand.setHeading(boat.getHeading(), vmc.getBestAngle());
			newBoatCommand.setSails(vmc.getBestSailConfiguration());
			newBoatCommand.sendCommand(boat);
		} catch (final LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }
	}
}
// - UNUSED CODE ............................................................................................
