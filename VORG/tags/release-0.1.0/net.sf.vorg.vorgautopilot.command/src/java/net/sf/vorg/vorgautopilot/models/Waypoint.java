//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

import java.util.logging.Logger;

import net.sf.vorg.vorgautopilot.internals.CommandTypes;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class Waypoint extends PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger				= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static int				DEFAULT_RANGE	= 3;
	private static final int	DEFAULT_AWD		= 20;

	// - F I E L D - S E C T I O N ............................................................................
	private CommandTypes			type					= CommandTypes.DIRECT;
	private int								range;
	private int								AWD;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Waypoint(String type) {
		setType(type);
	}

	public boolean isEquivalent(Waypoint targetWaypoint) {
		return targetWaypoint.getLocation().isEquivalent(getLocation());
	}

	public int getRange() {
		return range;
	}

	public void setRange(String value) {
		if (null == value) {
			range = DEFAULT_RANGE;
			return;
		}
		try {
			range = new Integer(value).intValue();
		} catch (Exception e) {
			range = DEFAULT_RANGE;
		}
	}

	public CommandTypes getType() {
		return type;
	}

	public void setType(String newType) {
		type = CommandTypes.decodeType(newType);
		if (type == CommandTypes.VMG)
			DEFAULT_RANGE = 5;
		else
			DEFAULT_RANGE = 3;
	}

	public int getAWD() {
		return AWD;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Waypoint ");
		buffer.append(type).append(", ");
		buffer.append("range=").append(range).append(", ");
		buffer.append("AWD=").append(AWD).append(", ");
		buffer.append(getLocation()).append("");
		buffer.append("]");
		return buffer.toString();
	}

	public void setMinAWD(String value) {
		if (null == value) {
			AWD = DEFAULT_AWD;
			return;
		}
		try {
			AWD = new Integer(value).intValue();
		} catch (Exception e) {
			AWD = DEFAULT_AWD;
		}
	}
}
// - UNUSED CODE ............................................................................................
