//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Sails;
import net.sf.vorg.vorgautopilot.internals.CommandStatus;

// - CLASS IMPLEMENTATION ...................................................................................
public class Boat {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger							= Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	// - USER data.
	private String									email;
	private String									userid;
	private String									name;
	private String									password;
	private String									clef;

	// - BOAT data.
	private final GeoLocation				location						= new GeoLocation();
	private Sails										sail								= Sails.JIB;
	private double									speed								= 0.0;
	private double									windSpeed						= 0.0;
	private int											windAngle						= 0;
	private int											heading							= 0;
	// private String lastAccess;
	// private String lastPositiondate;

	// - COMMAND data.
	private Vector<PilotCommand>		commands						= new Vector<PilotCommand>();
	private final Vector<Waypoint>	surpassedWaypoints	= new Vector<Waypoint>();
	private final Vector<Boat>			friends							= new Vector<Boat>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Boat() {
	}

	public Boat(final String value) {
		userid = value;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addCommand(final PilotCommand command) {
		commands.add(command);
	}

	public void addFriend(final Boat newFriend) {
		friends.add(newFriend);
	}

	public void clearCommands() {
		commands = new Vector<PilotCommand>();
	}

	public String getClef() {
		return clef;
	}

	public String getEmail() {
		return email;
	}

	public int getHeading() {
		return heading;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public Sails getSail() {
		return sail;
	}

	public Object getSails() {
		// TODO Auto-generated method stub
		return null;
	}

	public double getSpeed() {
		return speed;
	}

	public String getUserid() {
		return userid;
	}

	public int getWindAngle() {
		return windAngle;
	}

	public double getWindSpeed() {
		return windSpeed;
	}

	public boolean isSurpassed(final Waypoint waypoint) {
		final Iterator<Waypoint> wit = surpassedWaypoints.iterator();
		while (wit.hasNext()) {
			final Waypoint testWaypoint = wit.next();
			if (waypoint.isEquivalent(testWaypoint)) return true;
		}
		return false;
	}

	/**
	 * Order the list of commands and process them in order. Also use a criteria to detect command
	 * incompatibilities.
	 */
	public void performPilot() {
		// - Scan the commands to test which of them are active.
		final Vector<PilotCommand> activeCommands = new Vector<PilotCommand>();
		final Iterator<PilotCommand> cit = commands.iterator();
		while (cit.hasNext()) {
			final PilotCommand command = cit.next();
			final CommandStatus status = command.testActivity();
			if (status == CommandStatus.GO) activeCommands.add(command);
		}

		// - Process only the first commend if some.
		final PilotCommand command = activeCommands.firstElement();
		if (null != command)
			command.pilot();
		else
			System.out.println("No commands active. Waiting for activation.");
	}

	public void setClef(final String key) {
		clef = key;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public void setHeading(final int heading) {
		this.heading = heading;
	}

	// public void setLastDateAccess(final String value) {
	// lastAccess = value;
	// }
	//
	// public void setLastPositionDate(final String value) {
	// lastPositiondate = value;
	// }

	public void setLatitude(final String latitude) {
		location.setLat(new Double(latitude).doubleValue());
	}

	public void setLongitude(final String longitude) {
		location.setLon(new Double(longitude).doubleValue());
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public void setSail(final Sails sail) {
		this.sail = sail;
	}

	public void setSail(final String sailCode) {
		final int code = new Integer(sailCode).intValue();
		if (code == 1) sail = Sails.SPI;
		if (code == 2) sail = Sails.JIB;
		if (code == 4) sail = Sails.JPLUS;
		if (code == 8) sail = Sails.GENOA;
		if (code == 16) sail = Sails.C0;
		if (code == 32) sail = Sails.LSPI;
		if (code == 64) sail = Sails.HSPI;
	}

	public void setSpeed(final double speed) {
		this.speed = speed;
	}

	public void setUserid(final String userid) {
		this.userid = userid;
	}

	public void setWindAngle(final int windAngle) {
		this.windAngle = windAngle;
	}

	public void setWindSpeed(final double windSpeed) {
		this.windSpeed = windSpeed;
	}

	public void surpassWaypoint(final Waypoint waypoint) {
		surpassedWaypoints.add(waypoint);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Boat ");
		buffer.append("name=").append(name).append(", \n");
		buffer.append("location=").append(location.toString()).append(", ");
		buffer.append("speed=").append(speed).append(", ");
		buffer.append("windSpeed=").append(windSpeed).append(", ");
		buffer.append("AWD=").append(windAngle).append(", ");
		buffer.append("heading=").append(heading).append("");
		// buffer.append("commands=").append(commands).append("]\n");
		buffer.append("]\n");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
