//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.internals;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGURLRequest /* extends URLRequest */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger		= Logger.getLogger("net.sf.vorg.vorgautopilot.internals");
	static {
		logger.setLevel(Level.OFF);
	}
	private static String			protocol	= "http";
	private static String			gameHost	= "volvogame.virtualregatta.com";
	// private String cookies = "";

	// - F I E L D - S E C T I O N ............................................................................
	private String						request		= "";
	private URL								url;
	private HttpURLConnection	conn;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VORGURLRequest(String request) {
		logger.info("Sent URL: " + protocol + "://" + gameHost + request);
		this.request = request;
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public void executeGET(String cookies) {
		try {
			url = new URL(protocol, gameHost, request);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
			conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
			conn.setRequestProperty("Cookie", cookies);

			conn.setUseCaches(false);
			conn.setDoInput(false);
			conn.setDoOutput(true);
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void excutePOST(String cookies, Hashtable<String, String> parameters) {
		try {
			// - Process the POST parameters and encode them for output.
			StringBuffer urlParameters = new StringBuffer();
			Enumeration<String> pit = parameters.keys();
			int paramCounter = 0;
			while (pit.hasMoreElements()) {
				String name = pit.nextElement();
				String value = parameters.get(name);
				if (paramCounter > 1) urlParameters.append('&');
				urlParameters.append(name).append(URLEncoder.encode(value, "UTF-8"));
				paramCounter++;
			}
			url = new URL(protocol, gameHost, request);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
			conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
			conn.setRequestProperty("Cookie", cookies);

			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			Writer post = new OutputStreamWriter(conn.getOutputStream());
			post.write(urlParameters.toString());
			post.write("\r\n");
			post.flush();
			// post.close();
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public String getData() {
		conn.setDoInput(true);
		StringBuffer data = new StringBuffer();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
				data.append(line).append("\n");

			// - Process the headers.
			String headerName = null;
			for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
				if (headerName.equals("Set-Cookie")) {
					String cookie = conn.getHeaderField(i);
					cookie = cookie.substring(0, cookie.indexOf(";"));
					String cookieName = cookie.substring(0, cookie.indexOf("="));
					String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());

				}

			in.close();
		} catch (MalformedURLException ex) {
			System.err.println(ex);
		} catch (FileNotFoundException ex) {
			System.err.println("Failed to open stream to URL: " + ex);
		} catch (IOException ex) {
			System.err.println("Error reading URL content: " + ex);
		}
		return data.toString();
	}

	// private void dumpOutput(URLConnection connection) {
	// StringBuffer data = new StringBuffer();
	// try {
	// Map<String, List<String>> headers = connection.getHeaderFields();
	// Iterator<String> it = headers.keySet().iterator();
	// while (it.hasNext()) {
	// String key = it.next();
	// System.out.println(key + ": " + headers.get(key));
	// }
	// System.out.println();
	// BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	// String line = null;
	// while ((line = in.readLine()) != null)
	// data.append(line).append("\n");
	// System.out.println();
	// System.out.println("Returned data:");
	// System.out.println(data.toString());
	// } catch (MalformedURLException ex) {
	// System.err.println(ex);
	// } catch (FileNotFoundException ex) {
	// System.err.println("Failed to open stream to URL: " + ex);
	// } catch (IOException ex) {
	// System.err.println("Error reading URL content: " + ex);
	// }
	// }
}

// - UNUSED CODE ............................................................................................
