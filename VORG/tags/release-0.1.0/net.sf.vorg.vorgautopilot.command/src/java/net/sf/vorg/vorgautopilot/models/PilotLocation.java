//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import net.sf.vorg.routecalculator.internals.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	location	= new GeoLocation();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public PilotLocation() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public void setLatitude(final String waypointData) {
		if (null == waypointData) {
			location.setLat(0.0);
			return;
		}
		// - Divide the data into the degree and the minutes.
		final int pos = waypointData.indexOf(":");
		// - The latitude comes in double format
		if (pos < 1)
			location.setLat(new Double(waypointData).doubleValue());
		else {
			final int degree = new Integer(waypointData.substring(0, pos)).intValue();
			final int minute = new Integer(waypointData.substring(pos + 1, waypointData.length())).intValue();
			location.setLat(degree * 1.0 + minute / 60.0);
		}
	}

	public void setLongitude(final String waypointData) {
		if (null == waypointData) {
			location.setLon(0.0);
			return;
		}
		// - Divide the data into the degree and the minutes.
		final int pos = waypointData.indexOf(":");
		// - The longitude comes in double format
		if (pos < 1)
			location.setLon(new Double(waypointData).doubleValue());
		else {
			final int degree = new Integer(waypointData.substring(0, pos)).intValue();
			final int minute = new Integer(waypointData.substring(pos + 1, waypointData.length())).intValue();
			location.setLon(degree * 1.0 + minute / 60.0);
		}
	}

	public GeoLocation getLocation() {
		return location;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[PilotLocation ");
		buffer.append(location);
		buffer.append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
