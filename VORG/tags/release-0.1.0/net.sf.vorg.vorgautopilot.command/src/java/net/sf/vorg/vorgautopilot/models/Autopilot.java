//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.sf.vorg.vorgautopilot.command.main.VORGAutopilot;
import net.sf.vorg.vorgautopilot.internals.VORGURLRequest;
import net.sf.vorg.vorgautopilot.parsers.BoatDataParserHandler;
import net.sf.vorg.vorgautopilot.parsers.PilotConfigurationParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class Autopilot extends Thread {
	private static final int			SLEEP_SECONDS					= 30;
	private static final int			SLEEP_MSECONDS				= SLEEP_SECONDS * 1000;

	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger								= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static boolean				terminationRequested	= false;
	private static boolean				connected							= false;

	// - F I E L D - S E C T I O N ............................................................................
	private final String					configFileName;
	private final Vector<String>	cookies								= new Vector<String>();
	private final Boat						boat									= new Boat();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Autopilot(final String configurationFileName) {
		configFileName = configurationFileName;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Runs the autopilot in an endless loop. There currently no way to control the stop of the process but on
	 * next releases I may access this over a telnet interface.
	 */
	@Override
	public void run() {
		super.run();

		// - Start the endless loop inside the thread.
		if (VORGAutopilot.getRefresh() != 10) {
			// int waitingMinute = 0; // Reset the interval detection to run for the first time
			long lastMinute = minuteOfDay() - 100;
			while (!terminationRequested) {
				// - Check if we have entered a new 10 minute interval
				// final Calendar now = Calendar.getInstance();
				// final int hour = now.get(Calendar.HOUR_OF_DAY);
				// final int minute = now.get(Calendar.MINUTE);
				long dayMinute = minuteOfDay();

				if ((dayMinute < lastMinute) || (dayMinute >= lastMinute + VORGAutopilot.getRefresh())) {
					lastMinute = dayMinute;

					// - Process current configuration. Read the configuration file to load any updates.
					System.out.println("--------------------------------------------------------------------------------");
					System.out.println(GregorianCalendar.getInstance().getTime() + " - new scan run for configuration: "
							+ configFileName);
					processConfiguration();
					// - Perform the login if we are not connected.
					// if (!connected) login(boat.getEmail(), boat.getPassword());
					// - Get current boat data
					getBoatData(cookies);
					// - Execute any of the boat commands
					boat.performPilot();
				}
				// - Sleep the process for a minute before checking again the waiting interval.
				try {
					Thread.sleep(Autopilot.SLEEP_MSECONDS);
				} catch (final InterruptedException ie) {
					System.out.println("Autopilot interrupted. Terminating current process.");
				}
			}
		} else {
			int lastMinute = -1; // Reset the interval detection to run for the first time
			while (!terminationRequested) {
				// - Check if we have entered a new 10 minute interval
				final Calendar now = Calendar.getInstance();
				final int minute = now.get(Calendar.MINUTE) / 10;
				if (minute != lastMinute) {
					lastMinute = minute;
					// - Process current configuration. Read the configuration file to load any updates.
					System.out.println("--------------------------------------------------------------------------------");
					System.out.println(GregorianCalendar.getInstance().getTime() + " - new scan run for configuration: "
							+ configFileName);
					processConfiguration();
					// - Perform the login if we are not connected.
					// if (!connected) login(boat.getEmail(), boat.getPassword());
					// - Get current boat data
					getBoatData(cookies);
					// - Execute any of the boat commands
					boat.performPilot();
				} else
					// - Sleep the process for a minute before checking again the ten minute period
					try {
						Thread.sleep(Autopilot.SLEEP_MSECONDS);
					} catch (final InterruptedException ie) {
						System.out.println("Autopilot interrupted. Terminating current process.");
					}
			}
		}
		System.out.println("Termination request acknowledged.");
	}

	private long minuteOfDay() {
		final Calendar now = Calendar.getInstance();
		final int hour = now.get(Calendar.HOUR_OF_DAY);
		final int minute = now.get(Calendar.MINUTE);
		return hour * 60 + minute;
	}

	private void login(String email, String password) {
		String protocol = "http";
		String gameHost = "www.volvooceanracegame.org";
		String request = "/ajax/ajax_login.php";
		Hashtable<String, String> parameters = new Hashtable<String, String>();
		parameters.put("email", "boneyvorg@orangemail.es");
		parameters.put("password", password);
		try {
			// - Process the POST parameters and encode them for output.
			StringBuffer urlParameters = new StringBuffer();
			Enumeration<String> pit = parameters.keys();
			int paramCounter = 0;
			while (pit.hasMoreElements()) {
				String name = pit.nextElement();
				String value = parameters.get(name);
				if (paramCounter > 0) urlParameters.append('&');
				urlParameters.append(name).append("=").append(URLEncoder.encode(value, "UTF-8"));
				paramCounter++;
			}
			URL url = new URL(protocol, gameHost, request);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
			conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
			// conn.setRequestProperty("Cookie", cookies);

			conn.setUseCaches(false);
			// conn.setDoInput(true);
			conn.setDoOutput(true);
			Writer post = new OutputStreamWriter(conn.getOutputStream());
			post.write(urlParameters.toString());
			post.write("\r\n");
			post.flush();
			// post.close();

			// conn.setDoInput(true);
			StringBuffer data = new StringBuffer();
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null;
				while ((line = in.readLine()) != null)
					data.append(line).append("\n");
			} catch (MalformedURLException ex) {
				System.err.println(ex);
			} catch (FileNotFoundException ex) {
				System.err.println("Failed to open stream to URL: " + ex);
			} catch (IOException ex) {
				System.err.println("Error reading URL content: " + ex);
			}
			// System.out.println(data.toString());
			// System.out.println(conn.getContent());
			String headerName = null;
			for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
				if (headerName.equals("Set-Cookie")) {
					String cookie = conn.getHeaderField(i);
					cookie = cookie.substring(0, cookie.indexOf(";"));
					String cookieName = cookie.substring(0, cookie.indexOf("="));
					String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
					if (cookieName.equals("userhash")) boat.setClef(cookieValue);
					cookies.add(cookieName + "=" + cookieValue);
				}
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}

	/**
	 * Goes to the game service and gets the boat information for the user configured. It will then parse the
	 * returned XML data to build some of the boat data resources.
	 * 
	 * @param cookies
	 */
	private void getBoatData(Vector<String> cookies) {
		final StringBuffer request = new StringBuffer("/get_user.php?");
		request.append("pseudo=" + boat.getName());
		request.append("&").append("id_user_searching=").append(boat.getUserid());

		final VORGURLRequest boatRequest = new VORGURLRequest(request.toString());
		// cit = cookies.iterator();
		StringBuffer cookies2 = new StringBuffer();
		cookies2.append("userid=" + boat.getUserid());
		cookies2.append("useremail=" + boat.getEmail());
		boatRequest.executeGET(cookies.toString());
		parseBoatData(boatRequest.getData());
		System.out.println();
		System.out.println(boat.toString());
	}

	// private boolean login(final String user, final String passw) {
	// final String protocol = "http";
	// final String host = "www.volvooceanracegame.org";
	// final String prefix = "/ajax/ajax_login.php?";
	// final String suffix = "";/* "?email=" + user + "&password=" + passw; */
	//
	// final Vector<String> post = new Vector<String>();
	// post.add("usermail" + "=" + user);
	// final URLRequest loginReq = new URLRequest();
	// try {
	// loginReq.connect(new URL(protocol, host, prefix + suffix), post);
	// } catch (final MalformedURLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// //[01]
	// return false;
	// }

	@SuppressWarnings("deprecation")
	private void parseBoatData(final String data) {
		// - Parse the configuration file.
		InputStream stream;
		try {
			stream = new BufferedInputStream(new StringBufferInputStream(data));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final BoatDataParserHandler handler = new BoatDataParserHandler(boat);
			parser.parse(stream, handler);
		} catch (final FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
	}

	// [02]

	/**
	 * Reads and parses the configuration XML file to get the identification information and the autopilot
	 * commands.
	 */
	private void processConfiguration() {
		// - Parse the configuration file.
		InputStream stream;
		try {
			stream = new BufferedInputStream(new FileInputStream(configFileName));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final PilotConfigurationParserHandler handler = new PilotConfigurationParserHandler(boat);
			// - Clear current boat commands before reading a new set.
			boat.clearCommands();
			parser.parse(stream, handler);
		} catch (final FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			// TODO Auto-generated catch block
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		}
	}

	public static void setTerminate(boolean state) {
		terminationRequested = state;
	}

	public static void disconnect() {
		connected = false;
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// URL loginRequest;
// HashMap<String, List<String>> responseHeaders = new HashMap<String, List<String>>();
// Vector<String> header = new Vector<String>();
// header.add(user);
// responseHeaders.put("usermail", header);
// try {
// loginRequest = new URL(protocol, host, prefix + suffix);
// URI loginURI = new URI(protocol + "//" + host + prefix);
// cookieMgr.put(loginURI, responseHeaders);
//
// // HttpURLConnection request = new HttpURLConnection(loginRequest);
// URLConnection conn = loginRequest.openConnection();
// // conn.se
// conn.setRequestProperty("email", user);
// conn.setRequestProperty("usermail", user);
// conn.setRequestProperty("password", passw);
// // boneysp%40orangemail.es
// Object data = conn.getContent();
// System.out.println(data);
// return true;
// } catch (MalformedURLException mue) {
// // TODO Auto-generated catch block
// mue.printStackTrace();
// } catch (IOException ioe) {
// // TODO Auto-generated catch block
// ioe.printStackTrace();
// } catch (URISyntaxException use) {
// // TODO Auto-generated catch block
// use.printStackTrace();
// }

// [02]
// private String setCookies() {
// StringBuffer cookies = new StringBuffer();
// // cookies.append("PHPSESSID=28b826787186d9334b58876cc730fd6b");
// cookies.append("userid=85128");
// cookies.append("useremail=boneysp%40orangemail.es");
// // cookies.append("userhash=d05b9cadda9c96cb6c8bffa0ac6a8ee9");
// return cookies.toString();
// }

// private void getBoatData2() {
// final String protocol = "http";
// final String host = "volvogame.virtualregatta.com";
// final String prefix = "/get_user.php";
// Vector<String> post = new Vector<String>();
// post.add("pseudo=" + "bocarte");
// post.add("id_user_searching=85128");
// String suffix = "?" + "pseudo=" + "Bocarte" + "id_user_searching=85128";
//
// String fix = "/get_user.php?pseudo=bocarte&id_user_searching=85128";
//
// URLRequest boatRequest = new URLRequest();
// String boatData;
// try {
// boatData = boatRequest.connect(new URL(protocol, host, fix), post);
// System.out.println(boatData);
// } catch (MalformedURLException e) {
// // TODO Auto-generated catch block
// e.printStackTrace();
// }
// }

// private void dumpOutput(URLConnection connection) {
// StringBuffer data = new StringBuffer();
// try {
// Map<String, List<String>> headers = connection.getHeaderFields();
// Iterator<String> it = headers.keySet().iterator();
// while (it.hasNext()) {
// String key = it.next();
// System.out.println(key + ": " + headers.get(key));
// }
// System.out.println();
// BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
// String line = null;
// while ((line = in.readLine()) != null)
// data.append(line).append("\n");
// System.out.println();
// System.out.println("Returned data:");
// System.out.println(data.toString());
// } catch (MalformedURLException ex) {
// System.err.println(ex);
// } catch (FileNotFoundException ex) {
// System.err.println("Failed to open stream to URL: " + ex);
// } catch (IOException ex) {
// System.err.println("Error reading URL content: " + ex);
// }
// }
