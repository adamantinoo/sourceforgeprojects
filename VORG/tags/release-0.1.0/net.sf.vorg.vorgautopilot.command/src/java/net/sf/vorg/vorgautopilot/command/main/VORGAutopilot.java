//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.command.main;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.vorgautopilot.models.Autopilot;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGAutopilot {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger								= Logger.getLogger("net.sf.vorg.vorgautopilot.command.main");
	static {
		logger.setLevel(Level.OFF);
	}
	private static final String		APPLICATIONNAME				= "VORGAutopilot";
	private static final String		VERSION								= "Version 1.11 $Revision: 174 $ [26 january 2009]";
	protected static final int		NOCONFIG							= 10;
	protected static int					INVALIDCONFIGURATION	= 11;
	private static VORGAutopilot	singleton;
	private static boolean				onDebug								= false;
	private static PrintWriter		printer;
	private static int						refresh								= 10;

	// - F I E L D - S E C T I O N ............................................................................
	private String								configurationFileName	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are:
	 * <ul>
	 * <li><b>-conf<font color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where
	 * the application will expect the configuration files and data.</li>
	 * <li><b>-res<font color="GREY">[ourcesLocation</font></b> ${RESOURCEDIR} - is the directory where the
	 * application is going to locate the files that contains the SQL statements and other application
	 * resources.
	 */
	public VORGAutopilot(String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		banner();

		// - Process parameters and store them into the instance fields
		processParameters(args, APPLICATIONNAME);
	}

	// - M A I N S E C T I O N
	public static void main(String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		singleton = new VORGAutopilot(args);
		singleton.execute();
		exit(0);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	private void banner() {
		System.out.println("__     _____  ____   ____    _         _              _ _       _   ");
		System.out.println("\\ \\   / / _ \\|  _ \\ / ___|  / \\  _   _| |_ ___  _ __ (_) | ___ | |_ ");
		System.out.println(" \\ \\ / / | | | |_) | |  _  / _ \\| | | | __/ _ \\| '_ \\| | |/ _ \\| __|");
		System.out.println("  \\ \\ /| |_| |  _ <| |_| |/ ___ \\ |_| | || (_) | |_) | | | (_) | |_ ");
		System.out.println("   \\_/  \\___/|_| \\_\\\\____/_/   \\_\\__,_|\\__\\___/| .__/|_|_|\\___/ \\__|");
		System.out.println("                                               |_|                  ");
		System.out.println();
		System.out.println(VERSION);
		System.out.println();
	}

	public void processParameters(String[] args, String ApplicationName) {
		// super.processParameters(args, ApplicationName);
		for (int i = 0; i < args.length; i++) {
			logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-conf")) {
					// - Get and open the file with the autopilot configuration
					configurationFileName = argumentStringValue(args, i);
					if (validateConfiguration(configurationFileName))
						continue;
					else
						exit(INVALIDCONFIGURATION);
				}
				if (args[i].startsWith("-refr")) { //$NON-NLS-1$
					VORGAutopilot.refresh = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-debug")) { //$NON-NLS-1$
					VORGAutopilot.onDebug = true;
					continue;
				}
			}
		}
		// ... Check that required parameters have values.
		if (null == configurationFileName) exit(NOCONFIG);
	}

	/**
	 * Checks if the configuration file exists and has content. It does not check the content format or the
	 * content structure, al least on this release
	 */
	private boolean validateConfiguration(String configurationFileName) {
		try {
			BufferedReader creader = new BufferedReader(new FileReader(configurationFileName));
			creader.close();
		} catch (FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
			return false;
		} catch (IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
			return false;
		}
		return true;
	}

	private double argumentDoubleValue(String[] args, int position) {
		// - Get the next argument.
		String argument = argumentStringValue(args, position);
		double value = new Double(argument).doubleValue();
		return value;
	}

	protected String argumentStringValue(String[] args, int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			exit(NOCONFIG);
		return "";
	}

	protected int argumentIntegerValue(String[] args, int position) {
		// - Get the next argument.
		String argument = argumentStringValue(args, position);
		int value = new Integer(argument).intValue();
		return value;
	}

	/**
	 * Starts a new thread with the autopilot configuration file. From that file the pilot gets all the date
	 * needed to run the boat.
	 */
	public void execute() {
		Autopilot pilot = new Autopilot(configurationFileName);
		pilot.run();
	}

	public static void exit(int exitCode) {
		System.exit(exitCode);
	}

	public static boolean onDebug() {
		return onDebug;
	}

	public static void output(String message) {
		System.out.println(message);
		if (null == printer)
			try {
				printer = new PrintWriter("VORGAutopilot.output.txt");
				printer.println(message);
				printer.flush();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			printer.println(message);
			printer.flush();
		}
	}

	public static int getRefresh() {
		return refresh;
	}
}

// - UNUSED CODE ............................................................................................
