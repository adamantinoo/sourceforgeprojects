//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.Quadrants;
import net.sf.vorg.routecalculator.internals.SailConfiguration;

//- CLASS IMPLEMENTATION ...................................................................................
public class RouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	// - F I E L D - S E C T I O N ............................................................................
	protected WindCell		cell;
	protected GeoLocation	entryLocation;
	protected GeoLocation	exitLocation;
	private double				deltaLat;
	private double				deltaLon;
	/* The distance is measured in minutes so it is not exact. */
	private double				distance;
	protected int					alpha;
	private Quadrants			quadrant;
	private int						apparentHeading;
	private double				ttc;
	private double				speed;
	private Object				sail;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteCell() {
	}

	public RouteCell(final WindCell routeCell, final GeoLocation entry, final GeoLocation exit) {
		this.cell = routeCell;
		this.entryLocation = entry;
		this.exitLocation = exit;

		this.calculateCell();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int calcApparent() {
		int angle = Math.abs(this.cell.getWindDir() - this.alpha);
		if (angle > 180) {
			angle = angle - 360;
			// angle = 180 - (this.cell.getWindDir() - (this.alpha + 180));
		}
		return Math.abs(angle);
	}

	public double calcElapsed() {
		return this.ttc;
	}

	// public RouteCell clone() {
	// return new RouteCell(this.cell, this.entryLocation, this.exitLocation);
	// }

	public int getAlpha() {
		return this.alpha;
	}

	public WindCell getCell() {
		return this.cell;
	}

	public double getDistance() {
		return this.distance * 60.0;
	}

	public GeoLocation getEntryLocation() {
		return this.entryLocation;
	}

	public GeoLocation getExitLocation() {
		return this.exitLocation;
	}

	public double getSpeed() {
		return this.speed;
	}

	public double getTTC() {
		return this.ttc;
	}

	public void setEntryLocation(final GeoLocation entryLocation) {
		this.entryLocation = entryLocation;
		this.calculateCell();
	}

	public void setExitLocation(final GeoLocation exitLocation) {
		this.exitLocation = exitLocation;
		this.calculateCell();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[RouteCell ");
		buffer.append("cell=").append(this.cell).append(",");
		buffer.append("alpha=").append(this.alpha).append(",");
		buffer.append("distance=").append(this.distance * 60.0).append(",");
		buffer.append("speed=").append(this.speed).append(",");
		buffer.append("ttc=").append(this.ttc).append("]");
		return buffer.toString();
	}

	public void calculateCell() {
		// - Calculate time to traverse this cell.
		// DEBUG This seems to be valid for Quadrant I locations
		this.deltaLat = this.exitLocation.getLat() - this.entryLocation.getLat();
		this.deltaLon = this.exitLocation.getLon() - this.entryLocation.getLon();
		// - If destination is the same as the origin the ttc is ZERO.
		if ((this.deltaLat == 0.0) & (this.deltaLon == 0.0)) {
			this.ttc = 0.0;
			return;
		}
		int dum = 1;
		// DEBUG Added for Quadrant 2 locations.
		// this.deltaLat = this.entryLocation.getLat() - this.exitLocation.getLat();
		// this.deltaLon = this.entryLocation.getLon() - this.exitLocation.getLon();

		this.distance = Math.hypot(this.deltaLat, this.deltaLon);
		this.alpha = GeoLocation
				.adjustAngle(new Long(Math.round(this.entryLocation.angleTo(this.exitLocation))).intValue());
		this.quadrant = Quadrants.q4Angle(this.alpha);
		this.apparentHeading = this.calcApparent();

		// TODO Get the sailt type and the speed from the Polars.
		SailConfiguration configuration = Polars.lookup(this.apparentHeading, this.cell.getWindSpeed());
		speed = configuration.getSpeed();
		sail = configuration.getSail();
		if (0.0 == this.speed) {
			this.ttc = Double.POSITIVE_INFINITY;
			// } else if (this.speed < 2.0) {
			// this.ttc = Double.POSITIVE_INFINITY;
		} else {
			this.ttc = this.distance * 60.0 / this.speed;
		}
	}

	public String printReport(int waypointId) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("W").append(waypointId).append('\t');
		buffer.append(exitLocation.toReport()).append('\t');
		buffer.append(exitLocation.getLat()).append('\t');
		buffer.append(exitLocation.getLon()).append('\t');
		buffer.append(alpha).append('\t');
		buffer.append(speed).append('\t');
		buffer.append(sail).append('\t');
		buffer.append(this.deltaLat).append('\t');
		buffer.append(deltaLon).append('\t');
		buffer.append(distance * 60.0).append('\t');
		buffer.append(ttc).append('\t');
		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir());
		return buffer.toString();
	}

	public String printStartReport() {
		final StringBuffer buffer = new StringBuffer("START").append('\t');
		buffer.append(entryLocation.toReport()).append("\t");
		buffer.append(entryLocation.getLat()).append('\t');
		buffer.append(entryLocation.getLon()).append('\t');

		buffer.append("\t\t\t\t\t\t\t");

		buffer.append(cell.getWindSpeed()).append('\t');
		buffer.append(cell.getWindDir());
		return buffer.toString();
	}

	public int getApparent() {
		return apparentHeading;
	}

	public double getWindSpeed() {
		return this.cell.getWindSpeed();
	}

	public int getWindDirection() {
		return cell.getWindDir();
	}

	public boolean updateWindCell(WindCell newCell) {
		if ((newCell.getWindDir() == this.cell.getWindDir()) && (newCell.getWindSpeed() == this.cell.getWindSpeed()))
			return false;
		else {
			cell = newCell;
			return true;
		}
	}
}

// - CLASS IMPLEMENTATION ...................................................................................
class RouterFormatter {

	// - M E T H O D - S E C T I O N ..........................................................................
	public static String rounded(double data, int round) {
		long target = Math.round(Math.pow(data, round));
		return Double.toString(target / Math.pow(10.0, round));
	}

	public static String toHMS(double data) {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(Math.floor(data)).append("H ");
		buffer.append(data - Math.floor(data)).append("M");
		return buffer.toString();
	}

}
// - UNUSED CODE ............................................................................................
