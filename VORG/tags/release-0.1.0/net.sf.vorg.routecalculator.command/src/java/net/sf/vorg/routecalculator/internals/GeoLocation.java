//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.internals");

	public static double adjustAngle(final double incomingAngle) {
		if (incomingAngle > 360.0) return GeoLocation.adjustAngle(incomingAngle - 360.0);
		if (incomingAngle < 0.0) return GeoLocation.adjustAngle(incomingAngle + 360.0);
		return incomingAngle;
	}

	public static int adjustAngle(final int incomingAngle) {
		if (incomingAngle == 0) return 360;
		if (incomingAngle > 360) return GeoLocation.adjustAngle(incomingAngle - 360);
		if (incomingAngle < 0) return GeoLocation.adjustAngle(incomingAngle + 360);
		return incomingAngle;
	}

	public static int calculateAWD(double windAngle, double boatAngle) {
		double angle = windAngle - boatAngle;
		if (angle > 180) {
			angle = angle - 360;
		}
		return new Long(Math.round(Math.abs(angle))).intValue();
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected double	latitude	= 0.0;
	protected double	longitude	= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GeoLocation() {
	}

	public GeoLocation(final double lat, final double lon) {
		latitude = lat;
		longitude = lon;
	}

	public GeoLocation(final int latGrade, final int latMinute, final int lonGrade, final int lonMinute) {
		latitude = latGrade * 1.0 + latMinute / 60.0;
		longitude = lonGrade * 1.0 + lonMinute / 60.0;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double angleTo(final GeoLocation destination) {
		double alpha = 0.0;
		final double deltaLat = destination.getLat() - this.getLat();
		final double deltaLon = destination.getLon() - this.getLon();
		final double hyp = Math.hypot(deltaLat, deltaLon);

		// - In the case the origin and destination are the same we return by default the angle 0.
		if ((deltaLat == 0.0) | (deltaLat == 0.0)) return 0.0;

		Quadrants quadrant = Quadrants.QUADRANT_I;
		// - Detect the angle quadrant.
		if ((deltaLat >= 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_I;
		if ((deltaLat < 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_II;
		if ((deltaLat < 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_III;
		if ((deltaLat >= 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_IV;

		switch (quadrant) {
			case QUADRANT_I:
				// -Use the bigger value to have a more exact result.
				if (deltaLon > deltaLat)
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_II:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.PI - Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_IV:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp) * -1.0;
				break;

			default:
				break;
		}

		return Math.toDegrees(alpha);
	}

	public double distance(final GeoLocation destination) {
		final double deltaLat = destination.getLat() - this.getLat();
		final double deltaLon = destination.getLon() - this.getLon();
		final double hyp = Math.hypot(deltaLat, deltaLon);
		return hyp * 60.0;
	}

	public double getLat() {
		return latitude;
	}

	public double getLon() {
		return longitude;
	}

	public boolean isEquivalent(final GeoLocation targetLocation) {
		if ((targetLocation.getLat() == this.getLat()) && (targetLocation.getLon() == this.getLon()))
			return true;
		else
			return false;
	}

	public void setLat(final double lat) {
		latitude = lat;
	}

	public void setLat(final String waypointData) {
		if (null == waypointData) {
			this.setLat(0.0);
			return;
		}
		// - Divide the data into the degree and the minutes.
		final int pos = waypointData.indexOf(":");
		// - The latitude comes in double format
		if (pos < 1)
			this.setLat(new Double(waypointData).doubleValue());
		else {
			final int degree = new Integer(waypointData.substring(0, pos)).intValue();
			final int minute = new Integer(waypointData.substring(pos + 1, waypointData.length())).intValue();
			this.setLat(degree * 1.0 + minute / 60.0);
		}
	}

	public void setLon(final double lon) {
		longitude = lon;
	}

	public void setLon(final String waypointData) {
		if (null == waypointData) {
			this.setLon(0.0);
			return;
		}
		// - Divide the data into the degree and the minutes.
		final int pos = waypointData.indexOf(":");
		// - The longitude comes in double format
		if (pos < 1)
			this.setLon(new Double(waypointData).doubleValue());
		else {
			final int degree = new Integer(waypointData.substring(0, pos)).intValue();
			final int minute = new Integer(waypointData.substring(pos + 1, waypointData.length())).intValue();
			this.setLon(degree * 1.0 + minute / 60.0);
		}
	}

	public String toReport() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append(Integer.toString(new Double(Math.floor(latitude)).intValue())).append("� ");
		buffer.append(Math.round((latitude - Math.floor(latitude)) * 60.0)).append("'");
		if (latitude > 0)
			buffer.append(" N");
		else
			buffer.append(" S");
		buffer.append('\t');
		buffer.append(Integer.toString(new Double(Math.floor(longitude)).intValue())).append("� ");
		buffer.append(Math.round((longitude - Math.floor(longitude)) * 60.0)).append("'");
		if (longitude > 0)
			buffer.append(" E");
		else
			buffer.append(" W");
		return buffer.toString();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[GeoLocation ");
		buffer.append("lat=").append(latitude).append(",");
		buffer.append("lon=").append(longitude).append("]");
		buffer.append("[").append(this.toReport().replace('\t', ' ')).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
