//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.command.main.VMCRouter;
import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Intersection;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.Quadrants;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.internals.Sails;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Class to calculate alternate routes that will use the VMC projection to a cell by cell updated line to
 * destination. For each cell visited the program will calculate the best course on the projected current line
 * calculating the VMC. At the next cell the start route point will be recalculated to adapt the line to
 * destination.
 */
public class VMCRoute extends Route {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected GeoLocation	start;
	protected GeoLocation	end;

	private RouteCell			currentCell;

	private VMCRouter			mapHandler;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VMCRoute() {
	}

	public VMCRoute(GeoLocation start, GeoLocation end) {
		this.start = start;
		this.end = end;
	}

	public VMCRoute(VMCRouter router) {
		this.mapHandler = router;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void generateTree(RouteCell newRouteCell) throws LocationNotInMap {
		this.route.add(newRouteCell);

		// - Generate two new routes for the best and worst paths.
		double elapsed = newRouteCell.getTTC();
		int seconds = new Double(elapsed * 3600.0).intValue();
		Calendar newNow = GregorianCalendar.getInstance();
		newNow.add(Calendar.HOUR, -2);
		newNow.add(Calendar.SECOND, seconds);
		WindCell newWindCell = getNextCell(newRouteCell.getExitLocation(), newRouteCell.getCell(), newRouteCell.getAlpha(),
				newNow);

		// - Entry is the exit point for this cell.
		// - Exit is the intersection with the cells walls.
		// - The cell is the new wind cell depending ontime.
		// RouteCell bestRouteCell = new RouteCell(startCell, start, intersection.getLocation());
		// VMCRoute bestRoute = new VMCRoute();
		// bestRoute.generateTree(bestRouteCell);

		// entry =

	}

	private WindCell getNextCell(GeoLocation depart, WindCell departWindCell, int direction, Calendar searchTime)
			throws LocationNotInMap {
		// - With both angles calculate the start direction of two routes to be evaluated.
		// - Extend this line to calculate intersections with the cell.
		double endLat = depart.getLat() + 10.0 * Math.cos(Math.toRadians(direction));
		double endLon = depart.getLon() + 10.0 * Math.sin(Math.toRadians(direction));
		GeoLocation endPoint = new GeoLocation(endLat, endLon);

		// - Buildup the route to the end from the start cell.
		// final Route directRoute = new Route();
		Vector<Intersection> intersections = this.calculateIntersection(depart, endPoint, departWindCell);

		// - Select the next cell on the intersection.
		Directions borderDirection = intersections.lastElement().getDirection();
		Intersection targetIntersection = intersections.lastElement();

		// - Locate the target cell and evaluate it.
		WindCell nextCell = mapHandler
				.cellWithPoint(targetIntersection.getLocation(), departWindCell, searchTime.getTime());
		return nextCell;
	}

	/**
	 * Adds a new wind cell to the Route. While the cell is added the new VMC is calculated and the cell exit
	 * point generated. This class does not use controls because the route is not optimized.
	 */
	@Override
	public void add(final WindCell windCell, final Directions direction, final GeoLocation entry, final GeoLocation exit) {
		this.start = entry;
		currentCell = new RouteCell(windCell, entry, exit);

		// - Calculate the best VMC from start to end. Use the already generated RouteCell
		int initialAlpha = currentCell.getAlpha();
		int initialApparent = currentCell.getApparent();
		double initialSpeed = currentCell.getSpeed();
		int windDirection = currentCell.getWindDirection();
		double windSpeed = currentCell.getWindSpeed();

		// - Scan port angles
		for (int deviation = -1; deviation < 61; deviation--) {
			int newAlpha = initialAlpha + deviation;
			int newApparent = calcApparent(windDirection, newAlpha);
			SailConfiguration configuration = Polars.lookup(newApparent, windSpeed);
			double speed = configuration.getSpeed();
			Sails sail = configuration.getSail();

			// - Calculate VMC by projecting to the start-end line.
			double vmcSpeed = speed * Math.cos(Math.toRadians(initialAlpha - newAlpha));

			if (vmcSpeed > initialSpeed) {
				// - This is a new and better speed. Calculate the exit point for the cell.
				this.end = calculateExit(newAlpha);
			}
		}
	}

	private GeoLocation calculateExit(int newAlpha) {
		final Boundaries boundaries = currentCell.getCell().getBoundaries();
		Quadrants quad = Quadrants.q4Angle(newAlpha);
		double lat;
		double lon;
		GeoLocation target = new GeoLocation(start.getLat() + 10.0 * Math.cos(newAlpha), start.getLon() + 10.0
				* Math.sin(newAlpha));
		Vector<Intersection> intersections = calculateIntersection(start, target, currentCell.getCell());
		Intersection endLocation = intersections.lastElement();
		return endLocation.getLocation();
	}

	public Vector<Intersection> calculateIntersection(final GeoLocation start, final GeoLocation end,
			final WindCell targetCell) {
		final Vector<Intersection> intersections = new Vector<Intersection>(4);
		// - Calculate the direct path line.
		double deltaLat = end.getLat() - start.getLat();
		double deltaLon = end.getLon() - start.getLon();
		Quadrants quad = Quadrants.q4Angle(start.angleTo(end));

		// - Calculate cell limits.
		final Boundaries boundaries = targetCell.getBoundaries();
		double lat;
		double lon;
		GeoLocation intersection;
		if (Quadrants.QUADRANT_I == quad) {
			// - Solve the line intersections with the direct line.
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.W));
			}
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.S));
			}
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.N));
			}
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.E));
			}
		} else {
			// - Solve the line intersections with the direct line.
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.W));
			}
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.N));
			}
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.S));
			}
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (targetCell.contains(intersection)) {
				intersections.add(new Intersection(intersection, Directions.E));
			}
		}
		return intersections;
	}

	public static int calcApparent(int windDir, int alpha) {
		int angle = Math.abs(windDir - alpha);
		if (angle > 180) {
			angle = 180 - (windDir - (alpha + 180));
		}
		return angle;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(this.route.toString()).append("]");
		buffer.append('\n');
		buffer.append("[").append(this.controls.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
