package net.sf.vorg.routecalculator.internals;

public enum RouteDirection {
	EAST_WEST, SOUTH_NORTH, WEST_EAST, NORTH_SOUTH;
}
