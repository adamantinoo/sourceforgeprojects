//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Intersection;
import net.sf.vorg.routecalculator.internals.Limits;
import net.sf.vorg.routecalculator.internals.Quadrants;

// - CLASS IMPLEMENTATION ...................................................................................
public class WindCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	location;
	private int								direction	= 0;
	private double						speed			= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/** Create a new cell from the latitude and longitude points. */
	public WindCell(final int latitude, final int longitude) {
		location = new GeoLocation(latitude, 0, longitude, 0);
	}

	public WindCell(final int latitude, final int longitude, final int windDirection, final double windSpeed) {
		location = new GeoLocation(latitude, 0, longitude, 0);
		direction = windDirection;
		speed = windSpeed;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Vector<Intersection> calculateIntersection(final GeoLocation start, final GeoLocation end)
			throws LocationNotInMap {
		final Vector<Intersection> intersections = new Vector<Intersection>(4);

		// - Calculate the direct path line.
		final double deltaLat = end.getLat() - start.getLat();
		final double deltaLon = end.getLon() - start.getLon();
		final Quadrants quad = Quadrants.q4Angle(start.angleTo(end));

		// - Calculate cell limits.
		final Boundaries boundaries = this.getBoundaries();
		double lat;
		double lon;
		GeoLocation intersection;
		if (Quadrants.QUADRANT_I == quad) {
			// - Solve the line intersections with the direct line.
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.E));
		}
		if (Quadrants.QUADRANT_II == quad) {
			// - Solve the line intersections with the direct line.
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.E));
		}
		if (Quadrants.QUADRANT_III == quad) {
			// - Solve the line intersections with the direct line.
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.N));
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.E));
		}
		if (Quadrants.QUADRANT_IV == quad) {
			// - Solve the line intersections with the direct line.
			lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getSouth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.S));
			lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getEast());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.E));
			lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
			intersection = new GeoLocation(lat, boundaries.getWest());
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.W));
			lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
			intersection = new GeoLocation(boundaries.getNorth(), lon);
			if (this.contains(intersection)) intersections.add(new Intersection(intersection, Directions.N));
		}
		return intersections;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public boolean isEquivalent(WindCell target) {
		return this.location.isEquivalent(target.location);
	}

	public boolean contains(final GeoLocation intersection) {
		return this.doesContain(intersection);
	}

	/**
	 * Check if this cell contains the parameter location. This test is done checking if the location has a
	 * latitude between the cell latitudes and the longitude is also between the cell longitudes.
	 */
	public boolean doesContain(final GeoLocation start) {
		boolean contained = false;
		// - Calculate min and max latitudes for this cell.
		final double minLat = location.getLat() - 0.5;
		final double maxLat = location.getLat() + .5;
		if ((minLat <= start.getLat()) & (maxLat >= start.getLat())) {
			// -Calculate min and max longitudes to perform the same test.
			final double minLon = location.getLon() - .5;
			final double maxLon = location.getLon() + .5;
			if ((minLon <= start.getLon()) & (maxLon >= start.getLon())) contained = true;
		}
		return contained;
	}

	public Boundaries getBoundaries() {
		final Boundaries limits = new Boundaries();
		limits.setNorth(location.getLat() + .5);
		limits.setSouth(location.getLat() - .5);
		limits.setWest(location.getLon() - .5);
		limits.setEast(location.getLon() + .5);
		return limits;
	}

	/** Return the south and north limits for this cell. */
	public Limits getCeiling() {
		return new Limits(Limits.NORTH_SOUTH, location.getLat() - .5, location.getLat() + .5);
	}

	/** Return the west and east limits for this cell. */
	public Limits getWalls() {
		return new Limits(Limits.EAST_WEST, location.getLon() - .5, location.getLon() + .5);
	}

	public int getWindDir() {
		return direction;
	}

	public double getWindSpeed() {
		return speed;
	}

	// public void interception() {
	// }
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[WindCell ");
		buffer.append("direction=").append(direction).append(",");
		buffer.append("speed=").append(speed).append(",");
		buffer.append("location=").append(location.toString()).append("]");
		return buffer.toString();
	}

	// public void setMapReference(WindMapHandler windMapHandler) {
	// mapReference = windMapHandler;
	// }
	//
	// public WindMapHandler getMapReference() {
	// return mapReference;
	// }
}
// - UNUSED CODE ............................................................................................
