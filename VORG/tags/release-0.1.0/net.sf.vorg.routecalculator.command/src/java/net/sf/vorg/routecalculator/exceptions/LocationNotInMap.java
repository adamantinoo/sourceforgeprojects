package net.sf.vorg.routecalculator.exceptions;

public class LocationNotInMap extends Exception {
	private static final long	serialVersionUID	= 4187091156559130989L;
	private final String			message;

	public LocationNotInMap(String exceptionMessage) {
		this.message = exceptionMessage;
	}

	@Override
	public String getMessage() {
		return this.message;
	}
}
