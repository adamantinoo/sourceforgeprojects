//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.parsers;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.sf.vorg.routecalculator.models.ExtendedRoute;
import net.sf.vorg.routecalculator.models.Router;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteParserHandler extends DefaultHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger	= Logger.getLogger("net.sf.vorg.routecalculator.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	private final Router				routerRef;
	private final ExtendedRoute	route;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public RouteParserHandler(Router router, ExtendedRoute directRoute) {
		this.routerRef = router;
		this.route = directRoute;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(final String uri, final String localName, final String name, final Attributes attributes)
			throws SAXException {
		// - Store the information for the user account for login purposes
		if (name.toLowerCase().equals("route")) {
			route.setName(attributes.getValue("name"));
			route.setDeep(attributes.getValue("deep"));
			route.setIterations(attributes.getValue("iterations"));
		}
		if (name.toLowerCase().equals("waypointlist")) routerRef.startElement(name, attributes, route);
		if (name.toLowerCase().equals("waypoint")) routerRef.startElement(name, attributes, route);
	}
}

// - UNUSED CODE ............................................................................................
