//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Intersection;

// - CLASS IMPLEMENTATION ...................................................................................
public class ExtendedRoute extends Route {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");
	private static final double	TOMINUTES	= 30.0;

	// - F I E L D - S E C T I O N ............................................................................
	private String							name;
	private GeoLocation					startLocation;
	private GeoLocation					currentLocation;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ExtendedRoute() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void add(final WindCell routeCell, final Directions direction, final GeoLocation entry, final GeoLocation exit) {
		// - Before adding the cell, check that is not already on the list.
		boolean found = false;
		Iterator<RouteCell> rcit = this.route.iterator();
		while (rcit.hasNext()) {
			RouteCell testCell = rcit.next();
			if (testCell.getCell().isEquivalent(routeCell)) {
				found = true;
				break;
			}
		}
		if (found) return;
		super.add(routeCell, direction, entry, exit);
	}

	private void add(RouteCell newCell) {
		// - Before adding the cell, check that is not already on the list.
		boolean found = false;
		Iterator<RouteCell> rcit = this.route.iterator();
		while (rcit.hasNext()) {
			RouteCell testCell = rcit.next();
			if (testCell.getCell().isEquivalent(newCell.getCell())) {
				found = true;
				break;
			}
		}
		if (found)
			return;
		else {
			if (this.creationState == RouteState.EMPTY) {
				this.route.add(newCell);
				this.leftNode = newCell;
				startNode = newCell;
				this.creationState = RouteState.MIDDLE;
				return;
			}
			if (this.creationState == RouteState.MIDDLE) {
				this.route.add(newCell);
				// FIXME I have to solve how to identify the direction of the controls.
				// this.controls.add(new RouteControl(newCell.getEntryLocation(), undefined, this.leftNode, newCell));
				savedConfiguration.add(new GeoLocation(0.0, 0.0));
				this.leftNode = newCell;
				return;
			}
			// endNode = newCell;
		}
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public void setDeep(String value) {
		// TODO Auto-generated method stub

	}

	public void setIterations(String value) {
		// TODO Auto-generated method stub

	}

	// public void startElement(String name, Attributes attributes) {
	// // if (name.toLowerCase().equals("waypointlist")) waypointList = new Vector<Waypoint>();
	// if (name.toLowerCase().equals("waypoint")) {
	// Waypoint waypoint = new Waypoint();
	// waypoint.setLat(attributes.getValue("waypointlatitude"));
	// waypoint.setLongitude(attributes.getValue("waypointlongitude"));
	// waypointList.add(waypoint);
	// }
	// }
	/**
	 * The creation of a route from a set of multiple waypoints is now a process that is performed inside the
	 * Route. The data comes in GeoLocations that have to be connected to detect the cells that are going to be
	 * part of the route and the entry and exit points for that cells. It will also deal with duplicated points
	 * and with points that are not bounded to a border with the exception of the initial and last route points.
	 */
	public void addWaypoint(GeoLocation waypoint) {
		// - Check if this is the first point being received.
		if (null == this.startLocation) {
			startLocation = waypoint;
			currentLocation = waypoint;
		} else {
			// - This is not the first point so connect the two points and create a route between them
			try {
				Route newLeg = generateRoute(currentLocation, waypoint);
				// - Add current route cell to this route.
				Iterator<RouteCell> rcit = newLeg.route.iterator();
				while (rcit.hasNext()) {
					RouteCell newCell = rcit.next();
					this.add(newCell);
				}
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Calendar getSearchTime() {
		Calendar now = GregorianCalendar.getInstance();
		now.add(Calendar.HOUR, -2); // Adjust for the two hour difference with wind cell stamp time
		int elapsed = new Double(this.elapsed() * TOMINUTES).intValue();
		now.add(Calendar.MINUTE, elapsed);
		return now;
	}

	protected Route generateRoute(GeoLocation start, GeoLocation end) throws LocationNotInMap {
		// - The current search time is the current time plus the elapsed route time.
		Calendar now = getSearchTime();

		// - Locate the wind cells that contains the start and end locations.
		WindMapHandler.loadWinds(start);
		final WindCell startCell = WindMapHandler.cellWithPoint(start, now.getTime());
		WindCell endCell = WindMapHandler.cellWithPoint(end, now.getTime()); // Get the cell now but recalculate
		// later.

		// DEBUG Check that this works even for end points inside the same cell.
		// if(endCell.isEquivalent(startCell))

		// - Buildup the route to the end from the start cell.
		Route directRoute = new Route();

		Vector<Intersection> intersections = startCell.calculateIntersection(start, end);
		this.add(startCell, intersections.lastElement().getDirection(), start, intersections.lastElement().getLocation());
		// RouteCell routeElement = directRoute.getLast();
		// System.out.println("Adding cell to route: " + routeElement.toString());
		// elapsed += routeElement.calcElapsed();
		// Date newTime = addElapsed(now, elapsed);

		// - Locate the wind cells that contains the start and end locations.
		// Date now = GregorianCalendar.getInstance().getTime();
		// now = addElapsed(now, -2.0);
		// double elapsed = 0.0;
		// final WindCell startCell = this.cellWithPoint(route, now);
		// // - Get the cell now but recalculate later.
		// WindCell endCell = this.cellWithPoint(endLocation, now);
		//
		// // - Buildup the route to the end from the start cell.
		// final Route directRoute = new Route();
		// Vector<Intersection> intersections = this.calculateIntersection(route, endLocation, startCell);
		// directRoute.add(startCell, intersections.lastElement().getDirection(), route,
		// intersections.lastElement()
		// .getLocation());
		// RouteCell routeElement = directRoute.getLast();
		// System.out.println("Adding cell to route: " + routeElement.toString());
		// elapsed += routeElement.calcElapsed();
		// Date newTime = addElapsed(now, elapsed);
		// Calendar newNow = GregorianCalendar.getInstance();
		// newNow.setTimeInMillis(new Double(now.getTime() + elapsed * 60 * 60 * 1000).longValue());
		// Date newTime = newNow.getTime();

		// - Get cell one by one following the direct path line.
		// To get the next cell search the one that contains the end intersection.
		now = getSearchTime();
		WindCell nextCell = WindMapHandler.cellWithPoint(intersections.lastElement().getLocation(), startCell, now
				.getTime());
		// - The end cell is never reached because the cell maps change over time
		while (!nextCell.isEquivalent(endCell)) {
			intersections = nextCell.calculateIntersection(start, end);
			directRoute.add(nextCell, intersections.firstElement().getDirection(),
					intersections.firstElement().getLocation(), intersections.lastElement().getLocation());
			now = getSearchTime();
			nextCell = WindMapHandler.cellWithPoint(intersections.lastElement().getLocation(), nextCell, now.getTime());
			endCell = WindMapHandler.cellWithPoint(end, now.getTime());
		}

		now = getSearchTime();
		endCell = WindMapHandler.cellWithPoint(end, now.getTime());

		intersections = endCell.calculateIntersection(start, end);
		directRoute.add(endCell, intersections.firstElement().getDirection(), intersections.firstElement().getLocation(),
				end);

		return directRoute;
	}
}

// - UNUSED CODE ............................................................................................
