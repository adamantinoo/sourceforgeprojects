//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.command.main;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Intersection;
import net.sf.vorg.routecalculator.models.RouteCell;
import net.sf.vorg.routecalculator.models.Router;
import net.sf.vorg.routecalculator.models.VMCData;
import net.sf.vorg.routecalculator.models.WindCell;

// - CLASS IMPLEMENTATION ...................................................................................
public class VMCRouter extends Router {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger			logger	= Logger.getLogger("net.sf.vorg.routecalculator.command.main");

	// - F I E L D - S E C T I O N ............................................................................
	private final GeoLocation	waypoint;
	private int								span;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VMCRouter(GeoLocation waypoint, int cellsToSpan) {
		this.waypoint = waypoint;
		this.span = cellsToSpan;
	}

	public VMCRouter(GeoLocation waypoint) {
		this.waypoint = waypoint;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public VMCData getBestVMG(GeoLocation initialLocation) throws LocationNotInMap {
		Calendar now = GregorianCalendar.getInstance();
		now.add(Calendar.HOUR, -2);
		loadWinds(initialLocation);
		WindCell startCell = this.cellWithPoint(initialLocation, now.getTime());

		// - Calculate the direction now to the waypoint.
		double direction = initialLocation.angleTo(waypoint);

		// - Get the VMC from this start point to the end of the cell.
		ExtendedLocation extended = new ExtendedLocation(initialLocation.getLat(), initialLocation.getLon());
		VMCData vmc = extended.getVMC(startCell, direction);
		return vmc;
	}

	// public WindCell getWindCell(GeoLocation location) throws LocationNotInMap {
	// Calendar now = GregorianCalendar.getInstance();
	// now.add(Calendar.HOUR, -2);
	// loadWinds(location);
	// return this.cellWithPoint(location, now.getTime());
	// }

	public void getBestRoute(GeoLocation initialLocation) throws LocationNotInMap {
		if (span == 0) return;
		// - Get the cell for the initial location and calculate the VMC directions.
		StringBuffer routeBuffer = new StringBuffer("\n");

		Calendar now = GregorianCalendar.getInstance();
		now.add(Calendar.HOUR, -2);
		loadWinds(initialLocation);
		WindCell startCell = this.cellWithPoint(initialLocation, now.getTime());

		// - Calculate the direction now to the waypoint.
		double direction = initialLocation.angleTo(waypoint);

		// - Get the VMC from this start point to the end of the cell.
		ExtendedLocation extended = new ExtendedLocation(initialLocation.getLat(), initialLocation.getLon());
		VMCData vmc = extended.getVMC(startCell, direction);
		StringBuffer buffer = new StringBuffer("\n[VMC Cell calculation").append('\n');
		buffer.append("Start cell: ").append(initialLocation.toReport()).append("\n");
		buffer.append(vmc.printReport()).append("\n");
		buffer.append(vmc.printRecord()).append("\n");

		int bestAngle = vmc.getBestAngle();
		Intersection intersection = getIntersection(initialLocation, startCell, bestAngle, now);
		// GeoLocation bestLocation = intersection.getLocation();
		// if (intersection.getDirection().equals(Directions.N)) {
		// bestLocation.incrementLat(0.01);
		// }
		// if (intersection.getDirection().equals(Directions.E)) {
		// bestLocation.incrementLon(0.01);
		// }
		RouteCell bestRouteCell = new RouteCell(startCell, initialLocation, intersection.getLocation());
		routeBuffer.append(bestRouteCell.printStartReport()).append("\n");
		routeBuffer.append(bestRouteCell.printReport(1)).append("\n");
		routeBuffer.append("\n");
		int seconds = new Double(bestRouteCell.getTTC()).intValue();
		double bestDirection = intersection.getLocation().angleTo(waypoint);
		Calendar bestTime = GregorianCalendar.getInstance();
		bestTime.add(Calendar.HOUR, -2);
		bestTime.add(Calendar.SECOND, seconds);
		WindCell bestWindCell = this.cellWithPoint(intersection.getLocation(), startCell, bestTime.getTime());
		VMCData bestVMC = new VMCData(bestDirection, bestWindCell);
		buffer.append("----------------").append("\n");
		// buffer.append("Cell timing=").append(bestRouteCell.getTTC()).append("\n");
		// buffer.append("\n");
		buffer.append("VMC for best route: ").append(intersection.getLocation().toReport()).append("\n");
		buffer.append("Best route intercept direction=").append(intersection.getDirection()).append("\n");
		buffer.append(bestVMC.printReport()).append("\n");
		buffer.append(bestVMC.printRecord()).append("\n");
		int dummy = 1;
		// - The entry is the start position
		// - The exit is the intersection with the cell border,
		// - The cell is evident.
		// VMCRoute bestRoute = new VMCRoute(this);
		// bestRoute.generateTree(bestRouteCell);

		int worstAngle = vmc.getWorstAngle();
		intersection = getIntersection(initialLocation, startCell, worstAngle, now);
		// GeoLocation worstLocation = intersection.getLocation();
		// if (intersection.getDirection().equals(Directions.N)) {
		// worstLocation.incrementLat(0.01);
		// }
		// if (intersection.getDirection().equals(Directions.E)) {
		// worstLocation.incrementLon(0.01);
		// }
		RouteCell worstRouteCell = new RouteCell(startCell, initialLocation, intersection.getLocation());
		routeBuffer.append(bestRouteCell.printStartReport()).append("\n");
		routeBuffer.append(worstRouteCell.printReport(1)).append("\n");
		seconds = new Double(bestRouteCell.getTTC()).intValue();
		double worstDirection = intersection.getLocation().angleTo(waypoint);
		Calendar worstTime = GregorianCalendar.getInstance();
		worstTime.add(Calendar.HOUR, -2);
		bestTime.add(Calendar.SECOND, seconds);
		WindCell worstWindCell = this.cellWithPoint(intersection.getLocation(), startCell, worstTime.getTime());
		VMCData worstVMC = new VMCData(worstDirection, worstWindCell);
		// buffer.append("Cell timing=").append(worstRouteCell.getTTC()).append("\n");
		// buffer.append("\n");
		buffer.append("VMC for worst route: ").append(intersection.getLocation().toReport()).append("\n");
		buffer.append("Worst route intercept direction=").append(intersection.getDirection()).append("\n");
		buffer.append(worstVMC.printReport()).append("\n");
		buffer.append(worstVMC.printRecord()).append("\n");
		// VMCRoute worstRoute = new VMCRoute(this);
		// worstRoute.generateTree(worstRouteCell);
		System.out.println(buffer.toString());
		System.out.println(routeBuffer.toString());

		// - Do this for a new level of cells.
		span--;
		// getBestRoute(bestLocation);
		// getBestRoute(worstLocation);
	}

	private Intersection getIntersection(GeoLocation startLocation, WindCell startWindCell, int direction,
			Calendar searchTime) throws LocationNotInMap {
		// - With both angles calculate the start direction of two routes to be evaluated.
		// - Extend this line to calculate intersections with the cell.
		double endLat = startLocation.getLat() + 10.0 * Math.cos(Math.toRadians(direction));
		double endLon = startLocation.getLon() + 10.0 * Math.sin(Math.toRadians(direction));
		GeoLocation endPoint = new GeoLocation(endLat, endLon);

		// - Buildup the route to the end from the start cell.
		// final Route directRoute = new Route();
		Vector<Intersection> intersections = this.calculateIntersection(startLocation, endPoint, startWindCell);
		return intersections.lastElement();
	}

}

// - UNUSED CODE ............................................................................................
