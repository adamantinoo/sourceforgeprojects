package net.sf.vorg.routecalculator.internals;

public enum Sails {
	JIB, SPI, JPLUS, GENOA, C0, LSPI, HSPI;

	public static Sails minSail(Sails lowSail, Sails highSail) {
		int comparison = lowSail.compareTo(highSail);
		if (comparison < 0) return lowSail;
		if (comparison > 0) return highSail;
		return lowSail;
	}

	public static int encodeSail(Sails sail) {
		if (sail == JIB) return 1;
		if (sail == SPI) return 2;
		if (sail == JPLUS) return 4;
		if (sail == GENOA) return 8;
		if (sail == C0) return 16;
		if (sail == LSPI) return 32;
		if (sail == HSPI) return 64;
		return 1;
	}
}
