//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.internals.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class Map {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger			= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation		boat				= new GeoLocation();
	private GeoLocation		destination	= new GeoLocation();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Map() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setBoatPosition(GeoLocation boat) {
		this.boat = boat;
	}

	public void setDestination(GeoLocation dest) {
		destination = dest;
	}

	public double getDestinationLon() {
		return destination.getLongitude();
	}

	public double getDestinationLat() {
		return destination.getLatitude();
	}

	public double getBoatLon() {
		return boat.getLongitude();
	}

	public double getBoatLat() {
		return boat.getLatitude();
	}
}

// - UNUSED CODE ............................................................................................
