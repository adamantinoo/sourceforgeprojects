//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

import java.util.Vector;

import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Limits;

public class RouteControl {
	protected final GeoLocation		location;
	protected Directions					direction						= Directions.N;
	public RouteCell							left;
	protected RouteCell						right;
	protected GeoLocation					directLocation;
	protected GeoLocation					optimizedLocation;
	protected Vector<GeoLocation>	savedConfiguration	= new Vector<GeoLocation>(32);

	public RouteControl(final GeoLocation location, final Directions direction) {
		this.location = location;
		this.directLocation = location;
		this.optimizedLocation = location;
		this.direction = direction;
		for (int index = 0; index < 32; index++) {
			savedConfiguration.add(new GeoLocation(0.0, 0.0));
		}
	}

	/** Creates a new control that connect two wind cells, setting a direction and a control location. */
	// public RouteControl(final GeoLocation location, final RouteCell leftNode, final RouteCell rightNode) {
	// this.location = location;
	// // this.direction = direction;
	// this.left = leftNode;
	// this.right = rightNode;
	// }
	public RouteControl(final GeoLocation location, final Directions direction, final RouteCell leftNode,
			final RouteCell rightNode) {
		this.location = location;
		this.directLocation = location;
		this.optimizedLocation = location;
		this.direction = direction;
		this.left = leftNode;
		this.right = rightNode;
		for (int index = 0; index < 32; index++) {
			savedConfiguration.add(new GeoLocation(0.0, 0.0));
		}
	}

	public Vector<GeoLocation> getSavedConfiguration() {
		return savedConfiguration;
	}

	public GeoLocation getDirectLocation() {
		return directLocation;
	}

	public GeoLocation getOptimizedLocation() {
		return optimizedLocation;
	}

	@Override
	public RouteControl clone() {
		return new RouteControl(this.location, this.direction, this.left, this.right);
	}

	public Directions getDirection() {
		return this.direction;
	}

	public double getElapsed() {
		return this.left.getTTC() + this.right.getTTC();
	}

	public double getFixedLat() {
		return this.left.getExitLocation().getLat();
	}

	public double getFixedLon() {
		return this.left.getExitLocation().getLon();
	}

	public Limits getLimits() {
		if ((Directions.E == this.direction) | (Directions.W == this.direction)) return this.left.getCell().getCeiling();
		if ((Directions.N == this.direction) | (Directions.S == this.direction)) return this.left.getCell().getWalls();
		return this.left.getCell().getCeiling();
	}

	public String printOptimizedData() {
		final StringBuffer buffer = new StringBuffer("[Optim ");
		buffer.append("location=").append(this.optimizedLocation).append("]");
		return buffer.toString();
	}

	public String printReport(final boolean start) {
		// - Update cell calculation before printing the data.
		this.updateLeft(this.optimizedLocation);
		this.updateRight(this.optimizedLocation);
		final StringBuffer buffer = new StringBuffer();
		if (start) {
			buffer.append("START").append('\t');
			buffer.append(this.left.entryLocation.toReport()).append('\n');
		}
		buffer.append("W-").append(this.direction).append('\t');
		buffer.append(this.left.exitLocation.toReport()).append('\t');
		buffer.append("alpha ").append(this.left.getAlpha()).append('\t');
		buffer.append("speed ").append(this.left.getSpeed()).append('\t');
		buffer.append("distance ").append(this.left.getDistance()).append('\t');
		buffer.append("ETA ").append(this.left.getTTC()).append('\n');
		return buffer.toString();
	}

	public void setOptimizedLocation(final GeoLocation newLocation) {
		this.optimizedLocation = newLocation;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[RouteControl ");
		buffer.append("location=").append(this.location.toString()).append(",");
		buffer.append("left=").append(this.left.toString()).append(",");
		buffer.append("right=").append(this.right.toString()).append(",");
		buffer.append("optimization=").append(this.optimizedLocation).append("]");
		return buffer.toString();
	}

	public double updateLeft(final GeoLocation newLocation) {
		this.left.setExitLocation(newLocation);
		return this.left.getTTC();
	}

	public double updateRight(final GeoLocation newLocation) {
		this.right.setEntryLocation(newLocation);
		return this.right.getTTC();
	}

	public void reset() {
		this.optimizedLocation = directLocation;
	}

	public void saveControlConfiguration(int controlId, Vector<RouteControl> controls) {
		for (int index = controlId; index < controls.size(); index++) {
			savedConfiguration.set(index, controls.get(index).getOptimizedLocation());
		}
	}

	public void updateLocation(GeoLocation newLocation) {
		this.left.setExitLocation(newLocation);
		this.right.setEntryLocation(newLocation);
		directLocation = newLocation;
	}

	public double getLeftTTC() {
		return this.left.getTTC();
	}

	public double getRightTTC() {
		return this.right.getTTC();
	}

	public void saveControlConfiguration(GeoLocation newLocation, int controlId, Vector<RouteControl> controls) {
		// - Get control saved on next level.
		try {
			RouteControl nextControl = controls.get(controlId + 1);
			for (int index = controlId; index < controls.size(); index++) {
				savedConfiguration.set(index, nextControl.savedConfiguration.get(index));
			}
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			// - Save configuration of last control.
			savedConfiguration.set(controlId, newLocation);
		} finally {
			savedConfiguration.set(controlId, newLocation);
		}
	}

}
