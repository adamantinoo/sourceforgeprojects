//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;


// - CLASS IMPLEMENTATION ...................................................................................
public class SailConfiguration {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................
	protected double			speed;
	protected Sails				sail;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SailConfiguration(final Sails sail, final double newSpeed) {
		this.sail = sail;
		this.speed = newSpeed;
	}

	public Sails getSail() {
		return this.sail;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public double getSpeed() {
		return this.speed;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[SailConfiguration ");
		buffer.append(sail).append(",");
		buffer.append("speed=").append(speed).append("]");
		return buffer.toString();
	}

	public void setSail(final Sails sail) {
		this.sail = sail;
	}

	public void setSpeed(final double speed) {
		this.speed = speed;
	}

}
// - UNUSED CODE ............................................................................................
