//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

//- IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import net.sf.vorg.routecalculator.command.main.RouteCalculator;
import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Limits;

//- CLASS IMPLEMENTATION ...................................................................................
public class Route {
	private static final long				BLOCK_ADJUSTER					= 15000;
	// - S T A T I C - S E C T I O N ..........................................................................
	private static int							OPTIMIZER_CHANGE_LEVEL	= 0;
	private static double						ITERATION_INCREMENT			= 1.0 / 60.0;
	private static int							deepLevel								= 5;

	// - F I E L D - S E C T I O N ............................................................................
	protected Vector<RouteCell>			route										= new Vector<RouteCell>();
	protected Vector<RouteControl>	controls								= new Vector<RouteControl>();
	protected Vector<GeoLocation>		savedConfiguration			= new Vector<GeoLocation>();
	protected RouteCell							startNode								= null;
	protected RouteCell							endNode									= null;
	protected RouteState						creationState						= RouteState.EMPTY;
	protected RouteCell							leftNode								= null;
	private long										iterationCounter				= 0;
	private double									optimElapsed						= 0.0;
	private double									bestTime								= Double.POSITIVE_INFINITY;
	private long										lastIteration						= 0;
	private static boolean					windShiftActive					= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Route() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Adds a new wind cell to the Route. This procedure checks for the formation of the Route and determines if
	 * this is a Start cell or a middle cell and also connect the controls.
	 * 
	 * @param direction
	 */
	public void add(final WindCell routeCell, final Directions direction, final GeoLocation entry, final GeoLocation exit) {
		final RouteCell newCell = new RouteCell(routeCell, entry, exit);
		// - Check for the start formation of the route.
		if (this.creationState == RouteState.EMPTY) {
			this.route.add(newCell);
			this.leftNode = newCell;
			startNode = newCell;
			this.creationState = RouteState.MIDDLE;
			return;
		}
		if (this.creationState == RouteState.MIDDLE) {
			this.route.add(newCell);
			this.controls.add(new RouteControl(entry, direction, this.leftNode, newCell));
			savedConfiguration.add(new GeoLocation(0.0, 0.0));
			this.leftNode = newCell;
			return;
		}
		// endNode = newCell;
	}

	public double elapsed() {
		final Iterator<RouteCell> rit = this.route.iterator();
		double elapsed = 0.0;
		while (rit.hasNext()) {
			elapsed += rit.next().calcElapsed();
		}
		return elapsed;
	}

	public static void setWindShiftActive(boolean windShiftActive) {
		windShiftActive = windShiftActive;
	}

	/**
	 * Starts the process to calculate a new optimun route based on the current route setup. New route
	 * calculation will be performed by exploring the values possible for the route controls.
	 */
	public double optimizeRoute() {
		optimElapsed = Double.POSITIVE_INFINITY;
		double elapsed = this.optimizeControl(0, 0.0);
		// - Before returning the result we have to update the route controls to the saved right values.
		// Vector<GeoLocation> bestConfiguration = controls.get(0).getSavedConfiguration();
		for (int index = 0; index < controls.size(); index++) {
			RouteControl control = controls.get(index);
			control.setOptimizedLocation(savedConfiguration.get(index));
			control.updateLeft(control.getOptimizedLocation());
			control.updateRight(control.getOptimizedLocation());
		}
		return elapsed;
	}

	public String printOptimization() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(this.controls.toString()).append("]");
		return buffer.toString();
	}

	public String printReport() {
		// - Before printing the route data
		final StringBuffer buffer = new StringBuffer("[RouteReport ").append('\n');
		Iterator<RouteCell> rit = this.route.iterator();
		int routeCounter = 1;
		while (rit.hasNext()) {
			RouteCell routeCell = rit.next();

			// - Check if this cell is the start of the route.
			if (startNode.equals(routeCell)) buffer.append(routeCell.printStartReport()).append('\n');
			buffer.append(routeCell.printReport(routeCounter++)).append('\n');
		}
		// - Use that last control to get the last cell data.
		buffer.append("]");
		return buffer.toString();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Route ");
		buffer.append(this.route.toString()).append("]");
		buffer.append('\n');
		buffer.append("[").append(this.controls.toString()).append("]");
		return buffer.toString();
	}

	private RouteControl getControl(final int controlId) {
		return this.controls.elementAt(controlId);
	}

	/**
	 * Main recursive calculation method. This release divides the scan in two sections and then applies
	 * different optimization procedures depending on the deep level of the control.
	 * 
	 * @param elapsed
	 */
	private double optimizeControl(final int controlId, double elapsed) {
		// - If the level is below the limit then perform a full control scan. Otherwise use the new optimizer.
		if (controlId < OPTIMIZER_CHANGE_LEVEL) return optimizerLevel1(controlId, elapsed);
		return optimizerLevel2(controlId, elapsed);
	}

	/**
	 * Bug Description: When a new optimized time if found we store the current location of the control, but we
	 * loose the current locations and optimized for this one for any control up in the list. So we have to keep
	 * a copy of the control positions at this point that should be the real data returned to the initial
	 * optimizer, because el the end we have the right elapsed time, but not the control configuration that
	 * generates that elapsed time.
	 */
	private double optimizerLevel1(int controlId, double elapsed) {
		// - Check if we have run out of controls.
		try {
			final RouteControl control = this.getControl(controlId);
			double currentElapsed = Double.POSITIVE_INFINITY;
			if ((Directions.E == control.getDirection()) | (Directions.W == control.getDirection())) {
				final Limits iterationLimits = control.getLimits();
				final double fixedLon = control.getFixedLon();

				for (double controlRange = iterationLimits.getWest(); controlRange <= iterationLimits.getEast(); controlRange += 1 / 60.0) {
					final GeoLocation newLocation = new GeoLocation(controlRange, fixedLon);
					// final double rigthTTC = control.updateRight(newLocation);
					// if (Double.POSITIVE_INFINITY == rigthTTC) {
					// continue;
					// }
					final double leftTTC = control.updateLeft(newLocation);
					if (Double.POSITIVE_INFINITY == leftTTC) {
						continue;
					}

					// - Calculate the total elapsed time for this iteration.
					double newElapsed = leftTTC;
					// if (0 == controlId) newElapsed += leftTTC;
					newElapsed += this.optimizeControl(controlId + 1, elapsed);
					if (newElapsed < currentElapsed) {
						currentElapsed = newElapsed;
						control.setOptimizedLocation(newLocation);
						saveControlConfiguration();
					}
				}
				return currentElapsed;
			}
			if ((Directions.N == control.getDirection()) | (Directions.S == control.getDirection())) {
				final Limits iterationLimits = control.getLimits();
				final double fixedLat = control.getFixedLat();

				for (double controlRange = iterationLimits.getSouth(); controlRange <= iterationLimits.getNorth(); controlRange += 1 / 60.0) {
					final GeoLocation newLocation = new GeoLocation(fixedLat, controlRange);
					final double rigthTTC = control.updateRight(newLocation);
					if (Double.POSITIVE_INFINITY == rigthTTC) {
						continue;
					}
					final double leftTTC = control.updateLeft(newLocation);
					if (Double.POSITIVE_INFINITY == leftTTC) {
						continue;
					}

					// - Calculate the total elapsed time for this iteration.
					double newElapsed = leftTTC;
					// if (0 == controlId) newElapsed += leftTTC;
					newElapsed += this.optimizeControl(controlId + 1, elapsed);
					if (newElapsed < currentElapsed) {
						currentElapsed = newElapsed;
						control.setOptimizedLocation(newLocation);
						saveControlConfiguration();
					}
				}
				return currentElapsed;
			}
			return 0.0;
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			return 0.0;
		}
	}

	@SuppressWarnings("unused")
	private void saveControlConfiguration() {
		for (int index = 0; index < controls.size(); index++) {
			savedConfiguration.set(index, controls.get(index).getDirectLocation());
		}
		this.lastIteration = iterationCounter;

		// - Recalculate the wind cells after optimization to select the right wind cells.
		Iterator<RouteCell> rit = route.iterator();
		Date now = GregorianCalendar.getInstance().getTime();
		now = WindMapHandler.addElapsed(now, -2.0);
		Date newTime = now;
		double elapsed = 0.0;
		while (rit.hasNext()) {
			RouteCell routeCell = rit.next();
			WindCell windCell = routeCell.getCell();
			// WindMapHandler mapReference = windCell.getMapReference();
			try {
				WindCell newCell = WindMapHandler.cellWithPoint(routeCell.entryLocation, newTime);
				if (routeCell.updateWindCell(newCell)) routeCell.calculateCell();
				elapsed += routeCell.getTTC();
				newTime = WindMapHandler.addElapsed(now, elapsed);
			} catch (LocationNotInMap e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				int dum = 1;
			}
		}
	}

	/**
	 * This optimizer trims the number of possible iterations based on the control deep level. For initial
	 * levels it scans the 50 per cent of the point while for higher levels this numer is reduced.
	 * 
	 * @param elapsed
	 */
	private double optimizerLevel2(int controlId, double elapsed) {
		// - Check if we have run out of controls.
		try {
			final RouteControl control = this.getControl(controlId);

			if ((Directions.E == control.getDirection()) | (Directions.W == control.getDirection())) {
				double startPoint = control.getDirectLocation().getLat();
				// double lowElapsed = processLatitudeLow(controlId, control, startPoint, elapsed);
				// return processLatitudeHigh(controlId, control, startPoint, elapsed);
				processLatitude(controlId, control, startPoint, elapsed);
				return 10.0;
			}
			if ((Directions.N == control.getDirection()) | (Directions.S == control.getDirection())) {
				double startPoint = control.getDirectLocation().getLon();
				// double lowElapsed = processLongitudeLow(controlId, control, startPoint, elapsed);
				// return processLongitudeHigh(controlId, control, startPoint, elapsed);
				processLongitude(controlId, control, startPoint, elapsed);
				return 10.0;
			}
			return 0.0;
		} catch (final ArrayIndexOutOfBoundsException aioobe) {
			return 0.0;
		}
	}

	private void processLatitude(int controlId, RouteControl control, double startPoint, double elapsed) {
		final Limits iterationLimits = control.getLimits();
		final double fixedLon = control.getFixedLon();
		long iterations = levelIterations(controlId);
		for (int counter = 0; counter < iterations; counter++) {
			// - Calculate the control position for the port side.
			iterationCounter++;
			double controlRange = startPoint - ITERATION_INCREMENT * counter;
			if (controlRange < iterationLimits.getWest()) {
				controlRange = iterationLimits.getWest();
			}
			GeoLocation newLocation = new GeoLocation(controlRange, fixedLon);
			if (controlId == 0) {
				System.out.println("Iterating level 0 with " + newLocation.toReport());
				// lastIteration = iterationCounter;
			}
			calculateRouteTime(controlId, control, newLocation, elapsed);
			if (controlRange == iterationLimits.getWest()) continue;

			// - Calculate the control position for the starboard side.
			iterationCounter++;
			controlRange = startPoint + ITERATION_INCREMENT * counter;
			if (controlRange > iterationLimits.getEast()) controlRange = iterationLimits.getEast();
			newLocation = new GeoLocation(controlRange, fixedLon);
			if (controlId == 0) {
				System.out.println("Iterating level 0 with " + newLocation.toReport());
				// lastIteration = iterationCounter;
			}
			calculateRouteTime(controlId, control, newLocation, elapsed);
			if (controlRange == iterationLimits.getEast()) continue;
		}
	}

	private void processLongitude(int controlId, RouteControl control, double startPoint, double elapsed) {
		final Limits iterationLimits = control.getLimits();
		final double fixedLat = control.getFixedLat();
		long iterations = levelIterations(controlId);
		for (int counter = 0; counter < iterations; counter++) {
			// - Calculate the control position for the port side.
			iterationCounter++;
			double controlRange = startPoint - ITERATION_INCREMENT * counter;
			if (controlRange < iterationLimits.getSouth()) {
				controlRange = iterationLimits.getSouth();
			}
			GeoLocation newLocation = new GeoLocation(fixedLat, controlRange);
			calculateRouteTime(controlId, control, newLocation, elapsed);
			if (controlRange == iterationLimits.getSouth()) continue;

			// - Calculate the control position for the starboard side.
			iterationCounter++;
			controlRange = startPoint + ITERATION_INCREMENT * counter;
			if (controlRange > iterationLimits.getNorth()) controlRange = iterationLimits.getNorth();
			newLocation = new GeoLocation(fixedLat, controlRange);
			calculateRouteTime(controlId, control, newLocation, elapsed);
			if (controlRange == iterationLimits.getNorth()) continue;
		}
	}

	/** New function that test for wind shift and then average the result before giving the cell. */
	private boolean calculateRouteTime(int controlId, RouteControl control, GeoLocation newLocation, double elapsed) {
		control.updateLocation(newLocation);
		double leftTTC = control.getLeftTTC();
		if (Double.POSITIVE_INFINITY == leftTTC) return false;

		// - Check if wind shift on this cell.
		if (windShiftActive) {
			if (checkIfWindShift(elapsed, leftTTC)) {
				// - Create a new wind shift cell with the corresponding control.
				RouteCell leftCell = control.left;

				// - Optimization No2. If the run is short do not optimize it.
				if (leftTTC > 1.5) {
					WindShiftRouteCell shiftRoute = new WindShiftRouteCell(elapsed, leftCell.getEntryLocation(), leftCell
							.getExitLocation());

					// - Optimize the wind shit for the current values and then return this as the new value.
					double shiftElapsed = shiftRoute.optimize(leftTTC);
					System.out.println("Detected wind shift: leftTTC=" + leftTTC + " - " + shiftElapsed);
					leftTTC = shiftElapsed;
				}
			}
		}

		// - Check if this is the last available control.
		if (lastControl(controlId)) {
			final double rigthTTC = control.getRightTTC();
			if (Double.POSITIVE_INFINITY == rigthTTC) return false;
			double currentElapsed = elapsed + leftTTC + rigthTTC;

			// - Check if this is a better time.
			if (currentElapsed < bestTime) {
				bestTime = currentElapsed;
				if (RouteCalculator.onDebug()) {
					System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
				}
				saveControlConfiguration();
				return true;
			}
		} else
			this.optimizeControl(controlId + 1, elapsed + leftTTC);
		return false;
	}

	private boolean checkIfWindShift(double elapsed, double additionalTime) {
		Date elapsedDate = GregorianCalendar.getInstance().getTime();
		elapsedDate = WindMapHandler.addElapsed(elapsedDate, elapsed);
		int elapsedHours = elapsedDate.getHours();
		int elapsedMinutes = elapsedDate.getMinutes();
		double nextHours = elapsedHours + elapsedMinutes / 60.0 + additionalTime;
		if (elapsedHours < 11) {
			if (nextHours > 11) return true;
		} else {
			if (nextHours > 23) return true;
		}
		return false;
	}

	// private boolean calculateRouteTime2(int controlId, RouteControl control, GeoLocation newLocation, double
	// elapsed) {
	// control.updateLocation(newLocation);
	// final double leftTTC = control.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) return false;
	// // - Check if this is the last available control.
	// if (lastControl(controlId)) {
	// final double rigthTTC = control.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) return false;
	// double currentElapsed = elapsed + leftTTC + rigthTTC;
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) {
	// bestTime = currentElapsed;
	// if (RouteCalculator.onDebug()) {
	// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
	// }
	// saveControlConfiguration();
	// return true;
	// }
	// } else
	// this.optimizeControl(controlId + 1, elapsed + leftTTC);
	// return false;
	// }
	//
	private boolean lastControl(int controlId) {
		if (controlId + 1 == controls.size())
			return true;
		else
			return false;
	}

	// private double processLatitudeLow(int controlId, RouteControl control, double startPoint, double elapsed)
	// {
	// // if (iterationCounter > 200000) return 300.0;
	// final Limits iterationLimits = control.getLimits();
	// final double fixedLon = control.getFixedLon();
	// long iterations = levelIterations(controlId);
	// // double currentElapsed = minElapsed;
	// boolean activateTrimmer = false;
	// long trimmerLimit = iterations + 1;
	// long trimmerCount = 0;
	//
	// for (int counter = 0; counter < iterations; counter++) {
	// // - Calculate the control position for this iteration.
	// double controlRange = startPoint - ITERATION_INCREMENT * counter;
	// if (controlRange < iterationLimits.getWest()) controlRange = iterationLimits.getWest();
	// final GeoLocation newLocation = new GeoLocation(controlRange, fixedLon);
	// iterationCounter++;
	// control.updateLocation(newLocation);
	// final double leftTTC = control.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getWest()) break;
	// iterations++;
	// continue;
	// }
	//
	// // - Calculate the total elapsed time for this iteration.
	// double newElapsed = this.optimizeControl(controlId + 1, elapsed + leftTTC);
	// if (0.0 == newElapsed) {
	// // - This is the last control. Compute the total elapsed and tehen check if is best.
	// final double rigthTTC = control.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getWest()) break;
	// iterations++;
	// continue;
	// }
	// double currentElapsed = elapsed + leftTTC + rigthTTC;
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) {
	// // control.setOptimizedLocation(newLocation);
	// bestTime = currentElapsed;
	// if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
	// }
	// saveControlConfiguration();
	// }
	// }
	//
	// // } else
	// // newElapsed += leftTTC;
	// // if (activateTrimmer) {
	// // trimmerCount++;
	// // if (trimmerCount > trimmerLimit) break;
	// // }
	// // if (newElapsed < currentElapsed) {
	// // activateTrimmer = true;
	// // trimmerLimit = iterations / 2;
	// // trimmerCount = 0;
	// // currentElapsed = newElapsed;
	// // control.setOptimizedLocation(newLocation);
	// // if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// // System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed +
	// // "location="
	// // + newLocation);
	// // }
	// // control.saveControlConfiguration(newLocation, controlId, this.controls);
	// // }
	// if (controlRange == iterationLimits.getWest()) break;
	// }
	// return 100.0;
	// }
	//
	// private double processLatitudeHigh(int controlId, RouteControl control, double startPoint, double
	// elapsed) {
	// // if (iterationCounter > 200000) return 300.0;
	// final Limits iterationLimits = control.getLimits();
	// final double fixedLon = control.getFixedLon();
	// long iterations = levelIterations(controlId);
	// // double currentElapsed = minElapsed;
	// boolean activateTrimmer = false;
	// long trimmerLimit = iterations + 1;
	// long trimmerCount = 0;
	//
	// for (int counter = 0; counter < iterations; counter++) {
	// // - Calculate the control position for this iteration.
	// double controlRange = startPoint + ITERATION_INCREMENT * counter;
	// if (controlRange > iterationLimits.getEast()) controlRange = iterationLimits.getEast();
	// final GeoLocation newLocation = new GeoLocation(controlRange, fixedLon);
	// iterationCounter++;
	// control.updateLocation(newLocation);
	// final double leftTTC = control.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getEast()) break;
	// iterations++;
	// continue;
	// }
	//
	// // - Calculate the total elapsed time for this iteration.
	// double newElapsed = this.optimizeControl(controlId + 1, elapsed + leftTTC);
	// if (0.0 == newElapsed) {
	// final double rigthTTC = control.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getEast()) break;
	// iterations++;
	// continue;
	// }
	// double currentElapsed = elapsed + leftTTC + rigthTTC;
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) {
	// // control.setOptimizedLocation(newLocation);
	// bestTime = currentElapsed;
	// if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
	// }
	// saveControlConfiguration();
	// }
	// }
	// // newElapsed += leftTTC + rigthTTC;
	// // } else
	// // newElapsed += leftTTC;
	// // if (activateTrimmer) {
	// // trimmerCount++;
	// // if (trimmerCount > trimmerLimit) break;
	// // }
	// // if (newElapsed < currentElapsed) {
	// // activateTrimmer = true;
	// // trimmerLimit = iterations / 2;
	// // currentElapsed = newElapsed;
	// // control.setOptimizedLocation(newLocation);
	// // if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// // System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed +
	// // "location="
	// // + newLocation);
	// // }
	// // control.saveControlConfiguration(newLocation, controlId, this.controls);
	// // }
	// if (controlRange == iterationLimits.getEast()) break;
	// }
	// return 200.0;
	// }
	//
	// private double processLongitudeLow(int controlId, RouteControl control, double startPoint, double
	// elapsed) {
	// // if (iterationCounter > 200000) return 300.0;
	// final Limits iterationLimits = control.getLimits();
	// final double fixedLat = control.getFixedLat();
	// long iterations = levelIterations(controlId);
	// // double currentElapsed = minElapsed;
	// boolean activateTrimmer = false;
	// long trimmerLimit = iterations + 1;
	// long trimmerCount = 0;
	//
	// for (int counter = 0; counter < iterations; counter++) {
	// // - Calculate the control position for this iteration.
	// double controlRange = startPoint - ITERATION_INCREMENT * counter;
	// if (controlRange < iterationLimits.getSouth()) {
	// controlRange = iterationLimits.getSouth();
	// }
	// final GeoLocation newLocation = new GeoLocation(fixedLat, controlRange);
	// iterationCounter++;
	// control.updateLocation(newLocation);
	// final double leftTTC = control.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getSouth()) break;
	// iterations++;
	// continue;
	// }
	//
	// // - Calculate the total elapsed time for this iteration.
	// double newElapsed = this.optimizeControl(controlId + 1, elapsed + leftTTC);
	// if (0.0 == newElapsed) {
	// final double rigthTTC = control.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getSouth()) break;
	// iterations++;
	// continue;
	// }
	// double currentElapsed = elapsed + leftTTC + rigthTTC;
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) {
	// // control.setOptimizedLocation(newLocation);
	// bestTime = currentElapsed;
	// if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
	// }
	// saveControlConfiguration();
	// }
	// }
	// // newElapsed += leftTTC + rigthTTC;
	// // } else
	// // newElapsed += leftTTC;
	// // if (activateTrimmer) {
	// // trimmerCount++;
	// // if (trimmerCount > trimmerLimit) break;
	// // }
	// // if (newElapsed < currentElapsed) {
	// // activateTrimmer = true;
	// // trimmerLimit = iterations / 2;
	// // currentElapsed = newElapsed;
	// // control.setOptimizedLocation(newLocation);
	// // if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// // System.out.println("CTRL " + controlId + "-" + currentElapsed + "location=" + newLocation);
	// // }
	// // control.saveControlConfiguration(newLocation, controlId, this.controls);
	// // }
	// if (controlRange == iterationLimits.getSouth()) break;
	// }
	// return 300.0;
	// }
	//
	// private double processLongitudeHigh(int controlId, RouteControl control, double startPoint, double
	// elapsed) {
	// // if (iterationCounter > 200000) return 300.0;
	// final Limits iterationLimits = control.getLimits();
	// final double fixedLat = control.getFixedLat();
	// long iterations = levelIterations(controlId);
	// // double currentElapsed = minElapsed;
	// boolean activateTrimmer = false;
	// long trimmerLimit = iterations + 1;
	// long trimmerCount = 0;
	//
	// for (int counter = 0; counter < iterations; counter++) {
	// // - Calculate the control position for this iteration.
	// double controlRange = startPoint + ITERATION_INCREMENT * counter;
	// if (controlRange > iterationLimits.getNorth()) controlRange = iterationLimits.getNorth();
	// final GeoLocation newLocation = new GeoLocation(fixedLat, controlRange);
	// iterationCounter++;
	// control.updateLocation(newLocation);
	// final double leftTTC = control.getLeftTTC();
	// if (Double.POSITIVE_INFINITY == leftTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getNorth()) break;
	// iterations++;
	// continue;
	// }
	//
	// // - Calculate the total elapsed time for this iteration.
	// double newElapsed = this.optimizeControl(controlId + 1, elapsed + leftTTC);
	// if (0.0 == newElapsed) {
	// final double rigthTTC = control.getRightTTC();
	// if (Double.POSITIVE_INFINITY == rigthTTC) {
	// // - Infinite routes should not count against the limits
	// if (controlRange == iterationLimits.getNorth()) break;
	// iterations++;
	// continue;
	// }
	// double currentElapsed = elapsed + leftTTC + rigthTTC;
	//
	// // - Check if this is a better time.
	// if (currentElapsed < bestTime) {
	// // control.setOptimizedLocation(newLocation);
	// bestTime = currentElapsed;
	// if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// System.out.println("It: " + iterationCounter + " - CTRL " + controlId + "-" + currentElapsed);
	// }
	// saveControlConfiguration();
	// }
	// }
	// // newElapsed += leftTTC + rigthTTC;
	// // } else
	// // newElapsed += leftTTC;
	// // if (activateTrimmer) {
	// // trimmerCount++;
	// // if (trimmerCount > trimmerLimit) break;
	// // }
	// // if (newElapsed < currentElapsed) {
	// // activateTrimmer = true;
	// // trimmerLimit = iterations / 2;
	// // currentElapsed = newElapsed;
	// // control.setOptimizedLocation(newLocation);
	// // if (RouteCalculator.onDebug()) {
	// // if (controlId < 5)
	// // System.out.println("CTRL " + controlId + "-" + currentElapsed + "location=" + newLocation);
	// // }
	// // control.saveControlConfiguration(newLocation, controlId, this.controls);
	// // }
	// if (controlRange == iterationLimits.getNorth()) break;
	// }
	// return 400.0;
	// }

	private long levelIterations(int controlId) {
		if (controlId == 0) return deepLevel;
		if (controlId == 1) return deepLevel;
		// if (controlId == OPTIMIZER_CHANGE_LEVEL + 2) return 8;
		// if (controlId == OPTIMIZER_CHANGE_LEVEL + 3) return 8;
		// if (controlId == OPTIMIZER_CHANGE_LEVEL + 4) return 6;
		// if (controlId == OPTIMIZER_CHANGE_LEVEL + 5) return 6;
		long iterationDiff = iterationCounter - lastIteration;
		if (iterationDiff < BLOCK_ADJUSTER)
			return deepLevel;
		else {
			// - Recalculate the number of iterations.
			long newLevel = deepLevel - iterationDiff / BLOCK_ADJUSTER;
			if (newLevel < 2) {
				// System.out.println("Changed iteration level to 2");
				return 2;
			} else {
				// System.out.println("Changed iteration level to " + newLevel);
				return newLevel;
			}
		}
	}

	// [01]

	public static void setOptimizerLevel(int optimizerLevel) {
		OPTIMIZER_CHANGE_LEVEL = optimizerLevel;
	}

	public static void setDeepLevel(int level) {
		deepLevel = level;
	}

	public RouteCell getLast() {
		return leftNode;
	}
}

enum RouteState {
	EMPTY, MIDDLE, CLONED;
}
// - UNUSED CODE ............................................................................................
// [01]
// protected Route clone() throws CloneNotSupportedException {
// final Route newRoute = new Route();
// final Iterator<RouteCell> cit = this.route.iterator();
// while (cit.hasNext()) {
// RouteCell cell = cit.next();
// Vector<Intersection> intersections = this.calculateIntersection(start, end, startCell);
// newRoute.add(cell.getCell(), cell.getDirection(),cell.getEntryLocation(), cell.getExitLocation());
// }
//
// return newRoute;
// }
// protected Route clone() throws CloneNotSupportedException {
// final Route newRoute = new Route();
// final Vector<RouteCell> newRouteCells = new Vector<RouteCell>();
// final Vector<RouteControl> newControls = new Vector<RouteControl>();
//
// final Iterator<RouteCell> rit = this.route.iterator();
// while (rit.hasNext()) {
// final RouteCell cell = rit.next();
// newRouteCells.add(cell.clone());
// }
// final Iterator<RouteControl> cit = this.controls.iterator();
// while (cit.hasNext()) {
// final RouteControl control = cit.next();
// newControls.add(control.clone());
// }
// newRoute.loadData(newRouteCells, newControls);
// return newRoute;
// }
