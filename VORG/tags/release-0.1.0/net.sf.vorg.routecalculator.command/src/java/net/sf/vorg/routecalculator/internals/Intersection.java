//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;


// - CLASS IMPLEMENTATION ...................................................................................
public class Intersection {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.routecalculator.internals");

	// - F I E L D - S E C T I O N ............................................................................
	protected GeoLocation	location;
	protected Directions	direction;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Intersection(GeoLocation location, Directions direction) {
		this.location = location;
		this.direction = direction;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}

	public Directions getDirection() {
		return direction;
	}

	public void setDirection(Directions direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Intersection ");
		buffer.append("location=").append(this.location).append(",");
		buffer.append("direction=").append(this.direction).append("]");
		return buffer.toString();
	}

}
// - UNUSED CODE ............................................................................................
