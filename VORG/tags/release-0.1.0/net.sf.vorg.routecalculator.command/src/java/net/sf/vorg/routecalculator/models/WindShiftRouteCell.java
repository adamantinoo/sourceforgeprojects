//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.Directions;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Limits;

// - CLASS IMPLEMENTATION ...................................................................................
public class WindShiftRouteCell extends RouteCell {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger		logger		= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	protected RouteCell			preRoute;
	protected RouteCell			postRoute;
	protected RouteControl	shiftControl;

	private RouteCell				globalRoute;

	private boolean					optimize	= true;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WindShiftRouteCell() {
	}

	public WindShiftRouteCell(double elapsed, GeoLocation entry, GeoLocation exit) {
		// - Get the two cells that conform the wind shift.
		Calendar elapsedDate = GregorianCalendar.getInstance();
		int seconds = new Double(elapsed * 3600.0).intValue();
		elapsedDate.add(Calendar.SECOND, seconds);
		try {
			WindCell preWindCell = WindMapHandler.cellWithPoint(entry, elapsedDate.getTime());
			globalRoute = new RouteCell(preWindCell, entry, exit);
			double timeToEleven = nextElevenDifference(elapsedDate);

			// - Optimization No1. If the time for a wind shift is less than 1 hour no dot optimize.
			if (timeToEleven <= 1) {
				optimize = false;
				return;
			}

			// - Calculate new coordinate for this leg.
			GeoLocation newExit = calculatePosition(entry, exit, timeToEleven);
			preRoute = new RouteCell(preWindCell, entry, newExit);

			seconds = new Double(globalRoute.getTTC() * 3600.0).intValue();
			elapsedDate.add(Calendar.SECOND, seconds);
			WindCell postWindCell = WindMapHandler.cellWithPoint(entry, elapsedDate.getTime());
			postRoute = new RouteCell(postWindCell, newExit, exit);

			shiftControl = new RouteControl(new GeoLocation(newExit.getLat(), newExit.getLon()), calculateControlDirection(
					entry, exit), preRoute, postRoute);
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Directions calculateControlDirection(GeoLocation start, GeoLocation end) {
		double dLat = end.getLat() - start.getLat();
		double dLon = end.getLon() - start.getLon();
		if (dLat > dLon)
			return Directions.N;
		else
			return Directions.E;
	}

	private GeoLocation calculatePosition(GeoLocation start, GeoLocation end, double timeToEleven) {
		double angle = start.angleTo(end);
		// - Calculate location from start.
		double dLat = end.getLat() - start.getLat();
		double dLon = end.getLon() - start.getLon();
		double hyp = Math.hypot(start.getLat() - end.getLat(), start.getLon() - end.getLon());
		// - Movement units per hour.
		double ttc = globalRoute.getTTC();
		double rad = timeToEleven / globalRoute.getTTC();
		double diffLat = dLat * rad;
		double diffLon = dLon * rad;
		GeoLocation newLocation = new GeoLocation(start.getLat() + diffLat, start.getLon() + diffLon);
		return newLocation;
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	private double nextElevenDifference(Calendar elapsedDate) {
		int elapsedHours = elapsedDate.get(Calendar.HOUR_OF_DAY);
		Calendar elevenTime;
		if (elapsedHours < 11) {
			elevenTime = GregorianCalendar.getInstance();
			elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH), elevenTime.get(Calendar.DATE), 11,
					0, 0);
		} else {
			elevenTime = GregorianCalendar.getInstance();
			elevenTime.set(elevenTime.get(Calendar.YEAR), elevenTime.get(Calendar.MONTH), elevenTime.get(Calendar.DATE), 23,
					0, 0);
		}
		long diff = elevenTime.getTimeInMillis() - elapsedDate.getTimeInMillis();
		// - Convert to hours.
		double hours = diff / (60 * 60 * 1000);
		return hours;
	}

	public double optimize(double ttc) {
		if (optimize) {
			if (shiftControl.getDirection() == Directions.E)
				return processLatitude();
			else
				return processLongitude();
		} else
			return ttc;
	}

	private double processLatitude() {
		final Limits iterationLimits = shiftControl.getLimits();
		final double fixedLon = shiftControl.getFixedLon();
		double bestTime = Double.POSITIVE_INFINITY;
		for (double controlRange = iterationLimits.getWest(); controlRange <= iterationLimits.getEast(); controlRange += 1 / 60.0) {
			GeoLocation newLocation = new GeoLocation(controlRange, fixedLon);
			double currentElapsed = calculateRouteTime(newLocation);

			// - Check if this is a better time.
			if (currentElapsed < bestTime) {
				bestTime = currentElapsed;
				// if (RouteCalculator.onDebug()) {
				// System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
				// shiftControl.left.getCell().toString());
				// }
			}
		}
		return bestTime;
	}

	private double processLongitude() {
		final Limits iterationLimits = shiftControl.getLimits();
		final double fixedLat = shiftControl.getFixedLat();
		double bestTime = Double.POSITIVE_INFINITY;
		for (double controlRange = iterationLimits.getSouth(); controlRange <= iterationLimits.getNorth(); controlRange += 1 / 60.0) {
			GeoLocation newLocation = new GeoLocation(fixedLat, controlRange);
			double currentElapsed = calculateRouteTime(newLocation);

			// - Check if this is a better time.
			if (currentElapsed < bestTime) {
				bestTime = currentElapsed;
				// if (RouteCalculator.onDebug()) {
				// System.out.println("Wind Shift CTRL=" + currentElapsed + " - " +
				// shiftControl.left.getCell().toString());
				// }
			}
		}
		return bestTime;
	}

	private double calculateRouteTime(GeoLocation newLocation) {
		shiftControl.updateLocation(newLocation);
		double leftTTC = shiftControl.getLeftTTC();
		if (Double.POSITIVE_INFINITY == leftTTC) return Double.POSITIVE_INFINITY;
		double rigthTTC = shiftControl.getRightTTC();
		if (Double.POSITIVE_INFINITY == rigthTTC) return Double.POSITIVE_INFINITY;
		return leftTTC + rigthTTC;
	}
}

// - UNUSED CODE ............................................................................................
