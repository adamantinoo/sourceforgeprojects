package net.sf.vorg.routecalculator.models;

import java.util.Vector;

import junit.framework.TestCase;
import net.sf.vorg.routecalculator.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.GeoLocation;

public class TestWindMap extends TestCase {
	private final WindMap			testMap;
	private final GeoLocation	start;
	private final GeoLocation	end;

	public TestWindMap() {
		testMap = new WindMap();
		testMap.addCell(new WindCell(3, 100, 180, 10.0));
		testMap.addCell(new WindCell(3, 101, 90, 7.0));
		testMap.addCell(new WindCell(3, 102, 0, 12.0));
		start = new GeoLocation(2, 40, 100, 0);
		end = new GeoLocation(3, 20, 102, 20);
	}

	public void testIntersection() {
		try {
			Vector intersections = testMap.calculateIntersection(start, end, testMap.cellWithPoint(start));
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testDirectRoute() {
		// WindMap testMap = new WindMap();
		// testMap.addCell(new WindCell(3, 100, 180, 10.0));
		// testMap.addCell(new WindCell(3, 101, 90, 8.0));
		// testMap.addCell(new WindCell(3, 102, 0, 12.0));
		Route testRoute = null;
		try {
			// testMap.addStart(start);
			// testMap.addEnd(end);
			testRoute = testMap.directRoute(start, end);
			double elapsedTime = testRoute.elapsed();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return testRoute;
	}

	public void testDirectRoutePolarTest() {
		WindMap polarTestMap = new WindMap();
		polarTestMap.addCell(new WindCell(3, 100, 322, 6.5));
		polarTestMap.addCell(new WindCell(3, 101, 304, 7.6));
		polarTestMap.addCell(new WindCell(2, 101, 327, 3.8));
		polarTestMap.addCell(new WindCell(2, 102, 311, 7.0));
		GeoLocation polarTestStart = new GeoLocation(3, 27, 100, 0);
		GeoLocation polarTestEnd = new GeoLocation(2, 8, 101, 52);
		Route polarTest = null;
		try {
			// testMap.addStart(start);
			// testMap.addEnd(end);
			polarTest = polarTestMap.directRoute(polarTestStart, polarTestEnd);
			double elapsedTime = polarTest.elapsed();
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testOptimizePolarTest() {
		WindMap polarTestMap = new WindMap();
		polarTestMap.addCell(new WindCell(3, 100, 322, 6.5));
		polarTestMap.addCell(new WindCell(3, 101, 304, 7.6));
		polarTestMap.addCell(new WindCell(2, 101, 327, 3.8));
		polarTestMap.addCell(new WindCell(2, 102, 311, 7.0));
		GeoLocation polarTestStart = new GeoLocation(3, 27, 100, 0);
		GeoLocation polarTestEnd = new GeoLocation(2, 8, 101, 52);
		// Route polarTest = null;
		// double elapsedTime = 0.0;
		try {
			polarTestMap.optimizeRoute(polarTestStart, polarTestEnd);
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testOptimizeRoute() {
		try {
			testMap.optimizeRoute(start, end);
		} catch (LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testVMCRoute() throws Exception {
		WindMapHandler vmcMap = new WindMapHandler();
		GeoLocation vmcStart = new GeoLocation(1.25405, 104.29709);
		GeoLocation vmcEnd = new GeoLocation(10.0, 109.0);
		vmcMap.loadWinds(vmcStart);
		vmcMap.loadWinds(vmcEnd);
		vmcMap.calculateVMCRoute(vmcStart, vmcEnd);
	}
}
