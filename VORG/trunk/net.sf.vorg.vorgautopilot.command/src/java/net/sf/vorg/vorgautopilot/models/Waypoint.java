//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

//- IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.Locale;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.internals.VMCData;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.PilotLocation;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;

// - CLASS IMPLEMENTATION ...................................................................................
public class Waypoint extends PilotLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	private static double			DEFAULT_RANGE	= 4.0;
	private static final int	DEFAULT_AWD		= 30;

	// - F I E L D - S E C T I O N ............................................................................
	protected String					name					= null;
	protected WaypointTypes		type					= WaypointTypes.NOACTION;
	private double						range;
	protected int							minAWD;
	protected int							maxAWD;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Waypoint(final String type) {
		setType(type);
	}

	public String activationReport(final ExtendedLocation boatLocation) {
		final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		return toString() + "\n[distance= " + nf.format(getDistance(boatLocation)) + "]";
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public int getAWD() {
		return minAWD;
	}

	public double getDistance(final GeoLocation boatLocation) {
		return getLocation().distance(boatLocation);
	}

	public double getRange() {
		return range;
	}

	public WaypointTypes getType() {
		return type;
	}

	public boolean isEquivalent(final Waypoint targetWaypoint) {
		return targetWaypoint.getLocation().isEquivalent(getLocation());
	}

	public boolean isSurpassed(final GeoLocation boatLocation) {
		// - Location detection for VMG and DIRECT waypoints
		if (type == WaypointTypes.DIRECT) {
			// - Calculate distance to waypoint. Discard it if the distance is less than control.
			final GeoLocation waypointLocation = getLocation();
			final double distance = waypointLocation.distance(boatLocation);
			if (distance < getRange()) {
				System.out.println("--- Discarded waypoint " + waypointLocation + ". Distance " + distance + " below "
						+ getRange() + " miles.");
				return true;
			}
		}
		if (type == WaypointTypes.VMG) {
			// - Calculate distance to waypoint. Discard it if the distance is less than control.
			final GeoLocation waypointLocation = getLocation();
			final double distance = waypointLocation.distance(boatLocation);
			if (distance < getRange()) {
				System.out.println("--- Discarded waypoint " + waypointLocation + ". Distance " + distance + " below "
						+ getRange() + " miles.");
				return true;
			}
		}
		return false;
	}

	public void sendCommand(final Boat boat) throws LocationNotInMap {
		final ExtendedLocation boatLocation = new ExtendedLocation(boat.getLocation());
		if (getType() == WaypointTypes.DIRECT) {
			// - Get the direction to the waypoint and adjust the course and polars.
			final double angle = adjustSailAngle(boatLocation, this);
			final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
			// - Round the angle to get the AWD. This can change the result.
			final double apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), Math.round(angle));
			final SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());
			final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
			nf.setMaximumFractionDigits(3);
			System.out.println();
			System.out.println("[Selected angle= " + nf.format(angle) + " - AWD= " + Math.abs(Math.round(apparentHeading))
					+ "]");
			System.out.println(sails);
			System.out.println();
			final BoatCommand newBoatCommand = new BoatCommand();
			newBoatCommand.setHeading(boat.getHeading(), new Double(Math.round(angle)).intValue());
			newBoatCommand.setSails(sails);
			newBoatCommand.sendCommand(boat);
		}
		if (getType() == WaypointTypes.VMG) {
			final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
			final double angle = boatLocation.angleTo(getLocation());
			// - Get the VMC from this start point to the end of the cell.
			final VMCData vmc = new VMCData(angle, windCell);

			// - Check if current boat configuration is the same as requested.
			System.out.println();
			System.out.println(vmc.printReport());
			System.out.println();
			final BoatCommand newBoatCommand = new BoatCommand();
			newBoatCommand.setHeading(boat.getHeading(), vmc.getBestAngle());
			newBoatCommand.setSails(vmc.getBestSailConfiguration());
			newBoatCommand.sendCommand(boat);
		}
	}

	public void setMinAWD(final String value) {
		if (null == value) {
			minAWD = Waypoint.DEFAULT_AWD;
			return;
		}
		try {
			minAWD = new Integer(value).intValue();
		} catch (final Exception e) {
			minAWD = Waypoint.DEFAULT_AWD;
		}
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setRange(final String value) {
		if (null == value) {
			range = Waypoint.DEFAULT_RANGE;
			return;
		}
		try {
			range = new Double(value);
		} catch (final Exception e) {
			range = Waypoint.DEFAULT_RANGE;
		}
	}

	public void setType(final String newType) {
		type = WaypointTypes.decodeType(newType);
		if (type == WaypointTypes.VMG)
			Waypoint.DEFAULT_RANGE = 5;
		else
			Waypoint.DEFAULT_RANGE = 3;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Waypoint ");
		buffer.append(type).append(" ");
		if (null != name) buffer.append('"').append(name).append('"');
		buffer.append("  ");
		buffer.append("range=").append(range).append(", ");
		buffer.append("AWD=").append(minAWD).append("/").append(maxAWD).append(", ");
		buffer.append(getLocation()).append("");
		buffer.append("]");
		return buffer.toString();
	}

	/*
	 * Adjusts the sailing angle to not to be up to the wind. The angle is changes until the speed is different
	 * from 0.0 and the minimum AWD angle is reached.
	 */
	private double adjustSailAngle(final GeoLocation boatLocation, final Waypoint waypoint) throws LocationNotInMap {
		final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
		double angle = boatLocation.angleTo(waypoint.getLocation());
		double apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
		SailConfiguration sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());

		// - If angle is less that the AWD adjust it. Also adjust id speed < 0.0
		boolean search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
		int displacement = 1;
		if (apparentHeading > 0)
			displacement = -1;
		else
			displacement = 1;
		while (search) {
			angle += displacement;
			apparentHeading = GeoLocation.calculateAWD(windCell.getWindDir(), angle);
			sails = Polars.lookup(new Double(apparentHeading).intValue(), windCell.getWindSpeed());
			search = (sails.getSpeed() == 0.0) | (Math.abs(Math.round(apparentHeading)) < Math.abs(waypoint.getAWD()));
		}
		return angle;
	}

	public boolean isActive() {
		if (type == WaypointTypes.NOACTION) return false;
		return true;
	}
}
// - UNUSED CODE ............................................................................................
