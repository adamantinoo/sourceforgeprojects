//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.vorgautopilot.models.VRToolAutopilot;
import net.sf.vorg.vorgautopilot.models.XMLAutopilot;

// - CLASS IMPLEMENTATION ...................................................................................
public class VORGAutopilot {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger					logger					= Logger.getLogger("net.sf.vorg.vorgautopilot.command.main");
	private static final String		APPLICATIONNAME	= "VORGAutopilot";
	private static VORGAutopilot	singleton;
	private static boolean				onDebug					= false;
	private static PrintWriter		printer;
	private static int						refresh					= 10;
	public static int							timeDelay				= 65;
	static {
		VORGAutopilot.logger.setLevel(Level.OFF);
	}

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	public static int getRefresh() {
		return VORGAutopilot.refresh;
	}

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		VORGAutopilot.singleton = new VORGAutopilot(args);
		VORGAutopilot.singleton.execute();
		VORGAutopilot.exit(0);
	}

	public static boolean onDebug() {
		return VORGAutopilot.onDebug;
	}

	public static void output(final String message) {
		System.out.println(message);
		if (null == VORGAutopilot.printer)
			try {
				VORGAutopilot.printer = new PrintWriter("VORGAutopilot.output.txt");
				VORGAutopilot.printer.println(message);
				VORGAutopilot.printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			VORGAutopilot.printer.println(message);
			VORGAutopilot.printer.flush();
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	private String	configurationFileName	= null;
	private boolean	activateVRTool				= false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/*
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are: <ul> <li><b>-conf<font
	 * color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where the application will
	 * expect the configuration files and data.</li> <li><b>-res<font color="GREY">[ourcesLocation</font></b>
	 * ${RESOURCEDIR} - is the directory where the application is going to locate the files that contains the
	 * SQL statements and other application resources.
	 */
	public VORGAutopilot(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		banner();

		// - Process parameters and store them into the instance fields
		processParameters(args, VORGAutopilot.APPLICATIONNAME);
	}

	/*
	 * Starts a new thread with the autopilot configuration file. From that file the pilot gets all the date
	 * needed to run the boat.
	 */
	public void execute() {
		// - Detect the type of configuration file and then run the right loop.
		if (activateVRTool) {
			final VRToolAutopilot pilot = new VRToolAutopilot(configurationFileName);
			pilot.run();
		} else {
			final XMLAutopilot pilot = new XMLAutopilot(configurationFileName);
			pilot.run();
		}
	}

	public void processParameters(final String[] args, final String ApplicationName) {
		// super.processParameters(args, ApplicationName);
		for (int i = 0; i < args.length; i++) {
			VORGAutopilot.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-conf")) {
					// - Get and open the file with the autopilot configuration
					configurationFileName = argumentStringValue(args, i);
					if (validateConfiguration(configurationFileName))
						continue;
					else
						VORGAutopilot.exit(VORGConstants.INVALIDCONFIGURATION);
				}
				if (args[i].startsWith("-nav")) {
					// - Get and open the file with the autopilot configuration
					configurationFileName = argumentStringValue(args, i);
					activateVRTool = true;
				}
				if (args[i].startsWith("-refr")) { //$NON-NLS-1$
					VORGAutopilot.refresh = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-time")) { //$NON-NLS-1$
					VORGAutopilot.timeDelay = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-debug")) { //$NON-NLS-1$
					VORGAutopilot.onDebug = true;
					continue;
				}
				if (args[i].startsWith("-help")) { //$NON-NLS-1$
					help();
					continue;
				}
			}
		}
		// ... Check that required parameters have values.
		if (null == configurationFileName) VORGAutopilot.exit(VORGConstants.NOCONFIG);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	protected double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	private void banner() {
		System.out.println("__     _____  ____   ____    _         _              _ _       _   ");
		System.out.println("\\ \\   / / _ \\|  _ \\ / ___|  / \\  _   _| |_ ___  _ __ (_) | ___ | |_ ");
		System.out.println(" \\ \\ / / | | | |_) | |  _  / _ \\| | | | __/ _ \\| '_ \\| | |/ _ \\| __|");
		System.out.println("  \\ \\ /| |_| |  _ <| |_| |/ ___ \\ |_| | || (_) | |_) | | | (_) | |_ ");
		System.out.println("   \\_/  \\___/|_| \\_\\\\____/_/   \\_\\__,_|\\__\\___/| .__/|_|_|\\___/ \\__|");
		System.out.println("                                               |_|                  ");
		System.out.println();
		System.out.println(VORGConstants.VERSION);
		System.out.println();
	}

	private void help() {
		System.out.println("Description:");
		System.out
				.println("   Aplication to run the boat based on a configuration file with the commands and points to follow.");
		System.out.println();
		System.out.println("Command API for the RouteFinder:");
		System.out.println("   java -classpath vorgautopilot019.jar net.sf.vorg.vorgautopilot.command.main.VORGAutopilot ");
		System.out.println("Allowed parameters:");
		System.out.println("-config <file name>  -- configuration file with the autentication and the route");
		// System.out.println("-boat <boat name>   -- name of the game boat where to extract the current location");
		// System.out.println("-nav <.NAV file path>  -- path to the VRTool file where to store the results");
		// System.out.println("-scan <number>  -- number of cells to route");
		// System.out.println("-AWD <number>  -- AWD angle to follow in the alternative route");
		// System.out.println("Allowed toggles");
		// System.out.println("-port -- gets the route using the port side angle");
		System.out.println();
	}

	/*
	 * Checks if the configuration file exists and has content. It does not check the content format or the
	 * content structure, at least on this release
	 */
	private boolean validateConfiguration(final String configurationFileName) {
		try {
			final BufferedReader creader = new BufferedReader(new FileReader(configurationFileName));
			creader.close();
		} catch (final FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
			return false;
		} catch (final IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
			return false;
		}
		return true;
	}

	protected int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	protected String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			VORGAutopilot.exit(VORGConstants.NOCONFIG);
		return "";
	}
}

// - UNUSED CODE ............................................................................................
