//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.parsers.MapParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class WindMapHandler {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	private static Hashtable<Date, WindMap>	windMaps	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public WindMapHandler() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// public VMCRoute calculateVMCRoute(final GeoLocation start, final GeoLocation end) throws LocationNotInMap
	// {
	// // - Calculate the direct line to destination.
	// Date now = GregorianCalendar.getInstance().getTime();
	// final WindMap activeMap = this.getActiveMap();
	// final WindCell startCell = this.cellWithPoint(start, now);
	// final WindCell endCell = this.cellWithPoint(end, now);
	//
	// // - Buildup the route to the end from the start cell.
	// final VMCRoute vmcRoute = new VMCRoute();
	// Vector<Intersection> intersections = activeMap.calculateIntersection(start, end, startCell);
	// vmcRoute.add(startCell, intersections.lastElement().getDirection(), start, intersections.lastElement()
	// .getLocation());
	//
	// // - Get cell one by one following the direct path line.
	// // To get the next cell search the one that contains the end intersection.
	// WindCell nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), startCell, now);
	// while (!nextCell.equals(endCell)) {
	// intersections = activeMap.calculateIntersection(start, end, nextCell);
	// vmcRoute.add(nextCell, intersections.firstElement().getDirection(),
	// intersections.firstElement().getLocation(),
	// intersections.lastElement().getLocation());
	// nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), nextCell, now);
	// }
	//
	// intersections = activeMap.calculateIntersection(start, end, endCell);
	// vmcRoute.add(endCell, intersections.firstElement().getDirection(),
	// intersections.firstElement().getLocation(), end);
	//
	// return vmcRoute;
	// }

	public static WindCell getWindCell(GeoLocation location) throws LocationNotInMap {
		Calendar now = GregorianCalendar.getInstance();
		now.add(Calendar.HOUR, -2);
		// loadWinds(location);
		return cellWithPoint(location, now.getTime());
	}

	// public void directRoute(final GeoLocation startLocation, final GeoLocation endLocation) throws
	// LocationNotInMap,
	// CloneNotSupportedException {
	// System.out.println("Starting calculation for Route. - " + Calendar.getInstance());
	// System.out.println("Direct Path.");
	// System.out.println("START " + startLocation.toReport());
	// System.out.println("END " + endLocation.toReport());
	// System.out.println();
	// this.loadWinds(startLocation);
	// this.loadWinds(endLocation);
	// final WindMap actualMap = this.getActiveMap();
	// final Route directRoute = actualMap.directRoute(startLocation, endLocation);
	// System.out.println("Direct Route");
	// System.out.println(directRoute.printReport());
	// System.out.println();
	// System.out.println("Time at end - " + Calendar.getInstance());
	// }

	public static void loadWinds(final GeoLocation location) {
		final String protocol = "http";
		final String host = "volvogame.virtualregatta.com";
		final String prefix = "/resources/winds/meteo_";

		// - Calculate the wind box that matches this location.
		final double latitude = Math.floor(location.getLat() + 0.5);
		double l2 = Math.round(latitude / 10.0) * 10.0;
		int mapLat;
		if (latitude > l2)
			mapLat = new Double(l2).intValue() + 10;
		else
			mapLat = new Double(l2).intValue();

		// - Check if the longitude is below or after the 0.5 limit.
		double minutes = location.getLon() - Math.floor(location.getLon());
		long longitude;
		if (minutes >= 0.5)
			longitude = Math.round(location.getLon() + 0.5);
		else
			longitude = Math.round(location.getLon());
		int mapLon = new Double(Math.floor(Math.floor(longitude) / 10.0) * 10.0).intValue();
		if (mapLon == 180) mapLon = -180;
		final String suffix = mapLon + "_" + mapLat + ".xml?rnd=9165";

		try {
			final URL mapReference = new URL(protocol, host, prefix + suffix);
			System.out.println("Loading map data for box " + suffix);
			final InputStream stream = new BufferedInputStream(mapReference.openStream());
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			// - Check for an empty wind map table.
			if (null == windMaps) windMaps = new Hashtable<Date, WindMap>();
			final MapParserHandler handler = new MapParserHandler(windMaps);
			parser.parse(stream, handler);
		} catch (final MalformedURLException mue) {
			// TODO Auto-generated catch block
			mue.printStackTrace();
		} catch (final IOException ioe) {
			// TODO Auto-generated catch block
			// ioe.printStackTrace();

			// - Code to create a fake wind map to be used while debugging without line.
			Date mapDate = GregorianCalendar.getInstance().getTime();

			// - Check if we have registered a map with that reference.
			WindMap currentMap = new WindMap();
			currentMap.setRef(mapDate.toString());
			windMaps.put(mapDate, currentMap);

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(32, 123, 40, 12.5));
			currentMap.addCell(new WindCell(32, 124, 330, 9.8));
			currentMap.addCell(new WindCell(32, 125, 70, 11.3));
			currentMap.addCell(new WindCell(33, 125, 32, 11.3));
			currentMap.addCell(new WindCell(33, 126, 32, 11.3));
			currentMap.addCell(new WindCell(34, 126, 349, 8.6));

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(1, 104, 16, 9.2));
			currentMap.addCell(new WindCell(1, 105, 3, 23.6));
			currentMap.addCell(new WindCell(2, 105, 10, 21.0));
			currentMap.addCell(new WindCell(2, 106, 9, 22.0));
			currentMap.addCell(new WindCell(3, 106, 13, 19.2));

			Calendar nextMap = GregorianCalendar.getInstance();
			nextMap.add(Calendar.HOUR, 12);
			mapDate = nextMap.getTime();

			// - Check if we have registered a map with that reference.
			currentMap = new WindMap();
			currentMap.setRef(mapDate.toString());
			windMaps.put(mapDate, currentMap);

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(32, 123, 40, 12.5));
			currentMap.addCell(new WindCell(32, 124, 330, 9.8));
			currentMap.addCell(new WindCell(32, 125, 120, 11.3));
			currentMap.addCell(new WindCell(33, 125, 32, 11.3));
			currentMap.addCell(new WindCell(33, 126, 32, 11.3));
			currentMap.addCell(new WindCell(34, 126, 349, 8.6));

			// - Add the debug data needed.
			currentMap.addCell(new WindCell(1, 104, 16, 9.2));
			currentMap.addCell(new WindCell(1, 105, 3, 23.6));
			currentMap.addCell(new WindCell(2, 105, 10, 21.0));
			currentMap.addCell(new WindCell(2, 106, 9, 22.0));
			currentMap.addCell(new WindCell(3, 106, 13, 19.2));
		} catch (final ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
		} catch (final SAXException se) {
			// TODO Auto-generated catch block
			se.printStackTrace();
		}
	}

	// public void optimizeRoute(final GeoLocation startLocation, final GeoLocation endLocation) throws
	// LocationNotInMap,
	// CloneNotSupportedException {
	// System.out.println("Starting calculation for Route. - " /* + Calendar.getInstance() */);
	// System.out.println("Direct Path.");
	// System.out.println("START\t" + startLocation.toReport());
	// System.out.println("END\t" + endLocation.toReport());
	// System.out.println();
	// this.loadWinds(startLocation);
	// this.loadWinds(endLocation);
	// // this.loadWinds(new GeoLocation(2.0, 101.0));
	// // this.loadWinds(new GeoLocation(2.0, 111.0));
	// // this.loadWinds(new GeoLocation(2.0, 121.0));
	// // this.loadWinds(new GeoLocation(12.0, 101.0));
	// // this.loadWinds(new GeoLocation(12.0, 111.0));
	// // this.loadWinds(new GeoLocation(12.0, 121.0));
	// System.out.println();
	// final Route directRoute = this.selectDirectRoute(startLocation, endLocation);
	// System.out.println("Direct Route");
	// System.out.println(directRoute.printReport());
	// System.out.println();
	//
	// System.out.println("Optimized Route");
	// directRoute.optimizeRoute();
	// System.out.println(directRoute.printReport());
	// }

	// public void optimizeRoute2(final GeoLocation startLocation, final GeoLocation endLocation) throws
	// LocationNotInMap,
	// CloneNotSupportedException {
	// System.out.println("Starting calculation for Route. - " /* + Calendar.getInstance() */);
	// System.out.println("Direct Path.");
	// System.out.println("START\t" + startLocation.toReport());
	// System.out.println("END\t" + endLocation.toReport());
	// System.out.println();
	// this.loadWinds(startLocation);
	// this.loadWinds(endLocation);
	// this.loadWinds(new GeoLocation(2.0, 101.0));
	// System.out.println();
	// final WindMap actualMap = this.getActiveMap();
	// final Route directRoute = actualMap.directRoute(startLocation, endLocation);
	// System.out.println("Direct Route");
	// System.out.println(directRoute.printReport());
	// System.out.println();
	//
	// actualMap.optimizeRoute(startLocation, endLocation);
	// // System.out.println("Time at end - "/* + Calendar.getInstance() */);
	// }

	// protected Route selectDirectRoute(GeoLocation startLocation, GeoLocation endLocation) throws
	// LocationNotInMap {
	// // - Locate the wind cells that contains the start and end locations.
	// Date now = GregorianCalendar.getInstance().getTime();
	// now = addElapsed(now, -2.0);
	// double elapsed = 0.0;
	// final WindCell startCell = this.cellWithPoint(startLocation, now);
	// // - Get the cell now but recalculate later.
	// WindCell endCell = this.cellWithPoint(endLocation, now);
	//
	// // - Buildup the route to the end from the start cell.
	// final Route directRoute = new Route();
	// Vector<Intersection> intersections = this.calculateIntersection(startLocation, endLocation, startCell);
	// directRoute.add(startCell, intersections.lastElement().getDirection(), startLocation,
	// intersections.lastElement()
	// .getLocation());
	// RouteCell routeElement = directRoute.getLast();
	// System.out.println("Adding cell to route: " + routeElement.toString());
	// elapsed += routeElement.getTTC();
	// Date newTime = addElapsed(now, elapsed);
	// // Calendar newNow = GregorianCalendar.getInstance();
	// // newNow.setTimeInMillis(new Double(now.getTime() + elapsed * 60 * 60 * 1000).longValue());
	// // Date newTime = newNow.getTime();
	//
	// // - Get cell one by one following the direct path line.
	// // To get the next cell search the one that contains the end intersection.
	// WindCell nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), startCell, newTime);
	// // - The end cell is never reached becuase the cell maps change over time
	// while (!nextCell.equals(endCell)) {
	// intersections = this.calculateIntersection(startLocation, endLocation, nextCell);
	// directRoute.add(nextCell, intersections.firstElement().getDirection(),
	// intersections.firstElement().getLocation(), intersections.lastElement().getLocation());
	//
	// routeElement = directRoute.getLast();
	// System.out.println("Adding cell to route: " + routeElement.toString());
	// elapsed += routeElement.getTTC();
	// newTime = addElapsed(now, elapsed);
	//
	// nextCell = this.cellWithPoint(intersections.lastElement().getLocation(), nextCell, newTime);
	// endCell = this.cellWithPoint(endLocation, newTime);
	// }
	//
	// routeElement = directRoute.getLast();
	// System.out.println("Adding cell to route: " + routeElement.toString());
	// elapsed += routeElement.getTTC();
	// newTime = addElapsed(now, elapsed);
	// endCell = this.cellWithPoint(endLocation, newTime);
	//
	// intersections = this.calculateIntersection(startLocation, endLocation, endCell);
	// directRoute.add(endCell, intersections.firstElement().getDirection(),
	// intersections.firstElement().getLocation(),
	// endLocation);
	//
	// return directRoute;
	// }

	// public static Date addElapsed(Date startTime, double elapsed) {
	// Calendar newNow = GregorianCalendar.getInstance();
	// newNow.setTimeInMillis(new Double(startTime.getTime() + elapsed * 60 * 60 * 1000).longValue());
	// Date newTime = newNow.getTime();
	// return newTime;
	// }

	// public Vector<Intersection> calculateIntersection(final GeoLocation start, final GeoLocation end,
	// final WindCell targetCell) throws LocationNotInMap {
	// final Vector<Intersection> intersections = new Vector<Intersection>(4);
	// // - Calculate the direct path line.
	// double deltaLat = end.getLat() - start.getLat();
	// double deltaLon = end.getLon() - start.getLon();
	// Quadrants quad = Quadrants.q4Angle(start.angleTo(end));
	//
	// // - Calculate cell limits.
	// final Boundaries boundaries = targetCell.getBoundaries();
	// double lat;
	// double lon;
	// int dum = 1;
	// GeoLocation intersection;
	// if (Quadrants.QUADRANT_I == quad) {
	// // - Solve the line intersections with the direct line.
	// lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getWest());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.W));
	// }
	// lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getSouth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.S));
	// }
	// lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getNorth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.N));
	// }
	// lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getEast());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.E));
	// }
	// return intersections;
	// }
	// if (Quadrants.QUADRANT_IV == quad) {
	// // - Solve the line intersections with the direct line.
	// lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getSouth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.S));
	// }
	// lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getEast());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.E));
	// }
	// lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getWest());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.W));
	// }
	// lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getNorth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.N));
	// }
	// return intersections;
	// }
	// // - Solve the line intersections with the direct line.
	// lat = (deltaLat / deltaLon) * (boundaries.getWest() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getWest());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.W));
	// }
	// lon = (deltaLon / deltaLat) * (boundaries.getNorth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getNorth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.N));
	// }
	// lon = (deltaLon / deltaLat) * (boundaries.getSouth() - start.getLat()) + start.getLon();
	// intersection = new GeoLocation(boundaries.getSouth(), lon);
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.S));
	// }
	// lat = (deltaLat / deltaLon) * (boundaries.getEast() - start.getLon()) + start.getLat();
	// intersection = new GeoLocation(lat, boundaries.getEast());
	// if (targetCell.contains(intersection)) {
	// intersections.add(new Intersection(intersection, Directions.E));
	// }
	// return intersections;
	// }
	//
	public static void printVMC(final GeoLocation target, final int heading) {
		Calendar now = GregorianCalendar.getInstance();
		now.add(Calendar.HOUR, -2);
		try {
			loadWinds(target);
			final WindCell startCell = cellWithPoint(target, now.getTime());
			ExtendedLocation extended = new ExtendedLocation(target.getLat(), target.getLon());
			extended.printVMC(startCell, heading);
		} catch (final LocationNotInMap e) {
			e.printStackTrace();
		}
	}

	// private WindMap getActiveMap() {
	// // - Select the right map depending on the current time.
	// final Date now = Calendar.getInstance().getTime();
	// final Enumeration<Date> mit = this.windMaps.keys();
	// Date lastReference = null;
	// WindMap activeMap = null;
	// while (mit.hasMoreElements()) {
	// final Date mapReference = mit.nextElement();
	// lastReference = mapReference;
	// if (now.after(mapReference)) {
	// activeMap = this.windMaps.get(mapReference);
	// // return activeMap;
	// }
	// }
	// if (null == activeMap) activeMap = this.windMaps.get(lastReference);
	// return activeMap;
	// }

	private static WindMap getActiveMap(Date selectTime) {
		// - Select the right map depending on passing time by the cell.
		Vector<Date> keyVector = new Vector<Date>(8);
		keyVector.addAll(windMaps.keySet());
		Collections.sort(keyVector);
		Iterator<Date> mit = keyVector.iterator();
		Date lastReference = null;
		Date selectedReference = null;
		WindMap activeMap = null;
		while (mit.hasNext()) {
			Date mapReference = mit.next();
			lastReference = mapReference;
			if (null == selectedReference) selectedReference = lastReference;
			if (selectTime.after(mapReference)) {
				if (mapReference.before(selectedReference)) {
					if (lastReference.before(selectedReference)) selectedReference = lastReference;
				} else {
					activeMap = windMaps.get(mapReference);
					selectedReference = mapReference;
				}
			}
		}
		if (null == activeMap) activeMap = windMaps.get(lastReference);
		return activeMap;
	}

	public static WindCell cellWithPoint(final GeoLocation target, Date selectTime) throws LocationNotInMap {
		if (null == windMaps) {
			// - The map is empty so load the first point.
			windMaps = new Hashtable<Date, WindMap>();
			loadWinds(target);
		}
		final WindMap activeMap = getActiveMap(selectTime);
		// - If the map is null it can be because of error or that the cell data is not loaded.
		if (null != activeMap)
			try {
				// final WindCell cell =
				// cell.setMapReference(this); // Add map reference to cell
				return activeMap.cellWithPoint(target);
			} catch (final LocationNotInMap lnime) {
				// lnime.printStackTrace();
				// - Search for more map data from the game site.
				loadWinds(target);
				return activeMap.cellWithPoint(target);
			}
		else
			throw new LocationNotInMap("The " + target.toString() + " location is not contained inside the Wind Map.");
	}

	public static WindCell cellWithPoint(final GeoLocation target, final WindCell skip, Date selectTime)
			throws LocationNotInMap {
		if (null == windMaps) {
			// - The map is empty so load the first point.
			windMaps = new Hashtable<Date, WindMap>();
			loadWinds(target);
		}
		final WindMap activeMap = getActiveMap(selectTime);
		if (null != activeMap)
			try {
				return activeMap.cellWithPoint(target, skip);
				// int dum = 1;
				// // - Add map reference to cell
				// cell.setMapReference(this);
				// return cell;
			} catch (final LocationNotInMap lnime) {
				// lnime.printStackTrace();
				// - Search for a new map.
				loadWinds(target);
				return activeMap.cellWithPoint(target, skip);
			}
		else
			throw new LocationNotInMap("The " + target.toString() + " location is not contained inside the Route Map.");
	}

	public static void clear() {
		windMaps = null;
	}
}

// - UNUSED CODE ............................................................................................
