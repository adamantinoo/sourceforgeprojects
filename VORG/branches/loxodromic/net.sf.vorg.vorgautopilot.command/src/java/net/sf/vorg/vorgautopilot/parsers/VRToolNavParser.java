//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.parsers;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import net.sf.vorg.vorgautopilot.models.Boat;
import net.sf.vorg.vorgautopilot.models.PilotCommand;
import net.sf.vorg.vorgautopilot.models.PilotLimits;

// - CLASS IMPLEMENTATION ...................................................................................
public class VRToolNavParser {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.parsers");

	// - F I E L D - S E C T I O N ............................................................................
	private final Boat		boatRef;
	private String				inputReference;
	private PilotCommand	buildUpCommand;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public VRToolNavParser(Boat boat) {
		boatRef = boat;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void setInput(String fileName) {
		inputReference = fileName;
	}

	/*
	 * Read the input file and convert each line to a list of attributes like the XML parsers. Then send that
	 * information to the <code>startElement</code> method for processing and data acquisition.
	 */
	public void parse(Boat refBoat) {
		// - Open the input file.
		try {
			BufferedReader input = new BufferedReader(new FileReader(inputReference));
			String line = input.readLine();
			while (null != line) {
				if (detectPilotRoute(line)) if (parseBoatName(line)) processWaypoints(input);
				line = input.readLine();
			}
		} catch (FileNotFoundException fnfe) {
			System.out.println(fnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	private void processWaypoints(BufferedReader input) {
		try {
			String line = input.readLine();
			while (null != line) {
				String[] sections = line.split(";");
				String type = sections[0];
				if (type.equals("P")) {
					boolean waypointPresent = false;
					NavAttributes attr = new NavAttributes();
					String name = "waypoint";
					attr.add("latitude", sections[1]);
					// - Coordinate system on VRTool is not the same as the game. Change the sign of the longitude.
					attr.add("longitude", changeSign(sections[2]));

					// - Detect and parse the section 3 that contains the waypoint attributes
					if (sections.length > 3) // - Parse the waypoint attributes
						waypointPresent = parseWaypointAttributes(sections[3], attr);
					if (sections.length > 4) attr.add("date", sections[4]);
					if (waypointPresent) startElement(name, attr);
				}
				line = input.readLine();
				if (line.startsWith("O;")) line = null;
			}
			// - Close the pilot command.
			endElement("pilotcommand");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}

	private String changeSign(String value) {
		double detectedValue = 0.0;
		if (null == value) return "0.0";
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = value.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1)
				detectedValue = new Double(value).doubleValue();
			else {
				final int degree = new Integer(value.substring(0, pos)).intValue();
				final int minute = new Integer(value.substring(pos + 1, value.length())).intValue();
				// - Test for negative values.
				if (degree < 0)
					detectedValue = degree * 1.0 - minute / 60.0;
				else
					detectedValue = degree * 1.0 + minute / 60.0;
			}
		} catch (final Exception ex) {
			detectedValue = 0.0;
		}
		return new Double(detectedValue * -1.0).toString();
	}

	private boolean parseWaypointAttributes(String text, NavAttributes attr) {
		// - Check that the string contains a valid list of attributes. Search for the key type.
		if (text.toLowerCase().contains("type")) {
			String[] parameters = text.split(" ");
			for (String parameter : parameters) {
				if ((null == parameter) || (parameter.equals(""))) continue;
				String[] values = parameter.split("=");
				// - Check for bad formed parameters.
				if (values.length < 2) continue;
				if (null == values[0]) continue;
				if (null == values[1]) continue;
				attr.add(values[0], values[1].replace('"', ' ').trim());
			}
			System.out.println("Waypoint detected and OK. Contents. " + text);
			return true;
		} else
			System.out.println("Waypoint detected but not properly configured. Skipped. Contents=" + text);
		return false;
	}

	private boolean parseBoatName(String line) {
		String[] sections = line.split(";");
		String boatName = sections[2];
		boatName = boatName.substring(3);

		// - Read the file with the identification information.
		Properties idprops = new Properties();
		try {
			idprops.load(new BufferedInputStream(new FileInputStream(boatName + ".ident")));
			System.out.println("Detected configuration Route for boat name=" + boatName);
		} catch (FileNotFoundException e) {
			System.out.println("Identification file for " + boatName + " not found.");
			return false;
		} catch (IOException e) {
			System.out.println("Identification file for " + boatName + " load error.");
			return false;
		}
		startElement("user", new NavAttributes(idprops));

		NavAttributes attr = new NavAttributes();
		attr.add("name", boatName);
		startElement("boat", attr);
		startElement("waypointlist", new NavAttributes());
		return true;
	}

	private boolean detectPilotRoute(String line) {
		String[] sections = line.split(";");
		String type = sections[0];
		if (type.equals("O")) {
			String name = sections[1];
			if (name.equals("Route")) {
				// - Get the name of the boat from the suffix.
				String boatName = sections[2];
				if (boatName.toUpperCase().startsWith("AP_")) return true;
			}
		}
		return false;
	}

	public void startElement(final String name, final Attributes attributes) {
		// - Store the information for the user account for login purposes
		if (name.toLowerCase().equals("user")) {
			boatRef.setEmail(attributes.getValue("useremail"));
			boatRef.setUserid(attributes.getValue("userid"));
			boatRef.setPassword(attributes.getValue("password"));
			boatRef.setClef(attributes.getValue("clef"));
		}
		// - Get the boat name to be managed.
		if (name.toLowerCase().equals("boat")) {
			boatRef.setName(attributes.getValue("name"));
			buildUpCommand = new PilotCommand("ROUTE", boatRef);
			buildUpCommand.startElement(name, attributes);
			final PilotLimits newLimit = new PilotLimits();
			newLimit.setBorder("NOBORDER");
			newLimit.setAction("connect");
			buildUpCommand.addLimit(newLimit);
		}

		// - Read and create the list of commands to be processed for the boat.
		if (name.toLowerCase().equals("pilotcommand")) {
			buildUpCommand = new PilotCommand(attributes.getValue("type"), boatRef);
			buildUpCommand.startElement(name, attributes);
		}
		if (name.toLowerCase().equals("pilotlimits")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("limit")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("waypointlist")) buildUpCommand.startElement(name, attributes);
		if (name.toLowerCase().equals("waypoint")) buildUpCommand.startElement(name, attributes);
	}

	public void endElement(String name) throws SAXException {
		if (name.toLowerCase().equals("pilotcommand")) boatRef.addCommand(buildUpCommand);
	}
}

class NavAttributes implements Attributes {
	private final Hashtable<String, String>	contents	= new Hashtable<String, String>();

	public NavAttributes(Properties props) {
		Enumeration<?> penu = props.propertyNames();
		while (penu.hasMoreElements()) {
			String propName = (String) penu.nextElement();
			contents.put(propName, props.getProperty(propName));
		}
	}

	public NavAttributes() {
		// TODO Auto-generated constructor stub
	}

	public void add(String name, String value) {
		contents.put(name, value);
	}

	public int getIndex(String name) {
		return 0;
	}

	public int getIndex(String uri, String localName) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLength() {
		return contents.size();
	}

	public String getLocalName(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getQName(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(String uri, String localName) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getURI(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getValue(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getValue(String name) {
		return contents.get(name);
	}

	public String getValue(String uri, String localName) {
		// TODO Auto-generated method stub
		return null;
	}
}
// - UNUSED CODE ............................................................................................
