//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.util.Formatter;
import java.util.Locale;

import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.ExtendedLocation;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.internals.VMCData;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;

// - CLASS IMPLEMENTATION ...................................................................................
public class ProjectedWaypoint extends Waypoint {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.vorgautopilot.models");

	// - F I E L D - S E C T I O N ............................................................................
	private final Waypoint	reference;
	private final double		angle;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ProjectedWaypoint(final String value, final int setAWD) {
		super(value);
		reference = this;
		angle = setAWD;
	}

	public ProjectedWaypoint(final String value, final Waypoint refWaypoint) {
		super(value);
		reference = refWaypoint;
		angle = reference.getLocation().angleTo(getLocation());
	}

	@Override
	public String activationReport(final ExtendedLocation boatLocation) {
		// NumberFormat nf = NumberFormat.getInstance();
		final double boatAngle = GeoLocation.adjustAngleTo360(getLocation().angleTo(boatLocation) - angle);
		return toString() + "\n[boat angle= " + boatAngle + "]";
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/*
	 * A waypoint of this type is surpassed if the perpendicular line that connect this point with the preceding
	 * and reference point is passed. This is a complex task because can be lines to be crossed in the four
	 * quadrants.<br> The best implementation is to test the angle to the waypoint from the boat. If it is more
	 * than 90.0 degrees from the Reference-Waypoint line then we have passed the point.
	 */
	@Override
	public boolean isSurpassed(final GeoLocation boatLocation) {
		if (getType() == WaypointTypes.PROJECTED) {
			final double referenceAngle = angle;
			final double boatAngle = GeoLocation.adjustAngleTo360(getLocation().angleTo(boatLocation) - referenceAngle);
			if (referenceAngle <= 180.0) {
				if ((boatAngle >= 270.0) || (boatAngle <= 90.0))
					return true;
				else
					return false;
			} else if ((boatAngle >= 90.0) && (boatAngle <= 270.0))
				return true;
			else
				return false;
		}
		if (getType() == WaypointTypes.ANGLE) {
			final double referenceAngle = angle;
			// double boatAngle = GeoLocation.adjustAngle(boatLocation.angleTo(getLocation()) - referenceAngle);
			final double boatAngle = GeoLocation.adjustAngleTo360(getLocation().angleTo(boatLocation) - referenceAngle);
			if (referenceAngle <= 180.0) {
				if ((boatAngle >= 270.0) || (boatAngle <= 90.0)) {
					System.out.println("--- Discarded waypoint " + getLocation() + ". Crossed line with angle " + boatAngle);
					return true;
				} else
					return false;
			} else if ((boatAngle >= 90.0) && (boatAngle <= 270.0)) {
				System.out.println("--- Discarded waypoint " + getLocation() + ". Crossed line with angle " + boatAngle);
				return true;
			} else
				return false;
		} else
			return super.isSurpassed(boatLocation);
	}

	@Override
	public void sendCommand(final Boat boat) throws LocationNotInMap {
		// final ExtendedLocation boatLocation = new ExtendedLocation(boat.getLocation());
		if (getType() == WaypointTypes.PROJECTED) {
			doCommand(boat);
			return;
		}
		if (getType() == WaypointTypes.ANGLE) {
			doCommand(boat);
			return;
		}
		super.sendCommand(boat);
	}

	public void setMaxAWD(final String value) {
		if (null == value) {
			maxAWD = 180;
			return;
		}
		try {
			maxAWD = new Integer(value).intValue();
		} catch (final Exception e) {
			maxAWD = 180;
		}
	}

	private int adjustPortAWD(final int windDirection, final int heading) {
		int thisAWD = GeoLocation.calculateAWD(windDirection, heading);
		if (thisAWD < 0) thisAWD *= -1;
		if (thisAWD < minAWD) return heading + (minAWD - thisAWD);
		if (thisAWD > maxAWD) return heading - (thisAWD - maxAWD);
		return heading;
	}

	private int adjustStartboardAWD(final int windDirection, final int heading) {
		int thisAWD = GeoLocation.calculateAWD(windDirection, heading);
		if (thisAWD < 0) thisAWD *= -1;
		if (thisAWD < minAWD) return heading - (minAWD - thisAWD);
		if (thisAWD > maxAWD) return heading + (thisAWD - maxAWD);
		return heading;
	}

	private void doCommand(final Boat boat) throws LocationNotInMap {
		final ExtendedLocation boatLocation = new ExtendedLocation(boat.getLocation());
		final WindCell windCell = WindMapHandler.getWindCell(boatLocation);
		final VMCData vmc = new VMCData(angle, windCell);

		// - Check if current boat configuration is the same as requested.
		System.out.println();
		System.out.println(vmc.printReport());
		System.out.println();
		final BoatCommand newBoatCommand = new BoatCommand();
		int heading = vmc.getBestAngle();
		// - Get the board of the wind.
		if ((windCell.getWindDir() - heading) > 0)
			heading = adjustStartboardAWD(windCell.getWindDir(), heading); // Starboard
		else
			heading = adjustPortAWD(windCell.getWindDir(), heading); // Port

		// - Bug. Get the new sail configuration after the heading adjust.
		final int apparentHeading = new Double(GeoLocation.calculateAWD(windCell.getWindDir(), heading)).intValue();
		final SailConfiguration sails = Polars.lookup(apparentHeading, windCell.getWindSpeed());
		// final NumberFormat nf = NumberFormat.getInstance();
		// nf.setMaximumFractionDigits(3);
		Formatter formatter = new Formatter(Locale.ENGLISH);
		formatter.format("[Adjusted angle=%1$03d - AWD=%2$03d]", heading, apparentHeading);
		System.out.println(formatter);
		System.out.println(sails);
		System.out.println();

		newBoatCommand.setHeading(boat.getHeading(), heading);
		newBoatCommand.setSails(sails);
		newBoatCommand.sendCommand(boat);
	}
}

// - UNUSED CODE ............................................................................................
