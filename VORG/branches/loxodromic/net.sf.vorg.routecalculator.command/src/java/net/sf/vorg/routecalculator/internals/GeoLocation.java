//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.internals;

import java.text.NumberFormat;
import java.util.Formatter;
import java.util.Locale;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.enums.Quadrants;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class GeoLocation {
	// - S T A T I C - S E C T I O N ..........................................................................
	// private static Logger logger = Logger.getLogger("net.sf.vorg.routecalculator.internals");

	public static double adjustAngleTo360(final double heading) {
		if (heading == 0.0) return 360.0;
		if (heading > 360.0) return GeoLocation.adjustAngleTo360(heading - 360.0);
		if (heading < 0.0) return GeoLocation.adjustAngleTo360(heading + 360.0);
		return heading;
	}

	public static int adjustAngleTo360(final int heading) {
		if (heading == 0) return 360;
		if (heading > 360) return GeoLocation.adjustAngleTo360(heading - 360);
		if (heading < 0) return GeoLocation.adjustAngleTo360(heading + 360);
		return heading;
	}

	public static int calculateAWD(double windAngle, double boatAngle) {
		double angle = windAngle - boatAngle;
		if (angle > 180) {
			angle = angle - 360;
		}
		return new Long(Math.round(angle)).intValue();
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected double	latitude	= 0.0;
	protected double	longitude	= 0.0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GeoLocation() {
	}

	public GeoLocation(final double lat, final double lon) {
		latitude = lat;
		longitude = lon;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public GeoLocation(final int latGrade, final int latMinute, final int lonGrade, final int lonMinute) {
		latitude = latGrade * 1.0 + latMinute / 60.0;
		longitude = lonGrade * 1.0 + lonMinute / 60.0;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * This method calculates the angle that forms a destination point with a start point. The resulting value
	 * may be a positive or a negative number. 0-360 normalization should be performed at the call level.
	 */
	public double angleTo(final GeoLocation destination) {
		double alpha = 0.0;
		final double deltaLat = destination.getLat() - this.getLat();
		double deltaLon = destination.getLon() - this.getLon();
		// - Control the change over the date line.
		if (Math.abs(deltaLon) > 180.0) deltaLon = 360.0 - Math.abs(deltaLon);
		final double hyp = Math.hypot(deltaLat, deltaLon);

		// - In the case the origin and destination are the same we return by default the angle 0.
		if ((deltaLat == 0.0) && (deltaLon == 0.0)) return 0.0;

		Quadrants quadrant = Quadrants.QUADRANT_I;
		// - Detect the angle quadrant.
		if ((deltaLat >= 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_I;
		if ((deltaLat < 0.0) & (deltaLon >= 0.0)) quadrant = Quadrants.QUADRANT_II;
		if ((deltaLat < 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_III;
		if ((deltaLat >= 0.0) & (deltaLon < 0.0)) quadrant = Quadrants.QUADRANT_IV;

		switch (quadrant) {
			case QUADRANT_I:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_II:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.PI - Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_III:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.PI - Math.asin(deltaLon / hyp);
				else
					alpha = 2.0 * Math.PI - Math.acos(deltaLat / hyp);
				break;
			case QUADRANT_IV:
				// -Use the bigger value to have a more exact result.
				if (Math.abs(deltaLon) > Math.abs(deltaLat))
					alpha = Math.asin(deltaLon / hyp);
				else
					alpha = Math.acos(deltaLat / hyp) * -1.0;
				break;

			default:
				break;
		}

		return GeoLocation.adjustAngleTo360(Math.toDegrees(alpha));
	}

	public double distance(final GeoLocation destination) {
		final double deltaLat = destination.getLat() - this.getLat();
		final double deltaLon = destination.getLon() - this.getLon();
		final double hyp = Math.hypot(deltaLat, deltaLon);
		double Ls = Math.toRadians(this.getLat());
		double Ld = Math.toRadians(destination.getLat());
		double ldelta = Math.toRadians(deltaLon);
		double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		return loxDistance;
	}

	public double getLat() {
		return latitude;
	}

	public double getLon() {
		return longitude;
	}

	public boolean isEquivalent(final GeoLocation targetLocation) {
		if (Math.abs(targetLocation.getLat() - this.getLat()) < VORGConstants.DIFFERENTIAL)
			if (Math.abs(targetLocation.getLon() - this.getLon()) < VORGConstants.DIFFERENTIAL) return true;
		return false;
	}

	public void setLat(final double lat) {
		latitude = lat;
	}

	public void setLat(final String newLatitude) {
		if (null == newLatitude) {
			this.setLat(0.0);
			return;
		}
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = newLatitude.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1)
				this.setLat(new Double(newLatitude).doubleValue());
			else {
				final int degree = new Integer(newLatitude.substring(0, pos)).intValue();
				final int minute = new Integer(newLatitude.substring(pos + 1, newLatitude.length())).intValue();
				// - Test for negative values.
				if (degree < 0)
					this.setLat(degree * 1.0 - minute / 60.0);
				else
					this.setLat(degree * 1.0 + minute / 60.0);
			}
		} catch (final Exception ex) {
			this.setLat(0.0);
		}
	}

	public void setLocation(final GeoLocation newLocation) {
		this.latitude = newLocation.getLat();
		longitude = newLocation.getLon();
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public void setLon(final String newLongitude) {
		if (null == newLongitude) {
			this.setLon(0.0);
			return;
		}
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = newLongitude.indexOf(":");
			// - The longitude comes in double format
			if (pos < 1)
				this.setLon(new Double(newLongitude).doubleValue());
			else {
				final int degree = new Integer(newLongitude.substring(0, pos)).intValue();
				final int minute = new Integer(newLongitude.substring(pos + 1, newLongitude.length())).intValue();
				// - Test for negative values.
				if (degree < 0)
					this.setLon(degree * 1.0 - minute / 60.0);
				else
					this.setLon(degree * 1.0 + minute / 60.0);
			}
		} catch (final Exception ex) {
			this.setLon(0.0);
		}
	}

	public void setLon(final double lon) {
		longitude = lon;
		// - BUG Related to the crossing of the date line.
		if (Math.abs(longitude) > 180.0) longitude -= 360.0 * Math.signum(longitude);
	}

	public String toReport() {
		final StringBuffer buffer = new StringBuffer();
		// Formatter formatter = new Formatter(Locale.ENGLISH);
		int latDegree;
		long latMinute;
		// - Generate the numbers.
		if (latitude < 0) {
			latDegree = new Double(Math.floor(latitude * -1.0)).intValue();
			latMinute = Math.round(((latitude * -1.0) - Math.floor(latitude * -1.0)) * 60.0);
		} else {
			latDegree = new Double(Math.floor(latitude)).intValue();
			latMinute = Math.round((latitude - Math.floor(latitude)) * 60.0);
		}
		int lonDegree;
		long lonMinute;
		if (longitude < 0) {
			lonDegree = new Double(Math.floor(longitude * -1.0)).intValue();
			lonMinute = Math.round(((longitude * -1.0) - Math.floor(longitude * -1.0)) * 60.0);
		} else {
			lonDegree = new Double(Math.floor(longitude)).intValue();
			lonMinute = Math.round((longitude - Math.floor(longitude)) * 60.0);
		}

		// - Generate the output.
		Formatter formatter = new Formatter(Locale.ENGLISH);
		formatter.format("%1$03d� %2$02d'", latDegree, latMinute);
		if (latitude > 0)
			buffer.append(formatter).append(" N");
		else
			buffer.append(formatter).append(" S");
		buffer.append('\t');
		formatter = new Formatter(Locale.ENGLISH);
		formatter.format("%1$03d� %2$02d'", lonDegree, lonMinute);
		if (longitude > 0)
			buffer.append(formatter).append(" E");
		else
			buffer.append(formatter).append(" W");
		return buffer.toString();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[GeoLocation ");
		NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
		nf.setMaximumFractionDigits(4);
		nf.setMinimumFractionDigits(4);
		buffer.append("lat=").append(nf.format(latitude)).append(",");
		buffer.append("lon=").append(nf.format(longitude)).append("]");
		buffer.append(" [").append(this.toReport().replace('\t', ' ').replace('�', ' ')).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
