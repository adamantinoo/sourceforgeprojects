//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.routecalculator.parsers;

// - IMPORT SECTION .........................................................................................
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.logging.Logger;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMap;

// - CLASS IMPLEMENTATION ...................................................................................
public class MapParserHandler extends DefaultHandler {
	private static final double					KN2KNOTS		= 0.53995680346;

	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger								logger			= Logger.getLogger("net.sf.vorg.routecalculator.models");

	// - F I E L D - S E C T I O N ............................................................................
	// protected WindMap mapTarget;
	// protected Hashtable<String, WindMap> mapList = new Hashtable<String, WindMap>();
	protected Hashtable<Date, WindMap>	mapTarget;
	private WindMap											currentMap	= null;

	// protected WindMap firstMap;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MapParserHandler(Hashtable<Date, WindMap> windMaps) {
		mapTarget = windMaps;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		if (name.equals("PREVISION")) {
			// - Convert the reference to a Date for boundary management.
			String mapRef = attributes.getValue("DATE");
			try {
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date mapDate = df.parse(mapRef);

				// - Check if we have registered a map with that reference.
				currentMap = mapTarget.get(mapDate);
				if (null == currentMap) {
					currentMap = new WindMap();
					currentMap.setRef(mapRef);
					currentMap.setTimeStamp(mapDate);
					mapTarget.put(mapDate, currentMap);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (name.equals("M")) {
			if (null == currentMap) return;
			String lat = attributes.getValue("LAT");
			String lon = attributes.getValue("LON");
			String heading = attributes.getValue("D");
			double speed = Double.parseDouble(attributes.getValue("V")) * KN2KNOTS;
			// speed = Math.round(speed * 10.0) / 10.0;
			currentMap.addCell(new WindCell(Integer.parseInt(lat), Integer.parseInt(lon), Integer.parseInt(heading), speed));
		}
	}

	// public WindMap getMaps() {
	// return firstMap;
	// }
}
// - UNUSED CODE ............................................................................................
