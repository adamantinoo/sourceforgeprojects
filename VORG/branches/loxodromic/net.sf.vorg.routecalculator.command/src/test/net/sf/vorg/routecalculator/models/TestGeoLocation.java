package net.sf.vorg.routecalculator.models;

import java.util.Formatter;
import java.util.Locale;

import junit.framework.TestCase;
import net.sf.vorg.routecalculator.internals.GeoLocation;

public class TestGeoLocation extends TestCase {

	/**
	 * Check the calculations for all the main angles. The resulting value may be a positive or a negative
	 * number. 0-360 normalization should be performed at the call level.
	 */
	public void testAngleTo() {
		GeoLocation start = new GeoLocation(10, 0, 10, 0);
		// GeoLocation end = new GeoLocation(15, 0, 10, 0);
		double alpha = start.angleTo(new GeoLocation(12, 0, 10, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 11, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(11, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(10, 0, 12, 0));

		alpha = start.angleTo(new GeoLocation(9, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 12, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 11, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 10, 0));

		alpha = start.angleTo(new GeoLocation(8, 0, 9, 0));
		alpha = start.angleTo(new GeoLocation(8, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(9, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(10, 0, 8, 0));

		alpha = start.angleTo(new GeoLocation(11, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 8, 0));
		alpha = start.angleTo(new GeoLocation(12, 0, 9, 0));
		alpha = start.angleTo(new GeoLocation(120, 0, 10, 0));
		// assertEquals(74.0546, alpha, 0.001);
		start = new GeoLocation(-2.500000, 157.295949);
		alpha = start.angleTo(new GeoLocation(-2.500000, 157.517088));
	}

	public void testReport() {
		final StringBuffer buffer = new StringBuffer();
		GeoLocation start = new GeoLocation(-2.500000, 157.295949);
		double latitude = start.getLat();
		Formatter formatter = new Formatter(Locale.ENGLISH);
		Formatter result = formatter.format("%1$03d", new Double(Math.floor(latitude * -1.0)).intValue());
		int dum = 0;
	}
}
