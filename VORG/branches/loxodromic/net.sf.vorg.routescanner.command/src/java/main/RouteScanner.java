//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

//- IMPORT SECTION .........................................................................................
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.core.exceptions.DataLoadingException;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.VMCData;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.WindCell;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.routescanner.models.Scanner;
import net.sf.vorg.vorgautopilot.models.Boat;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteScanner {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger					= Logger.getLogger("net.sf.vorg.routecalculator.command.main");
	private static final String	APPLICATIONNAME	= "RouteScanner";
	/* Reference to the static part of the application */
	private static RouteScanner	singleton;
	private static boolean			onDebug					= false;
	private static PrintWriter	printer;
	static {
		RouteScanner.logger.setLevel(Level.OFF);
	}

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		RouteScanner.singleton = new RouteScanner(args);
		RouteScanner.singleton.execute();
		RouteScanner.exit(0);
	}

	public static boolean onDebug() {
		return RouteScanner.onDebug;
	}

	public static void output() {
		RouteScanner.output("\n");
	}

	public static void output(final String message) {
		System.out.println(message);
		if (null == RouteScanner.printer)
			try {
				RouteScanner.printer = new PrintWriter(new FileOutputStream("RouteScanner.output.txt", true), true);
				RouteScanner.printer.println(message);
				RouteScanner.printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			RouteScanner.printer.println(message);
			RouteScanner.printer.flush();
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation	startLocation		= new GeoLocation();
	private int					cellsToScan			= 10;
	private int					maxAWD					= 120;
	private String			boatName;
	private String			navigationFile	= null;
	private boolean			activeOutput		= true;
	private boolean			activeMax				= false;
	private boolean			activeAWD				= false;
	private boolean			activeVMG				= false;
	private int					VMGAngle				= 90;
	private boolean			starboardSide		= true;
	private int					searchAngle			= 90;
	private boolean			activeSearch		= false;
	private Boat				boat;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/*
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are: <ul> <li><b>-conf<font
	 * color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where the application will
	 * expect the configuration files and data.</li> <li><b>-res<font color="GREY">[ourcesLocation</font></b>
	 * ${RESOURCEDIR} - is the directory where the application is going to locate the files that contains the
	 * SQL statements and other application resources.
	 */
	public RouteScanner(final String[] args) {
		// - Initialize log and print out the banner
		banner();
		// - Process parameters and store them into the instance fields
		processParameters(args, RouteScanner.APPLICATIONNAME);
	}

	public void processParameters(final String[] args, final String ApplicationName) {
		for (int i = 0; i < args.length; i++) {
			RouteScanner.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].toLowerCase().startsWith("-help")) {
					help();
					RouteScanner.exit(0);
				}
				if (args[i].startsWith("-sta")) { //$NON-NLS-1$
					startLocation.setLat(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					startLocation.setLon(argumentGeopositionValue(argumentStringValue(args, i)));
					i++;
					// startLocation = new GeoLocation(latGrade, latMinute, lonGrade, lonMinute);
					continue;
				}
				if (args[i].toLowerCase().startsWith("-boat")) { //$NON-NLS-1$
					boatName = argumentStringValue(args, i);
					i++;
					boat = new Boat();
					boat.setName(boatName);
					try {
						boat.getBoatData();
						startLocation = boat.getLocation();
					} catch (DataLoadingException dle) {
						System.out.println(dle.getMessage());
						exit(VORGConstants.GENERICERROR);
					}
					continue;
				}
				if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
					RouteScanner.onDebug = true;
					continue;
				}
				if (args[i].startsWith("-port")) { //$NON-NLS-1$
					starboardSide = false;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-nav")) { //$NON-NLS-1$
					navigationFile = argumentStringValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-scan")) { //$NON-NLS-1$
					cellsToScan = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-cell")) { //$NON-NLS-1$
					cellsToScan = argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-max")) { //$NON-NLS-1$
					activeMax = true;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-no")) { //$NON-NLS-1$
					activeOutput = false;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-AWD")) { //$NON-NLS-1$
					maxAWD = argumentIntegerValue(args, i);
					activeAWD = true;
					i++;
					continue;
				}
				if (args[i].toUpperCase().startsWith("-VMG")) { //$NON-NLS-1$
					VMGAngle = argumentIntegerValue(args, i);
					activeVMG = true;
					i++;
					continue;
				}
				if (args[i].toLowerCase().startsWith("-search")) { //$NON-NLS-1$
					searchAngle = argumentIntegerValue(args, i);
					activeSearch = true;
					i++;
					continue;
				}
			}
		}
		// ... Check that required parameters have values.
		if (null == startLocation) RouteCalculator.exit(VORGConstants.NOCONFIG);
	}

	private double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	private double argumentGeopositionValue(final String positionData) {
		if (null == positionData) return 0.0;
		try {
			// - Divide the data into the degree and the minutes.
			final int pos = positionData.indexOf(":");
			// - The latitude comes in double format
			if (pos < 1)
				return new Double(positionData).doubleValue();
			else {
				final int degree = new Integer(positionData.substring(0, pos)).intValue();
				final int minute = new Integer(positionData.substring(pos + 1, positionData.length())).intValue();
				// - Test for negative values.
				// - Check for negative values but in the range from 0 to 1 degree.
				if (positionData.substring(0, 1).equals("-"))
					return degree * 1.0 - minute / 60.0;
				else if (degree < 0)
					return degree * 1.0 - minute / 60.0;
				else
					return degree * 1.0 + minute / 60.0;
			}
		} catch (final Exception ex) {
			return 0.0;
		}
	}

	private int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	private String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			RouteCalculator.exit(VORGConstants.NOCONFIG);
		return "";
	}

	private void banner() {
		System.out.println("  ____             _       _____ _           _           ");
		System.out.println(" |  _ \\ ___  _   _| |_ ___|  ___(_)_ __   __| | ___ _ __ ");
		System.out.println(" | |_) / _ \\| | | | __/ _ \\ |_  | | '_ \\ / _` |/ _ \\ '__|");
		System.out.println(" |  _ < (_) | |_| | ||  __/  _| | | | | | (_| |  __/ |   ");
		System.out.println(" |_| \\_\\___/ \\__,_|\\__\\___|_|   |_|_| |_|\\__,_|\\___|_|   ");
		System.out.println("");
		System.out.println();
		System.out.println(VORGConstants.VERSION);
		System.out.println();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	private void execute() {
		final Scanner theRouter = new Scanner();
		if (activeMax) {
			// - Generate the max speed route
			final Route maxSpeedRoute = theRouter.getMaxSpeed(startLocation, cellsToScan);
			RouteScanner.output("Max Speed Route");
			RouteScanner.output(maxSpeedRoute.printReport());
			RouteScanner.output();
		}

		if (activeAWD) {
			// - Generate the AWD route if the AWD parameter is present on the input.
			final Route awdRoute = theRouter.getAWDRoute(startLocation, cellsToScan, maxAWD);
			awdRoute.setName("AWD Route - " + maxAWD);
			// Calendar now = GregorianCalendar.getInstance();
			// output("AWD Route - " + maxAWD + " [" + now.getTime() + "]");
			RouteScanner.output(awdRoute.printReport());
			RouteScanner.output();

			// - If the VRTool output file is present, then send the information to the tail of the file.
			final String title = "AWD " + maxAWD;
			if (null != navigationFile) send2VRTool(awdRoute, title);
		}
		if (activeVMG) {
			// - Generate the V<G route if requested.
			final Route vmgRoute = theRouter.getVMGRoute(startLocation, cellsToScan, VMGAngle);
			vmgRoute.setName("VMG Route - " + VMGAngle);
			// Calendar now = GregorianCalendar.getInstance();
			// output("VMG Route - " + VMGAngle + " [" + now.getTime() + "]");
			RouteScanner.output(vmgRoute.printReport());
			RouteScanner.output();

			// - If the VRTool output file is present, then send the information to the tail of the file.
			if (null != navigationFile) send2VRTool(vmgRoute, "VMG " + VMGAngle);
		}
		if (activeSearch) searchSolution();
	}

	private void help() {
		System.out.println("Description:");
		System.out.println("   Utility to generate a route from a start point or from the current location of a boat");
		System.out.println("   following the max speed route or the AWD for the angle specified.");
		System.out.println();
		System.out.println("Command API for the RouteFinder:");
		System.out.println("   java -classpath routecalculator.jar main.RouteScanner ");
		System.out.println("Allowed parameters:");
		System.out.println("-sta[rt] <lat grade:lat minute> <lon grade:lon minute> or <lat> <lon>");
		System.out.println("-boat <boat name>   -- name of the game boat where to extract the current location");
		System.out.println("-nav <.NAV file path>  -- path to the VRTool file where to store the results");
		System.out.println("-scan <number>  -- number of cells to route");
		System.out.println("-AWD <number>  -- AWD angle to follow in the alternative route");
		System.out.println("Allowed toggles");
		System.out.println("-port -- gets the route using the port side angle");
		System.out.println();
	}

	private void searchSolution() {
		try {
			final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
			nf.setMaximumFractionDigits(6);
			nf.setMinimumFractionDigits(6);
			final Scanner theRouter = new Scanner();
			int searchVMGAngle = searchAngle;
			final WindCell boatCell = WindMapHandler.getWindCell(startLocation);
			final VMCData vmc = new VMCData(searchVMGAngle, boatCell);
			final int bestAngle = vmc.getBestAngle();
			final int searchAWD = GeoLocation.calculateAWD(boatCell.getWindDir(), bestAngle) * -1;
			StringBuffer buffer = new StringBuffer();
			buffer.append("Search List for VMG\n");
			buffer.append("Name\tStart l\tStart L\tEnd l\tEnd L\tAngle\tDistance\tDiff Lat\tDiff Lon\n");
			double maxDistance = 0.0;
			final Vector<Route> records = new Vector<Route>();
			// - Search in the VMG list
			for (int i = -10; i < 11; i++) {
				searchVMGAngle = searchAngle + i;
				final Route vmgRoute = theRouter.getVMGRoute(startLocation, 15, searchVMGAngle);
				vmgRoute.setName("VMG Route - " + searchVMGAngle);
				buffer.append("VMG " + searchVMGAngle).append("\t");
				buffer.append(startLocation.toReport()).append("\t");
				// buffer.append(startLocation.getLat()).append("\t").append(startLocation.getLon()).append("\t");
				final GeoLocation target = vmgRoute.getWindChange(2);
				buffer.append(target.toReport()).append("\t");
				// buffer.append(target.getLat()).append("\t").append(target.getLon()).append("\t");
				final double angle = startLocation.angleTo(target);
				buffer.append(angle).append("\t");
				final double distance = startLocation.distance(target);
				buffer.append(distance).append("\t");
				buffer.append(nf.format(target.getLat() - startLocation.getLat())).append("\t");
				buffer.append(nf.format(target.getLon() - startLocation.getLon())).append("\n");
				if (distance > maxDistance) {
					maxDistance = distance;
					records.add(0, vmgRoute);
				}
			}
			RouteScanner.output(buffer.toString());

			// - Search in the AWD list
			buffer = new StringBuffer();
			buffer.append("\nSearch List for AWD\n");
			buffer.append("Name\tStart l\tStart L\tEnd l\tEnd L\tAngle\tDistance\tDiff Lat\tDiff Lon\n");
			for (int i = -10; i < 11; i++) {
				final int searchAWDAngle = searchAWD + i;
				final Route awdRoute = theRouter.getAWDRoute(startLocation, 15, searchAWDAngle);
				buffer.append("AWD " + searchAWDAngle).append("\t");
				buffer.append(startLocation.toReport()).append("\t");
				// buffer.append(startLocation.getLat()).append("\t").append(startLocation.getLon()).append("\t");
				final GeoLocation target = awdRoute.getWindChange(2);
				buffer.append(target.toReport()).append("\t");
				// buffer.append(target.getLat()).append("\t").append(target.getLon()).append("\t");
				final double angle = startLocation.angleTo(target);
				buffer.append(angle).append("\t");
				final double distance = startLocation.distance(target);
				buffer.append(distance).append("\t");
				buffer.append(nf.format(target.getLat() - startLocation.getLat())).append("\t");
				buffer.append(nf.format(target.getLon() - startLocation.getLon())).append("\n");
				if (distance > maxDistance) {
					maxDistance = distance;
					records.add(0, awdRoute);
				}
			}
			RouteScanner.output(buffer.toString());

			// - Output the two more long routes
			RouteScanner.output(records.get(0).printReport());
			RouteScanner.output(records.get(1).printReport());
		} catch (final LocationNotInMap e) {
			e.printStackTrace();
		}
	}

	// /** Runs the optimizer or the route calculator depending on the specified parameters. */
	// public void execute() {
	// if (null != waypoint)
	// generateVMCRoute(waypoint, cellsToSpan);
	// else if (onlyDirect) {
	// directRoute();
	// } else {
	// optimizeRoute();
	// }
	// }

	// /**
	// * Evaluates the VMC routes and gets the best one for the number of cells specified on the parameters.
	// *
	// * @param cellsToSpan
	// * @param waypoint
	// */
	// private void generateVMCRoute(GeoLocation waypoint, int cellsToSpan) {
	// VMCRouter router = new VMCRouter(waypoint, cellsToSpan);
	// try {
	// router.getBestRoute(startLocation);
	// } catch (LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// private void evaluateCell(final GeoLocation start, final GeoLocation end, final int apparent) {
	// final Router router = new Router(start, end);
	// try {
	// router.evaluateCell(start, apparent);
	// } catch (final LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// private void directRoute() {
	// try {
	// WindMapHandler actualMap = new WindMapHandler();
	// actualMap.directRoute(startLocation, endLocation);
	// } catch (LocationNotInMap lnime) {
	// lnime.printStackTrace();
	// } catch (CloneNotSupportedException cnse) {
	// cnse.printStackTrace();
	// }
	// }
	//
	// private void optimizeRoute() {
	// try {
	// WindMapHandler actualMap = new WindMapHandler();
	// actualMap.optimizeRoute(startLocation, endLocation);
	// } catch (LocationNotInMap lnime) {
	// lnime.printStackTrace();
	// } catch (CloneNotSupportedException cnse) {
	// cnse.printStackTrace();
	// }
	// }

	private void send2VRTool(final Route awdRoute, final String title) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(navigationFile, true));

			// - Compose the report.
			final StringBuffer buffer = new StringBuffer();
			buffer.append("\n");
			buffer.append("O;Route;").append(title).append("\n");
			buffer.append("A;Color=$0000CCFF").append(maxAWD).append("\n");
			buffer.append("A;Visible=TRUE").append("\n");
			buffer.append("A;Linewidth=2").append("\n");
			buffer.append("A;ShowText=TRUE").append("\n");
			buffer.append("A;ShowBoat=FALSE").append("\n");
			buffer.append(awdRoute.vrtoolReport());
			buffer.append("\n");
			writer.write(buffer.toString());
			writer.close();
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// /**
	// * Give the max VMC results for the projection on a given direction. We have to know the wind direction
	// and
	// * the wind speep and those values have to be obtained from some map
	// */
	// private void printVMC(final int heading) {
	// final WindMapHandler actualMap = new WindMapHandler();
	// actualMap.printVMC(startLocation, heading);
	// }
	//
	// private void printVMC(final int heading, final int windDirection, final double windSpeed) {
	// final VMCData vmc = new VMCData(heading, windDirection, windSpeed);
	// RouteCalculator.output(vmc.printRecord());
	// }
	//
	// public static boolean onlyDirect() {
	// return onlyDirect;
	// }
}
// - UNUSED CODE ............................................................................................
