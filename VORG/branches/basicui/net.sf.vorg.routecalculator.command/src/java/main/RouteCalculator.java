//  PROJECT:        net.sf.vorg.routecalculator.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

//- IMPORT SECTION .........................................................................................
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.routecalculator.internals.GeoLocation;
import net.sf.vorg.routecalculator.internals.Polars;
import net.sf.vorg.routecalculator.internals.SailConfiguration;
import net.sf.vorg.routecalculator.internals.VMCData;
import net.sf.vorg.routecalculator.models.Route;
import net.sf.vorg.routecalculator.models.Router;
import net.sf.vorg.routecalculator.models.WindMapHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class RouteCalculator /* extends CommandLineApp */{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger						logger					= Logger.getLogger("net.sf.vorg.routecalculator.command.main");
	private static final String			APPLICATIONNAME	= "RouteCalculator";
	/** Reference to the static part of the application */
	private static RouteCalculator	singleton;
	private static boolean					onDebug					= false;
	private static PrintWriter			printer;
	static {
		RouteCalculator.logger.setLevel(Level.OFF);
	}

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	public static boolean onDebug() {
		return RouteCalculator.onDebug;
	}

	public static void output(final String message) {
		System.out.println(message);
		if (null == RouteCalculator.printer)
			try {
				RouteCalculator.printer = new PrintWriter("RouteCalculator.output.txt");
				RouteCalculator.printer.println(message);
				RouteCalculator.printer.flush();
			} catch (final FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else {
			RouteCalculator.printer.println(message);
			RouteCalculator.printer.flush();
		}
	}

	// - F I E L D - S E C T I O N ............................................................................
	private GeoLocation			startLocation	= null;
	private GeoLocation			endLocation		= null;
	private static boolean	onlyDirect		= false;
	private GeoLocation			waypoint			= null;
	private int							cellsToSpan		= 4;

	// - M A I N S E C T I O N
	public static void main(final String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		RouteCalculator.singleton = new RouteCalculator(args);
		// singleton.execute();
		RouteCalculator.exit(0);
	}

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/**
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are:
	 * <ul>
	 * <li><b>-conf<font color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where
	 * the application will expect the configuration files and data.</li>
	 * <li><b>-res<font color="GREY">[ourcesLocation</font></b> ${RESOURCEDIR} - is the directory where the
	 * application is going to locate the files that contains the SQL statements and other application
	 * resources.
	 */
	public RouteCalculator(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		this.banner();

		// - Process parameters and store them into the instance fields
		this.processParameters(args, RouteCalculator.APPLICATIONNAME);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void processParameters(final String[] args, final String ApplicationName) {
		for (int i = 0; i < args.length; i++) {
			RouteCalculator.logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-help")) {
					this.help();
					RouteCalculator.exit(0);
				}
				if (args[i].startsWith("-sta")) { //$NON-NLS-1$
					final int latGrade = this.argumentIntegerValue(args, i);
					i++;
					final int latMinute = this.argumentIntegerValue(args, i);
					i++;
					final int lonGrade = this.argumentIntegerValue(args, i);
					i++;
					final int lonMinute = this.argumentIntegerValue(args, i);
					i++;
					startLocation = new GeoLocation(latGrade, latMinute, lonGrade, lonMinute);
					continue;
				}
				if (args[i].startsWith("-end")) { //$NON-NLS-1$
					final int latGrade = this.argumentIntegerValue(args, i);
					i++;
					final int latMinute = this.argumentIntegerValue(args, i);
					i++;
					final int lonGrade = this.argumentIntegerValue(args, i);
					i++;
					final int lonMinute = this.argumentIntegerValue(args, i);
					i++;
					endLocation = new GeoLocation(latGrade, latMinute, lonGrade, lonMinute);
					continue;
				}
				if (args[i].startsWith("-way")) { //$NON-NLS-1$
					final int latGrade = this.argumentIntegerValue(args, i);
					i++;
					final int latMinute = this.argumentIntegerValue(args, i);
					i++;
					final int lonGrade = this.argumentIntegerValue(args, i);
					i++;
					final int lonMinute = this.argumentIntegerValue(args, i);
					i++;
					waypoint = new GeoLocation(latGrade, latMinute, lonGrade, lonMinute);
					continue;
				}
				if (args[i].startsWith("-span")) { //$NON-NLS-1$
					cellsToSpan = this.argumentIntegerValue(args, i);
					i++;
					continue;
				}
				if (args[i].startsWith("-debug")) { //$NON-NLS-1$
					RouteCalculator.onDebug = true;
					continue;
				}
				// if (args[i].startsWith("-direct")) { //$NON-NLS-1$
				// onlyDirect = true;
				// continue;
				// }
				if (args[i].startsWith("-VMC")) { //$NON-NLS-1$
					final int heading = this.argumentIntegerValue(args, i);
					i++;
					if (null == startLocation) {
						System.out.println("Start location not specified in the right order. -start before -VMC.");
						RouteCalculator.exit(1);
					}
					this.printVMC(heading);
					if (null != RouteCalculator.printer) RouteCalculator.printer.close();
					RouteCalculator.exit(0);
				}
				if (args[i].startsWith("-VMG")) { //$NON-NLS-1$
					final int heading = this.argumentIntegerValue(args, i);
					i++;
					final int windDir = this.argumentIntegerValue(args, i);
					i++;
					final double windSpeed = this.argumentDoubleValue(args, i);
					i++;
					this.printVMC(heading, windDir, windSpeed);
					RouteCalculator.exit(0);
				}
				if (args[i].startsWith("-polar")) { //$NON-NLS-1$
					final int apparent = this.argumentIntegerValue(args, i);
					i++;
					final double windSpeed = this.argumentDoubleValue(args, i);
					i++;
					final SailConfiguration conf = Polars.lookup(apparent, windSpeed);
					final StringBuffer buffer = new StringBuffer("[Polar search results").append('\n');
					buffer.append("Sail=").append(conf.getSail()).append('\n');
					buffer.append("Speed=").append(conf.getSpeed()).append('\n');
					System.out.println(buffer.toString());
					RouteCalculator.exit(0);
				}
				if (args[i].startsWith("-direct")) { //$NON-NLS-1$
					onlyDirect = true;
					continue;
				}
				// if (args[i].startsWith("-wind")) { //$NON-NLS-1$
				// Route.setWindShiftActive(true);
				// continue;
				// }
				// if (args[i].startsWith("-evaluate")) { //$NON-NLS-1$
				// int apparent = argumentIntegerValue(args, i);
				// i++;
				// if (null == startLocation) {
				// System.out.println("Start location not specified in the right order. -start before -evaluate.");
				// exit(1);
				// }
				// evaluateCell(startLocation, endLocation, apparent);
				// exit(0);
				// // double windSpeed = argumentDoubleValue(args, i);
				// // i++;
				// // SailConfiguration conf = Polars.lookup(apparent, windSpeed);
				// // StringBuffer buffer = new StringBuffer("[Polar search results").append('\n');
				// // buffer.append("Sail=").append(conf.getSail()).append('\n');
				// // buffer.append("Speed=").append(conf.getSpeed()).append('\n');
				// // System.out.println(buffer.toString());
				// // continue;
				// }
				// if (args[i].startsWith("-optim")) { //$NON-NLS-1$
				// int optimizerLevel = argumentIntegerValue(args, i);
				// i++;
				// Route.setOptimizerLevel(optimizerLevel);
				// continue;
				// }
				if (args[i].startsWith("-deep")) { //$NON-NLS-1$
					int deepLevel = argumentIntegerValue(args, i);
					i++;
					Route.setDeepLevel(deepLevel);
					continue;
				}
				if (args[i].startsWith("-router")) {
					// - Get the router type. Can be DIRECT or ROUTE.
					final String routerType = this.argumentStringValue(args, i);
					i++;
					if (routerType.equals("DIRECT")) {
						// final int heading = this.argumentIntegerValue(args, i);
						// i++;
						if (null == startLocation) {
							System.out.println("Start location not specified in the right order. -start before -router.");
							RouteCalculator.exit(1);
						}
						if (null == endLocation) {
							System.out.println("Start location not specified in the right order. -end before -router.");
							RouteCalculator.exit(1);
						}
						final Router theRouter = new Router();
						theRouter.optimizeRoute(startLocation, endLocation);
					}
					if (routerType.equals("ROUTE")) {
						final String routeDefinitionFile = this.argumentStringValue(args, i);
						i++;
						final Router theRouter = new Router();
						theRouter.optimizeRoute(routeDefinitionFile);
					}
					// Router router = new Router(startLocation, endLocation);
					// try {
					// router.generateRoute(heading);
					// } catch (LocationNotInMap e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					RouteCalculator.exit(0);
				}
			}
		}
		// ... Check that required parameters have values.
		if ((null == startLocation) || (null == endLocation)) RouteCalculator.exit(VORGConstants.NOCONFIG);
	}

	private double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = this.argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	private int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = this.argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	private String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else
			// - Exit point 10. There are no enough arguments in the list to find a value.
			RouteCalculator.exit(VORGConstants.NOCONFIG);
		return "";
	}

	private void help() {
		System.out.println("Command API for the RouteCalcualtor:");
		System.out.println("java -classpath routecalculator.jar net.sf.vorg.routecalculator.command.main.RouteCalculator ");
		System.out.println("Allowed parameters:");
		System.out.println("-sta[rt] <lat grade> <lat minute> <lon grade> <lon minute>");
		System.out.println("-end <lat grade> <lat minute> <lon grade> <lon minute>");
		System.out.println("-way[point] <lat grade> <lat minute> <lon grade> <lon minute>");
		System.out.println();
		System.out.println("Allowed commands");
		System.out.println();
		System.out.println("Allowed toggles");
		System.out.println("-span   -- number of levels to process in the VMG routing. Not used.");
		System.out.println("-wind   -- if present then consider wind shift cell for splitting and recalculation.");
		System.out.println("-debug  -- if defined opens the ouput for verbose debugging information.");
		System.out.println();
		System.out.println("Allowed commands");
		System.out
				.println("-VMC <direction> -- calculates the VMC for the cell that contains the -start location over the parameter direction.");
		System.out
				.println("-VMG <direction> <winddir> <windspeed>  -- calculates the VMC for the parameter values. Does not need a cell location.");
		System.out
				.println("-polar <AWD> <windspeed>  -- calculates the exact polars for this AWD on the selected wind speed.");
		System.out
				.println("-router  -- optimizes a route. The next parameters specify the route and the optimizer operation.");
		System.out.println("    <type>  -- if there is a direct route of a loaded one.");
		System.out.println("    <deep>  -- number of iterations to process between 10 and 60.");
		System.out.println("    <iterations> -- number of thousands of iterations to descend a deep level.");
		System.out.println();
	}

	// /** Runs the optimizer or the route calculator depending on the specified parameters. */
	// public void execute() {
	// if (null != waypoint)
	// generateVMCRoute(waypoint, cellsToSpan);
	// else if (onlyDirect) {
	// directRoute();
	// } else {
	// optimizeRoute();
	// }
	// }

	// /**
	// * Evaluates the VMC routes and gets the best one for the number of cells specified on the parameters.
	// *
	// * @param cellsToSpan
	// * @param waypoint
	// */
	// private void generateVMCRoute(GeoLocation waypoint, int cellsToSpan) {
	// VMCRouter router = new VMCRouter(waypoint, cellsToSpan);
	// try {
	// router.getBestRoute(startLocation);
	// } catch (LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// private void evaluateCell(final GeoLocation start, final GeoLocation end, final int apparent) {
	// final Router router = new Router(start, end);
	// try {
	// router.evaluateCell(start, apparent);
	// } catch (final LocationNotInMap e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// private void directRoute() {
	// try {
	// WindMapHandler actualMap = new WindMapHandler();
	// actualMap.directRoute(startLocation, endLocation);
	// } catch (LocationNotInMap lnime) {
	// lnime.printStackTrace();
	// } catch (CloneNotSupportedException cnse) {
	// cnse.printStackTrace();
	// }
	// }
	//
	// private void optimizeRoute() {
	// try {
	// WindMapHandler actualMap = new WindMapHandler();
	// actualMap.optimizeRoute(startLocation, endLocation);
	// } catch (LocationNotInMap lnime) {
	// lnime.printStackTrace();
	// } catch (CloneNotSupportedException cnse) {
	// cnse.printStackTrace();
	// }
	// }

	private void banner() {
		System.out.println(" ____             _        ____      _            _       _             ");
		System.out.println("|  _ \\ ___  _   _| |_ ___ / ___|__ _| | ___ _   _| | __ _| |_ ___  _ __ ");
		System.out.println("| |_) / _ \\| | | | __/ _ \\ |   / _` | |/ __| | | | |/ _` | __/ _ \\| '__|");
		System.out.println("|  _ < (_) | |_| | ||  __/ |__| (_| | | (__| |_| | | (_| | || (_) | |   ");
		System.out.println("|_| \\_\\___/ \\__,_|\\__\\___|\\____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   ");
		System.out.println();
		System.out.println(VORGConstants.VERSION);
		System.out.println();
	}

	/**
	 * Give the max VMC results for the projection on a given direction. We have to know the wind direction and
	 * the wind speep and those values have to be obtained from some map
	 */
	private void printVMC(final int heading) {
		final WindMapHandler actualMap = new WindMapHandler();
		actualMap.printVMC(startLocation, heading);
	}

	private void printVMC(final int heading, final int windDirection, final double windSpeed) {
		final VMCData vmc = new VMCData(heading, windDirection, windSpeed);
		RouteCalculator.output(vmc.printRecord());
	}

	public static boolean onlyDirect() {
		return onlyDirect;
	}
}
// - UNUSED CODE ............................................................................................
