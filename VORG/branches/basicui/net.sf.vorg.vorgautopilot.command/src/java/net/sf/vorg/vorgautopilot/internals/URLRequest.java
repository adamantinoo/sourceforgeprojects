//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen 
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) 
//  RELEASE:        $Revision: 174 
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.internals;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class URLRequest {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("net.sf.vorg.vorgautopilot.internals");

	// - F I E L D - S E C T I O N ............................................................................
	protected URL					url;
	private URLConnection	connection;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public URLRequest() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void initializeURL(URL newURL) throws IOException {
		url = newURL;
		connection = url.openConnection();
		connection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		connection.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
		connection.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
	}

	public void setPost(Vector<String> postParameters) throws IOException {
		connection.setDoOutput(true);
		Writer post = new OutputStreamWriter(connection.getOutputStream());
		Iterator<String> ppit = postParameters.iterator();
		int paramCounter = 0;
		while (ppit.hasNext()) {
			String param = ppit.next();
			if (paramCounter > 1) post.write('&');
			post.write(encodeParameter(param));
			paramCounter++;
		}
		post.write("\r\n");
		post.close();
	}

	public String connect(URL newURL, Vector<String> postParameters) {
		StringBuffer data = new StringBuffer();
		try {
			initializeURL(newURL);
			if (null != postParameters) setPost(postParameters);
			connection.connect();
			Map<String, List<String>> headers = connection.getHeaderFields();
			Iterator<String> it = headers.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				System.out.println(key + ": " + headers.get(key));
			}
			System.out.println();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line = null;
			while ((line = in.readLine()) != null)
				data.append(line).append("\n");
		} catch (MalformedURLException ex) {
			System.err.println(ex);
		} catch (FileNotFoundException ex) {
			System.err.println("Failed to open stream to URL: " + ex);
		} catch (IOException ex) {
			System.err.println("Error reading URL content: " + ex);
		}
		return data.toString();
	}

	// public static void main(String[] args) {
	// BufferedReader in = null;
	// if (args.length > 0) {
	// try {
	// URL url = new URL(args[0]);
	// URLConnection connection = url.openConnection();
	// connection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	// if (args.length > 1) {
	// connection.setDoOutput(true);
	// Writer post = new OutputStreamWriter(connection.getOutputStream());
	// for (int i = 1; i < args.length; i++) {
	// if (i > 1) post.write('&');
	// post.write(encodeParameter(args[i]));
	// }
	// post.write("\r\n");
	// post.close();
	// }
	// connection.connect();
	// Map headers = connection.getHeaderFields();
	// Iterator it = headers.keySet().iterator();
	// while (it.hasNext()) {
	// String key = (String) it.next();
	// System.out.println(key + ": " + headers.get(key));
	// }
	// System.out.println();
	// in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	// String line = null;
	// while ((line = in.readLine()) != null)
	// System.out.println(line);
	// } catch (MalformedURLException ex) {
	// System.err.println(ex);
	// } catch (FileNotFoundException ex) {
	// System.err.println("Failed to open stream to URL: " + ex);
	// } catch (IOException ex) {
	// System.err.println("Error reading URL content: " + ex);
	// }
	// if (in != null) try {
	// in.close();
	// } catch (IOException ex) {
	// }
	// } else {
	// System.err.println("Usage: URLRequest URL (uses GET)");
	// System.err.println("       URLRequest URL parameters... (uses POST)");
	// }
	// }

	private static String encodeParameter(String parameter) {
		StringBuffer result = new StringBuffer();
		try {
			String name = null;
			String value = "";
			int ix = parameter.indexOf('=');
			if (ix == -1)
				name = parameter;
			else {
				name = parameter.substring(0, ix);
				value = parameter.substring(ix + 1);
			}
			result.append(name);
			result.append('=');
			result.append(URLEncoder.encode(value, "UTF-8"));
		} catch (UnsupportedEncodingException ex) {
			System.err.println(ex);
		}
		return result.toString();
	}
}

// - UNUSED CODE ............................................................................................
