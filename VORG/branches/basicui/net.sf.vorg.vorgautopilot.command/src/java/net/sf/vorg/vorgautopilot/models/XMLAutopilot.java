//  PROJECT:        net.sf.vorg.vorgautopilot.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package net.sf.vorg.vorgautopilot.models;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringBufferInputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import main.VORGAutopilot;
import net.sf.vorg.core.exceptions.LocationNotInMap;
import net.sf.vorg.routecalculator.models.WindMapHandler;
import net.sf.vorg.vorgautopilot.internals.VORGURLRequest;
import net.sf.vorg.vorgautopilot.internals.WaypointTypes;
import net.sf.vorg.vorgautopilot.parsers.BoatDataParserHandler;
import net.sf.vorg.vorgautopilot.parsers.PilotConfigurationParserHandler;
import net.sf.vorg.vorgautopilot.parsers.PointsParserHandler;

// - CLASS IMPLEMENTATION ...................................................................................
public class XMLAutopilot extends Thread {
	private static final int		SLEEP_SECONDS					= 25;
	protected static final int	SLEEP_MSECONDS				= XMLAutopilot.SLEEP_SECONDS * 1000;

	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger				logger								= Logger.getLogger("net.sf.vorg.vorgautopilot.models");
	protected static boolean		terminationRequested	= false;
	private static boolean			connected							= false;

	public static void disconnect() {
		XMLAutopilot.connected = false;
	}

	public static void setTerminate(final boolean state) {
		XMLAutopilot.terminationRequested = state;
	}

	// - F I E L D - S E C T I O N ............................................................................
	protected String								configFileName;
	protected final Vector<String>	cookies		= new Vector<String>();
	protected final Boat						boat			= new Boat();
	private int											lastHour	= 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public XMLAutopilot() {
	}

	public XMLAutopilot(final String configurationFileName) {
		configFileName = configurationFileName;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/**
	 * Runs the autopilot in an endless loop. There currently no way to control the stop of the process but on
	 * next releases I may access this over a telnet interface.<br>
	 * There are two sequences to run the pilot. If the refresh parameter is on the command line then we iterate
	 * every number of minutes as specified there. If the parameter is the default of 10 minutes, then we delay
	 * the execution for 65 seconds after the minute change to get full boat refresh.
	 */
	@Override
	public void run() {
		try {
			super.run();
			// - Before starting, read the list of persistent points already surpassed.
			readSurpassed(boat);

			// - Start the endless loop inside the thread.
			if (VORGAutopilot.getRefresh() != 10) {
				long lastMinute = minuteOfDay() - 100;
				while (!XMLAutopilot.terminationRequested)
					try {
						checkMapReload();
						// - Check if we have entered a new 10 minute interval
						final long dayMinute = minuteOfDay();
						if ((dayMinute < lastMinute) || (dayMinute >= lastMinute + VORGAutopilot.getRefresh())) {
							lastMinute = dayMinute;
							performOperation();
						}
						// - Sleep the process for a minute before checking again the waiting interval.
						try {
							Thread.sleep(XMLAutopilot.SLEEP_MSECONDS);
						} catch (final InterruptedException ie) {
							System.out.println("Autopilot interrupted. Terminating current process.");
						}
					} catch (final Exception ex) {
						// - Any class of exception. Record it and continue.
						System.out.println("EEE EXCEPTION - " + ex.getMessage());
					}
			} else {
				int lastMinute = -1; // Reset the interval detection to run for the first time
				while (!XMLAutopilot.terminationRequested)
					try {
						checkMapReload();
						// - Check if we have entered a new 10 minute interval
						final Calendar now = Calendar.getInstance();
						final int minute = now.get(Calendar.MINUTE) / 10;
						if (minute != lastMinute) {
							// - Wait until minute 1 and 5 seconds.
							int minutes = now.get(Calendar.MINUTE);
							final int seconds = now.get(Calendar.SECOND);
							// - If the minute is multiple of 10, wait.
							minutes = minutes % 10;
							if (minutes == 0) try {
								if (VORGAutopilot.timeDelay > 0) Thread.sleep((VORGAutopilot.timeDelay - seconds) * 1000);
							} catch (final InterruptedException ie) {
								System.out.println("Autopilot interrupted. Terminating current process.");
							} catch (final IllegalArgumentException iae) {
								// - The value to wait is not valid. Skip
							}
							lastMinute = minute;
							performOperation();
						} else
							// - Sleep the process for a minute before checking again the ten minute period
							try {
								Thread.sleep(XMLAutopilot.SLEEP_MSECONDS);
							} catch (final InterruptedException ie) {
								System.out.println("Autopilot interrupted. Terminating current process.");
							}
					} catch (final Exception ex) {
						// - Any class of exception. Record it and continue.
						System.out.println("EEE EXCEPTION - " + ex.getMessage());
					}
			}
			System.out.println("Termination request acknowledged.");
		} catch (final Exception ex) {
			// - Any class of exception. Record it and continue.
			System.out.println("EEE EXCEPTION - " + ex.getMessage());
		}
	}

	protected void checkMapReload() {
		final Calendar now = Calendar.getInstance();
		final int hour = now.get(Calendar.HOUR_OF_DAY);
		if (hour != lastHour) {
			WindMapHandler.clear();
			lastHour = hour;
		}
	}

	/*
	 * Goes to the game service and gets the boat information for the user configured. It will then parse the
	 * returned XML data to build some of the boat data resources.
	 * 
	 * @param cookies
	 */
	protected void getBoatData(final Vector<String> cookies) {
		final StringBuffer request = new StringBuffer("/get_user.php?");
		request.append("pseudo=" + URLEncoder.encode(boat.getName()));
		request.append("&").append("id_user_searching=").append(boat.getUserid());

		final VORGURLRequest boatRequest = new VORGURLRequest(request.toString());
		// cit = cookies.iterator();
		final StringBuffer cookies2 = new StringBuffer();
		cookies2.append("userid=" + boat.getUserid());
		cookies2.append("useremail=" + boat.getEmail());
		boatRequest.executeGET(cookies.toString());
		// FIXME This lines have to be removed to save memory.
		final String data = boatRequest.getData();
		parseBoatData(data);

		// - Add current boat location point to the list of surpassed points.
		final Waypoint waypoint = new Waypoint(WaypointTypes.DIRECT.toString());
		waypoint.setLocation(boat.getLocation());
		boat.surpassWaypoint(waypoint);

		System.out.println();
		try {
			System.out.println(boat.printReport());
			System.out.println();
		} catch (final LocationNotInMap e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void login(final String email, final String password) {
		final String protocol = "http";
		final String gameHost = "www.volvooceanracegame.org";
		final String request = "/ajax/ajax_login.php";
		final Hashtable<String, String> parameters = new Hashtable<String, String>();
		parameters.put("email", "boneyvorg@orangemail.es");
		parameters.put("password", password);
		try {
			// - Process the POST parameters and encode them for output.
			final StringBuffer urlParameters = new StringBuffer();
			final Enumeration<String> pit = parameters.keys();
			int paramCounter = 0;
			while (pit.hasMoreElements()) {
				final String name = pit.nextElement();
				final String value = parameters.get(name);
				if (paramCounter > 0) urlParameters.append('&');
				urlParameters.append(name).append("=").append(URLEncoder.encode(value, "UTF-8"));
				paramCounter++;
			}
			final URL url = new URL(protocol, gameHost, request);
			final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
			conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
			// conn.setRequestProperty("Cookie", cookies);

			conn.setUseCaches(false);
			// conn.setDoInput(true);
			conn.setDoOutput(true);
			final Writer post = new OutputStreamWriter(conn.getOutputStream());
			post.write(urlParameters.toString());
			post.write("\r\n");
			post.flush();
			// post.close();

			// conn.setDoInput(true);
			final StringBuffer data = new StringBuffer();
			try {
				final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null;
				while ((line = in.readLine()) != null)
					data.append(line).append("\n");
			} catch (final MalformedURLException ex) {
				System.err.println(ex);
			} catch (final FileNotFoundException ex) {
				System.err.println("Failed to open stream to URL: " + ex);
			} catch (final IOException ex) {
				System.err.println("Error reading URL content: " + ex);
			}
			// System.out.println(data.toString());
			// System.out.println(conn.getContent());
			String headerName = null;
			for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
				if (headerName.equals("Set-Cookie")) {
					String cookie = conn.getHeaderField(i);
					cookie = cookie.substring(0, cookie.indexOf(";"));
					final String cookieName = cookie.substring(0, cookie.indexOf("="));
					final String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
					if (cookieName.equals("userhash")) boat.setClef(cookieValue);
					cookies.add(cookieName + "=" + cookieValue);
				}
		} catch (final MalformedURLException mue) {
			mue.printStackTrace();
		} catch (final UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}

	}

	private void login2(final String email, final String password) {
		try {
			final URL url = new URL(
					"http://volvogame.virtualregatta.com/auth_login.php?onLoad=[type%20Function]&identifiantPlayer=119878&pass=b4064b30d11b9f7646822aa5ff37bc7b119878");
			final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			conn.setRequestProperty("Accept", "text/javascript, text/html, application/xml, text/xml");
			conn.setRequestProperty("Referer", "http://www.volvooceanracegame.org/home.php");
			final StringBuffer cookies = new StringBuffer();
			cookies.append("userid=" + "119878").append(",");
			cookies.append("useremail=" + "boneyvorg%40orangemail.es").append(",");
			cookies.append("userhash=" + "352fda387c0eb38e0e47149a019d44ca");
			conn.setRequestProperty("Cookie", cookies.toString());

			conn.setUseCaches(false);

			conn.setDoInput(true);
			final StringBuffer data = new StringBuffer();
			try {
				final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line = null;
				while ((line = in.readLine()) != null)
					data.append(line).append("\n");
			} catch (final MalformedURLException ex) {
				System.err.println(ex);
			} catch (final FileNotFoundException ex) {
				System.err.println("Failed to open stream to URL: " + ex);
			} catch (final IOException ex) {
				System.err.println("Error reading URL content: " + ex);
			}
			int parada = 0;
			parada++;
			// System.out.println(data.toString());
			// System.out.println(conn.getContent());
			// int d = 0;

			// String headerName = null;
			// for (int i = 1; (headerName = conn.getHeaderFieldKey(i)) != null; i++)
			// if (headerName.equals("Set-Cookie")) {
			// String cookie = conn.getHeaderField(i);
			// cookie = cookie.substring(0, cookie.indexOf(";"));
			// String cookieName = cookie.substring(0, cookie.indexOf("="));
			// String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());
			// if (cookieName.equals("userhash")) boat.setClef(cookieValue);
			// cookies.add(cookieName + "=" + cookieValue);
			// }
		} catch (final MalformedURLException mue) {
			mue.printStackTrace();
		} catch (final UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}

	}

	protected long minuteOfDay() {
		final Calendar now = Calendar.getInstance();
		final int hour = now.get(Calendar.HOUR_OF_DAY);
		final int minute = now.get(Calendar.MINUTE);
		return hour * 60 + minute;
	}

	@SuppressWarnings("deprecation")
	private void parseBoatData(final String data) {
		// - Parse the configuration file.
		InputStream stream;
		try {
			stream = new BufferedInputStream(new StringBufferInputStream(data));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final BoatDataParserHandler handler = new BoatDataParserHandler(boat);
			parser.parse(stream, handler);
		} catch (final FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void performOperation() {
		// - Process current configuration. Read the configuration file to load any updates.
		System.out.println("------------------------------------------------------------------------------------------");
		System.out.println(Calendar.getInstance().getTime() + " - new scan run for configuration: " + configFileName);
		processConfiguration();
		// - Perform the login if we are not connected.
		// if (!connected) login(boat.getEmail(), boat.getPassword());
		// - Get current boat data
		getBoatData(cookies);
		// - Execute any of the boat commands
		boat.performPilot();
	}

	/*
	 * Reads and parses the configuration XML file to get the identification information and the autopilot
	 * commands.
	 */
	private void processConfiguration() {
		// - Parse the configuration file.
		InputStream stream;
		try {
			stream = new BufferedInputStream(new FileInputStream(configFileName));
			final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			final PilotConfigurationParserHandler handler = new PilotConfigurationParserHandler(boat);
			// - Clear current boat commands before reading a new set.
			boat.clearCommands();
			parser.parse(stream, handler);
		} catch (final FileNotFoundException fnfe) {
			// TODO Auto-generated catch block
			fnfe.printStackTrace();
		} catch (final ParserConfigurationException pce) {
			// TODO Auto-generated catch block
			pce.printStackTrace();
		} catch (final SAXException saxe) {
			// TODO Auto-generated catch block
			saxe.printStackTrace();
		} catch (final IOException ioe) {
			// TODO Auto-generated catch block
			ioe.printStackTrace();
		}
	}

	private void readSurpassed(final Boat boat) {
		// - Load boat information. The boat now is empty.
		processConfiguration();
		// - Open the file with the boat name.
		if (null != boat.getName())
			try {
				final BufferedInputStream pointListFile = new BufferedInputStream(new FileInputStream(boat.getName()
						+ ".points"));
				final SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
				parser.parse(pointListFile, new PointsParserHandler(boat));
			} catch (final FileNotFoundException fnfe) {
				// TODO Auto-generated catch block
				fnfe.printStackTrace();
			} catch (final ParserConfigurationException pce) {
				// TODO Auto-generated catch block
				pce.printStackTrace();
			} catch (final SAXException saxe) {
				// TODO Auto-generated catch block
				saxe.printStackTrace();
			} catch (final IOException ioe) {
				// TODO Auto-generated catch block
				ioe.printStackTrace();
			}
	}
}

// - UNUSED CODE ............................................................................................
