//  PROJECT:        net.sf.vorg.routescanner.command
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package models;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import junit.framework.TestCase;
import net.sf.vorg.core.VORGConstants;
import net.sf.vorg.routecalculator.internals.GeoLocation;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestLoxodromic extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("models");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public TestLoxodromic() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testLoxodromicDistance() throws Exception {
		GeoLocation start = new GeoLocation(-25.0, 174.0);
		GeoLocation end = new GeoLocation(-26.0, 175.0);
		@SuppressWarnings("unused")
		double distance = 100.0;

		double b = (end.getLat() - start.getLat()) * 60.0;
		double exagStart = VORGConstants.EARTHRADIUS * Math.tan(Math.toRadians(45.0 + start.getLat() / 2.0));
		double exagEnd = VORGConstants.EARTHRADIUS * Math.tan(Math.toRadians(45.0 + end.getLat() / 2.0));
		double L = (end.getLon() - start.getLon()) * 60.0;
		double B = exagEnd - exagStart;

		double k = Math.toDegrees(Math.atan(L / B)) + 180.0;
		double cosk = Math.cos(Math.toRadians(k));
		double d = b / Math.cos(Math.toRadians(k));

		// - Current calculations
		// TRIG
		double deltaLat = end.getLat() - start.getLat();
		double deltaLon = end.getLon() - start.getLon();
		distance = Math.hypot(deltaLat, deltaLon) * 60.0;

		// LOX
		double Ls = Math.toRadians(start.getLat());
		double Ld = Math.toRadians(end.getLat());
		double ldelta = Math.toRadians(deltaLon);
		double loxDistance = VORGConstants.EARTHRADIUS
				* Math.acos(Math.sin(Ls) * Math.sin(Ld) + Math.cos(Ls) * Math.cos(Ld) * Math.cos(ldelta));
		int alpha = GeoLocation.adjustAngleTo360(new Long(Math.round(start.angleTo(end))).intValue());

		// FUSION
		double ls = start.getLat() * 60.0;
		double le = end.getLat() * 60.0;
		double exagls = VORGConstants.EARTHRADIUS * Math.tan(Math.toRadians(45.0 + start.getLat() / 2.0));
		double exagle = VORGConstants.EARTHRADIUS * Math.tan(Math.toRadians(45.0 + end.getLat() / 2.0));
		double dln = le - ls;
		double dle = exagle - exagls;

		double beta = Math.toDegrees(Math.atan(B / L));

		double mid = (end.getLat() - start.getLat()) / 2.0;
		double mult = Math.cos(Math.toRadians(start.getLat() + mid));
		double dlest = dln * mult;

		double gamma = 90.0 + Math.abs(Math.toDegrees(Math.atan(dlest / L)));
		@SuppressWarnings("unused")
		int dummy = 0;

		// RECONSTRUCTIONS
		// Fixing L to 100 miles
		L = 60.0;
		double endL = L / 60.0 + start.getLon();
		double dlest2 = Math.tan(Math.toRadians(gamma - 90.0)) * L;
		double mid2 = Math.acos(mult) - start.getLat();

		// dlest=(x - start.getLat() * 60.0)*Math.cos(start.getLat() +(x - start.getLat()) / 2.0)

	}
}

// - UNUSED CODE ............................................................................................
