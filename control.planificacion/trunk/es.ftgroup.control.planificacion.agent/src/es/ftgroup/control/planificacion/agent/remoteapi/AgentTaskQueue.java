//  PROJECT:        es.ftgroup.control.planificacion.agent
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.agent.remoteapi;

// - IMPORT SECTION .........................................................................................
import java.rmi.RemoteException;
import java.rmi.server.RemoteObject;
import java.util.Vector;
import java.util.logging.Logger;

import es.ftgroup.control.planificacion.model.ITask;

// - CLASS IMPLEMENTATION ...................................................................................
public class AgentTaskQueue extends RemoteObject implements RITaskQueue {
	private static Logger				logger				= Logger.getLogger("es.ftgroup.control.planificacion.agent.remoteapi");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";
	private Vector							taskQueue			= new Vector();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public AgentTaskQueue() {
		super();
	}

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// - P R O T E C T E D - S E C T I O N

	@Override
	public Aknowledge scheduleTask(ITask t) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Aknowledge storeTask(ITask receivedTask) throws RemoteException {
		taskQueue.add(receivedTask);
		return Aknowledge.OK();
	}
}

// - UNUSED CODE ............................................................................................
