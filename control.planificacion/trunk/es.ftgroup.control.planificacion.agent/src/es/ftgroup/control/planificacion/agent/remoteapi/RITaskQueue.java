//  PROJECT:        es.ftgroup.control.planificacion.agent
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.agent.remoteapi;

// - IMPORT SECTION .........................................................................................
import java.rmi.Remote;
import java.rmi.RemoteException;

import es.ftgroup.control.planificacion.model.ITask;

// - CLASS IMPLEMENTATION ...................................................................................
public interface RITaskQueue extends Remote {
	// - F I E L D - S E C T I O N ............................................................................
	// - M E T H O D - S E C T I O N ..........................................................................
	public Aknowledge scheduleTask(ITask t) throws RemoteException;

	public Aknowledge storeTask(ITask t) throws RemoteException;
}

class Aknowledge {
	private static Aknowledge	AckowledgeOk	= new Aknowledge("OK");
	private String						message;

	public Aknowledge(String message) {
		this.message = message;
	}

	public static Aknowledge OK() {
		// TODO Auto-generated method stub
		return AckowledgeOk;
	}

}
// - UNUSED CODE ............................................................................................
