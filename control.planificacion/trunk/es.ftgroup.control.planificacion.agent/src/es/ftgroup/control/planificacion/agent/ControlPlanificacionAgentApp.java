//  PROJECT:        es.ftgroup.control.planificacion.agent
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.agent;

// - IMPORT SECTION .........................................................................................
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

import com.sun.jndi.cosnaming.IiopUrl.Address;

import es.ftgroup.control.planificacion.agent.remoteapi.AgentTaskQueue;
import es.ftgroup.control.planificacion.agent.remoteapi.RITaskQueue;
import wdoo.app.CommandLineApp;

// - CLASS IMPLEMENTATION ...................................................................................
public class ControlPlanificacionAgentApp extends CommandLineApp {
	private static Logger					logger							= Logger.getLogger("es.ftgroup.control.planificacion.agent");

	// - F I E L D - S E C T I O N ............................................................................
	// - A P P L I C A T I O N - C O N S T A N T S
	private static final String		APPLICATIONNAME			= "ControlPlanificacionAgentApp";
	private static final String		STATE_RUNNING				= "CommandLineApp.STATE_RUNNING";
	private static final String		STATE_UNINITIALIZED	= "CommandLineApp.STATE_UNINITIALIZED";
	private static final String		STATE_STOPREQUESTED	= "CommandLineApp.STATE_STOPREQUESTED";
	private static final String		STATE_STARTING_UP		= "CommandLineApp.STATE_STARTING_UP";

	// - G L O B A L - C O N S T A N T S
	private static final String		CONSTANT_NAME				= "CONSTANT_VALUE";

	// - G L O B A L
	private static CommandLineApp	singleton						= null;

	private String								state								= STATE_UNINITIALIZED;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - M A I N A P P L I C A T I O N E N T R Y P O I N T
	/**
	 * This is the application entry point. As most applications it will create a singleton and then initiate
	 * the configuration and execution of the main event loop. The code is almost identical to all command line
	 * applications and thus the code can be inherited.
	 * 
	 * @param args
	 *          list of arguments received from the host command line
	 */
	public static void main(String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherited code to
		// be executed instead making a lot of calls. So we start by instantiating a new application object
		// and then calling the common initialization methods to perform all inherited initialization.
		// ControlPlanificacionAgentApp.setApplicationCodeName(APPLICATIONNAME);
		// ControlPlanificacionAgentApp.setErrorCodePrefix(APPLICATIONNAME + ".ERR"); //$NON-NLS-1$
		singleton = new ControlPlanificacionAgentApp();
		singleton.configure(args);
		singleton.execute();
		// singleton.writeHeader(singleton.out);
		// singleton.doCommand();
		// singleton.writeFooter(singleton.out);
	}

	// - C O N S T R U C T O R S
	// public ControlPlanificacionAgentApp() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	@Override
	public void configure(String[] args) {
		super.configure(args);
	}

	@Override
	public void execute() {
		super.execute();
		// - Start the main loop process to read commands and execute them.
		state = STATE_STARTING_UP;
		String name = "AgentTaskQueue";
		RITaskQueue engine = new AgentTaskQueue();
		RITaskQueue stub;
		try {
			stub = (RITaskQueue) UnicastRemoteObject.exportObject(engine, 0);
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind(name, stub);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("ComputeEngine bound");

		state = STATE_RUNNING;
		while (STATE_RUNNING.equals(state)) {

		}
		// - Terminate the process. Disconnect and close all resources in use.
	}

	public void stop() {
		state = STATE_STOPREQUESTED;
	}

	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// - P R O T E C T E D - S E C T I O N
	/**
	 * Register this Agent in the Naming Directory that is configured in the property configuration. The JNDI
	 * interface is used to control binding and lookup of named structures in this architecture and then can
	 * simplify agent configuration because Server lookup is made with common technology APIs.
	 */
	private void registerAgent() {

	}

	/**
	 * Initialize the data structures to communicate with other elements of the architecture. There are an
	 * undefined number of channels for data input and at least an output data link to the central server. On
	 * fail-safe configurations there could be multiple output channels that should be multiplexed.
	 * 
	 * Connection is open and closed every time a new message has to be sent and in case of connection failure
	 * the action is tried some time later. Messages are queued for transmission depending on the sender
	 * direction, there is a message queue for each destination.
	 */
	private void openMessageQueues() {

	}
}

class Message {
	private Address	destination;
	private Address	source;
}

/**
 * This class is able to try to send pending messages if there are communication errors. The retry operation
 * is configurable by the application properties. The operation retries for a number of times before looking
 * again for a destination address with the same name. This class will send the same message to all
 * multiplexed destinations but will keep track of the communication queues for each of them so after a
 * problem correction the multiplexed entities will synchronize again.
 */
class RetrieableMessageQueue extends MultiplexedMessageQueue {

}

class MultiplexedMessageQueue extends MessageQueue {

}

class MessageQueue implements IMessageQueue {
	private String	state	= MESSAGEQUEUE_UNINITIALIZED;

	@Override
	public String getState() {
		return null;
	}

	@Override
	public int send(Message message) {
		// TODO Auto-generated method stub
		return 0;
	}

}

/**
 * Declares the structure that have to implement any class that wants to be able to send information to
 * another <code>MessageQueue</code>.
 */
interface IMessageQueue {
	public static final String	MESSAGEQUEUE_UNINITIALIZED	= "IMessageQueue.MESSAGEQUEUE_UNINITIALIZED";

	public int send(Message message);

	public String getState();
}
// - UNUSED CODE ............................................................................................
