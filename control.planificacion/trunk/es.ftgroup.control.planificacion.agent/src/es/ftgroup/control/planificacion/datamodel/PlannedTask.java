//  PROJECT:        es.ftgroup.control.planificacion.agent
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.datamodel;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Vector;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * A <code>PlannedTask</code> is a full featured Task that is composed of a set of Processes that are build
 * by Actions. The key information for a Task is the Group, the Name and the planned schedule.
 */
@Entity
public class PlannedTask implements Serializable {
	private static Logger				logger				= Logger.getLogger("es.ftgroup.control.planificacion.datamodel");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	private Sequence						ID						= DataID.getNewID();
	private TaskGroup						group;
	private TaskName						name;
	private Vector<Process>			processes			= new Vector<Process>();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public PlannedTask() {
	}

	// - G E T T E R S / S E T T E R S
	public Long getId() {
		return ID.getID();
	}

	public TaskGroup getGroup() {
		return group;
	}

	public void setGroup(TaskGroup group) {
		this.group = group;
	}

	public TaskName getName() {
		return name;
	}

	public void setName(TaskName name) {
		this.name = name;
	}
	// - P U B L I C - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// - P R O T E C T E D - S E C T I O N

}

class TaskGroup {
	private String	groupName	= "GROUP";

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}

class TaskName {
	private String	shortName	= "TDR";
	private String	longName	= "TARIFICACION_RESELLERS";

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}
}

class Process {
	private Sequence				ID			= DataID.getNewID();
	private Vector<Action>	actions	= new Vector<Action>();

	public Vector<Action> getActions() {
		return actions;
	}

	public void addAction(Action newAction) {
		actions.add(newAction);
	}

	// public boolean actionsEmpty() {
	// boolean value=false;
	// if (0 == actions.size() ? value=true : value=false);
	// return value;
	// }
}

class Action {
	private Sequence							ID	= DataID.getNewID();
	private static OutputProxy		logOutput;
	private static OutputProxy		output;
	private static DirectoryProxy	interchangePoolDirectory;

}

class OutputProxy {

}

class DirectoryProxy {

}

class Sequence {
	private long	sequence	= 0;

	// public Sequence() {
	// this.sequence= ID.getNewID();
	// }
	public Sequence(long sequenceValue) {
		sequence = sequenceValue;
	}

	public Long getID() {
		return new Long(sequence);
	}
}

class DataID {
	private static long	IDCurrentValue	= 1;

	public static Sequence getNewID() {
		return new Sequence(IDCurrentValue++);
	}
}
// - UNUSED CODE ............................................................................................
