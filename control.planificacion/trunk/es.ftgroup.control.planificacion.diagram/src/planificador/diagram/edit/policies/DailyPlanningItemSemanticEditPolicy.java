/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import planificador.PlanificadorPackage;
import planificador.diagram.edit.commands.EnterpriseCreateCommand;
import planificador.diagram.edit.commands.GenericTaskCreateCommand;
import planificador.diagram.edit.commands.ResellerCreateCommand;
import planificador.diagram.edit.commands.ResidentialCreateCommand;
import planificador.diagram.providers.PlanificadorElementTypes;

/**
 * @generated
 */
public class DailyPlanningItemSemanticEditPolicy extends PlanificadorBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (PlanificadorElementTypes.Residential_1001 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(PlanificadorPackage.eINSTANCE.getDailyPlanning_Tasks());
			}
			return getGEFWrapper(new ResidentialCreateCommand(req));
		}
		if (PlanificadorElementTypes.Enterprise_1002 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(PlanificadorPackage.eINSTANCE.getDailyPlanning_Tasks());
			}
			return getGEFWrapper(new ResellerCreateCommand(req));
		}
		if (PlanificadorElementTypes.Reseller_1003 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(PlanificadorPackage.eINSTANCE.getDailyPlanning_Tasks());
			}
			return getGEFWrapper(new GenericTaskCreateCommand(req));
		}
		if (PlanificadorElementTypes.GenericTask_1004 == req.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(PlanificadorPackage.eINSTANCE.getDailyPlanning_Tasks());
			}
			return getGEFWrapper(new EnterpriseCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain, DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(), req.getAllDuplicatedElementsMap());
		}

	}

}
