/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.edit.parts;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.notation.View;

import planificador.diagram.edit.policies.DailyPlanningCanonicalEditPolicy;
import planificador.diagram.edit.policies.DailyPlanningItemSemanticEditPolicy;

/**
 * @generated
 */
public class DailyPlanningEditPart extends ExtendedDailyPlanningEditPart {

	/**
	 * @generated
	 */
	public final static String	MODEL_ID	= "Planificador"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final int			VISUAL_ID	= 79;

	/**
	 * @generated
	 */
	public DailyPlanningEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	@Override
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new DailyPlanningItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE, new DailyPlanningCanonicalEditPolicy());
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.POPUPBAR_ROLE);
	}
}
