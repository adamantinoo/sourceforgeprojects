//  PROJECT:        es.ftgroup.control.planificacion.diagram
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package planificador.diagram.edit.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

// - CLASS IMPLEMENTATION ...................................................................................
public class ExtendedDailyPlanningEditPart extends DiagramEditPart {
//	private static Logger				logger				= Logger.getLogger("planificador.diagram.edit.parts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
//	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public ExtendedDailyPlanningEditPart(View diagramView) {
		super(diagramView);
	}
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// - P R O T E C T E D - S E C T I O N
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
	installEditPolicy(EditPolicy.LAYOUT_ROLE, new XYLayoutEditPolicy());}

	@Override
	protected IFigure createFigure() {
		// TODO Auto-generated method stub
		return super.createFigure();
	}
}

// - UNUSED CODE ............................................................................................
