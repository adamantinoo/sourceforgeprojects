//  PROJECT:        es.ftgroup.control.planificacion
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $
 
package planificador.diagram.edit.parts;

  /**
 * @generated
 */
public class ResidentialEditPart extends org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart {

      /**
 * @generated
 */
  public static final int VISUAL_ID = 1001;

    /**
 * @generated
 */
  protected org.eclipse.draw2d.IFigure contentPane;

    /**
 * @generated
 */
  protected org.eclipse.draw2d.IFigure primaryShape;

    /**
 * @generated
 */
  public ResidentialEditPart(org.eclipse.gmf.runtime.notation.View view) {
    super(view);
  }

    /**
 * @generated
 */
  protected void createDefaultEditPolicies() {
        
    super.createDefaultEditPolicies();
          installEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.SEMANTIC_ROLE, new planificador.diagram.edit.policies.ResidentialItemSemanticEditPolicy());
                    installEditPolicy(org.eclipse.gef.EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
          // XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
    // removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
  }

    /**
 * @generated
 */
  protected org.eclipse.gef.editpolicies.LayoutEditPolicy createLayoutEditPolicy() {
        
    org.eclipse.gef.editpolicies.FlowLayoutEditPolicy lep =
        new org.eclipse.gef.editpolicies.FlowLayoutEditPolicy() {

      
      protected org.eclipse.gef.commands.Command createAddCommand(
          org.eclipse.gef.EditPart child, org.eclipse.gef.EditPart after) {
        return null;
      }

      protected org.eclipse.gef.commands.Command createMoveChildCommand(
          org.eclipse.gef.EditPart child, org.eclipse.gef.EditPart after) {
        return null;
      }

      protected org.eclipse.gef.commands.Command getCreateCommand(
          org.eclipse.gef.requests.CreateRequest request) {
        return null;
      }
    };
    return lep;
      }

  
    /**
 * @generated
 */
  protected org.eclipse.draw2d.IFigure createNodeShape() {
    ResidentialFigure figure = new ResidentialFigure();
        return primaryShape = figure;
  }

    /**
 * @generated
 */
  public ResidentialFigure getPrimaryShape() {
    return (ResidentialFigure) primaryShape;
  }

      /**
 * @generated
 */
  protected boolean addFixedChild(org.eclipse.gef.EditPart childEditPart) {
              if (childEditPart instanceof planificador.diagram.edit.parts.ResidentialShortNameEditPart) {
      ((planificador.diagram.edit.parts.ResidentialShortNameEditPart) childEditPart).setLabel(
        getPrimaryShape().getFigureResidentialShortNameFigure());
      return true;
    }
                    if (childEditPart instanceof planificador.diagram.edit.parts.ResidentialStartTimeEditPart) {
      ((planificador.diagram.edit.parts.ResidentialStartTimeEditPart) childEditPart).setLabel(
        getPrimaryShape().getFigureResidentialStartTimeFigure());
      return true;
    }
                      return false;
  }

    /**
 * @generated
 */
  protected boolean removeFixedChild(org.eclipse.gef.EditPart childEditPart) {
    
            return false;
  }

    /**
 * @generated
 */
  protected void addChildVisual(org.eclipse.gef.EditPart childEditPart, int index) {
    if (addFixedChild(childEditPart)) {
      return;
    }
    super.addChildVisual(childEditPart, -1);
  }

    /**
 * @generated
 */
  protected void removeChildVisual(org.eclipse.gef.EditPart childEditPart) {
    if (removeFixedChild(childEditPart)){
      return;
    }
    super.removeChildVisual(childEditPart);
  }

    /**
 * @generated
 */
  protected org.eclipse.draw2d.IFigure getContentPaneFor(org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart editPart) {
    
            return super.getContentPaneFor(editPart);
  }

  
  
    /**
 * @generated
 */
  protected org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure createNodePlate() {
    org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure result =
      new org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure(
        getMapMode().DPtoLP(40),
        getMapMode().DPtoLP(40));
        return result;
  }

//        /**
// * @generated
// */
//  public org.eclipse.gef.EditPolicy getPrimaryDragEditPolicy() {
//        return new ();
//      }
    
  /**
 * Creates figure for this edit part.
 * 
 * Body of this method does not depend on settings in generation model
 * so you may safely remove <i>generated</i> tag and modify it.
 * 
 * @generated
 */
  protected org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure createNodeFigure() {
    org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure figure = createNodePlate();
    figure.setLayoutManager(new org.eclipse.draw2d.StackLayout());
    org.eclipse.draw2d.IFigure shape = createNodeShape();
    figure.add(shape);
    contentPane = setupContentPane(shape);
    return figure;
  }

  /**
 * Default implementation treats passed figure as content pane.
 * Respects layout one may have set for generated figure.
 * @param nodeShape instance of generated figure class
 * @generated
 */
  protected org.eclipse.draw2d.IFigure setupContentPane(org.eclipse.draw2d.IFigure nodeShape) {
    if (nodeShape.getLayoutManager() == null) {
          org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout layout =
        new org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout();
      layout.setSpacing(getMapMode().DPtoLP(5));
      nodeShape.setLayoutManager(layout);
        }
    return nodeShape; // use nodeShape itself as contentPane
  }

    /**
 * @generated
 */
  public org.eclipse.draw2d.IFigure getContentPane() {
    if (contentPane != null) {
      return contentPane;
    }
    return super.getContentPane();
  }

      /**
 * @generated
 */
  public org.eclipse.gef.EditPart getPrimaryChildEditPart() {
    return getChildBySemanticHint(planificador.diagram.part.PlanificadorVisualIDRegistry.getType(
      planificador.diagram.edit.parts.ResidentialShortNameEditPart.VISUAL_ID));
  }
  
  
  
  
    
  
  
/**
 * @generated
 */
public class ResidentialFigure extends org.eclipse.draw2d.RectangleFigure {


  /**
   * @generated
   */
  private org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel fFigureResidentialShortNameFigure; 
  /**
   * @generated
   */
  private org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel fFigureResidentialLongNameFigure; 
  /**
   * @generated
   */
  private org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel fFigureResidentialStartTimeFigure; 


  /**
   * @generated
   */
  public ResidentialFigure() {
    

  org.eclipse.draw2d.FlowLayout layoutThis = new org.eclipse.draw2d.FlowLayout();
  layoutThis.setStretchMinorAxis(false);
  layoutThis.setMinorAlignment(org.eclipse.draw2d.FlowLayout.ALIGN_LEFTTOP
);

  layoutThis.setMajorAlignment(org.eclipse.draw2d.FlowLayout.ALIGN_LEFTTOP
);
  layoutThis.setMajorSpacing(5);
  layoutThis.setMinorSpacing(5);
  layoutThis.setHorizontal(true);

  this.setLayoutManager(layoutThis);

        createContents();
  }
  /**
   * @generated
   */
  private void createContents(){


fFigureResidentialShortNameFigure = new org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel();
fFigureResidentialShortNameFigure.setText("<...>");

this.add(fFigureResidentialShortNameFigure);



fFigureResidentialLongNameFigure = new org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel();
fFigureResidentialLongNameFigure.setText("<...>");

this.add(fFigureResidentialLongNameFigure);



fFigureResidentialStartTimeFigure = new org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel();
fFigureResidentialStartTimeFigure.setText("<...>");

this.add(fFigureResidentialStartTimeFigure);


  }




  /**
   * @generated
   */
  private boolean myUseLocalCoordinates = false;

  /**
   * @generated
   */
  protected boolean useLocalCoordinates() {
    return myUseLocalCoordinates;
  }

  /**
   * @generated
   */
  protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
    myUseLocalCoordinates = useLocalCoordinates;
  }



  /**
   * @generated
   */
  public org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel getFigureResidentialShortNameFigure() {
    return fFigureResidentialShortNameFigure;
  }
  /**
   * @generated
   */
  public org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel getFigureResidentialLongNameFigure() {
    return fFigureResidentialLongNameFigure;
  }
  /**
   * @generated
   */
  public org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel getFigureResidentialStartTimeFigure() {
    return fFigureResidentialStartTimeFigure;
  }


}


}
