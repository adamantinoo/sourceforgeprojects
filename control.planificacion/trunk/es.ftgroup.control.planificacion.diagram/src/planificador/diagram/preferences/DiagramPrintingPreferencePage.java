/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import planificador.diagram.part.PlanificadorDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(PlanificadorDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
