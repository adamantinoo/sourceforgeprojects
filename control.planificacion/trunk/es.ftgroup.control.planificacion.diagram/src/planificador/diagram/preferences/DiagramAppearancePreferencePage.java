/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;

import planificador.diagram.part.PlanificadorDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

	/**
	 * @generated
	 */
	public DiagramAppearancePreferencePage() {
		setPreferenceStore(PlanificadorDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
