/*
 * //  PROJECT:        es.ftgroup.control.planificacion
 * //  FILE NAME:      $RCSfile: ProcessorApp.java,v $
 * //  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
 * //  LAST UPDATE:    $Date$
 * //  RELEASE:        $Revision$
 * //  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
 * //  LAST USER:      $Author$
 * //  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
 * //  LOG:
 * //    $Log: ProcessorApp.java,v $
 * 
 */
package planificador.diagram.view.factories;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gmf.runtime.diagram.ui.view.factories.BasicNodeViewFactory;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class ResidentialStartTimeViewFactory extends BasicNodeViewFactory {

	/**
	 * @generated
	 */
	protected List createStyles(View view) {
		List styles = new ArrayList();
		return styles;
	}
}
