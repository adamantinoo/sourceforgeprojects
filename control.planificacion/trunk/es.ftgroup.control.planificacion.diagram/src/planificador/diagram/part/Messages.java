/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String	PlanificadorCreateShortcutAction_OpenModelTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreateShortcutAction_CreateShortcutTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String	PlanificadorCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String	PlanificadorDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String	PlanificadorInitDiagramFileAction_InitDiagramFileResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorInitDiagramFileAction_InitDiagramFileResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorInitDiagramFileAction_InitDiagramFileWizardTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorInitDiagramFileAction_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String	PlanificadorNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String	PlanificadorElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageName;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageEmptyError;

	/**
	 * @generated
	 */
	public static String	ShortcutCreationWizard_ReferencedElementSelectionPageInvalidError;

	/**
	 * @generated
	 */
	public static String	ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String	ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String	TareasPlanificacion1Group_title;

	/**
	 * @generated
	 */
	public static String	TareasPlanificacion1Group_desc;

	/**
	 * @generated
	 */
	public static String	GenericTask1CreationTool_title;

	/**
	 * @generated
	 */
	public static String	GenericTask1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	Residential2CreationTool_title;

	/**
	 * @generated
	 */
	public static String	Residential2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	Enterprise3CreationTool_title;

	/**
	 * @generated
	 */
	public static String	Enterprise3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	Reseller4CreationTool_title;

	/**
	 * @generated
	 */
	public static String	Reseller4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String	DiagramEditorActionBarAdvisor_DefaultFileEditorTitle;

	/**
	 * @generated
	 */
	public static String	DiagramEditorActionBarAdvisor_DefaultFileEditorMessage;

	/**
	 * @generated
	 */
	public static String	DiagramEditorActionBarAdvisor_DefaultEditorOpenErrorTitle;

	/**
	 * @generated
	 */
	public static String	DiagramEditorActionBarAdvisor_AboutDialogTitle;

	/**
	 * @generated
	 */
	public static String	DiagramEditorActionBarAdvisor_AboutDialogMessage;

	/**
	 * @generated
	 */
	public static String	ApplicationMenuName_File;

	/**
	 * @generated
	 */
	public static String	ApplicationMenuName_Edit;

	/**
	 * @generated
	 */
	public static String	ApplicationMenuName_Window;

	/**
	 * @generated
	 */
	public static String	ApplicationMenuName_Help;

	/**
	 * @generated
	 */
	public static String	ApplicationMenuName_New;

	/**
	 * @generated
	 */
	public static String	DiagramEditorWorkbenchWindowAdvisor_Title;

	/**
	 * @generated
	 */
	public static String	WizardNewFileCreationPage_FileLabel;

	/**
	 * @generated
	 */
	public static String	WizardNewFileCreationPage_BrowseButton;

	/**
	 * @generated
	 */
	public static String	WizardNewFileCreationPage_SelectNewFileDialog;

	/**
	 * @generated
	 */
	public static String	WizardNewFileCreationPage_EmptyFileNameError;

	/**
	 * @generated
	 */
	public static String	WizardNewFileCreationPage_InvalidFileNameError;

	/**
	 * @generated
	 */
	public static String	CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String	AbstractParser_UnexpectedValueTypeMessage;

	/**
	 * @generated
	 */
	public static String	AbstractParser_WrongStringConversionMessage;

	/**
	 * @generated
	 */
	public static String	AbstractParser_UnknownLiteralMessage;

	/**
	 * @generated
	 */
	public static String	MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String	PlanificadorModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String	PlanificadorModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
