/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import planificador.DailyPlanning;
import planificador.PlanificadorPackage;
import planificador.diagram.edit.parts.DailyPlanningEditPart;
import planificador.diagram.edit.parts.EnterpriseEditPart;
import planificador.diagram.edit.parts.EnterpriseShortNameEditPart;
import planificador.diagram.edit.parts.GenericTaskEditPart;
import planificador.diagram.edit.parts.GenericTaskShortNameEditPart;
import planificador.diagram.edit.parts.ResellerEditPart;
import planificador.diagram.edit.parts.ResellerShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialEditPart;
import planificador.diagram.edit.parts.ResidentialShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialStartTimeEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class PlanificadorVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String	DEBUG_KEY	= PlanificadorDiagramEditorPlugin.getInstance().getBundle()
																						.getSymbolicName()
																						+ "/debug/visualID";	//$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (DailyPlanningEditPart.MODEL_ID.equals(view.getType())) {
				return DailyPlanningEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return planificador.diagram.part.PlanificadorVisualIDRegistry.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) { return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(Platform.getDebugOption(DEBUG_KEY))) {
				PlanificadorDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: " + type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) { return -1; }
		if (PlanificadorPackage.eINSTANCE.getDailyPlanning().isSuperTypeOf(domainElement.eClass())
				&& isDiagram((DailyPlanning) domainElement)) { return DailyPlanningEditPart.VISUAL_ID; }
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) { return -1; }
		String containerModelID = planificador.diagram.part.PlanificadorVisualIDRegistry
				.getModelID(containerView);
		if (!DailyPlanningEditPart.MODEL_ID.equals(containerModelID)) { return -1; }
		int containerVisualID;
		if (DailyPlanningEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = planificador.diagram.part.PlanificadorVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = DailyPlanningEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
			case DailyPlanningEditPart.VISUAL_ID:
				if (PlanificadorPackage.eINSTANCE.getResidential().isSuperTypeOf(domainElement.eClass())) { return ResidentialEditPart.VISUAL_ID; }
				if (PlanificadorPackage.eINSTANCE.getEnterprise().isSuperTypeOf(domainElement.eClass())) { return ResellerEditPart.VISUAL_ID; }
				if (PlanificadorPackage.eINSTANCE.getReseller().isSuperTypeOf(domainElement.eClass())) { return GenericTaskEditPart.VISUAL_ID; }
				if (PlanificadorPackage.eINSTANCE.getGenericTask().isSuperTypeOf(domainElement.eClass())) { return EnterpriseEditPart.VISUAL_ID; }
				break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = planificador.diagram.part.PlanificadorVisualIDRegistry
				.getModelID(containerView);
		if (!DailyPlanningEditPart.MODEL_ID.equals(containerModelID)) { return false; }
		int containerVisualID;
		if (DailyPlanningEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = planificador.diagram.part.PlanificadorVisualIDRegistry.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = DailyPlanningEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
			case ResidentialEditPart.VISUAL_ID:
				if (ResidentialShortNameEditPart.VISUAL_ID == nodeVisualID) { return true; }
				if (ResidentialStartTimeEditPart.VISUAL_ID == nodeVisualID) { return true; }
				break;
			case ResellerEditPart.VISUAL_ID:
				if (EnterpriseShortNameEditPart.VISUAL_ID == nodeVisualID) { return true; }
				break;
			case GenericTaskEditPart.VISUAL_ID:
				if (ResellerShortNameEditPart.VISUAL_ID == nodeVisualID) { return true; }
				break;
			case EnterpriseEditPart.VISUAL_ID:
				if (GenericTaskShortNameEditPart.VISUAL_ID == nodeVisualID) { return true; }
				break;
			case DailyPlanningEditPart.VISUAL_ID:
				if (ResidentialEditPart.VISUAL_ID == nodeVisualID) { return true; }
				if (ResellerEditPart.VISUAL_ID == nodeVisualID) { return true; }
				if (GenericTaskEditPart.VISUAL_ID == nodeVisualID) { return true; }
				if (EnterpriseEditPart.VISUAL_ID == nodeVisualID) { return true; }
				break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) { return -1; }
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(DailyPlanning element) {
		return true;
	}

}
