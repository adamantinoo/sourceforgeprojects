/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

import planificador.diagram.providers.PlanificadorElementTypes;

/**
 * @generated
 */
public class PlanificadorPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createTareasPlanificacion1Group());
	}

	/**
	 * Creates "Tareas Planificacion" palette tool group
	 * @generated
	 */
	private PaletteContainer createTareasPlanificacion1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(Messages.TareasPlanificacion1Group_title);
		paletteContainer.setDescription(Messages.TareasPlanificacion1Group_desc);
		paletteContainer.add(createGenericTask1CreationTool());
		paletteContainer.add(createResidential2CreationTool());
		paletteContainer.add(createEnterprise3CreationTool());
		paletteContainer.add(createReseller4CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGenericTask1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(PlanificadorElementTypes.Reseller_1003);
		NodeToolEntry entry = new NodeToolEntry(Messages.GenericTask1CreationTool_title,
				Messages.GenericTask1CreationTool_desc, types);
		entry.setSmallIcon(PlanificadorElementTypes.getImageDescriptor(PlanificadorElementTypes.Reseller_1003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createResidential2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(PlanificadorElementTypes.Residential_1001);
		NodeToolEntry entry = new NodeToolEntry(Messages.Residential2CreationTool_title,
				Messages.Residential2CreationTool_desc, types);
		entry
				.setSmallIcon(PlanificadorElementTypes.getImageDescriptor(PlanificadorElementTypes.Residential_1001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createEnterprise3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(PlanificadorElementTypes.Enterprise_1002);
		NodeToolEntry entry = new NodeToolEntry(Messages.Enterprise3CreationTool_title,
				Messages.Enterprise3CreationTool_desc, types);
		entry.setSmallIcon(PlanificadorElementTypes.getImageDescriptor(PlanificadorElementTypes.Enterprise_1002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReseller4CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(PlanificadorElementTypes.Reseller_1003);
		NodeToolEntry entry = new NodeToolEntry(Messages.Reseller4CreationTool_title,
				Messages.Reseller4CreationTool_desc, types);
		entry.setSmallIcon(PlanificadorElementTypes.getImageDescriptor(PlanificadorElementTypes.Reseller_1003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List	elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description, List elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
