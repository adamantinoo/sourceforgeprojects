/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramActionBarContributor;

/**
 * @generated
 */
public class PlanificadorDiagramActionBarContributor extends DiagramActionBarContributor {

	/**
	 * @generated
	 */
	protected Class getEditorClass() {
		return PlanificadorDiagramEditor.class;
	}

	/**
	 * @generated
	 */
	protected String getEditorId() {
		return PlanificadorDiagramEditor.ID;
	}
}
