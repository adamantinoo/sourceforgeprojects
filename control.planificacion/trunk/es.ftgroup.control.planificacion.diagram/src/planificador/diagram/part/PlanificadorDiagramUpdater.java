/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gmf.runtime.notation.View;

import planificador.DailyPlanning;
import planificador.Task;
import planificador.diagram.edit.parts.DailyPlanningEditPart;
import planificador.diagram.edit.parts.EnterpriseEditPart;
import planificador.diagram.edit.parts.GenericTaskEditPart;
import planificador.diagram.edit.parts.ResellerEditPart;
import planificador.diagram.edit.parts.ResidentialEditPart;

/**
 * @generated
 */
public class PlanificadorDiagramUpdater {

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view) {
		switch (PlanificadorVisualIDRegistry.getVisualID(view)) {
			case DailyPlanningEditPart.VISUAL_ID:
				return getDailyPlanning_79SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDailyPlanning_79SemanticChildren(View view) {
		if (!view.isSetElement()) { return Collections.EMPTY_LIST; }
		DailyPlanning modelElement = (DailyPlanning) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getTasks().iterator(); it.hasNext();) {
			Task childElement = (Task) it.next();
			int visualID = PlanificadorVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == ResidentialEditPart.VISUAL_ID) {
				result.add(new PlanificadorNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ResellerEditPart.VISUAL_ID) {
				result.add(new PlanificadorNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == GenericTaskEditPart.VISUAL_ID) {
				result.add(new PlanificadorNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == EnterpriseEditPart.VISUAL_ID) {
				result.add(new PlanificadorNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view) {
		switch (PlanificadorVisualIDRegistry.getVisualID(view)) {
			case DailyPlanningEditPart.VISUAL_ID:
				return getDailyPlanning_79ContainedLinks(view);
			case ResidentialEditPart.VISUAL_ID:
				return getResidential_1001ContainedLinks(view);
			case ResellerEditPart.VISUAL_ID:
				return getEnterprise_1002ContainedLinks(view);
			case GenericTaskEditPart.VISUAL_ID:
				return getReseller_1003ContainedLinks(view);
			case EnterpriseEditPart.VISUAL_ID:
				return getGenericTask_1004ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view) {
		switch (PlanificadorVisualIDRegistry.getVisualID(view)) {
			case ResidentialEditPart.VISUAL_ID:
				return getResidential_1001IncomingLinks(view);
			case ResellerEditPart.VISUAL_ID:
				return getEnterprise_1002IncomingLinks(view);
			case GenericTaskEditPart.VISUAL_ID:
				return getReseller_1003IncomingLinks(view);
			case EnterpriseEditPart.VISUAL_ID:
				return getGenericTask_1004IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view) {
		switch (PlanificadorVisualIDRegistry.getVisualID(view)) {
			case ResidentialEditPart.VISUAL_ID:
				return getResidential_1001OutgoingLinks(view);
			case ResellerEditPart.VISUAL_ID:
				return getEnterprise_1002OutgoingLinks(view);
			case GenericTaskEditPart.VISUAL_ID:
				return getReseller_1003OutgoingLinks(view);
			case EnterpriseEditPart.VISUAL_ID:
				return getGenericTask_1004OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getDailyPlanning_79ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getResidential_1001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEnterprise_1002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getReseller_1003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getGenericTask_1004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getResidential_1001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEnterprise_1002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getReseller_1003IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getGenericTask_1004IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getResidential_1001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEnterprise_1002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getReseller_1003OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getGenericTask_1004OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

}
