  /*
 * //  PROJECT:        es.ftgroup.control.planificacion
 * //  FILE NAME:      $RCSfile: ProcessorApp.java,v $
 * //  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
 * //  LAST UPDATE:    $Date$
 * //  RELEASE:        $Revision$
 * //  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
 * //  LAST USER:      $Author$
 * //  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
 * //  LOG:
 * //    $Log: ProcessorApp.java,v $
 * 
 */
package planificador.diagram.part;

import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDiagramDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocumentProvider;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.parts.DiagramDocumentEditor;
import org.eclipse.ui.IEditorInput;

    /**
 * @generated
 */
public class PlanificadorDiagramEditor  extends DiagramDocumentEditor {

      /**
 * @generated
 */
public static final String ID = "planificador.diagram.part.PlanificadorDiagramEditorID"; //$NON-NLS-1$
  
    /**
 * @generated
 */
public static final String CONTEXT_ID = "planificador.diagram.ui.diagramContext"; //$NON-NLS-1$
  
      /**
 * @generated
 */
public PlanificadorDiagramEditor() {
  super(true);
}
  
      /**
 * @generated
 */
protected String getContextID() {
  return CONTEXT_ID;
}
  
    /**
 * @generated
 */
protected PaletteRoot createPaletteRoot(PaletteRoot existingPaletteRoot) {
  PaletteRoot root = super.createPaletteRoot(existingPaletteRoot);
  new PlanificadorPaletteFactory().fillPalette(root);
  return root;
}
  
      /**
 * @generated
 */
protected PreferencesHint getPreferencesHint() {
  return PlanificadorDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT;
}
  
      /**
 * @generated
 */
public String getContributorId() {
  return PlanificadorDiagramEditorPlugin.ID;
}
      
      /**
 * @generated
 */
protected IDocumentProvider getDocumentProvider(IEditorInput input) {
  if (input instanceof URIEditorInput) {
    return PlanificadorDiagramEditorPlugin.getInstance().getDocumentProvider();
  }
  return super.getDocumentProvider(input);
}
  
      /**
 * @generated
 */
public TransactionalEditingDomain getEditingDomain() {
  IDocument document = getEditorInput() != null ? getDocumentProvider().getDocument(getEditorInput()) : null;
  if (document instanceof IDiagramDocument) {
    return ((IDiagramDocument) document).getEditingDomain();
  }
  return super.getEditingDomain();
}
  
      /**
 * @generated
 */
protected void setDocumentProvider(IEditorInput input) {
  if (input instanceof URIEditorInput) {
    setDocumentProvider(PlanificadorDiagramEditorPlugin.getInstance().getDocumentProvider());
  } else {
    super.setDocumentProvider(input);
  }
}
    
  }
