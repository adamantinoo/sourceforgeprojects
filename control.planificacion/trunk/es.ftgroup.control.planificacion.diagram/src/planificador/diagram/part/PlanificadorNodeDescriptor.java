/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.part;

import org.eclipse.emf.ecore.EObject;

/**
 * @generated
 */
public class PlanificadorNodeDescriptor {

	/**
	 * @generated
	 */
	private EObject	myModelElement;

	/**
	 * @generated
	 */
	private int			myVisualID;

	/**
	 * @generated
	 */
	private String	myType;

	/**
	 * @generated
	 */
	public PlanificadorNodeDescriptor(EObject modelElement, int visualID) {
		myModelElement = modelElement;
		myVisualID = visualID;
	}

	/**
	 * @generated
	 */
	public EObject getModelElement() {
		return myModelElement;
	}

	/**
	 * @generated
	 */
	public int getVisualID() {
		return myVisualID;
	}

	/**
	 * @generated
	 */
	public String getType() {
		if (myType == null) {
			myType = PlanificadorVisualIDRegistry.getType(getVisualID());
		}
		return myType;
	}

}
