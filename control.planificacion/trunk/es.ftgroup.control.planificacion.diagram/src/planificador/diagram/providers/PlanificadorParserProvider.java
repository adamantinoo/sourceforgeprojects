/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import planificador.PlanificadorPackage;
import planificador.diagram.edit.parts.EnterpriseShortNameEditPart;
import planificador.diagram.edit.parts.GenericTaskShortNameEditPart;
import planificador.diagram.edit.parts.ResellerShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialStartTimeEditPart;
import planificador.diagram.parsers.MessageFormatParser;
import planificador.diagram.part.PlanificadorVisualIDRegistry;

/**
 * @generated
 */
public class PlanificadorParserProvider extends AbstractProvider implements IParserProvider {

	/**
	 * @generated
	 */
	private IParser	residentialShortName_4001Parser;

	/**
	 * @generated
	 */
	private IParser getResidentialShortName_4001Parser() {
		if (residentialShortName_4001Parser == null) {
			residentialShortName_4001Parser = createResidentialShortName_4001Parser();
		}
		return residentialShortName_4001Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createResidentialShortName_4001Parser() {
		EAttribute[] features = new EAttribute[] { PlanificadorPackage.eINSTANCE.getTask_ShortName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser	residentialStartTime_4002Parser;

	/**
	 * @generated
	 */
	private IParser getResidentialStartTime_4002Parser() {
		if (residentialStartTime_4002Parser == null) {
			residentialStartTime_4002Parser = createResidentialStartTime_4002Parser();
		}
		return residentialStartTime_4002Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createResidentialStartTime_4002Parser() {
		EAttribute[] features = new EAttribute[] { PlanificadorPackage.eINSTANCE.getTask_StartTime(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser	enterpriseShortName_4003Parser;

	/**
	 * @generated
	 */
	private IParser getEnterpriseShortName_4003Parser() {
		if (enterpriseShortName_4003Parser == null) {
			enterpriseShortName_4003Parser = createEnterpriseShortName_4003Parser();
		}
		return enterpriseShortName_4003Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createEnterpriseShortName_4003Parser() {
		EAttribute[] features = new EAttribute[] { PlanificadorPackage.eINSTANCE.getTask_ShortName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser	resellerShortName_4004Parser;

	/**
	 * @generated
	 */
	private IParser getResellerShortName_4004Parser() {
		if (resellerShortName_4004Parser == null) {
			resellerShortName_4004Parser = createResellerShortName_4004Parser();
		}
		return resellerShortName_4004Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createResellerShortName_4004Parser() {
		EAttribute[] features = new EAttribute[] { PlanificadorPackage.eINSTANCE.getTask_ShortName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	private IParser	genericTaskShortName_4005Parser;

	/**
	 * @generated
	 */
	private IParser getGenericTaskShortName_4005Parser() {
		if (genericTaskShortName_4005Parser == null) {
			genericTaskShortName_4005Parser = createGenericTaskShortName_4005Parser();
		}
		return genericTaskShortName_4005Parser;
	}

	/**
	 * @generated
	 */
	protected IParser createGenericTaskShortName_4005Parser() {
		EAttribute[] features = new EAttribute[] { PlanificadorPackage.eINSTANCE.getTask_ShortName(), };
		MessageFormatParser parser = new MessageFormatParser(features);
		return parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
			case ResidentialShortNameEditPart.VISUAL_ID:
				return getResidentialShortName_4001Parser();
			case ResidentialStartTimeEditPart.VISUAL_ID:
				return getResidentialStartTime_4002Parser();
			case EnterpriseShortNameEditPart.VISUAL_ID:
				return getEnterpriseShortName_4003Parser();
			case ResellerShortNameEditPart.VISUAL_ID:
				return getResellerShortName_4004Parser();
			case GenericTaskShortNameEditPart.VISUAL_ID:
				return getGenericTaskShortName_4005Parser();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) { return getParser(PlanificadorVisualIDRegistry.getVisualID(vid)); }
		View view = (View) hint.getAdapter(View.class);
		if (view != null) { return getParser(PlanificadorVisualIDRegistry.getVisualID(view)); }
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (PlanificadorElementTypes.getElement(hint) == null) { return false; }
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType	elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) { return elementType; }
			return super.getAdapter(adapter);
		}
	}

}
