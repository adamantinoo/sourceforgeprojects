/*
 * copyright France Telecom Espa�a.
 * programmer Luis de Diego
 */
package planificador.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.providers.AbstractViewProvider;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.View;

import planificador.diagram.edit.parts.DailyPlanningEditPart;
import planificador.diagram.edit.parts.EnterpriseEditPart;
import planificador.diagram.edit.parts.EnterpriseShortNameEditPart;
import planificador.diagram.edit.parts.GenericTaskEditPart;
import planificador.diagram.edit.parts.GenericTaskShortNameEditPart;
import planificador.diagram.edit.parts.ResellerEditPart;
import planificador.diagram.edit.parts.ResellerShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialEditPart;
import planificador.diagram.edit.parts.ResidentialShortNameEditPart;
import planificador.diagram.edit.parts.ResidentialStartTimeEditPart;
import planificador.diagram.part.PlanificadorVisualIDRegistry;
import planificador.diagram.view.factories.DailyPlanningViewFactory;
import planificador.diagram.view.factories.EnterpriseShortNameViewFactory;
import planificador.diagram.view.factories.EnterpriseViewFactory;
import planificador.diagram.view.factories.GenericTaskShortNameViewFactory;
import planificador.diagram.view.factories.GenericTaskViewFactory;
import planificador.diagram.view.factories.ResellerShortNameViewFactory;
import planificador.diagram.view.factories.ResellerViewFactory;
import planificador.diagram.view.factories.ResidentialShortNameViewFactory;
import planificador.diagram.view.factories.ResidentialStartTimeViewFactory;
import planificador.diagram.view.factories.ResidentialViewFactory;

/**
 * @generated
 */
public class PlanificadorViewProvider extends AbstractViewProvider {

	/**
	 * @generated
	 */
	protected Class getDiagramViewClass(IAdaptable semanticAdapter, String diagramKind) {
		EObject semanticElement = getSemanticElement(semanticAdapter);
		if (DailyPlanningEditPart.MODEL_ID.equals(diagramKind)
				&& PlanificadorVisualIDRegistry.getDiagramVisualID(semanticElement) != -1) { return DailyPlanningViewFactory.class; }
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(IAdaptable semanticAdapter, View containerView, String semanticHint) {
		if (containerView == null) { return null; }
		IElementType elementType = getSemanticElementType(semanticAdapter);
		EObject domainElement = getSemanticElement(semanticAdapter);
		int visualID;
		if (semanticHint == null) {
			// Semantic hint is not specified. Can be a result of call from CanonicalEditPolicy.
			// In this situation there should be NO elementType, visualID will be determined
			// by VisualIDRegistry.getNodeVisualID() for domainElement.
			if (elementType != null || domainElement == null) { return null; }
			visualID = PlanificadorVisualIDRegistry.getNodeVisualID(containerView, domainElement);
		} else {
			visualID = PlanificadorVisualIDRegistry.getVisualID(semanticHint);
			if (elementType != null) {
				// Semantic hint is specified together with element type.
				// Both parameters should describe exactly the same diagram element.
				// In addition we check that visualID returned by VisualIDRegistry.getNodeVisualID() for
				// domainElement (if specified) is the same as in element type.
				if (!PlanificadorElementTypes.isKnownElementType(elementType)
						|| (!(elementType instanceof IHintedType))) { return null; // foreign element type
				}
				String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
				if (!semanticHint.equals(elementTypeHint)) { return null; // if semantic hint is specified it should be the same as in element type
				}
				if (domainElement != null
						&& visualID != PlanificadorVisualIDRegistry.getNodeVisualID(containerView, domainElement)) { return null; // visual id for node EClass should match visual id from element type
				}
			} else {
				// Element type is not specified. Domain element should be present (except pure design elements).
				// This method is called with EObjectAdapter as parameter from:
				//   - ViewService.createNode(View container, EObject eObject, String type, PreferencesHint preferencesHint) 
				//   - generated ViewFactory.decorateView() for parent element
				if (!DailyPlanningEditPart.MODEL_ID.equals(PlanificadorVisualIDRegistry.getModelID(containerView))) { return null; // foreign diagram
				}
				switch (visualID) {
					case ResidentialEditPart.VISUAL_ID:
					case ResellerEditPart.VISUAL_ID:
					case GenericTaskEditPart.VISUAL_ID:
					case EnterpriseEditPart.VISUAL_ID:
						if (domainElement == null
								|| visualID != PlanificadorVisualIDRegistry.getNodeVisualID(containerView, domainElement)) { return null; // visual id in semantic hint should match visual id for domain element
						}
						break;
					case ResidentialShortNameEditPart.VISUAL_ID:
					case ResidentialStartTimeEditPart.VISUAL_ID:
						if (ResidentialEditPart.VISUAL_ID != PlanificadorVisualIDRegistry.getVisualID(containerView)
								|| containerView.getElement() != domainElement) { return null; // wrong container
						}
						break;
					case EnterpriseShortNameEditPart.VISUAL_ID:
						if (ResellerEditPart.VISUAL_ID != PlanificadorVisualIDRegistry.getVisualID(containerView)
								|| containerView.getElement() != domainElement) { return null; // wrong container
						}
						break;
					case ResellerShortNameEditPart.VISUAL_ID:
						if (GenericTaskEditPart.VISUAL_ID != PlanificadorVisualIDRegistry.getVisualID(containerView)
								|| containerView.getElement() != domainElement) { return null; // wrong container
						}
						break;
					case GenericTaskShortNameEditPart.VISUAL_ID:
						if (EnterpriseEditPart.VISUAL_ID != PlanificadorVisualIDRegistry.getVisualID(containerView)
								|| containerView.getElement() != domainElement) { return null; // wrong container
						}
						break;
					default:
						return null;
				}
			}
		}
		return getNodeViewClass(containerView, visualID);
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(View containerView, int visualID) {
		if (containerView == null || !PlanificadorVisualIDRegistry.canCreateNode(containerView, visualID)) { return null; }
		switch (visualID) {
			case ResidentialEditPart.VISUAL_ID:
				return ResidentialViewFactory.class;
			case ResidentialShortNameEditPart.VISUAL_ID:
				return ResidentialShortNameViewFactory.class;
			case ResidentialStartTimeEditPart.VISUAL_ID:
				return ResidentialStartTimeViewFactory.class;
			case ResellerEditPart.VISUAL_ID:
				return ResellerViewFactory.class;
			case EnterpriseShortNameEditPart.VISUAL_ID:
				return EnterpriseShortNameViewFactory.class;
			case GenericTaskEditPart.VISUAL_ID:
				return GenericTaskViewFactory.class;
			case ResellerShortNameEditPart.VISUAL_ID:
				return ResellerShortNameViewFactory.class;
			case EnterpriseEditPart.VISUAL_ID:
				return EnterpriseViewFactory.class;
			case GenericTaskShortNameEditPart.VISUAL_ID:
				return GenericTaskShortNameViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(IAdaptable semanticAdapter, View containerView, String semanticHint) {
		IElementType elementType = getSemanticElementType(semanticAdapter);
		if (!PlanificadorElementTypes.isKnownElementType(elementType) || (!(elementType instanceof IHintedType))) { return null; // foreign element type
		}
		String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
		if (elementTypeHint == null) { return null; // our hint is visual id and must be specified
		}
		if (semanticHint != null && !semanticHint.equals(elementTypeHint)) { return null; // if semantic hint is specified it should be the same as in element type
		}
		int visualID = PlanificadorVisualIDRegistry.getVisualID(elementTypeHint);
		EObject domainElement = getSemanticElement(semanticAdapter);
		if (domainElement != null
				&& visualID != PlanificadorVisualIDRegistry.getLinkWithClassVisualID(domainElement)) { return null; // visual id for link EClass should match visual id from element type
		}
		return getEdgeViewClass(visualID);
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(int visualID) {
		switch (visualID) {
		}
		return null;
	}

	/**
	 * @generated
	 */
	private IElementType getSemanticElementType(IAdaptable semanticAdapter) {
		if (semanticAdapter == null) { return null; }
		return (IElementType) semanticAdapter.getAdapter(IElementType.class);
	}
}
