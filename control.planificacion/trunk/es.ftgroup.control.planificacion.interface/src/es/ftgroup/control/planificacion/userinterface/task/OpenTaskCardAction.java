//  PROJECT:        es.ftgroup.control.planificacion.interface
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.userinterface.task;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.control.planificacion.userinterface.ICommandIds;
import es.ftgroup.control.planificacion.userinterface.OpenViewAction;

import org.eclipse.ui.IWorkbenchWindow;

// - CLASS IMPLEMENTATION ...................................................................................
public class OpenTaskCardAction extends OpenViewAction {
	private static Logger				logger				= Logger.getLogger("es.ftgroup.control.planificacion.userinterface.task");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public OpenTaskCardAction(IWorkbenchWindow window, String label, String viewId) {
		super(window, label, viewId);
		this.window = window;
		this.viewId = viewId;
		this.label = label;
		setText(label);
		// The id is used to refer to the action in a menu or toolbar
		setId(ICommandIds.CMD_OPEN_TASK);
		// Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(ICommandIds.CMD_OPEN_TASK);
		setImageDescriptor(es.ftgroup.control.planificacion.plugin.Activator.getImageDescriptor("/icons/icon-task.gif.gif"));
	}
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// - I N T E R F A C E - N A M E
	// - P R O T E C T E D - S E C T I O N
}

// - UNUSED CODE ............................................................................................
