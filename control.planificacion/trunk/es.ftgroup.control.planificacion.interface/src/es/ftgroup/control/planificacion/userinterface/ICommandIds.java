package es.ftgroup.control.planificacion.userinterface;

/**
 * Interface defining the application's command IDs. Key bindings can be defined for specific commands. To
 * associate an action with a command, use IAction.setActionDefinitionId(commandId).
 * 
 * @see org.eclipse.jface.action.IAction#setActionDefinitionId(String)
 */
public interface ICommandIds {

	public static final String	CMD_OPEN					= "es.ftgroup.control.planificacion.userinterface.open";
	public static final String	CMD_OPEN_TASK			= "es.ftgroup.control.planificacion.userinterface.open.task";
	public static final String	CMD_OPEN_MESSAGE	= "es.ftgroup.control.planificacion.userinterface.openMessage";

}
