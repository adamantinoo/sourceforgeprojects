//  PROJECT:        es.ftgroup.control.planificacion.interface
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2008 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package es.ftgroup.control.planificacion.userinterface.task;

import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.SWT;
// - IMPORT SECTION .........................................................................................
import java.util.Date;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.jface.viewers.ComboViewer;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class Base extends ViewPart {

	public static final String	ID	= "es.ftgroup.control.planificacion.userinterface.task.Base"; // TODO Needs to be whatever is mentioned in plugin.xml
	private String[]	groups= {"GROUPNAME",
			"COLLECTIONS",
			"INFORMES",
			"COMSYS",
			"WEBMETHODS",
			"FACTURACION_RESELLER",
			"PRE_TARIFICACION",
			"POST_TARIFICACION",
			"PROVISION",
			"GRF",
			"TARIFICACION_RESELLERS"
				};

	private Composite						top	= null;
	private Composite taskClass = null;
	private Label labelGroup = null;
	private Combo groupCombo = null;
	private ComboViewer groupComboViewer = null;
	private Composite taskName = null;
	private Label	labelShortName;
	private Combo	nameCombo;
	private ComboViewer nameComboViewer = null;

	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		top = new Composite(parent, SWT.NONE);
		top.setLayout(gridLayout);
		createTaskClass();
		createTaskName();
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub

	}

	/**
	 * This method initializes TaskClass	
	 *
	 */
	private void createTaskClass() {
		taskClass = new Composite(top, SWT.NONE);
		taskClass.setLayout(new RowLayout());
		labelGroup = new Label(taskClass, SWT.NONE);
		labelGroup.setText("Group");
		groupCombo = new Combo(taskClass, SWT.NONE);
		groupComboViewer = new ComboViewer(groupCombo);
		groupComboViewer.setLabelProvider(new GroupLabelProvider());
		groupComboViewer.add("RETRIBUCION");
		groupComboViewer.add("JNL");
		groupComboViewer.add("POST_BIP");
		groupComboViewer.add("TARIFICACION");
		groupComboViewer.add("ARCHIVADO");
	}

	/**
	 * This method initializes taskName	
	 *
	 */
	private void createTaskName() {
		taskName = new Composite(top, SWT.NONE);
		taskName.setLayout(new RowLayout());
		labelShortName = new Label(taskName, SWT.NONE);
		labelShortName.setText("Short Name");
		nameCombo = new Combo(taskName, SWT.NONE);
		nameComboViewer = new ComboViewer(nameCombo);
	}
}
class GroupLabelProvider implements IBaseLabelProvider{
	
}
// - UNUSED CODE ............................................................................................
