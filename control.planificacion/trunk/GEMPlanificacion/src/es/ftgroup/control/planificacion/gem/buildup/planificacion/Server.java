/**
 * France Telecom Espa�a, S.A.
 * Luis de Diego Toro
 * Control Facturacion Fijo
 *
 * $Id$
 */
package es.ftgroup.control.planificacion.gem.buildup.planificacion;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Server</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getCustomerID <em>Customer ID</em>}</li>
 *   <li>{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getServerID <em>Server ID</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.PlanificacionPackage#getServer()
 * @model
 * @generated
 */
public interface Server extends EObject {
	/**
	 * Returns the value of the '<em><b>Customer ID</b></em>' attribute.
	 * The literals are from the enumeration {@link es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Customer ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Customer ID</em>' attribute.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers
	 * @see #setCustomerID(AvailableServers)
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.PlanificacionPackage#getServer_CustomerID()
	 * @model
	 * @generated
	 */
	AvailableServers getCustomerID();

	/**
	 * Sets the value of the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getCustomerID <em>Customer ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Customer ID</em>' attribute.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers
	 * @see #getCustomerID()
	 * @generated
	 */
	void setCustomerID(AvailableServers value);

	/**
	 * Returns the value of the '<em><b>Server ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Server ID</em>' attribute.
	 * @see #setServerID(int)
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.PlanificacionPackage#getServer_ServerID()
	 * @model
	 * @generated
	 */
	int getServerID();

	/**
	 * Sets the value of the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getServerID <em>Server ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Server ID</em>' attribute.
	 * @see #getServerID()
	 * @generated
	 */
	void setServerID(int value);

} // Server
