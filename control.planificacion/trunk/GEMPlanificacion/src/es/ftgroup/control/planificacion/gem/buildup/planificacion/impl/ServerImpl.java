/**
 * France Telecom Espa�a, S.A.
 * Luis de Diego Toro
 * Control Facturacion Fijo
 *
 * $Id$
 */
package es.ftgroup.control.planificacion.gem.buildup.planificacion.impl;

import es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers;
import es.ftgroup.control.planificacion.gem.buildup.planificacion.PlanificacionPackage;
import es.ftgroup.control.planificacion.gem.buildup.planificacion.Server;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl#getCustomerID <em>Customer ID</em>}</li>
 *   <li>{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl#getServerID <em>Server ID</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServerImpl extends EObjectImpl implements Server {
	/**
	 * The default value of the '{@link #getCustomerID() <em>Customer ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomerID()
	 * @generated
	 * @ordered
	 */
	protected static final AvailableServers CUSTOMER_ID_EDEFAULT = AvailableServers.ADMIN;

	/**
	 * The cached value of the '{@link #getCustomerID() <em>Customer ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomerID()
	 * @generated
	 * @ordered
	 */
	protected AvailableServers customerID = CUSTOMER_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getServerID() <em>Server ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServerID()
	 * @generated
	 * @ordered
	 */
	protected static final int SERVER_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getServerID() <em>Server ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServerID()
	 * @generated
	 * @ordered
	 */
	protected int serverID = SERVER_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificacionPackage.Literals.SERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AvailableServers getCustomerID() {
		return customerID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCustomerID(AvailableServers newCustomerID) {
		AvailableServers oldCustomerID = customerID;
		customerID = newCustomerID == null ? CUSTOMER_ID_EDEFAULT : newCustomerID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.SERVER__CUSTOMER_ID, oldCustomerID, customerID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getServerID() {
		return serverID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServerID(int newServerID) {
		int oldServerID = serverID;
		serverID = newServerID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlanificacionPackage.SERVER__SERVER_ID, oldServerID, serverID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlanificacionPackage.SERVER__CUSTOMER_ID:
				return getCustomerID();
			case PlanificacionPackage.SERVER__SERVER_ID:
				return new Integer(getServerID());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlanificacionPackage.SERVER__CUSTOMER_ID:
				setCustomerID((AvailableServers)newValue);
				return;
			case PlanificacionPackage.SERVER__SERVER_ID:
				setServerID(((Integer)newValue).intValue());
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlanificacionPackage.SERVER__CUSTOMER_ID:
				setCustomerID(CUSTOMER_ID_EDEFAULT);
				return;
			case PlanificacionPackage.SERVER__SERVER_ID:
				setServerID(SERVER_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlanificacionPackage.SERVER__CUSTOMER_ID:
				return customerID != CUSTOMER_ID_EDEFAULT;
			case PlanificacionPackage.SERVER__SERVER_ID:
				return serverID != SERVER_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (customerID: ");
		result.append(customerID);
		result.append(", serverID: ");
		result.append(serverID);
		result.append(')');
		return result.toString();
	}

} //ServerImpl
