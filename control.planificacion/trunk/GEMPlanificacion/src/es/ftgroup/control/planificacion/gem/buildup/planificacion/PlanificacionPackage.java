/**
 * France Telecom Espa�a, S.A.
 * Luis de Diego Toro
 * Control Facturacion Fijo
 *
 * $Id$
 */
package es.ftgroup.control.planificacion.gem.buildup.planificacion;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.PlanificacionFactory
 * @model kind="package"
 * @generated
 */
public interface PlanificacionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "planificacion";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/examples/gmf/planificacion";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "planificacion";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlanificacionPackage eINSTANCE = es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl.init();

	/**
	 * The meta object id for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.DailyPlanningImpl <em>Daily Planning</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.DailyPlanningImpl
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getDailyPlanning()
	 * @generated
	 */
	int DAILY_PLANNING = 0;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING__TASKS = 0;

	/**
	 * The feature id for the '<em><b>Represented Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING__REPRESENTED_DATE = 1;

	/**
	 * The number of structural features of the '<em>Daily Planning</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.TaskImpl
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SHORT_NAME = 0;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__LONG_NAME = 1;

	/**
	 * The feature id for the '<em><b>Servers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SERVERS = 2;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl <em>Server</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getServer()
	 * @generated
	 */
	int SERVER = 2;

	/**
	 * The feature id for the '<em><b>Customer ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__CUSTOMER_ID = 0;

	/**
	 * The feature id for the '<em><b>Server ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER__SERVER_ID = 1;

	/**
	 * The number of structural features of the '<em>Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers <em>Available Servers</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getAvailableServers()
	 * @generated
	 */
	int AVAILABLE_SERVERS = 3;


	/**
	 * Returns the meta object for class '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning <em>Daily Planning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Daily Planning</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning
	 * @generated
	 */
	EClass getDailyPlanning();

	/**
	 * Returns the meta object for the containment reference list '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning#getTasks()
	 * @see #getDailyPlanning()
	 * @generated
	 */
	EReference getDailyPlanning_Tasks();

	/**
	 * Returns the meta object for the attribute '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning#getRepresentedDate <em>Represented Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Represented Date</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.DailyPlanning#getRepresentedDate()
	 * @see #getDailyPlanning()
	 * @generated
	 */
	EAttribute getDailyPlanning_RepresentedDate();

	/**
	 * Returns the meta object for class '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the attribute '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getShortName()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getLongName <em>Long Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Name</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getLongName()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_LongName();

	/**
	 * Returns the meta object for the containment reference list '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getServers <em>Servers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Servers</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Task#getServers()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Servers();

	/**
	 * Returns the meta object for class '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server <em>Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Server
	 * @generated
	 */
	EClass getServer();

	/**
	 * Returns the meta object for the attribute '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getCustomerID <em>Customer ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Customer ID</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getCustomerID()
	 * @see #getServer()
	 * @generated
	 */
	EAttribute getServer_CustomerID();

	/**
	 * Returns the meta object for the attribute '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getServerID <em>Server ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Server ID</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.Server#getServerID()
	 * @see #getServer()
	 * @generated
	 */
	EAttribute getServer_ServerID();

	/**
	 * Returns the meta object for enum '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers <em>Available Servers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Available Servers</em>'.
	 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers
	 * @generated
	 */
	EEnum getAvailableServers();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PlanificacionFactory getPlanificacionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.DailyPlanningImpl <em>Daily Planning</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.DailyPlanningImpl
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getDailyPlanning()
		 * @generated
		 */
		EClass DAILY_PLANNING = eINSTANCE.getDailyPlanning();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAILY_PLANNING__TASKS = eINSTANCE.getDailyPlanning_Tasks();

		/**
		 * The meta object literal for the '<em><b>Represented Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAILY_PLANNING__REPRESENTED_DATE = eINSTANCE.getDailyPlanning_RepresentedDate();

		/**
		 * The meta object literal for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.TaskImpl
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__SHORT_NAME = eINSTANCE.getTask_ShortName();

		/**
		 * The meta object literal for the '<em><b>Long Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__LONG_NAME = eINSTANCE.getTask_LongName();

		/**
		 * The meta object literal for the '<em><b>Servers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__SERVERS = eINSTANCE.getTask_Servers();

		/**
		 * The meta object literal for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl <em>Server</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.ServerImpl
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getServer()
		 * @generated
		 */
		EClass SERVER = eINSTANCE.getServer();

		/**
		 * The meta object literal for the '<em><b>Customer ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER__CUSTOMER_ID = eINSTANCE.getServer_CustomerID();

		/**
		 * The meta object literal for the '<em><b>Server ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER__SERVER_ID = eINSTANCE.getServer_ServerID();

		/**
		 * The meta object literal for the '{@link es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers <em>Available Servers</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.AvailableServers
		 * @see es.ftgroup.control.planificacion.gem.buildup.planificacion.impl.PlanificacionPackageImpl#getAvailableServers()
		 * @generated
		 */
		EEnum AVAILABLE_SERVERS = eINSTANCE.getAvailableServers();

	}

} //PlanificacionPackage
