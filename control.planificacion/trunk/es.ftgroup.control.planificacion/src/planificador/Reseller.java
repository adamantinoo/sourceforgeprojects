/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reseller</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see planificador.PlanificadorPackage#getReseller()
 * @model
 * @generated
 */
public interface Reseller extends Task {
} // Reseller
