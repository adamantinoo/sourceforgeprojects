/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see planificador.PlanificadorFactory
 * @model kind="package"
 * @generated
 */
public interface PlanificadorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "planificador";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://i-ldediego.cosmos.es.ftgroup/control/planificacion/Planificador";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "planificador";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PlanificadorPackage eINSTANCE = planificador.impl.PlanificadorPackageImpl.init();

	/**
	 * The meta object id for the '{@link planificador.impl.DailyPlanningImpl <em>Daily Planning</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.DailyPlanningImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getDailyPlanning()
	 * @generated
	 */
	int DAILY_PLANNING = 0;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING__TASKS = 0;

	/**
	 * The feature id for the '<em><b>Represented Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING__REPRESENTED_DATE = 1;

	/**
	 * The number of structural features of the '<em>Daily Planning</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_PLANNING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link planificador.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.TaskImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 1;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__START_TIME = 0;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SHORT_NAME = 1;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__LONG_NAME = 2;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link planificador.impl.GenericTaskImpl <em>Generic Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.GenericTaskImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getGenericTask()
	 * @generated
	 */
	int GENERIC_TASK = 2;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TASK__START_TIME = TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TASK__SHORT_NAME = TASK__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TASK__LONG_NAME = TASK__LONG_NAME;

	/**
	 * The feature id for the '<em><b>Launcher</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TASK__LAUNCHER = TASK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Generic Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERIC_TASK_FEATURE_COUNT = TASK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link planificador.impl.ResidentialImpl <em>Residential</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.ResidentialImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getResidential()
	 * @generated
	 */
	int RESIDENTIAL = 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENTIAL__START_TIME = TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENTIAL__SHORT_NAME = TASK__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENTIAL__LONG_NAME = TASK__LONG_NAME;

	/**
	 * The number of structural features of the '<em>Residential</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESIDENTIAL_FEATURE_COUNT = TASK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link planificador.impl.EnterpriseImpl <em>Enterprise</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.EnterpriseImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getEnterprise()
	 * @generated
	 */
	int ENTERPRISE = 4;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTERPRISE__START_TIME = TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTERPRISE__SHORT_NAME = TASK__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTERPRISE__LONG_NAME = TASK__LONG_NAME;

	/**
	 * The number of structural features of the '<em>Enterprise</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTERPRISE_FEATURE_COUNT = TASK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link planificador.impl.ResellerImpl <em>Reseller</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.impl.ResellerImpl
	 * @see planificador.impl.PlanificadorPackageImpl#getReseller()
	 * @generated
	 */
	int RESELLER = 5;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESELLER__START_TIME = TASK__START_TIME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESELLER__SHORT_NAME = TASK__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Long Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESELLER__LONG_NAME = TASK__LONG_NAME;

	/**
	 * The number of structural features of the '<em>Reseller</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESELLER_FEATURE_COUNT = TASK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link planificador.AvailableServers <em>Available Servers</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see planificador.AvailableServers
	 * @see planificador.impl.PlanificadorPackageImpl#getAvailableServers()
	 * @generated
	 */
	int AVAILABLE_SERVERS = 6;


	/**
	 * Returns the meta object for class '{@link planificador.DailyPlanning <em>Daily Planning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Daily Planning</em>'.
	 * @see planificador.DailyPlanning
	 * @generated
	 */
	EClass getDailyPlanning();

	/**
	 * Returns the meta object for the containment reference list '{@link planificador.DailyPlanning#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see planificador.DailyPlanning#getTasks()
	 * @see #getDailyPlanning()
	 * @generated
	 */
	EReference getDailyPlanning_Tasks();

	/**
	 * Returns the meta object for the attribute '{@link planificador.DailyPlanning#getRepresentedDate <em>Represented Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Represented Date</em>'.
	 * @see planificador.DailyPlanning#getRepresentedDate()
	 * @see #getDailyPlanning()
	 * @generated
	 */
	EAttribute getDailyPlanning_RepresentedDate();

	/**
	 * Returns the meta object for class '{@link planificador.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see planificador.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the attribute list '{@link planificador.Task#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Start Time</em>'.
	 * @see planificador.Task#getStartTime()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link planificador.Task#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see planificador.Task#getShortName()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link planificador.Task#getLongName <em>Long Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Name</em>'.
	 * @see planificador.Task#getLongName()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_LongName();

	/**
	 * Returns the meta object for class '{@link planificador.GenericTask <em>Generic Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic Task</em>'.
	 * @see planificador.GenericTask
	 * @generated
	 */
	EClass getGenericTask();

	/**
	 * Returns the meta object for the attribute list '{@link planificador.GenericTask#getLauncher <em>Launcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Launcher</em>'.
	 * @see planificador.GenericTask#getLauncher()
	 * @see #getGenericTask()
	 * @generated
	 */
	EAttribute getGenericTask_Launcher();

	/**
	 * Returns the meta object for class '{@link planificador.Residential <em>Residential</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Residential</em>'.
	 * @see planificador.Residential
	 * @generated
	 */
	EClass getResidential();

	/**
	 * Returns the meta object for class '{@link planificador.Enterprise <em>Enterprise</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enterprise</em>'.
	 * @see planificador.Enterprise
	 * @generated
	 */
	EClass getEnterprise();

	/**
	 * Returns the meta object for class '{@link planificador.Reseller <em>Reseller</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reseller</em>'.
	 * @see planificador.Reseller
	 * @generated
	 */
	EClass getReseller();

	/**
	 * Returns the meta object for enum '{@link planificador.AvailableServers <em>Available Servers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Available Servers</em>'.
	 * @see planificador.AvailableServers
	 * @generated
	 */
	EEnum getAvailableServers();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PlanificadorFactory getPlanificadorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link planificador.impl.DailyPlanningImpl <em>Daily Planning</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.DailyPlanningImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getDailyPlanning()
		 * @generated
		 */
		EClass DAILY_PLANNING = eINSTANCE.getDailyPlanning();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DAILY_PLANNING__TASKS = eINSTANCE.getDailyPlanning_Tasks();

		/**
		 * The meta object literal for the '<em><b>Represented Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAILY_PLANNING__REPRESENTED_DATE = eINSTANCE.getDailyPlanning_RepresentedDate();

		/**
		 * The meta object literal for the '{@link planificador.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.TaskImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__START_TIME = eINSTANCE.getTask_StartTime();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__SHORT_NAME = eINSTANCE.getTask_ShortName();

		/**
		 * The meta object literal for the '<em><b>Long Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__LONG_NAME = eINSTANCE.getTask_LongName();

		/**
		 * The meta object literal for the '{@link planificador.impl.GenericTaskImpl <em>Generic Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.GenericTaskImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getGenericTask()
		 * @generated
		 */
		EClass GENERIC_TASK = eINSTANCE.getGenericTask();

		/**
		 * The meta object literal for the '<em><b>Launcher</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERIC_TASK__LAUNCHER = eINSTANCE.getGenericTask_Launcher();

		/**
		 * The meta object literal for the '{@link planificador.impl.ResidentialImpl <em>Residential</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.ResidentialImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getResidential()
		 * @generated
		 */
		EClass RESIDENTIAL = eINSTANCE.getResidential();

		/**
		 * The meta object literal for the '{@link planificador.impl.EnterpriseImpl <em>Enterprise</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.EnterpriseImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getEnterprise()
		 * @generated
		 */
		EClass ENTERPRISE = eINSTANCE.getEnterprise();

		/**
		 * The meta object literal for the '{@link planificador.impl.ResellerImpl <em>Reseller</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.impl.ResellerImpl
		 * @see planificador.impl.PlanificadorPackageImpl#getReseller()
		 * @generated
		 */
		EClass RESELLER = eINSTANCE.getReseller();

		/**
		 * The meta object literal for the '{@link planificador.AvailableServers <em>Available Servers</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see planificador.AvailableServers
		 * @see planificador.impl.PlanificadorPackageImpl#getAvailableServers()
		 * @generated
		 */
		EEnum AVAILABLE_SERVERS = eINSTANCE.getAvailableServers();

	}

} //PlanificadorPackage
