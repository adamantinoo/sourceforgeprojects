/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Available Servers</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see planificador.PlanificadorPackage#getAvailableServers()
 * @model
 * @generated
 */
public enum AvailableServers implements Enumerator {
	/**
	 * The '<em><b>Admin</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADMIN_VALUE
	 * @generated
	 * @ordered
	 */
	ADMIN(100, "Admin", "ADMIN"),

	/**
	 * The '<em><b>C101</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #C101_VALUE
	 * @generated
	 * @ordered
	 */
	C101(101, "C101", "CUSTOMER101"),

	/**
	 * The '<em><b>C102</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #C102_VALUE
	 * @generated
	 * @ordered
	 */
	C102(102, "C102", "CUSTOMER102"),

	/**
	 * The '<em><b>C201</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #C201_VALUE
	 * @generated
	 * @ordered
	 */
	C201(201, "C201", "CUSTOMER201");

	/**
	 * The '<em><b>Admin</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Admin</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADMIN
	 * @model name="Admin" literal="ADMIN"
	 * @generated
	 * @ordered
	 */
	public static final int ADMIN_VALUE = 100;

	/**
	 * The '<em><b>C101</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>C101</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #C101
	 * @model literal="CUSTOMER101"
	 * @generated
	 * @ordered
	 */
	public static final int C101_VALUE = 101;

	/**
	 * The '<em><b>C102</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>C102</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #C102
	 * @model literal="CUSTOMER102"
	 * @generated
	 * @ordered
	 */
	public static final int C102_VALUE = 102;

	/**
	 * The '<em><b>C201</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>C201</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #C201
	 * @model literal="CUSTOMER201"
	 * @generated
	 * @ordered
	 */
	public static final int C201_VALUE = 201;

	/**
	 * An array of all the '<em><b>Available Servers</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AvailableServers[] VALUES_ARRAY =
		new AvailableServers[] {
			ADMIN,
			C101,
			C102,
			C201,
		};

	/**
	 * A public read-only list of all the '<em><b>Available Servers</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AvailableServers> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Available Servers</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AvailableServers get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AvailableServers result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Available Servers</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AvailableServers getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AvailableServers result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Available Servers</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AvailableServers get(int value) {
		switch (value) {
			case ADMIN_VALUE: return ADMIN;
			case C101_VALUE: return C101;
			case C102_VALUE: return C102;
			case C201_VALUE: return C201;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AvailableServers(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AvailableServers
