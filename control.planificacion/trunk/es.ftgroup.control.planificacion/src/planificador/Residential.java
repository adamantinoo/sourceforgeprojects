/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Residential</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see planificador.PlanificadorPackage#getResidential()
 * @model
 * @generated
 */
public interface Residential extends Task {
} // Residential
