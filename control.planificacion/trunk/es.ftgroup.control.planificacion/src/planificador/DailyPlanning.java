/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;

import java.util.Date;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Daily Planning</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link planificador.DailyPlanning#getTasks <em>Tasks</em>}</li>
 *   <li>{@link planificador.DailyPlanning#getRepresentedDate <em>Represented Date</em>}</li>
 * </ul>
 * </p>
 *
 * @see planificador.PlanificadorPackage#getDailyPlanning()
 * @model
 * @generated
 */
public interface DailyPlanning extends EObject {
	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link planificador.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tasks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see planificador.PlanificadorPackage#getDailyPlanning_Tasks()
	 * @model containment="true"
	 * @generated
	 */
	EList<Task> getTasks();

	/**
	 * Returns the value of the '<em><b>Represented Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Represented Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Represented Date</em>' attribute.
	 * @see #setRepresentedDate(Date)
	 * @see planificador.PlanificadorPackage#getDailyPlanning_RepresentedDate()
	 * @model
	 * @generated
	 */
	Date getRepresentedDate();

	/**
	 * Sets the value of the '{@link planificador.DailyPlanning#getRepresentedDate <em>Represented Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Represented Date</em>' attribute.
	 * @see #getRepresentedDate()
	 * @generated
	 */
	void setRepresentedDate(Date value);

} // DailyPlanning
