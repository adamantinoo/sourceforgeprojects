/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enterprise</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see planificador.PlanificadorPackage#getEnterprise()
 * @model
 * @generated
 */
public interface Enterprise extends Task {
} // Enterprise
