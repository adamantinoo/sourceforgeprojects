/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import org.eclipse.emf.ecore.EClass;

import planificador.Enterprise;
import planificador.PlanificadorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enterprise</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EnterpriseImpl extends TaskImpl implements Enterprise {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnterpriseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificadorPackage.Literals.ENTERPRISE;
	}

} //EnterpriseImpl
