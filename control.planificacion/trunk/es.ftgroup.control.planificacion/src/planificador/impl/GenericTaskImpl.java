/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import planificador.GenericTask;
import planificador.PlanificadorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link planificador.impl.GenericTaskImpl#getLauncher <em>Launcher</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GenericTaskImpl extends TaskImpl implements GenericTask {
	/**
	 * The cached value of the '{@link #getLauncher() <em>Launcher</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLauncher()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> launcher;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificadorPackage.Literals.GENERIC_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getLauncher() {
		if (launcher == null) {
			launcher = new EDataTypeUniqueEList<Integer>(Integer.class, this, PlanificadorPackage.GENERIC_TASK__LAUNCHER);
		}
		return launcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlanificadorPackage.GENERIC_TASK__LAUNCHER:
				return getLauncher();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlanificadorPackage.GENERIC_TASK__LAUNCHER:
				getLauncher().clear();
				getLauncher().addAll((Collection<? extends Integer>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlanificadorPackage.GENERIC_TASK__LAUNCHER:
				getLauncher().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlanificadorPackage.GENERIC_TASK__LAUNCHER:
				return launcher != null && !launcher.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (launcher: ");
		result.append(launcher);
		result.append(')');
		return result.toString();
	}

} //GenericTaskImpl
