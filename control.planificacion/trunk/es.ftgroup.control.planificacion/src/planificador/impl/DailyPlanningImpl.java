/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import planificador.DailyPlanning;
import planificador.PlanificadorPackage;
import planificador.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Daily Planning</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link planificador.impl.DailyPlanningImpl#getTasks <em>Tasks</em>}</li>
 *   <li>{@link planificador.impl.DailyPlanningImpl#getRepresentedDate <em>Represented Date</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DailyPlanningImpl extends EObjectImpl implements DailyPlanning {
	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> tasks;

	/**
	 * The default value of the '{@link #getRepresentedDate() <em>Represented Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentedDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date REPRESENTED_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRepresentedDate() <em>Represented Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentedDate()
	 * @generated
	 * @ordered
	 */
	protected Date representedDate = REPRESENTED_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DailyPlanningImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificadorPackage.Literals.DAILY_PLANNING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentEList<Task>(Task.class, this, PlanificadorPackage.DAILY_PLANNING__TASKS);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getRepresentedDate() {
		return representedDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentedDate(Date newRepresentedDate) {
		Date oldRepresentedDate = representedDate;
		representedDate = newRepresentedDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PlanificadorPackage.DAILY_PLANNING__REPRESENTED_DATE, oldRepresentedDate, representedDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PlanificadorPackage.DAILY_PLANNING__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PlanificadorPackage.DAILY_PLANNING__TASKS:
				return getTasks();
			case PlanificadorPackage.DAILY_PLANNING__REPRESENTED_DATE:
				return getRepresentedDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PlanificadorPackage.DAILY_PLANNING__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends Task>)newValue);
				return;
			case PlanificadorPackage.DAILY_PLANNING__REPRESENTED_DATE:
				setRepresentedDate((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PlanificadorPackage.DAILY_PLANNING__TASKS:
				getTasks().clear();
				return;
			case PlanificadorPackage.DAILY_PLANNING__REPRESENTED_DATE:
				setRepresentedDate(REPRESENTED_DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PlanificadorPackage.DAILY_PLANNING__TASKS:
				return tasks != null && !tasks.isEmpty();
			case PlanificadorPackage.DAILY_PLANNING__REPRESENTED_DATE:
				return REPRESENTED_DATE_EDEFAULT == null ? representedDate != null : !REPRESENTED_DATE_EDEFAULT.equals(representedDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (representedDate: ");
		result.append(representedDate);
		result.append(')');
		return result.toString();
	}

} //DailyPlanningImpl
