/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import planificador.AvailableServers;
import planificador.DailyPlanning;
import planificador.Enterprise;
import planificador.GenericTask;
import planificador.PlanificadorFactory;
import planificador.PlanificadorPackage;
import planificador.Reseller;
import planificador.Residential;
import planificador.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PlanificadorPackageImpl extends EPackageImpl implements PlanificadorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dailyPlanningEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass genericTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass residentialEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enterpriseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resellerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum availableServersEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see planificador.PlanificadorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PlanificadorPackageImpl() {
		super(eNS_URI, PlanificadorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PlanificadorPackage init() {
		if (isInited) return (PlanificadorPackage)EPackage.Registry.INSTANCE.getEPackage(PlanificadorPackage.eNS_URI);

		// Obtain or create and register package
		PlanificadorPackageImpl thePlanificadorPackage = (PlanificadorPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof PlanificadorPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new PlanificadorPackageImpl());

		isInited = true;

		// Create package meta-data objects
		thePlanificadorPackage.createPackageContents();

		// Initialize created meta-data
		thePlanificadorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePlanificadorPackage.freeze();

		return thePlanificadorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDailyPlanning() {
		return dailyPlanningEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDailyPlanning_Tasks() {
		return (EReference)dailyPlanningEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDailyPlanning_RepresentedDate() {
		return (EAttribute)dailyPlanningEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_StartTime() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_ShortName() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_LongName() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGenericTask() {
		return genericTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGenericTask_Launcher() {
		return (EAttribute)genericTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResidential() {
		return residentialEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnterprise() {
		return enterpriseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReseller() {
		return resellerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAvailableServers() {
		return availableServersEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlanificadorFactory getPlanificadorFactory() {
		return (PlanificadorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dailyPlanningEClass = createEClass(DAILY_PLANNING);
		createEReference(dailyPlanningEClass, DAILY_PLANNING__TASKS);
		createEAttribute(dailyPlanningEClass, DAILY_PLANNING__REPRESENTED_DATE);

		taskEClass = createEClass(TASK);
		createEAttribute(taskEClass, TASK__START_TIME);
		createEAttribute(taskEClass, TASK__SHORT_NAME);
		createEAttribute(taskEClass, TASK__LONG_NAME);

		genericTaskEClass = createEClass(GENERIC_TASK);
		createEAttribute(genericTaskEClass, GENERIC_TASK__LAUNCHER);

		residentialEClass = createEClass(RESIDENTIAL);

		enterpriseEClass = createEClass(ENTERPRISE);

		resellerEClass = createEClass(RESELLER);

		// Create enums
		availableServersEEnum = createEEnum(AVAILABLE_SERVERS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		genericTaskEClass.getESuperTypes().add(this.getTask());
		residentialEClass.getESuperTypes().add(this.getTask());
		enterpriseEClass.getESuperTypes().add(this.getTask());
		resellerEClass.getESuperTypes().add(this.getTask());

		// Initialize classes and features; add operations and parameters
		initEClass(dailyPlanningEClass, DailyPlanning.class, "DailyPlanning", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDailyPlanning_Tasks(), this.getTask(), null, "tasks", null, 0, -1, DailyPlanning.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDailyPlanning_RepresentedDate(), ecorePackage.getEDate(), "representedDate", null, 0, 1, DailyPlanning.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTask_StartTime(), ecorePackage.getEInt(), "startTime", null, 100, -1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_ShortName(), ecorePackage.getEString(), "shortName", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_LongName(), ecorePackage.getEString(), "longName", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(genericTaskEClass, GenericTask.class, "GenericTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGenericTask_Launcher(), ecorePackage.getEInt(), "launcher", null, 0, 4, GenericTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(residentialEClass, Residential.class, "Residential", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(enterpriseEClass, Enterprise.class, "Enterprise", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(resellerEClass, Reseller.class, "Reseller", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(availableServersEEnum, AvailableServers.class, "AvailableServers");
		addEEnumLiteral(availableServersEEnum, AvailableServers.ADMIN);
		addEEnumLiteral(availableServersEEnum, AvailableServers.C101);
		addEEnumLiteral(availableServersEEnum, AvailableServers.C102);
		addEEnumLiteral(availableServersEEnum, AvailableServers.C201);

		// Create resource
		createResource(eNS_URI);
	}

} //PlanificadorPackageImpl
