/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import org.eclipse.emf.ecore.EClass;

import planificador.PlanificadorPackage;
import planificador.Reseller;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reseller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ResellerImpl extends TaskImpl implements Reseller {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResellerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificadorPackage.Literals.RESELLER;
	}

} //ResellerImpl
