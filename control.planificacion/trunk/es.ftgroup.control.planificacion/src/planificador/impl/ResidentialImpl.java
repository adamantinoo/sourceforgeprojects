/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador.impl;

import org.eclipse.emf.ecore.EClass;

import planificador.PlanificadorPackage;
import planificador.Residential;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Residential</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ResidentialImpl extends TaskImpl implements Residential {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResidentialImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PlanificadorPackage.Literals.RESIDENTIAL;
	}

} //ResidentialImpl
