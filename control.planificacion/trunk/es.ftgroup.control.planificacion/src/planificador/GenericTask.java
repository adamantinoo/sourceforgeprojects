/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package planificador;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link planificador.GenericTask#getLauncher <em>Launcher</em>}</li>
 * </ul>
 * </p>
 *
 * @see planificador.PlanificadorPackage#getGenericTask()
 * @model
 * @generated
 */
public interface GenericTask extends Task {
	/**
	 * Returns the value of the '<em><b>Launcher</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Integer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Launcher</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Launcher</em>' attribute list.
	 * @see planificador.PlanificadorPackage#getGenericTask_Launcher()
	 * @model upper="4"
	 * @generated
	 */
	EList<Integer> getLauncher();

} // GenericTask
