package es.ftgroup.control.planificacion.editors;

import org.eclipse.ui.editors.text.TextEditor;

public class ControlPlanificacionEditor extends TextEditor {

	private ColorManager	colorManager;

	public ControlPlanificacionEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new XMLConfiguration(colorManager));
		setDocumentProvider(new XMLDocumentProvider());
	}

	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}

}
