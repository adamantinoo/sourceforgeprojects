package es.ftgroup.control.planificacion.gem.buildup.figures;

import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel;

/**
 * @generated
 */
public class ServerFigure extends RectangleFigure {

	/**
	 * @generated
	 */
	private WrapLabel	fFigureServerCustomerIDFigure;

	/**
	 * @generated
	 */
	public ServerFigure() {

		FlowLayout layoutThis = new FlowLayout();
		layoutThis.setStretchMinorAxis(false);
		layoutThis.setMinorAlignment(FlowLayout.ALIGN_LEFTTOP);

		layoutThis.setMajorAlignment(FlowLayout.ALIGN_LEFTTOP);
		layoutThis.setMajorSpacing(5);
		layoutThis.setMinorSpacing(5);
		layoutThis.setHorizontal(true);

		this.setLayoutManager(layoutThis);

		createContents();
	}

	/**
	 * @generated
	 */
	private void createContents() {

		fFigureServerCustomerIDFigure = new WrapLabel();
		fFigureServerCustomerIDFigure.setText("<...>");

		this.add(fFigureServerCustomerIDFigure);

	}

	/**
	 * @generated
	 */
	private boolean	myUseLocalCoordinates	= false;

	/**
	 * @generated
	 */
	protected boolean useLocalCoordinates() {
		return myUseLocalCoordinates;
	}

	/**
	 * @generated
	 */
	protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
		myUseLocalCoordinates = useLocalCoordinates;
	}

	/**
	 * @generated
	 */
	public WrapLabel getFigureServerCustomerIDFigure() {
		return fFigureServerCustomerIDFigure;
	}

}
