package es.ftgroup.control.planificacion.gem.buildup.figures;

import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrapLabel;

/**
 * @generated
 */
public class TaskFigure extends RectangleFigure {

	/**
	 * @generated
	 */
	private WrapLabel	fFigureTaskShortNameFigure;
	/**
	 * @generated
	 */
	private WrapLabel	fFigureTaskLongNameFigure;

	/**
	 * @generated
	 */
	public TaskFigure() {

		FlowLayout layoutThis = new FlowLayout();
		layoutThis.setStretchMinorAxis(false);
		layoutThis.setMinorAlignment(FlowLayout.ALIGN_LEFTTOP);

		layoutThis.setMajorAlignment(FlowLayout.ALIGN_LEFTTOP);
		layoutThis.setMajorSpacing(5);
		layoutThis.setMinorSpacing(5);
		layoutThis.setHorizontal(true);

		this.setLayoutManager(layoutThis);

		createContents();
	}

	/**
	 * @generated
	 */
	private void createContents() {

		fFigureTaskShortNameFigure = new WrapLabel();
		fFigureTaskShortNameFigure.setText("<...>");

		this.add(fFigureTaskShortNameFigure);

		fFigureTaskLongNameFigure = new WrapLabel();
		fFigureTaskLongNameFigure.setText("<...>");

		this.add(fFigureTaskLongNameFigure);

	}

	/**
	 * @generated
	 */
	private boolean	myUseLocalCoordinates	= false;

	/**
	 * @generated
	 */
	protected boolean useLocalCoordinates() {
		return myUseLocalCoordinates;
	}

	/**
	 * @generated
	 */
	protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
		myUseLocalCoordinates = useLocalCoordinates;
	}

	/**
	 * @generated
	 */
	public WrapLabel getFigureTaskShortNameFigure() {
		return fFigureTaskShortNameFigure;
	}

	/**
	 * @generated
	 */
	public WrapLabel getFigureTaskLongNameFigure() {
		return fFigureTaskLongNameFigure;
	}

}
