package es.ftgroup.control.planificacion.gem.buildup.figures.activator;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.IMapMode;
import org.eclipse.gmf.runtime.draw2d.ui.mapmode.MapModeTypes;

/**
 * @generated
 */
public class PluginActivator extends Plugin {

	/**
	 * @generated
	 */
	public static final String			ID	= "es.ftgroup.control.planificacion.gem.buildup.figures"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static PluginActivator	ourInstance;

	/**
	 * @generated
	 */
	public PluginActivator() {
	}

	/**
	 * @generated
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		ourInstance = this;
	}

	/**
	 * @generated
	 */
	public void stop(BundleContext context) throws Exception {
		ourInstance = null;
		super.stop(context);
	}

	/**
	 * @generated
	 */
	public static PluginActivator getDefault() {
		return ourInstance;
	}
}
