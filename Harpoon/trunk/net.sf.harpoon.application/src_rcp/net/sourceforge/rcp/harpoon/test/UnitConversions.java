//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitConversions.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/UnitConversions.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:25:14  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.tests;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitConversions {
	
	static public final double EARTH_RADIUS = 6378.140;
	
	/** Convert degress to radians */
	static public double Deg2Rad(double degrees) {
		return (Math.PI * degrees) / 180.0;
	}

	/** Convert radians to degrees */
	static public double Rad2Deg(double rads) {
		return (rads * 180.0) / Math.PI;
	}

}

// - UNUSED CODE ............................................................................................
