//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SelectionView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/views/SelectionView.java,v $
//  LAST UPDATE:    $Date: 2008-03-24 16:10:22 $
//  RELEASE:        $Revision: 1.16 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.15  2007-12-10 13:56:32  ldiego
//    - TASK Separate to new source files the classes related to the ReferencePoint.
//    - TASK Changed the management of visibility. Now state comes
//      from the parent state.
//    - TASK Added a new listening to the page to be updated when the model changes.
//    - TASK Removed some outdated code.
//
//    Revision 1.14  2007-11-30 12:11:36  ldiego
//    - MOVEMENTPATH.R01.C - Movement path changes.
//    - MOVEMENTPATH.R01.E - Movement path changes.
//    - MOVEMENTPATH.R01.D - Movement path changes.
//    - TASK All plugins unified into a single one plugin because the need
//      to access some classes between source file sets.
//    - TASK reorganization of classes into a new set of packages
//      reducing the name length.
//
//    Revision 1.13  2007-11-16 10:50:02  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.12  2007-11-02 09:35:04  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.11.2.4  2007-10-31 14:44:38  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.11.2.3  2007-10-24 16:45:44  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.11.2.2  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.11.2.1  2007-10-11 07:52:30  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.11  2007-10-05 11:24:44  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.10  2007-10-03 16:50:47  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.9  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.8  2007-10-02 09:04:25  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.7  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.6  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.5  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.4  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.3  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-17 15:11:40  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//

package net.sf.harpoon.views;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import net.sf.harpoon.app.HarpoonRegistry;
import net.sf.harpoon.editors.SceneryEditor;
import net.sf.harpoon.editors.SceneryPage;
import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.pages.IPropertyPage;
import net.sourceforge.harpoon.parts.AirportPart;
import net.sourceforge.harpoon.parts.ISelectablePart;
import net.sourceforge.harpoon.parts.ReferencePart;
import net.sourceforge.harpoon.parts.WarPart;
import net.sourceforge.harpoon.parts.WirePart;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This view is responsible to display the properties of the selected unit when there is only one unit selected or if
 * the selection contains more than one item, it should display the list of elements in the selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection to the
 * newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class SelectionView extends ViewPart {
	private static Logger					logger				= Logger.getLogger("net.sf.harpoon.views");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	public static final String		ID						= "net.sourceforge.rcp.harpoon.app.selectionview";

	// private static SelectionView singleton;
	/**
	 * Top element in the display hierarchy. The children of this element are the ones disposed when changing the
	 * selection.
	 */
	private Composite							top;
	/**
	 * Stores the list of used pages to be removed when the selection changes. If not removed the pages stay dangling from
	 * the listener notification and receive events when they are not visible or used.
	 */
	private Vector<IPropertyPage>	propertyPages	= new Vector<IPropertyPage>();

	// - S T A T I C - S E C T I O N
	// public static SelectionView getSingleton() {
	// return singleton;
	// }

	// - C O N S T R U C T O R S
	public SelectionView() {
		// - Register the view. This will remove the requirement to have the view declared as a static singleton
		HarpoonRegistry.getRegistry().put(ID, this);
	}

	// - P U B L I C - S E C T I O N
	public Composite getTopControl() {
		return top;
	}

	/**
	 * Disposes the elements that are hanging from the top container of the view. This really clears the view contents and
	 * prepares the control to display a new set of elements that will be added in sequence. This method also clears the
	 * list of pages that belong to selected elements that are the be available for disposal and disconnected from the
	 * notification system.
	 */
	public void clearTopControl() {
		final Control[] childs = top.getChildren();
		for (int i = 0; i < childs.length; i++) {
			final Control child = childs[i];
			child.dispose();
		}
		// - Clear property pages references to clean up the listeners.
		final Iterator<IPropertyPage> it = propertyPages.iterator();
		while (it.hasNext()) {
			it.next().dispose();
		}
		propertyPages = new Vector<IPropertyPage>();
	}

	/**
	 * Updates the content of the <code>SelectionView</code> with the selection elements from the
	 * <code>EditorPanel</code>.<br>
	 * The contents presentation depends on the selection. For single units the view presents some information, for
	 * multiple units it displays a list of the selected units and if the selection goes to default (the background Map)
	 * the view displays the whole list of visible units.
	 */
	public void updateSelection(StructuredSelection selectionContent, IWorkbenchPart part) {
		if (!selectionContent.isEmpty()) {
			// - Count the elements in the selection to check if we display a property page or a table
			if (selectionContent.size() > 1) {
				updateMultipleSelection(selectionContent, part);
			} else {
				// - Get the Part selected (there is only one) and convert it to the higher common interface implemented.
				// FIXME Change this global class to a new interface created for this implementation
				final ISelectablePart selectedPart = (ISelectablePart) selectionContent.getFirstElement();
				updateSingleSelection(selectedPart);
			}
		}
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change their
	 * presentation dynamically depending on the selection, so there should be a link point where other content structures
	 * can plug-in to be displayed.
	 */
	@Override
	public void createPartControl(Composite parent) {
		// singleton = this;
		// - Create a new container and set it to a layout of row elements inside a one column grid.
		top = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		top.setLayout(layout);
	}

	@Override
	public void setFocus() {
	}

	// - P R O T E C T E D - S E C T I O N
	private void addPropertyPage(IPropertyPage page) {
		propertyPages.addElement(page);
	}

	private void newTableViewer(StructuredSelection selectionContent, final IWorkbenchPart part2) {
		// TODO Create a list of special labels (with icon) and put them in a column
		final Composite table = new Composite(top, SWT.NONE);
		final FillLayout grid = new FillLayout();
		// grid.numColumns = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.type = SWT.VERTICAL;
		table.setLayout(grid);
		final SelectionView vv = this;
		final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
		while (it.hasNext()) {
			final AbstractGraphicalEditPart part = it.next();
			final Unit model = (Unit) part.getModel();
			final CLabel lb = new CLabel(table, SWT.SHADOW_OUT);
			lb.setData(part);
			lb.addMouseListener(new MouseListener() {

				public void mouseDoubleClick(MouseEvent e) {
					// TODO The label has been selected and then we can forward this to the new selection
					logger.info("Label " + lb.getText() + " selected");
				}

				public void mouseDown(MouseEvent e) {
					// TODO Access the Editor. This is not currently accesible at this point. Check if it is possible to get a
					// reference when getting the selection
					if (part2 instanceof SceneryEditor) {
						// TODO Get a reference to the GraphicalViewer. This is located inside the Editor Page.
						final SceneryEditor editor = (SceneryEditor) part2;
						final SceneryPage page = editor.getMapPage();
						final GraphicalViewer viewer = page.getGraphicalViewer();
						// - The Label generic Data contains the EditPart to be selected.
						final EditPart data = (EditPart) lb.getData();
						// DEBUG Allow the paths to be invidible to perform selection on units
						// if(data instanceof HarpoonPartFactory$PathPart)
						// ((AbstractGraphicalEditPart) data).getFigure().setVisible(false);
						viewer.select(data);

						// TODO Update the selection again after the change
						// updateSelection(StructuredSelection selectionContent

						logger.info("Label " + lb.getText() + " selected");
					}
				}

				public void mouseUp(MouseEvent e) {
					// EMPTY method. Not being used
				}

			});
			final ImageDescriptor im = HarpoonRegistry.getImageDescriptor("/icons/sample2.gif");
			lb.setImage(im.createImage());
			lb.setText(model.getName());
		}
	}

	private void updateMultipleSelection(StructuredSelection selectionContent, IWorkbenchPart workbench) {
		clearTopControl();
		// - Display a table with the selection elements
		newTableViewer(selectionContent, workbench);

		final Composite table = new Composite(top, SWT.NONE);
		final FillLayout grid = new FillLayout();
		// grid.numColumns = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.type = SWT.VERTICAL;
		table.setLayout(grid);

		// TODO For each selected element add a new <code>Group</code> that is clickable to set the selection
		final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
		while (it.hasNext()) {
			final AbstractGraphicalEditPart part = it.next();
			final IPropertyPage page = ((ISelectablePart) part).createPropertyPage(table, false);
			addPropertyPage(page);
		}
	}

	/**
	 * Depending on the part type selected this method creates and connect to the <code>top</code> container the
	 * presentation data that is available for that unit. There are presentation differences depending on unit side and
	 * other game model data (detection, type).
	 */
	private void updateSingleSelection(ISelectablePart selectedPart) {
		// - Detect the type of part selected and do the operations necessary for each type
		if (selectedPart instanceof WirePart) { return; }
		if (selectedPart instanceof AirportPart) {
			final AirportPart airport = (AirportPart) selectedPart;
			clearTopControl();
			// - Airports present the same identification information as units but have additional interface elements.
			addPropertyPage(airport.createPropertyPage(top, true));
		}
		if (selectedPart instanceof WarPart) {
			final WarPart war = (WarPart) selectedPart;
			clearTopControl();
			addPropertyPage(war.createPropertyPage(top, true));
		}
		if (selectedPart instanceof ReferencePart) {
			final ReferencePart reference = (ReferencePart) selectedPart;
			clearTopControl();
			addPropertyPage(reference.createPropertyPage(top, true));
		}
		top.layout();
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// Unit abstractModel = (Unit) thePart.getModel();
// // Object page = abstractModel.getPropertyPage();
// // if(null!=page) {
// // clearTopControl();
// // abstractModel.createPropertyPage(top);
// // }
//
// // TODO If the selection is the RootPart then show a table viewer will all the visible units
// // - Detect if the part is a Friend. This displays the full information
// final String side = abstractModel.getSide();
// if (Unit.FRIEND_SIDE.equals(side)) {
// // [04]
// if (abstractPart instanceof WarPart) {
// clearTopControl();
// final WarPart war = (WarPart) abstractPart;
// war.createPropertyPage(top);
// // page.redraw();
// // page.changed(page.getChildren());
// }
// } else if (abstractPart instanceof ReferencePart) {
// clearTopControl();
// final ReferencePart reference = (ReferencePart) abstractPart;
// reference.createPropertyPage(top);
// } else {
// // - The unit selected is not a Friend unit. We can compose some small information panel
// clearTopControl();
// // - Display a table with the selection elements
// newTableViewer(selectionContent, part);
// }}
