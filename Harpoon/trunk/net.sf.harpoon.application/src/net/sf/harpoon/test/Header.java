//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: Header.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/Header.java,v $
//  LAST UPDATE:    $Date: 2008-03-24 16:10:22 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sf.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class Header {
	private static Logger				logger				= Logger.getLogger("net.sf.harpoon.test");
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static final String	CONSTANT_NAME	= "CONSTANT_VALUE";

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public Header() {
	}
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
}

// - UNUSED CODE ............................................................................................
