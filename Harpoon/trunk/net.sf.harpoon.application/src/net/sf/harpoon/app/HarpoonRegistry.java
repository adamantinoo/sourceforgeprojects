//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonRegistry.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonRegistry.java,v $
//  LAST UPDATE:    $Date: 2008-03-24 16:10:22 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-11-30 12:11:35  ldiego
//    - MOVEMENTPATH.R01.C - Movement path changes.
//    - MOVEMENTPATH.R01.E - Movement path changes.
//    - MOVEMENTPATH.R01.D - Movement path changes.
//    - TASK All plugins unified into a single one plugin because the need
//      to access some classes between source file sets.
//    - TASK reorganization of classes into a new set of packages
//      reducing the name length.
//
//    Revision 1.4  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//

package net.sf.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The activator class controls the plug-in life cycle. It also contains a registry to keep global access to a set of
 * instances that will be required along the life cycle.
 */
public class HarpoonRegistry extends AbstractUIPlugin {
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	/** The plug-in ID. */
	public static final String							PLUGIN_ID	= "net.sourceforge.rcp.harpoon";

	// - S T A T I C - S E C T I O N
	/** The shared instance singleton. */
	private static HarpoonRegistry					plugin;
	/** Hash map where I can store and then retrieve global items. */
	private static HashMap<Object, Object>	registry	= new HashMap<Object, Object>();

	// - M E T H O D - S E C T I O N ..........................................................................
	// - S T A T I C - S E C T I O N
	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path.
	 * 
	 * @param path
	 *          the relative path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * Returns an element in the registry that it is identified by the unique ID. If the element is not found in the
	 * registry then an exception is thrown to be cached by any methods that will interpret this runtime class of
	 * exceptions.
	 */
	public static Object getByID(String id) {
		final Object reference = registry.get(id);
		Assert.isNotNull(reference, "Reference in the registry is not found. This is a runtime error.");
		return reference;
	}

	// - C O N S T R U C T O R S
	// /** Constructor will create and initialize the registry. */
	// public HarpoonRegistry() {
	// registry = new HashMap<Object, Object>();
	// }

	// - G E T T E R S / S E T T E R S
	public static HashMap<Object, Object> getRegistry() {
		return registry;
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Called at initialization when the plug-in is read.
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		// - Register the plugin
		registry.put(PLUGIN_ID, this);
	}

	/**
	 * I suppose that this is called when the application is about to stop. Not needs more implementation.
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		// - Unregister the plugin
		registry.remove(PLUGIN_ID);
		super.stop(context);
	}
	// [01]

}
// - UNUSED CODE ............................................................................................
// [01]
// // TODO This two methods perform the same operation. Remove one of them (the less used)
// /**
// * Returns the shared instance
// *
// * @return the shared instance
// */
// public static HarpoonRegistry getDefault() {
// return plugin;
// }
//
// /**
// * Returns the shared instance.
// */
// public static HarpoonRegistry getInstance() {
// return plugin;
// }
