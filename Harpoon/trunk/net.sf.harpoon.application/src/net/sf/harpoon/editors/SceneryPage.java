//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryPage.java,v $
//  LAST UPDATE:    $Date: 2008-03-24 16:10:22 $
//  RELEASE:        $Revision: 1.11 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//									Copyright (c) 2000, 2003 IBM Corporation and others.
//									All rights reserved. This program and the accompanying materials 
//									are made available under the terms of the Common Public License v1.0
//									which accompanies this distribution, and is available at
//									http://www.eclipse.org/legal/cpl-v10.html
//
//									Contributors:
//									    IBM Corporation - initial API and implementation
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.10  2007-11-30 12:11:35  ldiego
//    - MOVEMENTPATH.R01.C - Movement path changes.
//    - MOVEMENTPATH.R01.E - Movement path changes.
//    - MOVEMENTPATH.R01.D - Movement path changes.
//    - TASK All plugins unified into a single one plugin because the need
//      to access some classes between source file sets.
//    - TASK reorganization of classes into a new set of packages
//      reducing the name length.
//
//    Revision 1.9  2007-11-27 10:30:21  ldiego
//    - Added a Tool Palette support to create new tool entries.
//
//    Revision 1.8  2007-11-07 16:28:57  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.7  2007-11-02 09:35:04  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.6.2.2  2007-10-31 14:44:38  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.6.2.1  2007-10-24 16:45:44  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.6  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.5  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.4  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.3  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-10 12:56:18  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//

package net.sf.harpoon.editors;

//- IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.Tool;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.palette.CombinedTemplateCreationEntry;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.SelectionToolEntry;
import org.eclipse.gef.requests.SimpleFactory;
import org.eclipse.gef.tools.CreationTool;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.part.MultiPageEditorPart;

import net.sf.harpoon.app.HarpoonRegistry;
import net.sf.harpoon.model.units.ReferencePoint;
import net.sf.harpoon.model.units.RootMapUnit;

import net.sourceforge.harpoon.parts.HarpoonPartFactory;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This the the main editor page. It has been modified to support for the main game window where the scenery map is
 * shown and then the game units are displayed.
 * 
 * @author Luis de Diego
 * @author Gunnar Wagenknecht
 */
public class SceneryPage extends EditorPart {
	// - S T A T I C - S E C T I O N
	public static final String									SELECTION_TOOL						= "SceneryPage.SELECTION_TOOL";
	public static final String									MARQUEE_TOOL							= "SceneryPage.MARQUEE_TOOL";
	public static final String									MOVEMENTPATHCREATION_TOOL	= "SceneryPage.MOVEMENTPATHCREATION_TOOL";

	private static Logger												logger										= Logger.getLogger("net.sf.harpoon");
	/** Tool used to make selections. */
	public static SelectionToolEntry						selectionTool;
	/** Tool used to make selections by selecting an area. */
	public static MarqueeToolEntry							marqueeTool;
	/** Tool to create new reference points and edit movement paths. */
	public static CombinedTemplateCreationEntry	referencePointCreationTool;
	/* Initialize the tools used on this view. */
	static {
		selectionTool = new SelectionToolEntry();
		marqueeTool = new MarqueeToolEntry();
		referencePointCreationTool = new CombinedTemplateCreationEntry("Reference Point",
				"Create the reference point for the construction of a movement path", ReferencePoint.class,
				new SimpleFactory(ReferencePoint.class), HarpoonRegistry.getImageDescriptor("icons/ellipse16.gif"),
				HarpoonRegistry.getImageDescriptor("icons/ellipse24.gif"));
	}
	/** The parent multi page editor. */
	protected final MultiPageEditorPart					editor;
	/** The edit domain to be used by default. */
	protected static EditDomain									domain;
	/** The graphical viewer where the unit information is displayed. */
	protected GraphicalViewer										viewer;

	// [01]

	public static void setTool(String toolID) {
		if (SELECTION_TOOL.equals(toolID)) {
			final Tool tool = selectionTool.createTool();
			domain.setActiveTool(tool);
		}
		if (MARQUEE_TOOL.equals(toolID)) {
			final Tool tool = marqueeTool.createTool();
			domain.setActiveTool(tool);
		}
		if (MOVEMENTPATHCREATION_TOOL.equals(toolID)) {
			final CreationTool tool = (CreationTool) referencePointCreationTool.createTool();
			domain.setActiveTool(tool);
		}
	}

	// - C O N S T R U C T O R S
	/**
	 * Creates a new SceneryPage instance.
	 * 
	 * By design this page uses its own <code>EditDomain</code>. The main goal of this approach is that this page has
	 * its own undo/redo command stack.<br>
	 * Undo/redo is something that may be disabled in the game. Set the domain to the default domain used in other
	 * examples.
	 * 
	 * @param editorOwner
	 *          the parent multi page editor
	 */
	public SceneryPage(MultiPageEditorPart editorOwner) {
		super();
		editor = editorOwner;
		setEditDomain();
		setTool(SELECTION_TOOL);
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Returns the edit domain this editor page uses.
	 * 
	 * @return the edit domain this editor page uses
	 */
	public final EditDomain getEditDomain() {
		return domain;
	}

	public void setEditDomain() {
		domain = new DefaultEditDomain(editor);
	}

	/**
	 * Returns the graphical viewer of this page.<br>
	 * This viewer is used for example for zoom support and for the thumbnail in the overview of the outline page.
	 * 
	 * @return the viewer
	 */
	public GraphicalViewer getGraphicalViewer() {
		return viewer;
	}

	// [02]

	/**
	 * Returns the multi page workflow editor this editor page is contained in.
	 * 
	 * @return the parent multi page editor
	 */
	protected final MultiPageEditorPart getSceneryEditor() {
		return editor;
	}

	/**
	 * Returns the scenery root of the model that is being played.
	 * 
	 * @return the scenery model
	 */
	protected RootMapUnit getSceneryModel() {
		final SceneryEditor editor = (SceneryEditor) getSceneryEditor();
		return editor.modelUnits;
	}

	@Override
	public String getTitle() {
		// TODO See when is used this name in the display area.
		return "Edit the whole workflow";
	}

	/**
	 * Returns the human readable name of this editor page. Return the name for this page. It seems that is something
	 * superfluous.
	 * 
	 * @return the human readable name of this editor page
	 */
	protected String getPageName() {
		// TODO See when is used this name in the display area.
		return "Main Scenery";
	}

	/**
	 * Returns the <code>CommandStack</code> of this editor page.
	 * 
	 * @return the <code>CommandStack</code> of this editor page
	 */
	protected final CommandStack getCommandStack() {
		return getEditDomain().getCommandStack();
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
		// TODO See when is used this name in the display area.
		setPartName("Ttile in Tab");
		setContentDescription("This part description");
	}

	/**
	 * This method is called when the interface creates the page to be displayed in the editor panel. The functionality is
	 * to create the graphic elements that compose the page and initialize them.<br>
	 * <br>
	 * This particular implementation we discard the palette creation and we set the main contents to our graphical
	 * viewer. Then we load the model into the viewer to create the controllers and the view elements.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#createPageControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		// - Create a simple control and set the full fill layout. This is the base control for the viewer.
		final Canvas can = new Canvas(parent, SWT.NONE);
		can.setLayout(new FillLayout());
		viewer = new GraphicalViewerImpl();
		viewer.setControl(can);
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());

		// - Hook the viewer into the editor
		registerEditPartViewer(viewer);

		// - Initialize the viewer with input
		try {
			viewer.setEditPartFactory(new HarpoonPartFactory());
			viewer.setContents(getSceneryModel());
		} catch (final Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	// [03]

	/**
	 * Creates the GraphicalViewer on the specified <code>Composite</code>.
	 * 
	 * @param parent
	 *          the parent composite
	 */
	private void createGraphicalViewer(Composite parent) {
		viewer = new ScrollingGraphicalViewer();
		viewer.createControl(parent);

		// configure the viewer
		viewer.getControl().setBackground(parent.getBackground());
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		// hook the viewer into the editor
		registerEditPartViewer(viewer);

		// // configure the viewer with drag and drop
		// configureEditPartViewer(viewer);

		// initialize the viewer with input
		viewer.setEditPartFactory(new HarpoonPartFactory());
		viewer.setContents(getSceneryModel());
	}

	/**
	 * Hooks a <code>EditPartViewer</code> to the rest of the Editor.
	 * 
	 * <p>
	 * By default, the viewer is added to the SelectionSynchronizer, which can be used to keep 2 or more EditPartViewers
	 * in sync. The viewer is also registered as the ISelectionProvider for the Editor's PartSite.
	 * 
	 * @param viewer
	 *          the viewer to hook into the editor
	 */
	protected void registerEditPartViewer(EditPartViewer viewer) {
		// register viewer to edit domain
		getEditDomain().addViewer(viewer);

		// // the multi page workflow editor keeps track of synchronizing
		// final SceneryEditor editor = (SceneryEditor) getWorkflowEditor();
		// editor.getSelectionSynchronizer().addViewer(viewer);

		// add viewer as selection provider
		getSite().setSelectionProvider(viewer);
	}

	// - E D I T P A R T - S E C T I O N
	@Override
	public final void doSave(IProgressMonitor monitor) {
		// our policy: delegate saving to the parent
		getSceneryEditor().doSave(monitor);
	}

	@Override
	public final void doSaveAs() {
		// our policy: delegate saving to the parent
		getSceneryEditor().doSaveAs();
	}

	public void gotoMarker(IMarker marker) {
	}

	@Override
	public final boolean isDirty() {
		// our policy: delegate saving to the parent
		// note: this method shouldn't get called anyway
		return getSceneryEditor().isDirty();
	}

	@Override
	public final boolean isSaveAsAllowed() {
		// our policy: delegate saving to the parent
		return getSceneryEditor().isSaveAsAllowed();
	}

	@Override
	public void setFocus() {
		getGraphicalViewer().getControl().setFocus();
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// /**
// * Creates the PaletteRoot and adds all palette elements. Use this factory method to create a new palette for your
// * graphical editor.
// *
// * @return a new PaletteRoot
// */
// private static PaletteRoot createPalette() {
// final PaletteRoot palette = new PaletteRoot();
// palette.add(createToolsGroup(palette));
// palette.add(createShapesDrawer());
// return palette;
// }

// /** Create the "Tools" group. */
// private static PaletteContainer createToolsGroup(PaletteRoot palette) {
// final PaletteGroup toolGroup = new PaletteGroup("Tools");
//
// // - Add a selection tool to the group
// selectionTool = new SelectionToolEntry();
// toolGroup.add(selectionTool);
// // - Add a marquee tool to the group
// marqueeTool = new MarqueeToolEntry();
// toolGroup.add(marqueeTool);
//
// return toolGroup;
// }

// /** Create the "Shapes" drawer. */
// private static PaletteContainer createShapesDrawer() {
// final PaletteDrawer componentsDrawer = new PaletteDrawer("Shapes");
//
// referencePointCreationTool = new CombinedTemplateCreationEntry("Reference Point",
// "Create the reference point for the construction of a movement path", ReferencePoint.class,
// new SimpleFactory(ReferencePoint.class), HarpoonRegistry.getImageDescriptor("icons/ellipse16.gif"),
// HarpoonRegistry.getImageDescriptor("icons/ellipse24.gif"));
// componentsDrawer.add(referencePointCreationTool);
//
// return componentsDrawer;
// }

// [02]
// public PaletteRoot getPaletteRoot() {
// if (paletteModel == null) paletteModel = SceneryPage.createPalette();
// return paletteModel;
// }
//
// public void setPaletteRoot() {
// paletteModel = SceneryPage.createPalette();
// domain.setPaletteRoot(paletteModel);
// }

// /**
// * Returns the palette viewer.
// *
// * @return the palette viewer
// */
// protected PaletteViewer getPaletteViewer() {
// return paletteViewer;
// }

// [03]
// /**
// * Creates the createPaletteViewer on the specified <code>Composite</code>.
// *
// * @param parent
// * the parent composite
// */
// protected void createPaletteViewer(Composite parent) {
// // create graphical viewer
// paletteViewer = new PaletteViewer();
// paletteViewer.createControl(parent);
//
// // configure the viewer
// paletteViewer.getControl().setBackground(parent.getBackground());
//
// // hook the viewer into the EditDomain (only one palette per EditDomain)
// getEditDomain().setPaletteViewer(paletteViewer);
//
// // important: the palette is initialized via EditDomain
// // fancy palette: paletteViewer.setEditPartFactory(new CustomizedPaletteEditPartFactory());
// // getEditDomain().setPaletteRoot(getPaletteRoot());
//
// // enable the palette as source for drag operations
// paletteViewer.addDragSourceListener(new TemplateTransferDragSourceListener(paletteViewer));
// }
