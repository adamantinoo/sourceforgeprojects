//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: IPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/IPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface IPropertyPage {
	/**
	 * Activates or deactivates the connection to the property listener. Any change on a target property will fire a call
	 * on the <code>propertyChange</code> method.
	 */
	public void activate();

	/** Deactivates the property listening to the model changes and clears the activity flag. */
	public void deactivate();

	/**
	 * Add all the common elements that are visible for any unit. This depends on the information available on the model
	 * reference at this interface level. This method will build up the visible contents of this page instance.
	 */
	public void build();

	/**
	 * Called when the page is no longer accessible and the SWT elements have been disposed from the viewer.
	 */
	public void dispose();

}

// - UNUSED CODE ............................................................................................
