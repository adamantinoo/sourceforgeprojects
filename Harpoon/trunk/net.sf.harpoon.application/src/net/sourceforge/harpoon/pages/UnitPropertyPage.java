//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/UnitPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.5  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.4.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.4  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.3  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.2  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.1  2007-09-21 12:11:30  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.model.PropertyModel;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitPropertyPage implements PropertyChangeListener, IPropertyPage {
	// - F I E L D - S E C T I O N ............................................................................
	/** Reference to the model. But limited to the interface level of a Unit. */
	private Unit								unitModel;
	/** Group element that will contain all the unit properties to be presented on selection. */
	private Group								page;
	/** Text element where to display the location of the model element. */
	private Text								location;
	/**
	 * Reference to the link Control where all page composition will be linked. This element is located in the
	 * <code>SelectionView</code> control contents and it is received by a sentence.
	 */
	private transient Composite	container;
	/**
	 * A value of <code>true</code> in this field signals that this instance is registered as a listener to the
	 * <code>unitModel</code> property change notification mechanism.
	 */
	private boolean							active	= false;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The <code>top</code>
	 * container allows this method to hang the new interface elements from the <code>SelectionView</code> at runtime.
	 * Other fields may be <code>null</code> and in those situation the code will generate a informational message.
	 */
	public UnitPropertyPage(final Composite top) {
		container = top;
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Get the associated model element. At this class this model will be subclassed to the <code>Unit</code> interface.
	 * If the model is null then we should throw an exception to signal some internal code error.
	 */
	private Unit getModel() {
		Assert.isNotNull(unitModel, "The page unit model instance is not set up but it is being accessed.");
		return unitModel;
	}

	/**
	 * Store a reference to the model inside the page. Check also if a value existed previously to call the notification
	 * registration mechanism so this instance will receive any property modification to the model reference.
	 */
	public void setModel(final Unit model) {
		if (null != unitModel) deactivate();
		unitModel = model;
		activate();
	}

	// public Composite getContainer() {
	// return container;
	// }

	/** Changes the parent container where to hang the new SQL interface elements. */
	public void setContainer(final Composite parent) {
		Assert.isNotNull(parent, "The page view contained that is received is null.");
		container = parent;
	}

	/**
	 * Return the constructed page with the interface SWL components. If the <code>build()</code> has not been called
	 * then if will return a fake page with a single Label.
	 */
	public Group getPropertyPage() {
		if (null == page) {
			page = new Group(container, SWT.NONE);
			page.setText("Building ERROR");
		}
		return page;
	}

	// - P U B L I C - S E C T I O N

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * Intercept changes to the model properties. Changes supported by this class are only in the next list of properties:
	 * <ul>
	 * <li><b>Name</b> - name of the unit. When units are detected or identified they may change their representation
	 * name.</li>
	 * <li><b>Location</b> - the location of the unit may change is the unit moves or if the user deletes or changes the
	 * item.</li>
	 * </ul>
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Unit.NAME.equals(prop)) {
			try {
				page.setText(getModel().getName());
			} catch (final AssertionFailedException afe) {
				// - Just for the name change it to some meaningful value.
				page.setText("Building ERROR");
			}
		}
		if (Unit.LOCATION_PROP.equals(prop)) {
			try {
				location.setText(getModel().getLocation().toDisplay());
			} catch (final AssertionFailedException afe) {
				// - Just for the name change it to some meaningful value.
				location.setText("Model information not available.");
			}
		}
	}

	// - I P R O P E R T Y P A G E - I N T E R F A C E
	/**
	 * Activates or deactivates the connection to the property listener. Any change on a target property will fire a call
	 * on the <code>propertyChange</code> method.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#activate()
	 */
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		((PropertyModel) getModel()).addPropertyChangeListener(this);
		active = true;
	}

	/**
	 * Deactivates the property listening to the model changes and clears the activity flag.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#deactivate()
	 */
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		((PropertyModel) getModel()).removePropertyChangeListener(this);
		active = false;
	}

	/**
	 * Add all the common elements that are visible for any unit. This depends on the information available on the model
	 * reference at this interface level. This method will build up the visible contents of this page instance.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#build()
	 */
	public void build() {
		// - Create the common contents of the page structure.
		final RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;

		// - Create the group and set the name
		page = new Group(container, SWT.NONE);
		page.setLayout(rowLayout);
		try {
			page.setText(getModel().getName());
		} catch (final AssertionFailedException afe) {
			// - Just for the name change it to some meaningful value.
			page.setText("Building ERROR");
		}
		// - Add location information
		location = new Text(page, SWT.BORDER);
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		try {
			location.setText(getModel().getLocation().toDisplay());
		} catch (final AssertionFailedException afe) {
			// - Just for the name change it to some meaningful value.
			location.setText("Model information not available.");
		}
	}

	/**
	 * Called when the page is no longer accessible and the SWT elements have been disposed from the viewer.
	 * 
	 * @see net.sourceforge.harpoon.pages.IPropertyPage#dispose()
	 */
	public void dispose() {
		deactivate();
		unitModel = null;
		container = null;
		location.dispose();
		page.dispose();
	}

	// - P R O T E C T E D - S E C T I O N
	/** Accessor to the <code>active</code> notification listener flag. */
	private boolean isActive() {
		return active;
	}

}

// - UNUSED CODE ............................................................................................
