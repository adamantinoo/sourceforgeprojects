//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ReferenceFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/ReferenceFigure.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class ReferenceFigure extends UnitFigure {
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	protected int	width	= 8;

	// - C O N S T R U C T O R S
	public ReferenceFigure() {
		this.setSize(this.getPreferredSize());
	}

	// - G E T T E R S / S E T T E R S
	public int getWidth() {
		return width;
	}

	public void setWidth(final int width) {
		this.width = width;
		this.setSize(this.getPreferredSize());
	}

	// - P U B L I C - S E C T I O N
	public void init() {
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public Point getHotSpot() {
		return new Point(width / 2, width / 2);
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(width, width);
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		final Point loc = getLocation();
		graphics.setForegroundColor(ColorConstants.black);
		graphics.drawOval(loc.x, loc.y, width - 1, width - 1);
	}

	/**
	 * Makes the additional figure data (the label contents) visible or invisible. This affects the enemy units that will
	 * not display this information until <code>IDENTIFIED</code>.
	 */
	@Override
	public void setDataVisibility(final boolean visibility) {
		int dummy = 0;
		dummy++;
	}
}
// - UNUSED CODE ............................................................................................
