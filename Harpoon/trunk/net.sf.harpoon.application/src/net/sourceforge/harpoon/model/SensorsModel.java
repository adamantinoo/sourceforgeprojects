//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SensorsModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/SensorsModel.java,v $
//  LAST UPDATE:    $Date: 2007-11-07 16:28:44 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.5.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.5  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.4  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.3  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.model;

//- IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.WarUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - P R I V A T E S E C T I O N
public class SensorsModel extends PropertyModel {
	private static final long	serialVersionUID	= -1402055842598003340L;
	private static Logger			logger						= Logger.getLogger("net.sourceforge");

	// - F I E L D - S E C T I O N .............................................................................
	/** Configurable flags to indicate which sensors are available for the referent Unit. */
	private boolean						availableRadar		= false;
	private boolean						availableSonar		= false;
	private boolean						availableECM			= true;
	/** Sensor state for all possible sensors. */
	private boolean						radar							= false;
	private boolean						sonar							= false;
	private boolean						ECM								= false;
	/**
	 * Reference to the parent WarUnit where there are the calculation functions for the sensors range. Sensor
	 * range information depends on many parameters only known to the model part.
	 */
	private WarUnit						referent;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	public SensorsModel(final WarUnit warUnit) {
		referent = warUnit;
	}

	// - G E T T E R S / S E T T E R S
	public boolean getRadarState() {
		return radar;
	}

	public void setRadarState(final boolean newState) {
		final boolean oldState = radar;
		radar = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.SENSORCHANGE_PROP, oldState, newState);
	}

	public boolean getSonarState() {
		return sonar;
	}

	public void setSonarState(final boolean newState) {
		final boolean oldState = sonar;
		sonar = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.SENSORCHANGE_PROP, oldState, newState);
	}

	public boolean getRadioState() {
		return ECM;
	}

	public void setRadioState(final boolean newState) {
		final boolean oldState = ECM;
		ECM = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.SENSORCHANGE_PROP, oldState, newState);
	}

	public Unit getReferent() {
		if (null == referent) throw new NullPointerException("The reference parent unit is not defined.");
		return referent;
	}

	public void setReferent(final WarUnit warUnit) {
		referent = warUnit;
	}

	public int getSensorRange(final String sensorType) {
		if (HarpoonConstants.RADAR_TYPE.equals(sensorType)) return new Integer(referent
				.getExtendedData(ExtendedData.XDT_RADARRANGE)).intValue();
		if (HarpoonConstants.SONAR_TYPE.equals(sensorType)) return new Integer(referent
				.getExtendedData(ExtendedData.XDT_SONARRANGE)).intValue();
		if (HarpoonConstants.RADIO_TYPE.equals(sensorType)) return new Integer(referent
				.getExtendedData(ExtendedData.XDT_RADIORANGE)).intValue();
		return -1;
	}

	// public String getUnitType() {
	// return HarpoonConstants.UNIT_SENSOR;
	// }

	// /** Return the range for this sensor. */
	// public int getRange() {
	// // TODO Auto-generated method stub
	// return 20;
	// }
	//
	// public void setRange(int range) {
	// // TODO Auto-generated method stub
	// this.range = range;
	// }

	// - P U B L I C S E C T I O N
	/**
	 * Return the controls to edit the sensor status and information for this particular Unit. This model is
	 * configurable and can define any combination of sensors suitable for any type of WarUnit.
	 */
	public void addSensorsPage(final Group page) {
		final RowLayout row = new RowLayout();
		row.type = SWT.VERTICAL;
		row.marginLeft = 2;
		row.marginRight = 2;
		row.marginBottom = 1;
		row.marginTop = 1;
		row.wrap = false;
		final Group sensorsGroup = new Group(page, SWT.NONE);
		sensorsGroup.setLayout(row);
		sensorsGroup.setText("Sensors:");

		// - Check the availability for each control. Add high configuration options
		if (availableRadar) {
			final Button radar = new Button(sensorsGroup, SWT.CHECK);
			radar.setText("Radar");
			radar.setSelection(this.radar);
			radar.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(final SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(final SelectionEvent e) {
					logger.info("Radar selected to state " + radar.getSelection() + " for Unit:" + referent.toString());
					setRadarState(radar.getSelection());
				}
			});
		}
		if (availableSonar) {
			final Button sonar = new Button(sensorsGroup, SWT.CHECK);
			sonar.setText("Sonar");
			sonar.setSelection(this.sonar);
			sonar.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(final SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(final SelectionEvent e) {
					logger.info("Sonar selected to state " + sonar.getSelection() + " for Unit:" + referent.toString());
					setSonarState(sonar.getSelection());
				}
			});
		}
		if (availableECM) {
			final Button radio = new Button(sensorsGroup, SWT.CHECK);
			radio.setText("EEM");
			radio.setSelection(ECM);
			radio.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(final SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(final SelectionEvent e) {
					logger.info("Radio receiver selected to state " + radio.getSelection() + " for Unit:"
							+ referent.toString());
					setRadioState(radio.getSelection());
				}
			});
		}
	}

	public void setAvailable(final boolean radar, final boolean sonar, final boolean radio) {
		availableRadar = radar;
		availableSonar = sonar;
		availableECM = radio;
	}

	public void setAvailable(final String sensorType) {
		if (HarpoonConstants.RADAR_TYPE.equals(sensorType)) availableRadar = true;
		if (HarpoonConstants.SONAR_TYPE.equals(sensorType)) availableSonar = true;
		if (HarpoonConstants.ECM_TYPE.equals(sensorType)) availableECM = true;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[SensorsModel:");
		if (availableRadar) {
			buffer.append("[Rad:AV-");
			if (radar)
				buffer.append("ON]");
			else buffer.append("OFF]");
		} else buffer.append("[Rad:NAV]");
		buffer.append("-");
		if (availableECM) {
			buffer.append("[ECM:AV-");
			if (ECM)
				buffer.append("ON]");
			else buffer.append("OFF]");
		} else buffer.append("[ECM:NAV]");
		buffer.append("-");
		if (availableSonar) {
			buffer.append("[Son:AV-");
			if (sonar)
				buffer.append("ON]");
			else buffer.append("OFF]");
		} else buffer.append("[Son:NAV]").append("]");
		//		buffer.append("-");
		//		buffer.append(super.toString());
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
