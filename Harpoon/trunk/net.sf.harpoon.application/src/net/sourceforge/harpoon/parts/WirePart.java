//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WirePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/WirePart.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-11-23 11:33:38  ldiego
//    - TASK Detection method is changed to a detection control unit by unit.
//    - DEFECT After saving a game the units return to the original position.
//    - DEFECT Changes in the movement path points is not reflected.
//    - DEFECT Enemies have not to show path.
//    - DEFECT Friend unit traces are not shown.
//
//    revision 1.2
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    revision 1.1.2.6
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    revision 1.1.2.5
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    revision 1.1.2.4
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    revision 1.1.2.3
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    revision 1.1.2.2
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    revision 1.1.2.1
//    - Version with all code enabled but that still does not execute properly. The moving
//      of the bending point still generates an exception.
package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RelativeBendpoint;
import org.eclipse.draw2d.RoutingAnimator;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.editparts.AbstractConnectionEditPart;

import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.model.WireBendpoint;
import net.sourceforge.harpoon.policy.WireBendpointEditPolicy;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Implements a Connection Editpart to represent a Wire like connection. This connection may connect a <code>Unit</code>
 * to a <code>ReferencePoint</code> or two <code>ReferencePoint</code>s or two <code>Unit</code>s.
 */
public class WirePart extends AbstractConnectionEditPart implements PropertyChangeListener {
	// - P U B L I C - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// AccessibleEditPart acc;

	private RootMapPart	rootPart;

	// public static final Color alive = new Color ( Display . getDefault ( ) ;
	// 0 ;
	// 74 ;
	// 168 ) ;
	// public static final Color dead = new Color ( Display . getDefault ( ) ;
	// 0 ;
	// 0 ;
	// 0 ) ;

	// - C O N S T R U C T O R S
	/**
	 * Creates a new instance and associates the <code>Wire</code> object that is the model from where to get the
	 * information.
	 */
	public WirePart(final Object model) {
		setModel(model);
	}

	// - I N T E R F A C E - P R O P E R T Y C H A N G E L I S T E N E R
	/**
	 * Listens to changes in properties of the Wire (like the contents being carried), and reflects is in the visuals.
	 * 
	 * @param event
	 *          Event notifying the change.
	 */
	public void propertyChange(final PropertyChangeEvent event) {
		final String property = event.getPropertyName();
		if (Connection.PROPERTY_CONNECTION_ROUTER.equals(property)) {
			refreshBendpoints();
			// refreshBendpointEditPolicy();
		}
		if (Wire.PARENT_SELECTED_PROP.equals(property)) refreshVisuals();
		if (Wire.SOURCE_PROP.equals(property)) {
			refreshBendpoints();
			// refreshBendpointEditPolicy();
		}
		if (Wire.TARGET_PROP.equals(property)) {
			refreshBendpoints();
			// refreshBendpointEditPolicy();
		}
		if (Wire.BENDPOINT_PROP.equals(property)) refreshBendpoints();
	}

	// - G E T T E R S / S E T T E R S
	public RootMapPart getRootPart() {
		return rootPart;
	}

	public void setRootPart(final RootMapPart rootPart) {
		// TODO Auto-generated method stub
		this.rootPart = rootPart;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public void activate() {
		super.activate();
		getCastedModel().addPropertyChangeListener(this);
	}

	@Override
	public void activateFigure() {
		super.activateFigure();
		// - Once the figure has been added to the ConnectionLayer, start listening for its router to change.
		getFigure().addPropertyChangeListener(Connection.PROPERTY_CONNECTION_ROUTER, this);
	}

	/**
	 * Returns a newly created <code>PolylineConnection</code> to represent the connection.
	 * 
	 * @return The created <code>PolylineConnection</code>.
	 */
	@Override
	protected IFigure createFigure() {
		final PolylineConnection conn = new PolylineConnection();
		conn.setForegroundColor(ColorConstants.gray);
		conn.setTargetDecoration(new PolygonDecoration()); // arrow at target endpoint
		conn.addRoutingListener(RoutingAnimator.getDefault());
		return conn;
	}

	@Override
	public void deactivate() {
		getCastedModel().removePropertyChangeListener(this);
		super.deactivate();
	}

	@Override
	public void deactivateFigure() {
		getFigure().removePropertyChangeListener(Connection.PROPERTY_CONNECTION_ROUTER, this);
		super.deactivateFigure();
	}

	// [01]
	/**
	 * Refreshes the visual aspects of this, based upon the model (Wire).
	 * 
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final PolylineConnection fig = (PolylineConnection) getFigure();
		final Wire model = getCastedModel();
		if (EditPart.SELECTED_PRIMARY == model.getParentSelected()) {
			fig.setVisible(true);
			refreshBendpoints();
		} else
			fig.setVisible(false);
		super.refreshVisuals();
	}

	// - P R O T E C T E D - S E C T I O N
	/**
	 * Adds extra EditPolicies as required.
	 */
	@Override
	protected void createEditPolicies() {
		// installEditPolicy(EditPolicy.CONNECTION_ENDPOINTS_ROLE, new WireEndpointEditPolicy());
		// - Note that the Connection is already added to the diagram and knows its Router.
		// if (getConnectionFigure().getConnectionRouter() instanceof ManhattanConnectionRouter)
		// installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, null);
		// else installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new WireBendpointEditPolicy());
		installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new WireBendpointEditPolicy());
		// DEBUG Check that this policy is not required. related to the deletion of the wire.
		// installEditPolicy(EditPolicy.CONNECTION_ROLE, new WireEditPolicy());
	}

	/**
	 * Returns the model of this represented as a Wire.
	 * 
	 * @return Model of this as <code>Wire</code>
	 */
	protected Wire getCastedModel() {
		return (Wire) getModel();
	}

	/**
	 * Updates the bendpoints, based on the model.
	 */
	protected void refreshBendpoints() {
		if (getConnectionFigure().getConnectionRouter() instanceof ManhattanConnectionRouter) return;
		final Vector<Bendpoint> modelConstraint = getCastedModel().getBendpoints();
		final ArrayList figureConstraint = new ArrayList();
		for (int i = 0; i < modelConstraint.size(); i++) {
			final WireBendpoint wbp = (WireBendpoint) modelConstraint.get(i);
			final RelativeBendpoint rbp = new RelativeBendpoint(getConnectionFigure());
			rbp.setRelativeDimensions(wbp.getFirstRelativeDimension(), wbp.getSecondRelativeDimension());
			rbp.setWeight((i + 1) / ((float) modelConstraint.size() + 1));
			figureConstraint.add(rbp);
		}
		getConnectionFigure().setRoutingConstraint(figureConstraint);
	}
	// protected void refreshBendpointEditPolicy() {
	// if (getConnectionFigure().getConnectionRouter() instanceof ManhattanConnectionRouter)
	// installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, null);
	// else installEditPolicy(EditPolicy.CONNECTION_BENDPOINTS_ROLE, new WireBendpointEditPolicy());
	// }

}

// - UNUSED CODE ............................................................................................
// [01]
// @Override
// public AccessibleEditPart getAccessibleEditPart() {
// if (acc == null)
// acc = new AccessibleGraphicalEditPart() {
// @Override
// public void getName(final AccessibleEvent e) {
// e.result = "Wire";
// }
// };
// return acc;
// }
