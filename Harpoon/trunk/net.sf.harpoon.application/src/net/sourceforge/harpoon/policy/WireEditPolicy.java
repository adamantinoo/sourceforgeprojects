//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WireEditPolicy.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/policy/WireEditPolicy.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:38 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
/*************************************************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others. All rights reserved. This program and the accompanying
 * materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 ************************************************************************************************************/
//  LOG:
//    $Log: not supported by cvs2svn $
//    revision 1.2
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    revision 1.1.2.2
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    revision 1.1.2.1
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
package net.sourceforge.harpoon.policy;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.ConnectionEditPolicy;
import org.eclipse.gef.requests.GroupRequest;

import net.sf.harpoon.model.units.Wire;

// - CLASS IMPLEMENTATION ...................................................................................
public class WireEditPolicy extends ConnectionEditPolicy {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	protected Command getDeleteCommand(final GroupRequest request) {
		final ConnectionCommand c = new ConnectionCommand();
		c.setWire((Wire) getHost().getModel());
		return c;
	}

}
// - UNUSED CODE ............................................................................................
