//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonColorConstants.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonColorConstants.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:48 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-10-01 14:43:24  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.4  2007-09-27 16:43:56  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.1  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sf.harpoon.app;

//- IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

//- CLASS IMPLEMENTATION ...................................................................................
//- C O N S T R U C T O R S
//- P U B L I C S E C T I O N
//- G E T T E R S / S E T T E R S
//- P R I V A T E S E C T I O N
/**
 * Contains a collection of the palette of colors that can be used in the Harpoon game.
 */
public interface HarpoonColorConstants extends HarpoonConstants{

	class HarpoonColorFactory {
		private static Color getColor(final int which) {
			Display display = Display.getCurrent();
			if (display != null) return display.getSystemColor(which);
			display = Display.getDefault();
			final Color result[] = new Color[1];
			display.syncExec(new Runnable() {
				public void run() {
					synchronized (result) {
						result[0] = Display.getCurrent().getSystemColor(which);
					}
				}
			});
			synchronized (result) {
				return result[0];
			}
		}
	}

	Color	UNKNOWN_SIDE	= new Color(Display.getCurrent(), 0, 0, 127);
	Color	ORANGE	= new Color(Display.getCurrent(), 200, 120, 0);
	Color	HANDLE_COLOR	= ColorConstants.black;
	Color	SELECTION_COLOR	= ColorConstants.black;
	Color	ECM_COLOR	= new Color(Display.getCurrent(), 180, 50, 50);

	// /**
	// * @see SWT#COLOR_WIDGET_HIGHLIGHT_SHADOW
	// */
	// Color buttonLightest = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW);
	// /**
	// * @see SWT#COLOR_WIDGET_BACKGROUND
	// */
	// Color button = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_BACKGROUND);
	// /**
	// * @see SWT#COLOR_WIDGET_NORMAL_SHADOW
	// */
	// Color buttonDarker = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_NORMAL_SHADOW);
	// /**
	// * @see SWT#COLOR_WIDGET_DARK_SHADOW
	// */
	// Color buttonDarkest = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_DARK_SHADOW);
	//
	// /**
	// * @see SWT#COLOR_LIST_BACKGROUND
	// */
	// Color listBackground = HarpoonColorFactory.getColor(SWT.COLOR_LIST_BACKGROUND);
	// /**
	// * @see SWT#COLOR_LIST_FOREGROUND
	// */
	// Color listForeground = HarpoonColorFactory.getColor(SWT.COLOR_LIST_FOREGROUND);
	//
	// /**
	// * @see SWT#COLOR_WIDGET_BACKGROUND
	// */
	// Color menuBackground = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_BACKGROUND);
	// /**
	// * @see SWT#COLOR_WIDGET_FOREGROUND
	// */
	// Color menuForeground = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_FOREGROUND);
	// /**
	// * @see SWT#COLOR_LIST_SELECTION
	// */
	// Color menuBackgroundSelected = HarpoonColorFactory.getColor(SWT.COLOR_LIST_SELECTION);
	// /**
	// * @see SWT#COLOR_LIST_SELECTION_TEXT
	// */
	// Color menuForegroundSelected = HarpoonColorFactory.getColor(SWT.COLOR_LIST_SELECTION_TEXT);
	//
	// /**
	// * @see SWT#COLOR_TITLE_BACKGROUND
	// */
	// Color titleBackground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_BACKGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_BACKGROUND_GRADIENT
	// */
	// Color titleGradient = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT);
	// /**
	// * @see SWT#COLOR_TITLE_FOREGROUND
	// */
	// Color titleForeground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveForeground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveBackground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveGradient = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	//
	// /**
	// * @see SWT#COLOR_INFO_FOREGROUND
	// */
	// Color tooltipForeground = HarpoonColorFactory.getColor(SWT.COLOR_INFO_FOREGROUND);
	// /**
	// * @see SWT#COLOR_INFO_BACKGROUND
	// */
	// Color tooltipBackground = HarpoonColorFactory.getColor(SWT.COLOR_INFO_BACKGROUND);
	//
	// /*
	// * Misc. colors
	// */
	// /** One of the pre-defined colors */
	// Color white = new Color(null, 255, 255, 255);
	// /** One of the pre-defined colors */
	// Color lightGray = new Color(null, 192, 192, 192);
	// /** One of the pre-defined colors */
	// Color gray = new Color(null, 128, 128, 128);
	// /** One of the pre-defined colors */
	// Color darkGray = new Color(null, 64, 64, 64);
	// /** One of the pre-defined colors */
	// Color black = new Color(null, 0, 0, 0);
	// /** One of the pre-defined colors */
	// Color red = new Color(null, 255, 0, 0);
	// /** One of the pre-defined colors */
	// Color orange = new Color(null, 255, 196, 0);
	// /** One of the pre-defined colors */
	// Color yellow = new Color(null, 255, 255, 0);
	// /** One of the pre-defined colors */
	// Color green = new Color(null, 0, 255, 0);
	// /** One of the pre-defined colors */
	// Color lightGreen = new Color(null, 96, 255, 96);
	// /** One of the pre-defined colors */
	// Color darkGreen = new Color(null, 0, 127, 0);
	// /** One of the pre-defined colors */
	// Color cyan = new Color(null, 0, 255, 255);
	// /** One of the pre-defined colors */
	// Color lightBlue = new Color(null, 127, 127, 255);
	// /** One of the pre-defined colors */
	// Color blue = new Color(null, 0, 0, 255);
	// /** One of the pre-defined colors */
	// Color darkBlue = new Color(null, 0, 0, 127);
}

// - UNUSED CODE ............................................................................................
