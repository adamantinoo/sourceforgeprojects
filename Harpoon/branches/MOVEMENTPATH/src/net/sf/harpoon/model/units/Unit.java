//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: Unit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/Unit.java,v $
//  LAST UPDATE:    $Date: 2007-11-16 10:50:18 $
//  RELEASE:        $Revision: 1.18 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.17  2007-11-12 11:23:48  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.16  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.15  2007-11-02 09:34:48  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.14.2.4  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.14.2.3  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.14.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.14.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.14  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.13  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.12  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.11  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.10  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.9  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.8  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.7  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.6  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.5  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.4  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.3  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//
//    Revision 1.2  2007-08-21 13:45:11  ldiego
//    - Initial implementation in a working model that has to be adapted to a more classical model.
//
//    Revision 1.1  2007-08-20 13:05:41  ldiego
//    - This first release implements the base structure for storing the initial
//      attributes for a game unit.
//

package net.sf.harpoon.model.units;

// - IMPORT SECTION .........................................................................................
import java.text.NumberFormat;
import java.util.logging.Logger;

import net.sf.harpoon.app.HarpoonConstants;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.DetailsModel;
import net.sourceforge.harpoon.model.DetectionModel;
import net.sourceforge.harpoon.model.PropertyModel;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Has the base and common behavior for all game units not depending if they are air borne or ships. This
 * class is the base building block for the game user model that is manipulated by the game controller.<br>
 * Is <code>Serializable</code> to allow for game saving and model storage and also has to implement the key
 * methods for the <code>IPropertySource</code> interface to allow for controller registration.<br>
 * <br>
 * The common characteristics for all model elements on the game are defined next:
 * <dl>
 * <dt><b>Side</b></dt>
 * <dt><b>VisibilityState</b></dt>
 * <dt><b>Name</b></dt>
 * <dd>Not all units have a real name like USS Enterprise or Enola Gay. But all them have to have a user
 * reference to help the right identification. Names for some units may change because the creation and
 * destruction of fleets or task forces may require to do so.<br>
 * Game contacts also have to be named, so a single <code>Unit</code> may have more than one name at the
 * same time or during the game lifetime.<br>
 * So the <code>Name</code> property may have different senses depending on the environment but mainly
 * serves to the purpose of identification. If the game database or user does not give a name to an unit it
 * will generate one depending on name type.</dd>
 * <dt><b>Latitude</b></dt>
 * <dt><b>Longitude</b></dt>
 * </dl>
 * 
 * <b>References:</b><br>
 * http://www.eclipse.org/articles/Article-GEF-diagram-editor/shape.html<br>
 * http://java.sun.com/developer/technicalArticles/Programming/serialization/
 */
public abstract class Unit extends PropertyModel {
	private static final long		serialVersionUID				= 2209528353626060016L;
	private static Logger				logger									= Logger.getLogger("net");
	/** Counter value for the identification ID in unnamed units. */
	private static int					IDGenerator							= 0;

	// - U N I T S I D E S
	//TODO Move this identifiers to the HarpoonContants class
	public static final String	UNKNOWN_SIDE						= "UNKNOWN";
	public static final String	FRIEND_SIDE							= "FRIEND";
	public static final String	FOE											= "FOE";
	public static final String	NEUTRAL									= "NEUTRAL";
	public static final String	PATH										= "PATH";
	// - V I S I B I L I T Y - S T A T E S
	//TODO Move this identifiers to the HarpoonContants class
	public static final String	NOT_VISIBLE							= "Unit.NOT_VISIBLE";
	public static final String	CONTACT_VISIBLE					= "Unit.CONTACT_VISIBLE";
	public static final String	IDENTIFICATION_VISIBLE	= "Unit.IDENTIFICATION_VISIBLE";
	public static final String	FULL_VISIBLE						= "Unit.FULL_VISIBLE";
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	NAME										= "Unit.NAME";											//$NON-NLS-1$
	public static final String	LATITUDE								= "Unit.LATITUDE";									//$NON-NLS-1$
	public static final String	LONGITUDE								= "Unit.LONGITUDE";								//$NON-NLS-1$
	public static final String	LOCATION_PROP						= "Unit.LOCATION_PROP";						//$NON-NLS-1$
	public static final String	SIDE										= "Unit.SIDE";											//$NON-NLS-1$
	public static final String	DETECTIONCHANGE_PROP		= "Unit.DETECTIONCHANGE_PROP";

	// - M O D E L F I E L D S
	/** Unit side. This allows to identify other units not known to the user or detected during the game. */
	protected String						side										= UNKNOWN_SIDE;
	protected String						visibilityState					= NOT_VISIBLE;
	/**
	 * Contains an identifier to the Unit. This can be a user defined name or a database supplied name or the
	 * Unit ID when this attribute is not displayed on some war units.
	 */
	protected String						name										= null;
	protected DetailsModel			unitDetails							= null;
	/** Unit latitude in the global coordinate DMS system. */
	protected DMSCoordinate			latitude								= new DMSCoordinate(0, 0, 0, 'N');
	/** Unit longitude in the global coordinate DMS system. */
	protected DMSCoordinate			longitude								= new DMSCoordinate(0, 0, 0, 'W');
	//	/** UNUSED. This will contain the list of model children elements when the unit has to implement this. */
	// private final Vector<Unit> children = new Vector<Unit>();
	// - M O D E L - H E L P E R S
	/** Stores the detection or detection phase status of this unit in rerefence to other enemy units. */
	protected DetectionModel		detectState							= new DetectionModel();

	// - C O N S T R U C T O R S
	protected static String nextID(final String prefix) {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		nf.setMinimumIntegerDigits(4);
		nf.setMaximumFractionDigits(0);
		return prefix + nf.format(IDGenerator++);
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Returns the visible name for this Unit. This name depends on multiple helpers and factors. For FRIEND
	 * units the name must be the real name. For ReferencePoints the name is a system generated sequential and
	 * for OTHER units it is a contact generated name.
	 * 
	 * @return The visible string that represents this unit name.
	 */
	public String getName() {
		// - Check if this unit is of class FRIEND.
		if (FRIEND_SIDE.equals(getSide())) {
			// - Check for the database unit helper. If not defined then resort to the name generator.
			if (null != unitDetails)
				return unitDetails.getName();
		}
		// else name = Unit.nextID("FID");
		if (null == name)
			name = Unit.nextID("UID");
		return name;
	}

	public void setName(final String newName) {
		final String oldName = name;
		if (FRIEND_SIDE.equals(getSide())) {
			if (null != unitDetails)
				unitDetails.setName(newName);
			else name = newName;
			// - Fire a notification to all listeners to update the presentation layer.
			firePropertyChange(Unit.NAME, oldName, newName);
			return;
		}
		if (UNKNOWN_SIDE.equals(getSide())) {
			if (null != unitDetails)
				unitDetails.setName(newName);
			else name = newName;
			// - Fire a notification to all listeners to update the presentation layer.
			firePropertyChange(Unit.NAME, oldName, newName);
			return;
		}
		name = newName;
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.NAME, oldName, newName);
	}

	/**
	 * Initialize the Unit detailed data. Current implementation just supports the name attribute for FRIEND
	 * type units.
	 * 
	 * @param locator
	 *          the real name of this unit to the UI interface. Use only on FRIEND units.
	 */
	public void loadDetails(final String locator) {
		unitDetails = new DetailsModel();
		unitDetails.setName(locator);
	}

	/**
	 * Return the latitude coordinate for this unit. The coordinates are expressed in an instance of a Degree,
	 * Minute Second global coordinate.
	 * 
	 * @return The DMS coordinate for the latitude component of the unit location.
	 */
	public DMSCoordinate getDMSLatitude() {
		return latitude;
	}

	public void setLatitude(final DMSCoordinate lat) {
		final DMSCoordinate oldLatitude = latitude;
		latitude = lat;
		// - Fire a notification to all listeners to update the presentation layer.
		if (!oldLatitude.identical(lat))
			firePropertyChange(Unit.LATITUDE, oldLatitude, lat);
	}

	/**
	 * Return the longitude coordinate for this unit. The coordinates are expressed in an instance of a Degree,
	 * Minute Second global coordinate.
	 * 
	 * @return The DMS coordinate for the longitude component of the unit location.
	 */
	public DMSCoordinate getDMSLongitude() {
		return longitude;
	}

	public void setLongitude(final DMSCoordinate lon) {
		final DMSCoordinate oldLongitude = latitude;
		longitude = lon;
		// - Fire a notification to all listeners to update the presentation layer.
		if (!oldLongitude.identical(lon))
			firePropertyChange(Unit.LATITUDE, oldLongitude, lon);
	}

	/**
	 * Returns a DMS point location with the latitude and the longitude of the real location of the unit in the
	 * map surface.
	 * 
	 * @return The unit location in DMS coordinates.
	 */
	public DMSPoint getLocation() {
		return new DMSPoint(latitude, longitude);
	}

	public String getSide() {
		return side;
	}

	public void setSide(final String side) {
		final String oldSide = this.side;
		this.side = side;
		//- If this unit is a friend unit the the visibility state is ever set to visible.
		if (Unit.FRIEND_SIDE.equals(this.side)) {
			this.setVisibilityState(Unit.FULL_VISIBLE);
		}
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.SIDE, oldSide, side);
	}

	public String getDetectState() {
		return detectState.getState();
	}

	public String getVisibilityState() {
		return visibilityState;
	}

	/**
	 * Set the visibility of the unit and of the other objects that are related to it depending on the detection
	 * status and the type of unit.
	 */
	public void setVisibilityState(String state) {
		//- For simple Units the state just affects the unit visibility status.
		if (Unit.FRIEND_SIDE.equals(this.getSide())) {
			this.visibilityState = FULL_VISIBLE;
		} else {
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(state))
				this.visibilityState = NOT_VISIBLE;
			if (HarpoonConstants.CONTACT_STATE.equals(state))
				this.visibilityState = CONTACT_VISIBLE;
			if (HarpoonConstants.IDENTIFIED_STATE.equals(state))
				this.visibilityState = IDENTIFICATION_VISIBLE;
		}
	}

	// - C H I L D R E N - S E C T I O N
	// public Vector<Unit> getChildren() {
	// return children;
	// }

	//	/**
	//	 * This field is used to store all units that depend on this one. This will require the
	//	 * <code>getModelChildren()</code> method to generate the controller Parts. This is only osed on the
	//	 * RootMapUnit model element.
	//	 */
	// public void addChild(final Unit child) {
	// // TODO The event parameters have to be adapted to the implementation needs.
	// children.add(child);
	// fireStructureChange(CHILDREN, null, child);
	// }
	//
	// public void removeChild(final Unit child) {
	// // TODO The event parameters have to be adapted to the implementation needs.
	// children.remove(child);
	// fireStructureChange(CHILDREN, child, null);
	// }
	//
	// public void clear() {
	// children.clear();
	// }

	//	public int getDetectStateCode() {
	//		return detectState.getDetectStateCode();
	//	}
	//
	//	public boolean isDetected() {
	//		if (HarpoonConstants.DETECTED_STATE == detectState.getState()) return true;
	//		return false;
	//	}

	//	public void degradeDetection() {
	//		final Calendar time = detectState.getLastStamp();
	//		final Calendar now = Calendar.getInstance();
	//		final long diff = now.getTimeInMillis() - time.getTimeInMillis();
	//		if (diff > DETECT_TIME_DECAY) {
	//			detectState.back();
	//			logger.info("Downgrading to state " + detectState.getState() + " - " + toString());
	//			// - Clear the display only if the state reaches the NOT_DETECTED_STATE otherwise maintain it on display
	//			if (HarpoonConstants.NOT_DETECTED_STATE.equals(detectState.getState())) firePropertyChange(
	//					Unit.DETECTED, true, isDetected());
	//		}
	//	}

	// - D E T E C T E C T I O N - S E C T I O N
	/**
	 * Initiates the detection mechanism. A unit detected keep this state at least for 60 seconds. After that
	 * time the detection state decays until the unit is no longer detected. This is performed by some unit
	 * states. <br>
	 * 
	 * @param event
	 */
	public String fireDetection(final String event) {
		//- Send the detection event and check if the state has changed.
		final String currentState = detectState.getState();
		final String state = detectState.detectionEvent(event);
		if (!state.equals(currentState)) {
			//- Change model visibility state and the dependent children depending on detection state.
			this.setVisibilityState(state);
			//- Notify the EditPart of the change.
			firePropertyChange(Unit.DETECTIONCHANGE_PROP, currentState, state);
		}
		return state;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[Unit:");
		buffer.append(getName()).append("-");
		buffer.append("[").append(getLocation().toDisplay()).append("]").append("-");
		// buffer.append(longitude.toDisplay()).append("-");
		buffer.append(side).append("-");
		buffer.append(detectState.getState()).append("]");
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
