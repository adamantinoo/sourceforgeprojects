//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitProfile.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/UnitProfile.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.3  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.2  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-17 15:13:07  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//

package net.sourceforge.rcp.harpoon.test;

import java.io.Serializable;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.MultiFigure;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class UnitProfile implements Serializable {
	private static final long	serialVersionUID	= 4451183160964479590L;
	private String						name							= "Default";
	private int								margin						= 2;

	public int getMargin() {
		return margin;
	}

	public void setMargin(int margin) {
		this.margin = margin;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public IconFigure getDrawFigure(MultiFigure multiFigure) {
		if (name.equalsIgnoreCase("T1")) return new Type1DrawFigure(multiFigure);
		if (name.equalsIgnoreCase("T2")) return new Type2DrawFigure(multiFigure);
		if (name.equalsIgnoreCase("T3")) return new Type3DrawFigure(multiFigure);
		if (name.equalsIgnoreCase("T4")) return new Type4DrawFigure(multiFigure);
		if (name.equalsIgnoreCase("Submarine")) return new SubmarineDrawFigure(multiFigure);
		if (name.equalsIgnoreCase("Surface")) return new SurfaceDrawFigure(multiFigure);
		if (name.equalsIgnoreCase("Figther")) return new AircraftDrawFigure(multiFigure);
		if (name.equalsIgnoreCase("Missile")) return new MissileDrawFigure(multiFigure);
		if (name.equalsIgnoreCase("base")) return new BaseDrawFigure(multiFigure);
		return new DefaultFigure(multiFigure);
	}

	class DefaultFigure extends IconFigure {
		public DefaultFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		public Dimension getHotSpot() {
			// TODO Auto-generated method stub
			return new Dimension(0, 0);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			graphics.setBackgroundColor(getColor());
			graphics.fillRectangle(getBounds().getCopy());
		}

		@Override
		public Dimension getPreferredSize(int hint, int hint2) {
			// TODO Auto-generated method stub
			return new Dimension(10, 10);
		}
	}

	class Type1DrawFigure extends BasicFigure {
		private static final int	FIGURE_SIZE	= 16;

		public Type1DrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			setDrawingSize(FIGURE_SIZE);
			// this.setSize(this.getPreferredSize());
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			super.paintFigure(graphics);
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			bound.width -= 1;
			bound.height -= 1;

			// - Draw the figure body
			graphics.setForegroundColor(getColor());
			graphics.setLineWidth(2);
			graphics.drawRectangle(bound.x + 1, bound.y + 1, bound.width - 1, bound.height - 1);
			graphics.setLineWidth(1);
			if (isSelected()) {
				drawHandles(graphics);
				graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
				graphics.drawRectangle(bound);
				// graphics.drawRectangle(bound.x,bound.y,bound.width-1,bound.height-1);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}
	}

	class Type2DrawFigure extends BasicFigure {
		private static final int	FIGURE_SIZE	= 16;

		public Type2DrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			setDrawingSize(FIGURE_SIZE);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			// Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			bound.width -= 1;
			bound.height -= 1;

			// - Draw the figure body
			graphics.setForegroundColor(getColor());
			graphics.setLineWidth(2);
			graphics.drawOval(bound);
			graphics.setLineWidth(1);
			if (isSelected()) {
				drawHandles(graphics);
				graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
				graphics.drawRectangle(bound);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}
	}

	class Type3DrawFigure extends BasicFigure {
		private static final int	FIGURE_SIZE	= 16;

		public Type3DrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			setDrawingSize(FIGURE_SIZE);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			// Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			bound.width -= 1;
			bound.height -= 1;

			// - Draw the figure body. This is a triangle
			graphics.setForegroundColor(getColor());
			graphics.setLineWidth(2);
			final Point p1 = new Point(bound.x + bound.width / 2, bound.y);
			final Point p2 = new Point(bound.x + bound.width, bound.y + bound.height);
			final Point p3 = new Point(bound.x, bound.y + bound.height);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(p1, p2);
			graphics.drawLine(p2, p3);
			graphics.drawLine(p3, p1);
			graphics.setLineWidth(1);
			if (isSelected()) {
				drawHandles(graphics);
				graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
				graphics.drawRectangle(bound);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}
	}

	class Type4DrawFigure extends BasicFigure {
		private static final int	FIGURE_SIZE	= 16;

		public Type4DrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			setDrawingSize(FIGURE_SIZE);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			bound.width -= 1;
			bound.height -= 1;

			// - Draw the figure body. This is a triangle
			graphics.setForegroundColor(getColor());
			graphics.setLineWidth(2);
			// - Draw the figure body. This is a triangle
			final Rectangle box = getBounds().getCopy();
			box.width -= 2;
			box.height -= 2;
			final Point p1 = new Point(box.x + box.width / 2, box.y);
			final Point p2 = new Point(box.x + box.width, box.y + box.height / 2);
			final Point p3 = new Point(box.x + box.width / 2, box.y + box.height);
			final Point p4 = new Point(box.x, box.y + box.height / 2);
			graphics.drawLine(p1, p2);
			graphics.drawLine(p2, p3);
			graphics.drawLine(p3, p4);
			graphics.drawLine(p4, p1);
			graphics.setLineWidth(1);
			if (isSelected()) {
				drawHandles(graphics);
				graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
				graphics.drawRectangle(bound);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}
	}

	class SubmarineDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		public SubmarineDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			final Rectangle border = getBounds().getCopy();
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			if (isSelected()) {
				graphics.setForegroundColor(HarpoonColorConstants.ORANGE);
				graphics.setLineWidth(2);
				graphics.drawOval(border);
				graphics.setLineWidth(1);
				graphics.setForegroundColor(getColor());
				graphics.drawOval(bound);
			} else {
				// - Draw the figure body
				graphics.setForegroundColor(getColor());
				graphics.drawOval(bound);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

	}

	class SurfaceDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		public SurfaceDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			final Rectangle border = getBounds().getCopy();
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			// - Draw the figure body
			graphics.setForegroundColor(getColor());
			graphics.drawRectangle(bound);

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

	}

	class AircraftDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		public AircraftDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			final Rectangle border = getBounds().getCopy();
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			// - Draw the figure body. This is a triangle
			final Point p1 = new Point(bound.x + bound.width / 2, bound.y);
			final Point p2 = new Point(bound.x + bound.width, bound.y + bound.height);
			final Point p3 = new Point(bound.x, bound.y + bound.height);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(p1, p2);
			graphics.drawLine(p2, p3);
			graphics.drawLine(p3, p1);

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

	}

	class MissileDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		public MissileDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			final Rectangle border = getBounds().getCopy();
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			// - Draw the figure body. This is a triangle
			final Rectangle box = getBounds().getCopy();
			final Point p1 = new Point(box.x + box.width / 2, box.y);
			final Point p2 = new Point(box.x + box.width, box.y + box.height / 2);
			final Point p3 = new Point(box.x + box.width / 2, box.y + box.height);
			final Point p4 = new Point(box.x, box.y + box.height / 2);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(p1, p2);
			graphics.drawLine(p2, p3);
			graphics.drawLine(p3, p4);
			graphics.drawLine(p4, p1);

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

	}

	class BaseDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		// private UnitFigure parent;

		public BaseDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// super.paintFigure(graphics);

			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			// bound.expand(-1, -1);
			final Rectangle border = getBounds().getCopy();
			// bound.x+=1;
			// bound.y+=1;
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			// - Check if the figure is selected. This information is on the parent
			// if (isSelected()) {
			// AirportPart.log.log(Level.INFO, "Painting TestDrawFigure SELECTED");
			// - The unit is selected. Draw an orange border of two pixels.
			graphics.setForegroundColor(getColor());
			graphics.drawRectangle(bound);
			graphics.setForegroundColor(HarpoonColorConstants.ORANGE);
			graphics.setLineWidth(1);
			graphics.drawRectangle(border);
			graphics.drawRectangle(border.expand(-1, -1));
			// } else {
			// AirportPart.log.log(Level.INFO, "Painting TestDrawFigure UNSELECTED");
			// graphics.setForegroundColor(getColor());
			// graphics.drawRectangle(bound);
			// }

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

	}
}

// - UNUSED CODE ............................................................................................
