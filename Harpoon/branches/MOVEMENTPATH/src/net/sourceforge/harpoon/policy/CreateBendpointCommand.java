/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.harpoon.policy;

import net.sourceforge.harpoon.model.WireBendpoint;

public class CreateBendpointCommand extends BendpointCommand {

	public void execute() {
		final WireBendpoint wbp = new WireBendpoint();
		wbp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		getWire().insertBendpoint(getIndex(), wbp);
		super.execute();
	}

	public void undo() {
		super.undo();
		getWire().removeBendpoint(getIndex());
	}

}
