//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DMSCoordinate.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/DMSCoordinate.java,v $
//  LAST UPDATE:    $Date: 2007-11-16 10:50:18 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-11-02 09:34:49  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.4.2.2  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.4.2.1  2007-10-16 14:47:14  ldiego
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.2  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.harpoon.geom;

//- IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.text.NumberFormat;

// - CLASS IMPLEMENTATION ...................................................................................
public class DMSCoordinate implements Serializable {
	private static final long	serialVersionUID	= -8767644931020534493L;
	public static final int		LATITUDE					= 1;
	public static final int		LONGITUDE					= 2;

	protected int							degrees						= 0;
	protected int							minutes						= 0;
	protected int							seconds						= 0;
	protected char						sense							= 'X';

	// - C O N S T R U C T O R S
	/**
	 * Construct a new coordinate from the degrees, minutes, seconds and the character identifier of the side of
	 * this coordinate. Valid values are <i>N</i> and <i>S</i> for latitudes and <i>W</i> and <i>E</i> for
	 * longitudes. This character determines the type of coordinate but its usage is being deprecated to use the
	 * next constructor.
	 */
	public DMSCoordinate(final int degrees, final int minutes, final int seconds, final char sense) {
		this.degrees = degrees;
		this.minutes = minutes;
		this.seconds = seconds;
		this.sense = sense;
	}

	/**
	 * Construct a coordinate that is a latitude or longitude from its basic components that are the degrees,
	 * the minutes and the seconds. Any negative value will change the side for the component, thus is that if
	 * we create a new latitude with the values -45 0 0 then it will be interpreted as 045� 00' 00'' S because
	 * of that minus sign.
	 */
	public DMSCoordinate(final int degrees, final int minutes, final int seconds, final int coordType) {
		//FIXME Reduce values to the range for valid values of 60 for minutes and seconds.
		this(Math.abs(degrees), Math.abs(minutes), Math.abs(seconds), 'X');

		//- Convert values outside standard limits to the sexagesimal ranges
		adjustSeconds();
		adjustMinutes();
		adjustDegrees();

		// - Interpret the data depending on the type. LATITUDE or LONGITUDE types are accepted.
		if (LATITUDE == coordType) {
			if ((degrees < 0) || (minutes < 0) || (seconds < 0))
				sense = 'S';
			else sense = 'N';
		}
		if (LONGITUDE == coordType) {
			if ((degrees < 0) || (minutes < 0) || (seconds < 0))
				sense = 'W';
			else sense = 'E';
		}
	}

	public DMSCoordinate(final int degrees, final int minutes, final int seconds) {
		this(degrees, minutes, seconds, 'X');
	}

	public DMSCoordinate() {
		// TODO Auto-generated constructor stub
		this(0, 0, 0, 'X');
	}

	// - G E T T E R S / S E T T E R S
	public void setSense(final char sense) {
		this.sense = sense;
	}

	//- P U B L I C - S E C T I O N
	/**
	 * Returns a boolean indication if this two coordinates contains or not the same coordinate value
	 * equivalence.
	 * 
	 * @param coordinate
	 * @return
	 */
	public boolean identical(DMSCoordinate coordinate) {
		if (toSeconds() == coordinate.toSeconds())
			return true;
		else return false;
	}

	/**
	 * Generates the string representation for a Degree-Minute-Seconds coordinate using the next format:
	 * <dl>
	 * <dt><b>999�</b>
	 * <dt>
	 * <dd>3 digits for the degrees and ranging from 000 to 359 followed by the sign of degrees</dd>
	 * <dt><b>99'9</b></dt>
	 * <dd>2 digits for the minutes and a decimal minute to represent the seconds, ranging from 0 to 59.9 and
	 * followed by the minute indicator.</dd>
	 * </dl>
	 * 
	 * </pre>
	 */
	public String toDisplay() {
		final NumberFormat nf = NumberFormat.getIntegerInstance();
		final StringBuffer buffer = new StringBuffer();
		nf.setMinimumIntegerDigits(3);
		buffer.append(nf.format(degrees)).append("� ");
		nf.setMinimumIntegerDigits(2);
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(1);
		double mins = minutes + seconds / 60.0;
		buffer.append(nf.format(mins)).append("' ");
		//		else buffer.append(nf.format(minutes)).append("' ");
		buffer.append(sense);
		return buffer.toString();
	}

	/**
	 * Convert degrees to a number of seconds. Set the sign depending on the coordinate sense.
	 * 
	 * @return this coordinate converted to seconds of arc with a sign depending on the sense.
	 */
	public long toSeconds() {
		// - Convert degrees to a number of seconds. Set the sign depending on the coordinate sense.
		long secs = degrees * 60 * 60 + minutes * 60 + seconds;
		if ('S' == sense)
			return -secs;
		if ('W' == sense)
			return -secs;
		return secs;
	}

	// - S T A T I C - S E C T I O N
	//	/**
	//	 * Static function to create a new DMSCoordinate from a value expressed in seconds. It requires something to
	//	 * indicate if the resulting coordinate is a latitude or a longitude so this abstract function is
	//	 * deprecated.
	//	 * 
	//	 * @deprecated
	//	 */
	//	public static DMSCoordinate fromSeconds(final long offset) {
	//		// - Get the sign of the result.
	//		int sign = 1;
	//		if (offset < 0)
	//			sign = -1;
	//		// -Calculate the number of entire degrees and minutes and recreate the DMS coordinate.
	//		Double value = Math.abs(new Double(offset));
	//		int degrees = new Double(Math.floor(value / 3600.0)).intValue();
	//		value = value - degrees * 3600.0;
	//		final int minutes = new Double(Math.floor(value / 60.0)).intValue();
	//		value = value - minutes * 60.0;
	//		if (1 == sign)
	//			return new DMSCoordinate(degrees, minutes, value.intValue(), 'X');
	//		else return new DMSCoordinate(-degrees, minutes, value.intValue(), 'Y');
	//	}

	public static DMSCoordinate fromSeconds(final long offset, final int sense) {
		// - Get the sign of the result.
		int sign = 1;
		if (offset < 0)
			sign = -1;
		// - Calculate the number of entire degrees and minutes and recreate the DMS coordinate.
		Double value = Math.abs(new Double(offset));
		final int degrees = new Double(Math.floor(value / 3600.0)).intValue();
		value = value - degrees * 3600.0;
		final int minutes = new Double(Math.floor(value / 60.0)).intValue();
		value = value - minutes * 60.0;
		final int seconds = value.intValue();
		if (DMSCoordinate.LATITUDE == sense) {
			if (1 == sign)
				return new DMSCoordinate(degrees, minutes, seconds, 'N');
			else return new DMSCoordinate(degrees, minutes, seconds, 'S');
		}
		if (DMSCoordinate.LONGITUDE == sense) {
			if (1 == sign)
				return new DMSCoordinate(degrees, minutes, seconds, 'E');
			else return new DMSCoordinate(degrees, minutes, seconds, 'W');
		}
		return new DMSCoordinate();
	}

	//- P R O T E C T E D - S E C T I O N
	protected void adjustSeconds() {
		if (this.seconds > 60) {
			this.seconds -= 60;
			this.minutes++;
			this.adjustSeconds();
		} else return;
	}

	protected void adjustMinutes() {
		if (this.minutes > 60) {
			this.minutes -= 60;
			this.degrees++;
			this.adjustMinutes();
		} else return;
	}

	protected void adjustDegrees() {
		if (this.degrees > 360) {
			this.degrees -= 360;
			adjustDegrees();
		}
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		return "DMSCoordinate(" + degrees + "," + minutes + "," + seconds + "," + sense + ")";
	}
}

// - UNUSED CODE ............................................................................................
