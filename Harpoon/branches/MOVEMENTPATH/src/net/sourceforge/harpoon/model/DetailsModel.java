//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: DetailsModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/DetailsModel.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:50 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//

package net.sourceforge.harpoon.model;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class DetailsModel implements Serializable {
	private static final long	serialVersionUID	= -6597821748405804774L;
	protected String					name;
	protected String					model;

	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setModel(String modeldescription) {
		model = modeldescription;
	}
}

// - UNUSED CODE ............................................................................................
