//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: BaseTestFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/BaseTestFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-03 16:50:10 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.3  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.1  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class BaseTestFigure extends UnitFigure {

	private static final int	MARGIN		= 2;
	protected Label						nameLabel	= new Label("Test Figure");

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a square box with
	 * some lines inside with a name label at the right center.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center of the icon.
	 */
	public BaseTestFigure() {
	}

	// - P U B L I C S E C T I O N
	/**
	 * Sets the name of the label. The size of the parent figure has to be adjusted, but tet if the real soize of the
	 * label has also to be adjusted.
	 */
	public void setName(String name) {
		nameLabel.setText(name);
		Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		size = new Dimension((nameLabel.getText().length() + 1) * 6 + 2, FigureUtilities.getStringExtents(
				nameLabel.getText(), nameLabel.getFont()).height);
		// size.width+=8;
		nameLabel.setSize(size);
		nameLabel.setPreferredSize(size);
		// nameLabel.setBorder(new LineBorder(1));
		invalidate();
		TestPart.log.log(Level.FINE, "values for nameLabel after setting the new name");
		TestPart.log.log(Level.FINE, nameLabel.getText());
		TestPart.log.log(Level.FINE, nameLabel.getBounds().toString());
		// - Recalculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	@Override
	public void setSide(int newSide) {
		if (Unit.FRIEND == newSide) setColor(HarpoonConstants.COL_FRIEND);
		if (Unit.FOE == newSide) setColor(HarpoonConstants.COL_FOE);
		if (Unit.NEUTRAL == newSide) setColor(HarpoonConstants.COL_NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	@Override
	public Dimension getHotSpot() {
		final Dimension hot = iconic.getHotSpot();
		final int dum = 1;
		return new Dimension(hot.width + 4, hot.height + 4);
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension nameLabelSize2 = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel
				.getFont());
		// DEBUG Compare calculated size with current label size - No size set because name not changed
		final Dimension lab = nameLabel.getSize();
		final Dimension nameLabelSize = new Dimension((nameLabel.getText().length() + 1) * 6 + 2,
				nameLabelSize2.height);

		TestPart.log.log(Level.FINE, "icon size:" + iconicSize);
		TestPart.log.log(Level.FINE, "calculated label size:" + nameLabelSize);
		TestPart.log.log(Level.FINE, "label size:" + lab);

		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = MARGIN + iconicSize.width + MARGIN + nameLabelSize.width - 1;
		fullSize.height = MARGIN + Math.max(iconicSize.height, nameLabelSize.height) + MARGIN - 1;
		TestPart.log.log(Level.FINE, "final size:" + fullSize);
		return fullSize;
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("TestFigure(" + nameLabel.getText());
		buffer.append(",");
		buffer.append("preferredsize:" + getPreferredSize());
		buffer.append(",");
		buffer.append("size:" + getSize());
		buffer.append(",");
		buffer.append("bounds:" + getBounds());
		return buffer.toString();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// TODO Auto-generated method stub
		super.paintFigure(graphics);
	}

	class TestDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE				= 20;
		private static final int	SELECTION_BORDER	= 2;

		// private UnitFigure parent;

		public TestDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		@Override
		protected void paintFigure(Graphics graphics) {
			// super.paintFigure(graphics);

			// - Get drawing location. This should be already displaced from the top-left.
			final Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			// - Displace it the selection margin to get the top-left point.
			bound.x += SELECTION_BORDER;
			bound.y += SELECTION_BORDER;
			bound.width -= SELECTION_BORDER;
			bound.height -= SELECTION_BORDER;
			// - Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width -= (SELECTION_BORDER + 1 + 1);
			bound.height -= (SELECTION_BORDER + 1 + 1);
			// bound.expand(-1, -1);
			final Rectangle border = getBounds().getCopy();
			// bound.x+=1;
			// bound.y+=1;
			border.width -= 1 + 1;
			border.height -= 1 + 1;

			// - Check if the figure is selected. This information is on the parent
			// if (isSelected()) {
			// AirportPart.log.log(Level.INFO, "Painting TestDrawFigure SELECTED");
			// - The unit is selected. Draw an orange border of two pixels.
			graphics.setForegroundColor(getColor());
			graphics.drawRectangle(bound);
			graphics.setForegroundColor(HarpoonColorConstants.ORANGE);
			graphics.setLineWidth(1);
			graphics.drawRectangle(border);
			graphics.drawRectangle(border.expand(-1, -1));
			// } else {
			// AirportPart.log.log(Level.INFO, "Painting TestDrawFigure UNSELECTED");
			// graphics.setForegroundColor(getColor());
			// graphics.drawRectangle(bound);
			// }

			// - Draw the figure center
			bound = getBounds().getCopy();
			final Dimension hotspot = getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			final Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

		@Override
		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE + SELECTION_BORDER + 1, FIGURE_SIZE + SELECTION_BORDER + 1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE / 2, FIGURE_SIZE / 2);
		}

	}

}

// - UNUSED CODE ............................................................................................
