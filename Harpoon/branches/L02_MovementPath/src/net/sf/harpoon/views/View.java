package harpoonrcp;

import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import net.sourceforge.harpoon.model.units.RootMapUnit;

public class View extends ViewPart {

	public static final String	ID	= "net.sourceforge.rcp.harpoon.view";
	private SashForm						windowSplit;
	// private GraphicalViewerImpl viewer;
	private Composite						propertyViewer;
	private RootMapUnit					units;

	@Override
	public void createPartControl(Composite parent) {
		final Composite top = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		top.setLayout(layout);
		// top banner
		final Composite banner = new Composite(top, SWT.NONE);
		banner.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL, GridData.VERTICAL_ALIGN_BEGINNING,
				true, false));
		layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 10;
		layout.numColumns = 2;
		banner.setLayout(layout);

		// setup bold font
		final Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);

		Label l = new Label(banner, SWT.WRAP);
		l.setText("Subject:");
		l.setFont(boldFont);
		l = new Label(banner, SWT.WRAP);
		l.setText("This is a message about the cool Eclipse RCP!");

		l = new Label(banner, SWT.WRAP);
		l.setText("From:");
		l.setFont(boldFont);

		final Link link = new Link(banner, SWT.NONE);
		link.setText("<a>nicole@mail.org</a>");
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MessageDialog.openInformation(getSite().getShell(), "Not Implemented",
						"Imagine the address book or a new message being created now.");
			}
		});

		l = new Label(banner, SWT.WRAP);
		l.setText("Date:");
		l.setFont(boldFont);
		l = new Label(banner, SWT.WRAP);
		l.setText("10:34 am");
		// message contents
		final Text text = new Text(top, SWT.MULTI | SWT.WRAP);
		text
				.setText("This RCP Application was generated from the PDE Plug-in Project wizard. This sample shows how to:\n"
						+ "- add a top-level menu and toolbar with actions\n"
						+ "- add keybindings to actions\n"
						+ "- create views that can't be closed and\n"
						+ "  multiple instances of the same view\n"
						+ "- perspectives with placeholders for new views\n"
						+ "- use the default about dialog\n"
						+ "- create a product definition\n");
		text.setLayoutData(new GridData(GridData.FILL_BOTH));

		// //DEVEL Additions to incorporate our display
		// windowSplit = new SashForm(top, SWT.HORIZONTAL);
		// createMapViewer();
		// createPropertyViewer();
		// int[] we = new int [2];
		// we [0] = 80;we[1]=20;
		// windowSplit.setWeights(we);
	}

	/**
	 * Creates the canvas for the Map viewer. This is a special canvas that id a Draw2d viewer that presents figures and
	 * not SWT elements.<br>
	 * There must be a SWT <code>Composite</code> as the parent because all drawing elements must have a layout and
	 * adding directly the Draw2D viewer to the sash did not displayed any object.
	 */
	private void createMapViewer() {
		final Composite c1 = new Composite(windowSplit, SWT.NONE);
		c1.setBackground(new Color(Display.getDefault(), 255, 255, 255));
		c1.setLayout(new FillLayout());
		final Canvas can = new Canvas(c1, SWT.NONE);
		final GraphicalViewerImpl vi = new GraphicalViewerImpl();
		vi.setControl(can);
		final GraphicalViewerImpl viewer = vi;
		// this.loadData();
		// viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		// // TODO Initialize the root edit part that is the map to load the data
		// // scenery.getRootPart().generateMap();
		// viewer.setEditPartFactory(new HarpoonPartFactory());
		// viewer.setContents(units);

		// viewer.addSelectionChangedListener(new BaseSelectionListenerActionHarpoon("nombre"));
		// DefaultEditDomain domain = new DefaultEditDomain(null);
		// viewer.setEditDomain(domain);
	}

	// private void loadData() {
	// String fileLocation = "U:/ldiego/Workstage/Harpoon/sceneries/TestModel.harpoon";
	// try {
	// // - There are two things to be read, the Model and the scenery properties
	// String modelFileName = fileLocation;
	// if (null == modelFileName) throw new FileNotFoundException("The model store file does not exist");
	// FileInputStream fis = new FileInputStream(modelFileName);
	// ObjectInputStream ois = new ObjectInputStream(fis);
	// units = (RootMapUnit) ois.readObject();
	// ois.close();
	// // DEBUG Initialize to the preset set of units.
	// units.setMapProperties(new Properties());
	// units.createTestUnits();
	// // - Create the controller from the model.
	// // parts = new HarpoonController(units);
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (ClassNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	/**
	 * Creates the left pane of the sash with a property presentation where selected object will display their interface
	 * information.<br>
	 * This object will be replaced by an instance that is particular to each object selected so the code have to change
	 * to reflect that contents changes.
	 */
	private void createPropertyViewer() {
		final RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;
		propertyViewer = new Composite(windowSplit, SWT.NONE);
		propertyViewer.setLayout(rowLayout);
		final Text name = new Text(propertyViewer, SWT.BORDER);
		name.setText("USS Alpha");
		name.setEditable(false);
		final Text location = new Text(propertyViewer, SWT.BORDER);
		location.setText("51� 40\' 30\'\' N - 0� 6\' 30\'\' E");
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		createSpeedControl();
		createDirectionControl();
		createSensorsArea();
	}

	private Composite	speedControl	= null;
	private Text			speedLabel		= null;
	private Spinner		speedSet			= null;
	private Composite	directionControl;
	private Text			directionLabel;
	private Spinner		directionSet;
	private Composite	sensorsArea		= null;
	private Text			sensorLabel		= null;
	private Composite	sensorList		= null;
	private Button		radar					= null;
	private Button		sonar					= null;
	private Button		EEM						= null;

	private void createSpeedControl() {
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		speedControl = new Composite(propertyViewer, SWT.NONE);
		speedControl.setLayout(gridLayout);
		speedLabel = new Text(speedControl, SWT.NONE);
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedSet = new Spinner(speedControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		speedSet.setMaximum(900);
	}

	private void createDirectionControl() {
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		directionControl = new Composite(propertyViewer, SWT.NONE);
		directionControl.setLayout(gridLayout);
		directionLabel = new Text(directionControl, SWT.NONE);
		directionLabel.setEditable(false);
		directionLabel.setText("Direction:");
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionSet = new Spinner(directionControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setMaximum(900);
	}

	private void createSensorsArea() {
		sensorsArea = new Composite(propertyViewer, SWT.NONE);
		sensorsArea.setLayout(new RowLayout());
		sensorLabel = new Text(sensorsArea, SWT.BORDER);
		sensorLabel.setText("Sensors:");
		sensorLabel.setEditable(false);
		sensorLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		createSensorList();
	}

	private void createSensorList() {
		final RowLayout rowLayout1 = new RowLayout();
		rowLayout1.type = org.eclipse.swt.SWT.VERTICAL;
		rowLayout1.spacing = 2;
		rowLayout1.marginBottom = 2;
		rowLayout1.marginLeft = 1;
		rowLayout1.marginRight = 1;
		rowLayout1.marginTop = 2;
		rowLayout1.wrap = false;
		sensorList = new Composite(sensorsArea, SWT.NONE);
		sensorList.setLayout(rowLayout1);
		radar = new Button(sensorList, SWT.CHECK);
		radar.setText("Rad");
		sonar = new Button(sensorList, SWT.CHECK);
		sonar.setText("Son");
		EEM = new Button(sensorList, SWT.CHECK);
		EEM.setText("EEM");
	}

	@Override
	public void setFocus() {
		// System.out.println("setFocus on View");
	}
}
