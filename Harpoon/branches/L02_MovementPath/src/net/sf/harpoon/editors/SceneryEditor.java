//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryEditor.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryEditor.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:44:38 $
//  RELEASE:        $Revision: 1.8.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.8.2.2  2007-10-24 16:45:44  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.8.2.1  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.8  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.7  2007-10-02 09:04:25  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.6  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.5  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.4  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.3  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-10 12:56:18  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.1  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.editors;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.MultiPageEditorPart;

import net.sourceforge.harpoon.model.units.RootMapUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.app.HarpoonRegistry;
import net.sourceforge.rcp.harpoon.app.HarpoonSelectionListener;
import net.sourceforge.rcp.harpoon.app.SaveSceneryAction;
import net.sourceforge.rcp.harpoon.model.Scenery;
import net.sourceforge.rcp.harpoon.views.SelectionView;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class is created to wrap the already available code from other sources. Because the used code has a
 * lot of development that is uncertain if it is needed or not for our current development I will add new
 * classes in front where I will move validated code and new adaptations. It will also show the methods that
 * are really required to create an editor.<br>
 * <br>
 * This class is started when the user opens a file with a extension that is mapped to this editor as it is
 * configured in the plugin descriptor. The first method to be called is the <code>init()</code> that will
 * check the existence of the selected file and prepare the environment for the reading of the data that is
 * performed in the method <code>setInput()</code>.The method <code>createPages()</code> is also called
 * when the editor page is created and perform all visual structures creation.
 */
public class SceneryEditor extends MultiPageEditorPart {
	/**
	 * Reference to the main map page for this editor. This will help to set and change the
	 * <code>SelectionView</code> selection synchronization.
	 */
	protected SceneryPage								mapPage;
	/** The selection listener for this editor instance. Every editor has its own listener. */
	protected final ISelectionListener	selectionListener	= new HarpoonSelectionListener(this);
	/** The model manager used for i/o operations with models. */
	protected Scenery										scenery;
	/** Index of the workflowPage */
	protected int												workflowPageID;
	/** This is the reference to the root of the model. */
	protected RootMapUnit								modelUnits;

	// - C O N S T R U C T O R S
	// - P U B L I C S E C T I O N
	// - G E T T E R S / S E T T E R S
	/**
	 * Returns the selection listener.
	 * 
	 * @return the <code>ISelectionListener</code>
	 */
	protected ISelectionListener getSelectionListener() {
		return selectionListener;
	}

	/** Method to access to the main Map page. */
	public SceneryPage getMapPage() {
		return mapPage;
	}

	/**
	 * Method to get access to the Scenery model information. This is of use to the pages to set up the contents
	 * of the viewers and to the <code>Scenery</code> to track model changes and perform the periodic
	 * processing on the game data.
	 */
	public RootMapUnit getSceneryModel() {
		return modelUnits;
	}

	public Scenery getScenery() {
		return scenery;
	}

	public void setScenery(Scenery newScenery) {
		scenery = newScenery;
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * This method is called upon file selection with the matching file extension. File extension associations
	 * may be defined at the plug-in level or at the Workbench configuration Properties.<br>
	 * This method is also used to register our selection listener.
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#init(org.eclipse.ui.IEditorSite,org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		// - Scenery is ok. Do any other initialization.
		super.init(site, input);
		// - Add this editor selection listener to the list of listeners of this window
		final ISelectionService globalSelectionServide = getSite().getWorkbenchWindow().getSelectionService();
		globalSelectionServide.addSelectionListener(getSelectionListener());
	}

	/**
	 * Creates the pages of a multipage Editor. In our implementation we have only a single page so we can move
	 * it to a local field and avoid any page change. <br>
	 * The method creates the SceneryPage and then configures the editor with the page name. All this methods
	 * can be simplified.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#createPages()
	 */
	@Override
	protected void createPages() {
		try {
			// - Create scenery page and keep a reference for later access.
			mapPage = new SceneryPage(this);
			workflowPageID = addPage(mapPage, getEditorInput());
			setActivePage(workflowPageID);
		} catch (final PartInitException pie) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"An error occured during opening the scenery.", pie.getStatus());
		}
	}

	/**
	 * Opens the model and reads it into the workspace. Model is a serialized object from other save points. The
	 * management of the model input/output is delegated to the <code>ModelManager</code> that also reports to
	 * the <code>Scenery</code> instance for model registration.
	 */
	@Override
	protected void setInput(IEditorInput input) {
		// - Get access to the Scenery instance that we are reading.
		try {
			// FIXME This access is not allowed. We should access the corresponding global scenery and not a static
			// scenery = Scenery.getNewScenery();
			//			modelManager = new Scenery();
			modelUnits = Scenery.readSceneryModel(input);
			//			workflow = modelManager.getModel();
			super.setInput(input);
		} catch (final PartInitException pie) {
			final StringBuffer message = new StringBuffer(
					"Error during the reading of the data. The scenery cannot be read. Reason:\n");
			message.append(pie.getLocalizedMessage());
			message.append(pie.getStackTrace());
			MessageDialog.openError(getSite().getShell(), "Title", message.toString());
		}
	}

	/**
	 * Saves the state of the scenery. This has to be disabled until the game logic is set.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// EMPTY Not implemented at this release
	}

	/**
	 * Saves the state of the scenery. This has to be disabled until the game logic is set.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// EMPTY Not implemented at this release
	}

	/**
	 * Save as... is not supported.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/** Not necessary. We have no markers. */
	// @Override
	public void gotoMarker(IMarker marker) {
		// EMPTY Not implemented at this release
	}

	/**
	 * Not used at this implementation
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/**
	 * When this element is closed and disposed this methos will be called by the site to release any external
	 * or system resources.
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#dispose()
	 */
	@Override
	public void dispose() {
		// - Stop the running periodic processes. They are defined on the Scenery.
		getScenery().stop();
		// - Remove selection listener
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(getSelectionListener());
		//- Clean the Selection View from any unit property data
		SelectionView view = (SelectionView) HarpoonRegistry.getRegistry().get(SelectionView.ID);
		view.clearTopControl();
		//TODO Remove this editor from the Registry.
		HashMap<Object, Object> registry = HarpoonRegistry.getRegistry();
		registry.remove(getScenery().getSceneryName());

//- If this is the last Scenery Editor to be closed, then disable the Save menu item.
		Object register = registry.get(HarpoonConstants.CMD_SAVE_SCENERY);
		if (register instanceof SaveSceneryAction) {
			SaveSceneryAction saveSceneryAction = (SaveSceneryAction) register;
			boolean keepActive = false;
			Iterator<Object> instanceIt = registry.values().iterator();
			while (instanceIt.hasNext()) {
				Object instance = instanceIt.next();
				if (instance instanceof Scenery) {
					keepActive = true;
				}
			}
			if (!keepActive) saveSceneryAction.setEnabled(false);
		}
		// - important: always call super implementation of dispose
		super.dispose();
	}
}

// - UNUSED CODE ............................................................................................
