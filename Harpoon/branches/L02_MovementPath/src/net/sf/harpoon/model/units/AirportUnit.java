//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirportUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/AirportUnit.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.5.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.4  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.1  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.model;

import net.sourceforge.harpoon.model.units.Unit;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class AirportUnit extends Unit {
	private static final long	serialVersionUID	= 3005430142099050712L;

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("[Airport:");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
