//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/WarUnit.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.6.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.model.units;

import net.sourceforge.harpoon.model.SensorsModel;

//- IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class WarUnit extends MovableUnit {
	private static final long		serialVersionUID	= -3514624844988656815L;

	// - M O D E L F I E L D I D E N T I F I E R S
	public static final String	FLD_SENSORS				= "FLD_SENSORS";

	// - M O D E L F I E L D S
	private SensorsModel				sensorInformation	= new SensorsModel(this);

	// - G E T T E R S / S E T T E R S
	public SensorsModel getSensorInformation() {
		if (null == sensorInformation) sensorInformation = new SensorsModel(this);
		return sensorInformation;
	}

	public void setSensorInformation(SensorsModel sensor) {
		// - Check parameter validity. Null values have no effect.
		if (null == sensor) return;
		sensorInformation = sensor;
		firePropertyChange(FLD_SENSORS, null, sensor);
	}

	// [03]

	// - P U B L I C - S E C T I O N
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("[WarUnit:");
		buffer.append(sensorInformation.toString());
		buffer.append(super.toString());
		buffer.append("]");
		return buffer.toString();
	}
	// [02]
}

// - UNUSED CODE ............................................................................................
// [01]
// public boolean getRadarState() {
// return activeRadar;
// }
//
// public boolean getSonarState() {
// return activeSonar;
// }
//
// public boolean getECMState() {
// return activeECM;
// }

// public void activateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) {
// activeRadar = true;
// firePropertyChange(WarUnit.RADAR, false, true);
// }
// if (sensor.equals(WarUnit.SONAR)) {
// activeSonar = true;
// firePropertyChange(WarUnit.SONAR, false, true);
// }
// if (sensor.equals(WarUnit.ECM)) {
// activeECM = true;
// firePropertyChange(WarUnit.ECM, false, true);
// }
// // final int oldSide = this.side;
// // this.side = side;
// // - Fire a notification to all listeners to update the presentation layer.
//
// }

// public void deActivateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) activeRadar = false;
// if (sensor.equals(WarUnit.SONAR)) activeSonar = false;
// if (sensor.equals(WarUnit.ECM)) activeECM = false;
// }

// [02]
// public SensorsModel getSensors() {
// if (null == this.sensors) this.sensors = new SensorsModel(this);
// return this.sensors;
// }
// public boolean getRadarState() {
// return getSensors().getRadarState();
// }
//
// public void setRadarState(boolean newState) {
// final boolean oldState = getSensors().getRadarState();
// getSensors().setRadarState(newState);
// firePropertyChange(RADAR, oldState, newState);
// }
//
// public boolean getECMState() {
// return getSensors().getECMState();
// }
//
// public void setECMState(boolean newState) {
// final boolean oldState = getSensors().getECMState();
// getSensors().getECMState(newState);
// firePropertyChange(ECM, oldState, newState);
// }

// [03]
//
// public int getSonarRange() {
// // TODO Auto-generated method stub
// return 10;
// }
//
// public int getRadioRange() {
// // TODO Auto-generated method stub
// return 30;
// }

// public boolean getDetected() {
// return detected;
// }

// - G E T T E R S / S E T T E R S
// [01]
