//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ReferencePoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/ReferencePoint.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.1.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.1.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.1  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.model.units;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.Vector;

import org.eclipse.gef.EditPart;

import net.sourceforge.harpoon.model.WireEndPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class ReferencePoint extends Unit implements WireEndPoint {
	private static final long		serialVersionUID	= 3185696789228033997L;

	public static final String	WIDTH_PROP				= "ReferencePoint.WIDTH_PROP";
	public static final String	PARENT_SELECTED		= "ReferencePoint.PARENT_SELECTED";
	private static final int		WIDTH							= 10;

	/**
	 * This field replicates the state of the selection for the unit parent. If the MovableUnit is selected then
	 * all children are in the state of parent state selected.
	 */
	protected int								parentselected		= EditPart.SELECTED_NONE;
	/** List of outgoing Connections. */
	protected Vector<Wire>			sourceConnections	= new Vector<Wire>();
	/** List of incoming Connections. */
	protected Vector<Wire>			targetConnections	= new Vector<Wire>();

	// - G E T T E R S / S E T T E R S
	public int getWidth() {
		return WIDTH;
	}

//[01]

	public int getParentSelected() {
		return parentselected;
	}

	public void setParentselected(final int parentselected) {
		this.parentselected = parentselected;
		firePropertyChange(PARENT_SELECTED, null, parentselected);
	}

	//- W I R E E N D P O I N T - S E C T I O N
	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	public void addConnection(final Wire wire) {
		if ((wire == null) || (wire.getSource() == wire.getTarget())) throw new IllegalArgumentException(
				"Connections cannot be null or closed.");
		if (wire.getSource() == this) {
			sourceConnections.add(wire);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, null, wire);
		} else if (wire.getTarget() == this) {
			targetConnections.add(wire);
			firePropertyChange(TARGET_CONNECTIONS_PROP, null, wire);
		}
	}

	/**
	 * Return the list of outgoing connections that apply to this point.
	 * 
	 * @see net.sourceforge.harpoon.model.units.WireEndPoint#getSourceConnections()
	 */
	public List<Wire> getSourceConnections() {
		return sourceConnections;
	}

	/**
	 * Returns the list of incoming connections that apply to this point.
	 * 
	 * @see net.sourceforge.harpoon.model.units.WireEndPoint#getTargetConnections()
	 */
	public List<Wire> getTargetConnections() {
		return targetConnections;
	}

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	public void removeConnection(final Wire conn) {
		if (conn == null) throw new IllegalArgumentException("Connections cannot be null");
		if (conn.getSource() == this) {
			sourceConnections.remove(conn);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, conn, null);
		} else if (conn.getTarget() == this) {
			targetConnections.remove(conn);
			firePropertyChange(TARGET_CONNECTIONS_PROP, conn, null);
		}
	}

}

// - UNUSED CODE ............................................................................................
//[01]
// public void addSourceConnection(final PathConnectionModel conn) {
// // Assert.isNUll
// sourceConn = conn;
// }
//
// public void addTargetConnection(final PathConnectionModel conn) {
// // Assert.isNUll
// targetConn = conn;
// }
//
// public void removeSourceConnection(final PathConnectionModel conn) {
// if (sourceConn == conn) sourceConn = null;
// }
//
// public void removeTargetConnection(final PathConnectionModel conn) {
// if (targetConn == conn) targetConn = conn;
// }
