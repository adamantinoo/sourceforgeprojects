//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/MovableUnit.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.9.2.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.9.2.5  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.9.2.4  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.9.2.3  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.9.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.9.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.9  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.8  2007-10-03 16:50:09  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.7  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.6  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.5  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.4  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.3  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.model.units;

//- IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.eclipse.gef.EditPart;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.Details;
import net.sourceforge.harpoon.model.ExtendedData;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.TracePath;
import net.sourceforge.harpoon.model.WireEndPoint;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class MovableUnit extends Unit implements ExtendedData, WireEndPoint {
	private static final long			serialVersionUID	= 2209528353626060016L;

	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String		MODEL							= "MODEL";
	public static final String		DIRECTION					= "DIRECTION";
	public static final String		SPEED							= "SPEED";
	public static final String		MOVEMENTPATH			= "MOVEMENTPATH";
	public static final String		UNITTYPE					= "UNITTYPE";
	public static final String		EXTENDED_DATA			= "EXTENDED_DATA";
	public static final String		TRACEPATH					= "TRACEPATH";

	// - M O D E L F I E L D S
	/** This is the model description for the unit. In next releases will contain the database record reference. */
//	private final String					model							= "";
	/** Stores the speed at witch the unit is traveling. This is only valid for units that may move. */
	protected int									speed							= 0;
	/** Direction of movement of this Unit. This is the last value calculated from the current movement path. */
	private int										bearing						= -1;
	/**
	 * The path points for the movement commands for the unit. This is a series of points where the unit should
	 * travel along. Once the unit reaches the last point it signal this event and continues in the same
	 * direction until a new path is set. This information is passed along to the Figure for drawing if the
	 * figure is selected.
	 */
	protected MovementPath				path							= new MovementPath(this);
	/**
	 * This point reflect the start point for movement calculations. We now use this point instead using the
	 * first Reference Point found in the Movement Path.
	 */
	protected DMSPoint						startPoint				= getLocation();
	/**
	 * This field contains the type of unit to be displayed. Movable units can be sub classed to different
	 * presentation that all share the same architecture. This sub classing simplifies the number of classes.
	 */
	protected String							unitType					= HarpoonConstants.UNIT_AIR;
	protected TracePath						tracePath					= new TracePath();
	private ExtendedDataModel			extendedData			= new ExtendedDataModel();

	private int										selected					= EditPart.SELECTED_NONE;
	protected transient Calendar	lastMoveEvent;
	/** List of outgoing Connections. */
	protected Vector<Wire>				sourceConnections	= new Vector<Wire>();
	/** List of incoming Connections. */
	protected Vector<Wire>				targetConnections	= new Vector<Wire>();

	// - G E T T E R S / S E T T E R S
//	@Override
//	public void setLatitude(DMSCoordinate lat) {
//		super.setLatitude(lat);
//		startPoint.setDMSLatitude(lat);
//	}
//	@Override
//	public void setLongitude(DMSCoordinate lon) {
//		super.setLongitude(lon);
//		this.startPoint.setDMSLongitude(lon);
//	}
	/**
	 * Sets the unit model description. This attribute may change in the future to a reference to a database
	 * record.<br>
	 * This information is stored in the generic filed for Unit details calles <code>unitDetails</code> of
	 * class <code>Details</code>.
	 * 
	 * @param modeldescription
	 *          model description string.
	 */
	public void setModel(final String modeldescription) {
		if (null == unitDetails) unitDetails = new Details();
		unitDetails.setModel(modeldescription);
	}

	/**
	 * Return the speed at witch is traveling the unit. There are coded ranges for each unit model and speed for
	 * conversion.
	 * 
	 * @return the current speed of the unit in knots.
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * Sets the speed on the unit. Range validation are outside the responsibilities of this class.
	 * 
	 * @param speed
	 *          speed at witch this unit should travel from this instant.
	 */
	public void setSpeed(final int speed) {
		final int oldSpeed = this.speed;
		this.speed = speed;
		firePropertyChange(SPEED, oldSpeed, speed);
	}

	/**
	 * Gets the current calculated direction in degrees. North equivalence is 0 and represent the upward
	 * direction. Ranges from 0� to 360�. A no calculated value is signaled by a negative value and then the
	 * direction defaults to 0�.
	 * 
	 * @return direction of the unit in degrees.
	 */
	public int getBearing() {
		if (bearing < 0) // A new calculation for the direction is fired.
		bearing = getMovementPath().calculateBearing(getLocation());
		return bearing;
	}

	/**
	 * Return the current order of movement for this unit. This can be represented visually on the map as a set
	 * of connected points.
	 * 
	 * @return the current movement orders.
	 */
	public MovementPath getMovementPath() {
		if (null == path) path = new MovementPath(this);
		return path;
	}

	/**
	 * Sets the new movement path for this unit. A movement path is a list of destination points that define the
	 * travel path for the unit.
	 * 
	 * @param movement
	 *          new travel path for this unit.
	 */
	public void setMovementPath(final MovementPath movement) {
		if (null == movement) path = new MovementPath(this);
		path = movement;
		// - Clear direction cache so next accesses will recalculate the value.
		bearing = -1;
		lastMoveEvent = Calendar.getInstance();
		startPoint = getLocation();
		firePropertyChange(MOVEMENTPATH, null, path);
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(final String type) {
		unitType = type;
		firePropertyChange(UNITTYPE, null, unitType);
	}

	public Calendar getMovementStamp() {
		if (null == lastMoveEvent) lastMoveEvent = Calendar.getInstance();
		return lastMoveEvent;
	}

	public TracePath getTracePath() {
		if (null == tracePath) tracePath = new TracePath();
		return tracePath;
	}

	// - E X T E N D E D D A T A - S E C T I O N
	private ExtendedDataModel getData() {
		if (null == extendedData) extendedData = new ExtendedDataModel();
		return extendedData;
	}

	public String getExtendedData(final String dataIdentifier) {
		final String defaultValue = getExtendedDefault(dataIdentifier);
		return getData().getDataValue(dataIdentifier, defaultValue);
	}

	public String getExtendedDefault(final String dataIdentifier) {
		if (ExtendedData.XDT_SPEEDMAX.equals(dataIdentifier)) return "1000";
		if (ExtendedData.XDT_SPEEDMIN.equals(dataIdentifier)) return "0";
		if (ExtendedData.XDT_SPEEDINCREMENT.equals(dataIdentifier)) return "1";
		return "-1";
	}

	public void setExtendedData(final String dataIdentifier, final int value) {
		getData().setIntegerData(dataIdentifier, value);
	}

	public int getSensorRange(final String sensorType) {
		return getData().getIntegerValue(sensorType, 20);
	}

	public boolean isAirBorne() {
		if (HarpoonConstants.UNIT_AIR.equals(unitType)) return true;
		else return false;
	}

	public boolean isSurface() {
		if (HarpoonConstants.UNIT_SURFACE.equals(unitType)) return true;
		else return false;
	}

	public boolean isSubmarine() {
		if (HarpoonConstants.UNIT_SUBMARINE.equals(unitType)) return true;
		else return false;
	}

//- W I R E E N D P O I N T - S E C T I O N
	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	public void addConnection(final Wire wire) {
		if ((wire == null) || (wire.getSource() == wire.getTarget())) throw new IllegalArgumentException(
				"Connections cannot be null or closed.");
		if (wire.getSource() == this) {
			sourceConnections.add(wire);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, null, wire);
		} else if (wire.getTarget() == this) {
			targetConnections.add(wire);
			firePropertyChange(TARGET_CONNECTIONS_PROP, null, wire);
		}
	}

	/**
	 * Return a copy of the source connections for this unit.
	 * 
	 * @see net.sourceforge.harpoon.model.WireEndPoint#getSourceConnections()
	 */
	public List<Wire> getSourceConnections() {
		return new Vector<Wire>(sourceConnections);
	}

	/**
	 * Return a copy of the target connections for this unit.
	 * 
	 * @see net.sourceforge.harpoon.model.WireEndPoint#getTargetConnections()
	 */
	public List<Wire> getTargetConnections() {
		return new Vector<Wire>(targetConnections);
	}

	/**
	 * Remove an incoming or outgoing connection from this unit.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	public void removeConnection(final Wire conn) {
		if (conn == null) throw new IllegalArgumentException("Connections cannot be null");
		if (conn.getSource() == this) {
			sourceConnections.remove(conn);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, conn, null);
		} else if (conn.getTarget() == this) {
			targetConnections.remove(conn);
			firePropertyChange(TARGET_CONNECTIONS_PROP, conn, null);
		}
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Add this point to the points that mark the trace the movement of the unit is leaving on the map. The
	 * points stored are defined in the model coordinates so in a later stage this will also allow the
	 * presentation of this data when the map pans or the zoom changes.
	 */
	public void addTrace(final DMSPoint location) {
		getTracePath().addPoint(location);
		firePropertyChange(TRACEPATH, null, getTracePath());
	}

	/**
	 * Gets the time in seconds elapsed since the last movement path change. This is the information used to
	 * calculate the displacement on the current direction line since the unit StrartPoint.
	 * 
	 * @return time since last path change in seconds.
	 */
	public long elapsedMoveTime() {
		final Calendar now = Calendar.getInstance();
		if (null == lastMoveEvent) lastMoveEvent = Calendar.getInstance();
//		return new Double(now.getTimeInMillis() - lastMoveEvent.getTimeInMillis()).doubleValue() / 1000.0;
		return (now.getTimeInMillis() - lastMoveEvent.getTimeInMillis()) / 1000;
	}

	/**
	 * Return the list of model instances that are suitable to be converted into EditableParts. It has been
	 * detected that Parts can only be drawn inside the bounds of their parents so this limits our ability to
	 * draw any information outside the small area of the unit presentation.
	 */
	public List<Object> getControllableChilds() {
		final List<Object> childs = new Vector<Object>();
		childs.add(getTracePath());
		childs.addAll(getMovementPath().getControllableChilds());
		return childs;
	}

	public DMSPoint getStartMovementPoint() {
		return startPoint;
//		try {
//			return path.getPoints().getFirst();
//		} catch (final Exception e) {
//			final ReferencePoint ref = new ReferencePoint();
//			ref.setLatitude(getDMSLatitude());
//			ref.setLongitude(getDMSLongitude());
//			path.addPoint(getDMSLatitude(), getDMSLongitude());
//			return ref;
//		}
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer("[MovableUnit:");
		buffer.append(speed).append("-");
		buffer.append(bearing).append("-");
		buffer.append(unitType).append("-");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}

	/**
	 * Control the selection for this units. When the unit is selected, it has to activate all other dependent
	 * elements. To keep the presentation clean, this moment is where we disable the presentation of the
	 * additional elements for any of the other selectable units.
	 */
	public void setSelected(final int newState) {
		int oldState = selected;
		if (oldState != newState) {
			// - Signal selection to all children like the ReferencePoint in the movement path
			path.setParentselected(newState);
			selected = newState;
			firePropertyChange(Wire.PARENT_SELECTED, oldState, newState);
		}
	}

	public boolean isSelected() {
		if (EditPart.SELECTED == selected) return true;
		if (EditPart.SELECTED_PRIMARY == selected) return true;
		return false;
	}

	//	public Vector<DMSPoint> getTrace() {
	//		return tracePath;
	//	}

}

// - CLASS IMPLEMENTATION.................................................................................
class ExtendedDataModel implements Serializable {
	private final Properties	extendedData	= new Properties();
	{
		extendedData.setProperty(ExtendedData.XDT_SPEEDINCREMENT, new Integer(1).toString());
	}

	public int getIntegerValue(final String dataName, final int defaultValue) {
		// - Read the property value and convert it to integer.
		final String dataValue = extendedData.getProperty(dataName);
		if (null == dataValue) return defaultValue;
		try {
			return new Integer(dataValue).intValue();
		} catch (final Exception e) {
			return defaultValue;
		}
	}

	public String getDataValue(final String dataName, final String defaultValue) {
		// - Read the property value and convert it to integer.
		String dataValue = extendedData.getProperty(dataName);
		if (null == dataValue) dataValue = defaultValue;
		return dataValue;
	}

	public void setIntegerData(final String dataIdentifier, final int value) {
		extendedData.setProperty(dataIdentifier, new Integer(value).toString());
	}

}
// - UNUSED CODE ............................................................................................
