//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: Scenery.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/Scenery.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:44:38 $
//  RELEASE:        $Revision: 1.12.2.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.12.2.4  2007-10-24 16:45:44  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.12.2.3  2007-10-22 08:14:19  ldiego
//    - Changed the location of the test scenery.
//    - Activation of the processing loop.
//
//    Revision 1.12.2.2  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.12.2.1  2007-10-16 14:47:35  ldiego
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.12  2007-10-05 11:24:44  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.11  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.10  2007-10-02 09:04:25  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.9  2007-10-01 14:44:40  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.8  2007-09-28 11:24:51  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.7  2007-09-28 07:56:46  ldiego
//    - Changes to remove the standard threading model and substituing it by the
//      recommended UIJob model. This modification did not worked as expected and
//      the new version will use a new recommendation.
//
//    Revision 1.6  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.5  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.4  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.3  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-12 11:26:07  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Logger;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.MultiPageEditorPart;

import net.sourceforge.harpoon.model.units.RootMapUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonLoop;
import net.sourceforge.rcp.harpoon.editors.SceneryEditor;

// - CLASS IMPLEMENTATION ...................................................................................
public class Scenery {
	private static Logger					logger									= Logger.getLogger("net.sourceforge");
	/** Information about the types of files than can be open from the interface menus. */
	private final static String[]	OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
	private final static String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };

	//- States of the initialization of an scenery
	private static final String		UNINITIALIZED						= "UNINITIALIZED";
	private static final String		PREPARED								= "PREPARED";
	private static final String		COMPLETED								= "COMPLETED";

	/**
	 * Global storage while we construct the scenery information and we load the data inside the Editor methods.
	 * After this static instance is initialized the reference is not longer valid.
	 */
	private static Scenery				newScenery;
	//- I N S T A N C E - F I E L D S
	/** Status of the current scenery initialization progress. */
	private String								status									= UNINITIALIZED;

	@SuppressWarnings("unused")
	private String								sceneryName;																												// Scenery system file name
	@SuppressWarnings("unused")
	private String								modelFileName;																											// Model�s data system file name
	private Properties						sceneryProperties;																									// Scenery contents. Properties for the model
	private IFileStore						fileStoreModel;																										// Store where the editor can read the model data
	private MultiPageEditorPart		editor;																														// Reference to the container editor that display this scenery
	private RootMapUnit						units;																															// Reference to the root of the model data
	protected Display							display;																														// Reference to the application display artifact
	protected boolean							allowRun;																													// Flag that allows the periodic process to run

	// - S T A T I C - S E C T I O N .........................................................................
	public static Scenery getNewScenery() {
		if (null == newScenery) newScenery = new Scenery();
		return newScenery;
	}

	public static boolean checkFileStore(String sceneryName) throws PartInitException {
		if (null == sceneryName) return false;
		newScenery = new Scenery();
		newScenery.setSceneryName(sceneryName);

		// - Check that the file selected exists and is valid.
		final IFileStore fileStore = EFS.getLocalFileSystem().getStore(new Path(sceneryName));
		if (!fileStore.fetchInfo().isDirectory() && fileStore.fetchInfo().exists()) {
			// - Read the scenery properties and locate the scenery object data.
			final File sceneryFile = new File(sceneryName);
			if (null == sceneryFile) throw new PartInitException(
					"The scenery file selected cannot be open or is empty.");
			final Properties props = new Properties();
			try {
				props.load(new BufferedInputStream(new FileInputStream(sceneryFile)));
				newScenery.setProperties(props);
				final String modelFileName = props.getProperty("modelLocation");
				if (null == modelFileName) throw new PartInitException(
						"The scenery model data file selected cannot be open or is empty.");
				newScenery.setModelFileName(modelFileName);

				// - Test if the model exists and then open through the interface.
				final IFileStore fileStoreModel = EFS.getLocalFileSystem().getStore(new Path(modelFileName));
				if (!fileStoreModel.fetchInfo().isDirectory() && fileStoreModel.fetchInfo().exists()) {
					newScenery.setFileStore(fileStoreModel);
					newScenery.setLoadStatus(Scenery.PREPARED);
					return true;
				}
			} catch (final FileNotFoundException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be found.");
			} catch (final IOException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be read or open.");
			}
		}
		return false;
	}

	/**
	 * Once the scenery file selected is valid and accessible, this method calla the IDE to locate the specific
	 * editor that understands the selected file's extension. This will fire the EditorPart creation and
	 * initialization method with calls to the methods <code>init()</code> and <code>setInput()</code>.
	 */
	public static Scenery open(IWorkbenchWindow window) throws PartInitException {
		if (getNewScenery().getLoadStatus().equals(Scenery.PREPARED)) {
			final Scenery currentScenery = getNewScenery();
			final IWorkbenchPage page = window.getActivePage();
			final MultiPageEditorPart editor = (MultiPageEditorPart) IDE.openEditorOnFileStore(page, currentScenery
					.getFileStoreModel());
			((SceneryEditor) editor).setScenery(currentScenery);
			currentScenery.setEditor(editor);
			currentScenery.setModel(((SceneryEditor) editor).getSceneryModel());
			currentScenery.setLoadStatus(Scenery.COMPLETED);
			//- Clear the working scenery.
			newScenery = null;
			return currentScenery;
		} else throw new PartInitException("The scenery preallocated instance is not initialized.");
	}

	public static String openDialog(IWorkbenchWindow window) {
		// - Get the user to choose an scenery file.
		final FileDialog fileChooser = new FileDialog(window.getShell(), SWT.OPEN);
		// FIXME Configure the text of the dialog to be presented to the user.
		fileChooser.setText("Texto to be configured.");
		fileChooser.setFilterPath(null);
		fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(OPEN_FILTER_NAMES);
		final String sceneryName = fileChooser.open();
		return sceneryName;
	}

	/**
	 * Loads the model data and creates the corresponding MVC structures and initializes them.
	 * 
	 * @param input
	 * @throws PartInitException
	 */
	public static RootMapUnit readSceneryModel(IEditorInput input) throws PartInitException {
		try {
			// - Convert to a suitable File class to access the URI
			final FileStoreEditorInput fse = (FileStoreEditorInput) input;

			// - The URI gets the file name and path for the scenery data.
			final URI sceneryURI = fse.getURI();
			final FileInputStream fis = new FileInputStream(sceneryURI.getPath());

			// - There are two things to be read, the Model and the scenery properties
			final ObjectInputStream ois = new ObjectInputStream(fis);
			final RootMapUnit units = (RootMapUnit) ois.readObject();
			ois.close();
			units.setMapProperties(newScenery.getProperties());
			return units;
		} catch (final FileNotFoundException e) {
			throw new PartInitException("The model store file does not exist");
		} catch (final IOException ioe) {
			throw new PartInitException("The model store file can not be read\n" + ioe.getMessage());
		} catch (final ClassNotFoundException e) {
			throw new PartInitException("A class object required to read the model was not found.");
		}
	}

	// - G E T T E R S / S E T T E R S
	private String getLoadStatus() {
		return status;
	}

	private void setLoadStatus(String newStatus) {
		if (null != newStatus) status = newStatus;
	}

	public String getSceneryName() {
		return sceneryName;
	}

	private void setSceneryName(String sceneryName) {
		this.sceneryName = sceneryName;
	}

	public String getModelFileName() {
		return modelFileName;
	}

	private void setModelFileName(String modelFileName) {
		this.modelFileName = modelFileName;
	}

	public MultiPageEditorPart getEditor() {
		return editor;
	}

	private Properties getProperties() {
		return sceneryProperties;
	}

	private void setProperties(Properties props) {
		sceneryProperties = props;
	}

	private IFileStore getFileStoreModel() {
		return fileStoreModel;
	}

	private void setFileStore(IFileStore fileStoreModel) {
		// TODO Auto-generated method stub
		this.fileStoreModel = fileStoreModel;
	}

	private void setEditor(MultiPageEditorPart containerEditor) {
		editor = containerEditor;
	}

	public RootMapUnit getModel() {
		return units;
	}

	//FIXME The properties assignment is too late for the first readout of the map. This has to be moved to the setInput()
	public void setModel(RootMapUnit sceneryModel) {
		units = sceneryModel;
//		units.setMapProperties(getProperties());
	}

	// - P U B L I C - S E C T I O N
	/**
	 * This methods starts or restarts the game processing loop and the game timers.<br>
	 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
	 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside that
	 * thread that requires access to the UI (updating, creating new model elements) will be posted as another
	 * Runnable instance to the Display created for the application.
	 */
	public void startProcessingLoop() {
		// - Mark the flag that keeps the main thread running.
		display = Display.getCurrent();
		allowRun = true;
		final Scenery sce = this;
		// HarpoonLoop job = new HarpoonLoop(Display.getCurrent(), "GameProcessing loop");
		// job.schedule(2 * 1000);
		// job.runInUIThread(new NullProgressMonitor());
		// // TODO Launch a new thread to perform all the tasks
		// final Scenery scene = this;
		// - Create a new thread and keep it running the infinite loop
		final Thread gameProcessingThread = new Thread(new Runnable() {
			public void run() {
				while (allowRun) {
					// - Sleep until the right number of seconds. Only 0 - 15 - 30 and 45
					Calendar now = Calendar.getInstance();
					int secs = now.get(Calendar.SECOND);
					if ((0 == secs) || (15 == secs) || (30 == secs) || (45 == secs)) {
						try {
							// - Run the job but this time inside the Display UI environment.
							logger.info("Starting UIJob");
							HarpoonLoop job = new HarpoonLoop(sce, "Enemy Detection Block");
							display.syncExec(job);
							//						try {
							// - Sleep for some time.
							Thread.yield();
							Thread.sleep(5 * 1000);
						} catch (InterruptedException ie) {
							// - If the thread is interrupted then exit it by turning the flag.
							allowRun = false;
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						try {
							// - Sleep for some time.
							Thread.yield();
							Thread.sleep(2 * 1000);
						} catch (InterruptedException ie) {
							// - If the thread is interrupted then exit it by turning the flag.
							allowRun = false;
						}
					}
				}
			};
		});
		gameProcessingThread.start();
		gameProcessingThread.setName("GameProcessing loop");
	}

	public void stop() {
		allowRun = false;
	}

	// - CLASS IMPLEMENTATION .................................................................................

	//	class DMSVector {
	//
	//		private final DMSPoint	from;
	//		private final DMSPoint	to;
	//		private final int				module;
	//		private final int				bearing;
	//
	//		public DMSVector(DMSPoint from, DMSPoint to) {
	//			this.from = from;
	//			this.to = to;
	//			module = calculateModule();
	//			bearing = calculateAngle();
	//		}
	//
	//		public int getModule() {
	//			// TODO Auto-generated method stub
	//			return module;
	//		}
	//
	//		public int getBearing() {
	//			// TODO Auto-generated method stub
	//			return bearing;
	//		}
	//
	//		/** The module of a vector is the distance between the definition points. */
	//		private int calculateModule() {
	//			// - Calculate the module by substracting the points
	//			final DMSPoint norm = to.translate(from);
	//			double result = StrictMath.pow(new Double(norm.getLatitude().toSeconds() / 60.0).doubleValue(), 2.0);
	//			result += StrictMath.pow(new Double(norm.getLongitude().toSeconds() / 60.0).doubleValue(), 2.0);
	//			return new Double(StrictMath.sqrt(result)).intValue();
	//		}
	//
	//		private int calculateAngle() {
	//			// - Translate coordinates to point P1
	//			// final DMSPoint nextPoint = to;
	//			final DMSPoint vector = to.translate(from);
	//
	//			// - Calculate quadrant depending on the point signs.
	//			final long lat = vector.getLatitude().toSeconds();
	//			final long lon = vector.getLongitude().toSeconds();
	//			final int quadrant = calculateVectorQuadrant(lat, lon);
	//			final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
	//			// int quadrant = 0;
	//			// if ((lat >= 0) && (lon >= 0))
	//			// quadrant = 1;
	//			// else if ((lat >= 0) && (lon < 0))
	//			// quadrant = 4;
	//			// else if ((lat < 0) && (lon >= 0))
	//			// quadrant = 2;
	//			// else if ((lat < 0) && (lon < 0)) quadrant = 3;
	//			switch (quadrant) {
	//				case 0:
	//				case 1:
	//					// - Check if > 45�
	//					if (lon > lat) {
	//						final double alpha = StrictMath.asin(lon / h);
	//						return new Double(StrictMath.toDegrees(alpha)).intValue();
	//					} else {
	//						// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
	//						final double alpha = StrictMath.acos(lat / h);
	//						return new Double(StrictMath.toDegrees(alpha)).intValue();
	//					}
	//				case 2:
	//					// - Check if < 135�
	//					if (lat < lon) {
	//						final double alpha = StrictMath.acos(lon / h) + 90.0;
	//						return new Double(StrictMath.toDegrees(alpha)).intValue();
	//					} else {
	//						final double alpha = StrictMath.asin(lat / h) + 90.0;
	//						return new Double(StrictMath.toDegrees(alpha)).intValue();
	//					}
	//				case 3:
	//					// TODO Implement calculations for quadrant 3
	//					break;
	//				case 4:
	//					// TODO Implement calculations for quadrant 4
	//					break;
	//				default: // This vallue is never reached
	//					break;
	//			}
	//			return 0;
	//		}
	//
	//		private int calculateVectorQuadrant(long vectorLat, long vectorLon) {
	//			// - Calculate quadrant depending on the point signs.
	//			int quadrant = 0;
	//			if ((vectorLat >= 0) && (vectorLon >= 0))
	//				quadrant = 1;
	//			else if ((vectorLat >= 0) && (vectorLon < 0))
	//				quadrant = 4;
	//			else if ((vectorLat < 0) && (vectorLon >= 0))
	//				quadrant = 2;
	//			else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
	//			return quadrant;
	//		}
	//
	//		@Override
	//		public String toString() {
	//			return "DMSVector (" + module + "," + bearing + ")";
	//		}
	//	}

}

// - UNUSED CODE ............................................................................................
// [01]
// public static IWorkbenchWindow getWindow() {
// return window;
// }

// public static void setWindow(IWorkbenchWindow window) {
// Scenery.window = window;
// }
// public static void closeOld() {
// // TODO Auto-generated method stub
// currentScenery.close();
// }
