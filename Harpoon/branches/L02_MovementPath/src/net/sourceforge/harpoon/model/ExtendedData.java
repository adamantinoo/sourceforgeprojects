//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: ExtendedData.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/ExtendedData.java,v $
//  LAST UPDATE:    $Date: 2007-10-03 16:50:09 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface ExtendedData {
	String	ALTITUDE							= "ALTITUDE";
	String	DEEP									= "DEEP";
	String	XDT_SPEEDINCREMENT		= "XDT_SPEEDINCREMENT";
	String	XDT_SPEEDMIN					= "XDT_SPEEDMIN";
	String	XDT_SPEEDMAX					= "XDT_SPEEDMAX";
	String	XDT_ALTITUDEMAX				= "XDT_ALTITUDEMAX";
	String	XDT_ALTITUDEMIN				= "XDT_ALTITUDEMIN";
	String	XDT_ALTITUDEINCREMENT	= "XDT_ALTITUDEINCREMENT";
	String	XDT_DEEPMAX						= "XDT_DEEPMAX";
	String	XDT_DEEPMIN						= "XDT_DEEPMIN";
	String	XDT_DEEPINCREMENT			= "XDT_DEEPINCREMENT";
	String	XDT_RADARRANGE				= "XDT_RADARRANGE";
	String	XDT_SONARRANGE				= "XDT_SONARRANGE";
	String	XDT_RADIORANGE				= "XDT_RADIORANGE";

	public int getSensorRange(String sensorType);

	public boolean isAirBorne();

	public boolean isSurface();

	public boolean isSubmarine();

	// public boolean hasSensors();
}

// - UNUSED CODE ............................................................................................
