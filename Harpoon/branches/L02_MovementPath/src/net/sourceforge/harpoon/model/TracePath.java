//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: TracePath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/TracePath.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.1.2.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-23 15:55:02  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class TracePath extends PropertyModel {
	private static final long		serialVersionUID	= -5865448843677529652L;

	public static final String	ADDTRACE					= "ADDTRACE";

	protected Vector<DMSPoint>	dmsPoints					= new Vector<DMSPoint>();

//	protected Vector<Point>			xyPoints					= new Vector<Point>();

	// - G E T T E R S / S E T T E R S
	public Vector<DMSPoint> getTracePoints() {
		if (null == dmsPoints) dmsPoints = new Vector<DMSPoint>();
		return dmsPoints;
	}

	// - P U B L I C - S E C T I O N
	public void addPoint(final DMSPoint location) {
		getTracePoints().add(location);
		firePropertyChange(ADDTRACE, null, location);
	}
}

// - UNUSED CODE ............................................................................................
