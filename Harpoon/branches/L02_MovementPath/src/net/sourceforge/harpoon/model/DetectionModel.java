//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: DetectionModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/DetectionModel.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.1.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Calendar;

import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitState implements Serializable {
	private static final long	serialVersionUID	= 4135940623738210352L;
	private static final int	LOWER_STATE				= decode(HarpoonConstants.NOT_DETECTED_STATE);
	private static final int	HIGHER_STATE			= decode(HarpoonConstants.DETECTED_STATE);
	private String						state;
	private int								stateCode					= 0;
	private Calendar					lastStateTime			= Calendar.getInstance();

	// - S T A T I C - S E C T I O N
	public static int decode(String state) {
		if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) return 0;
		if (HarpoonConstants.CONTACT_STATE.equals(state)) return 1;
		if (HarpoonConstants.IDENTIFICATION_PHASE_STATE.equals(state)) return 2;
		if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) return 3;
		if (HarpoonConstants.DETECTED_STATE.equals(state)) return 4;
		return 0;
	}

	public static String encode(int code) {
		if (0 == code) return HarpoonConstants.NOT_DETECTED_STATE;
		if (1 == code) return HarpoonConstants.CONTACT_STATE;
		if (2 == code) return HarpoonConstants.IDENTIFICATION_PHASE_STATE;
		if (3 == code) return HarpoonConstants.IDENTIFIED_STATE;
		if (4 == code) return HarpoonConstants.DETECTED_STATE;
		return HarpoonConstants.NOT_DETECTED_STATE;
	}

	// - C O N S T R U C T O R S
	public UnitState(String state) {
		this.state = state;
		stateCode = decode(state);
	}

	// - G E T T E R S / S E T T E R S
	public String getState() {
		return state;
	}

	public int getDetectStateCode() {
		return stateCode;
	}

	public Calendar getLastStamp() {
		if (null == lastStateTime) {
			// - This can never happen.
			// FIXME This code has never to be executed
			@SuppressWarnings("unused")
			int dummy = 1;
			lastStateTime = Calendar.getInstance();
		}
		return lastStateTime;
	}

	// - P U B L I C - S E C T I O N
	public void stamp() {
		lastStateTime = Calendar.getInstance();
	}

	public void advance() {
		if (HIGHER_STATE == stateCode) return;
		stateCode++;
		state = encode(stateCode);
	}

	public void back() {
		if (LOWER_STATE == stateCode) return;
		stateCode--;
		state = encode(stateCode);
	}

	@Override
	public String toString() {
		return "[UnitState:" + state + "-" + lastStateTime.toString() + "]";
	}
}
// - UNUSED CODE ............................................................................................
