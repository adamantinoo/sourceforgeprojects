//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/ShipUnit.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.3.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.model;

import net.sourceforge.harpoon.model.units.MovableUnit;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipUnit extends MovableUnit {
	private static final long	serialVersionUID	= 1L;

	private ShipType	type;
//	private RootMapUnit				root;

	// private HarpoonMap map;

	// - C O N S T R U C T O R S
	// public ShipUnit() {}
	// public ShipUnit(HarpoonModel harpoonUnits, String unitName) {
	// super(harpoonUnits, unitName);
	// }
	// public ShipUnit(HarpoonModel harpoonUnits, String unitName, int side) {
	// super(harpoonUnits, unitName,side);
	// }

//	public ShipUnit() {
//		// TODO Auto-generated constructor stub
//		this.root = rootMapUnit;
//	}

	public void setType(ShipType unitType) {
		// TODO Auto-generated method stub
		this.type = unitType;
	}

//	public RootMapUnit getRoot() {
//		return this.root;
//	}
	//
	// public HarpoonMap getMap() {
	// return this.map;
	// }
}

// - UNUSED CODE ............................................................................................
