//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/WarPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.4.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.3  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.units.WarUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P R I V A T E S E C T I O N
public class WarPropertyPage extends MovablePropertyPage {
	/**
	 * This reference contains the pointer to all the model data. This will be needed to compose and fill the
	 * interface elements.
	 */
	private WarUnit	warModel;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the
	 * <code>SelectionView</code> at runtime. Other fields may be <code>null</code> and in those situation
	 * the code will generate a informational message.
	 */
	public WarPropertyPage(Composite top) {
		super(top);
	}

	// - P U B L I C S E C T I O N
	public void setModel(WarUnit model) {
		warModel = model;
		super.setModel(model);
	}

	// - O V E R R I D E S E C T I O N
	/**
	 * Add the interface items that are specific for WarParts. This include all the sensor information and
	 * weapons control.
	 * 
	 * @see net.sourceforge.harpoon.pages.UnitPropertyPage#build()
	 */
	@Override
	public void build() {
		// - Parent will create the base element and then add all data for other levels in the inheritance chain.
		super.build();

		// - Create the contents of sensors structure that are the items declared on a WarUnit
		// - Add each one of the available sensors for this unit. This is obtained from the model.
		final SensorsModel sensors = warModel.getSensorInformation();
		sensors.addSensorsPage(getPropertyPage());
	}
	//
	// private void createSensorsArea(Composite parent) {
	//
	// }
	//
	// private void createSensorList(Composite parent) {
	// }
}

// - UNUSED CODE ............................................................................................
