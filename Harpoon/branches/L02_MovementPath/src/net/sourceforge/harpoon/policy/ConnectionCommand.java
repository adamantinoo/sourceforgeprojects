/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.harpoon.policy;

import org.eclipse.gef.commands.Command;

import net.sourceforge.harpoon.model.WireEndPoint;
import net.sourceforge.harpoon.model.units.Wire;

public class ConnectionCommand extends Command {

	protected WireEndPoint	oldSource;
	protected String				oldSourceTerminal;
	protected WireEndPoint	oldTarget;
	protected String				oldTargetTerminal;
	protected WireEndPoint	source;
	protected String				sourceTerminal;
	protected WireEndPoint	target;
	protected String				targetTerminal;
	protected Wire					wire;

	public ConnectionCommand() {
		super("wire connection");
	}

	public boolean canExecute() {
		// if (target != null) {
		// final Vector conns = target.getConnections();
		// final Iterator i = conns.iterator();
		// while (i.hasNext()) {
		// Wire conn = (Wire) i.next();
		// if ((targetTerminal != null) && (conn.getTargetTerminal() != null))
		// if (conn.getTargetTerminal().equals(targetTerminal) && conn.getTarget().equals(target))
		// return false;
		// }
		// }
		return true;
	}

	public void execute() {
		if (source != null) {
			// wire.detachSource();
			wire.setSource(source);
			// wire.setSourceTerminal(sourceTerminal);
			// wire.attachSource();
		}
		if (target != null) {
			// wire.detachTarget();
			wire.setTarget(target);
			// wire.setTargetTerminal(targetTerminal);
			// wire.attachTarget();
		}
		if ((source == null) && (target == null)) {
			// wire.detachSource();
			// wire.detachTarget();
			wire.setTarget(null);
			wire.setSource(null);
		}
	}

	public String getLabel() {
		return "Connection change";
	}

	public WireEndPoint getSource() {
		return source;
	}

	public java.lang.String getSourceTerminal() {
		return sourceTerminal;
	}

	public WireEndPoint getTarget() {
		return target;
	}

	public String getTargetTerminal() {
		return targetTerminal;
	}

	public Wire getWire() {
		return wire;
	}

	public void redo() {
		execute();
	}

	public void setSource(final WireEndPoint newSource) {
		source = newSource;
	}

	public void setSourceTerminal(final String newSourceTerminal) {
		sourceTerminal = newSourceTerminal;
	}

	public void setTarget(final WireEndPoint newTarget) {
		target = newTarget;
	}

	public void setTargetTerminal(final String newTargetTerminal) {
		targetTerminal = newTargetTerminal;
	}

	public void setWire(final Wire w) {
		wire = w;
		oldSource = w.getSource();
		oldTarget = w.getTarget();
		// oldSourceTerminal = w.getSourceTerminal();
		// oldTargetTerminal = w.getTargetTerminal();
	}

	public void undo() {
		source = wire.getSource();
		target = wire.getTarget();
		// sourceTerminal = wire.getSourceTerminal();
		// targetTerminal = wire.getTargetTerminal();

		// wire.detachSource();
		// wire.detachTarget();

		wire.setSource(oldSource);
		wire.setTarget(oldTarget);
		// wire.setSourceTerminal(oldSourceTerminal);
		// wire.setTargetTerminal(oldTargetTerminal);

		// wire.attachSource();
		// wire.attachTarget();
	}

}
