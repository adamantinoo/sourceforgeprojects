//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovablePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/MovablePart.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:37 $
//  RELEASE:        $Revision: 1.5.2.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5.2.5  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.5.2.4  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.5.2.3  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.5.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.5.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.5  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.4  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.NodeEditPart;

import net.sourceforge.harpoon.figures.HarpoonFigureFactory;
import net.sourceforge.harpoon.figures.MovableFigure;
import net.sourceforge.harpoon.model.units.MovableUnit;
import net.sourceforge.harpoon.model.units.RootMapUnit;
import net.sourceforge.harpoon.model.units.Unit;
import net.sourceforge.harpoon.model.units.Wire;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
public class MovablePart extends UnitPart {
	// - P U B L I C - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelSourceConnections()
	 */
	@Override
	protected List<Wire> getModelSourceConnections() {
		return getCastedModel().getSourceConnections();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#getModelTargetConnections()
	 */
	@Override
	protected List<Wire> getModelTargetConnections() {
		return getCastedModel().getTargetConnections();
	}

	@Override
	protected MovableUnit getCastedModel() {
		return (MovableUnit) getModel();
	}

//	protected List<Object> getModelChildren2() {
//		final MovableUnit model = (MovableUnit) getModel();
//		final List<Object> childs = new Vector<Object>();
//		childs.add(model.getTracePath());
//		return childs;
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.harpoon.parts.UnitPart#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (MovableUnit.MODEL.equals(prop)) refreshVisuals();
		if (MovableUnit.DIRECTION.equals(prop)) refreshVisuals();
		if (MovableUnit.SPEED.equals(prop)) refreshVisuals();
		if (MovableUnit.MOVEMENTPATH.equals(prop)) refreshVisuals();
		if (Wire.PARENT_SELECTED.equals(prop)) refreshVisuals();

		// - Causes Graph to re-layout
		// ((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
		super.propertyChange(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.harpoon.parts.UnitPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final MovableFigure fig = (MovableFigure) getFigure();
		final MovableUnit model = getCastedModel();

		//- Be sure that FRIEND units are visible
		if (Unit.FRIEND_SIDE.equals(model.getSide())) fig.setVisible(true);
		else {
			//- Enemy units visibility depend on the detection state. Like other units.
			String state = model.getDetectState();
			if (HarpoonConstants.CONTACT_STATE.equals(state)) {
				fig.setVisible(true);
				fig.setDataVisibility(false);
				fig.setColorIntensity(HarpoonConstants.LOWLIGHT_INTENSITY);
			}
			if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) {
				fig.setVisible(true);
				fig.setDataVisibility(true);
				fig.setColorIntensity(HarpoonConstants.HIGHLIGHT_INTENSITY);
			}
		}

		// - Update figure visuals from current model data.
		fig.setSpeed(model.getSpeed());
		fig.setDirection(model.getBearing());
//		fig.setMovementPath(model.getMovementPath());
		super.refreshVisuals();
	}

	// - P R I V A T E - S E C T I O N
	/**
	 * Creation of the figure depends on some subtle model information to set the right icon to the common
	 * presentation and model unit. A MovableUnit model unit may represent many unit types, detecting which one
	 * is the right one to create the proper visible figure and property page depends on this method.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		final MovableUnit unit = (MovableUnit) getModel();

		// - Identify the subtype for this model.
		final String subType = unit.getUnitType();

		// - Create and initialize the figure
		final Figure fig = HarpoonFigureFactory.createFigure(this, unit, subType);
		// refreshVisuals();
		// // HarpoonLogger.log(Level.FINE, "Creating Figure ShipFigure");
		// fig.setSide(unit.getSide());
		// fig.setDirection(unit.getDirection());
		// fig.setSpeed(unit.getSpeed());
		// HarpoonLogger.log(Level.FINE, fig.toString());
		return fig;
	}

	// [01]

	/**
	 * Changes in the selection of MovableUnits change the elements that are visible on the presentation map. If
	 * a <code>MovableUnit</code> is selected, all the other visible elements have to be notified to change
	 * their visibility state. This makes the next behavior:
	 * <ul>
	 * <li>If the selection code is <code>SELECTED_PRIMARY</code> then all dependent elements are notified to
	 * make them visible.</li>
	 * <li>If the selection code is other that this one, then check that the selection is for another
	 * <code>MovableUnit</code>, if it is then the elements are notified to be hidden.</li>
	 * <li>If the selection is not another type of unit, then just change the selection state for this unit and
	 * do not notify the dependent elements.</li>
	 * </ul>
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 * @see net.sourceforge.harpoon.parts.GamePart#setSelected(int)
	 */
	@Override
	public void setSelected(final int value) {
		super.setSelected(value);

		if (EditPart.SELECTED_NONE != value) {
			//- Clean selection of all MovableUnits
			final RootMapPart root = getRootPart();
			RootMapUnit modelRoot = root.getCastedModel();
			final Iterator<Unit> it = modelRoot.getChildren().iterator();
			while (it.hasNext()) {
				final Unit unit = it.next();
				if (unit instanceof MovableUnit) {
					final MovableUnit movable = (MovableUnit) unit;
					movable.setSelected(EditPart.SELECTED_NONE);
				}
			}

			// - Get the model and update the selection status. Activate if we are being selected only.
			final MovableUnit model = getCastedModel();
			model.setSelected(value);
		}
	}

	@Override
	protected void createEditPolicies() {
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// public void activate() {
// if (isActive()) return;
// // - Start listening for changes in the model.
// getUnit().addPropertyChangeListener(this);
// super.activate();
// }
// public void deactivate() {
// if (!isActive()) return;
// // - Stop listening to events in the model.
// getUnit().removePropertyChangeListener(this);
// super.deactivate();
// }
