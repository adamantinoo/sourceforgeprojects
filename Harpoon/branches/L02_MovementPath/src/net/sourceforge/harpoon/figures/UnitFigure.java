//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/UnitFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.11.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.11  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.10  2007-10-03 16:50:09  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.9  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.8  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.7  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.6  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.5  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.4  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.3  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Color;

import net.sourceforge.harpoon.model.units.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.BasicFigure;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitFigure extends Figure {
	/** Drawing color for the unit. Depends on the game side. */
	private Color					color			= HarpoonColorConstants.UNKNOWN_SIDE;
	/** Reference to the icon drawing class that represents this unit. */
	protected IconFigure	iconic		= new BasicFigure(this);
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int						selected	= EditPart.SELECTED_NONE;

	// - G E T T E R S / S E T T E R S
	protected Color getColor() {
		return color;
	}

	protected void setColor(Color newColor) {
		color = newColor;
		// this.movable.setColor(newColor);
		iconic.repaint();
		this.repaint();
	}

	public void setDrawFigure(IconFigure shipDrawFigure) {
		// - Add the icon drawing class instance.
		iconic = shipDrawFigure;
	}

	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return selected;
	}

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(int value) {
		selected = value;
		HarpoonLogger.info(toString() + "Selected = " + value);
	}

	/**
	 * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while
	 * FOES are marked red. NEUTRAL are yellow and other are blue.
	 */
	public void setSide(String newSide) {
		if (Unit.FRIEND_SIDE.equals(newSide)) setColor(HarpoonConstants.COLOR_FRIEND_NORMAL);
		if (Unit.FOE.equals(newSide)) setColor(HarpoonConstants.COLOR_FOE_NORMAL);
		if (Unit.NEUTRAL.equals(newSide)) setColor(HarpoonConstants.COLOR_NEUTRAL_NORMAL);
		if (Unit.UNKNOWN_SIDE.equals(newSide)) setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	// - P U B L I C - S E C T I O N
	public Rectangle getReferenceBox() {
		return iconic.getBounds().getCopy();
	}

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == getSelected()) || (EditPart.SELECTED == getSelected())) return true;
		else return false;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		return "[UnitFigure:" + decodeSelectionCode(selected);
	}

	protected String decodeSelectionCode(int selected) {
		if (EditPart.SELECTED_NONE == selected) return "SELECTED_NONE";
		if (EditPart.SELECTED == selected) return "SELECTED";
		if (EditPart.SELECTED_PRIMARY == selected) return "SELECTED_PRIMARY";
		return "UNDEFINED";
	}

	// - A B S T R A C T S E C T I O N
	public abstract Point getHotSpot();

	// public abstract Dimension getPreferredSize(int wHint, int hHint);
	// public abstract String toString();

	// - P R O T E C T E D - S E C T I O N
	protected GridLayout createStdLayout(int margin) {
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = margin;
		grid.marginHeight = margin;
		grid.marginWidth = margin;
		grid.verticalSpacing = 0;
		return grid;
	}
}

// - UNUSED CODE ............................................................................................
