//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/MovableFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:35 $
//  RELEASE:        $Revision: 1.10.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.10.2.2  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.10.2.1  2007-10-18 16:53:41  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.10  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.7  2007-10-03 16:50:09  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.6  2007-10-02 09:03:44  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.5  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.model.units.Unit;
import net.sourceforge.harpoon.parts.GamePart;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
public abstract class MovableFigure extends UnitFigure {
	private static final int	MARGIN					= 2;
	private static final int	CHAR_WIDTH			= 6;
	private static final int	CHAR_HEIGHT			= 13;

	/** Label to draw the value for the speed property. */
	protected final Label			speedLabel			= new UnitDisplayLabel("00");
	/** Label to draw the value if this unit direction. */
	protected final Label			directionLabel	= new UnitDisplayLabel("000");
//	private MovementPath			path;
	protected int							speedLen				= 1;
	protected GamePart				part;
	/** Unit side. This allows to identify other units not known to the user or detected during the game. */
	protected String					side						= Unit.UNKNOWN_SIDE;

	public MovableFigure() {
	}

	protected void drawMovePath(final Graphics graphics) {
		// path.draw(graphics, this);
	}

	// - P U B L I C - S E C T I O N
	//[01]

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[MovableFigure:");
		buffer.append(speedLabel.getText()).append("kn-");
		buffer.append(directionLabel.getText()).append("�-");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed
	 * is represented as a numeric label with the speed in knots.
	 */
	public void setSpeed(final int newSpeed) {
		speedLabel.setText(new Integer(newSpeed).toString());
		invalidate();
		this.setSize(this.getPreferredSize(-1, -1));
	}

	public void setDirection(final int newDirection) {
		directionLabel.setText(new Integer(newDirection).toString());
		invalidate();
		this.setSize(this.getPreferredSize(-1, -1));
	}

//	/**
//	 * Set the movement path ordered to this unit. The presentation is a connected list of display points that
//	 * define the path of movement ordered to the Unit.
//	 * 
//	 * @param movementPath
//	 */
//	public void setMovementPath(final MovementPath movementPath) {
//		// TODO Auto-generated method stub
//		path = movementPath;
//		this.repaint();
//	}

//[02]

	// - P R I V A T E - S E C T I O N
	private Dimension getLeftSideSize() {
		final int maxChars = StrictMath.max(speedLabel.getText().length(), directionLabel.getText().length());
		final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT * 2);
		// leftLabelSize.width+=MARGIN;
		// leftLabelSize.height+=MARGIN+MARGIN;
		return leftLabelSize;
	}

	private Figure createLeftSide() {
		final Figure labelContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		labelContainer.setLayoutManager(grid);
		speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// speedlabel.setBorder(new LineBorder(1));
		labelContainer.add(speedLabel);
		directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// dirLabel.setBorder(new LineBorder(1));
		labelContainer.add(directionLabel);
		labelContainer.validate();
		return labelContainer;
	}

	// - CLASS IMPLEMENTATION.................................................................................
	class UnitDisplayLabel extends Label {
		private static final int	CHAR_WIDTH	= 6;
		private static final int	CHAR_HEIGHT	= 12;

		public UnitDisplayLabel(final String text) {
			setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			setLabelAlignment(PositionConstants.RIGHT);
			setText(text);
		}

		@Override
		public void setText(final String newText) {
			super.setText(newText);
			final Dimension size = new Dimension((newText.length() + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT);
			this.setSize(size);
			this.setPreferredSize(size);
		}
	}

	public GamePart getPart() {
		return part;
	}

	/**
	 * Makes the additional figure data (the label contens) visible or invisible. This affects the enemy units
	 * that will not display this information until <code>IDENTIFIED</code>.
	 */
	public void setDataVisibility(boolean visibility) {
		speedLabel.setVisible(visibility);
		directionLabel.setVisible(visibility);
	}

	/**
	 * Sets the color intensity depending on the detect level for the unit. Contact units are represented with
	 * low intensity colors while identified units are represented with the highlight color. The normal color
	 * for other units than <code>FRIEND</code> is not used.
	 */
	public void setColorIntensity(String intensity) {
		if (HarpoonConstants.LOWLIGHT_INTENSITY.equals(intensity)) {
			if (Unit.FRIEND_SIDE.equals(getSide())) setColor(HarpoonConstants.COLOR_FRIEND_LOWLIGHT);
			if (Unit.FOE.equals(getSide())) setColor(HarpoonConstants.COLOR_FOE_LOWLIGHT);
			if (Unit.NEUTRAL.equals(getSide())) setColor(HarpoonConstants.COLOR_NEUTRAL_LOWLIGHT);
		}
		if (HarpoonConstants.HIGHLIGHT_INTENSITY.equals(intensity)) {
			if (Unit.FRIEND_SIDE.equals(getSide())) setColor(HarpoonConstants.COLOR_FRIEND_HIGHLIGHT);
			if (Unit.FOE.equals(getSide())) setColor(HarpoonConstants.COLOR_FOE_HIGHLIGHT);
			if (Unit.NEUTRAL.equals(getSide())) setColor(HarpoonConstants.COLOR_NEUTRAL_HIGHLIGHT);
		}
	}

	public String getSide() {
		return side;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
// /**
// * Create the figure structure and the list of drawing elements once all data has been stored in its
// place.
// * This figure has two parts.
// * <ul>
// * <li>The left column contains the direction and speed labels.</li>
// * <li>The center contains the iconic representation.</li>
// * </ul>
// */
// public void init() {
// // FIX-ME Review this method. Adapt to the same format of the WarFigure.
// // - Create the complex internal parts of this figure.
// final GridLayout grid = new GridLayout();
// grid.numColumns = 2;
// grid.horizontalSpacing = MARGIN;
// grid.marginHeight = MARGIN;
// grid.marginWidth = MARGIN;
// grid.verticalSpacing = 0;
// setLayoutManager(grid);
//
// // - Add the left part of the figure. It contains the speed and direction.
// final Figure labelContainer = createLeftSide();
// this.add(labelContainer);
// // - Add the icon drawing class instance.
// this.add(iconic);
//
// // - Calculate size and bounds
// this.setSize(this.getPreferredSize(-1, -1));
// this.repaint();
// }
//
// // - O V E R R I D E - S E C T I O N
// /**
// * Return a dimension with the vector from the top-left coordinate to the hotspot figure location. This
// * takes on account the size of the figure parts and the location of their children hotspots.
// */
// @Override
// public Point getHotSpot() {
// // FIX-ME Review this methos. Adapt to the same format of the WarFigure.
// final Point hot = iconic.getHotSpot();
// // - Add the distance of the left labels.
// final Dimension leftSize = getLeftSideSize();
// final Dimension fullSize = getPreferredSize();
// return new Point(fullSize.width / 2, fullSize.height / 2);
// // return new Dimension(hot.width + leftSize.width, hot.height/ 2);
// }
//
// @Override
// public Dimension getPreferredSize(final int wHint, final int hHint) {
// // FIX-ME Review this methos. Adapt to the same format of the WarFigure.
// HarpoonLogger.info(">>> ENTERING");
// // - Get the sized of the composition objects.
// final Dimension iconicSize = iconic.getSize();
// final Dimension leftSize = getLeftSideSize();
// final Dimension fullSize = new Dimension(0, 0);
// fullSize.width = MARGIN + leftSize.width + MARGIN + iconicSize.width + MARGIN;
// fullSize.height = MARGIN + Math.max(iconicSize.height, leftSize.height) + MARGIN;
// HarpoonLogger.info("iconicSize = " + iconicSize);
// HarpoonLogger.info("leftSize = " + leftSize);
// HarpoonLogger.info("fullSize = " + fullSize);
// HarpoonLogger.info("<<< EXITING");
// return fullSize;
// }

// @Override
// protected void paintFigure(Graphics graphics) {
// // - This type of figures are only painted if they are visible
// // if (isVisible()) {
// // - This paints the left side and the icon
// super.paintFigure(graphics);
//
// // - Paint the movement path if this unit is selected
// if (isSelected()) {
// path.paint();
// }
// // }
// }

//[02]
// /**
// * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while
// * FOES are marked red. NEUTRAL are yellow and other are blue.
// */
// @Override
// public void setSide(int newSide) {
// // DEBUG I think this property is not necessary.
// // this.side = newSide;
// if (Unit.FRIEND == newSide) setColor(HarpoonConstants.COL_FRIEND);
// if (Unit.FOE == newSide) setColor(HarpoonConstants.COL_FOE);
// if (Unit.NEUTRAL == newSide) setColor(HarpoonConstants.COL_NEUTRAL);
// if (Unit.UNKNOWN_SIDE == newSide) setColor(HarpoonColorConstants.UNKNOWN_SIDE);
// }
