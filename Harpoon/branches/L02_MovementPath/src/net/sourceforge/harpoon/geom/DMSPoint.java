//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DMSPoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/DMSPoint.java,v $
//  LAST UPDATE:    $Date: 2007-10-31 14:47:36 $
//  RELEASE:        $Revision: 1.3.2.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3.2.3  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.3.2.2  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.3.2.1  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.harpoon.geom;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class DMSPoint implements Serializable {
	private static final long	serialVersionUID	= -1674626351426841763L;
	private DMSCoordinate			latitude;
	private DMSCoordinate			longitude;

	//- C O N S T R U C T O R S
	public DMSPoint(final DMSCoordinate lat, final DMSCoordinate lon) {
		latitude = lat;
		longitude = lon;
	}

	// - S T A T I C - S E C T I O N .........................................................................
	private static DMSPoint fromSeconds(final long lat, final long lon) {
		return new DMSPoint(DMSCoordinate.fromSeconds(lat, DMSCoordinate.LATITUDE), DMSCoordinate.fromSeconds(
				lon, DMSCoordinate.LONGITUDE));
	}

	// - I N S T A N C E - S E C T I O N .....................................................................
	// - G E T T E R S / S E T T E R S
	public DMSCoordinate getDMSLongitude() {
		if (null == longitude) longitude = new DMSCoordinate();
		return longitude;
	}

	public void setDMSLatitude(DMSCoordinate latitude) {
		this.latitude = latitude;
	}

	public DMSCoordinate getDMSLatitude() {
		if (null == latitude) latitude = new DMSCoordinate();
		return latitude;
	}

	public void setDMSLongitude(DMSCoordinate longitude) {
		this.longitude = longitude;
	}

	//- P U B L I C - S E C T I O N
	/**
	 * Calculates the vector with base the point received as the parameter and the destination this point. This
	 * is equivalent to the translation of this point to an origin indicated by the parameter.
	 * 
	 * @param point
	 *          new coordinate origin to be used to calculate the vector for this point.
	 * @return a vector based on the received parameter and the destination is this point.
	 */
	public DMSPoint offset(final DMSPoint point) {
		final long resultLat = getDMSLatitude().toSeconds() - point.getDMSLatitude().toSeconds();
		final long resultLon = getDMSLongitude().toSeconds() - point.getDMSLongitude().toSeconds();
		return DMSPoint.fromSeconds(resultLat, resultLon);
	}

	/**
	 * Calculates the vector with base the point received as the parameter and and on this point.
	 * 
	 * @deprecated
	 */
	public DMSPoint substract(final DMSPoint point) {
		final long resultLat = getDMSLatitude().toSeconds() - point.getDMSLatitude().toSeconds();
		final long resultLon = getDMSLongitude().toSeconds() - point.getDMSLongitude().toSeconds();
		return DMSPoint.fromSeconds(resultLat, resultLon);
	}

	/**
	 * Return a new point whose coordinates are equivalent to translate the current point to a new coordinate
	 * system whose origin is the parameter point.
	 * 
	 * @deprecated
	 */
	public DMSPoint translate(final DMSPoint point) {
		final long resultLat = getDMSLatitude().toSeconds() - point.getDMSLatitude().toSeconds();
		final long resultLon = getDMSLongitude().toSeconds() - point.getDMSLongitude().toSeconds();
		return DMSPoint.fromSeconds(resultLat, resultLon);
	}

	/**
	 * Translates this point by the delta amount. This is equivalent to move the coordinate origin by the
	 * opposite of the delta values.
	 * 
	 * @param latDelta
	 * @param lonDelta
	 * @return
	 */
	public DMSPoint translate(final double latDelta, final double lonDelta) {
		//FIXME This is not a valid implementation. The call generates a new object and leaves the original intact.
		final long resultLat = getDMSLatitude().toSeconds() + new Double(latDelta).longValue();
		final long resultLon = getDMSLongitude().toSeconds() + new Double(lonDelta).longValue();
		return DMSPoint.fromSeconds(resultLat, resultLon);
	}

	/**
	 * Return the module of a vector that starts at the coordinate origin and ends on this point. The module is
	 * calculated in nautical miles but discarding the effect of latitude on the projection of the longitude.
	 * 
	 * @return module of the point in nautical miles
	 */
	public int getModule() {
		//FIXME This method should return a double. It is the caller who has to choose if trims the result.
		// - Calculate the module by triangle calculation
		double result = StrictMath.pow(new Double(getDMSLatitude().toSeconds() / 60.0).doubleValue(), 2.0);
		result += StrictMath.pow(new Double(getDMSLongitude().toSeconds() / 60.0).doubleValue(), 2.0);
		return new Double(StrictMath.sqrt(result)).intValue();
	}

	public String toDisplay() {
		return latitude.toDisplay() + " - " + longitude.toDisplay();
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		return "DMSPoint(" + latitude.toString() + "," + longitude.toString() + ")";
	}

	//- P R I V A T E - S E C T I O N
	private DMSCoordinate offset(final DMSCoordinate destination, final int type) {
		// -Convert coordinates to seconds for operation
		long origin;
		if (DMSCoordinate.LATITUDE == type) origin = latitude.toSeconds();
		else origin = longitude.toSeconds();
		final long dest = destination.toSeconds();
		final long offset = dest - origin;

		// - Convert back from seconds to a DMS coordinate
		final DMSCoordinate result = DMSCoordinate.fromSeconds(offset, type);
		// if (DMSCoordinate.LATITUDE == type)
		// if (offset < 0)
		// result.setSense('S');
		// else
		// result.setSense('N');
		// else if (offset < 0)
		// result.setSense('W');
		// else
		// result.setSense('E');
		return result;
	}

}

// - UNUSED CODE ............................................................................................
