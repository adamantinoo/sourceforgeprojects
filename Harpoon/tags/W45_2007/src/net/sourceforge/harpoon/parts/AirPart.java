//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/AirPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.5  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.4  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.IFigure;

import net.sourceforge.harpoon.figures.AircraftFigure;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class AirPart extends UnitPart {
	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		AirUnit model = (AirUnit) this.getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure AircraftFigure");
		AircraftFigure fig = new AircraftFigure();
//		air.setSide(unit.getSide());
//		fig.setDirection(model.getDirection());
		fig.setSpeed(model.getSpeed());
//		HarpoonLogger.log(Level.FINE, air.toString());
		return fig;
	}
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		AircraftFigure fig = (AircraftFigure) this.getUnitFigure();
		AirUnit model = (AirUnit) this.getUnit();
//		fig.setDirection(model.getDirection());
		fig.setSpeed(model.getSpeed());
		fig.setSide(model.getSide());
		fig.setSize(fig.getPreferredSize());
	}
}
// - UNUSED CODE ............................................................................................
