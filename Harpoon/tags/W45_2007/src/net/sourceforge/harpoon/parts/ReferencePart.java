//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: ReferencePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/ReferencePart.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:51 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.6  2007-10-31 14:47:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1.2.5  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.1.2.4  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.1.2.3  2007-10-16 14:47:14  ldiego
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.1.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.1.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.tools.DragEditPartsTracker;
import org.eclipse.swt.widgets.Composite;

import net.sf.harpoon.model.units.ReferencePoint;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.WireEndPoint;
import net.sourceforge.harpoon.pages.UnitPropertyPage;

// - CLASS IMPLEMENTATION .................................................................................
public class ReferencePart extends UnitPart implements NodeEditPart {
	private ConnectionAnchor	anchor;

	// - P U B L I C - S E C T I O N
	public Composite createPropertyPage(final Composite top) {
		// - Create a new set of controls for this EditPart.
		final UnitPropertyPage page = new UnitPropertyPage(top);
		page.setContainer(top);
		page.setModel((Unit) getModel());
		page.build();
		return page.getPropertyPage();
	}

	@Override
	protected void createEditPolicies() {
		// - Handles constraint changes (e.g. moving and/or resizing) of model elements
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE, new ReferencePartNonResizableEditPolicy());
	}

	@Override
	protected IFigure createFigure() {
		return new ReferenceFigure();
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	protected ReferencePoint getCastedModel() {
		return (ReferencePoint) getModel();
	}

	@Override
	public DragTracker getDragTracker(final Request request) {
		// super.getDragTracker(request)
		return new ReferenceDragTracker(this);
	}

	@Override
	protected List<Wire> getModelSourceConnections() {
		return getCastedModel().getSourceConnections();
	}

	@Override
	protected List<Wire> getModelTargetConnections() {
		return getCastedModel().getTargetConnections();
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (ReferencePoint.WIDTH_PROP.equals(prop)) {
			refreshVisuals();
			return;
		}
		if (WireEndPoint.SOURCE_CONNECTIONS_PROP.equals(prop)) {
			refreshSourceConnections();
			return;
		}
		if (WireEndPoint.TARGET_CONNECTIONS_PROP.equals(prop)) {
			refreshTargetConnections();
			return;
		}
		if (ReferencePoint.PARENT_SELECTED_PROP.equals(prop)) {
			refreshVisuals();
			return;
		}
		super.propertyChange(evt);
	}

	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final ReferenceFigure fig = (ReferenceFigure) getFigure();
		final ReferencePoint model = getCastedModel();

		fig.setWidth(model.getWidth());
		// - If parent is selected then it should be visible.
		if (EditPart.SELECTED_PRIMARY == model.getParentSelected())
			fig.setVisible(true);
		else
			fig.setVisible(false);
		super.refreshVisuals();
	}

	// - N O D E E D I T P A R T - S E C T I O N
	public ConnectionAnchor getSourceConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	protected ConnectionAnchor getConnectionAnchor() {
		if (anchor == null) anchor = new ChopboxAnchor(getFigure());
		return anchor;
	}

	// - CLASS IMPLEMENTATION .................................................................................
	protected class ReferenceDragTracker extends DragEditPartsTracker {

		public ReferenceDragTracker(final EditPart sourceEditPart) {
			super(sourceEditPart);
		}

		@Override
		protected boolean isMove() {
			return true;
		}

		@Override
		protected boolean handleDragInProgress() {
			// TODO Auto-generated method stub
			return super.handleDragInProgress();
		}
	}

	// - CLASS IMPLEMENTATION .................................................................................
	private class ReferencePartNonResizableEditPolicy extends NonResizableEditPolicy {
		@Override
		protected Command getMoveCommand(final ChangeBoundsRequest request) {
			return new MoveReferencePartCommand("MOVE Selected ReferenceParts", request);
		}

		@Override
		public EditPart getTargetEditPart(final Request request) {
			return super.getTargetEditPart(request);
			// if (null == target) {
			// if (request instanceof ChangeBoundsRequest) {
			// final Iterator it = ((ChangeBoundsRequest) request).getEditParts().iterator();
			// if (it.hasNext()) target = ((EditPart) it.next()).getParent();
			// }
			// }
			// return target;
		}

		@Override
		public boolean isDragAllowed() {
			// TODO Auto-generated method stub
			return super.isDragAllowed();
		}

		@Override
		public boolean understandsRequest(final Request req) {
			// TODO Auto-generated method stub
			return super.understandsRequest(req);
		}

		@Override
		public void showSourceFeedback(final Request request) {
			// TODO Auto-generated method stub
			super.showSourceFeedback(request);
		}
	}

	// - CLASS IMPLEMENTATION .................................................................................
	private class MoveReferencePartCommand extends Command {
		private static final String											UNINITIALIZED				= "UNINITIALIZED";
		private static final String											UNDO_COMMITTED			= "UNDO_COMMITTED";
		private static final String											UNDO_INCOMPLETE			= "UNDO_INCOMPLETE";
		private static final String											EXECUTION_COMPLETED	= "EXECUTION_COMPLETED";
		private String																	executionState			= UNINITIALIZED;
		private final ChangeBoundsRequest								request;
		private final HashMap<ReferencePart, DMSPoint>	undoMap							= new HashMap<ReferencePart, DMSPoint>();

		public MoveReferencePartCommand(final String label, final ChangeBoundsRequest request) {
			super(label);
			this.request = request;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public boolean canUndo() {
			return hasExecuted();
		}

		@Override
		public void execute() {
			super.execute();
			// - Set the new coordinates of the ReferencePoint to the new data.
			final List<Object> parts = request.getEditParts();
			final Iterator<Object> it = parts.iterator();
			final Point delta = request.getMoveDelta();
			while (it.hasNext()) {
				final Object part = it.next();
				if (part instanceof ReferencePart) {
					final ReferencePart reference = (ReferencePart) part;
					final DMSPoint actualData = reference.move(delta);
					undoMap.put(reference, actualData);
				}
			}
			executionState = EXECUTION_COMPLETED;
		}

		@Override
		public void undo() {
			// - Set the coordinates of the ReferencePoint back to the original saved data
			final Iterator<ReferencePart> it = undoMap.keySet().iterator();
			while (it.hasNext()) {
				final ReferencePart reference = it.next();
				if (reference.isActive()) {
					reference.move(undoMap.get(reference));
				} else {
					executionState = UNDO_INCOMPLETE;
				}
			}
			executionState = UNDO_COMMITTED;
			super.undo();
		}

		private boolean hasExecuted() {
			if (EXECUTION_COMPLETED.equals(executionState))
				return true;
			else
				return false;
		}
	}

	/**
	 * The point has been requested to move by this extent. We have to adapt the new Map location to the new
	 * figure location.
	 * 
	 * @param delta
	 *          the x-y coordinates that the point has moved in the diagram.
	 * @return the old DMS location of this model point.
	 */
	public DMSPoint move(final Point delta) {
		// - Get the model and the model map coordinates.
		final ReferencePoint point = getCastedModel();
		final DMSPoint oldLocation = new DMSPoint(point.getDMSLatitude(), point.getDMSLongitude());

		final Point graphicLocation = RootMapPart.dms2xy(this, oldLocation);
		graphicLocation.x += delta.x;
		graphicLocation.y += delta.y;

		// - If latitude is south then invert the sign of the y coordinates
		if (point.getDMSLatitude().toSeconds() < 0) graphicLocation.y *= -1;

		// - Adjust the model coordinates with x-y displacement.
		final DMSPoint newLocation = RootMapPart.xy2dms(this, graphicLocation);
		point.setLatitude(newLocation.getDMSLatitude());
		point.setLongitude(newLocation.getDMSLongitude());
		return oldLocation;
	}

	public DMSPoint move(final DMSPoint newLocation) {
		// - Get the model and the model map coordinates.
		final ReferencePoint point = getCastedModel();
		final DMSPoint oldLocation = new DMSPoint(point.getDMSLatitude(), point.getDMSLongitude());

		point.setLatitude(newLocation.getDMSLatitude());
		point.setLongitude(newLocation.getDMSLongitude());
		return oldLocation;
	}
}

// - CLASS IMPLEMENTATION .................................................................................
class ReferenceFigure extends UnitFigure {
	private int	width	= 8;

	public ReferenceFigure() {
		// setWidth(12);
		this.setSize(this.getPreferredSize());
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		return new Dimension(width, width);
	}

	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		final Point loc = getLocation();
		graphics.setForegroundColor(ColorConstants.black);
		graphics.drawOval(loc.x, loc.y, width - 1, width - 1);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(final int width) {
		this.width = width;
		this.setSize(this.getPreferredSize());
	}

	@Override
	public Point getHotSpot() {
		return new Point(width / 2, width / 2);
	}
}

// - UNUSED CODE ............................................................................................
