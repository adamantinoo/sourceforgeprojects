//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SubUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/SubUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-05 09:03:51 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class SubUnit extends Unit {
	private static final long	serialVersionUID	= 1L;
//
//	public SubUnit(HarpoonModel harpoonUnits, String unitName) {
//		super(harpoonUnits, unitName);
//		// TODO Auto-generated constructor stub
//	}
//
//	@Override
//	public void draw(GC gc) {
//		// TODO Auto-generated method stub
//		
//	}

}

// - UNUSED CODE ............................................................................................
