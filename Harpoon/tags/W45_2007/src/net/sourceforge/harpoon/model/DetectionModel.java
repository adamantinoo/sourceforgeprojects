//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: DetectionModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/DetectionModel.java,v $
//  LAST UPDATE:    $Date: 2007-11-07 16:28:44 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-11-02 09:34:49  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Logger;

import net.sf.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetectionModel implements Serializable {
	private static final long			serialVersionUID			= 4135940623738210352L;
	private static Logger					logger								= Logger.getLogger("net.sourceforge");

	//	private static final int			LOWER_STATE						= decode(HarpoonConstants.NOT_DETECTED_STATE);
	//	private static final int			HIGHER_STATE					= decode(HarpoonConstants.IDENTIFIED_STATE);

	public static final String		DETECTION_EVENT				= "DetectionModel.DETECTION_EVENT";
	public static final String		MOVED_EVENT						= "DetectionModel.MOVED_EVENT";
	public static final String		TIMEELAPSED_EVENT			= "DetectionModel.TIMEELAPSED_EVENT";

	protected static final int		DETECTION_TIME				= 60;
	protected static final int		UNDETECTEDLAPSE_TIME	= 120;
	protected String							state									= HarpoonConstants.NOT_DETECTED_STATE;
	//	protected int										stateCode							= 0;
	protected transient Calendar	lastStateTime					= Calendar.getInstance();

	// - S T A T I C - S E C T I O N
	//	public static int decode(final String state) {
	//		if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) return 0;
	//		if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(state)) return 1;
	//		if (HarpoonConstants.CONTACT_STATE.equals(state)) return 2;
	//		if (HarpoonConstants.CONTACT_MOVED_STATE.equals(state)) return 3;
	//		if (HarpoonConstants.LOST_CONTACT_STATE.equals(state)) return 4;
	//		if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) return 5;
	//		return 0;
	//	}
	//
	//	public static String encode(final int code) {
	//		if (0 == code) return HarpoonConstants.NOT_DETECTED_STATE;
	//		if (1 == code) return HarpoonConstants.CONTACT_STATE;
	//		if (2 == code) return HarpoonConstants.IDENTIFICATION_PHASE_STATE;
	//		if (3 == code) return HarpoonConstants.IDENTIFIED_STATE;
	//		if (4 == code) return HarpoonConstants.DETECTED_STATE;
	//		return HarpoonConstants.NOT_DETECTED_STATE;
	//	}

	// - C O N S T R U C T O R S
	//	public DetectionModel(final String state) {
	//		this.state = state;
	//		stateCode = decode(state);
	//	}

	// - G E T T E R S / S E T T E R S
	public String getState() {
		return state;
	}

	//	public int getDetectStateCode() {
	//		return stateCode;
	//	}

	// - P U B L I C - S E C T I O N
	/**
	 * Implements the state machine that operates for the detection mechanism. The external events are received
	 * at this method and then the instance applies the timing parameters to calculate id the is an state
	 * transition and which is the resulting state. <br>
	 * The detection state transition depends on the current state and some time constraints. This is described
	 * inside the State Diagram <b>DetectionStates Diagram</b>.
	 * <ul>
	 * <li>If the unit is in the <code>NOT_DETECTED_STATE</code> and receives any
	 * <code>DETECTION_EVENT</code> then it moves automatically to the <code>INITIATED_DETECTION_STATE</code>.</li>
	 * <li>The Unit stays in the <code>INITIATED_DETECTION_STATE</code> until another
	 * <code>DETECTION_EVENT</code> arrives and the time elapsed since the last state change is greater that
	 * the time limit specified. Then it moves to the <code>DETECTION_EVENT</code> state.</li>
	 * <li>It fails back to the <code>NOT_DETECTED_STATE</code> if after 2 minutes there is not another
	 * <code>DETECTION_EVENT</code> that enables the next state.</li>
	 * <li>The Unit remains in the <code>CONTACT_STATE</code> until the unit moves, and this is indicated by
	 * a <code>MOVED_EVENT</code>. If the unit is in this state and moves then the state upgrades to the next
	 * state called <code>CONTACT_MOVED_STATE</code> and from this state it can move to the final
	 * identification state.</li>
	 * <li>If enough time elapses while the unit is in the <code>CONTACT_STATE</code> then the state fails
	 * back to <code>LOST_CONTACT_STATE</code>.</li>
	 * <li>The units fall back to <code>NOT_DETECTED_STATE</code> after a long period of time in the
	 * <code>LOST_CONTACT_STATE</code> and no other detections. If the unit is detected on the
	 * <code>LOST_CONTACT_STATE</code> then it moves again to the <code>CONTACT_STATE</code> and wait to
	 * move before advancing on the ladder.
	 * </ul>
	 */
	public String detectionEvent(final String event) {
		String currentState = getState();
		if (DETECTION_EVENT.equals(event)) {
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(currentState))
				changeState(HarpoonConstants.INITIATED_DETECTION_STATE);
			if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(currentState)) {
				//- Check the time we have spent in this state.
				final long elapsed = getElapsed();
				if (elapsed > DETECTION_TIME)
					changeState(HarpoonConstants.CONTACT_STATE);
				//TODO Activate the presentation of this unit. It now appears on the Map.
			}
			if (HarpoonConstants.CONTACT_MOVED_STATE.equals(currentState)) {
				//- Check the time we have spent in this state.
				final long elapsed = getElapsed();
				if (elapsed > DETECTION_TIME)
					changeState(HarpoonConstants.IDENTIFIED_STATE);
				//TODO Activate the presentation of this unit. It now appears on the Map.
			}
		}
		if (MOVED_EVENT.equals(event))
			if (HarpoonConstants.CONTACT_STATE.equals(currentState))
				changeState(HarpoonConstants.CONTACT_MOVED_STATE);
		if (TIMEELAPSED_EVENT.equals(event)) {
			if (getElapsed() > UNDETECTEDLAPSE_TIME) {
				if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(currentState))
					changeState(HarpoonConstants.NOT_DETECTED_STATE);
				if (HarpoonConstants.CONTACT_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.CONTACT_MOVED_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.IDENTIFIED_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.LOST_CONTACT_STATE.equals(currentState))
					changeState(HarpoonConstants.NOT_DETECTED_STATE);
			}
		}
		return getState();
	}

	//	public void stamp() {
	//		lastStateTime = Calendar.getInstance();
	//	}

	//	/**
	//	 * Advances the detection state in the detection ladder. The mthod updated the timer and also checks that we
	//	 * are not at the upper step before performing the update.
	//	 */
	//	public void advance() {
	//		if (HIGHER_STATE == stateCode) return;
	//		stateCode++;
	//		state = encode(stateCode);
	//		stamp();
	//	}
	//
	//	public void back() {
	//		if (LOWER_STATE == stateCode) return;
	//		stateCode--;
	//		state = encode(stateCode);
	//	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		return "[DetectionModel:" + state + "-" + lastStateTime.toString() + "]";
	}

	// - P R O T E C T E D - S E C T I O N
	protected void changeState(final String newState) {
		logger.info("Upgrading to state from " + getState() + " to " + newState);
		state = newState;
		//		stateCode = decode(newState);
		resetTimer();
	}

	/** Return the number of seconds this instance has been in the same state. */
	protected long getElapsed() {
		return (Calendar.getInstance().getTimeInMillis() - getLastStamp().getTimeInMillis()) / 1000;
	}

	public Calendar getLastStamp() {
		if (null == lastStateTime)
			lastStateTime = Calendar.getInstance();
		return lastStateTime;
	}

	protected void resetTimer() {
		lastStateTime = Calendar.getInstance();
	}
}
// - UNUSED CODE ............................................................................................
