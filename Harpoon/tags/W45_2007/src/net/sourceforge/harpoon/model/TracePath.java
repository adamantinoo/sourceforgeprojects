//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: TracePath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/TracePath.java,v $
//  LAST UPDATE:    $Date: 2007-11-07 16:28:44 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.2  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1.2.1  2007-10-23 15:55:02  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class TracePath extends PropertyModel {
	private static final long		serialVersionUID	= -5865448843677529652L;

	public static final String	ADDTRACE					= "TracePath.ADDTRACE";
	public static final String	VISIBILITY_PROP		= "TracePath.VISIBILITY_PROP";

	protected Vector<DMSPoint>	dmsPoints					= new Vector<DMSPoint>();
	protected String						visibilityState		= Unit.NOT_VISIBLE;

	//	protected Vector<Point>			xyPoints					= new Vector<Point>();

	// - G E T T E R S / S E T T E R S
	public Vector<DMSPoint> getTracePoints() {
		if (null == dmsPoints)
			dmsPoints = new Vector<DMSPoint>();
		return dmsPoints;
	}

	public String getVisibilityState() {
		return visibilityState;
	}

	public void setVisibilityState(String state) {
		String newState = this.visibilityState;
		if (HarpoonConstants.NOT_DETECTED_STATE.equals(state))
			newState = Unit.NOT_VISIBLE;
		if (HarpoonConstants.CONTACT_STATE.equals(state))
			newState = Unit.CONTACT_VISIBLE;
		if (HarpoonConstants.IDENTIFIED_STATE.equals(state))
			newState = Unit.IDENTIFICATION_VISIBLE;
		if (!this.visibilityState.equals(newState)) {
			String oldVisibility = this.visibilityState;
			this.visibilityState = newState;
			firePropertyChange(VISIBILITY_PROP, oldVisibility, newState);
		}
	}

	// - P U B L I C - S E C T I O N
	public void addPoint(final DMSPoint location) {
		getTracePoints().add(location);
		firePropertyChange(ADDTRACE, null, location);
	}
}

// - UNUSED CODE ............................................................................................
