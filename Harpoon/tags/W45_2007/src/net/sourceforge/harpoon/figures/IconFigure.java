//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: IconFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/IconFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:29 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.3  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.1  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class IconFigure extends Figure {
	protected UnitFigure	container;

	// - C O N S T R U C T O R S
	public IconFigure(UnitFigure figure) {
		// TODO Auto-generated constructor stub
		container = figure;
	}

	// - P U B L I C S E C T I O N
	protected UnitFigure getContainer() {
		return container;
	}

	protected Color getColor() {
		if (null == container) return ColorConstants.black;
		return getContainer().getColor();
	}

	protected boolean isSelected() {
		return container.isSelected();
	}

	// - A B S T R A C T S E C T I O N

	@Override
	protected abstract void paintFigure(Graphics graphics);

	@Override
	public abstract Dimension getPreferredSize(int hint, int hint2);

	public abstract Point getHotSpot();
}

// - UNUSED CODE ............................................................................................
