//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonLogger.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/log/HarpoonLogger.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:48 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-10-03 12:36:52  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.2  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sf.harpoon.log;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class HarpoonLogger {
	private static Logger	logger	= Logger.getLogger("net.sourceforge");

	public static void log(final Level level, final String message) {
		logger.log(level, message);
	}

	public static void info(final String message) {
		// TODO Auto-generated method stub
		logger.info(message);
	}

}

// - UNUSED CODE ............................................................................................
