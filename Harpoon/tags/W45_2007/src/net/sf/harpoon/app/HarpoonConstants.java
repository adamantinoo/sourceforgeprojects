//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonConstants.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonConstants.java,v $
//  LAST UPDATE:    $Date: 2007-11-07 16:28:43 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-11-02 09:34:48  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.5.2.3  2007-10-31 14:47:58  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.5.2.2  2007-10-24 15:16:27  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//
//    Revision 1.5.2.1  2007-10-10 08:34:25  ldiego
//    - Minor modification during the development of the display of the movement paths.
//
//    Revision 1.5  2007-10-03 16:49:36  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.4  2007-10-01 14:43:24  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.3  2007-09-27 16:43:56  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sf.harpoon.app;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public interface HarpoonConstants {
	// - HEX EQUIVALENTS

	// - M E N U S
	String	M_OPEN										= "open";
	String	M_HELP										= "harpoonhelp";

	// - C O L O R S
	String	LOWLIGHT_INTENSITY				= "LOWLIGHT_INTENSITY";
	String	NORMAL_INTENSITY					= "NORMAL_INTENSITY";
	String	HIGHLIGHT_INTENSITY				= "HIGHLIGHT_INTENSITY";
	Color		COLOR_FRIEND_LOWLIGHT			= new Color(Display.getCurrent(), 0, 0x99, 0);
	Color		COLOR_FRIEND_NORMAL				= new Color(Display.getCurrent(), 0, 0xCC, 0);
	Color		COLOR_FRIEND_HIGHLIGHT		= new Color(Display.getCurrent(), 0, 0xFF, 0);
	Color		COLOR_FOE_LOWLIGHT				= new Color(Display.getCurrent(), 0xAA, 0, 0);
	Color		COLOR_FOE_NORMAL					= new Color(Display.getCurrent(), 0xDD, 0, 0);
	Color		COLOR_FOE_HIGHLIGHT				= new Color(Display.getCurrent(), 0xFF, 0, 0);
	Color		COLOR_NEUTRAL_LOWLIGHT		= new Color(Display.getCurrent(), 0xFF, 0xFF, 0);
	Color		COLOR_NEUTRAL_NORMAL			= new Color(Display.getCurrent(), 0xFF, 0xFF, 0);
	Color		COLOR_NEUTRAL_HIGHLIGHT		= new Color(Display.getCurrent(), 0xFF, 0xFF, 0);
	Color		COL_RADAR									= new Color(Display.getCurrent(), 0xFF, 0x66, 0x66);
	Color		COL_SONAR									= new Color(Display.getCurrent(), 0x66, 0xFF, 0x99);
	Color		COL_PATH									= ColorConstants.gray;

	// - U N I T S
	String	UNIT_SURFACE							= "UNIT_SURFACE";
	String	UNIT_AIR									= "UNIT_AIR";
	String	UNIT_SUBMARINE						= "UNIT_SUBMARINE";

	String	UNIT_SENSOR								= "UNIT_SENSOR";

	// - S E N S O R T Y P E
	String	VISUAL_TYPE								= "VISUAL_TYPE";
	String	RADIO_TYPE								= "RADIO_TYPE";
	String	ECM_TYPE									= "ECM_TYPE";
	String	SONAR_TYPE								= "SONAR_TYPE";
	String	RADAR_TYPE								= "RADAR_TYPE";

	// - S T A T E S
	String	NOT_DETECTED_STATE				= "DetectionState.NOT_DETECTED_STATE";
	String	INITIATED_DETECTION_STATE	= "DetectionState.INITIATED_DETECTION_STATE";
	String	CONTACT_STATE							= "DetectionState.CONTACT_STATE";
	String	CONTACT_MOVED_STATE				= "DetectionState.CONTACT_MOVED_STATE";
	String	LOST_CONTACT_STATE				= "DetectionState.LOST_CONTACT_STATE";
	String	IDENTIFIED_STATE					= "DetectionState.IDENTIFIED_STATE";

	String	CMD_OPEN_MESSAGE					= "net.sourceforge.rcp.harpoon.openMessage";
	String	CMD_OPEN_SCENERY					= "net.sourceforge.rcp.harpoon.open";
	String	CMD_SAVE_SCENERY					= "net.sourceforge.rcp.harpoon.saveScenery";
}

// - UNUSED CODE ............................................................................................
