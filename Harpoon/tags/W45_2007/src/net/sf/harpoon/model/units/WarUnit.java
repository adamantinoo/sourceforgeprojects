//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/WarUnit.java,v $
//  LAST UPDATE:    $Date: 2007-11-07 16:28:44 $
//  RELEASE:        $Revision: 1.8 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.7  2007-11-02 09:34:48  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.6.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.6  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sf.harpoon.model.units;

import java.util.HashMap;
import java.util.Iterator;

import net.sf.harpoon.app.HarpoonConstants;

import net.sourceforge.harpoon.model.DetectionModel;
import net.sourceforge.harpoon.model.SensorsModel;

//- IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class WarUnit extends MovableUnit {
	private static final long				serialVersionUID		= -3514624844988656815L;
	//	private static Logger						logger							= Logger.getLogger("net.sourceforge");	private static Logger						contactLog							= Logger.getLogger("contact");
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String			SENSORCHANGE_PROP		= "WarUnit.SENSORCHANGE_PROP";
	public static final String			DETECTEDUNITS_PROP	= "WarUnit.DETECTEDUNITS_PROP";

	// - M O D E L F I E L D S
	protected SensorsModel					sensorInformation		= new SensorsModel(this);
	protected HashMap<Unit, String>	detectedUnits				= new HashMap<Unit, String>();

	// - G E T T E R S / S E T T E R S
	public SensorsModel getSensorInformation() {
		if (null == sensorInformation)
			sensorInformation = new SensorsModel(this);
		return sensorInformation;
	}

	public void setSensorInformation(final SensorsModel sensor) {
		// - Check parameter validity. Null values have no effect.
		if (null == sensor)
			return;
		sensorInformation = sensor;
		firePropertyChange(SENSORCHANGE_PROP, null, sensor);
	}

	// [03]

	// - P U B L I C - S E C T I O N
	// [02]
	/**
	 * Adds the detected unit to the list of units that are on the detection range for this unit. Also notifies
	 * the detected unit that is being detected by this unit to activate the changes in the presentation. Units
	 * are dependent for the detection state.
	 */
	public void detectedEvent(final Unit unit) {
		unit.fireDetection(DetectionModel.DETECTION_EVENT);
		detectedUnits.put(unit, unit.getDetectState());
		firePropertyChange(DETECTEDUNITS_PROP, null, detectedUnits);
	}

	public void notDetectedEvent(final Unit detectedUnit) {
		final String state = detectedUnit.fireDetection(DetectionModel.TIMEELAPSED_EVENT);
		if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
			detectedUnits.remove(detectedUnit);
			firePropertyChange(DETECTEDUNITS_PROP, null, null);
		}
	}

	public void timeDetection() {
		Iterator<Unit> it = this.detectedUnits.keySet().iterator();
		while (it.hasNext()) {
			this.notDetectedEvent(it.next());
		}
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Initiates the detection mechanism. A unit detected keep this state at least for 60 seconds. After that
	 * time the detection state decays until the unit is no longer detected. This is performed by some unit
	 * states. <br>
	 * 
	 * @param event
	 */
	public String fireDetection(final String event) {
		//- Send the detection event and check if the state has changed.
		final String currentState = detectState.getState();
		final String state = detectState.detectionEvent(event);
		if (!state.equals(currentState)) { //- Notify the EditPart of the change.
			//- Change model visibility state and the dependent children depending on detection state.
			//			if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
			this.setVisibilityState(state);
			//			}
			//			if (HarpoonConstants.CONTACT_STATE.equals(state)) {
			//				model.setVisibility(true);
			//			}
			//			if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) {
			//				model.setVisibility(true);
			//			}

			firePropertyChange(Unit.DETECTIONCHANGE_PROP, currentState, state);
		}
		return state;
	}

	@Override
	public String toString() {
		//		Iterator<Unit> it = this.detectedUnits.keySet().iterator();
		//		while (it.hasNext()) {
		//			this.notDetectedEvent(it.next());
		//		}
		final StringBuffer buffer = new StringBuffer("[WarUnit:");
		buffer.append(sensorInformation.toString());
		buffer.append(super.toString());
		buffer.append("]");
		return buffer.toString();
	}

}

// - UNUSED CODE ............................................................................................
// [01]
// public boolean getRadarState() {
// return activeRadar;
// }
//
// public boolean getSonarState() {
// return activeSonar;
// }
//
// public boolean getECMState() {
// return activeECM;
// }

// public void activateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) {
// activeRadar = true;
// firePropertyChange(WarUnit.RADAR, false, true);
// }
// if (sensor.equals(WarUnit.SONAR)) {
// activeSonar = true;
// firePropertyChange(WarUnit.SONAR, false, true);
// }
// if (sensor.equals(WarUnit.ECM)) {
// activeECM = true;
// firePropertyChange(WarUnit.ECM, false, true);
// }
// // final int oldSide = this.side;
// // this.side = side;
// // - Fire a notification to all listeners to update the presentation layer.
//
// }

// public void deActivateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) activeRadar = false;
// if (sensor.equals(WarUnit.SONAR)) activeSonar = false;
// if (sensor.equals(WarUnit.ECM)) activeECM = false;
// }

// [02]
// public SensorsModel getSensors() {
// if (null == this.sensors) this.sensors = new SensorsModel(this);
// return this.sensors;
// }
// public boolean getRadarState() {
// return getSensors().getRadarState();
// }
//
// public void setRadarState(boolean newState) {
// final boolean oldState = getSensors().getRadarState();
// getSensors().setRadarState(newState);
// firePropertyChange(RADAR, oldState, newState);
// }
//
// public boolean getECMState() {
// return getSensors().getECMState();
// }
//
// public void setECMState(boolean newState) {
// final boolean oldState = getSensors().getECMState();
// getSensors().getECMState(newState);
// firePropertyChange(ECM, oldState, newState);
// }

// [03]
//
// public int getSonarRange() {
// // TODO Auto-generated method stub
// return 10;
// }
//
// public int getRadioRange() {
// // TODO Auto-generated method stub
// return 30;
// }

// public boolean getDetected() {
// return detected;
// }

// - G E T T E R S / S E T T E R S
// [01]
