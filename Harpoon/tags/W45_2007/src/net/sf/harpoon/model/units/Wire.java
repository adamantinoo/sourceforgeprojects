//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: Wire.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/Wire.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:48 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.4  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1.2.3  2007-10-15 16:23:00  ldiego
//		- [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.1.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.1.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//

package net.sf.harpoon.model.units;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.gef.EditPart;

import net.sourceforge.harpoon.model.PropertyModel;
import net.sourceforge.harpoon.model.WireEndPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class Wire extends PropertyModel {
	private static final long		serialVersionUID			= 6921170638040858271L;

	public static final String	PARENT_SELECTED_PROP	= "Wire.PARENT_SELECTED";
	public static final String	SOURCE_PROP						= "Wire.SOURCE";
	public static final String	TARGET_PROP						= "Wire.TARGET";
	//	public static final String	VISIBILITY				= "Wire.VISIBILITY";
	public static final String	BENDPOINT_PROP				= "Wire.BENDPOINT";

	/**
	 * This field replicates the state of the selection for the unit parent. If the MovableUnit is selected then
	 * all children are in the state of parent state selected.
	 */
	protected int								parentselected				= EditPart.SELECTED_NONE;
	protected WireEndPoint			source, target;
	protected Vector<Bendpoint>	bendpoints						= new Vector<Bendpoint>();

	//	private boolean							visible						= false;

	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// static final long serialVersionUID = 1;
	// protected boolean value;
	// protected String
	// sourceTerminal,
	// targetTerminal;
	// [01]

	// - C O N S T R U C T O R S
	public Wire(final WireEndPoint from, final WireEndPoint to) {
		setSource(from);
		setTarget(to);
		source.addConnection(this);
		target.addConnection(this);
	}

	// - G E T T E R S / S E T T E R S
	public int getParentSelected() {
		return parentselected;
	}

	public void setParentselected(final int parentselected) {
		this.parentselected = parentselected;
		firePropertyChange(PARENT_SELECTED_PROP, null, parentselected);
	}

	public WireEndPoint getSource() {
		return source;
	}

	public void setSource(final WireEndPoint newSource) {
		final Object old = source;
		source = newSource;
		firePropertyChange(SOURCE_PROP, old, source);
	}

	public WireEndPoint getTarget() {
		return target;
	}

	public void setTarget(final WireEndPoint newTarget) {
		final Object old = target;
		target = newTarget;
		firePropertyChange(TARGET_PROP, old, target);
	}

	public Vector<Bendpoint> getBendpoints() {
		if (null == bendpoints) bendpoints = new Vector<Bendpoint>();
		return bendpoints;
	}

	// [02]

	// - P U B L I C - S E C T I O N
	public void insertBendpoint(final int index, final Bendpoint point) {
		getBendpoints().add(index, point);
		firePropertyChange(BENDPOINT_PROP, null, null);
	}

	public void removeBendpoint(final int index) {
		getBendpoints().remove(index);
		firePropertyChange(BENDPOINT_PROP, null, null);
	}

	public void setBendpoint(final int index, final Bendpoint point) {
		getBendpoints().set(index, point);
		firePropertyChange(BENDPOINT_PROP, null, null);
	}

	public void setBendpoints(final Vector<Bendpoint> points) {
		bendpoints = points;
		firePropertyChange(BENDPOINT_PROP, null, null);
	}

	//	public void setVisible(final boolean selected) {
	//		final boolean oldState = visible;
	//		visible = selected;
	//		firePropertyChange(VISIBILITY, oldState, selected);
	//	}
	//
	//	public boolean isVisible() {
	//		return visible;
	//	}
	// [03]
}
// - UNUSED CODE ............................................................................................
// [01]
// public void attachSource(){
// if (getSource() == null || getSource().getSourceConnections().contains(this))
// return;
// getSource().connectOutput(this);
// }
//
// public void attachTarget(){
// if (getTarget() == null || getTarget().getTargetConnections().contains(this))
// return;
// getTarget().connectInput(this);
// }
//
// public void detachSource(){
// if (getSource() == null)
// return;
// getSource().disconnectOutput(this);
// }
//
// public void detachTarget(){
// if (getTarget() == null)
// return;
// getTarget().disconnectInput(this);
// }

// [02]
//
// public String getSourceTerminal(){
// return sourceTerminal;
// }
//
//
// public String getTargetTerminal(){
// return targetTerminal;
// }
//
// public boolean getValue(){
// return value;
// }
// public void setSourceTerminal(String s){
// Object old = sourceTerminal;
// sourceTerminal = s;
// firePropertyChange("sourceTerminal", old, sourceTerminal);//$NON-NLS-1$
// }

// [03]
// public void setTargetTerminal(String s){
// Object old = targetTerminal;
// targetTerminal = s;
// firePropertyChange("targetTerminal", old, targetTerminal);//$NON-NLS-1$
// }

// public void setValue(boolean value){
// if (value == this.value) return;
// this.value = value;
// if (target != null)
// target.update();
// firePropertyChange("value", null, null);//$NON-NLS-1$
// }

// public String toString() {
// return "Wire(" + getSource() + "," + getSourceTerminal() + "->" + getTarget() + "," + getTargetTerminal()
// + ")";//$NON-NLS-5$//$NON-NLS-4$//$NON-NLS-3$//$NON-NLS-2$//$NON-NLS-1$
// }
