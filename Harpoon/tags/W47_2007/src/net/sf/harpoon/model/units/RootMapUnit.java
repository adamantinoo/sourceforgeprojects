//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/RootMapUnit.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:37 $
//  RELEASE:        $Revision: 1.9 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.8  2007-11-02 09:34:48  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.7.2.2  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.7.2.1  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle translation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.7  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.4  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.3  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sf.harpoon.model.units;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;

import net.sourceforge.harpoon.geom.DMSCoordinate;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Root of the element model for the game. This model element contains the list of all the units available on
 * the scenery.<br>
 * This element in the model gets represented by the Map and so it has a lot of properties that control the
 * presentation visibility and graphic characteristics.
 */
public class RootMapUnit extends Unit {
	private static final long				serialVersionUID			= -3072256166014998802L;

	public static final String			CHILDRENCHANGED_PROP	= "RootMapUnit.CHILDRENCHANGED_PROP";
	public static final String			PROPERTIES						= "PROPERTIES";

	/** Represent the scenery properties that define some of the map presentation characteristics. */
	private transient Properties		props;
	/**
	 * Cached data for the coordinate conversion procedures. This two variables represent the values to be used
	 * to convert a Map XY coordinate to a model DMS coordinate.
	 */
	private transient DMSCoordinate	latitude2Zoom;
	private transient DMSCoordinate	longitude2Zoom;

	// - M O D E L F I E L D S
	/**
	 * Stores the list of all units that are used in the game. The visibility properties for this units is kept
	 * inside another structure.
	 */
	private final Vector<Unit>			children							= new Vector<Unit>();
	private HashMap<Unit, String>		visibilityStates			= new HashMap<Unit, String>();

	// - C O N S T R U C T O R S
	/**
	 * This class requires a constructor because it is a special implementation of a Unit. It is the root for
	 * the model but at the same time contains the Map information where the other units may move. This model
	 * has a lot of properties that are read from the scenery file but the global Unit properties have to be set
	 * to default values on the construction.<br>
	 * These values map the next properties:
	 * <ul>
	 * <li>name - the scenery description name like "North sea maneuvers".</li>
	 * <li>latitude - latitude for the top-left corner of the map.</li>
	 * <li>longitude - longitude for the top-left corner of the map.</li>
	 * <li>side - set to NEUTRAL but has no impact on presentation.</li>
	 * </ul>
	 */
	public RootMapUnit() {
		setName("North sea maneuvers");
		setLatitude(new DMSCoordinate(0, 0, 0, 'N'));
		setLongitude(new DMSCoordinate(0, 0, 0, 'E'));
		setSide(Unit.NEUTRAL);
	}

	// - G E T T E R S / S E T T E R S
	public Properties getMapProperties() {
		if (null == props)
			props = new Properties();
		return props;
	}

	public void setMapProperties(final Properties props) {
		this.props = props;
		if (null == props)
			this.props = new Properties();
		// TODO Process the properties received if they are required or leave processing until request.
		// - Get the scenery name and description from the properties.
		final String sceneryName = props.getProperty("sceneryName", getName());
		setName(sceneryName);
		// TODO Read the description location and the description file to display the second page that are the
		// scenery instructions.
		// final String sceneryDescription = props.getProperty("sceneryDescLocation",
		// "U:/ldiego/Workstage/3.3_BaseWorkspace/HarpoonRCP/sceneries/Lesson01_description.html");

		// - Read the location of the top-left corner
		final String topLat = props.getProperty("topLatitude", "0 0 0 N");
		String[] components = topLat.split(" ");
		setLatitude(new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
				.intValue(), new Integer(components[2]).intValue(), components[3].charAt(0)));
		final String topLon = props.getProperty("topLatitude", "0 0 0 N");
		components = topLon.split(" ");
		setLongitude(new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
				.intValue(), new Integer(components[2]).intValue(), components[3].charAt(0)));
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(PROPERTIES, null, props);
		// - Clear the cached properties
		latitude2Zoom = null;
		longitude2Zoom = null;
	}

	public DMSCoordinate getLatitude2Zoom() {
		if (null == latitude2Zoom) {
			// - Read value from the properties and the store the value in the field.
			final String topLat = props.getProperty("latitude2Zoom", "0 10 0");
			final String[] components = topLat.split(" ");
			latitude2Zoom = new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
					.intValue(), new Integer(components[2]).intValue());
		}
		return latitude2Zoom;
	}

	public DMSCoordinate getLongitude2Zoom() {
		if (null == longitude2Zoom) {
			// - Read value from the properties and the store the value in the field.
			final String topLat = props.getProperty("longitude2Zoom", "0 10 0");
			final String[] components = topLat.split(" ");
			longitude2Zoom = new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
					.intValue(), new Integer(components[2]).intValue());
		}
		return longitude2Zoom;
	}

	// - C H I L D R E N - S E C T I O N
	public Vector<Unit> getChildren() {
		return children;
	}

	/**
	 * This field is used to store all units that depend on this one. This will require the
	 * <code>getModelChildren()</code> method to generate the controller Parts. This is only osed on the
	 * RootMapUnit model element.
	 */
	public void addChild(final Unit child) {
		children.add(child);
		//- If unit are FRIEND then add then being visible.
		if (Unit.FRIEND_SIDE.equals(child.getSide()))
			this.visibilityStates.put(child, Unit.FULL_VISIBLE);
		else this.visibilityStates.put(child, Unit.NOT_VISIBLE);
		fireStructureChange(RootMapUnit.CHILDRENCHANGED_PROP, null, child);
	}

	public void removeChild(final Unit child) {
		children.remove(child);
		this.visibilityStates.remove(child);
		fireStructureChange(RootMapUnit.CHILDRENCHANGED_PROP, child, null);
	}

	public void clear() {
		children.clear();
		this.visibilityStates.clear();
		fireStructureChange(RootMapUnit.CHILDRENCHANGED_PROP, null, null);
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Return the visibility status for a selected unit. This is used while refresing the presentation to know
	 * what to represent on the Map.
	 */
	public String getVisibilityState(Unit target) {
		//- Locate the Unit inside the structure.
		String state = this.visibilityStates.get(target);
		if (null == state)
			return Unit.NOT_VISIBLE;
		else return state;
	}

	public void updateVisibilityState(Unit target, String visibilityState) {
		this.visibilityStates.put(target, visibilityState);
	}
}

// - UNUSED CODE ............................................................................................
// public void createTestUnits() {
// // - Clean the current model first.
// this.clear();
//
// // - Create a full defined battleship unit.
// ShipUnit unit = new ShipUnit();
// unit.setName("USS Alpha");
// unit.setSide(Unit.FRIEND);
// unit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());unit.setLongitude(new PolarCoordinate(0, 6,
// 30).toDegrees());
// // TODO Create a new speed for the ship.
// // TODO Instance a ship from the catalog of ship types.
// ShipType unitType = new ShipType("Frigate Class");
// unit.setType(unitType);
// unit.setSpeed(10);
// unit.setDirection(90.0);
// this.addChild(unit);
//		
// // - Create a FOE unit
// unit = new ShipUnit();
// unit.setName("URSS beta");
// unit.setSide(Unit.FOE);
// unit.setLatitude(new PolarCoordinate(51, 35, 30).toDegrees());unit.setLongitude(new PolarCoordinate(0, 3,
// 30).toDegrees());
// // TODO Create a new speed for the ship.
// // TODO Instance a ship from the catalog of ship types.
// unitType = new ShipType("Frigate Class");
// unit.setType(unitType);
// unit.setSpeed(10);
// unit.setDirection(90.0);
// this.addChild(unit);
//		
// // - Create a FRIEND Air unit.
// AirUnit aunit = new AirUnit();
// aunit.setName("F16 Gamma");
// aunit.setSide(Unit.FRIEND);
// aunit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());aunit.setLongitude(new PolarCoordinate(0,
// 12, 0).toDegrees());
// aunit.setSpeed(500);
// aunit.setDirection(100.0);
// this.addChild(aunit);
//		
// // - Create an air base
// AirportUnit airunit = new AirportUnit();
// airunit.setName("Air Base");
// airunit.setSide(Unit.FRIEND);
// airunit.setLatitude(new PolarCoordinate(51,32,0).toDegrees());
// airunit.setLongitude(new PolarCoordinate(0,3,0).toDegrees());
// this.addChild(airunit);
// }

// public Properties getMapProperties() {
// return this.props;
// }

// public void setMap(MapFigure map) {
// // TODO Auto-generated method stub
// this.map=map;
// }
//
// public HarpoonMap getMap() {
// // TODO Auto-generated method stub
// return this.map;
// }
