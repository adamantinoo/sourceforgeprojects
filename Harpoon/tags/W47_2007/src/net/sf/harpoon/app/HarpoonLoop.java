//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonLoop.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonLoop.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:50 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-11-16 10:50:02  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.3  2007-11-07 16:28:57  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.2  2007-11-02 09:35:04  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.4  2007-10-31 14:44:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1.2.3  2007-10-24 16:45:43  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.1.2.2  2007-10-23 15:55:17  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.1.2.1  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.AirportUnit;
import net.sf.harpoon.model.units.ISensorUnit;
import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.WarUnit;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.rcp.harpoon.editors.SceneryEditor;
import net.sourceforge.rcp.harpoon.editors.SceneryPage;
import net.sourceforge.rcp.harpoon.model.Scenery;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonLoop implements Runnable {
	private static Logger					logger			= Logger.getLogger("net.sourceforge");
	// static {
	// logger.setLevel(Level.ALL);
	// }
	private static final String		NOT_CACHED	= "NOT_CACHED";
	private static final String		UPDATED			= "UPDATED";
	private static final String		MODIFIED		= "MODIFIED";

	protected final String				name;																							// Process name
	// protected Scenery referenceScenery; // Application display artifact. Possible not needed
	protected Scenery							referenceScenery;																	// Referenced scenery
	// where to retrieve
	// unit data
	protected String							cacheState	= NOT_CACHED;
	protected Vector<Unit>				friendUnits;																				// Cache list of
	// friendly units
	protected Vector<Unit>				enemyUnits;																				// Cache list of any
	// other unit that it
	// is detectable
	protected Vector<MovableUnit>	movableUnits;																			// Cache list of any
	// other unit that it
	// is detectable
	protected HashMap<Unit, Unit>	fading;																						// Cache list of any

	// other unit that it
	// is detectable

	// - C O N S T R U C T O R S
	public HarpoonLoop(Scenery sce, String name) {
		referenceScenery = sce;
		this.name = name;
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Perform a single pass on the Enemy Detection Block and activate any enemies detected by the sensors that are
	 * active. Actions to be performed are:
	 * <ul>
	 * <li>Scan the list of units and get separate lists for friend War units, and enemy or other units.</li>
	 * <li>For each other unit, advance the time since it was detected, but only if it was detected.</li>
	 * <li>If this unit has been detected since at least 60 seconds, the move its state in the detection ladder.</li>
	 * <li>For any friend unit, get the sensors that have activated.</li>
	 * <li>If any unit has activated Radar sensors, scan the list of units for War units of Surface or Air type then are
	 * in range and then increment their detection state in the detection ladder.</li>
	 * <li>If any unit has the ECM sensor active, detect any unit that is Radio detectable (usually Airports).</li>
	 * </ul>
	 * <br>
	 * <br>
	 * <br>
	 * This loop is fired every 15 seconds on the exact minute seconds of 0, 15, 30 and 45. The actions to be performed on
	 * this periodic task loop are next:
	 * <ul>
	 * <li>Get the model units and classify them is the unit caches are empty or we have received a model change event.</li>
	 * <li>Match friend units with active sensors against other units to see if some detection event is generated. Update
	 * enemy uni state is detection is generated.</li>
	 * <li>If enemy units have moved to a undetected state, calculate the teoretical position area where the unit may
	 * have moved and update the model with this information.</li>
	 * <li>For each of the visible units calculate the new position from the movement algorithm. Compare this position
	 * with previous position and activate higlighting if the unit has moved.</li>
	 * </ul>
	 * 
	 */
	public void run() {
		logger.info(">>> Entering Processing Enemy Detection Loop");

		// - SECTION 01. Check if unit are cached. Otherwise separate and cache them.
		updateUnitData();

		// - SECTION 02. For every enemy unit check if it was detected.
		// updateEnemyDetection();
		// - SECTION 02. For every friend unit check if it detects an enemy with any of its sensors
		detectionPhase();

		// - SECTION 03. For every movable unit calculate the new position and register all units that visially move.
		movementPhase();
		logger.info("<<< Exiting Processing Enemy Detection Loop");
	}

	/**
	 * Checks the state of the unit cache. If some event has changed the number of elements on the model then we
	 * invalidate the caches and reprocess them again.
	 */
	protected void updateUnitData() {
		if (UPDATED.equals(cacheState)) return;
		// - Get the list of units and enemy units.
		final Iterator<Unit> it = referenceScenery.getModel().getChildren().iterator();
		friendUnits = new Vector<Unit>();
		enemyUnits = new Vector<Unit>();
		movableUnits = new Vector<MovableUnit>();
		while (it.hasNext()) {
			final Unit unit = it.next();
			// if (unit instanceof WarUnit) {
			// final WarUnit war = (WarUnit) unit;
			// }
			// if (unit instanceof Unit) {
			// final Unit thisUnit = (Unit) unit;
			if (Unit.FRIEND_SIDE == unit.getSide())
				friendUnits.add(unit);
			else
				enemyUnits.add(unit);
			if (unit instanceof MovableUnit) movableUnits.add((MovableUnit) unit);
		}// end while (it.hasNext())
		cacheState = UPDATED;
	}

	// protected void updateEnemyDetection() {
	// final Iterator<Unit> undetectedIt = enemyUnits.iterator();
	// while (undetectedIt.hasNext()) {
	// final Unit unit = undetectedIt.next();
	// final String state = unit.getDetectState();
	// if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
	// // - Add the unit to the list of units to be watched for fade out of the display
	// fading.put(unit, unit);
	// } else {
	// // - Unit is in the detection phase ladder.
	// logger.info("Enemy: " + unit.toString() + " goes undetected to " + state);
	// unit.degradeDetection();
	// }
	// }
	// }

	protected void detectionPhase() {
		logger.info(">>> Entering detection phase");
		final Iterator<Unit> it = referenceScenery.getModel().getChildren().iterator();
		while (it.hasNext()) {
			final Unit unit = it.next();
			// - Check it the unit is able to use sensors.
			if (unit instanceof ISensorUnit) {
				final ISensorUnit sensingUnit = (ISensorUnit) unit;
				logger.info("-- Detecting with unit " + sensingUnit.toString());
				// TODO Alternate implementation. Move the sensing code to the SensorInformation helper.
				// sensingUnit.runSensors();
				final SensorsModel sensors = sensingUnit.getSensorInformation();
				if (sensors.getRadarState()) radarDetectionPhase((Unit) sensingUnit, sensors);
				if (sensors.getRadioState()) radioEmissionDetectionPhase((Unit) sensingUnit, sensors);
				// - For each unit perform the TIMEELAPSED event to see it state changes
				sensingUnit.timeDetection();
			}
		}// end while (it.hasNext())
	}

	/** Radar detection tries to detect and identify Movable Surface and Movable Air units. */
	protected void radarDetectionPhase(Unit sensingUnit, SensorsModel sensor) {
		logger.info(">>> Entering RADAR detection phase");
		// - Detect any flying or surface unit with the radar
		final DMSPoint sourceLocation = sensingUnit.getLocation();
		final String sourceSide = sensingUnit.getSide();
		// final SensorsModel sensor = war.getSensorInformation();
		final int range = sensor.getSensorRange(HarpoonConstants.RADAR_TYPE);
		final Iterator<Unit> it = referenceScenery.getModel().getChildren().iterator();
		while (it.hasNext()) {
			final Unit unit = it.next();
			// [01]
			// - Detection with radar only applies to Movable units.
			if (unit instanceof MovableUnit) {
				if (!sourceSide.equals(unit.getSide())) {
					final MovableUnit detectableUnit = (MovableUnit) unit;
					final String type = detectableUnit.getUnitType();
					if ((HarpoonConstants.UNIT_SURFACE.equals(type)) || (HarpoonConstants.UNIT_AIR.equals(type))) {
						// - Get the locations of both elements.
						final DMSPoint targetLocation = detectableUnit.getLocation();
						final DMSPoint targetVector = targetLocation.offset(sourceLocation);
						if (targetVector.getModule() < range) ((ISensorUnit) sensingUnit).detectedEvent(detectableUnit);
					}
				}
			}
		}
	}

	// [02]
	/**
	 * ECM emission detection tries to detect and identify all units that are receiving commends. Units with their radar
	 * active are detected with this sensor.
	 */
	protected void radioEmissionDetectionPhase(Unit war, SensorsModel sensor) {
		logger.info(">>> Entering RARIO detection phase");
		// - Detect any unit using its radar
		final DMSPoint sourceLocation = war.getLocation();
		// final SensorsModel sensor = war.getSensorInformation();
		final int range = sensor.getSensorRange(HarpoonConstants.RADIO_TYPE);
		final Iterator<Unit> it = referenceScenery.getModel().getChildren().iterator();
		while (it.hasNext()) {
			final Unit unit = it.next();
			// - Detection with radar applies to war units with their radar active.
			if (unit instanceof WarUnit) {
				final WarUnit detectableUnit = (WarUnit) unit;
				final SensorsModel targetSensors = detectableUnit.getSensorInformation();
				final boolean radarState = targetSensors.getRadarState();
				if (radarState) {
					final DMSPoint targetLocation = detectableUnit.getLocation();
					final DMSPoint targetVector = targetLocation.offset(sourceLocation);
					if (targetVector.getModule() < range) ((ISensorUnit) war).detectedEvent(detectableUnit);
					// else
					// war.notDetectedEvent(detectableUnit);
				}
			}
			// - Or to airport units that also have sensors.
			if (unit instanceof AirportUnit) ((ISensorUnit) war).detectedEvent(unit);
		}
	}

	/**
	 * This phase iterates over all Movable units to move the location over the movement path it it exists. In cases where
	 * the path has completed, the unit has to continue moving on the same direction indefinitely.<br>
	 * The new location of the unit is calculated by the space run from the starting point that is the first movement path
	 * point plus the delta movement that is calculated as the current speed by the elapsed time since the last movement
	 * adjustment.<br>
	 * A movement adjustment happens when the user changes the movement path or when there is a change on the speed. At
	 * that time point the current location is recalculated and it becomes the initial movement point for next movement
	 * calculations.
	 */
	protected void movementPhase() {
		logger.info(">>> Entering MOVEMENT phase");
		final Iterator<MovableUnit> it = movableUnits.iterator();
		while (it.hasNext()) {
			final MovableUnit unit = it.next();
			// - Get start point, elapsed time and movement direction.
			// ReferencePoint start = unit.getStartMovementPoint();
			final DMSPoint startLocation = unit.getStartMovementPoint();
			final double elapsed = unit.elapsedMoveTime();
			if (elapsed <= 0.0) continue;
			final int direction = unit.getBearing();

			// - Adjust the angle to the base trigonometric coordinates.
			final double alpha = StrictMath.toRadians((360 - (direction - 90)) % 360);
			// - Calculate the space with the speed and the elapsed time. Use also the conversion to XY
			final SceneryEditor editor = (SceneryEditor) referenceScenery.getEditor();
			final SceneryPage page = editor.getMapPage();
			final GraphicalViewer viewer = page.getGraphicalViewer();
			final EditPart root = viewer.getContents();
			double space = 0.0;
			if (root instanceof RootMapPart) {
				final RootMapPart rootPart1 = (RootMapPart) root;
				final double speed = new Double(unit.getSpeed()).doubleValue();
				final Point spoint = RootMapPart.dms2xy(rootPart1, new DMSPoint(DMSCoordinate.fromSeconds(new Double(
						speed).longValue(), DMSCoordinate.LATITUDE), DMSCoordinate.fromSeconds(new Double(speed)
						.longValue(), DMSCoordinate.LONGITUDE)));
				space = StrictMath.abs(spoint.x * (elapsed / (60.0 * 60.0)));
			}
			// - Calculate movement projection on the latitude and longitude axis.
			final double latDelta = space * StrictMath.sin(alpha);
			final double lonDelta = space * StrictMath.cos(alpha);

			// - Calculate the new point from the start point.
			final DMSPoint lastLocation = unit.getLocation();
			final DMSPoint currentLocation = startLocation.translate(latDelta, lonDelta);

			// - Calculate the new XY location and compare it with the last location to check if moved
			// SceneryEditor editor = (SceneryEditor) referenceScenery.getEditor();
			// SceneryPage page = editor.getMapPage();
			// GraphicalViewer viewer = page.getGraphicalViewer();
			// EditPart root = viewer.getContents();
			if (root instanceof RootMapPart) {
				final RootMapPart rootPart = (RootMapPart) root;
				final Point prevXY = RootMapPart.dms2xy(rootPart, lastLocation);
				final Point newXY = RootMapPart.dms2xy(rootPart, currentLocation);
				if (!prevXY.equals(newXY)) {
					// - The object has moved on the presentation map
					logger.info("Unit " + unit.toString() + " has moved to new coordinates " + newXY.toString());
					// TODO Highlight the unit for 5 seconds
					// TODO Register previous point into trace line and change unit coordinates.
					unit.fireMovedEvent(currentLocation);
					// unit.addTrace(currentLocation);
					// unit.setLatitude(currentLocation.getDMSLatitude());
					// unit.setLongitude(currentLocation.getDMSLongitude());
				}
			}
		}
	}

	@Override
	public String toString() {
		return name + super.toString();
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// if (unit instanceof WarUnit) {
// final WarUnit enemy = (WarUnit) unit;
// final String type = enemy.getUnitType();
// if ((HarpoonConstants.SURFACE_UNIT.equals(type)) || (HarpoonConstants.AIR_UNIT.equals(type))) {
// // - Get the locations of both elements.
// final DMSPoint epoint = new DMSPoint(enemy.getDMSLatitude(), enemy.getDMSLongitude());
// final DMSPoint spoint = new DMSPoint(war.getDMSLatitude(), war.getDMSLongitude());
// final DMSVector enemyVector = new DMSVector(spoint, epoint);
// final int range = war.getRadarRange();
// if (enemyVector.getModule() < range) {
// // - This enemy is detected
// // HarpoonLogger.info("Enemy detected");
// HarpoonLogger.info("Enemy: " + enemy.toString() + " is being detected");
// enemy.fireDetection();
// }
// }
// }// end if (unit instanceof WarUnit)

// [02]
// /**
// * Radio emission detection detects any unit that is using the radio. Airports and neutral Air units are
// automatically
// * detected by this sensor.
// *
// * @param war
// * friendly unit that is using the detector
// */
// protected void radioEmissionDetectionPhase(WarUnit war) {
// logger.info(">>> Entering RARIO detection phase");
// // - Detect any airport or Neutral Air unit
// final Iterator<Unit> enemyIt = enemyUnits.iterator();
// while (enemyIt.hasNext()) {
// final Unit unit = enemyIt.next();
// if (unit instanceof AirportUnit) unit.upgradeDetection();
// if (unit instanceof MovableUnit) {
// final MovableUnit detectableUnit = (MovableUnit) unit;
// final String type = detectableUnit.getUnitType();
// if (HarpoonConstants.UNIT_AIR.equals(type)) {
// detectableUnit.upgradeDetection();
// }
// }
// }
// }
