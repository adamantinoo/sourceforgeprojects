//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/RootMapFigure.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:37 $
//  RELEASE:        $Revision: 1.13 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.12  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.11  2007-11-02 09:34:49  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.10.2.4  2007-10-31 14:47:35  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.10.2.3  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.10.2.2  2007-10-23 15:54:44  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.10.2.1  2007-10-18 16:53:41  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.10  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.9  2007-10-03 16:50:09  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.8  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.7  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.6  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.5  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.4  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.2  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-28 13:46:58  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import net.sf.harpoon.app.HarpoonColorConstants;
import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.WarUnit;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.parts.RootMapPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class RootMapFigure extends UnitFigure {
	/** This is the prefix to locate political maps in Google Map. */
	private static final String		GOOGLE_PREFIX				= "http://mt2.google.com/mt?";
	/** Those are the values for the size of the blocks of the images received from Google. */
	protected static final int		GOOGLE_BLOCK_HEIGHT	= 256;
	protected static final int		GOOGLE_BLOCK_WIDTH	= 256;

	// - L O C A L F I E L D S
	/** Represent the scenery properties that define some of the map presentation characteristics */
	protected Properties					mapProperties;
	protected RootMapPart					rootMapPart;
	/** Array structure where to store all map data read from the different URLs. */
	// protected ImageData[] mapDataArray = null;
	private final GoogleMapCache	mapCache						= new GoogleMapCache();

	// - C O N S T R U C T O R S
	/**
	 * Create the graphical elements that compose the figure. It is not drawn at this point, just initialized.
	 * The size represents a problem because the map has to fill the full editor area. That information will be
	 * the size to be used for this calculations. Contact the Editor panel to get the size.<br>
	 * The size can be supposed to the max extent of the screen, or the Display that is the greater element.
	 */
	public RootMapFigure() {
		// - Add a free form layout to this figure.
		setLayoutManager(new FreeformLayout());

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	/**
	 * Makes the additional figure data (the label contents) visible or invisible. This affects the enemy units
	 * that will not display this information until <code>IDENTIFIED</code>.
	 */
	public void setDataVisibility(final boolean visibility) {
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Receives the properties to configure the Map from the Model storage that where received from the scenery
	 * properties configuration file.
	 */
	public void setMapProperties(final Properties props) {
		mapProperties = props;
		mapCache.setConfiguration(props);
	}

	public RootMapPart getRootMap() {
		// TODO Auto-generated method stub
		return rootMapPart;
	}

	public void setRootMap(final RootMapPart rootMapPart) {
		this.rootMapPart = rootMapPart;
	}

	// public void setConfiguration(final Properties mapProperties) {
	// if (null == mapProperties)
	// props = new Properties();
	// else props = mapProperties;
	// // - Copy the properties to the cahe that is the class that will meke use of them.
	// mapCache.setConfiguration(props);
	// }
	// - O V E R R I D E - S E C T I O N
	@Override
	public Point getHotSpot() {
		return new Point(0, 0);
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		// final Dimension superSize = super.getPreferredSize(hint, hint2);
		final Display dis = Display.getCurrent();
		final Shell shell = dis.getActiveShell();
		final org.eclipse.swt.graphics.Point siz = shell.getSize();
		return new Dimension(siz);
	}

	/**
	 * Draw each of the map block until all the bounding are is covered with map data. Use the bounding to
	 * calculate the blocks to be read from the cache and to be copied to the display.
	 */
	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		// - If the figure has not received the properties (they are null) then initialize to blue screen
		if (true) {
			//			if ((null == mapProperties) || (0 == mapProperties.size())) {
			final Rectangle bounds = getBounds().getCopy();
			graphics.setForegroundColor(ColorConstants.darkGray);
			graphics.setBackgroundColor(ColorConstants.white);
			graphics.fillRectangle(0, 0, bounds.width, bounds.height);
		} else {
			// - Get the bounds to be painted. Calculate number of blocks for each axis.
			final Dimension areaSize = getSize();
			final int hBlocks = (areaSize.width + GOOGLE_BLOCK_WIDTH - 1) / GOOGLE_BLOCK_WIDTH;
			final int vBlocks = (areaSize.height + GOOGLE_BLOCK_HEIGHT - 1) / GOOGLE_BLOCK_HEIGHT;
			// - Copy to the canvas the map data that is stored in the cache.
			for (int j = 0; j < vBlocks; j++) {
				for (int i = 0; i < hBlocks; i++) {
					// - Get a block and paint it on the right location on the final display buffer.
					try {
						final Image newMap = new Image(Display.getCurrent(), mapCache.getBlock(i, j));
						final int x = GOOGLE_BLOCK_WIDTH * i;
						final int y = GOOGLE_BLOCK_HEIGHT * j;
						graphics.drawImage(newMap, x, y);
					} catch (final MalformedURLException mue) {
						// REVIEW Check if the removal of this lines generates errors on the presentation
						// final Rectangle bounds = getBounds().getCopy();
						// graphics.setForegroundColor(ColorConstants.darkGray);
						// graphics.setBackgroundColor(ColorConstants.white);
						// graphics.fillRectangle(0, 0, bounds.width, bounds.height);
					} catch (final IOException ioe) {
						// REVIEW Check if the removal of this lines generates errors on the presentation
						// final Rectangle bounds = getBounds().getCopy();
						// graphics.setForegroundColor(ColorConstants.darkGray);
						// graphics.setBackgroundColor(ColorConstants.white);
						// graphics.fillRectangle(0, 0, bounds.width, bounds.height);
					}
				}
			}
		}
		// - Paint the sensors that are active on any unit that is visible
		drawSensors(graphics);
		//		}
	}

	// - P R O T E C T E D - S E C T I O N
	/**
	 * Paint the sensors that are active on any unit that is visible
	 * 
	 * @param graphics
	 */
	protected void drawSensors(final Graphics graphics) {
		// - Get access to the list of units. These are the children of the RootMapPart
		final RootMapPart map = getRootMap();
		final RootMapUnit units = (RootMapUnit) map.getModel();
		final Iterator<Unit> it = units.getChildren().iterator();
		while (it.hasNext()) {
			final Object unit = it.next();
			if (unit instanceof WarUnit) {
				final WarUnit war = (WarUnit) unit;
				// TODO Simplify coordinate calculations
				// final Point loc = RootMapPart.convertToMap(getRootMap(), war);
				final Point loc = RootMapPart.dms2xy(getRootMap(), new DMSPoint(war.getDMSLatitude(), war
						.getDMSLongitude()));
				// )(getRootMap(), war);
				final SensorsModel sensors = war.getSensorInformation();
				int range;
				if (sensors.getRadarState()) {
					range = sensors.getSensorRange(HarpoonConstants.RADAR_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonConstants.COL_RADAR);
				}
				if (sensors.getSonarState()) {
					range = sensors.getSensorRange(HarpoonConstants.SONAR_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonConstants.COL_SONAR);
				}
				if (sensors.getRadioState()) {
					range = sensors.getSensorRange(HarpoonConstants.RADIO_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonColorConstants.ECM_COLOR);
				}
			}
		}
		// [01]
	}

	/**
	 * Draws the circle that matches the range selected.
	 * 
	 * @param graphics
	 * @param loc
	 * @param range
	 *          the range for the sensor for this unit at this moment. It is expressed in nautical miles.
	 * @param radarColor
	 *          the color to use to draw the range. The color is from the table of configured colors.
	 */
	protected void drawSensorRange(final Graphics graphics, final Point loc, final int range, final Color color) {
		// - Get the location where it is located the center of the sensor.
		// - This is the Unit location without biasing or the Figure location with biasing the hotspot.
		final Point location = loc.getCopy();
		final RootMapUnit modelRoot = (RootMapUnit) getRootMap().getModel();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();
		final int height = new Double(256 * range * 60.0 / latSpan).intValue();
		final int width = new Double(256 * range * 60.0 / lonSpan).intValue();
		location.x = location.x - width;
		location.y = location.y - height;
		final Rectangle bound = new Rectangle(location, new Dimension(width * 2, height * 2));
		graphics.setForegroundColor(color);
		graphics.drawOval(bound);
	}

	protected int getTopLatBlock() {
		// TODO Intercept NumberFormatExcetion exceptions or return the appropriate exception to the caller.
		final String propValue = mapProperties.getProperty("topLatBlock", "404");
		final Integer numberValue = new Integer(propValue);
		return numberValue.intValue();
	}

	protected String getTopLonBlock() {
		return mapProperties.getProperty("topLonBlock", "w2.60");
	}

	/** Obtain the Google URL reference to the piece of map. */
	protected String getURLReference(final int xBlock, final int yBlock) {
		// TODO Generate the URL based on the Google prefix and the internal configuration data for the scenery
		final StringBuffer url = new StringBuffer(GOOGLE_PREFIX);
		url.append("n=").append(getTopLatBlock());
		url.append("&v=").append(getTopLonBlock());
		url.append("&x=").append(xBlock);
		url.append("&y=").append(yBlock);
		url.append("&zoom=").append(getZoomFactor());
		return url.toString();
	}

	protected int getZoomFactor() {
		// TODO Intercept NumberFormatExcetion exceptions or return the appropriate exception to the caller.
		final String propValue = mapProperties.getProperty("zoomFactor", "11");
		final Integer numberValue = new Integer(propValue);
		return numberValue.intValue();
	}

	/**
	 * Map data information is kept in an array of <code>ImageData</code> instances. Each block is 256 x 256
	 * and there are lines and columns when gathering that information. The drawing for this information is
	 * performed with this cached data.<br>
	 * The data to be read depends on the coordinates for the top left block and the size of the canvas where we
	 * have to get the images. There is also a cache of some images su new access to GFogle are not necessary
	 * each time the user scrolls.<br>
	 * This sizes are adjusted every time the window is resized and the scroll buttons are touched.<br>
	 * <br>
	 * Reads a block of image data from Google and returns the first frame to the caller.
	 */
	protected ImageData readBlockData(final String imageName) throws MalformedURLException, IOException {
		// - Read the new image(s) from the chosen URL.
		final URL url = new URL(imageName);
		final InputStream stream = url.openStream();
		final ImageLoader loader = new ImageLoader();
		final ImageData[] blockData = loader.load(stream);
		stream.close();
		return blockData[0];
	}

	// - CLASS IMPLEMENTATION .................................................................................
	class GoogleMapCache {

		private Properties													properties		= new Properties();
		private final Hashtable<String, ImageData>	mapBlockCache	= new Hashtable<String, ImageData>();
		private int																	accesses			= 0;
		private int																	misses				= 0;
		private int																	hits					= 0;
		private int																	topX					= -1;
		private int																	topY					= -1;

		public void setConfiguration(final Properties props) {
			// TODO Auto-generated method stub
			properties = props;
			// - Clear already cached properties.
			topX = -1;
			topY = -1;
		}

		/**
		 * Gets a block of Google map data with coordinates relative to the initial start point of the scenery.
		 * The initial coordinates from the top-left block are defined on the properties. This will get a block
		 * <code>i</code> spaces to the right of this top-left corner and <code>j</code> spaces below that
		 * same block.
		 * 
		 * @throws IOException
		 * @throws MalformedURLException
		 */
		public ImageData getBlock(final int i, final int j) throws MalformedURLException, IOException {
			accesses++;
			// - Generate the URL to get the data form Google. This is also the cache key.
			// DEBUG Removed the load of images to reduce startup time
			String mapURL = "";
			if (true)
				mapURL = "http://mt0.google.com/mt?n=404&v=w2.61&x=14&y=10&zoom=12";
			else mapURL = getURLReference(getTopX() + i, getTopY() + j);
			ImageData block = mapBlockCache.get(mapURL);
			if (null == block) {
				// - Miss in the cache data. Got to Google for the information.
				misses++;
				block = readBlockData(mapURL);
				mapBlockCache.put(mapURL, block);
			} else hits++;
			return block;
		}

		private int getTopX() {
			if (-1 == topX) {
				final String propValue = properties.getProperty("topX", "52");
				final Integer numberValue = new Integer(propValue);
				try {
					topX = numberValue.intValue();
				} catch (final NumberFormatException nfe) {
					topX = 0;
				}
			}
			return topX;
		}

		private int getTopY() {
			if (-1 == topY) {
				final String propValue = properties.getProperty("topY", "26");
				final Integer numberValue = new Integer(propValue);
				try {
					topY = numberValue.intValue();
				} catch (final NumberFormatException nfe) {
					topY = 0;
				}
			}
			return topY;
		}

		@Override
		public String toString() {
			return "GoogleMapCache stats; (" + accesses + "," + hits + "," + misses + ")";
		}

	}
}

// - UNUSED CODE ............................................................................................
// @Override
// protected void paintChildren(Graphics graphics) {
// // TODO Auto-generated method stub
// super.paintChildren(graphics);
// }

// [01]
// // - For every sensor check if it detects an enemy
// final Iterator<WarUnit> warIt = warUnits.iterator();
// while (warIt.hasNext()) {
// final WarUnit war = warIt.next();
// final Point loc = RootMapPart.convertToMap(getRootMap(), war);
// // - Get the sensors that are active
// final SensorsModel sensors = war.getSensorInformation();
// int range;
// if (sensors.getRadarState()) {
// // - Get radar range from the model data
// range = war.getSensorRange(HarpoonConstants.RADAR_TYPE);
// drawSensorRange(graphics, loc, range, HarpoonConstants.COL_RADAR);
// }
// if (sensors.getSonarState()) {
// // - Get radar range from the model data
// range = war.getSensorRange(HarpoonConstants.SONAR_TYPE);
// drawSensorRange(graphics, loc, range, HarpoonConstants.COL_SONAR);
// }
// if (sensors.getRadioState()) {
// // - Get radar range from the model data
// range = war.getSensorRange(HarpoonConstants.RADIO_TYPE);
// drawSensorRange(graphics, loc, range, HarpoonColorConstants.ECM_COLOR);
// }
// // Iterator<WarUnit> enemyIt = enemyUnits.iterator();
// // while (enemyIt.hasNext()) {
// // WarUnit enemy = enemyIt.next();
// // // - Get the locations of both elements.
// // DMSPoint epoint = new DMSPoint(enemy.getDMSLatitude(), enemy.getDMSLongitude());
// // DMSPoint spoint = new DMSPoint(sensor.getReferent().getDMSLatitude(), sensor.getReferent()
// // .getDMSLongitude());
// // DMSVector v = new DMSVector(spoint, epoint);
// // int range = sensor.getRange();
// // if (v.getModule() < range) {
// // // - This enemy is detected
// // HarpoonLogger.info("Enemy detected");
// // // enemy.setDetected(true);
// // }
// }
