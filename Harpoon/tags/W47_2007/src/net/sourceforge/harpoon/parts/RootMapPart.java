//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/RootMapPart.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:38 $
//  RELEASE:        $Revision: 1.14 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.13  2007-11-02 09:34:51  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.12.2.7  2007-10-31 14:47:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.12.2.6  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.12.2.5  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.12.2.4  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.12.2.3  2007-10-16 14:47:14  ldiego
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.12.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.12.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.12  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.11  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.10  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.9  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.8  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.7  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.6  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.5  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.AutomaticRouter;
import org.eclipse.draw2d.BendpointConnectionRouter;
import org.eclipse.draw2d.ConnectionLayer;
import org.eclipse.draw2d.FanRouter;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.editpolicies.RootComponentEditPolicy;
import org.eclipse.gef.editpolicies.XYLayoutEditPolicy;
import org.eclipse.gef.requests.ChangeBoundsRequest;
import org.eclipse.gef.requests.CreateRequest;

import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.figures.RootMapFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This is the controller of the root model element implemented as the Map. With the inheritance chain we only
 * have to implement the key methods <code>createFigure()</code> and <code>getModelChildren()</code> if
 * this part is a container.
 */
public class RootMapPart extends GamePart implements PropertyChangeListener {
	private static Logger	logger						= Logger.getLogger("net.sf");
	/**
	 * Identifies the layer containing connections, which typically appear on top of anything in the primary
	 * layer.
	 */
	private final String	CONNECTION_LAYER	= "Connection Layer";				//$NON-NLS-1$

	// - C O N S T R U C T O R S

	/** Initialize this Part to set the default router for the movement path connections. */
	public RootMapPart() {
		// // - Set the routing by default
		// final ConnectionLayer cLayer = (ConnectionLayer) getLayer(CONNECTION_LAYER);
		// // DEBUG Try is the FanRouter can be removed altogether
		// final AutomaticRouter router = new FanRouter();
		// router.setNextRouter(new BendpointConnectionRouter());
		// cLayer.setConnectionRouter(router);
	}

	// - S T A T I C - S E C T I O N
	/**
	 * Global function to convert DMS coordinates of a unit to the graphicalXY coordinates. This code is being
	 * deprecated and substituted by more generic functions.
	 * 
	 * @deprecated
	 */
	public static Point convertToMap(final RootMapPart rootPart, final Unit model) {
		// - Get the unit coordinates and the top-left coordinates.
		final DMSCoordinate unitLat = model.getDMSLatitude();
		final DMSCoordinate unitLon = model.getDMSLongitude();

		// - Get the model root where there are stored the conversion properties.
		// final RootMapPart rootPart = (RootMapPart) getParent();
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint point = new DMSPoint(topLat, topLon);
		final DMSPoint vector = point.offset(new DMSPoint(unitLat, unitLon));

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getDMSLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getDMSLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	/**
	 * Global function to convert DMS coordinates of a point to the graphicalXY coordinates. This code is being
	 * deprecated and substituted by more generic functions.
	 * 
	 * @deprecated
	 */
	public static Point coordinateToMap(final RootMapPart rootPart, final DMSPoint to) {
		// // - Get the unit coordinates and the top-left coordinates.
		// final DMSCoordinate unitLat = to.getLatitude();
		// final DMSCoordinate unitLon = to.getLongitude();
		//
		// - Get the model root where there are stored the conversion properties.
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint from = new DMSPoint(topLat, topLon);
		final DMSPoint vector = from.offset(to);

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getDMSLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getDMSLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	/**
	 * Global function to convert DMS coordinates to the graphical XY coordinates. The code requires a GamePart
	 * to locate the RootMapPart. This has to be changed in new revisions to not require passing that reference.
	 */
	public static Point dms2xy(final GamePart part, final DMSPoint dmsLocation) {
		// FIXME Remove the need to pass a Part to get access to the RootMapPart
		// - Get the model root where there are stored the conversion properties.
		final RootMapUnit modelRoot = (RootMapUnit) part.getRootPart().getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint origin = new DMSPoint(topLat, topLon);
		final DMSPoint vector = dmsLocation.offset(origin);

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getDMSLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getDMSLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	/**
	 * Global function to convert graphical XY to the DMS coordinates. The code requires a GamePart to locate
	 * the RootMapPart. This has to be changed in new revisions to not require passing that reference.
	 */
	public static DMSPoint xy2dms(final GamePart part, final Point graphicLocation) {
		// FIXME Remove the need to pass a Part to get access to the RootMapPart
		// - Get the model root where there are stored the conversion properties.
		final RootMapUnit modelRoot = (RootMapUnit) part.getRootPart().getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Calculate new zero based latitude and longitude expressed in seconds.
		double lat = (new Double(graphicLocation.y).doubleValue() * latSpan) / 256.0;
		double lon = (new Double(graphicLocation.x).doubleValue() * lonSpan) / 256.0;
		lat -= topLat.toSeconds();
		lon -= topLon.toSeconds();
		final DMSCoordinate latitude = DMSCoordinate.fromSeconds(new Double(lat).longValue(),
				DMSCoordinate.LATITUDE);
		final DMSCoordinate longitude = DMSCoordinate.fromSeconds(new Double(lon).longValue(),
				DMSCoordinate.LONGITUDE);

		return new DMSPoint(latitude, longitude);
	}

	public static DMSPoint xy2dms(final RootMapUnit modelRoot, final Point graphicLocation) {
		// FIXME Remove the need to pass a Part to get access to the RootMapPart
		// - Get the model root where there are stored the conversion properties.
		//		final RootMapUnit modelRoot = (RootMapUnit) part.getRootPart().getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Calculate new zero based latitude and longitude expressed in seconds.
		double lat = (new Double(graphicLocation.y).doubleValue() * latSpan) / 256.0;
		double lon = (new Double(graphicLocation.x).doubleValue() * lonSpan) / 256.0;
		lat -= topLat.toSeconds();
		lon -= topLon.toSeconds();
		final DMSCoordinate latitude = DMSCoordinate.fromSeconds(new Double(lat).longValue(),
				DMSCoordinate.LATITUDE);
		final DMSCoordinate longitude = DMSCoordinate.fromSeconds(new Double(lon).longValue(),
				DMSCoordinate.LONGITUDE);

		return new DMSPoint(latitude, longitude);
	}

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	public void propertyChange(final PropertyChangeEvent evt) {
		final String propChanged = evt.getPropertyName();
		// if (Unit.CHILDREN.equals(propChanged)) {
		// // - There is a change on the children structure. Detect if we are adding or removing am element.
		// if (evt.getOldValue() instanceof PropertyModel) {
		// removeChild((EditPart) getViewer().getEditPartRegistry().get(evt.getOldValue()));
		// } else {
		// // this.createChild(evt.getNewValue());
		// addChild(createChild(evt.getNewValue()), children.size());
		// }
		// }
		if (RootMapUnit.PROPERTIES.equals(propChanged)) {
			// - Copy the model properties to the figure properties.
			final RootMapUnit model = (RootMapUnit) getModel();
			final RootMapFigure fig = (RootMapFigure) getFigure();
			fig.setMapProperties(model.getMapProperties());
		}

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Currently I have to get access to the Map that is the already created figure because it contains some of
	 * the model data mixed with the presentation data.<br>
	 * This figure is costly to be created many times. Now just create it every time we have the part requested
	 * that now is just a single time.<br>
	 * <br>
	 * The creation of the Figure can be generalized for most EditParts. Most of the parts get the model
	 * information to set the initial visual Figure properties (that most of the time are the <code>name</code>
	 * and the <code>side</code>). The Figure class can be intantiated by indirectly so all this standard
	 * code can be refactored to the base <code>UnitPart</code> class.<br>
	 * <br>
	 * Another complicated thing to be reviewed if the way to communicate the scenery properties. This version
	 * gets the properties from the PartFactory that received the editor as a parameter and it is accessible
	 * statically. This has been changed to get the properties from the Model piece that is more easily
	 * configured.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	public IFigure createFigure() {
		// - Create the presentation figure and initialize it with the scenery properties.
		final RootMapUnit model = (RootMapUnit) getModel();
		logger.log(Level.FINE, "Creating Figure MapFigure");
		final RootMapFigure fig = new RootMapFigure();
		// - For the root there is no need to set the side. The name id the scenery name and it it got form the
		// properties.
		fig.setMapProperties(model.getMapProperties());
		fig.setRootMap(this);
		return fig;
	}

	/**
	 * Create this figure before any initialization to give all other units a parent root unit that is valid and
	 * does not generate Exceptions.
	 */
	public void createEmptyFigure() {
		setFigure(new RootMapFigure());
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Define the EditPolicies that are applicable to this Part. The current definition install a default policy
	 * to disallow the removal of this part from the diagram by a delete command because this is the root part.
	 * Then also removed the selection feedback and then installs a policy to enable the selection and movement
	 * of some map elements like the movement path ReferencePoints.
	 * 
	 * @see net.sourceforge.harpoon.parts.GamePart#createEditPolicies()
	 */
	@Override
	protected void createEditPolicies() {
		// super.createEditPolicies();
		// TODO Change this to get all Policy from a policy factory.
		// - Disallows the removal of this edit part from its parent
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new RootComponentEditPolicy());
		// - Removes selection feedback from the top map
		installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, null);
		// - Handles constraint changes (e.g. moving and/or resizing) of model elements
		// installEditPolicy(EditPolicy.LAYOUT_ROLE, new ShapesXYLayoutEditPolicy());
		// installEditPolicy(EditPolicy.LAYOUT_ROLE, new ReferencePartNonResizableEditPolicy());
	}

	/**
	 * Return the model children that have to be assigned EditParts. Some model hierarchy elements are not
	 * generated as sub-parts of the equivalent model part but as children of the main root part. This is
	 * performed scanning the hierarchy element by element. MovableUnits add to this hierarchy level all the
	 * ReferencePoints that define their movement path.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#getModelChildren()
	 */
	@Override
	public List<Object> getModelChildren() {
		final List<Object> allChildren = new Vector<Object>();
		// - Change generation to include sub-model elements.
		// final Unit model = (Unit) getModel();

		// - This is the list of first level objects. Iterator to create the complete list.
		final Iterator<Unit> it = ((RootMapUnit) getModel()).getChildren().iterator();
		while (it.hasNext()) {
			final Unit unit = it.next();
			allChildren.add(unit);
			// - Add any dependent children to this build up list. Those are the MovementPath and the Trace.
			if (unit instanceof MovableUnit) {
				// final MovableUnit movable = (MovableUnit) unit;
				// final Vector<Object> children = movable.getMovementPath().getModelChildren();
				allChildren.addAll(((MovableUnit) unit).getControllableChilds());
			}
		}
		return allChildren;
	}

	@Override
	public void refresh() {
		super.refresh();
		//- Refresh all the ReferencePoints found on the children list.
		//		 RootMapUnit mod = getCastedModel();
		//		Iterator<Unit> it = getCastedModel().getChildren().iterator();
		Iterator<Object> it = children.iterator();
		while (it.hasNext()) {
			AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) it.next();
			if (part instanceof ReferencePart) {
				part.refresh();
			}
			if (part instanceof WirePart) {
				part.refresh();
			}
		}
	}

	/**
	 * This message is received when the Part is created or when the modification of the properties requires
	 * that the visual representation of this model reference have to be updated.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();

		// - Set the routing by default
		final ConnectionLayer cLayer = (ConnectionLayer) getLayer(CONNECTION_LAYER);
		// DEBUG Try is the FanRouter can be removed altogether
		final AutomaticRouter router = new FanRouter();
		router.setNextRouter(new BendpointConnectionRouter());
		cLayer.setConnectionRouter(router);

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	protected RootMapUnit getCastedModel() {
		return (RootMapUnit) getModel();
	}

	/**
	 * Common method to generate the information that can be seen on the debug window mainly.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractEditPart#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[RootMapPart:");
		// TODO Create a selection decode method to print string values
		buffer.append(getSelected()).append("-");
		buffer.append(getCastedModel().getClass().getSimpleName()).append("-");
		buffer.append(getCastedModel().getName()).append("-");
		buffer.append(getFigure().getClass().getSimpleName()).append("");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}

	public void deleteWire(WirePart thewire) {
		// TODO Auto-generated method stub
		int index = this.children.indexOf(thewire);
	}

	// [01]
}

// - CLASS IMPLEMENTATION.................................................................................
class ReferencePartNonResizableEditPolicy extends NonResizableEditPolicy {
	protected Command getMoveCommand(final ChangeBoundsRequest request) {
		return new MoveReferencePartCommand("MOVE Selected ReferenceParts", request);
	}

	// - CLASS IMPLEMENTATION.................................................................................
	private class MoveReferencePartCommand extends Command {
		private static final String											UNINITIALIZED				= "UNINITIALIZED";
		private static final String											UNDO_COMMITTED			= "UNDO_COMMITTED";
		private static final String											UNDO_INCOMPLETE			= "UNDO_INCOMPLETE";
		private static final String											EXECUTION_COMPLETED	= "EXECUTION_COMPLETED";
		private String																	executionState			= UNINITIALIZED;
		private final ChangeBoundsRequest								request;
		private final HashMap<ReferencePart, DMSPoint>	undoMap							= new HashMap<ReferencePart, DMSPoint>();

		public MoveReferencePartCommand(final String label, final ChangeBoundsRequest request) {
			super(label);
			this.request = request;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public boolean canUndo() {
			return hasExecuted();
		}

		@Override
		public void execute() {
			super.execute();
			// - Set the new coordinates of the ReferencePoint to the new data.
			final List<Object> parts = request.getEditParts();
			final Iterator<Object> it = parts.iterator();
			final Point delta = request.getMoveDelta();
			while (it.hasNext()) {
				final Object part = it.next();
				if (part instanceof ReferencePart) {
					final ReferencePart reference = (ReferencePart) part;
					final DMSPoint actualData = reference.move(delta);
					undoMap.put(reference, actualData);
				}
			}
			executionState = EXECUTION_COMPLETED;
		}

		@Override
		public void undo() {
			// - Set the coordinates of the ReferencePoint back to the original saved data
			final Iterator<ReferencePart> it = undoMap.keySet().iterator();
			while (it.hasNext()) {
				final ReferencePart reference = it.next();
				if (reference.isActive()) {
					reference.move(undoMap.get(reference));
				} else {
					executionState = UNDO_INCOMPLETE;
				}
			}
			executionState = UNDO_COMMITTED;
			super.undo();
		}

		private boolean hasExecuted() {
			if (EXECUTION_COMPLETED.equals(executionState))
				return true;
			else return false;
		}
	}
}

class ShapesXYLayoutEditPolicy extends XYLayoutEditPolicy {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(ChangeBoundsRequest, EditPart, Object)
	 */
	protected Command createChangeConstraintCommand(final ChangeBoundsRequest request, final EditPart child,
			final Object constraint) {
		// if ((child instanceof MovablePart) && (constraint instanceof Rectangle)) {
		if (child instanceof ReferencePart) {
			// return a command that can move and/or resize a Shape
			return new MoveReferencePartCommand2("MOVE Selected ReferenceParts", request, (Rectangle) constraint);
		}
		return super.createChangeConstraintCommand(request, child, constraint);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ConstrainedLayoutEditPolicy#createChangeConstraintCommand(EditPart, Object)
	 */
	protected Command createChangeConstraintCommand(final EditPart child, final Object constraint) {
		// not used in this example
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see LayoutEditPolicy#getCreateCommand(CreateRequest)
	 */
	protected Command getCreateCommand(final CreateRequest request) {
		// Object childClass = request.getNewObjectType();
		// if (childClass == EllipticalShape.class || childClass == RectangularShape.class) {
		// // return a command that can add a Shape to a ShapesDiagram
		// return new ShapeCreateCommand((Shape)request.getNewObject(),
		// (ShapesDiagram)getHost().getModel(), (Rectangle)getConstraintFor(request));
		// }
		return null;
	}

	private class MoveReferencePartCommand2 extends Command {
		private static final String											UNINITIALIZED				= "UNINITIALIZED";
		private static final String											UNDO_COMMITTED			= "UNDO_COMMITTED";
		private static final String											UNDO_INCOMPLETE			= "UNDO_INCOMPLETE";
		private static final String											EXECUTION_COMPLETED	= "EXECUTION_COMPLETED";
		private String																	executionState			= UNINITIALIZED;
		private final ChangeBoundsRequest								request;
		private final HashMap<ReferencePart, DMSPoint>	undoMap							= new HashMap<ReferencePart, DMSPoint>();

		public MoveReferencePartCommand2(final String label, final ChangeBoundsRequest request,
																			final Rectangle rectangle) {
			super(label);
			this.request = request;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public boolean canUndo() {
			return hasExecuted();
		}

		@Override
		public void execute() {
			super.execute();
			// - Set the new coordinates of the ReferencePoint to the new data.
			final List<Object> parts = request.getEditParts();
			final Iterator<Object> it = parts.iterator();
			final Point delta = request.getMoveDelta();
			while (it.hasNext()) {
				final Object part = it.next();
				if (part instanceof ReferencePart) {
					final ReferencePart reference = (ReferencePart) part;
					final DMSPoint actualData = reference.move(delta);
					undoMap.put(reference, actualData);
				}
			}
			executionState = EXECUTION_COMPLETED;
		}

		@Override
		public void undo() {
			// - Set the coordinates of the ReferencePoint back to the original saved data
			final Iterator<ReferencePart> it = undoMap.keySet().iterator();
			while (it.hasNext()) {
				final ReferencePart reference = it.next();
				if (reference.isActive()) {
					reference.move(undoMap.get(reference));
				} else {
					executionState = UNDO_INCOMPLETE;
				}
			}
			executionState = UNDO_COMMITTED;
			super.undo();
		}

		private boolean hasExecuted() {
			if (EXECUTION_COMPLETED.equals(executionState))
				return true;
			else return false;
		}
	}
}

// - UNUSED CODE ............................................................................................
// @Override
// protected void addChildVisual(EditPart childEditPart, int index) {
// super.addChildVisual(childEditPart, index);
// }
//
// @Override
// protected void removeChildVisual(EditPart childEditPart) {
// super.removeChildVisual(childEditPart);
// }

// @Override
// public DragTracker getDragTracker(Request arg0) {
// return null;
// }
// public Object getAdapter(Class key) {
// //if (AccessibleEditPart.class == key)
// return getAccessibleEditPart();
// //return Platform.getAdapterManager().getAdapter(this, key);
// }
// getModelChildren()
// @Override
// protected void addChildVisual(EditPart part, int index) {
// }
// @Override
// protected void removeChildVisual(EditPart arg0) {
// }
