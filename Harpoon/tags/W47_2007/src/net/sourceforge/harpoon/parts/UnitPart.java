//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/UnitPart.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:38 $
//  RELEASE:        $Revision: 1.18 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.17  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.16  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.15  2007-11-02 09:34:51  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.14.2.4  2007-10-31 14:47:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.14.2.3  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.14.2.2  2007-10-22 08:12:07  ldiego
//    - Changed some log labels to INFO for filtering.
//
//    Revision 1.14.2.1  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.14  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.13  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.12  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.11  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.10  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.9  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.8  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.7  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.DetectionModel;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitPart extends GamePart {
	private static Logger	logger	= Logger.getLogger("net.sourceforge");

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * The mechanism is not clearly explained. This is the way to receive that there have been changes to the
	 * model properties. This implementation intercepts event of type <em><code>Notification.SET</code></em>
	 * to trigger the change on the content of one of the model properties so new updates to the presentation
	 * take the new data from the model.<br>
	 * This implementation does not delegate other event to the super class implementation because no other
	 * class implements this interface.
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		//[02]
		//		if (Unit.NAME.equals(prop)) refreshVisuals();
		if (Unit.SIDE.equals(prop)) {
			// - The references to the model and figure objects.
			final UnitFigure fig = (UnitFigure) getFigure();
			final Unit model = getCastedModel();
			fig.setSide(model.getSide());
		}
		if (Unit.LATITUDE.equals(prop))
			refreshVisuals();
		if (Unit.LONGITUDE.equals(prop))
			refreshVisuals();
		if (Unit.LOCATION_PROP.equals(prop))
			refreshVisuals();
		if (Unit.DETECTIONCHANGE_PROP.equals(prop)) {
			//- Message a visibility change to the Map
			String visibilityState = DetectionModel.convertFromDetectionState(getCastedModel().getSide(),
					(String) evt.getNewValue());
			this.getRootPart().getCastedModel().updateVisibilityState(getCastedModel(), visibilityState);
			refreshVisuals();
		}
		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Updates the layout data for this object. The layout belongs to the Map root element that is associated to
	 * this Part when created by setting the <code>parent</code> property.<br>
	 * It also updates the visible parts of the element that may have changed by the change of a property.<br>
	 * <br>
	 * This is a EditPart, so to get the location we have to access the model and the global coordinate system.<br>
	 * The method calculates the location in XY coordinates for the model DMS coordinates. then moves that point
	 * to the hot spot of the figure because model location is not mapped to the top-left point of the figure.<br>
	 * <br>
	 * All parts have reference to the root part that also have reference to the top model element. We can get
	 * the top-left location from this chain and then calculate the XY point using that reference.
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final UnitFigure fig = (UnitFigure) getFigure();
		final Unit model = getCastedModel();

		// - Update other unit attributes
		fig.setSide(model.getSide());
		// [01]

		//- The changes in the display come from the values of the model and do not take decisions.
		refreshVisualState();
		logger.log(Level.INFO, "Using unit:" + model.toString());
		logger.log(Level.INFO, "UnitPart data:" + toString());
		logger.log(Level.INFO, "Unit DMS latitude: " + model.getDMSLatitude());
		logger.log(Level.INFO, "Unit DMS longitude: " + model.getDMSLongitude());

		// - This block calculates the coordinates XY for the DMS model location.
		final Point xyloc = RootMapPart.dms2xy(getRootPart(), model.getLocation());
		logger.log(Level.INFO, "Calculated XY location: " + xyloc);

		// - Move the point depending on the figure hotspot location.
		final Point hotspot = fig.getHotSpot();
		logger.log(Level.INFO, "Obtained hotspot difference: " + hotspot);
		xyloc.x -= hotspot.x;
		xyloc.y -= hotspot.y;

		final Dimension size = fig.getSize();
		logger.log(Level.INFO, "Calculated figure size: " + size);
		final Rectangle bound = new Rectangle(xyloc, size);
		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bound);
		fig.repaint();

		// - This triggers the parent layout to revalidate the position of the element.
		((AbstractGraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), bound);
	}

	/**
	 * This function updates the presentation visibility depending on the model visibility values that are
	 * calculated on the detection procedure and the other user interface actions.
	 */
	protected void refreshVisualState() {
		// - The references to the model and figure objects.
		final UnitFigure fig = (UnitFigure) getFigure();
		final Unit model = getCastedModel();
		String state = this.getRootPart().getCastedModel().getVisibilityState(model);

		//		// - Be sure that FRIEND units are visible
		//		if (Unit.FRIEND_SIDE.equals(model.getSide())) {
		//			fig.setVisible(true);
		//		} else {
		//			if (fig instanceof MovableFigure) {
		//				final MovableFigure movable = (MovableFigure) fig;
		//				// - Enemy units visibility depend on the detection state. Like other units.
		//				final String state = model.getVisibilityState();
		if (Unit.NOT_VISIBLE.equals(state)) {
			fig.setVisible(false);
		}
		if (Unit.CONTACT_VISIBLE.equals(state)) {
			fig.setVisible(true);
			fig.setDataVisibility(false);
			fig.setColorIntensity(HarpoonConstants.LOWLIGHT_INTENSITY);
		}
		if (Unit.IDENTIFICATION_VISIBLE.equals(state)) {
			fig.setVisible(true);
			fig.setDataVisibility(true);
			fig.setColorIntensity(HarpoonConstants.NORMAL_INTENSITY);
		}
		if (Unit.FULL_VISIBLE.equals(state)) {
			fig.setVisible(true);
			fig.setDataVisibility(true);
			fig.setColorIntensity(HarpoonConstants.NORMAL_INTENSITY);
		}
		//			}
		//		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[UnitPart:");
		buffer.append(getSelectedDecode()).append("-");
		buffer.append(getCastedModel().getName()).append("-");
		buffer.append(getFigure().getClass().getSimpleName()).append("]");
		return buffer.toString();
	}

	// - P R O T E C T E D - S E C T I O N
	protected Unit getCastedModel() {
		return (Unit) getModel();
	}

	protected String getSelectedDecode() {
		if (getSelected() == EditPart.SELECTED_NONE)
			return "SELECTED_NONE";
		if (getSelected() == EditPart.SELECTED)
			return "SELECTED";
		if (getSelected() == EditPart.SELECTED_PRIMARY)
			return "SELECTED_PRIMARY";
		return "UNDEFINED_VALUE";
	}

}
// - UNUSED CODE ............................................................................................
// [01]
// if (Unit.FRIEND_SIDE == model.getSide()) fig.setVisible(true);
// else {
// fig.setVisible(false);
// if (model.isDetected()) fig.setVisible(true);
// }

//[02]
// if (Unit.CHILDREN.equals(prop))
// refreshChildren();
// else
