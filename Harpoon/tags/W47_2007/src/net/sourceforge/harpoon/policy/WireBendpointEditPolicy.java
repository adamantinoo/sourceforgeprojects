//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WireBendpointEditPolicy.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/policy/WireBendpointEditPolicy.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:38 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
/*************************************************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others. All rights reserved. This program and the accompanying
 * materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this
 * distribution, and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 ************************************************************************************************************/
//  LOG:
//    $Log: not supported by cvs2svn $
//    revision 1.2
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    revision 1.1.2.2
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    revision 1.1.2.1
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
package net.sourceforge.harpoon.policy;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.BendpointEditPolicy;
import org.eclipse.gef.requests.BendpointRequest;

import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.WireBendpoint;
import net.sourceforge.harpoon.model.WireEndPoint;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.harpoon.parts.WirePart;

// - CLASS IMPLEMENTATION ...................................................................................
public class WireBendpointEditPolicy extends BendpointEditPolicy {
	// - A B S T R A C T - S E C T I O N
	protected Command getCreateBendpointCommand(final BendpointRequest request) {
		//- Get the elements that we need to add a point to a MovementPath
		WirePart part = (WirePart) request.getSource();
		RootMapPart root = part.getRootPart();
		Wire theWire = (Wire) part.getModel();
		WireEndPoint source = theWire.getSource();
		MovementPath pathOwner = source.getOwner();
		//		//- If the owner if null then the source point is a unit
		//		if(null==pathOwner)pathOwner=source.

		//- Convert to DMS coordinates to create and assign them to the new ReferencePoint
		Point xyPoint = request.getLocation();
		//- Change the sign of the latitude to adjust to our quadrant
		xyPoint.y = -xyPoint.y;
		DMSPoint location = RootMapPart.xy2dms((RootMapUnit) root.getModel(), xyPoint);

		//- Create and initialize the point.
		CreateReferencePointCommand refCommand = new CreateReferencePointCommand();
		refCommand.setWire(theWire);
		refCommand.setDMSLocation(location);
		refCommand.setOwner(pathOwner);

		//- Locate the point index in the path.
		int index = pathOwner.getPoints().indexOf(source);
		//		int reqIndex = request.getIndex();
		refCommand.setIndex(index);
		return refCommand;
	}

	protected Command getMoveBendpointCommand(final BendpointRequest request) {
		final MoveBendpointCommand com = new MoveBendpointCommand();
		final Point p = request.getLocation();
		final Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);

		final Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		final Point ref2 = getConnection().getTargetAnchor().getReferencePoint();

		conn.translateToRelative(ref1);
		conn.translateToRelative(ref2);

		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	protected Command getDeleteBendpointCommand(final BendpointRequest request) {
		final BendpointCommand com = new DeleteBendpointCommand();
		final Point p = request.getLocation();
		com.setLocation(p);
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}
	//		/**
	//		 * Shows feedback when a bendpoint is being created. The original figure is used for feedback and the
	//		 * original constraint is saved, so that it can be restored when feedback is erased.
	//		 * 
	//		 * @param request
	//		 *          the BendpointRequest
	//		 */
	//		protected void showCreateBendpointFeedback(final BendpointRequest request) {
	//			try {
	//				// final Connection con = getConnection();
	//				final List constraint = (List) getConnection().getRoutingConstraint();
	//				if (null == constraint) {
	//					final int dummy = 1;
	//					getConnection().setRoutingConstraint(new ArrayList());
	//				}
	//				final List constraint2 = (List) getConnection().getRoutingConstraint();
	//				// final List constraint2 = (List) getConnection().getRoutingConstraint();
	//				// // final int ind = request.getIndex();
	//				// final ArrayList constraint = new ArrayList();
	//				// constraint.add(request.getLocation());
	//				// con.setRoutingConstraint(cons);
	//				super.showCreateBendpointFeedback(request);
	//			} catch (final Exception e) {
	//				e.printStackTrace();
	//			}
	//		}
}

//- CLASS IMPLEMENTATION ...................................................................................
class CreateBendpointCommand extends BendpointCommand {

	public void execute() {
		//		return;
		final WireBendpoint wbp = new WireBendpoint();
		Dimension start = getFirstRelativeDimension();
		Dimension end = getSecondRelativeDimension();
		wbp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		getWire().insertBendpoint(getIndex(), wbp);
		super.execute();
	}

	public void undo() {
		super.undo();
		getWire().removeBendpoint(getIndex());
	}

}

//- CLASS IMPLEMENTATION ...................................................................................
class CreateReferencePointCommand extends BendpointCommand {
	private DMSPoint			dmsLocation;
	//	private WirePart	wirePart;
	private MovementPath	owner;

	public DMSPoint getDMSLocation() {
		return dmsLocation;
	}

	public void setDMSLocation(DMSPoint dmsLocation) {
		this.dmsLocation = dmsLocation;
	}

	public MovementPath getOwner() {
		return this.owner;
	}

	public void setOwner(MovementPath owner) {
		this.owner = owner;
	}

	//	public void setWirePart(WirePart part) {
	//		// TODO Auto-generated method stub
	//		this.wirePart = part;
	//	}
	//
	//	public WirePart getWirePart() {
	//		return this.wirePart;
	//	}

	//	public void setDMSLocation(DMSPoint location) {
	//		this.dmsLocation = location;
	//	}

	public void execute() {
		//TODO Get access to the current MovementPath
		//		Wire theWire = (Wire) this.wirePart.getModel();
		//		WireEndPoint source = this.getWire().getSource();
		//		MovementPath pathOwner = source.getOwner();
		getOwner().addPointInWire(this.getDMSLocation(), this.getWire());

		//		path=this.getOwnerPath()
		//		//- Create the new point and initialize
		//		ReferencePoint refPoint = new ReferencePoint();
		//		refPoint.setLatitude(dmsLocation.getDMSLatitude());
		//		refPoint.setLongitude(this.dmsLocation.getDMSLongitude());
		//		refPoint.setParentselected(EditPart.SELECTED_PRIMARY);

		//		//TODO Remove the current wire
		//		WirePart thewire = this.getWirePart();
		//		RootMapPart root = thewire.getRootPart();
		//		//		root.deleteWire(thewire);
		//		wire = (Wire) thewire.getModel();
		//		WireEndPoint source = wire.getSource();
		//		source.removeConnection(wire);
		//		WireEndPoint target = wire.getTarget();
		//		target.removeConnection(wire);
		//		wire.setSource(null);
		//		wire.setTarget(null);

		//TODO then create two new wires to connect the current ReferencePoint.

		//		Dimension start = getFirstRelativeDimension();
		//		Dimension end = getSecondRelativeDimension();
		//		wbp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		//		getWire().insertBendpoint(getIndex(), wbp);
		super.execute();
	}

	public void undo() {
		super.undo();
		//		WireEndPoint source = this.getWire().getSource();
		//		MovementPath pathOwner = source.getOwner();
		getOwner().removePointAtIndex(this.getIndex());
	}

}

//- CLASS IMPLEMENTATION ...................................................................................
class MoveBendpointCommand extends BendpointCommand {

	private Bendpoint	oldBendpoint;

	public void execute() {
		final WireBendpoint bp = new WireBendpoint();
		bp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		setOldBendpoint((Bendpoint) getWire().getBendpoints().get(getIndex()));
		getWire().setBendpoint(getIndex(), bp);
		super.execute();
	}

	protected Bendpoint getOldBendpoint() {
		return oldBendpoint;
	}

	public void setOldBendpoint(final Bendpoint bp) {
		oldBendpoint = bp;
	}

	public void undo() {
		super.undo();
		getWire().setBendpoint(getIndex(), getOldBendpoint());
	}

}

//- CLASS IMPLEMENTATION ...................................................................................
class DeleteBendpointCommand extends BendpointCommand {

	private Bendpoint	bendpoint;

	public void execute() {
		bendpoint = (Bendpoint) getWire().getBendpoints().get(getIndex());
		getWire().removeBendpoint(getIndex());
		super.execute();
	}

	public void undo() {
		super.undo();
		getWire().insertBendpoint(getIndex(), bendpoint);
	}

}

// - UNUSED CODE ............................................................................................
