/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.rcp.harpoon.editors;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.MultiPageEditorPart;

/**
 * This the the main editor page for modifying a complete workflow.
 * 
 * @author Gunnar Wagenknecht
 */
public class WorkflowPage extends AbstractEditorPage {
	/**
	 * Creates a new WorkflowPage instance.
	 * 
	 * <p>
	 * By design this page uses its own <code>EditDomain</code>. The main goal of this approach is that this page has
	 * its own undo/redo command stack.
	 * 
	 * @param parent
	 *          the parent multi page editor
	 * @param editDomain
	 */
	public WorkflowPage(MultiPageEditorPart parent, DefaultEditDomain editDomain) {
		super(parent, editDomain);
	}

	/**
	 * Return the name for this page. It seems that is something superfluous.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getPageName()
	 */
	@Override
	protected String getPageName() {
		return "Workflow";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#createPageControl(org.eclipse.swt.widgets.Composite)
	 */
	// protected void createPageControl(Composite parent) {
	// final Composite composite = new Composite(parent, SWT.NONE);
	// composite.setBackground(parent.getBackground());
	// composite.setLayout(new GridLayout(2, false));
	//
	// createPaletteViewer(composite);
	// GridData gd = new GridData(GridData.FILL_VERTICAL);
	// gd.widthHint = 125;
	// getPaletteViewer().getControl().setLayoutData(gd);
	//
	// createGraphicalViewer(composite);
	// gd = new GridData(GridData.FILL_BOTH);
	// gd.widthHint = 275;
	// getGraphicalViewer().getControl().setLayoutData(gd);
	// }
	/** the graphical viewer */
	protected GraphicalViewer	viewer;

	// private RootMapUnit getWorkflow() {
	// // TODO Auto-generated method stub
	// return getWorkflowEditor().getWorkflow();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getGraphicalViewerForZoomSupport()
	 */
	@Override
	public GraphicalViewer getGraphicalViewer() {
		return viewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPart#getTitle()
	 */
	@Override
	public String getTitle() {
		return "Edit the whole workflow";
	}

	@Override
	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub

	}

}
