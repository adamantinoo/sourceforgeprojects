//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: VectorCalculationTest.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/VectorCalculationTest.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:35:04 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import junit.framework.TestCase;
import net.sf.harpoon.model.units.MovableUnit;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;

// - CLASS IMPLEMENTATION ...................................................................................
public class VectorCalculationTest extends TestCase {
	/** Create a new MovementPath and load it with different points to check for directions calculations. */
	public void testDirectionCalculation() throws Exception {
		final MovableUnit unit = new MovableUnit();
		unit.setName("Direction Test");
		unit.setLatitude(new DMSCoordinate(0, 30, 0, 'S'));
		unit.setLongitude(new DMSCoordinate(0, 30, 0, 'E'));
		final DMSPoint startPoint = new DMSPoint(new DMSCoordinate(0, 30, 0, 'S'), new DMSCoordinate(0, 30, 0,
				'E'));

		MovementPath path = new MovementPath(unit);
		assertEquals("Checking direction for single point path� ", 0, path.calculateDirection(startPoint));

		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 30, 0, 'E')); // 000�
		assertEquals("Checking direction for angle 0� ", 0, path.calculateDirection(startPoint));
		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 30, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // 090�
		assertEquals("Checking direction for angle 90� ", 90, path.calculateDirection(startPoint));
		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 40, 0, 'S'), new DMSCoordinate(0, 30, 0, 'E')); // 180�
		assertEquals("Checking direction for angle 180� ", 180, path.calculateDirection(startPoint));
		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 30, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E')); // 270�
		assertEquals("Checking direction for angle 270� ", 270, path.calculateDirection(startPoint));

		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 10, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E')); // 30�
		assertEquals("Checking direction for angle 30� ", 30, path.calculateDirection(startPoint));
		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E')); // 45�
		assertEquals("Checking direction for angle 45� ", 45, path.calculateDirection(startPoint));
		path = new MovementPath(unit);
		path.addPoint(new DMSCoordinate(0, 40, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // 135�
		assertEquals("Checking direction for angle 90� ", 135, path.calculateDirection(startPoint));
	}
}

// - UNUSED CODE ............................................................................................
