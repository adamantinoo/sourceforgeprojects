//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonFigureFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/HarpoonFigureFactory.java,v $
//  LAST UPDATE:    $Date: 2007-10-02 09:03:44 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.4  2007-09-27 16:45:16  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.model.PropertyModel;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.BasicFigure;
import net.sourceforge.rcp.harpoon.test.RightUnit;
import net.sourceforge.rcp.harpoon.test.TestUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class HarpoonFigureFactory {

	private static final String	SEMISURFACE	= "semi";
	private static final String	RIGHTLABEL	= "right";

	public static Figure createFigure(PropertyModel unit, String subType) {
		HarpoonLogger.log(Level.FINE, "Creating Figure for model type: " + unit.getClass().getSimpleName());
		// - Create and initialize the figure
		Figure fig = null;

		if (unit instanceof RootMapUnit) {
			fig = new RootMapFigure();
		}
		if (unit instanceof WarUnit) {
			final WarFigure mfig = new WarFigure();
			if (HarpoonConstants.UNIT_SURFACE.equals(subType)) {
				mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_AIR.equals(subType)) {
				mfig.setDrawFigure(new AirDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_SUBMARINE.equals(subType)) {
				mfig.setDrawFigure(new SubmarineDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
		}
		if (unit instanceof TestUnit) {
			final TestFigure mfig = new TestFigure();
			if (HarpoonConstants.UNIT_SURFACE.equals(subType)) {
				mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_AIR.equals(subType)) {
				mfig.setDrawFigure(new AirDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_SUBMARINE.equals(subType)) {
				mfig.setDrawFigure(new SubmarineDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (SEMISURFACE.equals(subType)) {
				mfig.setDrawFigure(new SemiSurface(mfig));
				mfig.init();
				fig = mfig;
			}
			if (RIGHTLABEL.equals(subType)) {
				mfig.setDrawFigure(new SemiSurface(mfig));
				mfig.init();
				fig = mfig;
			}
		}
		if (unit instanceof RightUnit) {
			RightLabeledFigure mfig = new RightLabeledFigure();
			if (HarpoonConstants.UNIT_SURFACE.equals(subType)) {
				mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_AIR.equals(subType)) {
				mfig = new SingleLabeledFigure();
				mfig.setDrawFigure(new AirDrawFigure(mfig));
				mfig.init();
				fig = mfig;
			}
			if (HarpoonConstants.UNIT_SUBMARINE.equals(subType)) {
				PositionedLabeledFigure plf = new PositionedLabeledFigure();
				plf.setDrawFigure(new SubmarineDrawFigure(mfig));
				plf.init();
				fig = plf;
			}
		}
		// if (unit instanceof MovableUnit) {
		// MovableFigure3 mfig = new MovableFigure3();
		// if (HarpoonConstants.SURFACE_UNIT.equals(subType)) {
		// mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
		// fig = mfig;
		// }
		// mfig.init();
		// }
		if (unit instanceof SensorsModel) {
			final SensorsFigure mfig = new SensorsFigure();
			mfig.init();
			fig = mfig;
		}
		HarpoonLogger.log(Level.FINE, fig.toString());
		return fig;
	}
}

/**
 * Implements the complete figure for a game unit. This figure is usually composed of the icon figure and some labels
 * around that icon. The API to fulfill is:
 * <ul>
 * <li><code>init()</code> structure initilizer and build construction. This can be delayed to the first paint
 * invocation so a new method is not required when creating the structures. A two phase constructor is needed because
 * there are changes in the order of the elements to be addes to the internal composite.</li>
 * <li><code>getHotSpot()</code> to get the displacement from the location point to the dra3wing hotspot.</li>
 * <li><code>getPreferredSize(int wHint, int hHint)</code> to get the calculated size of the figure.</li>
 * </ul>
 */
class TestFigure extends WarFigure {
	@Override
	public void init() {
		this.add(iconic);

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
		this.repaint();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		// TODO Draw the center point in a different color
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = getBounds().getCopy();
		// bound.width -= 1;
		// bound.height -= 1;
		final Point loc = getLocation();
		// loc.x--;
		// loc.y--;
		// DEBUG Test if the resulting point and the location match.
		// - Draw the figure center
		final Dimension hotspot = getHotSpot();
		loc.x += hotspot.width;
		loc.y += hotspot.height;
		final Point endPoint = new Point(loc.x - 4, loc.y - 4);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(ColorConstants.lightBlue);
		graphics.drawLine(loc, endPoint);
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		return iconic.getSize();
	}

	@Override
	public Dimension getHotSpot() {
		return iconic.getHotSpot();
	}
}

class SingleLabeledFigure extends RightLabeledFigure {
	public void init() {
		// - Create the complex internal parts of this figure.
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = 2;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.verticalSpacing = 0;
		setLayoutManager(grid);
		this.add(iconic);
		this.add(createRightSide());

		// - Calculate size and bounds
		setBorder(new LineBorder(1));
		this.setSize(this.getPreferredSize(-1, -1));
		this.repaint();
	}

	private Figure createRightSide() {
		// final Figure labelContainer = new Figure();
		// final GridLayout grid = new GridLayout();
		// grid.numColumns = 1;
		// grid.verticalSpacing = 0;
		// grid.horizontalSpacing = 0;
		// grid.marginHeight = 0;
		// grid.marginWidth = 0;
		// labelContainer.setLayoutManager(grid);
		String labelText = "000" + '\n' + "00";
		Label infoLabel = new Label(labelText);
		// infoLabel.
		infoLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// // speedlabel.setBorder(new LineBorder(1));
		// labelContainer.add(speedLabel);
		// directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// // dirLabel.setBorder(new LineBorder(1));
		// labelContainer.add(directionLabel);
		// labelContainer.validate();
		return infoLabel;
	}

}

class RightLabeledFigure extends WarFigure {
	protected final Label			directionLabel	= new Label("000");
	protected final Label			speedLabel			= new Label("00");
	private static final int	MARGIN					= 2;
	private static final int	CHAR_WIDTH			= 6;
	private static final int	CHAR_HEIGHT			= 13;

	@Override
	public void init() {
		// - Create the complex internal parts of this figure.
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = 2;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.verticalSpacing = 0;
		setLayoutManager(grid);
		this.add(iconic);
		this.add(createRightSide());

		// - Calculate size and bounds
		setBorder(new LineBorder(1));
		this.setSize(this.getPreferredSize(-1, -1));
		this.repaint();
	}

	private Figure createRightSide() {
		final Figure labelContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		labelContainer.setLayoutManager(grid);
		speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// speedlabel.setBorder(new LineBorder(1));
		labelContainer.add(speedLabel);
		directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// dirLabel.setBorder(new LineBorder(1));
		labelContainer.add(directionLabel);
		labelContainer.validate();
		return labelContainer;
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		// TODO Draw the center point in a different color
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = getBounds().getCopy();
		// bound.width -= 1;
		// bound.height -= 1;
		final Point loc = getLocation();
		// loc.x--;
		// loc.y--;
		// DEBUG Test if the resulting point and the location match.
		// - Draw the figure center
		final Dimension hotspot = getHotSpot();
		loc.x += hotspot.width;
		loc.y += hotspot.height;
		final Point endPoint = new Point(loc.x - 4, loc.y - 4);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(ColorConstants.lightBlue);
		graphics.drawLine(loc, endPoint);
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = MARGIN + rightSize.width + MARGIN + iconicSize.width + MARGIN;
		fullSize.height = MARGIN + Math.max(iconicSize.height, rightSize.height) + MARGIN;
		HarpoonLogger.info(">>> ENTERING");
		HarpoonLogger.info("iconicSize = " + iconicSize);
		HarpoonLogger.info("rightSize = " + rightSize);
		HarpoonLogger.info("fullSize = " + fullSize);
		HarpoonLogger.info("<<< EXITING");
		return fullSize;
	}

	private Dimension getRightSideSize() {
		final int maxChars = StrictMath.max(speedLabel.getText().length(), directionLabel.getText().length());
		final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT * 2);
		return leftLabelSize;
	}

	@Override
	public Dimension getHotSpot() {
		return iconic.getHotSpot();
	}
}

/**
 * Draws a SURFACE figure that is represented by a circle. The basic API methods to be implemented for the game
 * interface are:
 * <ul>
 * <li><code><>DrawFigure(UnitFigure parentFigure)</code> constructor to associate the figure with the parent figure.</li>
 * <li><code>paintFigure(Graphics graphics)</code> to perform the figure drawing operations.</li>
 * <li><code>getHotSpot()</code> to get the displacement from the location point to the dra3wing hotspot.</li>
 * <li><code>getPreferredSize(int wHint, int hHint)</code> to get the calculated size of the figure.</li>
 * </ul>
 */
class SurfaceDrawFigure extends BasicFigure {
	private static final int	FIGURE_SIZE	= 16;

	// - C O N S T R U C T O R S
	public SurfaceDrawFigure(UnitFigure parentFigure) {
		super(parentFigure);
		setDrawingSize(FIGURE_SIZE);
	}

	// - O V E R R I D E S E C T I O N
	@Override
	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;

		// - Draw the figure body
		graphics.setForegroundColor(getColor());
		graphics.setLineWidth(2);
		graphics.drawOval(bound);
		graphics.setLineWidth(1);
		if (isSelected()) {
			drawHandles(graphics);
		}

		// - Draw the figure center
		bound = getBounds().getCopy();
		final Dimension hotspot = getHotSpot();
		bound.x += hotspot.width + 1;
		bound.y += hotspot.height + 1;
		final Point endPoint = new Point(bound.x + 1, bound.y + 1);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}
}

class SubmarineDrawFigure extends BasicFigure {
	private static final int	FIGURE_SIZE	= 16;

	// - C O N S T R U C T O R S
	public SubmarineDrawFigure(UnitFigure parentFigure) {
		super(parentFigure);
		setDrawingSize(FIGURE_SIZE);
	}

	// - O V E R R I D E S E C T I O N
	@Override
	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;
		// - Adjust the height to draw a half circle
		// enclosure=new Rectangle()
		// bound.height = new Double(2.0 * bound.height / 3.0).intValue();
		bound.height += bound.height / 3;

		// - Draw the figure body
		graphics.setForegroundColor(getColor());
		graphics.setLineWidth(2);
		graphics.drawArc(bound.x, bound.y, bound.width, bound.height, 180, -180);
		// graphics.drawOval(bound);
		graphics.setLineWidth(1);
		if (isSelected()) {
			drawHandles(graphics);
		}

		// - Draw the figure center
		bound = getBounds().getCopy();
		final Dimension hotspot = getHotSpot();
		bound.x += hotspot.width + 1;
		bound.y += hotspot.height + 1;
		final Point endPoint = new Point(bound.x + 1, bound.y + 1);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}
}

class SemiSurface extends BasicFigure {
	private static final int	FIGURE_SIZE	= 16;

	// - C O N S T R U C T O R S
	public SemiSurface(UnitFigure parentFigure) {
		super(parentFigure);
		setDrawingSize(FIGURE_SIZE);
	}

	// - O V E R R I D E S E C T I O N
	@Override
	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;
		// - Adjust the height to draw a half circle
		// enclosure=new Rectangle()
		// bound.height = new Double(2.0 * bound.height / 3.0).intValue();
		// bound.height += bound.height / 3;
		// bound.x += bound.height / 3;

		// - Draw the figure body
		graphics.setForegroundColor(getColor());
		graphics.setLineWidth(2);
		graphics.drawArc(bound.x, bound.y, bound.width, bound.height, 175, 190);
		// graphics.drawOval(bound);
		graphics.setLineWidth(1);
		if (isSelected()) {
			drawHandles(graphics);
		}

		// - Draw the figure center
		bound = getBounds().getCopy();
		final Dimension hotspot = getHotSpot();
		bound.x += hotspot.width + 1;
		bound.y += hotspot.height + 1;
		final Point endPoint = new Point(bound.x + 1, bound.y + 1);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}
}

class AirDrawFigure extends BasicFigure {
	private static final int	FIGURE_SIZE	= 16;

	// - C O N S T R U C T O R S
	public AirDrawFigure(UnitFigure parentFigure) {
		super(parentFigure);
		setDrawingSize(FIGURE_SIZE);
	}

	// - O V E R R I D E S E C T I O N
	@Override
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		// - Get drawing location. This should be already displaced from the top-left.
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;

		// - Draw the figure body
		graphics.setForegroundColor(getColor());
		graphics.setLineWidth(2);
		graphics.drawRectangle(bound.x + 1, bound.y + 1, bound.width - 1, bound.height - 1);
		graphics.setLineWidth(1);
		if (isSelected()) {
			drawHandles(graphics);
		}

		// - Draw the figure center
		bound = getBounds().getCopy();
		final Dimension hotspot = getHotSpot();
		bound.x += hotspot.width + 1;
		bound.y += hotspot.height + 1;
		final Point endPoint = new Point(bound.x + 1, bound.y + 1);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}
}

// - UNUSED CODE ............................................................................................
