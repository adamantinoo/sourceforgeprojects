//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: PositionedLabeledFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/PositionedLabeledFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-02 09:03:44 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
public class PositionedLabeledFigure extends MovableFigure3 {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C S E C T I O N
	// - P R O T E C T E D S E C T I O N
	// - P R I V A T E S E C T I O N
	// - O V E R R I D E S E C T I O N
	private static final int	CHAR_WIDTH	= 6;
	private static final int	CHAR_HEIGHT	= 12;
	private static final int	GAP					= 2;

	public void init() {
		// - Create the complex internal parts of this figure.
		final XYLayout grid = new XYLayout();
		// grid.numColumns = 2;
		// grid.horizontalSpacing = 2;
		// grid.marginHeight = 2;
		// grid.marginWidth = 2;
		// grid.verticalSpacing = 0;
		setLayoutManager(grid);
		speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		speedLabel.setLabelAlignment(PositionConstants.RIGHT);
		directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		directionLabel.setLabelAlignment(PositionConstants.RIGHT);
		this.add(iconic);
		// String labelText = "000" + '\n' + "00";
		// Label infoLabel = new Label(labelText);
		this.add(speedLabel);
		this.add(directionLabel);
		grid.setConstraint(iconic, new Rectangle(0, 0, iconic.getSize().width, iconic.getSize().height));
		grid.setConstraint(speedLabel, new Rectangle(iconic.getSize().width - 3, -3 - 1, 26, 15));
		grid.setConstraint(directionLabel, new Rectangle(iconic.getSize().width - 3, 13 - 4 - 2 - 1 - 1, 26, 15));

		// - Calculate size and bounds
		// setBorder(new LineBorder(1));
		this.setSize(this.getPreferredSize(-1, -1));
		this.repaint();
	}

	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = iconicSize.width + GAP + rightSize.width;
		fullSize.height = Math.max(iconicSize.height, rightSize.height) + GAP + GAP;
		HarpoonLogger.info(">>> ENTERING");
		HarpoonLogger.info("iconicSize = " + iconicSize);
		HarpoonLogger.info("rightSize = " + rightSize);
		HarpoonLogger.info("fullSize = " + fullSize);
		HarpoonLogger.info("<<< EXITING");
		return fullSize;
	}

	private Dimension getRightSideSize() {
		final int maxChars = StrictMath.max(speedLabel.getText().length(), directionLabel.getText().length());
		final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH, CHAR_HEIGHT);
		return leftLabelSize;
	}

	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
		// TODO Draw the center point in a different color
		// - Get drawing location. This should be already displaced from the top-left.
		final Rectangle bound = getBounds().getCopy();
		// bound.width -= 1;
		// bound.height -= 1;
		final Point loc = getLocation();
		// loc.x--;
		// loc.y--;
		// DEBUG Test if the resulting point and the location match.
		// - Draw the figure center
		final Dimension hotspot = getHotSpot();
		loc.x += hotspot.width;
		loc.y += hotspot.height;
		final Point endPoint = new Point(loc.x - 4, loc.y - 4);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(ColorConstants.lightBlue);
		graphics.drawLine(loc, endPoint);
	}

	public Dimension getHotSpot() {
		return iconic.getHotSpot();
	}
}
// - UNUSED CODE ............................................................................................
