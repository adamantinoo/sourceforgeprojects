//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/MovableFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.5  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.4  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.1  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.TestPart;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
public abstract class MovableFigure extends UnitFigure {

	/** Stores the direction of movement of the unit to represent it with a vector. */
	private final double			direction			= 0.0;
	/** Stores the speed of the unit to represent it with the length of a vector. */
	private int								speed					= 0;
	private MovementPath			path					= new MovementPath();

	private static final int	LAYOUT_MARGIN	= 2;
	/** Stores the reference to the class that draws the unit icon representation. */
	// protected IconFigure movable;
	/** Label to draw the value for the speed property. */
	private final Label				speedlabel		= new SpeedLabel(new Integer(speed).toString());
	/** Label to draw the value if this unit direction. */
	private final Label				dirLabel			= new Label(new Double(direction).toString());
	// DEVEL Possible other attributes to be used for drawing.
	private boolean						selected;
	private boolean						hasFocus;

	/** Stores the color that matches the side of the unit in the game. */
	// protected Color color;
	/** Reference to a Container to draw all the elements of the Ship. */
	private Figure						labelContainer;

	// - G E T T E R S / S E T T E R S
	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed is
	 * represented as a numeric label with the speed in knots.
	 */
	public void setSpeed(int newSpeed) {
		speed = newSpeed;

		speedlabel.setText(new Integer(speed).toString());
		invalidate();
		this.setSize(this.getPreferredSize(-1, -1));
	}

	/**
	 * Set the movement path ordered to this unit. The presentation is a connected list of display points that define the
	 * path of movement ordered to the Unit.
	 * 
	 * @param movementPath
	 */
	public void setMovementPath(MovementPath movementPath) {
		// TODO Auto-generated method stub
		path = movementPath;
	}

	class SpeedLabel extends Label {
		private static final int	CHAR_WIDTH	= 6;
		private static final int	CHAR_HEIGHT	= 13;

		public SpeedLabel(String text) {
			setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			setText(text);
		}

		@Override
		public void setText(String newText) {
			// TODO Auto-generated method stub
			super.setText(newText);
			final Dimension size = new Dimension((newText.length() + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT);
			this.setSize(size);
			this.setPreferredSize(size);
		}
	}

	// - P U B L I C S E C T I O N
	@Override
	public void setColor(Color newColor) {
		super.setColor(newColor);
		// this.color = newColor;
		// DEBUG This will not be used because the color now it got from the parent.
		// this.movable.setColor(newColor);
		iconic.repaint();
		// this.repaint();
	}

	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	@Override
	public Dimension getHotSpot() {
		final Dimension hot = iconic.getHotSpot();
		return new Dimension(hot.width + LAYOUT_MARGIN * 2, hot.height + LAYOUT_MARGIN * 2);
	}

	// /**
	// * The method sets the Figure values to be used for the presentation and also to make the size calculations.
	// * Every time a change on this values is detected then the corresponding size is recalculated.
	// */
	// public void setDirection(double newDirection) {
	// this.direction = newDirection;
	//
	// dirLabel.setText(new Integer(new Double(this.direction).intValue()).toString());
	// Dimension size = FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont());
	// Point loc = dirLabel.getLocation();
	// size = new Dimension((dirLabel.getText().length() + 1) * 6 + 2, 13);
	// dirLabel.setSize(size);
	// dirLabel.setPreferredSize(size);
	// // dirLabel.setBorder(new LineBorder(1));
	// // dirLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
	// this.invalidate();
	// this.setSize(this.getPreferredSize(-1, -1));
	// }

	/**
	 * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while FOES are
	 * marked red. NEUTRAL are yellow and other are blue.
	 */
	@Override
	public void setSide(int newSide) {
		// DEBUG I think this property is not necessary.
		// this.side = newSide;
		if (Unit.FRIEND == newSide) setColor(HarpoonConstants.COL_FRIEND);
		if (Unit.FOE == newSide) setColor(HarpoonConstants.COL_FOE);
		if (Unit.NEUTRAL == newSide) setColor(HarpoonConstants.NEUTRAL_COLOR);
		if (Unit.UNKNOWN_SIDE == newSide) setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	/**
	 * Creates the reference to the class that implements the graphical icon representation for the unit. An instance of
	 * that class is received as a parameter to allow generalization.
	 */
	public void createMovableIcon(IconFigure iconInstance) {
		iconic = iconInstance;
		this.add(iconic);
	}

	/** Creates the label structures for the movable items, that it is the speed and direction labels. */
	protected void createLabelinfo() {
		labelContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		labelContainer.setLayoutManager(grid);
		speedlabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// speedlabel.setBorder(new LineBorder(1));
		labelContainer.add(speedlabel);
		dirLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// dirLabel.setBorder(new LineBorder(1));
		labelContainer.add(dirLabel);
		this.add(labelContainer);
		labelContainer.validate();
		//
		// speedlabel = new Label(new Integer(this.speed).toString());
		// speedlabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// Dimension size = FigureUtilities.getStringExtents(speedlabel.getText(), speedlabel.getFont());
		// Point loc = speedlabel.getLocation();
		// speedlabel.setPreferredSize(size);
		// speedlabel.setBorder(new LineBorder(1));
		// speedlabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
		// speedlabel.setSize(size);
		// labelContainer.add(speedlabel);
		// this.add(labelContainer);
		//
		// dirLabel = new Label(new Double(this.direction).toString());
		// dirLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// size = FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont());
		// loc = dirLabel.getLocation();
		// dirLabel.setPreferredSize(size);
		// dirLabel.setBorder(new LineBorder(1));
		// dirLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
		// dirLabel.setSize(size);
		// labelContainer.add(dirLabel);

		// Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		// size=nameLabel.getMinimumSize(-1,-1);

	}

	// protected void paintFigure(Graphics graphics) {
	// this.paintFigure(graphics);
	// }

	/**
	 * The new visual representation is a colored circle with two text information at right side with the speed and
	 * direction. This adds some complexity to the creation and calculation of the right visual coordinates.<br>
	 * Also I have added a border for debug purposes.<br>
	 * <br>
	 * The complexity in the calculation of the new size resides in the margins for the layout and the threee item implied
	 * in the process.
	 */
	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension nameLabelSize = new Dimension((dirLabel.getText().length() + 1) * 6 + 2, 13);

		HarpoonLogger.log(Level.FINE, "icon size:" + iconicSize);
		HarpoonLogger.log(Level.FINE, "calculated label size:" + nameLabelSize);
		// HarpoonLogger.log(Level.FINE, "label size:" + lab);

		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = LAYOUT_MARGIN + iconicSize.width + LAYOUT_MARGIN + nameLabelSize.width - 1;
		fullSize.height = LAYOUT_MARGIN + Math.max(iconicSize.height, nameLabelSize.height * 2) + LAYOUT_MARGIN
				+ LAYOUT_MARGIN + 1 + 1;
		TestPart.log.log(Level.FINE, "final size:" + fullSize);
		return fullSize;

		/*
		 *  // FIXME This method has not been called before a refreshVisuals. // - Get the sized of the three objects. //
		 * Dimension iconSize = movable.getPreferredSize(); Dimension dirLabelSize =
		 * FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont()); Dimension speedLabelSize =
		 * FigureUtilities.getStringExtents(speedlabel.getText(), speedlabel.getFont());
		 * 
		 * int cals = (dirLabel.getText().length() + 1) * 8; dirLabelSize.width = cals;
		 *  // - Calculate the width. Icon width + grid horizontal spacing + max width for label GridLayout grid =
		 * (GridLayout) this.getLayoutManager(); int fullWidth = movable.getPreferredSize().width; fullWidth +=
		 * grid.horizontalSpacing + Math.max(dirLabelSize.width, speedLabelSize.width);
		 *  // - Calculate the height. max of Icon height or label container height int fullHeight =
		 * Math.max(movable.getPreferredSize().height, dirLabelSize.height + speedLabelSize.height + 2);
		 * 
		 * return new Dimension(fullWidth, fullHeight);
		 *  // new Dimension() // Dimension shipDim = movable.getPreferredSize(); // Dimension labelDim = new Dimension(); //
		 * labelDim.height = Math.max(dirLabelSize.height, speedLabelSize.height); // labelDim.width =
		 * Math.max(dirLabelSize.width, speedLabelSize.width); // // dim.width = shipDim.width + labelDim.width; //
		 * dim.height = Math.max(shipDim.height, labelDim.height); // return dim;
		 */
	}

	//
	// public void setBounds(Rectangle rect) {
	// super.setBounds(rect);
	// rect = Rectangle.SINGLETON;
	// getClientArea(rect);
	// // contents.setBounds(rect);
	// Dimension size = this.getPreferredSize();
	// // footer.setLocation(rect.getBottomLeft().translate(0, -size.height));
	// // footer.setSize(size);
	//
	// // size = header.getPreferredSize();
	// // header.setSize(size);
	// // header.setLocation(rect.getLocation());
	// }

	public void setSelected(boolean b) {
		selected = b;
		repaint();
	}

	/**
	 * Sets the focus state of this SimpleActivityLabel
	 * 
	 * @param b
	 *          true will cause a focus rectangle to be drawn around the text of the Label
	 */
	public void setFocus(boolean b) {
		hasFocus = b;
		repaint();
	}

}
// - UNUSED CODE ............................................................................................
