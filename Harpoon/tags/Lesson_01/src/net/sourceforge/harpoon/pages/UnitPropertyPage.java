//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/UnitPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.1  2007-09-21 12:11:30  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class UnitPropertyPage {
	/** Reference to the model. But limited to the interface level of a Unit. */
	private Unit			model;
	/** Group element that will contain all the unit properties to be presented on selection. */
	protected Group		page;
	/**
	 * Reference to the link Control where all page composition will be linked. This element is located in the
	 * SelectionView control contents and it is received by a sentence.
	 */
	private Composite	container;

	// - P U B L I C S E C T I O N
	// - G E T T E R S / S E T T E R S
	public void setModel(Unit model) {
		this.model = model;
	}

	public Composite getContainer() {
		return container;
	}

	public void setContainer(Composite parent) {
		container = parent;
	}

	public Composite getPropertyPage() {
		return container;
	}

	/**
	 * Add all the common elements that are visible for any unit. This depends on the information available on the model
	 * reference at this interface level.
	 */
	public void build() {
		// TODO Change some of the getters to detect if some required fields are not filled in before building up the
		// interface
		// - Create the common contents of the page structure.
		final RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;

		// - Create the group and set the name
		page = new Group(container, SWT.NONE);
		page.setLayout(rowLayout);
		if (Unit.FOE == model.getSide()) page.setForeground(HarpoonConstants.COL_FOE);
		if (Unit.NEUTRAL == model.getSide()) page.setForeground(HarpoonConstants.NEUTRAL_COLOR);
		page.setText(model.getName());

		// - Add model and location information
		// Text modelInfo = new Text(page, SWT.BORDER);
		// modelInfo.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		// modelInfo.setEditable(false);
		// modelInfo.setText(model.getModelInfo());
		final Text location = new Text(page, SWT.BORDER);
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		location.setText(model.getDMSLatitude().toDisplay() + " - " + model.getDMSLongitude().toDisplay());
	}
}

// - UNUSED CODE ............................................................................................
