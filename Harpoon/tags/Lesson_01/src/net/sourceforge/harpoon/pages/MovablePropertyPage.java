//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: MovablePropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/MovablePropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.ExtendedData;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class MovablePropertyPage extends UnitPropertyPage {
	/** Reference to the model. But limited to the interface level of a Unit. */
	private MovableUnit	movableModel;
	private Composite		movementControl;
	private Spinner			speedSet;
	private Spinner			directionSet;
	private Spinner			altitudeSet;
	private Spinner			submarineSet;

	// - G E T T E R S / S E T T E R S
	public void setModel(MovableUnit model) {
		movableModel = model;
		super.setModel(model);
	}

	// - O V E R R I D E S E C T I O N
	/**
	 * Add the interface items that are specific for MovableParts. This include the movement controls.
	 * 
	 * @see net.sourceforge.harpoon.pages.UnitPropertyPage#build()
	 */
	@Override
	public void build() {
		// - Parent will create the base element and then add all data for other levels in the inheritance chain.
		super.build();

		// - Create the speed and direction control
		createMovementControl(page);
		// - Additional movement information for special units like aircrafts and submarines
		createAdditional(page);

		// - Set the control values from the model data.
		directionSet.setSelection(movableModel.getDirection());
		speedSet.setSelection(movableModel.getSpeed());
		speedSet.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				HarpoonLogger.info("New speed value: " + speedSet.getSelection());
				movableModel.setSpeed(speedSet.getSelection());
			}
		});
	}

	// - P R O T E C T E D S E C T I O N
	protected void createMovementControl(Group group) {
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = 2;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		movementControl = new Composite(group, SWT.NONE);
		movementControl.setLayout(grid);

		// - Create the elements that are declared for MovableUnits
		final Text speedLabel = new Text(movementControl, SWT.NONE);
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedSet = new Spinner(movementControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		speedSet.setMaximum(2000);
		speedSet.setIncrement(movableModel.getExtendedData(ExtendedData.XDT_SPEEDINCREMENT));

		final Text directionLabel = new Text(movementControl, SWT.NONE);
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionLabel.setEditable(false);
		directionLabel.setText("Direction:");
		directionSet = new Spinner(movementControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setEnabled(false);
		directionSet.setMaximum(360);
	}

	private void createAdditional(Group page) {
		if (movableModel.isAirBorne()) {
			Text altitudeLabel = new Text(movementControl, SWT.NONE);
			altitudeLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			altitudeLabel.setEditable(false);
			altitudeLabel.setText("Altitude:");
			altitudeSet = new Spinner(movementControl, SWT.NONE);
			altitudeSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
			altitudeSet.setMaximum(15000);
			altitudeSet.setIncrement(1000);

			altitudeSet.setSelection(movableModel.getExtendedData(ExtendedData.ALTITUDE));
		}
		if (movableModel.isSubmarine()) {
			Text submarineLabel = new Text(movementControl, SWT.NONE);
			submarineLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			submarineLabel.setEditable(false);
			submarineLabel.setText("Deep:");
			submarineSet = new Spinner(movementControl, SWT.NONE);
			submarineSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
			submarineSet.setMaximum(800);

			submarineSet.setSelection(movableModel.getExtendedData(ExtendedData.DEEP));
		}
	}
}

// - UNUSED CODE ............................................................................................
