//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/WarPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.WarUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P R I V A T E S E C T I O N
public class WarPropertyPage extends MovablePropertyPage {
	/**
	 * This reference contains the pointer to all the model data. This will be needed to compose and fill the interface
	 * elements.
	 */
	private WarUnit	warModel;

	// - P U B L I C S E C T I O N
	public void setModel(WarUnit model) {
		warModel = model;
		super.setModel(model);
	}

	// - O V E R R I D E S E C T I O N
	/**
	 * Add the interface items that are specific for WarParts. This include all the sensor information and weapons
	 * control.
	 * 
	 * @see net.sourceforge.harpoon.pages.UnitPropertyPage#build()
	 */
	@Override
	public void build() {
		// - Parent will create the base element and then add all data for other levels in the inheritance chain.
		super.build();

		// - Create the contents of sersors structure that are the items declared on a WarUnit
		createSensorsArea(page);
	}

	private void createSensorsArea(Composite parent) {
		final RowLayout row = new RowLayout();
		row.type = SWT.VERTICAL;
		row.marginLeft = 2;
		row.marginRight = 2;
		row.marginBottom = 1;
		row.marginTop = 1;
		row.wrap = false;
		final Group sensorsGroup = new Group(parent, SWT.NONE);
		sensorsGroup.setLayout(row);
		sensorsGroup.setText("Sensors:");

		// - Add each one of the available sensors for this unit. This is obtained from the model.
		final SensorsModel sensors = warModel.getSensorInformation();
		sensors.addSensorsPage(sensorsGroup);
	}

	private void createSensorList(Composite parent) {
	}
}

// - UNUSED CODE ............................................................................................
