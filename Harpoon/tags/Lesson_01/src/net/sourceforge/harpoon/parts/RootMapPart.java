//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/RootMapPart.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.10 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.9  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.8  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.7  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.6  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.5  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.RootMapFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.PropertyModel;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
/**
 * This is the controller of the root model element implemented as the Map. With the inheritance chain we only have to
 * implement the key methods <code>createFigure()</code> and <code>getModelChildren()</code> if this part is a
 * container.
 */
public class RootMapPart extends AbstractGraphicalEditPart implements PropertyChangeListener {
	public static Point convertToMap(RootMapPart rootPart, Unit model) {
		// - Get the unit coordinates and the top-left coordinates.
		final DMSCoordinate unitLat = model.getDMSLatitude();
		final DMSCoordinate unitLon = model.getDMSLongitude();

		// - Get the model root where there are stored the conversion properties.
		// final RootMapPart rootPart = (RootMapPart) getParent();
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint point = new DMSPoint(topLat, topLon);
		final DMSPoint vector = point.offsetPoint(new DMSPoint(unitLat, unitLon));

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	/**
	 * Currently I have to get access to the Map that is the already created figure because it contains some of the model
	 * data mixed with the presentation data.<br>
	 * This figure is costly to be created many times. Now just create it every time we have the part requested that now
	 * is just a single time.<br>
	 * <br>
	 * The creation of the Figure can be generalized for most EditParts. Most of the parts get the model information to
	 * set the initial visual Figure properties (that most of the time are the <code>name</code> and the
	 * <code>side</code>). The Figure class can be intantiated by indirectly so all this standard code can be
	 * refactored to the base <code>UnitPart</code> class.<br>
	 * <br>
	 * Another complicated thing to be reviewed if the way to communicate the scenery properties. This version gets the
	 * properties from the PartFactory that received the editor as a parameter and it is accessible statically. This has
	 * been changed to get the properties from the Model piece that is more easily configured.
	 */
	@Override
	public IFigure createFigure() {
		// - Create the presentation figure and initialize it with the scenery properties.
		final RootMapUnit model = (RootMapUnit) getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure MapFigure");
		final RootMapFigure fig = new RootMapFigure();
		// - For the root there is no need to set the side. The name id the scenery name and it it got form the
		// properties.
		fig.setConfiguration(model.getMapProperties());
		fig.setRootMap(this);
		return fig;
	}

	/**
	 * Return the model children that have to be assigned EditParts. Not all children elements generate EditPart. Enemy
	 * units are processed but are not displayed and this is detected on the <code>paintFigure</code> of the unit.
	 */
	@Override
	protected List getModelChildren() {
		final Unit model = (Unit) getModel();
		final List li = model.getChildren();
		return li;
	}

	/**
	 * Activation and deactivation is something still not investigated. Keep the code for property management.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		((PropertyModel) getModel()).addPropertyChangeListener(this);
		super.activate();
	}

	@Override
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		((PropertyModel) getModel()).removePropertyChangeListener(this);
		super.deactivate();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Unit.CHILDREN.equals(prop)) {
			// - There is a change on the children structure. Detect if we are adding or removing am element.
			if (evt.getOldValue() instanceof PropertyModel) {
				removeChild((EditPart) getViewer().getEditPartRegistry().get(evt.getOldValue()));
			} else {
				// this.createChild(evt.getNewValue());
				addChild(createChild(evt.getNewValue()), children.size());
			}
		}

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	@Override
	public String toString() {
		return "RootMapPart:" + ((Unit) getModel()).getName();
	}

	@Override
	protected void refreshVisuals() {
		// TODO Auto-generated method stub
		super.refreshVisuals();
		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		// TODO Auto-generated method stub
		super.addChildVisual(childEditPart, index);
	}

	@Override
	protected void createEditPolicies() {
		// EMPTY
	}

	@Override
	protected void removeChildVisual(EditPart childEditPart) {
		super.removeChildVisual(childEditPart);
	}

	@Override
	public DragTracker getDragTracker(Request arg0) {
		return null;
	}
}

// - UNUSED CODE ............................................................................................
// public Object getAdapter(Class key) {
// //if (AccessibleEditPart.class == key)
// return getAccessibleEditPart();
// //return Platform.getAdapterManager().getAdapter(this, key);
// }
// getModelChildren()
// @Override
// protected void addChildVisual(EditPart part, int index) {
// // TODO Auto-generated method stub
//		
// }
// @Override
// protected void removeChildVisual(EditPart arg0) {
// // TODO Auto-generated method stub
//		
// }
