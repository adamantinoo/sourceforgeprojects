//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovementPath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/MovementPath.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.LinkedList;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class MovementPath implements Serializable {
	private static final long		serialVersionUID	= 2924232465880867103L;
	private final NNLinkedList	points						= new NNLinkedList();

	public void addPoint(DMSCoordinate latitude, DMSCoordinate longitude) {
		// TODO Auto-generated method stub
		points.addLast(new DMSPoint(latitude, longitude));
	}

	public DMSPoint getNextPoint() {
		// FIXME Replace the body and return the first element
		final int dum = 1;
		return points.getFirst();
	}

	class NNLinkedList extends LinkedList<DMSPoint> {
		private static final long	serialVersionUID	= -1433126363814347456L;

	}

	public void paint() {
		// TODO Auto-generated method stub

	}

	/**
	 * Calculates the direction resulting from the calculation of the vector from the current point to the next
	 * point in the path.
	 * 
	 * @param startingPoint
	 *          the current position of the unit. This is used as the start point og the movement vector.
	 * @return
	 */
	public int getDirection(DMSPoint startingPoint) {
		// - If path is empty default to direction 0�.
		if (0 == points.size()) return 0;
		// - Translate coordinates to point P1
		final DMSPoint nextPoint = getNextPoint();
		final DMSPoint vector = nextPoint.substract(startingPoint);

		// - Calculate quadrant depending on the point signs.
		final long lat = vector.getLatitude().toSeconds();
		final long lon = vector.getLongitude().toSeconds();
		final int quadrant = calculateVectorQuadrant(lat, lon);
		final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
		// int quadrant = 0;
		// if ((lat >= 0) && (lon >= 0))
		// quadrant = 1;
		// else if ((lat >= 0) && (lon < 0))
		// quadrant = 4;
		// else if ((lat < 0) && (lon >= 0))
		// quadrant = 2;
		// else if ((lat < 0) && (lon < 0)) quadrant = 3;
		switch (quadrant) {
			case 0:
			case 1:
				// - Check if > 45�
				if (lon > lat) {
					final double alpha = StrictMath.asin(lon / h);
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				} else {
					// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
					final double alpha = StrictMath.acos(lat / h);
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				}
			case 2:
				// - Check if < 135�
				if (lat < lon) {
					final double alpha = StrictMath.acos(lon / h) + 90.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				} else {
					final double alpha = StrictMath.asin(lat / h) + 90.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				}
			case 3:
				// TODO Implement calculations for quadrant 3
				break;
			case 4:
				// TODO Implement calculations for quadrant 4
				break;
			default: // This vallue is never reached
				break;
		}
		return 0;
	}

	private int calculateVectorQuadrant(long vectorLat, long vectorLon) {
		// - Calculate quadrant depending on the point signs.
		int quadrant = 0;
		if ((vectorLat >= 0) && (vectorLon >= 0))
			quadrant = 1;
		else if ((vectorLat >= 0) && (vectorLon < 0))
			quadrant = 4;
		else if ((vectorLat < 0) && (vectorLon >= 0))
			quadrant = 2;
		else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
		return quadrant;
	}
}

// - UNUSED CODE ............................................................................................
