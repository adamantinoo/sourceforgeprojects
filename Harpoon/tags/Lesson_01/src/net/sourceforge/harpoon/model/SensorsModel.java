//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SensorsModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/SensorsModel.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.model;

//- IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;

import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - P R I V A T E S E C T I O N
public class SensorsModel extends PropertyModel {
	private static final long	serialVersionUID	= -1402055842598003340L;
	// private static final String RADAR_PROPERTY = "radar";
	// private static final String SONAR_PROPERTY = "sonar";
	// private static final String ECM_PROPERTY = "ecm";

	/** Configurable flags to indicate which sensors are available for the referent Unit. */
	private boolean						availableRadar		= false;
	private boolean						availableSonar		= false;
	private boolean						availableECM			= true;
	/** Sensor state for al possible sensors. */
	private boolean						radar							= false;
	private boolean						sonar							= false;
	private boolean						ECM								= false;
	/**
	 * Reference to the parent WarUnit where there are the calculation functions for the sensors range. Sensor range
	 * information depends on many parameters only known to the model part.
	 */
	private WarUnit						referent;

	// - C O N S T R U C T O R S
	public SensorsModel(WarUnit warUnit) {
		referent = warUnit;
	}

	// - G E T T E R S / S E T T E R S
	public boolean getRadarState() {
		return radar;
	}

	public void setRadarState(boolean newState) {
		final boolean oldState = radar;
		radar = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.FLD_SENSORS, oldState, newState);
	}

	public boolean getSonarState() {
		return sonar;
	}

	public void setSonarState(boolean newState) {
		final boolean oldState = sonar;
		sonar = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.FLD_SENSORS, oldState, newState);
	}

	public boolean getRadioState() {
		return ECM;
	}

	public void setRadioState(boolean newState) {
		final boolean oldState = ECM;
		ECM = newState;
		// - Fire a notification to parent listeners to update the presentation layer.
		getReferent().firePropertyChange(WarUnit.FLD_SENSORS, oldState, newState);
	}

	public Unit getReferent() {
		if (null == referent) throw new NullPointerException("The reference parent unit is not defined.");
		return referent;
	}

	public void setReferent(WarUnit warUnit) {
		referent = warUnit;
	}

	public String getUnitType() {
		return HarpoonConstants.SENSOR_UNIT;
	}

	// /** Return the range for this sensor. */
	// public int getRange() {
	// // TODO Auto-generated method stub
	// return 20;
	// }
	//
	// public void setRange(int range) {
	// // TODO Auto-generated method stub
	// this.range = range;
	// }

	// - P U B L I C S E C T I O N
	/**
	 * Return the controls to edit the sensor status and information for this particular Unit. This model is configurable
	 * and can define any combination of sensors suitable for any type of WarUnit.
	 */
	public void addSensorsPage(Group sensorsGroup) {
		// - Check the availability for each control. Add high configuration options
		if (availableRadar) {
			final Button radar = new Button(sensorsGroup, SWT.CHECK);
			radar.setText("Radar");
			radar.setSelection(this.radar);
			radar.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(SelectionEvent e) {
					// TODO verify that this is the correct implementation
					HarpoonLogger.info("Radar selected to state " + radar.getSelection());
					setRadarState(radar.getSelection());
					// if (radar.getSelection())
					// warModel.activateSensor(WarUnit.RADAR);
					// else
					// warModel.deActivateSensor(WarUnit.RADAR);
				}

			});
		}
		if (availableSonar) {
			final Button sonar = new Button(sensorsGroup, SWT.CHECK);
			sonar.setText("Sonar");
			sonar.setSelection(this.sonar);
			sonar.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(SelectionEvent e) {
					// TODO verify that this is the correct implementation
					HarpoonLogger.info("sonar selected to state " + sonar.getSelection());
					setSonarState(sonar.getSelection());
				}

			});
		}
		if (availableECM) {
			final Button radio = new Button(sensorsGroup, SWT.CHECK);
			radio.setText("EEM");
			radio.setSelection(ECM);
			radio.addSelectionListener(new SelectionListener() {
				public void widgetDefaultSelected(SelectionEvent e) {
					// EMPTY method that is not in use
				}

				public void widgetSelected(SelectionEvent e) {
					// TODO verify that this is the correct implementation
					HarpoonLogger.info("radio selected to state " + radio.getSelection());
					setRadioState(radio.getSelection());
				}

			});
		}
	}

	public void setAvailable(boolean radar, boolean sonar, boolean radio) {
		availableRadar = radar;
		availableSonar = sonar;
		availableECM = radio;
	}

	public void setAvailable(String sensorType) {
		if (HarpoonConstants.RADAR_TYPE.equals(sensorType)) availableRadar = true;
		if (HarpoonConstants.SONAR_TYPE.equals(sensorType)) availableSonar = true;
		if (HarpoonConstants.ECM_TYPE.equals(sensorType)) availableECM = true;
	}
}

// - UNUSED CODE ............................................................................................
