//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: ExtendedData.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/ExtendedData.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:59 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public interface ExtendedData {
	String	ALTITUDE						= "ALTITUDE";
	String	DEEP								= "DEEP";
	String	XDT_SPEEDINCREMENT	= "XDT_SPEEDINCREMENT";

	public int getSensorRange(String sensorType);

	public boolean isAirBorne();

	public boolean isSurface();

	public boolean isSubmarine();

	// public boolean hasSensors();
}

// - UNUSED CODE ............................................................................................
