//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: BasicFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/BasicFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-21 12:11:30  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class BasicFigure extends IconFigure {
	protected int	drawingSize	= 16;

	// - C O N S T R U C T O R S
	public BasicFigure(UnitFigure parentFigure) {
		super(parentFigure);
		this.setSize(this.getPreferredSize());
	}

	// - P U B L I C S E C T I O N
	public void drawHandles(Graphics graphics) {
		final Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;
		final Point p1 = new Point(bound.x + bound.width / 2, bound.y);
		final Point p2 = new Point(bound.x + bound.width / 2, bound.y + bound.height / 2);
		final Point p3 = new Point(bound.x + bound.width, bound.y + bound.height / 2);
		final Point p4 = new Point(bound.x + bound.width / 2, bound.y + bound.height);
		final Point p5 = new Point(bound.x, bound.y + bound.height / 2);
		graphics.setBackgroundColor(HarpoonColorConstants.HANDLE_COLOR);
		graphics.fillRectangle(new Rectangle(new Point(p1.x, p1.y), new Point(p1.x + 1, p1.y + 2)));
		graphics.fillRectangle(new Rectangle(new Point(p2.x, p2.y), new Point(p2.x + 1, p2.y + 1)));
		graphics.fillRectangle(new Rectangle(new Point(p3.x - 2, p3.y), new Point(p3.x, p3.y + 1)));
		graphics.fillRectangle(new Rectangle(new Point(p4.x, p4.y), new Point(p4.x + 1, p4.y - 2)));
		graphics.fillRectangle(new Rectangle(new Point(p5.x, p5.y), new Point(p5.x + 2, p5.y + 1)));
		graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
		graphics.drawRectangle(bound);
	}

	// - O V E R R I D E S E C T I O N
	@Override
	protected void paintFigure(Graphics graphics) {
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		return new Dimension(drawingSize, drawingSize);
	}

	@Override
	public Dimension getHotSpot() {
		return new Dimension(drawingSize / 2, drawingSize / 2);
	}

	// - P R I V A T E S E C T I O N
	protected void setDrawingSize(int figureSize) {
		drawingSize = figureSize;
		this.setSize(figureSize, figureSize);
	}
}

// - UNUSED CODE ............................................................................................
