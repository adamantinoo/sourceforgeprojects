//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: Scenery.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/Scenery.java,v $
//  LAST UPDATE:    $Date: 2007-10-02 09:04:25 $
//  RELEASE:        $Revision: 1.10 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.9  2007-10-01 14:44:40  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.8  2007-09-28 11:24:51  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.7  2007-09-28 07:56:46  ldiego
//    - Changes to remove the standard threading model and substituing it by the
//      recommended UIJob model. This modification did not worked as expected and
//      the new version will use a new recommendation.
//
//    Revision 1.6  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.5  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.4  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.3  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-12 11:26:07  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.model;

// - IMPORT SECTION .........................................................................................
import harpoonrcp.WorkflowModelManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.ide.IDE;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;
import net.sourceforge.rcp.harpoon.editors.HarpoonSceneryEditor;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
public class Scenery extends WorkflowModelManager {
	/** Information about the types of files than can be open from the interface menus. */
	private final static String[]	OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
	private final static String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };

	private static final String		UNINITIALIZED						= "UNINITIALIZED";
	private static final String		PREPARED								= "PREPARED";

	// private static IWorkbenchWindow window;
	// private static Scenery currentScenery = null;
	private static Scenery				newScenery							= new Scenery();
	// private static Runnable process;
	private String								status									= UNINITIALIZED;

	private Properties						properties							= new Properties();
	private String								sceneryName;
	private String								modelFileName;
	private IFileStore						fileStoreModel;
	private HarpoonSceneryEditor	editor;
	private RootMapUnit						units;
	private boolean								allowRun;
	// private Thread t;
	private Display								display;

	// - S T A T I C S E C T I O N
	public static String openDialog(IWorkbenchWindow window) {
		// TODO Save locally the previous scenery just in case of errors.
		// TODO Check is an scenery is already open. If so ask for closing.
		// - Get the user to choose an scenery file.
		// String sceneryPath = null;
		final FileDialog fileChooser = new FileDialog(window.getShell(), SWT.OPEN);
		// TODO Configure the text of the dialog to be presented to the user.
		fileChooser.setText("Texto to be configured.");
		fileChooser.setFilterPath(null);
		fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(OPEN_FILTER_NAMES);
		final String sceneryName = fileChooser.open();
		return sceneryName;
	}

	// [01]

	public static boolean checkFileStore(String sceneryName) throws PartInitException {
		if (null == sceneryName) return false;
		newScenery = new Scenery();
		newScenery.setSceneryName(sceneryName);

		// - Check that the file selected exists and is valid.
		final IFileStore fileStore = EFS.getLocalFileSystem().getStore(new Path(sceneryName));
		if (!fileStore.fetchInfo().isDirectory() && fileStore.fetchInfo().exists()) {
			// - Read the scenery properties and locate the scenery object data.
			final File sceneryFile = new File(sceneryName);
			if (null == sceneryFile)
				throw new PartInitException("The scenery file selected cannot be open or is empty.");
			final Properties props = new Properties();
			try {
				props.load(new BufferedInputStream(new FileInputStream(sceneryFile)));
				newScenery.setProperties(props);
				final String modelFileName = props.getProperty("modelLocation");
				if (null == modelFileName)
					throw new PartInitException("The scenery model data file selected cannot be open or is empty.");
				newScenery.setModelFileName(modelFileName);

				// - Test if the model exists and then open through the interface.
				final IFileStore fileStoreModel = EFS.getLocalFileSystem().getStore(new Path(modelFileName));
				if (!fileStoreModel.fetchInfo().isDirectory() && fileStoreModel.fetchInfo().exists()) {
					newScenery.setFileStore(fileStoreModel);
					newScenery.setLoadStatus(Scenery.PREPARED);
					return true;
				}
			} catch (final FileNotFoundException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be found.");
			} catch (final IOException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be read or open.");
			}
		}
		return false;
	}

	/**
	 * Once the scenery file selected is valid and accessible, this method calla the IDE to locate the specific editor
	 * that understands the selected file's extension. This will fire the EditorPart creation and initialization method
	 * with calls to the methods <code>init()</code> and <code>setInput()</code>.
	 */
	public static Scenery open(IWorkbenchWindow window) throws PartInitException {
		if (getNewScenery().getLoadStatus().equals(Scenery.PREPARED)) {
			final IWorkbenchPage page = window.getActivePage();
			final HarpoonSceneryEditor editor = (HarpoonSceneryEditor) IDE.openEditorOnFileStore(page, newScenery
					.getFileStoreModel());
			newScenery.setEditor(editor);
			return newScenery;
		} else
			throw new PartInitException("The scenery model data file cannot be read or open.");
	}

	// - P U B L I C S E C T I O N
	// - G E T T E R S / S E T T E R S
	// - P R I V A T E S E C T I O N
	private String getLoadStatus() {
		// TODO Auto-generated method stub
		return status;
	}

	private void setLoadStatus(String newStatus) {
		if (null != newStatus) status = newStatus;
	}

	private void setFileStore(IFileStore fileStoreModel) {
		// TODO Auto-generated method stub
		this.fileStoreModel = fileStoreModel;
	}

	private IFileStore getFileStoreModel() {
		// TODO Auto-generated method stub
		return fileStoreModel;
	}

	private void setModelFileName(String modelFileName) {
		// TODO Auto-generated method stub
		this.modelFileName = modelFileName;
	}

	public Properties getProperties() {
		// TODO Auto-generated method stub
		return properties;
	}

	private void setProperties(Properties props) {
		// TODO Auto-generated method stub
		properties = props;
	}

	private void setSceneryName(String sceneryName) {
		// TODO Auto-generated method stub
		this.sceneryName = sceneryName;
	}

	private void setEditor(HarpoonSceneryEditor editor) {
		// TODO Auto-generated method stub
		this.editor = editor;
	}

	public static Scenery getNewScenery() {
		// TODO Auto-generated method stub
		return newScenery;
	}

	// public static Scenery getCurrentScenery() {
	// // TODO Auto-generated method stub
	// return currentScenery;
	// }
	// private void close() {
	// // TODO Close the current scenery.
	// // TODO Close the editor and the resources that are related to it
	// }
	// public static void cleanOldEditor() {
	// // TODO Auto-generated method stub
	// if (null != currentScenery) currentScenery.close();
	// currentScenery = newScenery;
	// }

	/**
	 * Loads the model data and creates the corresponding MVC structures and initializes them.
	 * 
	 * @param input
	 * @throws PartInitException
	 */
	public RootMapUnit readModel(IEditorInput input) throws PartInitException {
		try {
			// - Convert to a suitable File class to access the URI
			final FileStoreEditorInput fse = (FileStoreEditorInput) input;

			// - The URI gets the file name and path for the scenery data.
			final URI sceneryURI = fse.getURI();
			final FileInputStream fis = new FileInputStream(sceneryURI.getPath());

			// - There are two things to be read, the Model and the scenery properties
			final ObjectInputStream ois = new ObjectInputStream(fis);
			units = (RootMapUnit) ois.readObject();
			ois.close();
			units.setMapProperties(newScenery.getProperties());
			return units;
		} catch (final FileNotFoundException e) {
			throw new PartInitException("The model store file does not exist");
		} catch (final IOException ioe) {
			throw new PartInitException("The model store file can not be read\n" + ioe.getMessage());
		} catch (final ClassNotFoundException e) {
			throw new PartInitException("The model store file does not exist");
		}
	}

	// public RootMapUnit getRootModel() {
	// return units;
	// }

	/**
	 * This methods starts or restarts the game processing loop and the game timers.<br>
	 * The game will test for some conditions on a timely base that it is commanded to the scenery level.<br>
	 * The run process start an infinite loop in a thread that has no UI interface. Any operation inside that thread that
	 * requires access to the UI (updating, creating new model elements) will be posted as another Runnable instance to
	 * the Display created for the application.
	 */
	public void startProcessingLoop() {
		// - Mark the flag that keeps the main thread running.
		display = Display.getCurrent();
		allowRun = true;
		// HarpoonLoop job = new HarpoonLoop(Display.getCurrent(), "GameProcessing loop");
		// job.schedule(2 * 1000);
		// job.runInUIThread(new NullProgressMonitor());
		// // TODO Launch a new thread to perform all the tasks
		// final Scenery scene = this;
		// - Create a new thread and keep it running the infinite loop
		final Thread gameProcessingThread = new Thread(new Runnable() {
			public void run() {
				while (allowRun) {
					// - Sleep until the right number of seconds. Only 0 - 15 - 30 and 45
					Calendar now = Calendar.getInstance();
					int secs = now.get(Calendar.SECOND);
					if ((0 == secs) || (15 == secs) || (30 == secs) || (45 == secs)) {
						// - Run the job but this time inside the Display UI environment.
						// display.syncExec(new HarpoonLoop());
						// Display threadDisplay = Display.getCurrent();
						HarpoonLogger.info("Starting UIJob");
						HarpoonLoop job = new HarpoonLoop(display, "Enemy Detection Block");
						// job.schedule(2 * 1000);
						display.syncExec(job);
						// job.runInUIThread(new NullProgressMonitor());
						try {
							Thread.sleep(5 * 1000);
							int dummy = 1;
						} catch (InterruptedException ie) {
							// - If the thread is interrupted then exit it by turning the flag.
							allowRun = false;
						}
					} else {
						// - Sleep for some time.
						Thread.yield();
						try {
							Thread.sleep(2 * 1000);
						} catch (InterruptedException ie) {
							// - If the thread is interrupted then exit it by turning the flag.
							allowRun = false;
						}
					}
				}
			};
		});
		gameProcessingThread.start();
		gameProcessingThread.setName("GameProcessing loop");
	}

	public void stop() {
		allowRun = false;
	}

	// - CLASS IMPLEMENTATION ...................................................................................
	class HarpoonLoop implements Runnable {

		private Vector<WarUnit>	friendUnits;
		private Vector<Unit>		enemyUnits;
		private final Display		activeDisplay;
		private final String		name;

		public HarpoonLoop(Display activeDisplay, String name) {
			this.activeDisplay = activeDisplay;
			this.name = name;
		}

		/**
		 * Perform a single pass on the Enemy Detection Block and activate any enemies detected by the sensors that are
		 * active. Actions to be performed are:
		 * <ul>
		 * <li>Scan the list of units and get separate lists for friend War units, and enemy or other units.</li>
		 * <li>For each other unit, advance the time since it was detected, but only if it was detected.</li>
		 * <li>If this unit has been detected since at least 60 seconds, the move its state in the detection ladder.</li>
		 * <li>For any friend unit, get the sensors that have activated.</li>
		 * <li>If any unit has activated Radar sensors, scan the list of units for War units of Surface or Air type then
		 * are in range and then increment their detection state in the detection ladder.</li>
		 * <li>If any unit has the ECM sensor active, detect any unit that is Radio detectable (usually Airports).</li>
		 * </ul>
		 */
		// @Override
		// public IStatus runInUIThread(IProgressMonitor monitor) {
		public void run() {
			HarpoonLogger.info(">> Entering Processing Enemy Detection Loop");

			// - Process unit lists to separate processing data.
			getUnitData();

			// - For every enemy unit check if it was detected.
			final Iterator<Unit> undetectedIt = enemyUnits.iterator();
			while (undetectedIt.hasNext()) {
				final Unit unit = undetectedIt.next();
				final String state = unit.getDetectState();
				if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) continue;

				// - Unit is in the detection phase ladder.
				HarpoonLogger.info("Enemy: " + unit.toString() + " goes undetected to " + state);
				unit.degradeDetection();
			}

			// - For every friend unit check if it detects an enemy with any of its sensors
			final Iterator<WarUnit> friendIt = friendUnits.iterator();
			while (friendIt.hasNext()) {
				final WarUnit war = friendIt.next();
				// - Get the list of sensors active for this unit and add it to the list.
				final SensorsModel sensors = war.getSensorInformation();
				if (sensors.getRadarState()) {
					// - Detect any flying or surface unit with the radar
					final Iterator<Unit> enemyIt = enemyUnits.iterator();
					while (enemyIt.hasNext()) {
						final Unit unit = enemyIt.next();
						// if (unit instanceof WarUnit) {
						// final WarUnit enemy = (WarUnit) unit;
						// final String type = enemy.getUnitType();
						// if ((HarpoonConstants.SURFACE_UNIT.equals(type)) || (HarpoonConstants.AIR_UNIT.equals(type))) {
						// // - Get the locations of both elements.
						// final DMSPoint epoint = new DMSPoint(enemy.getDMSLatitude(), enemy.getDMSLongitude());
						// final DMSPoint spoint = new DMSPoint(war.getDMSLatitude(), war.getDMSLongitude());
						// final DMSVector enemyVector = new DMSVector(spoint, epoint);
						// final int range = war.getRadarRange();
						// if (enemyVector.getModule() < range) {
						// // - This enemy is detected
						// // HarpoonLogger.info("Enemy detected");
						// HarpoonLogger.info("Enemy: " + enemy.toString() + " is being detected");
						// enemy.fireDetection();
						// }
						// }
						// }// end if (unit instanceof WarUnit)
						if (unit instanceof MovableUnit) {
							final MovableUnit otherUnit = (MovableUnit) unit;
							final String type = otherUnit.getUnitType();
							if ((HarpoonConstants.UNIT_SURFACE.equals(type)) || (HarpoonConstants.UNIT_AIR.equals(type))) {
								// - Get the locations of both elements.
								final DMSPoint epoint = new DMSPoint(otherUnit.getDMSLatitude(), otherUnit.getDMSLongitude());
								final DMSPoint spoint = new DMSPoint(war.getDMSLatitude(), war.getDMSLongitude());
								final DMSVector enemyVector = new DMSVector(spoint, epoint);
								final int range = war.getSensorRange(HarpoonConstants.RADAR_TYPE);
								if (enemyVector.getModule() < range) {
									// - This enemy is detected
									// HarpoonLogger.info("Enemy detected");
									HarpoonLogger.info("Other unit: " + otherUnit.toString() + " is being detected");
									otherUnit.upgradeDetection();
								}
							}
						}
					}// end while (enemyIt.hasNext())
				} // end if if(sensors.getRadarState())

				// TODO Check detections with other sensors
			}// end while (friendIt.hasNext())

			// } else {
			// this.schedule(2 * 1000);
			// sleep();
			// }
			// }
			// return null;
			try {
				Thread.sleep(2 * 1000);
			} catch (final InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		protected void getUnitData() {
			// - Get the list of units, enemy units and sensors.
			final Iterator<Object> it = units.getChildren().iterator();
			friendUnits = new Vector<WarUnit>();
			enemyUnits = new Vector<Unit>();
			// final Vector<SensorsModel> sensorList = new Vector<SensorsModel>();
			while (it.hasNext()) {
				final Object unit = it.next();
				if (unit instanceof WarUnit) {
					final WarUnit war = (WarUnit) unit;
					if (war.getSide() == Unit.FRIEND) friendUnits.add(war);
				}
				if (unit instanceof Unit) {
					final Unit thisUnit = (Unit) unit;
					if (thisUnit.getSide() != Unit.FRIEND) enemyUnits.add(thisUnit);
				}
			}// end while (it.hasNext())
		}
	}

	class DMSVector {

		private final DMSPoint	from;
		private final DMSPoint	to;
		private final int				module;
		private final int				bearing;

		public DMSVector(DMSPoint from, DMSPoint to) {
			this.from = from;
			this.to = to;
			module = calculateModule();
			bearing = calculateAngle();
		}

		public int getModule() {
			// TODO Auto-generated method stub
			return module;
		}

		public int getBearing() {
			// TODO Auto-generated method stub
			return bearing;
		}

		/** The module of a vector is the distance between the definition points. */
		private int calculateModule() {
			// - Calculate the module by substracting the points
			final DMSPoint norm = to.translate(from);
			double result = StrictMath.pow(new Double(norm.getLatitude().toSeconds() / 60.0).doubleValue(), 2.0);
			result += StrictMath.pow(new Double(norm.getLongitude().toSeconds() / 60.0).doubleValue(), 2.0);
			return new Double(StrictMath.sqrt(result)).intValue();
		}

		private int calculateAngle() {
			// - Translate coordinates to point P1
			// final DMSPoint nextPoint = to;
			final DMSPoint vector = to.translate(from);

			// - Calculate quadrant depending on the point signs.
			final long lat = vector.getLatitude().toSeconds();
			final long lon = vector.getLongitude().toSeconds();
			final int quadrant = calculateVectorQuadrant(lat, lon);
			final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
			// int quadrant = 0;
			// if ((lat >= 0) && (lon >= 0))
			// quadrant = 1;
			// else if ((lat >= 0) && (lon < 0))
			// quadrant = 4;
			// else if ((lat < 0) && (lon >= 0))
			// quadrant = 2;
			// else if ((lat < 0) && (lon < 0)) quadrant = 3;
			switch (quadrant) {
				case 0:
				case 1:
					// - Check if > 45�
					if (lon > lat) {
						final double alpha = StrictMath.asin(lon / h);
						return new Double(StrictMath.toDegrees(alpha)).intValue();
					} else {
						// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
						final double alpha = StrictMath.acos(lat / h);
						return new Double(StrictMath.toDegrees(alpha)).intValue();
					}
				case 2:
					// - Check if < 135�
					if (lat < lon) {
						final double alpha = StrictMath.acos(lon / h) + 90.0;
						return new Double(StrictMath.toDegrees(alpha)).intValue();
					} else {
						final double alpha = StrictMath.asin(lat / h) + 90.0;
						return new Double(StrictMath.toDegrees(alpha)).intValue();
					}
				case 3:
					// TODO Implement calculations for quadrant 3
					break;
				case 4:
					// TODO Implement calculations for quadrant 4
					break;
				default: // This vallue is never reached
					break;
			}
			return 0;
		}

		private int calculateVectorQuadrant(long vectorLat, long vectorLon) {
			// - Calculate quadrant depending on the point signs.
			int quadrant = 0;
			if ((vectorLat >= 0) && (vectorLon >= 0))
				quadrant = 1;
			else if ((vectorLat >= 0) && (vectorLon < 0))
				quadrant = 4;
			else if ((vectorLat < 0) && (vectorLon >= 0))
				quadrant = 2;
			else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
			return quadrant;
		}

		@Override
		public String toString() {
			return "DMSVector (" + module + "," + bearing + ")";
		}
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// public static IWorkbenchWindow getWindow() {
// return window;
// }

// public static void setWindow(IWorkbenchWindow window) {
// Scenery.window = window;
// }
// public static void closeOld() {
// // TODO Auto-generated method stub
// currentScenery.close();
// }
