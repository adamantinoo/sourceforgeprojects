package net.sourceforge.rcp.harpoon.app;

import org.osgi.framework.BundleContext;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * The activator class controls the plug-in life cycle
 */
public class HarpoonActivator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "net.sourceforge.rcp.harpoon";

	// The shared instance
	private static HarpoonActivator plugin;
	
	/**
	 * The constructor
	 */
	public HarpoonActivator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	//TODO This two methods perform the same operation. Remove one of them (the less used)
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static HarpoonActivator getDefault() {
		return plugin;
	}
  /**
   * Returns the shared instance.
   */
  public static HarpoonActivator getInstance()
  {
      return plugin;
  }

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
