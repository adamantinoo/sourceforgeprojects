//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonConstants.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonConstants.java,v $
//  LAST UPDATE:    $Date: 2007-10-01 14:43:24 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-27 16:43:56  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.rcp.harpoon.app;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public interface HarpoonConstants {
	// - M E N U S
	String	M_OPEN											= "open";
	String	M_HELP											= "harpoonhelp";

	// - C O L O R S
	Color		COL_FRIEND									= new Color(Display.getCurrent(), 0, 255, 0);
	Color		COL_FOE											= new Color(Display.getCurrent(), 255, 0, 0);
	Color		NEUTRAL_COLOR								= new Color(Display.getCurrent(), 255, 255, 0);
	Color		COL_RADAR										= new Color(Display.getCurrent(), 180, 50, 50);
	Color		SONAR_COLOR									= new Color(Display.getCurrent(), 180, 50, 50);

	// - U N I T S
	String	UNIT_SURFACE								= "UNIT_SURFACE";
	String	UNIT_AIR										= "UNIT_AIR";
	String	UNIT_SUBMARINE							= "UNIT_SUBMARINE";

	String	SENSOR_UNIT									= "sensors";

	// - S E N S O R T Y P E
	String	VISUAL_TYPE									= "VISUAL_TYPE";
	String	RADIO_TYPE									= "RADIO_TYPE";
	String	ECM_TYPE										= "ECM_TYPE";
	String	SONAR_TYPE									= "SONAR_TYPE";
	String	RADAR_TYPE									= "RADAR_TYPE";

	// - S T A T E S
	String	IDENTIFIED_STATE						= "IDENTIFIED_STATE";
	String	IDENTIFICATION_PHASE_STATE	= "IDENTIFICATION_PHASE_STATE";
	String	DETECTED_STATE							= "DETECTED_STATE";
	String	DETECTION_PHASE_STATE				= "DETECTION_PHASE_STATE";
	String	NOT_DETECTED_STATE					= "NOT_DETECTED_STATE";
}

// - UNUSED CODE ............................................................................................
