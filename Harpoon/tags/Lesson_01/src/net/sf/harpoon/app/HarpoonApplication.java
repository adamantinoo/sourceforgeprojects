//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonApplication.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonApplication.java,v $
//  LAST UPDATE:    $Date: 2007-09-28 11:24:51 $
//  RELEASE:        $Revision: 1.12 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.11  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.10  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.9  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.8  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.7  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.6  2007-09-17 15:11:40  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.5  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.4  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.3  2007-09-10 12:56:17  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.2  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

//- IMPORT SECTION .........................................................................................
import harpoonrcp.ICommandIds;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.model.Scenery;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This class controls all aspects of the application's execution
 */
public class HarpoonApplication implements IApplication {
	/**
	 * This variable contains the global status for the debugging mode. By default during the development this flag is
	 * considered active.
	 */
	public static boolean	DEBUG_FLAG	= true;

	/**
	 * This method is called at application start. It just creates and starts the workbench with the particular
	 * configuration for this application. It also reads the application arguments and call the method to process them.
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		// - Process application arguments
		// processArguments(context.getArguments());
		final Display display = PlatformUI.createDisplay();
		try {
			// - This line fires the creation of the perspective and all its contents. Also enters main loop
			HarpoonLogger.log(Level.INFO,
					"Starting and creating the HarpoonApplication. Running workbench ApplicationWorkbenchAdvisor");
			final int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) { return IApplication.EXIT_RESTART; }
			return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed()) workbench.close();
			}
		});
	}

	// - P R I V A T E S E C T I O N
	private void processArguments(Map args) {
		final Set key = args.keySet();
		final Collection<String> val = args.values();
		final Object[] parameters = args.values().toArray();
		// String[] p = (String[]) args.values().toArray();
		for (int i = 0; i < parameters.length; i++) {
			// if(parameters[i] instanceof String) {
			final String parameter = (String) parameters[i];
			// - check only for parameters that start with "-". Other values are discarded
			if (parameter.startsWith("-")) {
				if (parameter.startsWith("-applicationdebug")) {
					// - Check that there is another parameter before trying to read it
					if (i + 1 < parameters.length) {
						final String state = (String) parameters[i + 1];
						if (state.equalsIgnoreCase("true")) DEBUG_FLAG = true;
					}
					// }
				}
			}
		}
		// Iterator it = val.iterator();
		// // int dummy=0;
		// while (it.hasNext()) {
		// String parameter = (String) it.next();
		// // - check only for parameters that start with "-". Other values are discarded
		// if (parameter.startsWith("-")) {
		// if (args[i].startsWith("-conf")) { //$NON-NLS-1$
		// // dummy+=1;
		// }
	}

	// - CLASS IMPLEMENTATION ...................................................................................
	/** Creates the initial windows and sets the default perspective. There should be a default perspective. */
	class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {
		private static final String	PERSPECTIVE_ID	= "net.sourceforge.rcp.harpoon.harpoonperspective";

		@Override
		public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			return new ApplicationWorkbenchWindowAdvisor(configurer);
		}

		@Override
		public String getInitialWindowPerspectiveId() {
			return PERSPECTIVE_ID;
		}

	}

	// - CLASS IMPLEMENTATION ...................................................................................
	/** Configures the application window. */
	class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

		// - C O N S T R U C T O R S
		public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			super(configurer);
		}

		// - P U B L I C S E C T I O N
		@Override
		public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
			return new ApplicationActionBarAdvisor(configurer);
		}

		/**
		 * This method is called before any window is open. This is the list of options that can be activated:
		 * 
		 * <pre>
		 * configurer.setShowCoolBar(false);
		 * configurer.setShowFastViewBars(false);
		 * configurer.setShowMenuBar(true); // Show the application menu bar
		 * configurer.setShowPerspectiveBar(false);
		 * configurer.setShowProgressIndicator(false);
		 * configurer.setShowStatusLine(false);
		 * </pre>
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#preWindowOpen()
		 */
		@Override
		public void preWindowOpen() {
			final IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
			configurer.setInitialSize(new Point(1100, 900));
			configurer.setShowCoolBar(true);
			configurer.setShowStatusLine(false);

			// - Set the title of the main window
			configurer.setTitle("Harpoon Simulation. Draft 00.02");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
		 */
		@Override
		public void postWindowOpen() {
			super.postWindowOpen();
			// - Set the window position
			Display.getCurrent().getShells()[1].setLocation(220, 0);
		}
	}

	// - CLASS IMPLEMENTATION ...................................................................................
	/**
	 * An action bar advisor is responsible for creating, adding, and disposing of the actions added to a workbench
	 * window. Each window will be populated with new actions.<br>
	 * <br>
	 * The methods that should be implemented by customized classes are:
	 * <ul>
	 * <li>fillActionBars(int flags)-Configures the action bars using the given action bar configurer. </li>
	 * <li>fillCoolBar(ICoolBarManager coolBar) - Fills the cool bar with the main toolbars for the window.</li>
	 * <li>fillMenuBar(IMenuManager menuBar) - Fills the menu bar with the main menus for the window. </li>
	 * <li>fillStatusLine(IStatusLineManager statusLine) - Fills the status line with the main status line contributions
	 * for the window. </li>
	 * </ul>
	 * Other method called at startup is the method <code>makeActions(IWorkbenchWindow window)</code>.
	 */
	class ApplicationActionBarAdvisor extends ActionBarAdvisor {

		// Actions - important to allocate these only in makeActions, and then use them
		// in the fill methods. This ensures that the actions aren't recreated
		// when fillActionBars is called with FILL_PROXY.
		private IWorkbenchAction				exitAction;
		private IWorkbenchAction				aboutAction;
		private OpenSceneryAction				openSceneryAction;
		private OpenActionLogViewAction	openActionLogViewAction;

		public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
			super(configurer);
		}

		/**
		 * Creates the menu actions and registers them. Registering is needed to ensure that key bindings work. The
		 * corresponding commands key bindings are defined in the plugin.xml file. Registering also provides automatic
		 * disposal of the actions when the window is closed.
		 */
		@Override
		protected void makeActions(final IWorkbenchWindow window) {
			// - Creates the menu actions and registers them.
			exitAction = ActionFactory.QUIT.create(window);
			register(exitAction);
			aboutAction = ActionFactory.ABOUT.create(window);
			register(aboutAction);

			openSceneryAction = new OpenSceneryAction(window, "Open Scenery...");
			register(openSceneryAction);
			openActionLogViewAction = new OpenActionLogViewAction(window, "Open Action Log View");
			register(openActionLogViewAction);
		}

		@Override
		protected void fillMenuBar(IMenuManager menuBar) {
			final MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
			final MenuManager helpMenu = new MenuManager("&Help", HarpoonConstants.M_HELP);
			final MenuManager openMenu = new MenuManager("&Open", HarpoonConstants.M_OPEN);

			// menuBar.add(fileMenu);
			menuBar.add(openMenu);
			// Add a group marker indicating where action set menus will appear.
			menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			menuBar.add(helpMenu);

			// File
			openMenu.add(openSceneryAction);
			openMenu.add(openActionLogViewAction);
			openMenu.add(new Separator());
			openMenu.add(exitAction);

			// Help
			helpMenu.add(aboutAction);
		}

		// protected void fillCoolBar(ICoolBarManager coolBar) {
		// IToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
		// coolBar.add(new ToolBarContributionItem(toolbar, "main"));
		// toolbar.add(openViewAction);
		// toolbar.add(messagePopupAction);
		// }

		// public void fillActionBars(int flags) {
		// super.fillActionBars(flags);
		// }
		//
		// protected void fillCoolBar(ICoolBarManager coolBar) {
		// super.fillCoolBar(coolBar);
		// }
		//
		// protected void fillStatusLine(IStatusLineManager statusLine) {
		// super.fillStatusLine(statusLine);
		// }

		// - CLASS IMPLEMENTATION ...................................................................................
		/**
		 * This class implements the action to open a new scenery.<br>
		 * The process of opening a new game scenery starts by closing any previous scenery. The command has to perform the
		 * next series of tasks in order to finish with an stable user interface session:
		 * <ul>
		 * <li>Checks if there is any scenery already open in the editor. If there is an scenery the command sends a
		 * message to stop the processing loop and the game time while the application processes the command.</li>
		 * <li>If there is already an scenery open thenit should ask the user for confirmation before proceeding to close
		 * or modify the current one.</li>
		 * <li>Then it opens a new scenery and updates the UI.</li>
		 * <li>If there is confirmation the command opens the new scenery.</li>
		 * <li>If there are no errors during the open of the new scenery, the command closes the old scenery.</li>
		 * <li>If during the open there are any error, the open command is canceled and the older scenery is kept.</li>
		 * <li>Finally the loaded scenery is activated and the processing loop is started so the game time.</li>
		 * </ul>
		 * <br>
		 * The order of operations to be performed to create a suitable MVC is described next:
		 * <ul>
		 * <li>Store the current scenery to be replaced if there is any error loading the new one.</li>
		 * <li>Open a dialog to allow the user to select the file.</li>
		 * <li>Open the selected file and create a new scenery with its contents.</li>
		 * <li>Load and create the internal data structures not initialized during the scenery creation.</li>
		 * <li>Initialize the GraphicalViewer. This will trigger the creation of all the EditParts and then the
		 * corresponding Figures for the drawing.</li>
		 * </ul>
		 */
		class OpenSceneryAction extends Action {
			// /** Information about the types of files than can be open from the interface menus. */
			// private final String[] OPEN_FILTER_NAMES = new String[] { "Sceneries (*.scenery)" };
			// private final String[] OPEN_FILTER_EXTENSIONS = new String[] { "*.scenery" };
			/** Reference to the window where the uses wants to open the editor. */
			private final IWorkbenchWindow	window;

			// - C O N S T R U C T O R S
			public OpenSceneryAction(IWorkbenchWindow window, String label) {
				HarpoonLogger.log(Level.FINE, "Creating instance of Action. OpenSceneryAction");
				this.window = window;
				setText(label);

				// - The id is used to refer to the action in a menu or toolbar.
				setId(ICommandIds.CMD_OPEN_SCENERY);
				// - Associate the action with a pre-defined command, to allow key bindings.
				setActionDefinitionId(ICommandIds.CMD_OPEN_SCENERY);
				setImageDescriptor(HarpoonActivator.getImageDescriptor("/icons/sample2.gif"));
			}

			/**
			 * This method is called when the user select the <i>Open Scenery...</i> menu item. This should open the file
			 * selector and then create and initialize the Scenery Editor to load and display the selected scenery.<br>
			 * There is only a single scenery open at the same time. So we have to test if we have any other open scenery in
			 * the global reference and keep a reference to it in case we detect any exception while opening the files.<br>
			 * If there is any exception we keep the previous scenery. If all goes OK before opening the scenery we close the
			 * previous.<br>
			 * We use the <code>Scenery</code> class to help to detect this situations. This class incorporates the code for
			 * the <code>HarpoonScenery</code> and the <code>WorkFlowModelManager</code>.
			 */
			@Override
			public void run() {
				HarpoonLogger.log(Level.FINE, "Menu Action OpenSceneryAction fired");
				if (window != null) {
					try {
						// TODO Detect is there already an scenery open
						// TODO Report this to the user and ask for previous scenery close
						// TODO Stop the clock on the scenery
						// Scenery.setWindow(window);
						final String sceneryName = Scenery.openDialog(window);
						HarpoonLogger.log(Level.INFO, "Selected scenery: " + sceneryName);
						// final boolean check = ;
						if (Scenery.checkFileStore(sceneryName)) {
							Scenery theScenery = Scenery.open(window);
							// TODO Close any other scenery. Sceneries are kept outside this class and registered.
							// TODO Register this new scenery
							theScenery.startProcessingLoop();
						}
					} catch (final PartInitException e) {
						// TODO Set the right title and information in the case there is an error during the load
						MessageDialog.openError(window.getShell(), "Title", "Error message");
					}
				}
			}
		}

		// - CLASS IMPLEMENTATION ...................................................................................
		class OpenActionLogViewAction extends Action {
			private static final String			CHANGE_VIEW	= "changeView";
			private static final String			viewId			= "net.sourceforge.rcp.harpoon.app.actionlogview";
			private final IWorkbenchWindow	window;

			// - C O N S T R U C T O R S
			/**
			 * Creates and initializes the new menu command. The creation registers the command ID for any configured key
			 * bindings.
			 */
			public OpenActionLogViewAction(IWorkbenchWindow window, String label) {
				HarpoonLogger.log(Level.FINE, "Creating instance of Action. OpenActionLogViewAction");
				this.window = window;
				setText(label);

				// The id is used to refer to the action in a menu or toolbar
				setId(OpenActionLogViewAction.CHANGE_VIEW);
				// Associate the action with a pre-defined command, to allow key bindings.
				setActionDefinitionId(OpenActionLogViewAction.CHANGE_VIEW);
				setImageDescriptor(net.sourceforge.rcp.harpoon.app.HarpoonActivator
						.getImageDescriptor("/icons/sample2.gif"));
			}

			/**
			 * This is the method executed when the command is activated. This method should perform the predefined command
			 * and indicated on the requirements.
			 * 
			 * @see org.eclipse.jface.action.Action#run()
			 */
			@Override
			public void run() {
				if (null != window) {
					try {
						window.getActivePage().showView(viewId);
					} catch (final PartInitException e) {
						MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
					}
				}
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
