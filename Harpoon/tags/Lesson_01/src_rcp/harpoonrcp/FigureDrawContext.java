//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: FigureDrawContext.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/harpoonrcp/FigureDrawContext.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:11:40 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package harpoon.devel;

//... IMPORT SECTION ...................................................................................................
//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
import org.eclipse.swt.graphics.*;

public class FigureDrawContext {
	/*
	 * <p>
	 * The GC must be set up as follows
	 * (it will be returned to this state upon completion of drawing operations)
	 * <ul>
	 *   <li>setXORMode(false)
	 * </ul>
	 * </p>
	 */
	public GC gc = null;
	public int xOffset = 0, yOffset = 0; // substract to get GC coords
	public int xScale = 1, yScale = 1;
	
	public Rectangle toClientRectangle(int x1, int y1, int x2, int y2) {
		return new Rectangle(
			Math.min(x1, x2) * xScale - xOffset,
			Math.min(y1, y2) * yScale - yOffset,
			(Math.abs(x2 - x1) + 1) * xScale,
			(Math.abs(y2 - y1) + 1) * yScale);
	}
	public Point toClientPoint(int x, int y) {
		return new Point(x * xScale - xOffset, y * yScale - yOffset);
	}
}


//... UNUSED CODE ......................................................................................................