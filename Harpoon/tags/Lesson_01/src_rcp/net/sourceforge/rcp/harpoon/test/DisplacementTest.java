//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DisplacementTest.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/DisplacementTest.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:25:14  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.tests;

// - IMPORT SECTION .........................................................................................

import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.geom.PolarPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class DisplacementTest extends TestCase {
	public void testEndDisplacement() throws Exception {
		PolarCoordinate startLat = new PolarCoordinate(0, 0, 0);
		PolarCoordinate startLon = new PolarCoordinate(0, 0, 0);
		double alpha1 = 45.0;
		double alpha2 = 90.0;
		double alpha3 = 90.0+45.0;
		double alpha4 = 180;
		double alpha5 = 180+45.0;
		double alpha6 = 270.0;
		double alpha7 = 270.0+45.0;
		double alpha8 = 360.0;
		double speed = 10.0;
		double time = 60.0;

		DisplacementVector vector = new DisplacementVector(new PolarPoint(startLat, startLon), speed,
				alpha1);

		// - Calculate new end point after the time programmed
		double space = speed * time / 3600.0;
		double expectedLat1 = space * Math.sin(UnitConversions.Deg2Rad(alpha1));
		double expectedLon1 = space * Math.cos(UnitConversions.Deg2Rad(alpha1));
		double expectedLat2 = space * Math.sin(UnitConversions.Deg2Rad(alpha2));
		double expectedLon2 = space * Math.cos(UnitConversions.Deg2Rad(alpha2));
		
		vector.setTime(time);
		assertEquals("Checking and latitude for angle " + alpha1, expectedLat1, vector.getEndPointLat().toDegrees(),
				0.000000001);
		assertEquals("Checking and longitude for angle " + alpha1, expectedLon1, vector.getEndPointLon().toDegrees(),
				0.000000001);
		vector.setAngle(alpha2);
		assertEquals("Checking and latitude for angle " + alpha2, expectedLat2, vector.getEndPointLat().toDegrees(),
				0.000000001);
		assertEquals("Checking and longitude for angle " + alpha2, expectedLon2, vector.getEndPointLon().toDegrees(),
				0.000000001);
		vector.setAngle(alpha3);
		assertEquals("Checking and latitude for angle " + alpha3, -expectedLat1, vector.getEndPointLat().toDegrees(),
				0.000000001);
		assertEquals("Checking and longitude for angle " + alpha3, expectedLon1, vector.getEndPointLon().toDegrees(),
				0.000000001);
		vector.setAngle(alpha4);
		assertEquals("Checking and latitude for angle " + alpha4, 0.0, vector.getEndPointLat().toDegrees(),
				0.000000001);
		assertEquals("Checking and longitude for angle " + alpha4, -expectedLon2, vector.getEndPointLon().toDegrees(),
				0.000000001);
	}
}

// - UNUSED CODE ............................................................................................
