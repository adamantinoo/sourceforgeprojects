/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.harpoon.policy;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.BendpointRequest;

import net.sourceforge.harpoon.model.Wire;

public class WireBendpointEditPolicy extends org.eclipse.gef.editpolicies.BendpointEditPolicy {

	protected Command getCreateBendpointCommand(final BendpointRequest request) {
		final CreateBendpointCommand com = new CreateBendpointCommand();
		final Point p = request.getLocation();
		final Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);
		final Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		final Point ref2 = getConnection().getTargetAnchor().getReferencePoint();

		conn.translateToRelative(ref1);
		conn.translateToRelative(ref2);

		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	protected Command getMoveBendpointCommand(final BendpointRequest request) {
		final MoveBendpointCommand com = new MoveBendpointCommand();
		final Point p = request.getLocation();
		final Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);

		final Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		final Point ref2 = getConnection().getTargetAnchor().getReferencePoint();

		conn.translateToRelative(ref1);
		conn.translateToRelative(ref2);

		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	protected Command getDeleteBendpointCommand(final BendpointRequest request) {
		final BendpointCommand com = new DeleteBendpointCommand();
		final Point p = request.getLocation();
		com.setLocation(p);
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	/**
	 * Shows feedback when a bendpoint is being created. The original figure is used for feedback and the
	 * original constraint is saved, so that it can be restored when feedback is erased.
	 * 
	 * @param request
	 *          the BendpointRequest
	 */
	protected void showCreateBendpointFeedback(final BendpointRequest request) {
		try {
			// final Connection con = getConnection();
			final List constraint = (List) getConnection().getRoutingConstraint();
			if (null == constraint) {
				final int dummy = 1;
				getConnection().setRoutingConstraint(new ArrayList());
			}
			final List constraint2 = (List) getConnection().getRoutingConstraint();
			// final List constraint2 = (List) getConnection().getRoutingConstraint();
			// // final int ind = request.getIndex();
			// final ArrayList constraint = new ArrayList();
			// constraint.add(request.getLocation());
			// con.setRoutingConstraint(cons);
			super.showCreateBendpointFeedback(request);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
