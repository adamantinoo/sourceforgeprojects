//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/RootMapFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-18 16:53:41 $
//  RELEASE:        $Revision: 1.10.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.10  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.9  2007-10-03 16:50:09  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.8  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.7  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.6  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.5  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.4  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.2  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-28 13:46:58  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
// - P U B L I C S E C T I O N
public class RootMapFigure extends HarpoonMap {
	private final GoogleMapCache	mapCache	= new GoogleMapCache();
	private RootMapPart						rootMapPart;

	//- C O N S T R U C T O R S
	/**
	 * Create the graphical elements that compose the figure. It is not drawn at this point, just initialized.
	 * The size represents a problem because the map has to fill the full editor area. That information will be
	 * the size to be used for this calculations. Contact the Editor panel to get the size.<br>
	 * The size can be supposed to the max extent of the screen, or the Display that is the greater element.
	 */
	public RootMapFigure() {
		// - Add a free form layout to this figure.
		setLayoutManager(new FreeformLayout());
		// DEBUG Set the border for debugging
		setBorder(new LineBorder(1));

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	//- G E T T E R S / S E T T E R S
	public void setMapProperties(final Properties props) {
		this.props = props;
		mapCache.setConfiguration(props);
	}

	@Override
	public Dimension getPreferredSize(final int hint, final int hint2) {
		// TODO Auto-generated method stub
		final Dimension superSize = super.getPreferredSize(hint, hint2);
		final Display dis = Display.getCurrent();
		final Shell shell = dis.getActiveShell();
		final org.eclipse.swt.graphics.Point siz = shell.getSize();
		return new Dimension(siz);
	}

	// @Override
	// public Rectangle getBounds() {
	// // TODO Auto-generated method stub
	// int width = this.getHorizontalBlocks()*GOOGLE_BLOCK_WIDTH;
	// int height = this.getVerticalBlocks()*GOOGLE_BLOCK_HEIGHT;
	// return new Rectangle(0,0,width,height);
	// }
	/**
	 * Draw each of the map block until all the bounding are is covered with map data. Use the bounding to
	 * calculate the blocks to be read from the cache and to be copied to the display.
	 */
	@Override
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		//- If the figure has not received the properties (they are null) then initialize to blue screen
		if ((null == props) || (0 == props.size())) {
			final Rectangle bounds = getBounds().getCopy();
			graphics.setForegroundColor(ColorConstants.darkGray);
			graphics.setBackgroundColor(ColorConstants.white);
			graphics.fillRectangle(0, 0, bounds.width, bounds.height);
		} else {
			// - Get the bounds to be painted. Calculate number of blocks for each axis.
			final Dimension areaSize = getSize();
			final int hBlocks = (areaSize.width + GOOGLE_BLOCK_WIDTH - 1) / GOOGLE_BLOCK_WIDTH;
			final int vBlocks = (areaSize.height + GOOGLE_BLOCK_HEIGHT - 1) / GOOGLE_BLOCK_HEIGHT;
			// - Copy to the canvas the map data that is stored in the cache.
			for (int j = 0; j < vBlocks; j++) {
				for (int i = 0; i < hBlocks; i++) {
					// - Get a block and paint it on the right location on the final display buffer.
					try {
						final Image newMap = new Image(Display.getCurrent(), mapCache.getBlock(i, j));
						final int x = GOOGLE_BLOCK_WIDTH * i;
						final int y = GOOGLE_BLOCK_HEIGHT * j;
						graphics.drawImage(newMap, x, y);
					} catch (final MalformedURLException mue) {
						final Rectangle bounds = getBounds().getCopy();
						graphics.setForegroundColor(ColorConstants.darkGray);
						graphics.setBackgroundColor(ColorConstants.white);
						graphics.fillRectangle(0, 0, bounds.width, bounds.height);
					} catch (final IOException ioe) {
						//REVIEW Check if the removal of this lines generates errors on the presentation
						//						final Rectangle bounds = getBounds().getCopy();
						//						graphics.setForegroundColor(ColorConstants.darkGray);
						//						graphics.setBackgroundColor(ColorConstants.white);
						//						graphics.fillRectangle(0, 0, bounds.width, bounds.height);
					}
				}
			}

			// - Paint the sensors that are active on any unit that is visible
			//			drawSensors(graphics);
			//- Paint movable unit traces.
			final RootMapPart map = getRootMap();
			final RootMapUnit units = (RootMapUnit) map.getModel();
			final Iterator<Unit> it = units.getChildren().iterator();
			while (it.hasNext()) {
				final Object unit = it.next();
				if (unit instanceof MovableUnit) {
					final MovableUnit movable = (MovableUnit) unit;
					final Vector<DMSPoint> trace = movable.getTrace();
					final Iterator<DMSPoint> traceIt = trace.iterator();
					Point start = null;
					while (traceIt.hasNext()) {
						final DMSPoint point = traceIt.next();
						final Point xy = RootMapPart.dms2xy(map, point);
						if (null == start)
							start = xy;
						else {
							graphics.setForegroundColor(ColorConstants.darkGray);
							graphics.drawLine(start, xy);
							start = xy;
						}
					}
				}
			}
		}
	}

	/**
	 * Paint the sensors that are active on any unit that is visible
	 * 
	 * @param graphics
	 */
	public void drawSensors(final Graphics graphics) {
		// - Get access to the list of units. These are the children of the RootMapPart
		final RootMapPart map = getRootMap();
		final RootMapUnit units = (RootMapUnit) map.getModel();
		final Iterator<Unit> it = units.getChildren().iterator();
		// final Vector<WarUnit> warUnits = new Vector<WarUnit>();
		// final Vector<WarUnit> friendUnits = new Vector<WarUnit>();
		// final Vector<WarUnit> enemyUnits = new Vector<WarUnit>();
		// Vector<SensorsModel> sensors = new Vector<SensorsModel>();
		while (it.hasNext()) {
			final Object unit = it.next();
			if (unit instanceof WarUnit) {
				final WarUnit war = (WarUnit) unit;
				// TODO Simplify coordinate calculations
				final Point loc = RootMapPart.convertToMap(getRootMap(), war);
				final SensorsModel sensors = war.getSensorInformation();
				int range;
				if (sensors.getRadarState()) {
					range = sensors.getSensorRange(HarpoonConstants.RADAR_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonConstants.COL_RADAR);
				}
				if (sensors.getSonarState()) {
					range = sensors.getSensorRange(HarpoonConstants.SONAR_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonConstants.COL_SONAR);
				}
				if (sensors.getRadioState()) {
					range = sensors.getSensorRange(HarpoonConstants.RADIO_TYPE);
					drawSensorRange(graphics, loc, range, HarpoonColorConstants.ECM_COLOR);
				}
			}
		}

		// // - For every sensor check if it detects an enemy
		// final Iterator<WarUnit> warIt = warUnits.iterator();
		// while (warIt.hasNext()) {
		// final WarUnit war = warIt.next();
		// final Point loc = RootMapPart.convertToMap(getRootMap(), war);
		// // - Get the sensors that are active
		// final SensorsModel sensors = war.getSensorInformation();
		// int range;
		// if (sensors.getRadarState()) {
		// // - Get radar range from the model data
		// range = war.getSensorRange(HarpoonConstants.RADAR_TYPE);
		// drawSensorRange(graphics, loc, range, HarpoonConstants.COL_RADAR);
		// }
		// if (sensors.getSonarState()) {
		// // - Get radar range from the model data
		// range = war.getSensorRange(HarpoonConstants.SONAR_TYPE);
		// drawSensorRange(graphics, loc, range, HarpoonConstants.COL_SONAR);
		// }
		// if (sensors.getRadioState()) {
		// // - Get radar range from the model data
		// range = war.getSensorRange(HarpoonConstants.RADIO_TYPE);
		// drawSensorRange(graphics, loc, range, HarpoonColorConstants.ECM_COLOR);
		// }
		// // Iterator<WarUnit> enemyIt = enemyUnits.iterator();
		// // while (enemyIt.hasNext()) {
		// // WarUnit enemy = enemyIt.next();
		// // // - Get the locations of both elements.
		// // DMSPoint epoint = new DMSPoint(enemy.getDMSLatitude(), enemy.getDMSLongitude());
		// // DMSPoint spoint = new DMSPoint(sensor.getReferent().getDMSLatitude(), sensor.getReferent()
		// // .getDMSLongitude());
		// // DMSVector v = new DMSVector(spoint, epoint);
		// // int range = sensor.getRange();
		// // if (v.getModule() < range) {
		// // // - This enemy is detected
		// // HarpoonLogger.info("Enemy detected");
		// // // enemy.setDetected(true);
		// // }
		// }

	}

	/**
	 * Draws the circle that matches the range selected.
	 * 
	 * @param graphics
	 * 
	 * @param color
	 * @param range
	 * @param location
	 * 
	 * @param war
	 * 
	 * @param range
	 *          the range for the sensor for this unit at this moment. It is expressed in nautical miles.
	 * @param radarColor
	 *          the color to use to draw the range. The color is from the table of configured colors.
	 */
	private void drawSensorRange(final Graphics graphics, final Point loc, final int range, final Color color) {
		// - Get the location where it is located the center of the sensor.
		// - This is the Unit location without biasing or the Figure location with biasing the hotspot.
		// DMSCoordinate lat = war.getDMSLatitude();
		final Point location = loc.getCopy();
		final RootMapUnit modelRoot = (RootMapUnit) getRootMap().getModel();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();
		final int height = new Double(256 * range * 60.0 / latSpan).intValue();
		final int width = new Double(256 * range * 60.0 / lonSpan).intValue();
		location.x = location.x - width / 2;
		location.y = location.y - height / 2;
		final Rectangle bound = new Rectangle(location, new Dimension(width, height));
		graphics.setForegroundColor(color);
		graphics.drawOval(bound);
	}

	private RootMapPart getRootMap() {
		// TODO Auto-generated method stub
		return rootMapPart;
	}

	/**
	 * Receives the properties to configure the Map from the Model storage that where received from the scenery
	 * properties configuration file.
	 */
	public void setConfiguration(final Properties mapProperties) {
		if (null == mapProperties)
			props = new Properties();
		else
			props = mapProperties;
		// - Copy the properties to the cahe that is the class that will meke use of them.
		mapCache.setConfiguration(props);
	}

	//- CLASS IMPLEMENTATION ...................................................................................
	class GoogleMapCache {

		private Properties			properties		= new Properties();
		private final Hashtable	mapBlockCache	= new Hashtable();
		private int							accesses			= 0;
		private int							misses				= 0;
		private int							hits					= 0;
		private int							topX					= -1;
		private int							topY					= -1;

		public void setConfiguration(final Properties props) {
			// TODO Auto-generated method stub
			properties = props;
			//- Clear already cached properties.
			topX = -1;
			topY = -1;
		}

		/**
		 * Gets a block of Google map data with coordinates relative to the initial start point of the scenery.
		 * The initial coordinates from the top-left block are defined on the properties. This will get a block
		 * <code>i</code> spaces to the right of this top-left corner and <code>j</code> spaces below that
		 * same block.
		 * 
		 * @throws IOException
		 * @throws MalformedURLException
		 */
		public ImageData getBlock(final int i, final int j) throws MalformedURLException, IOException {
			accesses++;
			// - Generate the URL to get the data form Google. This is also the cache key.
			// DEBUG Removed the load of images to reduce startup time
			String mapURL = "";
			if (true)
				mapURL = getURLReference(getTopX() + 1, getTopY() + 1);
			else
				mapURL = getURLReference(getTopX() + i, getTopY() + j);
			ImageData block = (ImageData) mapBlockCache.get(mapURL);
			if (null == block) {
				// - Miss in the cache data. Got to Google for the information.
				misses++;
				block = readBlockData(mapURL);
				mapBlockCache.put(mapURL, block);
			} else
				hits++;
			return block;
		}

		private int getTopX() {
			if (-1 == topX) {
				final String propValue = properties.getProperty("topX", "52");
				final Integer numberValue = new Integer(propValue);
				try {
					topX = numberValue.intValue();
				} catch (final NumberFormatException nfe) {
					topX = 0;
				}
			}
			return topX;
		}

		private int getTopY() {
			if (-1 == topY) {
				final String propValue = properties.getProperty("topY", "26");
				final Integer numberValue = new Integer(propValue);
				try {
					topY = numberValue.intValue();
				} catch (final NumberFormatException nfe) {
					topY = 0;
				}
			}
			return topY;
		}

		@Override
		public String toString() {
			return "GoogleMapCache stats; (" + accesses + "," + hits + "," + misses + ")";
		}

	}

	public void setRootMap(final RootMapPart rootMapPart) {
		this.rootMapPart = rootMapPart;
	}
}

// - UNUSED CODE ............................................................................................
// @Override
// protected void paintChildren(Graphics graphics) {
// // TODO Auto-generated method stub
// super.paintChildren(graphics);
// }

