//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/WarPart.java,v $
//  LAST UPDATE:    $Date: 2007-10-18 16:53:42 $
//  RELEASE:        $Revision: 1.8.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.8  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.7  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.6  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.harpoon.pages.WarPropertyPage;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class WarPart extends MovablePart {
	// - O V E R R I D E S E C T I O N
	// [03]
	/** Creates a set of SWT controls that representS the visible and editable properties for this Unit. */
	public Composite createPropertyPage(final Composite top) {
		// - Create a new set of controls for this EditPart.
		final WarPropertyPage page = new WarPropertyPage(top);
		page.setContainer(top);
		page.setModel((WarUnit) getModel());
		page.build();
		return page.getPropertyPage();
	}

	// [04]
	/**
	 * Any change on the sensor properties (received at this level by sensor delegation) only has to fire the
	 * repainting of the Map to update the location and visibility of the sensor ranges. No controll Part is
	 * created for sensors.
	 * 
	 * @see net.sourceforge.harpoon.parts.MovablePart#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (WarUnit.FLD_SENSORS.equals(prop)) {
			// - A change on the sensors requires to redraw the Map.
			getRootPart().getFigure().repaint();
		}
		super.propertyChange(evt);
	}
	// [02]
}

// - UNUSED CODE ............................................................................................
// [02]
//		
// // super.propertyChange(evt);
// String prop = evt.getPropertyName();
// if (WarUnit.RADAR.equals(prop))
// activateSensor(WarUnit.RADAR);
// else if (WarUnit.SONAR.equals(prop))
// activateSensor(WarUnit.SONAR);
// else if (WarUnit.ECM.equals(prop))
// activateSensor(WarUnit.ECM);
// else if (WarUnit.DETECTED.equals(prop))
// refreshVisuals();
//
// // - Causes Graph to re-layout
// ((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
// }
// public void activateSensor(String sensor) {
// if(sensor.equals(WarUnit.RADAR)) {
// //- Create a new sensor and add it to the controller through the RoorUnit
// SensorsModel newSensor = new SensorsModel(getUnit());
// newSensor.setRadarState(true);
// newSensor.setRange(10);
// // RootEditPart root = getRoot();
// EditPart r = getParent();
// // Object mod = root.getModel();
// RootMapPart root = (RootMapPart) r;
// RootMapUnit rootUnit = (RootMapUnit) root.getModel();
// //FIXME This is the key point.
// rootUnit.addChild(newSensor);
// }
// }

// [03]
/**
 * Update the visual representation of the Units (the Figure part) from the model data. This spplies for new
 * fields on this part and inheritance updates the rest of the fields.
 * 
 * @see net.sourceforge.harpoon.parts.MovablePart#refreshVisuals()
 */
// protected void refreshVisuals() {
// super.refreshVisuals();
// // - The references to the model and figure objects.
// WarFigure fig = (WarFigure) this.getUnitFigure();
// WarUnit model = (WarUnit) this.getUnit();
//		
// //- Update figure visuals from current model data.
// // fig.setRadarState(model.getRadarState());
// // fig.setECMState(model.getECMState());
//
// //- Get the model root where there are stored the conversion properties.
// RootMapPart rootPart=(RootMapPart) this.getParent();
// RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
// DMSCoordinate topLat = modelRoot.getDMSLatitude();
// DMSCoordinate topLon = modelRoot.getDMSLongitude();
// long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
// long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();
//		
// //- Generate the radar ranges converted to pixels
// int radarRange = 20;
// int radarSeconds = radarRange*60;
// int radarPixels = new Double(256 * radarSeconds / latSpan).intValue();
// int ECMRange=80;
// int ECMSeconds = ECMRange*60;
// int ECMPixels = new Double(256 * ECMSeconds / latSpan).intValue();
//		
// fig.setRadarRange(radarPixels);
// fig.setECMRange(ECMPixels);
// }
// [04]
// public void activate() {
// if (isActive()) return;
// // - Start listening for changes in the model.
// getUnit().addPropertyChangeListener(this);
// super.activate();
// }
// public void deactivate() {
// if (!isActive()) return;
// // - Stop listening to events in the model.
// getUnit().removePropertyChangeListener(this);
// super.deactivate();
// }
