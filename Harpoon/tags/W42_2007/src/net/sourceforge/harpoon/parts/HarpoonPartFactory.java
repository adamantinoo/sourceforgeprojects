//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPartFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/HarpoonPartFactory.java,v $
//  LAST UPDATE:    $Date: 2007-10-16 14:47:14 $
//  RELEASE:        $Revision: 1.13.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.13.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.13.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.13  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.12  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.11  2007-10-03 12:36:52  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.10  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.9  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.8  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.7  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.ReferencePoint;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.harpoon.model.Wire;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class implements the EditPart factory that is a requisite of the GEF model structure. Once we connect
 * a content to a Viewer it will start to call this factory to create the EditParts of the Controller side of
 * the GEF work model.<br>
 * The class has a global access element that is the root element of the class <code>RootMapPart</code> that
 * is references by any Part instance to access some global information and coordinate axis location.
 */
public class HarpoonPartFactory implements EditPartFactory {
	private static Logger	logger		= Logger.getLogger("net.sourceforge");
	// _ Create and initialize an empty but runnable RootMapPart
	private RootMapPart		rootPart	= new RootMapPart();
	{
		rootPart.setModel(new RootMapUnit());
		rootPart.createEmptyFigure();
		rootPart.setRootPart(rootPart);
	}

	// - O V E R R I D E S
	/**
	 * This method creates any of the EditParts in the application depending on the class of the element model
	 * received as the parameter. This is the main point where we assemble the model to the internal operating
	 * model used by GEF.
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context, final Object model) {
		logger.log(Level.FINE, "Creating EditPart for model type: " + model.getClass().getSimpleName());
		GamePart part = null;

		// - If the EditPart requested is the root diagram EditPart then set it as the diagram root.
		if (model instanceof RootMapUnit) {
			// logger.log(Level.FINE, "Creating root EditPart");
			part = new RootMapPart();
			rootPart = (RootMapPart) part;
			rootPart.setRootPart(rootPart);
		} else if (model instanceof WarUnit)
			part = new WarPart();
		else if (model instanceof MovableUnit)
			part = new MovablePart();
		else if (model instanceof AirportUnit)
			part = new AirportPart();
		else if (model instanceof ReferencePoint)
			part = new ReferencePart();
		// else if (model instanceof MovementPath) part = new PathPart();
		else if (model instanceof Wire) {
			final WireEditPart wire = new WireEditPart(model);
			wire.setModel(model);
			wire.setRootPart(rootPart);
			return wire;
		}

		// - Initialize the part setting the model and the root EditPart of the internal model.
		part.setModel(model);
		part.setRootPart(rootPart);
		return part;
	}

	// - CLASS IMPLEMENTATION .................................................................................
	// class PathPart extends GamePart {
	//
	// // private MovementPath path;
	// //
	// // public PathPart(Object model) {
	// // this.path=(MovementPath) model;
	// // }
	//
	// @Override
	// protected IFigure createFigure() {
	// return new MovementPathFigure(this, (MovementPath) getModel());
	// }
	//
	// protected void refreshVisuals() {
	// super.refreshVisuals();
	// // - The references to the model and figure objects.
	// // ReferenceFigure fig = getUnitFigure();
	// // ReferencePoint model = getModel();
	//
	// // fig.setBounds(new Rectangle)
	// // fig.setVisible(true);
	// }
	//
	// public void propertyChange(final PropertyChangeEvent evt) {
	// // TODO Auto-generated method stub
	//
	// }
	// }
	//
	// class MovementPathFigure extends UnitFigure {
	//
	// private final PathPart part;
	// private final MovementPath path;
	//
	// public MovementPathFigure(final PathPart part, final MovementPath path) {
	// this.part = part;
	// this.path = path;
	// this.setSize(this.getPreferredSize());
	// }
	//
	// protected void paintFigure(final Graphics graphics) {
	// final RootMapPart rootPart = part.getRootPart();
	// final Iterator<DMSPoint> it = path.getPointsIterator();
	// Point from = getLocation();
	// while (it.hasNext()) {
	// final Point to = RootMapPart.coordinateToMap(rootPart, it.next());
	// final Rectangle bound = new Rectangle(to.x - 3, to.y - 3, 7, 7);
	// // - Draw the path handle
	// graphics.setForegroundColor(ColorConstants.gray);
	// graphics.drawOval(bound);
	// graphics.drawLine(from, to);
	// from = to;
	// }
	// }
	//
	// public Point getHotSpot() {
	// return new Point(0, 0);
	// }
	//
	// public Dimension getPreferredSize(final int hint, final int hint2) {
	// // TODO Auto-generated method stub
	// return new Dimension(600, 600);
	// }
	//
	// }
}
// - UNUSED CODE ............................................................................................
