//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovablePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/MovablePart.java,v $
//  LAST UPDATE:    $Date: 2007-10-18 16:53:42 $
//  RELEASE:        $Revision: 1.5.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.5.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.5  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.4  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;

import net.sourceforge.harpoon.figures.HarpoonFigureFactory;
import net.sourceforge.harpoon.figures.MovableFigure;
import net.sourceforge.harpoon.model.MovableUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
public class MovablePart extends UnitPart {
	// - P U B L I C S E C T I O N
	// - O V E R R I D E - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.harpoon.parts.UnitPart#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (MovableUnit.MODEL.equals(prop))
			refreshVisuals();
		else if (MovableUnit.DIRECTION.equals(prop))
			refreshVisuals();
		else if (MovableUnit.SPEED.equals(prop))
			refreshVisuals();
		else if (MovableUnit.MOVEMENTPATH.equals(prop)) refreshVisuals();

		// - Causes Graph to re-layout
		// ((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
		super.propertyChange(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.harpoon.parts.UnitPart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		final MovableFigure fig = (MovableFigure) getUnitFigure();
		final MovableUnit model = (MovableUnit) getUnit();

		// - Update figure visuals from current model data.
		fig.setSpeed(model.getSpeed());
		fig.setDirection(model.getDirection());
		fig.setMovementPath(model.getMovementPath());
	}

	// - P R I V A T E - S E C T I O N
	/**
	 * Creation of the figure depends on some subtle model information to set the right icon to the common
	 * presentation and model unit. A MovableUnit model unit may represent many unit types, detecting which one
	 * is the right one to create the proper visible figure and property page depends on this method.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		final MovableUnit unit = (MovableUnit) getModel();

		// - Identify the subtype for this model.
		final String subType = unit.getUnitType();

		// - Create and initialize the figure
		final Figure fig = HarpoonFigureFactory.createFigure(this, unit, subType);
		// refreshVisuals();
		// // HarpoonLogger.log(Level.FINE, "Creating Figure ShipFigure");
		// fig.setSide(unit.getSide());
		// fig.setDirection(unit.getDirection());
		// fig.setSpeed(unit.getSpeed());
		// HarpoonLogger.log(Level.FINE, fig.toString());
		return fig;
	}

	// [01]

	/**
	 * Changes in the selection of MovableUnits change the elements in the Model. Activate this unit in the
	 * model to generate the new MovementPath Parts that have to be drawn in the new model.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 * @see net.sourceforge.harpoon.parts.GamePart#setSelected(int)
	 */
	public void setSelected(final int value) {
		super.setSelected(value);

		// - Get the model and update the selection status. Activate if we are being selected only.
		if (0 != value) {
			final MovableUnit model = (MovableUnit) getModel();
			model.setSelected(value);
		}
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// public void activate() {
// if (isActive()) return;
// // - Start listening for changes in the model.
// getUnit().addPropertyChangeListener(this);
// super.activate();
// }
// public void deactivate() {
// if (!isActive()) return;
// // - Stop listening to events in the model.
// getUnit().removePropertyChangeListener(this);
// super.deactivate();
// }
