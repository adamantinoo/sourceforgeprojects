//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovementPath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/MovementPath.java,v $
//  LAST UPDATE:    $Date: 2007-10-18 16:53:42 $
//  RELEASE:        $Revision: 1.2.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.2.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.2  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The MovementPath is a model element that stores the list of points that compose a unit path. A path is a
 * series of lines with associated commands that form the movement commands assigned to a unit.<br>
 * The unit direction is then derived from this list of commands. The termination of the list creates a new
 * virtual command to continue to the same direction indefinitely or in case there is a command associated to
 * the point, doing the same command action until changed.
 */
public class MovementPath extends Unit implements Serializable {
	private static final long									serialVersionUID	= 2924232465880867103L;

	/** List of points that compose the movement path for a unit. */
	// private final LinkedList<DMSPoint> points = new LinkedList<DMSPoint>();
	/** The list of reference model points that conform the movement path. */
	private final LinkedList<ReferencePoint>	referencePoints		= new LinkedList<ReferencePoint>();
	private final Vector<Wire>								wires							= new Vector<Wire>();
	/**
	 * Target Unit that applies to the path commands. Required to have a backlink to the unit to know the first
	 * reference point in the command path.
	 */
	private final MovableUnit									target;

	// - C O N S T R U C T O R S
	public MovementPath(final MovableUnit target) {
		this.target = target;
	}

	// - G E T T E R S / S E T T E R S
	public LinkedList<ReferencePoint> getPoints() {
		return referencePoints;
	}

	public void setSelected(final boolean selected) {
		final Iterator<ReferencePoint> pointIt = referencePoints.iterator();
		while (pointIt.hasNext()) {
			pointIt.next().setParentselected(selected);
		}
		final Iterator<Wire> wireIt = wires.iterator();
		while (wireIt.hasNext()) {
			final Wire wire = wireIt.next();
			wire.setVisible(selected);
		}
	}

	public Vector<Object> getModelChildren() {
		final Vector<Object> children = new Vector<Object>();
		children.addAll(getPoints());
		// children.addAll(wires);
		return children;
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Adds a new point to the command path. Teh point is added to the end of the chain. If the chain is empty
	 * then this is the first destination point and the current unit location os the from point.
	 */
	public void addPoint(final DMSPoint point) {
		if (referencePoints.isEmpty()) {
			// - Get the current location of the Unit as the first from point for this chain.
			final DMSPoint targetLocation = new DMSPoint(target.getDMSLatitude(), target.getDMSLongitude());
			referencePoints.addLast(convert2reference(targetLocation));
		}
		// - Connect the two points with a Wire.
		final ReferencePoint refPoint = convert2reference(point);
		final Wire conn = new Wire(referencePoints.getLast(), refPoint);
		wires.add(conn);
		referencePoints.addLast(refPoint);
	}

	public void addPoint(final DMSCoordinate latitude, final DMSCoordinate longitude) {
		addPoint(new DMSPoint(latitude, longitude));
	}

	/**
	 * Calculates the direction resulting from the calculation of the vector from the current point to the next
	 * point in the path.
	 * 
	 * @param startingPoint
	 *          the current position of the unit. This is used as the start point og the movement vector.
	 * @return
	 */
	public int calculateDirection(final DMSPoint startingPoint) {
		// - If path is empty default to direction 0�.
		if (referencePoints.isEmpty()) return 0;
		final DMSPoint nextPoint = getNextPoint();
		// - Check that there are enough points.
		if (null == nextPoint) return 0;

		// - Translate coordinates to point P1
		final DMSPoint vector = nextPoint.offset(startingPoint);

		// - Calculate direction depending on the vector quadrant
		final long lat = vector.getLatitude().toSeconds();
		final long lon = vector.getLongitude().toSeconds();
		//		final int quadrant = calculateVectorQuadrant(lat, lon);
		final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
		double beta = 0.0;
		if (lat > lon) {
			beta = StrictMath.acos(lat / h);
		} else {
			beta = StrictMath.asin(lon / h);
		}
		//		switch (quadrant) {
		//			case 0:
		//			case 1:
		//				// - Check if > 45�
		//				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
		//					final double alpha = StrictMath.asin(lon / h);
		//					double beta;
		//					if (lat > lon) {
		//						beta = StrictMath.acos(lat / h);
		//					} else {
		//						beta = StrictMath.asin(lon / h);
		//					}
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				} else {
		//					// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
		//					final double alpha = StrictMath.acos(lat / h);
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				}
		//			case 2:
		//				// - Check if < 135�
		//				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
		//					final double alpha = StrictMath.acos(lon / h);
		//					return new Double(StrictMath.toDegrees(alpha) + 90.0).intValue();
		//				} else {
		//					final double alpha = StrictMath.asin(lat / h);
		//					return new Double(StrictMath.toDegrees(alpha) + 90.0).intValue();
		//				}
		//			case 3:
		//				// - Check if < 225�
		//				if (StrictMath.abs(lat) > StrictMath.abs(lon)) {
		//					final double alpha = StrictMath.asin(lon / h) + 180.0;
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				} else {
		//					final double alpha = StrictMath.acos(lat / h) + 180.0;
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				}
		//			case 4:
		//				// - Check if < 315
		//				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
		//					final double alpha = StrictMath.acos(lon / h) + 270.0;
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				} else {
		//					final double alpha = StrictMath.asin(lat / h) + 270.0;
		//					return new Double(StrictMath.toDegrees(alpha)).intValue();
		//				}
		//		}
		return new Double(StrictMath.toDegrees(beta)).intValue();
	}

	// [01]

	// - P R I V A T E - S E C T I O N
	/** Creates a new model ReferencePoint form a DMS location. */
	protected ReferencePoint convert2reference(final DMSPoint point) {
		final ReferencePoint refPoint = new ReferencePoint();
		refPoint.setLatitude(point.getLatitude());
		refPoint.setLongitude(point.getLongitude());
		refPoint.setSide(Unit.PATH);
		return refPoint;
	}

	/**
	 * Returns the first movement destination point. The first point was the unit location in the moment the
	 * first point was added. That first point in the point returned by this method, or any other point edited
	 * since then.
	 * 
	 * @return the first movement destination point in the path.
	 */
	protected DMSPoint getNextPoint() {
		// - Get the second point that is the direction from the current location (the first point).
		final ReferencePoint to = referencePoints.get(1);
		return new DMSPoint(to.getDMSLatitude(), to.getDMSLongitude());
	}

	protected int calculateVectorQuadrant(final long vectorLat, final long vectorLon) {
		// - Calculate quadrant depending on the point signs.
		int quadrant = 0;
		if ((vectorLat >= 0) && (vectorLon >= 0))
			quadrant = 1;
		else if ((vectorLat >= 0) && (vectorLon < 0))
			quadrant = 4;
		else if ((vectorLat < 0) && (vectorLon >= 0))
			quadrant = 2;
		else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
		return quadrant;
	}

	// [02]
	// [03]
}

// - CLASS IMPLEMENTATION ...................................................................................
// class NNLinkedList extends LinkedList<DMSPoint> {
// private static final long serialVersionUID = -1433126363814347456L;
//
// }
// - UNUSED CODE ............................................................................................
// [01]
// public Iterator<AbstractGraphicalEditPart> getPathPartIterator() {
// final Vector<AbstractGraphicalEditPart> pointParts = new Vector<AbstractGraphicalEditPart>();
// final Iterator<DMSPoint> it = points.iterator();
// while (it.hasNext()) {
// // HarpoonPartFactory.
// final PointPart part = new PointPart();
// part.setModel(it.next());
// // part.setRootPart(part);
// pointParts.add(part);
// }
// return pointParts.iterator();
// }

// [02]
// /**
// * Return the list of PointUnits that contain the movement path points as units that also will generate the
// * corresponding Parts.
// */
// public Vector<PropertyModel> getReferencePoints(final MovableUnit movable) {
// final Vector<PropertyModel> pp = new Vector<PropertyModel>();
// ReferencePoint from = null;
//
// final MovementPath mp = movable.getMovementPath();
// final LinkedList<DMSPoint> points = mp.getPoints();
// final Iterator<DMSPoint> it = points.iterator();
// while (it.hasNext()) {
// final DMSPoint point = it.next();
// final ReferencePoint to = new ReferencePoint();
// to.setLatitude(point.getLatitude());
// to.setLongitude(point.getLongitude());
// to.setSide(Unit.PATH);
// pp.add(to);
// if (null != from) {
// // - Connect two adjacent points
// final Wire conn = new Wire(from, to);
// // pp.add(conn);
// }
// from = to;
// }
// return pp;
// }

// [03]
// public void draw(final Graphics graphics, final MovableFigure figure) {
// final RootMapPart rootPart = figure.getPart().getRootPart();
// final Iterator<DMSPoint> it = points.iterator();
// Point from = figure.getLocation();
// while (it.hasNext()) {
// final Point to = RootMapPart.coordinateToMap(rootPart, it.next());
// final Rectangle bound = new Rectangle(to.x - 3, to.y - 3, 7, 7);
// // - Draw the path handle
// graphics.setForegroundColor(ColorConstants.gray);
// graphics.drawOval(bound);
// graphics.drawLine(from, to);
// from = to;
// }
// }

// public Iterator<DMSPoint> getPointsIterator() {
// return points.iterator();
// }

// public void paint() {
// // TODO Auto-generated method stub
//
// }
