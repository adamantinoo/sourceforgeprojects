//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ReferencePoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/ReferencePoint.java,v $
//  LAST UPDATE:    $Date: 2007-10-15 14:23:21 $
//  RELEASE:        $Revision: 1.1.2.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.1  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.ArrayList;
import java.util.List;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public class ReferencePoint extends Unit {
	private static final long		serialVersionUID				= 3185696789228033997L;

	/** Property ID to use when the list of outgoing connections is modified. */
	public static final String	SOURCE_CONNECTIONS_PROP	= "ReferencePoint.SOURCE_CONNECTIONS_PROP";
	/** Property ID to use when the list of incoming connections is modified. */
	public static final String	TARGET_CONNECTIONS_PROP	= "ReferencePoint.TARGET_CONNECTIONS_PROP";
	public static final String	WIDTH_PROP							= "ReferencePoint.WIDTH_PROP";
	private static final int		width										= 12;
	// /** List of outgoing Connections. */
	// private PathConnectionModel sourceConn = null;
	// /** List of incoming Connections. */
	// private PathConnectionModel targetConn = null;
	/** List of outgoing Connections. */
	private final List					sourceConnections				= new ArrayList();
	/** List of incoming Connections. */
	private final List					targetConnections				= new ArrayList();
	/**
	 * This field replicates the state of the selection for the unit parent. If the MovableUnit is selected then
	 * all children are in the state of parent state selected.
	 */
	private boolean							parentselected					= false;																		;

	// - G E T T E R S / S E T T E R S
	public int getWidth() {
		return width;
	}

	// public void addSourceConnection(final PathConnectionModel conn) {
	// // Assert.isNUll
	// sourceConn = conn;
	// }
	//
	// public void addTargetConnection(final PathConnectionModel conn) {
	// // Assert.isNUll
	// targetConn = conn;
	// }
	//
	// public void removeSourceConnection(final PathConnectionModel conn) {
	// if (sourceConn == conn) sourceConn = null;
	// }
	//
	// public void removeTargetConnection(final PathConnectionModel conn) {
	// if (targetConn == conn) targetConn = conn;
	// }

	public boolean getParentSelected() {
		return parentselected;
	}

	public void setParentselected(final boolean parentselected) {
		this.parentselected = parentselected;
		firePropertyChange(TARGET_CONNECTIONS_PROP, null, parentselected);
	}

	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	void addConnection(final Wire conn) {
		if ((conn == null) || (conn.getSource() == conn.getTarget())) { throw new IllegalArgumentException(); }
		if (conn.getSource() == this) {
			sourceConnections.add(conn);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, null, conn);
		} else if (conn.getTarget() == this) {
			targetConnections.add(conn);
			firePropertyChange(TARGET_CONNECTIONS_PROP, null, conn);
		}
	}

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	void removeConnection(final Wire conn) {
		if (conn == null) { throw new IllegalArgumentException(); }
		if (conn.getSource() == this) {
			sourceConnections.remove(conn);
			firePropertyChange(SOURCE_CONNECTIONS_PROP, null, conn);
		} else if (conn.getTarget() == this) {
			targetConnections.remove(conn);
			firePropertyChange(TARGET_CONNECTIONS_PROP, null, conn);
		}
	}

	/**
	 * Return a List of outgoing Connections.
	 */
	public List getSourceConnections() {
		return new ArrayList(sourceConnections);
	}

	/**
	 * Return a List of incoming Connections.
	 */
	public List getTargetConnections() {
		return new ArrayList(targetConnections);
	}

}

// - UNUSED CODE ............................................................................................
