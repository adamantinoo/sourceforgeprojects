//  PROJECT:        DigramaCicloFacturacion
//  FILE NAME:      $RCSfile: HarpoonLogFormatter.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/log/HarpoonLogFormatter.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:06 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-04-23 08:05:00  ldiego
//    - Removed an extra SPACE on the output of lines header.
//
//    Revision 1.1  2006/09/26 13:32:18  ldiego
//    - Class to format the log generated by a project Logger.
//

package harpoonrcp;

//... IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.logging.*;

//import wdoo.log.WdooFormater;

//... CLASS IMPLEMENTATION ...................................................................................
public class HarpoonLogFormatter extends SimpleFormatter {
	public static boolean				includeCaller		= false;
	public static boolean				includeThread		= false;
	private static StringBuffer	ruler						= new StringBuffer(
																									"................................................................................");
	private static boolean			rulerActivated	= false;

	public String format(LogRecord logrec) {
		// ... Compose the log message to be displayed on the console.
		// The message format will be like this
		// TIMESTAMP LEVEL - MESSAGE
		StringBuffer outMessage = new StringBuffer();
		if (rulerActivated) {
			outMessage.append(ruler).append('\n');
		}
		//... TIMESTAMP
		Date timeStamp = new Date(logrec.getMillis());
		outMessage.append(timeStamp.toString()).append(" ");
/*		Object[] params = logrec.getParameters();
		StringBuffer paramString = new StringBuffer();
		if (params != null) {
			int paramsSize = params.length;
			for (int i = 0; i < params.length; i++) {
				paramString.append(params[i].toString());
			}
		}*/
		//... THREADID
		if (includeThread) {
			outMessage.append(logrec.getThreadID()).append(" ");
		}
		//... LEVEL
		outMessage.append(logrec.getLevel().getName()).append(" - ");
		//... CALLER
		// ... Add this lines if we want in the log the name of the calling method and class or if the log level requestes is FINER
		if (includeCaller || (logrec.getLevel().getName().startsWith("FINER"))||(logrec.getLevel().getName().startsWith("FINEST"))){
			outMessage.append(logrec.getSourceClassName()).append(" ").append(
					logrec.getSourceMethodName());
			outMessage.append(" - ");
		}
		//... PARAMETERS
//		outMessage.append(paramString).append(" ");
		//... MESSAGE
		outMessage.append(logrec.getMessage());
		outMessage.append('\n');

		rulerActivated = false;
		return outMessage.toString();
	}

	/** Print out a ruler to the output. Store this code initially on a buffer to wait for new output. */
	public static void ruler() {
		rulerActivated = true;
	}
}

// ... U N U S E D C O D E
