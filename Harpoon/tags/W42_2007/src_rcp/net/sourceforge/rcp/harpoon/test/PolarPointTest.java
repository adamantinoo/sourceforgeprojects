//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PolarPointTest.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/PolarPointTest.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:25:14  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.tests;

//- IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.geom.PolarPoint;

import junit.framework.TestCase;

// - CLASS IMPLEMENTATION ...................................................................................
public class PolarPointTest extends TestCase {

	public void testCreatePolarCoordinate() throws Exception {
		PolarCoordinate coord1 = new PolarCoordinate(30, 10, 20);
		PolarCoordinate coord2 = PolarCoordinate.fromDMS(30, 10, 20);

		assertEquals(coord1.toDegrees(), coord2.toDegrees());
	}

	public void testConversion() throws Exception {
		PolarCoordinate coord = new PolarCoordinate(30, 10, 20);

		double expected1 = new Double(20.0 * 1.0 / 60.0 * 1.0 / 60.0 + 10.0 * 1.0 / 60.0 + 30.0);
		assertEquals(expected1, coord.toDegrees(), 0.000000001);
	}

	public void testAddLatitude() throws Exception {
		PolarPoint coordinate = new PolarPoint(PolarCoordinate.fromDMS(30, 10, 20), PolarCoordinate
				.fromDMS(40, 30, 40));

		coordinate.addLatitude(new PolarCoordinate(2, 1, 2));
		double expected1 = new Double(20.0 * 1.0 / 60.0 * 1.0 / 60.0 + 10.0 * 1.0 / 60.0 + 30.0);
		double expected2 = new Double(2.0 * 1.0 / 60.0 * 1.0 / 60.0 + 1.0 / 60.0 + 2.0);
		assertEquals(expected1 + expected2, coordinate.getLatitude().toDegrees(), 0.000000001);
	}

	public void testAddLongitude() throws Exception {
		PolarPoint coordinate = new PolarPoint(PolarCoordinate.fromDMS(30, 10, 20), PolarCoordinate
				.fromDMS(40, 30, 40));

		coordinate.addLongitude(new PolarCoordinate(2, 1, 2));
		double expected1 = new Double(40.0 * 1.0 / 60.0 * 1.0 / 60.0 + 30.0 * 1.0 / 60.0 + 40.0);
		double expected2 = new Double(2.0 * 1.0 / 60.0 * 1.0 / 60.0 + 1.0 / 60.0 + 2.0);
		assertEquals(expected1 + expected2, coordinate.getLongitude().toDegrees(), 0.000000001);
	}

	public void testOperations() throws Exception {
		PolarCoordinate initialCoord = new PolarCoordinate(10, 20, 30); // 10 20'30''
		// PolarCoordinate limitCoord0 = new PolarCoordinate(0, 1, 2); //0 01'02''
		PolarCoordinate limitCoord180 = new PolarCoordinate(179, 50, 55); // 179 50'55''
		PolarCoordinate limitCoord360 = new PolarCoordinate(359, 50, 55); // 359 50'55''

		// - Basic arithmetic operations
		double degress1 = new Double(30.0 * 1.0 / 60.0 * 1.0 / 60.0 + 20.0 * 1.0 / 60.0 + 10.0); // 10 20'30''
		double degress2 = new Double(1.0 * 1.0 / 60.0 * 1.0 / 60.0 + 2.0 * 1.0 / 60.0 + 3.0); // 3 2'1''
		double expected1 = degress1 + degress2;
		initialCoord.addDegrees(degress2);
		assertEquals("Checking the addition of degrees", expected1, initialCoord.toDegrees(),
				0.000000001);

		initialCoord.subDegrees(degress2);
		assertEquals("Checking the substraction of degrees", degress1, initialCoord.toDegrees(),
				0.000000001);

		// - Limit overflow when coordinates are outside world range (360 degrees for latitude and 90 for
		// longitude).
		PolarPoint initialCoordinate180 = new PolarPoint(limitCoord180, limitCoord180); // 179 50'55'' 179 50'55''
		PolarPoint initialCoordinate360 = new PolarPoint(limitCoord360, limitCoord360); // 359 50'55'' 359 50'55''
		double degress3 = new Double(5.0 * 1.0 / 60.0 * 1.0 / 60.0 + 9.0 * 1.0 / 60.0 + 0.0); // 0 09'05''
		limitCoord180.addDegrees(degress3);
		limitCoord360.addDegrees(degress3);
		assertEquals("Checking addition to 180 00'00''", 180.0, limitCoord180.toDegrees(), 0.000000001);
		assertEquals("Checking addition to 360 00'00''", 360.0, limitCoord360.toDegrees(), 0.000000001);
		assertEquals("Checking addition to 180 00'00''", 180.0, initialCoordinate180.getLatitude()
				.toDegrees(), 0.000000001);
		assertEquals("Checking addition to 360 00'00''", 360.0, initialCoordinate360.getLongitude()
				.toDegrees(), 0.000000001);

		limitCoord180.addDegrees(degress3);
		limitCoord360.addDegrees(degress3);
		assertEquals("Checking addition past 180 00'00''", 180.0 + degress3, limitCoord180.toDegrees(),
				0.000000001);
		assertEquals("Checking addition past 360 00'00''", 360.0 + degress3, limitCoord360.toDegrees(),
				0.000000001);
		assertEquals("Checking latitude limit at 180", 180.0 + degress3, initialCoordinate180
				.getLatitude().toDegrees(), 0.000000001);
		initialCoordinate360.addLatitude(degress3);
		assertEquals("Checking latitude limit at 360", degress3 * 2, initialCoordinate360
				.getLongitude().toDegrees(), 0.000000001);
	}
}

// - UNUSED CODE ............................................................................................
