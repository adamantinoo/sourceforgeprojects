//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DisplacementVector.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/DisplacementVector.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:25:14  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.tests;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.geom.PolarPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class DisplacementVector {

	private static final double	SECONDS_HOUR	= 3600.0;
	private PolarPoint					startPoint;
	private PolarPoint					endPoint;
	private double							speed;
	private double							direction;
	private double							elapsedTime		= 0.0;

	public DisplacementVector(PolarPoint polarPoint, double speed, double direction) {
		// TODO Auto-generated constructor stub
		this.startPoint = polarPoint;
		this.speed = speed;
		this.direction = direction;
	}

	public void setTime(double time) {
		// TODO Auto-generated method stub
		this.elapsedTime = time;
	}

	public PolarCoordinate getEndPointLat() {
		double space = speed * this.elapsedTime / SECONDS_HOUR;
		PolarCoordinate startLat = new PolarCoordinate(this.startPoint.getLatitude().toDegrees());
		// - Check the quadrants to calculate the operation sign
		if ((this.direction > 90.0) && (this.direction < 270.0))
			startLat.subDegrees(space * Math.sin(UnitConversions.Deg2Rad(this.direction)));
		else
			startLat.addDegrees(space * Math.sin(UnitConversions.Deg2Rad(this.direction)));
		return startLat;
	}

	public PolarCoordinate getEndPointLon() {
		double space = speed * this.elapsedTime / SECONDS_HOUR;
		PolarCoordinate startLon = new PolarCoordinate(this.startPoint.getLongitude().toDegrees());
		// - Check the quadrants to calculate the operation sign
//	if ((this.direction >= 0.0) && (this.direction <= 180.0))
		if ((this.direction > 90.0) && (this.direction < 270.0))
			startLon.subDegrees(space * Math.cos(UnitConversions.Deg2Rad(this.direction)));
		else
			startLon.addDegrees(space * Math.cos(UnitConversions.Deg2Rad(this.direction)));
		return startLon;
	}

	public void setAngle(double angle) {
this.direction=angle;	}

}

// - UNUSED CODE ............................................................................................
