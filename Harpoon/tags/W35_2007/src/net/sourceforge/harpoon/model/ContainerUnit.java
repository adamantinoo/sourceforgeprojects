//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ContainerUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/ContainerUnit.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:24:52 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sourceforge.harpoon.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class ContainerUnit extends Unit {
	static final long serialVersionUID = 1;

	private static int count;

	protected List children = new ArrayList();

	public ContainerUnit() {}

	public void addChild(Unit child){
		addChild(child, -1);
	}

	public void addChild(Unit child, int index){
		if (index >= 0)
			children.add(index,child);
		else
			children.add(child);
//		fireStructureChange(CHILDREN, child);
	}

	public List getChildren(){
		return children;
	}

	public String getNewID() {
		return Integer.toString(count++);
	}

	public void removeChild(Unit child){
		children.remove(child);
//		fireStructureChange(CHILDREN, child);
	}

}

// - UNUSED CODE ............................................................................................
