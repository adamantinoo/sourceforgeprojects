//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/ShipUnit.java,v $
//  LAST UPDATE:    $Date: 2007-08-28 13:33:21 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public class ShipUnit extends Unit {
	private static final long	serialVersionUID	= 1L;

	/** Stores the speed at witch the unit is travelling. This is only valid for units that may move. */
	private int								speed							= 0;
	/** Direction of movement of this Unit. */
	private double							direction					= 0.0;
	private RootMapUnit				root;

	// private HarpoonMap map;

	// - C O N S T R U C T O R S
	// public ShipUnit() {}
	// public ShipUnit(HarpoonModel harpoonUnits, String unitName) {
	// super(harpoonUnits, unitName);
	// }
	// public ShipUnit(HarpoonModel harpoonUnits, String unitName, int side) {
	// super(harpoonUnits, unitName,side);
	// }

	public ShipUnit(RootMapUnit rootMapUnit) {
		// TODO Auto-generated constructor stub
		this.root = rootMapUnit;
	}

	// public ShipUnit(RootMapUnit rootMapUnit) {
	// // TODO Auto-generated constructor stub
	// this.map = rootMapUnit.getMap();
	// }
	//
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public double getDirection() {
		return direction;
	}

	public void setDirection(double dir) {
		this.direction = dir;
	}

	public RootMapUnit getRoot() {
		return this.root;
	}
	//
	// public HarpoonMap getMap() {
	// return this.map;
	// }
}

// - UNUSED CODE ............................................................................................
