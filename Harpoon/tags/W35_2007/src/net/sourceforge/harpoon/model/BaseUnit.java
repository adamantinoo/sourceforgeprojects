//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: BaseUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/BaseUnit.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:22 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.units;

import org.eclipse.draw2d.Figure;
import org.eclipse.swt.graphics.Color;

//... IMPORT SECTION .........................................................................................
//... CLASS IMPLEMENTATION ...................................................................................

public abstract class BaseUnit extends Figure {
	protected int posx = 0;
	protected int posy = 0;
	protected int scale = 1;
	protected String identifier = "BU";
	protected Color unitColor;
	
//... PUBLIC OPERATIONS ............................................................................
//... GETTER/SETTER PUBLIC OPERATIONS ..............................................................
	/**
	 * @return
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @return
	 */
	public int getPosx() {
		return posx;
	}

	/**
	 * @return
	 */
	public int getPosy() {
		return posy;
	}

	/**
	 * @return
	 */
	public Color getUnitColor() {
		return unitColor;
	}

	/**
	 * @param string
	 */
	public void setIdentifier(String string) {
		identifier = string;
	}

	/**
	 * @param i
	 */
	public void setPosx(int i) {
		posx = i;
	}

	/**
	 * @param i
	 */
	public void setPosy(int i) {
		posy = i;
	}

	/**
	 * @param color
	 */
	public void setUnitColor(Color color) {
		unitColor = color;
	}

	/**
	 * @return
	 */
	public int getScale() {
		// TODO Auto-generated method stub
		return this.scale;
	}

}

//... UNUSED CODE ......................................................................................................