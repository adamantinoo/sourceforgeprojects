//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: Aircraft.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/Aircraft.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:13 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.units;

import harpoon.devel.FigureDrawContext;

import net.sourceforge.harpoon.figures.AircraftFigure;
import net.sourceforge.harpoon.figures.BaseFigure;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Region;

//... IMPORT SECTION ...................................................................................................
//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class Aircraft extends BaseUnit {
	protected int altitude = 0;
	protected String name = "A";
	protected int[] shapePoints = { 1,-2,0,-2,3,-3,-3,0,0,-1,-2,0,0,1,-3,0,3,3 };
	protected BaseFigure shape;

	public Aircraft( int x, int y, String id ) {
		this.setPosx(x);
		this.setPosy(y);
		this.setName(this.getIdentifier() + id);
		this.shape = null;
	}
	/* (non-Javadoc)
	 * @see net.sourceforge.harpoon.figures.Figure#draw(net.sourceforge.harpoon.draw.FigureDrawContext)
	 */
	public void draw( FigureDrawContext fdc ) {
		// TODO Auto-generated method stub
		this.shape.draw( this.getPosx() + fdc.xOffset, this.getPosy() + fdc.yOffset, fdc.gc );
		
		//... Movement.
		this.posx += 2;
	}

	/* (non-Javadoc)
	 * @see net.sourceforge.harpoon.figures.Figure#addDamagedRegion(net.sourceforge.harpoon.draw.FigureDrawContext, org.eclipse.swt.graphics.Region)
	 */
	public void addDamagedRegion( FigureDrawContext fdc, Region region ) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * @return
	 */
	public int getAltitude() {
		return altitude;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param i
	 */
	public void setAltitude(int i) {
		altitude = i;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}
	@Override
	public void draw(int x, int y, GC gc) {
		// TODO Auto-generated method stub
		
	}

}

//... UNUSED CODE ......................................................................................................