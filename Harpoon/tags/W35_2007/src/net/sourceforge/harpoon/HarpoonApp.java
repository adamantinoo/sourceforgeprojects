//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonApp.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/HarpoonApp.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:21 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.2  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//
//    Revision 1.1  2007-08-14 11:07:23  ldiego
//    - First test release that is operative but just an initial framework.
//    - Some refactoring included but general structure and classes have
//      to be reviewed.
//

package net.sourceforge.harpoon;

//- IMPORT SECTION ..........................................................................................
import java.io.File;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.parts.HarpoonPartFactory;
import net.sourceforge.harpoon.ui.HarpoonScenery;

//- CLASS IMPLEMENTATION ....................................................................................
/**
 * This implements the main method to start the application and the main control of the User Interface. The
 * responsibilities are to create the main window and manage the application menus. It acts as the Controller
 * in the Model-View-Controller pattern.
 * 
 * @author ldiego
 */
public class HarpoonApp {
	// - A P P L I C A T I O N C O N S T A N T S
	/**
	 * This is the reference to the main display element that connect the SWT with the hosting OS.
	 */
	private static Display							theDisplay;
	/**
	 * This represent the window where all drawing takes place. This object will get all GUI events for
	 * processing.
	 */
	private static Shell								harpoonShell;
	/** Draw2d graphic connector system to relate SWM parts with the Draw2d drawing model. */
	private static LightweightSystem		harpoonLWSystem;
	/** Instance of the viewer that will allow us to use GEF and Draw2d for the painting of the model. */
	private static GraphicalViewerImpl				viewer;
	/** Object to reference external strings on messages structures. */
	private static ResourceBundle			bundle;
	/** Resources definition and log configuration to be used on this source. */
	private static Logger								log											= Logger.getLogger("net.sourceforge.harpoon");

	/** This section will create the list of palette colors to be used inside the GUI. This is just an example. */
//	private static Color								whiteColor;
//	private static Color								blackColor;
//	private static Color								mapColor;
//	private static Color								debugColor;

	// - A P P L I C A T I O N L O G A N D C O N F I G U R A T I O N
	/** Reference to the static part of the application */
	public static HarpoonApp						singleton;

	// - A P P L I C A T I O N E R R O R C O D E S
	// - A P P L I C A T I O N F I E L D S
	/** Time in milliseconds to wait before cycling the event loop. */
	private int													sleep										= 10 * 1000;
	/** Information about the types of files than can be open from the interface menus. */
	private static final String[]				OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
	private static final String[]				OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };
	/** Path to the scenery file that has been loaded or null if there is no scenery */
	private String											sceneryPath;
	/** Name of the scenery that is open by the interface. It's value includes the path to the file. */
	private String											sceneryName;
	/** Reference to the class that stores the user and game session data structures. */
	private HarpoonScenery							scenery;

	// [1]
	// - M A I N A P P L I C A T I O N E N T R Y P O I N T
	/**
	 * All application initialization tries to be as general as possible. This initialization will create an
	 * application singleton that will then be accessible by any other class to have access to global and helper
	 * data structures.<br>
	 * The Display is considered something external to the main loop code. Do the same with the shell and the
	 * LightweightSystem.<br>
	 * After the singleton is constructed (this is the most simple initialization task) and we have setup the
	 * most basic display system then the main setup method is called to create all initial data structures. The
	 * main method continues with the main event loog to get the user events and to process them to the right
	 * destination for processing.<br>
	 * <br>
	 * Normal procedure for using a LightweightSystem:
	 * <ol>
	 * <li>Create an SWT Canvas.</li>
	 * <li>Create a LightweightSystem passing it that Canvas.</li>
	 * <li>Create a Draw2d Figure and call // * setContents(IFigure). This Figure will be the top-level Figure
	 * of the Draw2d application.</li>
	 * </ol>
	 */
	public static void main(String[] args) {
		// - New and optimal version of a SWP application startup.
		Display display = new Display();
		theDisplay = display;
		Shell shell = new Shell(display, SWT.SHELL_TRIM | /*SWT.NO_BACKGROUND | */ SWT.PRIMARY_MODAL );
		harpoonShell = shell;

		// - Next version will not create the Viw�ewer directly to the Shell. Move the rest to the setup().
		// - Create the application and start running it.
		HarpoonApp app = new HarpoonApp();
		singleton = app;
		app.setup(display);

		// - Enter the event loop for the Display and wait until the exit commend or the close of the Display.
		while (!shell.isDisposed())
			if (!display.readAndDispatch()) display.sleep();
		app.exit();
		display.dispose();
	}

	// - C L A S S C O N S T R U C T O R S
	/** Initialize fields that must have some content at startup. Keep this constructor as simple as possible. */
	public HarpoonApp() {
		// TODO Initialize application preferences and load string resources into the static part of the singleton
		// to make
		// them accessible to all classes
		// TODO Read application preferences that are stored as a persistent information for this application
		// preferences.
		// TODO Move this code to the createWidgets method even this are made global.
		// whiteColor = new Color(theDisplay, 255, 255, 255);
		// blackColor = new Color(theDisplay, 0, 0, 0);
		// mapColor = new Color(theDisplay, 200, 200, 200);
		// debugColor = new Color(theDisplay, 200, 200, 0);
		// END CODE SNIPPET
		// TODO Check for runtime exceptions on the next line that can occur because it depends on external files.
		bundle = ResourceBundle.getBundle("net.sourceforge.harpoon.HarpoonMessages");
	}
	// - S T A T I C S E C T I O N

	// - P U B L I C S E C T I O N

//	private Composite						top	= null;
	private SashForm windowSplit = null;
	private Composite propertyViewer = null;
	private Text name = null;
	private Text location = null;

	// private Composite main = null;

	// - P R I V A T E S E C T I O N
	// [01]
	/**
	 * The application start with a single window that is currently empty. The start method creates the menus
	 * and the UI elements base for operation but does not load any scenery. This is performed by the UI menus.
	 */
	private Shell setup(Display display) {
		// - Configure the window to its initial state.
		harpoonShell.setText("Harpoon. Draft version - $Revision: 1.4 $");
		harpoonShell.setLocation(205, 0);
		// harpoonShell.setSize(520, 520);
		harpoonShell.setLayout(new FillLayout());
		// harpoonShell.addListener(SWT.Dispose, new Listener() {
		// public void handleEvent(Event e) {
		// // - Disposes of all contained maps.
		// // mapArea.dispose();
		// // display.dispose();
		// }
		// });

		// - Add the window contents. This data is obtained from the view editor.
		createWindowSplit();

		// - Initialize beat timer to move structures.
		Runnable timer = new Runnable() {
			public void run() {
				if (harpoonShell.isDisposed()) return;
				// - Renders the scene of the main area.
				// mapArea.render();
				theDisplay.timerExec(sleep, this);
			}
		};
		// timer.run();

		// TODO Hook resize and dispose listeners.
		// - Add a menu bar and widgets.
		createMenuBar();
		createWidgets();

		// - Clean up the scenery to signal that no information is loaded and that the main canvas is empty.
		// this.scenery = null;
		// this.surface = null;

		// - Open the window
		harpoonShell.open();
		return harpoonShell;
	}
	/** Creates the main window sash where we locate the two main panes. */
	private void createWindowSplit() {
		windowSplit = new SashForm(harpoonShell, SWT.HORIZONTAL);
		createMapViewer();
		createPropertyViewer();
		int[] we = new int [2];
		we [0] = 80;we[1]=20;
		windowSplit.setWeights(we);
	}
	/**
	 * Creates the canvas for the Map viewer. This is a special canvas that id a Draw2d viewer that presents
	 * figures and not SWT elements.<br>
	 * There must be a SWT <code>Composite</code> as the parent because all drawing elements must have a
	 * layout and adding directly the Draw2D viewer to the sash did not displayed any object.
	 */
	private void createMapViewer() {
		Composite c1 = new Composite(windowSplit, SWT.NONE);
		c1.setBackground(new Color(theDisplay, 255, 255, 255));
		c1.setLayout(new FillLayout());
		Canvas can = new Canvas(c1, SWT.NONE);
		GraphicalViewerImpl vi = new GraphicalViewerImpl();
		vi.setControl(can);
		viewer = vi;
	}
	/**
	 * Creates the left pane of the sash with a property presentation where selected object will display their
	 * interface information.<br>
	 * This object will be replaced by an instance that is particular to each object selected so the code have
	 * to change to reflect that contents changes.
	 */
	private void createPropertyViewer() {
		RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;
		propertyViewer = new Composite(windowSplit, SWT.NONE);
		propertyViewer.setLayout(rowLayout);
		name = new Text(propertyViewer, SWT.BORDER);
		name.setText("USS Alpha");
		name.setEditable(false);
		location = new Text(propertyViewer, SWT.BORDER);
		location.setText("51� 40\' 30\'\' N - 0� 6\' 30\'\' E");
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		createSpeedControl();
		createDirectionControl();
		createSensorsArea();
	}
	private Composite speedControl = null;
	private Text speedLabel = null;
	private Spinner speedSet = null;
	private Composite	directionControl;
	private Text	directionLabel;
	private Spinner	directionSet;
	private Composite sensorsArea = null;
	private Text sensorLabel = null;
	private Composite sensorList = null;
	private Button radar = null;
	private Button sonar = null;
	private Button EEM = null;

	private void createSpeedControl() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		speedControl = new Composite(propertyViewer, SWT.NONE);
		speedControl.setLayout(gridLayout);
		speedLabel = new Text(speedControl, SWT.NONE);
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedSet = new Spinner(speedControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		speedSet.setMaximum(900);
	}
	private void createDirectionControl() {
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		directionControl = new Composite(propertyViewer, SWT.NONE);
		directionControl.setLayout(gridLayout);
		directionLabel = new Text(directionControl, SWT.NONE);
		directionLabel.setEditable(false);
		directionLabel.setText("Direction:");
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionSet = new Spinner(directionControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setMaximum(900);
	}
	private void createSensorsArea() {
		sensorsArea = new Composite(propertyViewer, SWT.NONE);
		sensorsArea.setLayout(new RowLayout());
		sensorLabel = new Text(sensorsArea, SWT.BORDER);
		sensorLabel.setText("Sensors:");
		sensorLabel.setEditable(false);
		sensorLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		createSensorList();
	}
	private void createSensorList() {
		RowLayout rowLayout1 = new RowLayout();
		rowLayout1.type = org.eclipse.swt.SWT.VERTICAL;
		rowLayout1.spacing = 2;
		rowLayout1.marginBottom = 2;
		rowLayout1.marginLeft = 1;
		rowLayout1.marginRight = 1;
		rowLayout1.marginTop = 2;
		rowLayout1.wrap = false;
		sensorList = new Composite(sensorsArea, SWT.NONE);
		sensorList.setLayout(rowLayout1);
		radar = new Button(sensorList, SWT.CHECK);
		radar.setText("Rad");
		sonar = new Button(sensorList, SWT.CHECK);
		sonar.setText("Son");
		EEM = new Button(sensorList, SWT.CHECK);
		EEM.setText("EEM");
	}
	/**
	 * Method to release all system resources before terminating the application. This is the housekeeping
	 * method for the environment and not to functionally closing the application.
	 */
	private void exit() {
		// TODO Auto-generated method propertyViewer
		// TODO Dispose of the scenery and the surface. The scenery has to be dumped to persistent storage.
	}

	// - M E N U S E C T I O N
	/** Creates the menu bar where all main menus are added. */
	private Menu createMenuBar() {
		// - Menu bar.
		Menu menuBar = new Menu(harpoonShell, SWT.BAR);
		harpoonShell.setMenuBar(menuBar);
		createFileMenu(menuBar);
//		createDebugMenu(menuBar);
		return menuBar;
	}

	/** Definition of the "File" menu. */
	private void createFileMenu(Menu menuBar) {
		// - File menu
		MenuItem item = new MenuItem(menuBar, SWT.CASCADE);
		item.setText(bundle.getString("File"));
		Menu fileMenu = new Menu(harpoonShell, SWT.DROP_DOWN);
		item.setMenu(fileMenu);

		// File -> Open File...
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText("Open Scenery...");
		item.setAccelerator(SWT.MOD1 + 'O');
		item.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				menuOpenScenery();
			}
		});

		new MenuItem(fileMenu, SWT.SEPARATOR);

		// File -> Exit
		item = new MenuItem(fileMenu, SWT.PUSH);
		item.setText(bundle.getString("Exit"));
		item.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				harpoonShell.close();
			}
		});

	}
//[02]

	/**
	 * Opens a new scenery and updates the UI. If there is already an scenery open it should ask the user if the
	 * current scenery should be closed and if the modified data should be saved.<br>
	 * Changed the way to generate the presentation layer. Instead getting the main canvas from the map we
	 * create a graphical viewer from the scenery.<br>
	 * <br>
	 * The order of operations to be performed to create a suitable MVC is described next:
	 * <ul>
	 * <li>Store the current scenery to be retirved if there is any error loading the new one.</li>
	 * <li>Open a dialog to allow the user to select the file.</li>
	 * <li>Open the selected file and create a new scenery with its contents.</li>
	 * <li>Load and create the internal data structures not initialized during the scenery creation.</li>
	 * <li>Initialize the GraphicalViewer. This will trigger the creation of all the EditParts and then the
	 * corresponding Figures for the drawing./li>
	 * </ul>
	 */
	public void menuOpenScenery() {
		// TODO Save locally the previous scenery just in case of errors.
		// TODO Check is an scenery is already open. If so ask for closing.
		// - Get the user to choose an scenery file.
		final FileDialog fileChooser = new FileDialog(harpoonShell, SWT.OPEN);
		if (this.sceneryPath != null) fileChooser.setFilterPath(sceneryPath);
		fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(OPEN_FILTER_NAMES);
		this.sceneryName = fileChooser.open();
		this.sceneryPath = fileChooser.getFilterPath();
		if (this.sceneryName == null) return;
		// - There is a scenery file open and ready. Load the scenery data and create the corresponding UI
		// objects.
		final File sceneryFile = new File(this.sceneryName);

		// - With the File reference create the new scenery.
		// TODO Detect the fail of the creation of the scenery using an Exception
		// - Open the scenery and initialize it. This scenery gives the presentation layer that has to be
		// registered to the interface. This is why it receives the shell.
		// TODO Remove the shell access because it belongs to the application and to the viewer.
		this.scenery = new HarpoonScenery(sceneryFile, harpoonShell);
		if (null == this.scenery) {
			// TODO Open a dialog showing that there was a problem opening the scenery.
			// TODO Go back to the previous scenery.
			// - There was an error during the creation of the scenery. Log the incidence and reset the opening.
			log.warning("The creation of a new scenery did not returned a valid object.");
			// this.sceneryName = null;
		} else {
			scenery.loadData();
			// - The graphical viewer already exists and belongs to the application. Just set it to the new model
			// root node.
//			viewer.setRootEditPart(new BaseRootMapPart());
			viewer.setRootEditPart(new ScalableFreeformRootEditPart());
			// TODO Initialize the root edit part that is the map to load the data
			// scenery.getRootPart().generateMap();
			viewer.setEditPartFactory(new HarpoonPartFactory());
			viewer.setContents(scenery.getModelRoot());
			// DEBUG Other functions that may be needed to implement
			// viewer.setKeyHandler(new
			// GraphicalViewerKeyHandler(getGraphicalViewer()).setParent(getCommonKeyHandler()));
			// getGraphicalViewer().addDropTargetListener(new
			// TemplateTransferDropTargetListener(getGraphicalViewer()));

			// DEBUG Ths block should be reviewed

			// // TODO Move this to the Figure of the Map EditPart
			// this.surface = scenery.generateMap();
			// surface.setLocation(0, 0);
			// surface.redraw();
			// // - The size has to be calculated depending on the preferred width of the map
			// surface.setSize(scenery.calculatePrefSize());
			// surface.addPaintListener(new PaintListener() {
			// public void paintControl(PaintEvent event) {
			// scenery.drawMap(event);
			// scenery.drawUnits(event);
			// }
			// });
		}
		// END OF BLOCK
	}

	void showErrorDialog(String operation, String message) {
		MessageBox box = new MessageBox(harpoonShell, SWT.ICON_ERROR);
		// String message =
		box.setMessage(operation + message);
		box.open();
	}

	// - W I D G E T S E C T I O N
	private void createWidgets() {
		// TODO Create the inner elements in the canvas and views.
		// TODO Create colors and fonts. This should be added to the createWidgets method

		// - Create the Map canvas
		// mapCanvas = new HarpoonMap(harpoonShell, SWT.NO_REDRAW_RESIZE | SWT.BORDER);
		// mapCanvas.setLocation(0, 0);
		// mapCanvas.setSize(256, 256);
		// mapCanvas.setBackground(whiteColor);
		// mapCanvas.setCursor(crossCursor);

		// TODO Move this code to the createWidgets method.
		// - Create a GC for drawing, and hook the listener to dispose it.
		// this.imageCanvasGC = new GC(this.mapCanvas);
		// this.mapCanvas.addDisposeListener(new DisposeListener() {
		// public void widgetDisposed(DisposeEvent e) {
		// imageCanvasGC.dispose();
		// }
		// });
		// END CODE SNIPPET

	}

	public static Shell getShell() {
		// TODO Auto-generated method stub
		return harpoonShell;
	}

	public static Device getDisplay() {
		// TODO Auto-generated method stub
		return theDisplay;
	}

	// - P U B L I C S E C T I O N

	// ... PUBLIC OPERATIONS ............................................................................
	// public static Display getDisplay() {
	// return theDisplay;
	// }
	/** This method is called when the */
	// public void close() {
	// // TODO Auto-generated method stub
	// // this.mapArea.close();
	// }
	// public String getSceneryPath() {
	// return this.sceneryPath;
	// }
	//
	// public String getSceneryName() {
	// return this.sceneryName;
	// }
}
//class BaseRootMapPart extends AbstractEditPart{
//
//	public IFigure createFigure() {
//		// TODO Auto-generated method stub
//		return new Figure();
//	}
//}
// - U N U S E D C O D E ....................................................................................
//[01]
// TODO Remove this method that is not longer necessary.
// private void initialize() {
// // Snippet 21
// // - Draws an oval across all the size of the canvas
// // harpoonShell.addPaintListener(new PaintListener() {
// // public void paintControl(PaintEvent event) {
// // Rectangle rect = harpoonShell.getClientArea();
// // event.gc.drawOval(0, 0, rect.width - 1, rect.height - 1);
// // }
// // });
//
// // harpoonShell.setBounds(10, 10, 200, 200);
//
// // Snippet 13
// // - Draws a rectangle with a thick border line
// GC gc = new GC(harpoonShell);
// gc.setLineWidth(4);
// gc.drawRectangle(20, 20, 100, 100);
// gc.dispose();
//
// // this.display = theDisplay;
// // HarpoonApp.whiteColor = new Color( theDisplay, 255, 255, 255 );
// // HarpoonApp.blackColor = new Color( theDisplay, 0, 0, 0 );
//
// // harpoonShell.setLayout( new FillLayout() );
// harpoonShell.setText("Harpoon Initialization Test: Phase 01.01 $Revision: 1.4 $");
// harpoonShell.setLocation(10, 10);
// harpoonShell.setSize(500, 500);
// // harpoonShell.setBounds(10, 10, 200, 200); // This is equivalent to a setLocation and setSize
//
// // - First drawing and initialization test.
// // Label label1 = new Label(harpoonShell, SWT.BORDER);
// // label1.setText("See no evil");
// // label1.setSize(100, 20);
// // label1.setLocation(30, 30);
//
// // - Initialize test data structures.
// // - Drawing area structures
// // final HarpoonDisplay drawingArea = new HarpoonDisplay(harpoonShell, SWT.BORDER);
// final Canvas surface = new HarpoonMap(harpoonShell, SWT.BORDER);
// surface.setLocation(0, 0); // Location it set by the layout
// surface.setSize(400, 400); // The size has to be calculated depending on the width of the shell and the
// // preferred width of the elements
// surface.setBackground(mapColor);
// final Canvas debugArea = new Canvas(harpoonShell, SWT.BORDER);
// debugArea.setLocation(400, 0); // Location it set by the layout
// debugArea.setSize(100, 400);
// debugArea.setBackground(debugColor);
//
// final Text logArea = new Text(harpoonShell, SWT.MULTI);
// logArea.setLocation(0, 400); // Location it set by the layout
// logArea.setSize(400, 100);
// logArea.setBackground(whiteColor);
//
// // final Composite drawingArea = new Composite( harpoonShell, SWT.NONE );
// // drawingArea.setLayout(new RowLayout());
// final RowLayout rowLayout = new RowLayout();
// rowLayout.wrap = false;
// rowLayout.pack = true;
// rowLayout.justify = false;
// rowLayout.type = SWT.HORIZONTAL;
// rowLayout.marginLeft = 0;
// rowLayout.marginTop = 0;
// rowLayout.marginRight = 5;
// rowLayout.marginBottom = 0;
// rowLayout.spacing = 0;
// // harpoonShell.setLayout(rowLayout);
//
// // // mapArea = new PaintSurface( surface, new Text( surface, SWT.LEFT ), whiteColor );
//
// // drawingCanvas.addListener( SWT.Selection, new Listener() {
// // public void handleEvent( Event e ) {
// // OpenGLTab tab = tabs[tabFolder.getSelectionIndex()];
// // tab.setCurrent();
// // sleep = tab.getSleepLength();
// // }
// // });
//
// // - Open the display and start to accept events.
// harpoonShell.open();
// harpoonShell.setActive();
// harpoonShell.setVisible(true);
// while (!harpoonShell.isDisposed()) {
// if (!theDisplay.readAndDispatch()) theDisplay.sleep();
// }
// theDisplay.dispose();
// }

//[02]
//private void createDebugMenu(Menu menuBar) {
//// - Debug menu
//MenuItem item = new MenuItem(menuBar, SWT.CASCADE);
//item.setText("Debug");
//Menu debugMenu = new Menu(harpoonShell, SWT.DROP_DOWN);
//item.setMenu(debugMenu);
//
//// Debug -> Serialize units
//// Generate an string with the debut unit data to include on the sceneries for testing.
//item = new MenuItem(debugMenu, SWT.PUSH);
//item.setText("Serialize Units");
//// item.setAccelerator(SWT.MOD1 + 'O');
//item.addSelectionListener(new SelectionAdapter() {
//public void widgetSelected(SelectionEvent event) {
//	menuSerializeUnits();
//}
//});
//}
//void menuSerializeUnits() {
//ObjectOutputStream out;
//try {
//out = new ObjectOutputStream(new ByteArrayOutputStream());
//// TODO Perform the methods to serialize the scenery data.
//// scenery.writeObject(out);
//showErrorDialog("Serializacion de unidades:", out.toString());
//} catch (IOException e) {
//// TODO Auto-generated catch block
//e.printStackTrace();
//}
//}
