//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/ShipPart.java,v $
//  LAST UPDATE:    $Date: 2007-08-28 13:33:21 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.HarpoonMap;
import net.sourceforge.harpoon.figures.ShipFigure;
import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.model.ShipUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class ShipPart extends AbstractGraphicalEditPart {
	/** Creates the presentation figure and initializes it. */
	@Override
	public IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		ShipUnit unit = (ShipUnit) this.getModel();

		// - Create and initialize the figure
		ShipFigure ship = new ShipFigure();
		ship.setSide(unit.getSide());
		ship.setDirection(unit.getDirection());
		ship.setSpeed(unit.getSpeed());

		// - Calculate the figure location. The part has access to the model.
		HarpoonMap map = unit.getRoot().getMap();
		double topLeftLat = map.getTopLatitude();
		double topLeftLon = map.getTopLongitude();
		PolarCoordinate latSpan = map.getLatitude2Zoom();
		PolarCoordinate lonSpan = map.getLongitude2Zoom();
		
		double latDiff = Math.abs(topLeftLat-unit.getLatitude());
		double lonDiff = Math.abs(topLeftLon-unit.getLongitude());
		
		int xx = new Double(256*lonDiff/lonSpan.toDegrees()).intValue();
		int yy = new Double(256*latDiff/latSpan.toDegrees()).intValue();

		double degUnit = unit.getLatitude();
		double degTopLeft = topLeftLat;
		double diffLat = 0.0;
		if (degUnit < degTopLeft) diffLat = degTopLeft - degUnit;
		double span = diffLat / latSpan.toDegrees();
		int x = new Double(span * 256.0).intValue();
		int y = new Double((unit.getLongitude() - topLeftLon) / lonSpan.toDegrees() * 256.0).intValue();

		// - Calculate the pixel position of the center.
		ship.setLocation(new Point(xx, yy));

		return ship;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub
		
	}
}

// - UNUSED CODE ............................................................................................
