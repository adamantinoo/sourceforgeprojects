//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/RootMapPart.java,v $
//  LAST UPDATE:    $Date: 2007-08-28 13:33:21 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.List;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.MapFigure;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
/**
 * This is the controller of the root model element implemented as the Map. With the inheritance chain we only
 * have to implement the key methods <code>createFigure()</code> and <code>getModelChildren()</code> if
 * this part is a container.
 */
public class RootMapPart extends AbstractGraphicalEditPart {
	/**
	 * Currently I have to get access to the Map that is the already created figure because it contains some of
	 * the model data mixed with the presentation data.<br>
	 * This figure is costly to be created many times. Now just create it everytime we have the part requested
	 * that now is just a single time.
	 */
	public IFigure createFigure() {
		// - Create the presentation figure and initialize to be able to display.
		RootMapUnit model = (RootMapUnit) this.getModel();
		MapFigure map = new MapFigure();
		map.setConfiguration(model.getMapProperties());
		map.readMapData();
		// - Load the map into the Model for reference because it still contains some model information.
		model.setMap(map);
		return map;
	}

	@Override
	protected List<Unit> getModelChildren() {
		RootMapUnit model = (RootMapUnit) this.getModel();
		List<Unit> li = model.getChildren();
		return li;
	}

	public String toString() {
		return "RootMapPart hat has map " + ((RootMapUnit) getModel()).getName();
	}

	// refreshVisuals()
	// getModelChildren()
	// @Override
	// protected void addChildVisual(EditPart part, int index) {
	// // TODO Auto-generated method stub
	//		
	// }
	// @Override
	// protected void removeChildVisual(EditPart arg0) {
	// // TODO Auto-generated method stub
	//		
	// }
	@Override
	protected void addChildVisual(EditPart childEditPart, int index) {
		// TODO Auto-generated method stub
		super.addChildVisual(childEditPart, index);
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void removeChildVisual(EditPart childEditPart) {
		// TODO Auto-generated method stub
		super.removeChildVisual(childEditPart);
	}

	public DragTracker getDragTracker(Request arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}

// - UNUSED CODE ............................................................................................
