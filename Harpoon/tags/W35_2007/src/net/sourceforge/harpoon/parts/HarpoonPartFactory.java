//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPartFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/HarpoonPartFactory.java,v $
//  LAST UPDATE:    $Date: 2007-08-28 13:33:21 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.ShipUnit;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonPartFactory implements EditPartFactory {
	// - O V E R R I D E S
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart part = null;
		if (model instanceof RootMapUnit) part = new RootMapPart();
		else if (model instanceof ShipUnit) part = new ShipPart();
//		else if (model instanceof AirUnit) part = new AirPart();
		part.setModel(model);
		return part;
	}
}

// - UNUSED CODE ............................................................................................
