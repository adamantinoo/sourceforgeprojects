//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonScenery.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/ui/HarpoonScenery.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:13 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-23 09:33:41  ldiego
//    - Store this release that work before moving more toward the MVC of GEF.
//

package net.sourceforge.harpoon.ui;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Properties;

import org.eclipse.swt.widgets.Shell;

import net.sourceforge.harpoon.figures.HarpoonMap;
import net.sourceforge.harpoon.model.RootMapUnit;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonScenery implements Serializable {

	private static final long	serialVersionUID	= 9172888301981258427L;
	private Properties				props;
	/** Contains the map and geographical data of the game, this belongs to the presentation layer on the MVC. */
	private HarpoonMap				mapCanvas;
	/** Stores the data model for this game session. */
	private RootMapUnit				units = null;
	// private HarpoonModel units;
	/** Controls the relationship between the model and the viewer. Manages the game events and actions. */
//	private HarpoonController	parts;

	// - C O N S T R U C T O R S
	/**
	 * This class represents the configuration data that describes an scenery. Contains methods to instantiate
	 * the data structures and to load that configuration data. The data now is stored as a set of name-value
	 * pairs of strings then can be manipulated as properties. <br>
	 * The responsibility is set to the creation and storage of the game structures related to a game session.
	 * The elements of the UI related to this game session and created and stored in this class. <br>
	 * <br>
	 * The constructor implementation has to be simple to net generate any error during the execution. Any other
	 * data handling must be done on other methods called after the proper initialization.
	 * 
	 * @param harpoonShell
	 */
	public HarpoonScenery(File scenery, Shell harpoonShell) {
		if (null == scenery) return;
		// - Load the scenery configuration properties
		// They will indicate where is stored the model.
		props = new Properties();
		try {
			props.load(new BufferedInputStream(new FileInputStream(scenery)));
			// mapCanvas = new HarpoonMap(harpoonShell, SWT.NO_REDRAW_RESIZE | SWT.BORDER);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// - P U B L I C S E C T I O N
	public Object getModelRoot() {
		if (null != units)
			return units;
		else {
			RootMapUnit emptyModel = new RootMapUnit();
			emptyModel.createTestUnits();
			return emptyModel;
		}
	}

	/** Loads the model data and creates the corresponding MVC structures and initializes them. */
	public void loadData() {
		try {
			// - There are two things to be read, the Model and the scenery properties
			String modelFileName = props.getProperty("modelLocation");
			if (null == modelFileName) throw new FileNotFoundException("The model store file does not exist");
			FileInputStream fis = new FileInputStream(modelFileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			units = (RootMapUnit) ois.readObject();
			ois.close();
			// DEBUG Initialize to the preset set of units.
			units.setMapProperties(props);
			units.createTestUnits();
			// - Create the controller from the model.
			// parts = new HarpoonController(units);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	/** Draws the map contents to the destination canvas. */
//	public void drawMap(PaintEvent event) {
//		mapCanvas.draw(event.gc);
//	}
//
//	/**
//	 * Uses the controller to traverse the model and generate the viewer parts to perform the drawing commands
//	 * required to draw the visible part of the model.
//	 */
//	public void drawUnits(PaintEvent event) {
//		// TODO Auto-generated method stub
//		parts.draw(event.gc);
//	}
//
//	public Point calculatePrefSize() {
//		// TODO Auto-generated method stub
//		return mapCanvas.computeSize(10, 10);
//	}
//
//	public Canvas generateMap() {
//		// TODO Auto-generated method stub
//		mapCanvas.openScenery(props);
//		return mapCanvas;
//	}

}

// - UNUSED CODE ............................................................................................
