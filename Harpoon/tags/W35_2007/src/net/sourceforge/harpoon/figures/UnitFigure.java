//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/UnitFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:22 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitFigure extends Figure {
	public static final Color[]	UNIT_COLORS				= new Color[4];
	static {
//		UNIT_COLORS[UNKNOWN_SIDE] = new Color(HarpoonApp.getDisplay(), 0, 0, 0);
//		UNIT_COLORS[FRIEND] = new Color(HarpoonApp.getDisplay(), 0, 256, 0);
//		UNIT_COLORS[FOE] = new Color(HarpoonApp.getDisplay(), 256, 0, 0);
//		UNIT_COLORS[NEUTRAL] = new Color(HarpoonApp.getDisplay(), 256, 256, 0);
	}

}

// - UNUSED CODE ............................................................................................
