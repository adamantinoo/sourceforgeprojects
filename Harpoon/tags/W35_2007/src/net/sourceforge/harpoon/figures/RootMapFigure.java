//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/RootMapFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-28 13:46:58 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;

import net.sourceforge.harpoon.HarpoonApp;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class MapFigure extends HarpoonMap {
	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		int width = this.getHorizontalBlocks()*GOOGLE_BLOCK_WIDTH;
		int height = this.getVerticalBlocks()*GOOGLE_BLOCK_HEIGHT;
		return new Rectangle(0,0,width,height);
	}
	protected void paintFigure(Graphics g) {
		super.paintFigure(g);
		if (mapDataArray == null) {
			Rectangle bounds = getBounds().getCopy();
			g.setForegroundColor(ColorConstants.darkGray);
			g.setBackgroundColor(ColorConstants.white);
			g.fillRectangle(0, 0, bounds.width, bounds.height);
		} else {
			// TODO Copy to the canvas the map data that is stored in the cache.
			final int hBlocks = getHorizontalBlocks();
			final int vBlocks = getVerticalBlocks();
			for (int j = 0; j < vBlocks; j++) {
				for (int i = 0; i < hBlocks; i++) {
					// - Get a block and paint it on the right location on the final display buffer.
					Image newMap = new Image(HarpoonApp.getDisplay(), this.mapDataArray[j * hBlocks + i]);
					int x = GOOGLE_BLOCK_WIDTH * i;
					int y = GOOGLE_BLOCK_HEIGHT * j;
					g.drawImage(newMap, x, y);
				}
			}
		}
	}
	@Override
	protected void paintChildren(Graphics graphics) {
		// TODO Auto-generated method stub
		super.paintChildren(graphics);
	}

}

// - UNUSED CODE ............................................................................................
