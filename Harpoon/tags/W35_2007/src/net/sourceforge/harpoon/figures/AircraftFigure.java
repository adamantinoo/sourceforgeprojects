//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AircraftFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/AircraftFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:13 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

//... IMPORT SECTION ...................................................................................................
import harpoon.devel.FigureDrawContext;
import harpoon.devel.HarpoonPalette;

import java.lang.reflect.Array;

import net.sourceforge.harpoon.HarpoonApp;
import net.sourceforge.harpoon.units.BaseUnit;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;

//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AircraftFigure extends BaseFigure {
//... INSTANCE PRIVATE FIELDS
	protected int flag;
	protected Color bgColor = HarpoonPalette.getFigureBGColor();
	protected Color fgColor = HarpoonPalette.getFigureFGColor();
	protected int xcoord = 0;
	protected int ycoord = 0;
	protected Rectangle bounds = new Rectangle( 0,0,0,0 );
	
	protected BaseUnit unit;
	protected int[] shapePoints;

	public AircraftFigure( BaseUnit unit ) {
		this.unit = unit;
		this.bgColor = unit.getUnitColor();
//		this.fgColor = unit.getFgColor();
		this.setShapePoints( new int[] { 0,-5,0,-4,1,-3,1,0,5,4,1,4,1,5,-1,5,
			-1,4,-1,4,-1,0,-1,-3,0,-4 } );
	}
//... PUBLIC OPERATIONS ....................................................................................
	/** Draw the shape depending on the current parent unit associated to the instance.
	 */
	public void draw(int deltax, int deltay, GC gc) {
		this.draw( deltax, deltay, this.unit.getScale(), gc );
	}
	public void draw(int deltax, int deltay, int scale, GC gc) {
		//... Get the drawing position and drawing data.
		int pointsCount = this.getShapePoints().length;
		int[] movedShape = (int[])this.doubleArray(this.getShapePoints());
		for ( int i=0; i < pointsCount; i++) {
			if (Math.IEEEremainder( i , 2 ) == 0) {
				movedShape[i] = shapePoints[i]*scale + deltax;
			} else {
				movedShape[i] = shapePoints[i]*scale + deltay;
			}
		}
		gc.setBackground( this.bgColor );
		gc.setForeground( this.fgColor );
		gc.drawPolygon( movedShape );
	}

	private Object doubleArray(Object source) {
		int sourceLength = Array.getLength(source);
		Class arrayClass = source.getClass();
		Class componentClass = arrayClass.getComponentType();
		Object result = Array.newInstance(componentClass, sourceLength);     
		System.arraycopy(source, 0, result, 0, sourceLength);
		return result;
	 }
	/* (non-Javadoc)
	 * @see net.sourceforge.harpoon.figures.BaseFigure#draw(net.sourceforge.harpoon.figures.FigureDrawContext)
	 */
	public void draw(FigureDrawContext fdc) {
		// TODO Auto-generated method stub
		
	}
	/* (non-Javadoc)
	 * @see net.sourceforge.harpoon.figures.BaseFigure#addDamagedRegion(net.sourceforge.harpoon.figures.FigureDrawContext, org.eclipse.swt.graphics.Region)
	 */
	public void addDamagedRegion(FigureDrawContext fdc, Region region) {
		// TODO Auto-generated method stub
		
	}

//... GETTER/SETTER PUBLIC OPERATIONS ..............................................................
	public Rectangle getBounds() {
		return this.bounds;
	}
	public int[] getShapePoints() {
		return shapePoints;
	}
	public void setShapePoints(int[] points) {
		this.shapePoints = points;
	}
	public Color getBgColor() {
		return bgColor;
	}
	public Color getFgColor() {
		return fgColor;
	}
	public void setBgColor(Color color) {
		bgColor = color;
	}
	public void setFgColor(Color color) {
		fgColor = color;
	}
}

//... UNUSED CODE ......................................................................................................