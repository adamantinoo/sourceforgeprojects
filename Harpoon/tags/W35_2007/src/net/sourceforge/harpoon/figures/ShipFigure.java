//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/ShipFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:22 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-28 13:46:58  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonApp;
import net.sourceforge.harpoon.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - P U B L I C S E C T I O N
public class ShipFigure extends Figure {
	/** Stores the direction of movement of the unit to represent it with a vector. */
	private double		direction;
	/** Stores the spped of the unit to represent it with the legth of a vector. */
	private int			speed;
//	private ShipUnit	model;
	/** Stores the color that matches the side of the unit in the game. */
	private Color		color;
	/** Reference to a Container to draw all the elements of the Ship. */
//	private Figure		mainContainer;
	private Figure	labelContainer;
	private ShipDrawFigure	ship;

	// private Object color;

	// - C O N S T R U C T O R S
	/** Set the drawing size of the figure that it is a fixed value. */
	public ShipFigure() {
		this.setSide(Unit.UNKNOWN_SIDE);
		
		// - Create the complex internal parts of this figure
//		mainContainer = new Figure();
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		this.setLayoutManager(gridLayout);
		createShipbase();
		createShipinfo();
		
		this.setSize(this.getPreferredSize());
	}

	private void createShipbase() {
		ship = new ShipDrawFigure(this.color);
		this.add(ship);
	}

	private void createShipinfo() {
		labelContainer = new Figure();
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		gridLayout.verticalSpacing=0;
		gridLayout.horizontalSpacing=0;
		gridLayout.marginHeight=0;
		gridLayout.marginWidth=0;
		labelContainer.setLayoutManager(gridLayout);
		Label direction = new Label("017");
		direction.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		labelContainer.add(direction);
		Label speed = new Label("15.0");
		speed.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		labelContainer.add(speed);
		this.add(labelContainer);
		labelContainer.validate();
	}

	// - O V E R R I D E S
	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
	}
	/**
	 * The new visual representation is a colored circle with two text information at right side with the speed
	 * and direction. This adds some complexity to the creation and calculation of the right visual coordinates.<br>
	 * Also I have added a border for debug purposes.<br>
	 * <br>
	 * There can be two implementations. One when this method draws all the elements and other when we delegate
	 * to the children.
	 */
//	@Override
//	protected void paintFigure(Graphics graphics) {
//		super.paintFigure(graphics);
//
//		// - Set the drawing color. Fills color are done on background color.
//		graphics.setForegroundColor(this.color);
//		graphics.setBackgroundColor(this.color);
//		// int x = this.getLocation().x;
//		// int y = this.getLocation().y;
//		// Rectangle b = getBounds();
//		// Rectangle r = new Rectangle(0, 0, 100, 100);
//		graphics.fillOval(getBounds().getCopy());
//		// [1]
//	}

	public Dimension getPreferredSize(int wHint, int hHint) {
		Dimension dim = new Dimension();
//		dim.width = 50;
//		dim.height = 50;
		Dimension shipDim=ship.getPreferredSize();
		Dimension labelDim=labelContainer.getPreferredSize();
		dim.width=shipDim.width+labelDim.width;
		dim.height=Math.max(shipDim.height, labelDim.height);
		return dim;
	}
//
//	public void setBounds(Rectangle rect) {
//		super.setBounds(rect);
//		rect = Rectangle.SINGLETON;
//		getClientArea(rect);
//		// contents.setBounds(rect);
//		Dimension size = this.getPreferredSize();
//		// footer.setLocation(rect.getBottomLeft().translate(0, -size.height));
//		// footer.setSize(size);
//
//		// size = header.getPreferredSize();
//		// header.setSize(size);
//		// header.setLocation(rect.getLocation());
//	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while
	 * FOES are marked red. NEUTRAL are yellow and other are blue.
	 */
	public void setSide(int side) {
		// TODO Auto-generated method stub
		if (Unit.FRIEND == side) this.color = new Color(HarpoonApp.getDisplay(), 0, 255, 0);
		if (Unit.FOE == side) this.color = new Color(HarpoonApp.getDisplay(), 255, 0, 0);
		if (Unit.NEUTRAL == side) this.color = new Color(HarpoonApp.getDisplay(), 255, 255, 0);
		if (Unit.UNKNOWN_SIDE == side) this.color = new Color(HarpoonApp.getDisplay(), 0, 0, 255);
//		ship.setColor(this.color);
	}

	public void setDirection(double direction2) {
		// TODO Auto-generated method stub
		this.direction = direction2;
	}

	public void setSpeed(int speed2) {
		// TODO Auto-generated method stub
		this.speed = speed2;
	}
}

class ShipDrawFigure extends Figure {
	private Color	color;

	public ShipDrawFigure(Color color) {
		this.color = color;
	}

	public void setColor(Color color2) {
		// TODO Auto-generated method stub
		this.color=color2;
	}

	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);

		// - Set the drawing color. Fills color are done on background color.
		graphics.setForegroundColor(this.color);
		graphics.setBackgroundColor(this.color);
		graphics.fillOval(getBounds().getCopy());
	}
	public Dimension getPreferredSize(int wHint, int hHint) {
		Dimension dim = new Dimension();
		dim.width = 10;
		dim.height = 10;
		return dim;
	}

}
// - UNUSED CODE ............................................................................................
