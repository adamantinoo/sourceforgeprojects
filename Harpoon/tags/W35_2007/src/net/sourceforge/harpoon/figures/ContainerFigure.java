//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ContainerFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/ContainerFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:14 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

//... IMPORT SECTION ...................................................................................................
//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/


import harpoon.devel.FigureDrawContext;

import org.eclipse.swt.graphics.*;


/**
 * Container for BaseFigure objects with stacking preview mechanism.
 */
public class ContainerFigure {
	private static final int INITIAL_ARRAY_SIZE = 16;
	
	Object[]   objectStack = null;
	int      nextIndex = 0;

	/**
	 * Constructs an empty Container
	 */
	public ContainerFigure() {
	}
	/**
	 * Adds an object to the container for later drawing.
	 * 
	 * @param object the object to add to the drawing list
	 */
	public void add(BaseFigure object) {
		if (objectStack == null) {
			objectStack = new BaseFigure[INITIAL_ARRAY_SIZE];
		} else if (objectStack.length <= nextIndex) {
			BaseFigure[] newObjectStack = new BaseFigure[objectStack.length * 2];
			System.arraycopy(objectStack, 0, newObjectStack, 0, objectStack.length);
			objectStack = newObjectStack;
		}
		objectStack[nextIndex] = object;
		++nextIndex;
	}
	/**
	 * Determines if the container is empty.
	 * @return true if the container is empty
	 */
	public boolean isEmpty() {
		return nextIndex == 0;
	}
	/**
	 * Adds an object to the container and draws its preview then updates the supplied preview state.
	 * 
	 * @param object the object to add to the drawing list
	 * @param gc the GC to draw on
	 * @param offset the offset to add to virtual coordinates to get display coordinates
	 * @param rememberedState the state returned by a previous drawPreview() or addAndPreview()
	 *        using this Container, may be null if there was no such previous call
	 * @return object state that must be passed to erasePreview() later to erase this object
	 */
//	public Object addAndPreview(Figure object, GC gc, Point offset, Object rememberedState) {
//		Object[] stateStack = (Object[]) rememberedState;
//		if (stateStack == null) {
//			stateStack = new Object[INITIAL_ARRAY_SIZE];
//		} else if (stateStack.length <= nextIndex) {
//			Object[] newStateStack = new Object[stateStack.length * 2];
//			System.arraycopy(stateStack, 0, newStateStack, 0, stateStack.length);
//			stateStack = newStateStack;
//		}
//		add(object);
//		stateStack[nextIndex - 1] = object.drawPreview(gc, offset);
//		return stateStack;
//	}
	/**
	 * Clears the container.
	 * <p>
	 * Note that erasePreview() cannot be called after this point to erase any previous
	 * drawPreview()'s.
	 * </p>
	 */
	public void clear() {
		while (--nextIndex > 0) objectStack[nextIndex] = null;
		nextIndex = 0;
	}
	public void draw(FigureDrawContext fdc) {
		for (int i = 0; i < nextIndex; ++i) ((BaseFigure) objectStack[i]).draw(fdc);
	}
	public void addDamagedRegion(FigureDrawContext fdc, Region region) {
		for (int i = 0; i < nextIndex; ++i) ((BaseFigure) objectStack[i]).addDamagedRegion(fdc, region);
	}
}


//... UNUSED CODE ......................................................................................................