//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: BaseFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/BaseFigure.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:14 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

//- IMPORT SECTION .........................................................................................
import harpoon.devel.FigureDrawContext;
import harpoon.devel.HarpoonPalette;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Region;


//- CLASS IMPLEMENTATION ...................................................................................
/**
 * Superinterface for all drawing objects.
 * All drawing objects know how to render themselves to the screen and share the display common attributes.
 * @author ldiego
 */
public abstract class BaseFigure {
//- INSTANCE PRIVATE FIELDS
	protected int flag;
	protected Color bgColor = HarpoonPalette.getFigureBGColor();
	protected Color fgColor = HarpoonPalette.getFigureFGColor();
	protected int xcoord = 0;
	protected int ycoord = 0;
	protected Rectangle bounds = new Rectangle( 0,0,0,0 );
	protected int[] shapePoints;

//- GETTER/SETTER PUBLIC OPERATIONS ........................................................................
	public Rectangle getBounds() {
		return this.bounds;
	}
	public int[] getShapePoints() {
		return shapePoints;
	}
	public void setShapePoints(int[] points) {
		this.shapePoints = points;
	}
	public Color getBgColor() {
		return bgColor;
	}
	public Color getFgColor() {
		return fgColor;
	}
	public void setBgColor(Color color) {
		bgColor = color;
	}
	public void setFgColor(Color color) {
		fgColor = color;
	}

//- PUBLIC OPERATIONS ......................................................................................
	/**
	 * Draws this object.
	 * 
	 * @param fdc a parameter block specifying drawing-related information
	 */
	public abstract void draw( FigureDrawContext fdc );
	public abstract void draw( int x, int y, GC gc );

	/**
	 * Computes the damaged screen region caused by drawing this object (imprecise), then
	 * appends it to the supplied region.
	 * 
	 * @param fdc a parameter block specifying drawing-related information
	 * @param region a region to which additional damage areas will be added
	 */
	public abstract void addDamagedRegion( FigureDrawContext fdc, Region region );

}
//- UNUSED CODE ............................................................................................
