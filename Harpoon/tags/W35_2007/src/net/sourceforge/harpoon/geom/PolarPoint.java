//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PolarPoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/PolarPoint.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:13 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-21 13:32:26  ldiego
//    - First implementation where the coordinate points are stored in PolarCoordintes
//      instead on decimal notation as the documentation states.
//

package net.sourceforge.harpoon.geom;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Defines the methods to access and work with geographical polar coordinates (where the radius is already
 * known and matches the Earth radius) in the nautical format of degrees minutes seconds or the simpler
 * representation of degrees in decimal notation.<br>
 * Internal representation is the format of instances of the class <code>PolarCoordinate</code> with
 * positive and negative values. There are methods to convert to DMS (degree, minutes, seconds) or to create
 * instances from that values. Current implementation does not include radian conversions even they may be
 * required in the future.<br>
 * <br>
 * There are no limits to the degree values used in the implementation. Other classes may restrict those
 * values to the Earth notation with the 360� limit to longitude and the -90� to 90� to latitude.<br>
 * Other representation may convert to nautical support like the 15� W restricting values to the range 0� to
 * �80� for longitude and adding the sense to West or East. Even there may be representation with the wind
 * rose quadrants like 45� NE.<br>
 * <br>
 * Other methods allow for some arithmetic operations on the point values.
 * 
 */
public class PolarPoint implements Serializable {

	/** Latitude of the point. */
	double	latitude	= 0.0;
	/** Longitude of the point. */
	double	longitude	= 0.0;

	// - C O N S T R U C T O R S
	/** Construct the Point from two coordinate values in decimal notation. */
	public PolarPoint(double lat, double lon) {
		this.latitude = lat;
		this.longitude = lon;
	}

	/** Construct the Point from two DMS coordinates. */
	public PolarPoint(PolarCoordinate latitude, PolarCoordinate longitude) {
		this.latitude = latitude.toDegrees();
		this.longitude = longitude.toDegrees();
	}

	// - G E T T E R S / S E T T E R S
	public PolarCoordinate getLatitude() {
		return new PolarCoordinate(this.latitude);
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLatitude(PolarCoordinate latitude) {
		this.latitude = latitude.toDegrees();
	}

	public PolarCoordinate getLongitude() {
		return new PolarCoordinate(this.longitude);
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setLongitude(PolarCoordinate longitude) {
		this.longitude = longitude.toDegrees();
	}

	// - A R I T H M E T I C S
	public void add(double lat, double lon) {
		this.longitude += lat;
		this.longitude += lon;
	}

	public void add(PolarCoordinate latitude, PolarCoordinate longitude) {
		this.longitude += latitude.toDegrees();
		this.longitude += longitude.toDegrees();
	}

	public void add(PolarPoint displacement) {
		if (null != displacement) {
			this.latitude += displacement.getLatitude().toDegrees();
			this.longitude += displacement.getLongitude().toDegrees();
		}
	}

	public void addLatitude(double amount) {
		this.longitude += amount;
	}

	public void addLatitude(PolarCoordinate coordinate) {
		this.longitude += coordinate.toDegrees();
	}

	public void addLongitude(double amount) {
		this.longitude += amount;
	}

	public void addLongitude(PolarCoordinate coordinate) {
		this.longitude += coordinate.toDegrees();
	}

}

// - UNUSED CODE ............................................................................................
