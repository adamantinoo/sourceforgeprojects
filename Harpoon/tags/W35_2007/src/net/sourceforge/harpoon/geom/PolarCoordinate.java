//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PolarCoordinate.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/PolarCoordinate.java,v $
//  LAST UPDATE:    $Date: 2007-08-27 10:25:13 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.geom;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Date;
import java.util.List;

// - CLASS IMPLEMENTATION ...................................................................................
public class PolarCoordinate implements Serializable {
	// private double degrees;
	// private double minutes;
	// private double seconds;
	private double		decimalDegrees	= 0.0;

	static public PolarCoordinate fromDMS(int degrees, int minutes, int seconds) {
		return new PolarCoordinate(degrees, minutes, seconds);
	}

	public PolarCoordinate(int degrees, int minutes, int seconds) {
		this.decimalDegrees = degrees + minutes * 1.0 / 60.0 + seconds * 1.0 / 60.0 * 1.0 / 60.0;
	}

	public PolarCoordinate(double degrees) {
		this.decimalDegrees=degrees;
	}

	public double toDegrees() {
		// TODO Auto-generated method stub
		return this.decimalDegrees;
	}

	public double addDegrees(double displacement) {
		this.decimalDegrees += displacement;	
		// - Check for coordinates wrap over 360 degrees
//		if (this.decimalDegrees > 360.0) this.decimalDegrees -= 360.0;
		return this.decimalDegrees;
	}
	public double subDegrees(double displacement) {
		this.decimalDegrees -= displacement;	
		// - Check for coordinates wrap over 360 degrees
//		if (this.decimalDegrees > 360.0) this.decimalDegrees -= 360.0;
		return this.decimalDegrees;
	}

	public boolean checkLatitudeLimits() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean checkLongitudeLimits() {
		// TODO Auto-generated method stub
		return true;
		
	}
	public String toString() {
		return new Double(this.decimalDegrees).toString();
	}

//	public double getDegrees() {
//		// TODO Auto-generated method stub
//		return this.decimalDegrees;
//	}
}

// - UNUSED CODE ............................................................................................
