//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/RootMapUnit.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:22 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.Properties;

import net.sourceforge.harpoon.figures.HarpoonMap;
import net.sourceforge.harpoon.figures.MapFigure;
import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Root of the element model for the game.<br>
 * This model element contains the list of all the units available on the scenery. There are methods to create
 * more units that get added to the model on the fly, but those operations are used from a special menu on the
 * UI.<br>
 * This element in the model gets represented by the Map and so it has a lot of properties that control the
 * presentation visibility and graphic characteristics. The current implementation uses a helper class to
 * implement that functionality called <code>HarpoonMap</code>.
 */
public class RootMapUnit extends ContainerUnit {
	private static final long	serialVersionUID	= -3072256166014998802L;
	private Properties	props;
	private HarpoonMap	map;

	// - P U B L I C S E C T I O N
	public void createTestUnits() {
		// - Clean the current model first.
		this.clear();

		// - Create a full defined battleship unit.
		ShipUnit unit = new ShipUnit(this);
		unit.setName("USS Alpha");
		unit.setSide(Unit.FRIEND);
		unit.setPosition(new PolarCoordinate(51, 40, 30).toDegrees(), new PolarCoordinate(0, 6, 30).toDegrees());
		// TODO Create a new speed for the ship.
		// TODO Instance a ship from the catalog of ship types.
		ShipType unitType = new ShipType("Frigate Class");
		unit.setType(unitType);
		unit.setSpeed(10);
		unit.setDirection(90.0);
		this.addChild(unit);
		
		// - Create a FOE unit
		unit = new ShipUnit(this);
		unit.setName("URSS beta");
		unit.setSide(Unit.FOE);
		unit.setPosition(new PolarCoordinate(51, 35, 30).toDegrees(), new PolarCoordinate(0, 3, 30).toDegrees());
		// TODO Create a new speed for the ship.
		// TODO Instance a ship from the catalog of ship types.
		unitType = new ShipType("Frigate Class");
		unit.setType(unitType);
		unit.setSpeed(10);
		unit.setDirection(90.0);
		this.addChild(unit);
	}

	private void clear() {
		// TODO Auto-generated method stub
		this.children.clear();
	}

	public Properties getMapProperties() {
		// TODO Auto-generated method stub
		return this.props;
	}

	public void setMapProperties(Properties props) {
		// TODO Auto-generated method stub
		this.props = props;
//		// TODO Auto-generated constructor stub
//		this.map = new HarpoonMap();
//		map.setConfiguration(props);
//		map.readMapData();
	}

	public void setMap(MapFigure map) {
		// TODO Auto-generated method stub
		this.map=map;
	}

	public HarpoonMap getMap() {
		// TODO Auto-generated method stub
		return this.map;
	}
}

// - UNUSED CODE ............................................................................................
