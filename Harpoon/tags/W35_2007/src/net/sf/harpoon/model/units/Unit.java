//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: Unit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/Unit.java,v $
//  LAST UPDATE:    $Date: 2007-08-30 08:57:22 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//
//    Revision 1.2  2007-08-21 13:45:11  ldiego
//    - Initial implementation in a working model that has to be adapted to a more classical model.
//
//    Revision 1.1  2007-08-20 13:05:41  ldiego
//    - This first release implements the base structure for storing the initial
//      attributes for a game unit.
//

package net.sourceforge.harpoon.units;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;

import net.sourceforge.harpoon.geom.PolarPoint;
import net.sourceforge.harpoon.model.ShipType;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Has the base and common behavior for all game units not depending if they are air borne or ships. This
 * class is the base building block for the game user model that is manipulated by the game controller.<br>
 * Is <code>Serializable</code> to allow for game saving and model storage and also has to implement the key
 * methods for the <code>IPropertySource</code> interface to allow for controller registration.<br>
 * <br>
 * The common characteristics for all model elements on the game are defined next:
 * <dl>
 * <dt>Name</dt>
 * <dd>Not all units have a real name like USS Enterprise or Enola Gay. But all them have to have a user
 * reference to help the right identification. Names for some units may change because the creation and
 * destruction of fleets or task forces may require to do so.<br>
 * Game contacts also have to be named, so a single <code>Unit</code> may have more than one name at the
 * same time or during the game lifetime.<br>
 * So the <code>Name</code> property may have different senses depending on the environment but mainly
 * server to the purpose of identification. If the game database or user does not give a name to an unit it
 * will generate one depending on name type.</dd>
 * </dl>
 * 
 * <b>References:</b><br>
 * http://www.eclipse.org/articles/Article-GEF-diagram-editor/shape.html<br>
 * http://java.sun.com/developer/technicalArticles/Programming/serialization/
 */
public abstract class Unit implements /* IPropertySource ,*/ Serializable {

	private static final long	serialVersionUID	= 1L;
	public static final int		UNKNOWN_SIDE			= 0;
	public static final int		FRIEND						= 1;
	public static final int		FOE								= 2;
	public static final int		NEUTRAL						= 3;
	public static final int	SPEED_CRUISE	= 2;
	/** Contains an identifier to the Unit. This can be a user defined name or a database supplied name. */
	protected String					name;
	/** Unit location in the global coordinates. */
	protected PolarPoint			position					= new PolarPoint(0, 0);
	/** Unit side. This allows to identify other units not known to the user or detected during the game. */
	protected int						side							= UNKNOWN_SIDE;

	// protected HarpoonUnits parent;

	// protected PolarCoordinate latitude;
	// protected PolarCoordinate longitude;
	// protected Color redColor;

	// - C O N S T R U C T O R S
//	public Unit() {}
	// public Unit(HarpoonUnits harpoonUnits, String unitName) {
	// this.parent = harpoonUnits;
	// this.name = unitName;
	// this.side = UNKNOWN_SIDE;
	// }
	//
	// public Unit(HarpoonUnits harpoonUnits, String unitName, int side) {
	// this.parent = harpoonUnits;
	// this.name = unitName;
	// this.side = side;
	// }

	// - G E T T E R S / S E T T E R S
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	public double getLatitude() {
		return this.position.getLatitude().toDegrees();
	}

	public void setLatitude(double lat) {
		// TODO Auto-generated method stub
		this.position.setLatitude(lat);
	}

	public double getLongitude() {
		return this.position.getLongitude().toDegrees();
	}

	public void setLongitude(double lon) {
		this.position.setLongitude(lon);
	}

	public void setPosition(double lat, double lon) {
		this.setLatitude(lat);
		this.setLongitude(lon);
	}

	public void setType(ShipType unitType) {
		// TODO Auto-generated method stub
		
	}
}

// - UNUSED CODE ............................................................................................
