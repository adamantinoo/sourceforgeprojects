//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/RootMapUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-25 11:44:40 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.4  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.3  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.Properties;

import net.sourceforge.harpoon.geom.DMSCoordinate;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Root of the element model for the game.<br>
 * This model element contains the list of all the units available on the scenery. There are methods to create
 * more units that get added to the model on the fly, but those operations are used from a special menu on the
 * UI.<br>
 * This element in the model gets represented by the Map and so it has a lot of properties that control the
 * presentation visibility and graphic characteristics. The current implementation uses a helper class to
 * implement that functionality called <code>HarpoonMap</code>.
 */
public class RootMapUnit extends Unit {
	private static final long	serialVersionUID	= -3072256166014998802L;
	/** Represent the scenery properties that define some of the map presentation characteristics */
	private Properties				props;
	private DMSCoordinate	latitude2Zoom;
	private DMSCoordinate	longitude2Zoom;
	private StringBuffer description=new StringBuffer();

	/**
	 * This class requires a constructor because it is a special implementation of a Unit. It is the root for
	 * the model but at the same time contains the Map information where the other units may move. This model
	 * has a lot of properties that are read from the scenery file but the global Unit properties have to be set
	 * to default values on the construction.<br>
	 * These values map the next properties:
	 * <ul>
	 * <li>name - the scenery description name like "North sea maneuvers".</li>
	 * <li>latitude - latitude for the top-left corner of the map.</li>
	 * <li>longitude - longitude for the top-left corner of the map.</li>
	 * <li>side - set to NEUTRAL but has no impact on presentation.</li>
	 * </ul>
	 */
	public RootMapUnit() {
		// TODO Auto-generated constructor stub
		setName("North sea maneuvers");
		setLatitude(new DMSCoordinate(0, 0, 0, 'N'));
		setLongitude(new DMSCoordinate(0, 0, 0, 'E'));
		setSide(Unit.NEUTRAL);
	}

	//- G E T T E R S / S E T T E R S
	public void setMapProperties(Properties props) {
		this.props = props;
		if(null==props)this.props=new Properties();
		//TODO Process the properties received if they are required or leave processing until request.
		//- Get the scenery name and description from the properties.
		String sceneryName = props.getProperty("sceneryName",this.getName());
		this.setName(sceneryName);
		//TODO Read the description location and the description file to display the second page that are the scenery instructions.
		String sceneryDescription = props.getProperty("sceneryDescLocation","U:/ldiego/Workstage/3.3_BaseWorkspace/HarpoonRCP/sceneries/Lesson01_description.html");
		
		//- Read the location of the top-left corner
		String topLat = props.getProperty("topLatitude", "0 0 0 N");
		String[] components = topLat.split(" ");
		this.setLatitude(new DMSCoordinate(new Integer(components[0]).intValue(),
				new Integer(components[1]).intValue(), new Integer(components[2]).intValue(),components[3].charAt(0)));
		String topLon = props.getProperty("topLatitude", "0 0 0 N");
		components = topLon.split(" ");
		setLongitude(new DMSCoordinate(new Integer(components[0]).intValue(),
				new Integer(components[1]).intValue(), new Integer(components[2]).intValue(),components[3].charAt(0)));
	}

	public Properties getMapProperties() {
		if (null == this.props) this.props = new Properties();
		return props;
	}
	public DMSCoordinate getLatitude2Zoom() {
		if (null == this.latitude2Zoom) {
			// - Read value from the properties and the store the value in the field.
			String topLat = props.getProperty("latitude2Zoom", "0 10 0");
			String[] components = topLat.split(" ");
			this.latitude2Zoom = new DMSCoordinate(new Integer(components[0]).intValue(),
					new Integer(components[1]).intValue(), new Integer(components[2]).intValue());
		}
		return this.latitude2Zoom;
	}

	public DMSCoordinate getLongitude2Zoom() {
		if (null == this.longitude2Zoom) {
			// - Read value from the properties and the store the value in the field.
			String topLat = props.getProperty("longitude2Zoom", "0 10 0");
			String[] components = topLat.split(" ");
			this.longitude2Zoom = new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(
					components[1]).intValue(), new Integer(components[2]).intValue());
		}
		return this.longitude2Zoom;
	}

}

// - UNUSED CODE ............................................................................................
// public void createTestUnits() {
// // - Clean the current model first.
// this.clear();
//
// // - Create a full defined battleship unit.
// ShipUnit unit = new ShipUnit();
// unit.setName("USS Alpha");
// unit.setSide(Unit.FRIEND);
// unit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());unit.setLongitude(new PolarCoordinate(0, 6,
// 30).toDegrees());
// // TODO Create a new speed for the ship.
// // TODO Instance a ship from the catalog of ship types.
// ShipType unitType = new ShipType("Frigate Class");
// unit.setType(unitType);
// unit.setSpeed(10);
// unit.setDirection(90.0);
// this.addChild(unit);
//		
// // - Create a FOE unit
// unit = new ShipUnit();
// unit.setName("URSS beta");
// unit.setSide(Unit.FOE);
// unit.setLatitude(new PolarCoordinate(51, 35, 30).toDegrees());unit.setLongitude(new PolarCoordinate(0, 3,
// 30).toDegrees());
// // TODO Create a new speed for the ship.
// // TODO Instance a ship from the catalog of ship types.
// unitType = new ShipType("Frigate Class");
// unit.setType(unitType);
// unit.setSpeed(10);
// unit.setDirection(90.0);
// this.addChild(unit);
//		
// // - Create a FRIEND Air unit.
// AirUnit aunit = new AirUnit();
// aunit.setName("F16 Gamma");
// aunit.setSide(Unit.FRIEND);
// aunit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());aunit.setLongitude(new PolarCoordinate(0,
// 12, 0).toDegrees());
// aunit.setSpeed(500);
// aunit.setDirection(100.0);
// this.addChild(aunit);
//		
// // - Create an air base
// AirportUnit airunit = new AirportUnit();
// airunit.setName("Air Base");
// airunit.setSide(Unit.FRIEND);
// airunit.setLatitude(new PolarCoordinate(51,32,0).toDegrees());
// airunit.setLongitude(new PolarCoordinate(0,3,0).toDegrees());
// this.addChild(airunit);
// }

// public Properties getMapProperties() {
// return this.props;
// }

// public void setMap(MapFigure map) {
// // TODO Auto-generated method stub
// this.map=map;
// }
//
// public HarpoonMap getMap() {
// // TODO Auto-generated method stub
// return this.map;
// }
