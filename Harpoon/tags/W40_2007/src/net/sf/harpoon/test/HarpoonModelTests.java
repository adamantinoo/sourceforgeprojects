//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonModelTests.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/HarpoonModelTests.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:12:41 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import junit.framework.TestCase;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.ShipType;
import net.sourceforge.harpoon.model.ShipUnit;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class HarpoonModelTests extends TestCase {
	// public void testGetScenearyName() throws Exception {
	// final String[] OPEN_FILTER_NAMES = new String[] { "Sceneries (*.scenery)" };
	// final String[] OPEN_FILTER_EXTENSIONS = new String[] { "*.scenery" };
	// final Display display = new Display();
	// final Shell window = new Shell(display);
	// final FileDialog fileChooser = new FileDialog(window, SWT.OPEN);
	//
	// fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
	// fileChooser.setFilterNames(OPEN_FILTER_NAMES);
	// String sceneryName = fileChooser.open();
	// String sceneryPath = fileChooser.getFilterPath();
	//
	// String expectedPath = "";
	// assertEquals("Get file path", expectedPath, sceneryPath);
	// }

	public void testModelMulti() throws Exception {
		FileOutputStream fos = new FileOutputStream(
				"U:/ldiego/Workstage/Harpoon/sceneries/TestModelMulti.harpoon");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		RootMapUnit model = new RootMapUnit();
		createTestUnitsModelMulti(model);
		oos.writeObject(model);

		oos.close();
	}

	public void testModel1() throws Exception {
		FileOutputStream fos = new FileOutputStream("U:/ldiego/Workstage/Harpoon/sceneries/TestModel.harpoon");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		RootMapUnit model = new RootMapUnit();
		createTestUnitsModel1(model);
		oos.writeObject(model);

		oos.close();
	}

	public void testModel0() throws Exception {
		FileOutputStream fos = new FileOutputStream("U:/ldiego/Workstage/Harpoon/sceneries/TestModel0.harpoon");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		RootMapUnit model = new RootMapUnit();
		createTestUnitsModel0(model);
		oos.writeObject(model);

		oos.close();
	}

	/**
	 * Create a testing units.<br>
	 * Latitudes are made south because the model has a location of 0-0 at the top-left. Below that location (y
	 * coordinates positive) we have units in the south hemisphere.
	 */
	private void createTestUnitsModelMulti(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// - Create and test the Air bases or Airports
		AirportUnit base = new AirportUnit();
		base.setName("Interoceanic airport");
		base.setSide(Unit.FRIEND);
		base.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		base.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		model.addChild(base);

		base = new AirportUnit();
		base.setName("Enemy airport");
		base.setSide(Unit.FOE);
		base.setLatitude(new DMSCoordinate(0, 3, 0, 'S'));
		base.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		model.addChild(base);

		// - Create and test Air units.
		AirUnit aunit = new AirUnit();
		aunit.setName("F16 Gamma");
		aunit.setSide(Unit.FRIEND);
		aunit.setLatitude(new DMSCoordinate(0, 4, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		aunit.setSpeed(500);
		aunit.setDirection(100);
		model.addChild(aunit);

		aunit = new AirUnit();
		aunit.setName("SUv k21");
		aunit.setSide(Unit.FOE);
		aunit.setLatitude(new DMSCoordinate(0, 5, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		aunit.setSpeed(500);
		aunit.setDirection(200);
		model.addChild(aunit);

		aunit = new AirUnit();
		aunit.setName("Boeing 747");
		aunit.setSide(Unit.NEUTRAL);
		aunit.setLatitude(new DMSCoordinate(0, 6, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		aunit.setSpeed(800);
		aunit.setDirection(90);
		model.addChild(aunit);

		// - Create an air base
		// TestUnit tunit=new TestUnit();
		// tunit.setName("Interoceanic airport");
		// tunit.setSide(Unit.FRIEND);
		// tunit.setLatitude(new DMSCoordinate(0,9,0,'S'));
		// tunit.setLongitude(new DMSCoordinate(0,8,0,'E'));
		// model.addChild(tunit);

		MultiUnit multi = new MultiUnit();
		multi.setName("Type 01 FRIEND");
		UnitProfile profile = new UnitProfile();
		profile.setName("T1");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FRIEND);
		multi.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 8, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Type 01 FOE");
		profile = new UnitProfile();
		profile.setName("T2");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FOE);
		multi.setLatitude(new DMSCoordinate(0, 4, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 8, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Type 03 FRIEND");
		profile = new UnitProfile();
		profile.setName("T3");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FRIEND);
		multi.setLatitude(new DMSCoordinate(0, 6, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 8, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Type 04 FOE");
		profile = new UnitProfile();
		profile.setName("T4");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FOE);
		multi.setLatitude(new DMSCoordinate(0, 7, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 8, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Multi Submarine");
		profile = new UnitProfile();
		profile.setName("Submarine");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FRIEND);
		multi.setLatitude(new DMSCoordinate(0, 12, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Multi Surface");
		profile = new UnitProfile();
		profile.setName("Surface");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FOE);
		multi.setLatitude(new DMSCoordinate(0, 13, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Multi Air");
		profile = new UnitProfile();
		profile.setName("Figther");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FOE);
		multi.setLatitude(new DMSCoordinate(0, 14, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Multi Missile");
		profile = new UnitProfile();
		profile.setName("Missile");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FOE);
		multi.setLatitude(new DMSCoordinate(0, 15, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		model.addChild(multi);

		multi = new MultiUnit();
		multi.setName("Multi Base");
		profile = new UnitProfile();
		profile.setName("Base");
		profile.setMargin(2);
		multi.setProfile(profile);
		multi.setSide(Unit.FRIEND);
		multi.setLatitude(new DMSCoordinate(0, 16, 0, 'S'));
		multi.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		model.addChild(multi);

	}

	/**
	 * Create a testing units.<br>
	 * Latitudes are made south because the model has a location of 0-0 at the top-left. Below that location (y
	 * coordinates positive) we have units in the south hemisphere.
	 */
	private void createTestUnitsModel0(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// - Create an air base
		AirportUnit airunit = new AirportUnit();
		airunit.setName("123456789");
		airunit.setSide(Unit.FRIEND);
		// airunit.setLatitude(new PolarCoordinate(0, 3, 0).toDegrees());
		// airunit.setLongitude(new PolarCoordinate(0, 3, 0).toDegrees());
		airunit.setLatitude(new DMSCoordinate(0, 3, 0, 'S'));
		airunit.setLongitude(new DMSCoordinate(0, 3, 0, 'E'));
		model.addChild(airunit);

		// - Create a FRIEND Air unit.
		AirUnit aunit = new AirUnit();
		aunit.setName("F16 Gamma");
		aunit.setSide(Unit.FRIEND);
		// aunit.setLatitude(new PolarCoordinate(0, 5, 0).toDegrees());
		// aunit.setLongitude(new PolarCoordinate(0, 5, 0).toDegrees());
		aunit.setLatitude(new DMSCoordinate(0, 5, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 5, 0, 'E'));
		aunit.setSpeed(500);
		aunit.setDirection(100);
		model.addChild(aunit);

		TestUnit tunit = new TestUnit();
		tunit.setName("Segundo Nombre");
		tunit.setSide(Unit.FOE);
		tunit.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		tunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		model.addChild(tunit);

		// // - Create a FRIEND Air unit.
		// aunit = new AirUnit();
		// aunit.setName("F16 Gamma");
		// aunit.setSide(Unit.FRIEND);
		// aunit.setLatitude(new PolarCoordinate(0, 5, 0).toDegrees());
		// aunit.setLongitude(new PolarCoordinate(0, 5, 0).toDegrees());
		// aunit.setSpeed(300);
		// aunit.setDirection(80.0);
		// model.addChild(aunit);
	}

	private void createTestUnitsModel1(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// - Create a full defined battleship unit.
		ShipUnit unit = new ShipUnit();
		unit.setName("USS Alpha");
		unit.setSide(Unit.FRIEND);
		// unit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());
		// unit.setLongitude(new PolarCoordinate(0, 6, 30).toDegrees());
		// TODO Create a new speed for the ship.
		// TODO Instance a ship from the catalog of ship types.
		ShipType unitType = new ShipType("Frigate Class");
		unit.setType(unitType);
		unit.setSpeed(10);
		unit.setDirection(90);
		model.addChild(unit);

		// - Create a FOE unit
		unit = new ShipUnit();
		unit.setName("URSS beta");
		unit.setSide(Unit.FOE);
		// unit.setLatitude(new PolarCoordinate(51, 35, 30).toDegrees());
		// unit.setLongitude(new PolarCoordinate(0, 3, 30).toDegrees());
		// TODO Create a new speed for the ship.
		// TODO Instance a ship from the catalog of ship types.
		unitType = new ShipType("Frigate Class");
		unit.setType(unitType);
		unit.setSpeed(10);
		unit.setDirection(90);
		model.addChild(unit);

		// - Create a FRIEND Air unit.
		AirUnit aunit = new AirUnit();
		aunit.setName("F16 Gamma");
		aunit.setSide(Unit.FRIEND);
		// aunit.setLatitude(new PolarCoordinate(51, 40, 30).toDegrees());
		// aunit.setLongitude(new PolarCoordinate(0, 12, 0).toDegrees());
		aunit.setSpeed(500);
		aunit.setDirection(100);
		model.addChild(aunit);

		// - Create an air base
		AirportUnit airunit = new AirportUnit();
		airunit.setName("Air Base");
		airunit.setSide(Unit.FRIEND);
		// airunit.setLatitude(new PolarCoordinate(51, 32, 0).toDegrees());
		// airunit.setLongitude(new PolarCoordinate(0, 3, 0).toDegrees());
		model.addChild(airunit);
	}
}

// - UNUSED CODE ............................................................................................
