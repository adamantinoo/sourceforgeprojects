//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonMap.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/HarpoonMap.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:29 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//
//    Revision 1.1  2007-08-17 11:49:01  ldiego
//    - First working release of the project where the Open menu may
//      open and draw a 2 x 2 block map area with the coordinates
//      configured inside the external scenery file.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The <code>HarpoonMap</code> class will control the presentation of the battle set maps. The map
 * information will be downloaded from Google Map using the URLs to get the political map information. There
 * will be a button to allow the selection of other types of maps once they are available.<br>
 * The map is composed by a set of tiles of 256 x 256 pixels each composed inside an array of blocks. Teh
 * number of blocks depend on the current size of the window.
 * 
 * MAP DATA<br>
 * The map of coordinates:
 * <ul>
 * <li>zoomFactor=6</li>
 * <li>topLatBlock=404</li>
 * <li>topLonBlock=w2.60</li>
 * <li>topX=1023</li>
 * <li>topY=685</li>
 * </ul>
 * is just at the left of first meridian at a longitude about<br>
 * 50 58' 10''<br>
 * the next square in the south direction has coordinates<br>
 * 50 51' 10''<br>
 * and the next one moves to the north coordinates<br>
 * 51 04' 10''.<br>
 * 
 * This makes that differences between squareas are:<br>
 * 00 07' 00'' 00 06' 00''
 * 
 * The coordinates 1023 matches the first meridian.
 * 
 * 681 top 51 30'30'' 682 top 51 23'55'' 6'35'' 683 top 51 17'12'' 6'43'' 684 top 51 10'30'' 6'42''
 * 
 * 51 10'48'' 51 11'00''
 * 
 * Estimacion 6'30'' 682 51 24'00'' 684 51 11'00''
 * 
 * 
 * 681 1024 51 30'30'' 0 10'30'' 681 1025 51 30'30'' 0 21'00''
 * 
 * 0 10'30''
 */
public class HarpoonMap extends UnitFigure {

	/** This is the prefix to locate political maps in Google Map. */
	private static final String	GOOGLE_PREFIX				= "http://mt2.google.com/mt?";
	/** Those are the values for the size of the blocks of the images received from Google. */
	protected static final int	GOOGLE_BLOCK_HEIGHT	= 256;
	protected static final int	GOOGLE_BLOCK_WIDTH	= 256;

	// - L O C A L F I E L D S
	/** Represent the scenery properties that define some of the map presentation characteristics */
	protected Properties				props;
	/** Array structure where to store all map data read from the different URLs. */
	protected ImageData[]				mapDataArray				= null;

	// private DMSCoordinate latitude;
	// //[3]
	// private DMSCoordinate longitude;

	// - C L A S S C O N S T R U C T O R S
	// [4]

	/** Obtain the Google URL reference to the piece of map. */
	String getURLReference(int xBlock, int yBlock) {
		// TODO Generate the URL based on the Google prefix and the internal configuration data for the scenery
		StringBuffer url = new StringBuffer(GOOGLE_PREFIX);
		url.append("n=").append(getTopLatBlock());
		url.append("&v=").append(getTopLonBlock());
		url.append("&x=").append(xBlock);
		url.append("&y=").append(yBlock);
		url.append("&zoom=").append(getZoomFactor());
		return url.toString();
	}

	/**
	 * Obtain the Google URL reference to the piece of map but this time the coordinates are relative the the
	 * current top-left point.
	 */
	// String getURLReferenceRelative(int xBlock, int yBlock) {
	// // TODO Generate the URL based on the Google prefix and the internal configuration data for the scenery
	// StringBuffer url = new StringBuffer(GOOGLE_PREFIX);
	// url.append("n=").append(this.getTopLatBlock());
	// url.append("&v=").append(this.getTopLonBlock());
	// url.append("&x=").append(this.getTopX() + xBlock);
	// url.append("&y=").append(this.getTopY() + yBlock);
	// url.append("&zoom=").append(this.getZoomFactor());
	// return url.toString();
	// }
	int getZoomFactor() {
		// TODO Intercept NumberFormatExcetion exceptions or return the appropriate exception to the caller.
		final String propValue = props.getProperty("zoomFactor", "11");
		final Integer numberValue = new Integer(propValue);
		return numberValue.intValue();
	}

	String getSceneryName() {
		return props.getProperty("sceneryName", "-SceneryName not defined-");
	}

	// - P R I V A T E S E C T I O N
	private int getTopLatBlock() {
		// TODO Intercept NumberFormatExcetion exceptions or return the appropriate exception to the caller.
		final String propValue = props.getProperty("topLatBlock", "404");
		final Integer numberValue = new Integer(propValue);
		return numberValue.intValue();
	}

	private String getTopLonBlock() {
		return props.getProperty("topLonBlock", "w2.60");
	}

	// private Object getBlockWidth() {
	// // TODO Auto-generated method stub
	// return 3;
	// }

	/**
	 * Map data information is kept in an array of <code>ImageData</code> instances. Each block is 256 x 256
	 * and there are lines and columns when gathering that information. The drawing for this information is
	 * performed with this cached data.<br>
	 * The data to be read depends on the coordinates for the top left block and the size of the canvas where we
	 * have to get the images. There is also a cache of some images su new access to GFogle are not necessary
	 * each time the user scrolls.<br>
	 * This sizes are adjusted every time the window is resized and the scroll buttons are touched.
	 */
	// public void readMapData() {
	// // - Calculate the number of blocks to be read.
	// final int hBlocks = getHorizontalBlocks();
	// final int vBlocks = getVerticalBlocks();
	// final int noBlocks = Math.max(1, hBlocks) * Math.max(1, vBlocks);
	//
	// this.mapDataArray = new ImageData[noBlocks];
	// this.setSize(hBlocks * 256, vBlocks * 256);
	// for (int j = 0; j < vBlocks; j++) {
	// for (int i = 0; i < hBlocks; i++) {
	// try {
	// ImageData blockImageData = readBlockData(getURLReference(getTopX() + i, getTopY() + j));
	// this.mapDataArray[j * hBlocks + i] = blockImageData;
	// } catch (MalformedURLException e) {
	// // TODO Create and use an error alerter to send the suer messages about exceptions and errors
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// }
	// repaint();
	// }
	// private int getTopX() {return 0;}
	// private int getTopY() {return 0;}
	/** Reads a block of image data from Google and returns the first frame to the caller. */
	protected ImageData readBlockData(String imageName) throws MalformedURLException, IOException {
		// - Read the new image(s) from the chosen URL.
		URL url = new URL(imageName);
		InputStream stream = url.openStream();
		ImageLoader loader = new ImageLoader();
		ImageData[] blockData = loader.load(stream);
		stream.close();
		return blockData[0];
	}

	/**
	 * The size of the blocks that can be shown is obtained from the windows size. If the window is resized the
	 * the number of blocks may change.
	 */
	// public int getVerticalBlocks() {
	// // TODO Calculate the number of blocks depending on the window size.
	// // Integer height = new Integer(HarpoonApp.getShell().getSize().x);
	// // Integer noBlocks = new Integer(height / GOOGLE_BLOCK_HEIGHT);
	// // return Math.max(1, noBlocks.intValue());
	// return 3;
	// }
	/**
	 * The size of the blocks that can be shown is obtained from the windows size. If the window is resized the
	 * the number of blocks may change.
	 */
	// public int getHorizontalBlocks() {
	// // Integer noBlocks = new Integer(HarpoonApp.getShell().getSize().y / GOOGLE_BLOCK_WIDTH);
	// return 3;/*Math.max(1, noBlocks.intValue());*/
	// }
	// public DMSCoordinate getLongitude2Zoom() {
	// return new DMSCoordinate(0,10,0);
	// }
	//
	// public DMSCoordinate getLatitude2Zoom() {
	// return new DMSCoordinate(0,10,0);
	// }
	//
	// public double getTopLatitude() {
	// return topLatitude.toDegrees();
	// }
	//
	// public double getTopLongitude() {
	// return topLongitude.toDegrees();
	// }
	@Override
	public Point getHotSpot() {
		// TODO Auto-generated method stub
		return new Point(0, 0);
	}

	// public DMSCoordinate getDMSLatitude() {
	// if (null==this.latitude) {
	// //- Read value from the properties and the store the value in the field.
	// String topLat = props.getProperty("topLatitude", "10 0 0 N");
	// String[] components = topLat.split(" ");
	// latitude = new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
	// .intValue(), new Integer(components[2]).intValue(),components[3].charAt(0));
	// }
	// return this.latitude;
	// }
	// public DMSCoordinate getDMSLongitude() {
	// if (null==this.longitude) {
	// //- Read value from the properties and the store the value in the field.
	// String topLat = props.getProperty("topLongitude", "1 0 0 E");
	// String[] components = topLat.split(" ");
	// longitude = new DMSCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
	// .intValue(), new Integer(components[2]).intValue(),components[3].charAt(0));
	// }
	// return this.longitude;
	// }
}

// - UNUSED CODE ............................................................................................
// [1]
// public void readScenary(String scenaryName) throws FileNotFoundException, IOException {
// props = new Properties();
// // TODO Check the existence of the scenery before loading it
// // TODO Get the Scenery default directory from the application configuration properties
// final String scenaryDirectory = "U:\\ldiego\\Workstage\\Harpoon\\scenaries";
// File scenery = new File(scenaryDirectory, scenaryName);
// props.load(new BufferedInputStream(new FileInputStream(scenary))); //$NON-NLS-1$
// }

// [2]
// /**
// * This method calculates the particular size for this object based on the special premises:
// * <ul>
// * <li>The size must be a multiple of 256 pixels (but this can be changed later).</li>
// * <li>The minimum size is 512 x 512 pixels.</li>
// * </ul>
// *
// * @Override
// */
// public Point computeSizeDummy(int hintX, int hintY) {
// final Point preferred = new Point(hintX, hintY);
// if (hintX < 400) preferred.x = 400;
// if (hintY < 400) preferred.y = 400;
//
// // - Calculate max size depending on the window size
// final Point shellSize = this.getParent().getShell().getSize();
// preferred.x = new Double(Math.floor((shellSize.x - 100) / 100.0)).intValue() * 100;
// preferred.y = new Double(Math.floor((shellSize.y - 100) / 100.0)).intValue() * 100;
//
// return preferred;
// }
//
// /** @Override */
// public Point computeSizeDummy(int hint, int hint2, boolean changed) {
// return this.computeSize(hint, hint2);
// }
// [3]
// /** Path to the scenery file that has been loaded or null if there is no scenery */
// private String sceneryPath = null;
// - W I D G E T S
/* References to the different cursors that may be used on the interface. */
// /** This is the standard cursor associated to the Map. */
// private Cursor crossCursor = new Cursor(this.getDisplay(), SWT.CURSOR_CROSS);
// /** This is the cursor to show where the operation may take some time. */
// private Cursor waitCursor = new Cursor(this.getDisplay(), SWT.CURSOR_WAIT);
// private HarpoonModel units=new HarpoonModel(this);
// /** Stores the equivalence in minutes for a box for a determinate zoom factor. */
// private PolarCoordinate[] longitude2Zoom = new PolarCoordinate[12];
// /** The same but for latitudes. */
// private PolarCoordinate[] latitude2Zoom = new PolarCoordinate[12];
// private PolarCoordinate topLatitude;
// private PolarCoordinate topLongitude;
// private DMSCoordinate latitude=null;
// private DMSCoordinate longitude=null;
// [4]
// public HarpoonMap() {
// // - Initialize zoom to longitude equivalence for known levels.
// for (int i = 0; i < longitude2Zoom.length; i++) {
// longitude2Zoom[i] = new PolarCoordinate(0, 10, 0);
// latitude2Zoom[i] = new PolarCoordinate(0, 6, 30);
// }
// longitude2Zoom[6] = new PolarCoordinate(0, 6, 30);
// latitude2Zoom[6] = new PolarCoordinate(0, 10, 30);
// }
// public HarpoonMap(Composite parent, int style) {
// super(parent, style);
//
// // // - Add the listener to redraw the area if it has changed.
// // final HarpoonMap map = this;
// // map.addPaintListener(new PaintListener() {
// // public void paintControl(PaintEvent event) {
// // if (mapDataArray == null) {
// // Rectangle bounds = map.getBounds();
// // event.gc.fillRectangle(0, 0, bounds.width, bounds.height);
// // } else {
// // map.paintMap(event);
// // units.paint(event);
// // }
// // }
// // });
//
// // - Initialize zoom to longitude equivalence for known levels.
// for (int i = 0; i < longitude2Zoom.length; i++) {
// longitude2Zoom[i] = new PolarCoordinate(0, 10, 0);
// latitude2Zoom[i] = new PolarCoordinate(0, 6, 30);
// }
// longitude2Zoom[6] = new PolarCoordinate(0, 6, 30);
// latitude2Zoom[6] = new PolarCoordinate(0, 10, 30);
// }
// @Override
// public void dispose() {
// // TODO Auto-generated method stub
// super.dispose();
// crossCursor.dispose();
// waitCursor.dispose();
// }
// - P U B L I C S E C T I O N
// /** Processes the scenery properties and goes to Google to read the map data information. */
// public void openScenery(Properties props) {
// if (props != null) {
// this.props = props;
// // TODO Load some of the properties inside class fields to allow for better performance.
// String topLat = this.props.getProperty("topLatitude", "51 30 30");
// String[] components = topLat.split(" ");
// topLatitude = new PolarCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
// .intValue(), new Integer(components[2]).intValue());
// String topLon = this.props.getProperty("topLongitude", "0 0 0");
// components = topLon.split(" ");
// topLongitude = new PolarCoordinate(new Integer(components[0]).intValue(), new Integer(components[1])
// .intValue(), new Integer(components[2]).intValue());
// // TODO There is one else to this construction that is that no properties have to be counted as an
// // exception
// // - Load the Google images inside the canvas data to cache them.
// this.readMapData();
// }
// }
// /** Compute new size adding the hint from the shell parent. */
// @Override
// public Point computeSize(int hint, int hint2) {
// // TODO Auto-generated method stub
// Point superSize = super.computeSize(hint, hint2);
// if (256*2+10>superSize.x)superSize.x=256*2+10;
// if (256*2+10>superSize.y)superSize.y=256*2+10;
// return superSize;
// }
// [2]
// [1]
