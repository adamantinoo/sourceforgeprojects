//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SensorsFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/SensorsFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-26 16:56:35 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public class SensorsFigure  extends Figure {
	private boolean	radar;
	private boolean	sonar;
	private boolean	ECM;
	public void init() {
		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
//		this.repaint();
	}	
	public Dimension getHotSpot() {
		return new Dimension(50,50);
	}
	public Dimension getPreferredSize(int wHint, int hHint) {
return new Dimension(100,100);
	}
	protected void paintFigure(Graphics graphics) {
		// - This paints any figure parts that are defined in the hierarchy.
		super.paintFigure(graphics);

		// - If any sensor is activated, draw the sensor range.
		if (this.radar) {
			graphics.setForegroundColor(ColorConstants.white);
			Rectangle bo = bounds.getCopy();
//			bo.x=100;
//			bo.y=100;
			graphics.setBackgroundColor(ColorConstants.white);
			graphics.drawOval(bo.x,bo.y,bo.width-1,bo.height-1);
		}
		//DEBUG Draw the center point for debugging
		if(true) {
			Point loc = getLocation();
			Dimension hot = getHotSpot();
			graphics.drawLine(loc.x+hot.width, loc.y+hot.height,loc.x+hot.width+1, loc.y+hot.height+1);
		}
	}
	public void setRadarState(boolean radarState) {
		this.radar = radarState;
	}
	public void setSonarState(boolean sonarState) {
		this.sonar=sonarState;
	}
	public void setECMState(boolean state) {
		this.ECM=state;
	}

}

// - UNUSED CODE ............................................................................................
