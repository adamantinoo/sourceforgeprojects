//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovementPath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/MovementPath.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:29 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.figures.MovableFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.parts.RootMapPart;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The MovementPath is a model element that stores the list of points that compose a unit path. A path is a
 * series of lines with associated commands that form the movement commands assigned to a unit.<br>
 * The unit direction is then derived from this list of commands. The termination of the list creates a new
 * virtual command to continue to the same direction indefinitely or in case there is a command associated to
 * the point, doing the same command action until changed.
 */
public class MovementPath extends Unit implements Serializable {
	private static final long						serialVersionUID	= 2924232465880867103L;

	/** List of points that compose the movement path for a unit. */
	private final LinkedList<DMSPoint>	points						= new LinkedList<DMSPoint>();
	/**
	 * Target Unit that applies to the path commands. Required to have a backlink to the unit to know the first
	 * reference point in the command path.
	 */
	private Unit												target						= null;

	// - C O N S T R U C T O R S
	// public MovementPath() {
	// }

	public MovementPath(final Unit target) {
		this.target = target;
	}

	// - P U B L I C S E C T I O N
	/**
	 * Adds a new point to the command path. Teh point is added to the end of the chain. If the chain is empty
	 * then this is the first destination point and the current unit location os the from point.
	 */
	public void addPoint(final DMSPoint point) {
		if (points.isEmpty()) {
			// - Get the current location of the Unit as the first from point for this chain.
			points.addLast(new DMSPoint(target.getDMSLatitude(), target.getDMSLongitude()));
		}
		points.addLast(point);
	}

	public void addPoint(final DMSCoordinate latitude, final DMSCoordinate longitude) {
		addPoint(new DMSPoint(latitude, longitude));
	}

	/**
	 * Calculates the direction resulting from the calculation of the vector from the current point to the next
	 * point in the path.
	 * 
	 * @param startingPoint
	 *          the current position of the unit. This is used as the start point og the movement vector.
	 * @return
	 */
	public int calculateDirection(final DMSPoint startingPoint) {
		// - If path is empty default to direction 0�.
		if (points.isEmpty()) return 0;
		final DMSPoint nextPoint = getNextPoint();
		// - Check that there are enough points.
		if (null == nextPoint) return 0;

		// - Translate coordinates to point P1
		final DMSPoint vector = nextPoint.substract(startingPoint);

		// - Calculate direction depending on the vector quadrant
		final long lat = vector.getLatitude().toSeconds();
		final long lon = vector.getLongitude().toSeconds();
		final int quadrant = calculateVectorQuadrant(lat, lon);
		final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
		switch (quadrant) {
			case 0:
			case 1:
				// - Check if > 45�
				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
					final double alpha = StrictMath.asin(lon / h);
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				} else {
					// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
					final double alpha = StrictMath.acos(lat / h);
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				}
			case 2:
				// - Check if < 135�
				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
					final double alpha = StrictMath.acos(lon / h);
					return new Double(StrictMath.toDegrees(alpha) + 90.0).intValue();
				} else {
					final double alpha = StrictMath.asin(lat / h);
					return new Double(StrictMath.toDegrees(alpha) + 90.0).intValue();
				}
			case 3:
				// - Check if < 225�
				if (StrictMath.abs(lat) > StrictMath.abs(lon)) {
					final double alpha = StrictMath.asin(lon / h) + 180.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				} else {
					final double alpha = StrictMath.acos(lat / h) + 180.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				}
			case 4:
				// - Check if < 315
				if (StrictMath.abs(lon) > StrictMath.abs(lat)) {
					final double alpha = StrictMath.acos(lon / h) + 270.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				} else {
					final double alpha = StrictMath.asin(lat / h) + 270.0;
					return new Double(StrictMath.toDegrees(alpha)).intValue();
				}
		}
		return 0;
	}

	// - P R I V A T E - S E C T I O N
	/**
	 * Returns the first movement destination point. The first point was the unit location in the moment the
	 * first point was added. That first point in the point returned by this method, or any other point edited
	 * since then.
	 * 
	 * @return the first movement destination point in the path.
	 */
	protected DMSPoint getNextPoint() {
		return points.get(1);
	}

	private int calculateVectorQuadrant(final long vectorLat, final long vectorLon) {
		// - Calculate quadrant depending on the point signs.
		int quadrant = 0;
		if ((vectorLat >= 0) && (vectorLon >= 0))
			quadrant = 1;
		else if ((vectorLat >= 0) && (vectorLon < 0))
			quadrant = 4;
		else if ((vectorLat < 0) && (vectorLon >= 0))
			quadrant = 2;
		else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
		return quadrant;
	}

	public void draw(final Graphics graphics, final MovableFigure figure) {
		final RootMapPart rootPart = figure.getPart().getRootPart();
		final Iterator<DMSPoint> it = points.iterator();
		Point from = figure.getLocation();
		while (it.hasNext()) {
			final Point to = RootMapPart.coordinateToMap(rootPart, it.next());
			final Rectangle bound = new Rectangle(to.x - 3, to.y - 3, 7, 7);
			// - Draw the path handle
			graphics.setForegroundColor(ColorConstants.gray);
			graphics.drawOval(bound);
			graphics.drawLine(from, to);
			from = to;
		}
	}

	public Iterator<DMSPoint> getPointsIterator() {
		return points.iterator();
	}

	protected LinkedList<DMSPoint> getPoints() {
		return points;
	}

	// public void paint() {
	// // TODO Auto-generated method stub
	//
	// }

}

// - CLASS IMPLEMENTATION ...................................................................................
class NNLinkedList extends LinkedList<DMSPoint> {
	private static final long	serialVersionUID	= -1433126363814347456L;

}

// - UNUSED CODE ............................................................................................
