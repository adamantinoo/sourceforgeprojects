//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/RootMapPart.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:29 $
//  RELEASE:        $Revision: 1.12 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.11  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.10  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.9  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.8  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.7  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.6  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.5  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalEditPart;

import net.sourceforge.harpoon.figures.RootMapFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.PropertyModel;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
/**
 * This is the controller of the root model element implemented as the Map. With the inheritance chain we only
 * have to implement the key methods <code>createFigure()</code> and <code>getModelChildren()</code> if
 * this part is a container.
 */
public class RootMapPart extends GamePart implements PropertyChangeListener {
	// - S T A T I C - S E C T I O N
	public static Point convertToMap(final RootMapPart rootPart, final Unit model) {
		// - Get the unit coordinates and the top-left coordinates.
		final DMSCoordinate unitLat = model.getDMSLatitude();
		final DMSCoordinate unitLon = model.getDMSLongitude();

		// - Get the model root where there are stored the conversion properties.
		// final RootMapPart rootPart = (RootMapPart) getParent();
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint point = new DMSPoint(topLat, topLon);
		final DMSPoint vector = point.offsetPoint(new DMSPoint(unitLat, unitLon));

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	public static Point coordinateToMap(final RootMapPart rootPart, final DMSPoint to) {
		// // - Get the unit coordinates and the top-left coordinates.
		// final DMSCoordinate unitLat = to.getLatitude();
		// final DMSCoordinate unitLon = to.getLongitude();
		//
		// - Get the model root where there are stored the conversion properties.
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint from = new DMSPoint(topLat, topLon);
		final DMSPoint vector = from.offsetPoint(to);

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		return new Point(xx, yy);
	}

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Unit.CHILDREN.equals(prop)) {
			// - There is a change on the children structure. Detect if we are adding or removing am element.
			if (evt.getOldValue() instanceof PropertyModel) {
				removeChild((EditPart) getViewer().getEditPartRegistry().get(evt.getOldValue()));
			} else {
				// this.createChild(evt.getNewValue());
				addChild(createChild(evt.getNewValue()), children.size());
			}
		}

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Currently I have to get access to the Map that is the already created figure because it contains some of
	 * the model data mixed with the presentation data.<br>
	 * This figure is costly to be created many times. Now just create it every time we have the part requested
	 * that now is just a single time.<br>
	 * <br>
	 * The creation of the Figure can be generalized for most EditParts. Most of the parts get the model
	 * information to set the initial visual Figure properties (that most of the time are the <code>name</code>
	 * and the <code>side</code>). The Figure class can be intantiated by indirectly so all this standard
	 * code can be refactored to the base <code>UnitPart</code> class.<br>
	 * <br>
	 * Another complicated thing to be reviewed if the way to communicate the scenery properties. This version
	 * gets the properties from the PartFactory that received the editor as a parameter and it is accessible
	 * statically. This has been changed to get the properties from the Model piece that is more easily
	 * configured.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	public IFigure createFigure() {
		// - Create the presentation figure and initialize it with the scenery properties.
		final RootMapUnit model = (RootMapUnit) getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure MapFigure");
		final RootMapFigure fig = new RootMapFigure();
		// - For the root there is no need to set the side. The name id the scenery name and it it got form the
		// properties.
		fig.setConfiguration(model.getMapProperties());
		fig.setRootMap(this);
		return fig;
	}

	/**
	 * Create this figure before any initialization to give all other units a parent root unit that is valid and
	 * does not generate Exceptions.
	 */
	public void createEmptyFigure() {
		setFigure(new RootMapFigure());
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Return the model children that have to be assigned EditParts. Not all children elements generate
	 * EditPart. Some model hierarchy elements are not generated as sub-parts of the equivalent model part but
	 * as children of the main root part. This is performed scanning the hierarchy element by element.
	 * <code>paintFigure</code> of the unit.
	 */
	@Override
	protected List<Object> getModelChildren() {
		final List<Object> allChildren = new Vector<Object>();
		// FIXME Change generation to include submodel elements.
		final Unit model = (Unit) getModel();
		// - This is the list of firtst level objects. Iterato to crewte the complete list.
		final List<Unit> li = model.getChildren();
		final Iterator<Unit> it = li.iterator();
		while (it.hasNext()) {
			final Unit unit = it.next();
			allChildren.add(unit);
			if (unit instanceof MovableUnit) {
				final MovableUnit movable = (MovableUnit) unit;
				allChildren.add(movable.getMovementPath());
			}
		}
		return allChildren;
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[RootMapPart:");
		// TODO Create a selection decode method to print string values
		buffer.append(getSelected()).append("-");
		buffer.append(getUnit().getClass().getSimpleName()).append("-");
		buffer.append(getUnitFigure().getClass().getSimpleName()).append("]");
		// buffer.append(super.toString()).append("]");
		return buffer.toString();
	}
	// [01]

}

// - UNUSED CODE ............................................................................................
// @Override
// protected void addChildVisual(EditPart childEditPart, int index) {
// super.addChildVisual(childEditPart, index);
// }
//
// @Override
// protected void removeChildVisual(EditPart childEditPart) {
// super.removeChildVisual(childEditPart);
// }

// @Override
// public DragTracker getDragTracker(Request arg0) {
// return null;
// }
// public Object getAdapter(Class key) {
// //if (AccessibleEditPart.class == key)
// return getAccessibleEditPart();
// //return Platform.getAdapterManager().getAdapter(this, key);
// }
// getModelChildren()
// @Override
// protected void addChildVisual(EditPart part, int index) {
// }
// @Override
// protected void removeChildVisual(EditPart arg0) {
// }
