//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonTestSuite.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonTestSuite.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:16:29 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-13 13:30:24  ldiego
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import junit.framework.Test;
import junit.framework.TestSuite;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Harpoon Simulation Test Suite. Draft 00.02");
		// $JUnit-BEGIN$
		suite.addTestSuite(SceneryConstructorTest.class);
		// suite.addTestSuite(DisplacementTest.class);
		// suite.addTestSuite(PolarPointTest.class);
		// suite.addTestSuite(HarpoonAppTests.class);
		// suite.addTestSuite(HarpoonMapTests.class);
		// $JUnit-END$
		return suite;
	}

}

// - UNUSED CODE ............................................................................................
