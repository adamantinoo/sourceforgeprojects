//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: LocationSizeTest.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/LocationSizeTest.java,v $
//  LAST UPDATE:    $Date: 2007-10-02 09:04:25 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-10-01 14:44:40  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.2  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.1  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.3  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.2  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.1  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import junit.framework.TestCase;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class LocationSizeTest extends TestCase {
	public void testHotspotLocation() throws Exception {
		final FileOutputStream fos = new FileOutputStream(
				"U:/ldiego/Workstage/3.3_BaseWorkspace/HarpoonRCP/sceneries/hotspotLocation.harpoon");
		final ObjectOutputStream oos = new ObjectOutputStream(fos);

		final RootMapUnit model = new RootMapUnit();
		hotspotLocation(model);
		oos.writeObject(model);

		oos.close();
	}

	/**
	 * Create a testing units.<br>
	 * Latitudes are made south because the model has a location of 0-0 at the top-left. Below that location (y
	 * coordinates positive) we have units in the south hemisphere.
	 */
	private void hotspotLocation(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();
		MovementPath path = new MovementPath();

		// // - Create Friend units
		// AirportUnit base = new AirportUnit();
		// base.setName("Interoceanic airport");
		// base.setSide(Unit.FRIEND);
		// base.setLatitude(new DMSCoordinate(0, 55, 0, 'S'));
		// base.setLongitude(new DMSCoordinate(0, 30, 0, 'E'));
		// model.addChild(base);

		final WarUnit air = new WarUnit();
		// - Fields for a Unit
		air.setName("F16 Beta");
		air.setLatitude(new DMSCoordinate(0, 26, 0, 'S'));
		air.setLongitude(new DMSCoordinate(0, 26, 0, 'E'));
		air.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		air.setUnitType(HarpoonConstants.UNIT_AIR);
		air.setModel("F16 Figther");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 10, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E')); // surface.setDirection(90);
		air.setSpeed(500);
		air.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(air);

		TestUnit surface = new TestUnit();
		// - Fields for a Unit
		surface.setName("Test unit 1 - Surface");
		surface.setLatitude(new DMSCoordinate(0, 4, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 4, 0, 'E'));
		surface.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		surface.setUnitType(HarpoonConstants.UNIT_SURFACE);
		surface.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(surface);
		surface = new TestUnit();
		// - Fields for a Unit
		surface.setName("Test unit 2 - Air");
		surface.setLatitude(new DMSCoordinate(0, 6, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 4, 0, 'E'));
		surface.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		surface.setUnitType(HarpoonConstants.UNIT_AIR);
		surface.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(surface);
		surface = new TestUnit();
		// - Fields for a Unit
		surface.setName("Test unit 3 - Submarine");
		surface.setLatitude(new DMSCoordinate(0, 8, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 4, 0, 'E'));
		surface.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		surface.setUnitType(HarpoonConstants.UNIT_SUBMARINE);
		surface.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(surface);
		surface = new TestUnit();
		// - Fields for a Unit
		surface.setName("Test unit 4 - Semisurface");
		surface.setLatitude(new DMSCoordinate(0, 10, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 4, 0, 'E'));
		surface.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		surface.setUnitType("semi");
		surface.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(surface);

		RightUnit right = new RightUnit();
		// - Fields for a Unit
		right.setName("Right unit 1 - Surface");
		right.setLatitude(new DMSCoordinate(0, 8, 0, 'S'));
		right.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		right.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		right.setUnitType(HarpoonConstants.UNIT_SURFACE);
		right.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		right.setSpeed(21);
		right.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(right);
		right = new RightUnit();
		// - Fields for a Unit
		right.setName("Right unit 2 - Air");
		right.setLatitude(new DMSCoordinate(0, 5, 0, 'S'));
		right.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		right.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		right.setUnitType(HarpoonConstants.UNIT_AIR);
		right.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		right.setSpeed(21);
		right.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(right);

		right = new RightUnit();
		// - Fields for a Unit
		right.setName("Right unit 3 - Submnarine");
		right.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		right.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		right.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		right.setUnitType(HarpoonConstants.UNIT_SUBMARINE);
		right.setModel("Frigate Surface vessel");
		path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 0, 0, 'S'), new DMSCoordinate(0, 0, 0, 'E')); // surface.setDirection(45);
		right.setSpeed(21);
		right.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(right);
	}
	//
	// private void sensorTest() {
	// // DEBUG Add the sensors as a new unit to see it it is drawn on the map
	// // SensorsModel sensor = new SensorsModel();
	// // sensor.setRadarState(true);
	// // model.addChild(sensor);
	// }
	//
	// private void locationTest(RootMapUnit model) {
	// // - Clean the current model first.
	// model.clear();
	//
	// final ReferencePoint reference = new ReferencePoint();
	// reference.setName("R01");
	// reference.setLatitude(new DMSCoordinate(0, 1, 0, 'S'));
	// reference.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
	// model.addChild(reference);
	//
	// WarUnit ref2 = new WarUnit();
	// // - Fields for a Unit
	// ref2.setName("F16 Beta");
	// ref2.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
	// ref2.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
	// ref2.setSide(Unit.FRIEND);
	// // - Fields for a MovableUnit
	// ref2.setUnitType(HarpoonConstants.AIR_UNIT);
	// ref2.setModel("F16 Figther");
	// MovementPath path2 = new MovementPath();
	// path2.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(90);
	// ref2.setSpeed(500);
	// ref2.setMovementPath(path2);
	// // - Data for a WarUnit.
	// // SensorsModel sens = new SensorsModel();
	// // model.addChild(sens);
	// model.addChild(ref2);
	// ref2 = new WarUnit();
	// // - Fields for a Unit
	// ref2.setName("F16 Beta");
	// ref2.setLatitude(new DMSCoordinate(0, 1, 0, 'S'));
	// ref2.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
	// ref2.setSide(Unit.FRIEND);
	// // - Fields for a MovableUnit
	// ref2.setUnitType(HarpoonConstants.SURFACE_UNIT);
	// ref2.setModel("F16 Figther");
	// path2 = new MovementPath();
	// path2.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(90);
	// ref2.setSpeed(500);
	// ref2.setMovementPath(path2);
	// // - Data for a WarUnit.
	// // SensorsModel sens = new SensorsModel();
	// // model.addChild(sens);
	// model.addChild(ref2);
	//
	// }
}

// - UNUSED CODE ............................................................................................
