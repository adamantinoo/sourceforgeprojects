//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: TrigonometricTestCase.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/TrigonometricTestCase.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:35:04 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import junit.framework.TestCase;

// - CLASS IMPLEMENTATION ...................................................................................
public class TrigonometricTestCase extends TestCase {
	private static Logger	logger	= Logger.getLogger("net.sourceforge.rcp.harpoon.test");
	static {
		LogManager manager = LogManager.getLogManager();
		Enumeration<String> names = manager.getLoggerNames();
		logger.setLevel(Level.ALL);
	}

	//	private int	alpha=0;
	//	private double	expectedSin=0.0;
	//	private double	expectedCos=0.0;
	public void testTrigonometricAngles() throws Exception {
		//assertEquals("Checking and latitude for angle " + alpha1, expectedLat1, vector.getEndPointLat().toDegrees(),
		//0.000000001);
		calculatevalues(0);
		calculatevalues(15);
		calculatevalues(30);
		calculatevalues(45);
		calculatevalues(90);
		calculatevalues(135);
		calculatevalues(180);
		calculatevalues(180 + 45);
		calculatevalues(180 + 90);
		calculatevalues(270 + 45);
		calculatevalues(300);
		calculatevalues(360);
		calculatevalues(359);
		calculatevalues(361);
	}

	protected void calculatevalues(int alpha) {
		double expectedSin = StrictMath.sin(new Double(StrictMath.toRadians(alpha)).doubleValue());
		double expectedCos = StrictMath.cos(new Double(StrictMath.toRadians(alpha)).doubleValue());
		logger.info("Checking SIN for angle " + alpha + " = " + expectedSin);
		logger.info("Checking COS for angle " + alpha + " = " + expectedCos);
	}
}

// - UNUSED CODE ............................................................................................
