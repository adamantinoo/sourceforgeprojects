//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: DetectionModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/DetectionModel.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:37 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.4  2007-11-12 11:23:49  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.3  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.2  2007-11-02 09:34:49  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Logger;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public class DetectionModel implements Serializable {
	private static final long			serialVersionUID			= 4135940623738210352L;
	private static Logger					logger								= Logger.getLogger("net.sourceforge");

	// - D E T E C T I O N - E V E N T S
	public static final String		DETECTION_EVENT				= "DetectionModel.DETECTION_EVENT";
	public static final String		MOVED_EVENT						= "DetectionModel.MOVED_EVENT";
	public static final String		TIMEELAPSED_EVENT			= "DetectionModel.TIMEELAPSED_EVENT";

	protected static final int		DETECTION_TIME				= 60;
	protected static final int		UNDETECTEDLAPSE_TIME	= 120;

	// - M O D E L F I E L D S
	protected Unit								target;
	protected String							state									= HarpoonConstants.NOT_DETECTED_STATE;
	protected transient Calendar	lastStateChangeTime		= Calendar.getInstance();
	protected transient Calendar	lastDetectionTime			= Calendar.getInstance();

	//[01]
	// - S T A T I C - S E C T I O N
	public static String convertFromDetectionState(String side, String detectionState) {
		//- For simple Units the state just affects the unit visibility status.
		if (Unit.FRIEND_SIDE.equals(side)) {
			return Unit.FULL_VISIBLE;
		} else {
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(detectionState))
				return Unit.NOT_VISIBLE;
			if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(detectionState))
				return Unit.NOT_VISIBLE;
			if (HarpoonConstants.CONTACT_STATE.equals(detectionState))
				return Unit.CONTACT_VISIBLE;
			if (HarpoonConstants.CONTACT_MOVED_STATE.equals(detectionState))
				return Unit.CONTACT_VISIBLE;
			if (HarpoonConstants.LOST_CONTACT_STATE.equals(detectionState))
				return Unit.CONTACT_VISIBLE;
			if (HarpoonConstants.IDENTIFIED_STATE.equals(detectionState))
				return Unit.IDENTIFICATION_VISIBLE;
		}
		return Unit.NOT_VISIBLE;
	}

	// - C O N S T R U C T O R S
	//	public DetectionModel(final String state) {
	//		this.state = state;
	//		stateCode = decode(state);
	//	}

	public DetectionModel(Unit detectedUnit) {
		this.target = detectedUnit;
	}

	// - G E T T E R S / S E T T E R S
	public Calendar getLastStamp() {
		if (null == lastStateChangeTime)
			lastStateChangeTime = Calendar.getInstance();
		return lastStateChangeTime;
	}

	public Calendar getLastDetectionStamp() {
		if (null == lastDetectionTime)
			lastDetectionTime = Calendar.getInstance();
		return lastDetectionTime;
	}

	public String getState() {
		return state;
	}

	//[02]

	// - P U B L I C - S E C T I O N
	/**
	 * Implements the state machine that operates for the detection mechanism. The external events are received
	 * at this method and then the instance applies the timing parameters to calculate id the is an state
	 * transition and which is the resulting state. <br>
	 * The detection state transition depends on the current state and some time constraints. This is described
	 * inside the State Diagram <b>DetectionStates Diagram</b>.
	 * <ul>
	 * <li>If the unit is in the <code>NOT_DETECTED_STATE</code> and receives any
	 * <code>DETECTION_EVENT</code> then it moves automatically to the <code>INITIATED_DETECTION_STATE</code>.</li>
	 * <li>The Unit stays in the <code>INITIATED_DETECTION_STATE</code> until another
	 * <code>DETECTION_EVENT</code> arrives and the time elapsed since the last state change is greater that
	 * the time limit specified. Then it moves to the <code>DETECTION_EVENT</code> state.</li>
	 * <li>It fails back to the <code>NOT_DETECTED_STATE</code> if after 2 minutes there is not another
	 * <code>DETECTION_EVENT</code> that enables the next state.</li>
	 * <li>The Unit remains in the <code>CONTACT_STATE</code> until the unit moves, and this is indicated by
	 * a <code>MOVED_EVENT</code>. If the unit is in this state and moves then the state upgrades to the next
	 * state called <code>CONTACT_MOVED_STATE</code> and from this state it can move to the final
	 * identification state.</li>
	 * <li>If enough time elapses while the unit is in the <code>CONTACT_STATE</code> then the state fails
	 * back to <code>LOST_CONTACT_STATE</code>.</li>
	 * <li>The units fall back to <code>NOT_DETECTED_STATE</code> after a long period of time in the
	 * <code>LOST_CONTACT_STATE</code> and no other detections. If the unit is detected on the
	 * <code>LOST_CONTACT_STATE</code> then it moves again to the <code>CONTACT_STATE</code> and wait to
	 * move before advancing on the ladder.
	 * </ul>
	 */
	private String detectionEvent(final String event) {
		String currentState = getState();
		if (DETECTION_EVENT.equals(event)) {
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(currentState))
				changeState(HarpoonConstants.INITIATED_DETECTION_STATE);
			if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(currentState)) {
				//- Check the time we have spent in this state.
				final long elapsed = getElapsed();
				if (elapsed > DETECTION_TIME)
					changeState(HarpoonConstants.CONTACT_MOVED_STATE);
			}
			if (HarpoonConstants.CONTACT_MOVED_STATE.equals(currentState)) {
				//- Check the time we have spent in this state.
				final long elapsed = getElapsed();
				if (elapsed > DETECTION_TIME)
					changeState(HarpoonConstants.IDENTIFIED_STATE);
			}
			if (HarpoonConstants.LOST_CONTACT_STATE.equals(currentState))
				changeState(HarpoonConstants.CONTACT_STATE);
			//			if (HarpoonConstants.IDENTIFIED_STATE.equals(currentState)) resetTimer();
			resetDetection();
		}
		if (MOVED_EVENT.equals(event))
			if (HarpoonConstants.CONTACT_STATE.equals(currentState))
				changeState(HarpoonConstants.CONTACT_MOVED_STATE);
		if (TIMEELAPSED_EVENT.equals(event)) {
			if (getElapsedDetection() > UNDETECTEDLAPSE_TIME) {
				if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(currentState))
					changeState(HarpoonConstants.NOT_DETECTED_STATE);
				if (HarpoonConstants.CONTACT_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.CONTACT_MOVED_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.IDENTIFIED_STATE.equals(currentState))
					changeState(HarpoonConstants.LOST_CONTACT_STATE);
				if (HarpoonConstants.LOST_CONTACT_STATE.equals(currentState))
					changeState(HarpoonConstants.NOT_DETECTED_STATE);
			}
		}
		return getState();
	}

	/**
	 * Fires detection events to the <code>DetectionModel</code> detection manager. This will change the
	 * visibility state for the target unit that is associated to this detection block instance.
	 * 
	 * @param event
	 *          the detection event to be processed.
	 */
	public String fireDetection(final String event) {
		//- Send the detection event and check if the state has changed.
		final String currentState = this.getState();
		final String state = this.detectionEvent(event);
		if (!state.equals(currentState)) {
			target.firePropertyChange(Unit.DETECTIONCHANGE_PROP, currentState, state);
		}
		return state;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		return "[DetectionModel:" + target.getDebugName() + "-" + state + "]";
	}

	// - P R O T E C T E D - S E C T I O N
	private void changeState(final String newState) {
		logger.info("Changing state from " + getState() + " to " + newState);
		state = newState;
		resetTimer();
		resetDetection();
	}

	//[03]
	/** Return the number of seconds this instance has been in the same state. */
	private long getElapsed() {
		return (Calendar.getInstance().getTimeInMillis() - getLastStamp().getTimeInMillis()) / 1000;
	}

	/** Returns the number of seconds since the last <code>DETECTION_EVENT</code> event. */
	private long getElapsedDetection() {
		return (Calendar.getInstance().getTimeInMillis() - getLastDetectionStamp().getTimeInMillis()) / 1000;
	}

	private void resetTimer() {
		lastStateChangeTime = Calendar.getInstance();
	}

	private void resetDetection() {
		this.lastDetectionTime = Calendar.getInstance();
	}
}
// - UNUSED CODE ............................................................................................
//[01]
// - S T A T I C - S E C T I O N
//	public static int decode(final String state) {
//		if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) return 0;
//		if (HarpoonConstants.INITIATED_DETECTION_STATE.equals(state)) return 1;
//		if (HarpoonConstants.CONTACT_STATE.equals(state)) return 2;
//		if (HarpoonConstants.CONTACT_MOVED_STATE.equals(state)) return 3;
//		if (HarpoonConstants.LOST_CONTACT_STATE.equals(state)) return 4;
//		if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) return 5;
//		return 0;
//	}
//
//	public static String encode(final int code) {
//		if (0 == code) return HarpoonConstants.NOT_DETECTED_STATE;
//		if (1 == code) return HarpoonConstants.CONTACT_STATE;
//		if (2 == code) return HarpoonConstants.IDENTIFICATION_PHASE_STATE;
//		if (3 == code) return HarpoonConstants.IDENTIFIED_STATE;
//		if (4 == code) return HarpoonConstants.DETECTED_STATE;
//		return HarpoonConstants.NOT_DETECTED_STATE;
//	}

//[02]
//	public int getDetectStateCode() {
//		return stateCode;
//	}

//[03]
//private void detectionStateChange(String startState, String endState) {
////- Notify the EditPart of the change.
////- Change model visibility state and the dependent children depending on detection state.
////	if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
////String newState = ;
//this.setVisibilityState(convertFromDetectionState(endState));
////	}
////	if (HarpoonConstants.CONTACT_STATE.equals(state)) {
////		model.setVisibility(true);
////	}
////	if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) {
////		model.setVisibility(true);
////	}
//
//firePropertyChange(Unit.DETECTIONCHANGE_PROP, startState, endState);
//}
