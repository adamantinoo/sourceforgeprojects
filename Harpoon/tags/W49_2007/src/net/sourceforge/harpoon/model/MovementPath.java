//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovementPath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/MovementPath.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-11-30 12:11:36  ldiego
//    - MOVEMENTPATH.R01.C - Movement path changes.
//    - MOVEMENTPATH.R01.E - Movement path changes.
//    - MOVEMENTPATH.R01.D - Movement path changes.
//    - TASK All plugins unified into a single one plugin because the need
//      to access some classes between source file sets.
//    - TASK reorganization of classes into a new set of packages
//      reducing the name length.
//
//    Revision 1.4  2007-11-23 11:33:37  ldiego
//    - TASK Detection method is changed to a detection control unit by unit.
//    - DEFECT After saving a game the units return to the original position.
//    - DEFECT Changes in the movement path points is not reflected.
//    - DEFECT Enemies have not to show path.
//    - DEFECT Friend unit traces are not shown.
//
//    Revision 1.3  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.2.2.6  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.2.2.5  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.2.2.4  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.2.2.3  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.2.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.2.2.1  2007-10-11 07:52:12  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.2  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.eclipse.gef.EditPart;

import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.ReferencePoint;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * The MovementPath is a model element that stores the list of points that compose a unit path. A path is a series of
 * lines with associated commands that form the movement commands assigned to a unit.<br>
 * The unit direction is then derived from this list of commands. The termination of the list creates a new virtual
 * command to continue to the same direction indefinitely or in case there is a command associated to the point, doing
 * the same command action until changed.
 */
public class MovementPath extends PropertyModel implements Serializable {
	private static final long					serialVersionUID		= 2924232465880867103L;

	public static final String				MOVEMENTPATH_CHANGE	= "MovementPath.MOVEMENTPATH_CHANGE";

	/** List of points that compose the movement path for a unit. */
	// private final LinkedList<DMSPoint> points = new LinkedList<DMSPoint>();
	/** The list of reference model points that conform the movement path. */
	protected Vector<ReferencePoint>	referencePoints			= new Vector<ReferencePoint>();
	/** Wires that connect the path points. */
	protected Vector<Wire>						wires								= new Vector<Wire>();
	/**
	 * Target Unit that applies to the path commands. Required to have a backlink to the unit to know the first reference
	 * point in the command path.
	 */
	protected final MovableUnit				target;

	private int												selected						= EditPart.SELECTED_NONE;

	// - C O N S T R U C T O R S
	public MovementPath(final MovableUnit target) {
		this.target = target;
	}

	// - G E T T E R S / S E T T E R S
	public Vector<ReferencePoint> getPoints() {
		if (null == referencePoints) referencePoints = new Vector<ReferencePoint>();
		return referencePoints;
	}

	public MovableUnit getTarget() {
		return target;
	}

	public Vector<Wire> getWires() {
		return wires;
	}

	public void setParentselected(final int selected) {
		this.selected = selected;
		final Iterator<ReferencePoint> pointIt = referencePoints.iterator();
		while (pointIt.hasNext()) {
			pointIt.next().setParentselected(selected);
		}
		final Iterator<Wire> wireIt = wires.iterator();
		while (wireIt.hasNext()) {
			wireIt.next().setParentselected(selected);
		}
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Adds a new point to the command path. The point is added to the end of the chain and connected to the previous
	 * point by a new <code>Wire</code>.
	 */
	public int addPoint(final DMSPoint point) {
		// - Create the new point. If the path is empty we have finished the work.
		final ReferencePoint refPoint = convert2reference(point);

		Wire conn;
		// - If the first point, connect the unit with this point. Otherwise connect the last point.
		if (getPoints().isEmpty())
			conn = new Wire(target, refPoint);
		else
			conn = new Wire(getPoints().lastElement(), refPoint);
		getPoints().add(refPoint);
		getWires().add(conn);
		// TODO Activate the visibility for the new elements.
		if ((EditPart.SELECTED == selected) || (EditPart.SELECTED_PRIMARY == selected)) {
			refPoint.setParentselected(selected);
			conn.setParentselected(selected);
		}
		fireStructureChange(MovementPath.MOVEMENTPATH_CHANGE, null, getPoints());
		target.firePropertyChange(MovableUnit.MOVEMENTPATH_PROP, null, this);

		return getPoints().size() - 1;
	}

	public void addPoint(final DMSCoordinate latitude, final DMSCoordinate longitude) {
		addPoint(new DMSPoint(latitude, longitude));
	}

	/**
	 * @deprecated
	 */
	@Deprecated
	public void addPointAtIndex(final DMSPoint point, int index) {
		// - Create the new point. If the path is empty we have finished the work.
		final ReferencePoint refPoint = convert2reference(point);

		// - Get the wires connected to the previous point. It have to be changed.
		WireEndPoint leftPoint = null;
		if (0 == index)
			leftPoint = target;
		else
			leftPoint = getPoints().get(index - 1);
		final WireEndPoint rightPoint = getPoints().get(index);
		dropConnections(leftPoint.getSourceConnections());
		dropConnections(rightPoint.getSourceConnections());

		final Wire leftConn = new Wire(leftPoint, refPoint);
		getWires().add(leftConn);
		final Wire rightConn = new Wire(refPoint, rightPoint);
		getWires().add(rightConn);
		getPoints().add(index, refPoint);

		// - Adjust the properties to be the same as the other ReferencePoints.
		refPoint.setParentselected((getPoints().get(index)).getParentSelected());
		leftConn.setParentselected((getPoints().get(index)).getParentSelected());
		rightConn.setParentselected((getPoints().get(index)).getParentSelected());

		firePropertyChange(MovementPath.MOVEMENTPATH_CHANGE, null, getPoints());
	}

	public int addPointInWire(final DMSPoint point, Wire selectedWire) {
		// - Create the new point. If the path is empty we have finished the work.
		final ReferencePoint refPoint = convert2reference(point);
		// target.fireStructureChange(WarUnit.MOVEMENTPATH, getWires(), getPoints());

		// - Get the wires connected to the previous point. It have to be changed.
		final WireEndPoint leftPoint = selectedWire.getSource();
		final WireEndPoint rightPoint = selectedWire.getTarget();
		dropConnections(leftPoint.getSourceConnections());
		dropConnections(rightPoint.getSourceConnections());

		final int index = getPoints().indexOf(rightPoint);

		final Wire leftConn = new Wire(leftPoint, refPoint);
		getWires().add(leftConn);
		final Wire rightConn = new Wire(refPoint, rightPoint);
		getWires().add(rightConn);
		getPoints().add(index, refPoint);

		// - Adjust the properties to be the same as the other ReferencePoints.
		refPoint.setParentselected((leftPoint).getParentSelected());
		leftConn.setParentselected((leftPoint).getParentSelected());
		rightConn.setParentselected((leftPoint).getParentSelected());

		// - Signal the target to clear the direction to recalculate it
		target.fireStructureChange(MovementPath.MOVEMENTPATH_CHANGE, getWires(), getPoints());
		return index;
	}

	public void removePointAtIndex(int index) {
		// - Get the wires connected to the previous point. It have to be changed.
		WireEndPoint leftPoint = null;
		if (0 == index)
			leftPoint = target;
		else
			leftPoint = getPoints().get(index - 1);
		final WireEndPoint rightPoint = getPoints().get(index);
		dropConnections(leftPoint.getTargetConnections());
		dropConnections(rightPoint.getSourceConnections());

		final Wire conn = new Wire(leftPoint, rightPoint);
		getWires().add(conn);
		getPoints().remove(index);

		// - Signal the target to clear the direction to recalculate it
		target.fireStructureChange(MovementPath.MOVEMENTPATH_CHANGE, getWires(), getPoints());
	}

	private void dropConnections(List<Wire> targetConnections) {
		final Iterator<Wire> it = targetConnections.iterator();
		while (it.hasNext()) {
			final Wire con = it.next();
			getWires().remove(con);
		}
	}

	/**
	 * Calculates the direction resulting from the calculation of the vector from the current point to the next point in
	 * the path.
	 * 
	 * @param startingPoint
	 *          the current position of the unit. This is used as the start point og the movement vector.
	 * @return
	 */
	public int calculateBearing(final DMSPoint startingPoint) {
		// - If path is empty default to direction 0�.
		if (referencePoints.isEmpty()) return 0;
		final DMSPoint nextPoint = referencePoints.firstElement().getLocation();
		// - Check that there are enough points.
		// if (null == nextPoint) return 0;

		// - Translate coordinates to point P1
		final DMSPoint vector = nextPoint.offset(startingPoint);

		// - Calculate direction depending on the vector quadrant
		final long lat = vector.getDMSLatitude().toSeconds();
		final long lon = vector.getDMSLongitude().toSeconds();
		final double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
		double beta = 0.0;
		if (StrictMath.abs(lat) > StrictMath.abs(lon)) {
			beta = StrictMath.acos(lat / h);
		} else {
			beta = StrictMath.asin(lon / h);
		}
		int degrees = new Double(StrictMath.toDegrees(beta)).intValue();
		if (degrees < 0) degrees += 360;
		return degrees;
	}

	public Vector<Object> getControllableChilds() {
		final Vector<Object> children = new Vector<Object>();
		children.addAll(getPoints());
		// children.addAll(wires);
		return children;
	}

	// - P R I V A T E - S E C T I O N
	/** Creates a new model ReferencePoint form a DMS location. */
	protected ReferencePoint convert2reference(final DMSPoint point) {
		final ReferencePoint refPoint = new ReferencePoint(this);
		refPoint.setLatitude(point.getDMSLatitude());
		refPoint.setLongitude(point.getDMSLongitude());
		refPoint.setSide(Unit.PATH);
		return refPoint;
	}

	/**
	 * Returns the first movement destination point. The first point was the unit location in the moment the first point
	 * was added. That first point in the point returned by this method, or any other point edited since then.
	 * 
	 * @return the first movement destination point in the path.
	 */
	// protected DMSPoint getNextPoint() {
	// // - Get the second point that is the direction from the current location (the first point).
	// final ReferencePoint to = referencePoints.get(1);
	// return new DMSPoint(to.getDMSLatitude(), to.getDMSLongitude());
	// }
	// protected int calculateVectorQuadrant(final long vectorLat, final long vectorLon) {
	// // - Calculate quadrant depending on the point signs.
	// int quadrant = 0;
	// if ((vectorLat >= 0) && (vectorLon >= 0))
	// quadrant = 1;
	// else if ((vectorLat >= 0) && (vectorLon < 0))
	// quadrant = 4;
	// else if ((vectorLat < 0) && (vectorLon >= 0))
	// quadrant = 2;
	// else if ((vectorLat < 0) && (vectorLon < 0)) quadrant = 3;
	// return quadrant;
	// }
	// [02]
}

// - UNUSED CODE ............................................................................................
// [02]
// /**
// * Return the list of PointUnits that contain the movement path points as units that also will generate the
// * corresponding Parts.
// */
// public Vector<PropertyModel> getReferencePoints(final MovableUnit movable) {
// final Vector<PropertyModel> pp = new Vector<PropertyModel>();
// ReferencePoint from = null;
//
// final MovementPath mp = movable.getMovementPath();
// final LinkedList<DMSPoint> points = mp.getPoints();
// final Iterator<DMSPoint> it = points.iterator();
// while (it.hasNext()) {
// final DMSPoint point = it.next();
// final ReferencePoint to = new ReferencePoint();
// to.setLatitude(point.getLatitude());
// to.setLongitude(point.getLongitude());
// to.setSide(Unit.PATH);
// pp.add(to);
// if (null != from) {
// // - Connect two adjacent points
// final Wire conn = new Wire(from, to);
// // pp.add(conn);
// }
// from = to;
// }
// return pp;
// }
