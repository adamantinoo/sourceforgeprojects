//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/WarPart.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.14 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.13  2007-11-23 11:33:38  ldiego
//    - TASK Detection method is changed to a detection control unit by unit.
//    - DEFECT After saving a game the units return to the original position.
//    - DEFECT Changes in the movement path points is not reflected.
//    - DEFECT Enemies have not to show path.
//    - DEFECT Friend unit traces are not shown.
//
//    Revision 1.12  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.11  2007-11-12 11:23:49  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.10  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.9  2007-11-02 09:34:51  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.8.2.3  2007-10-31 14:47:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.8.2.2  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.8.2.1  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.8  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.7  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.6  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;
import org.eclipse.swt.widgets.Composite;

import net.sf.harpoon.model.units.WarUnit;

import net.sourceforge.harpoon.figures.MovableFigure;
import net.sourceforge.harpoon.pages.IPropertyPage;
import net.sourceforge.harpoon.pages.WarPropertyPage;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class WarPart extends MovablePart implements NodeEditPart {
	private ConnectionAnchor	anchor;

	// [03]
	// - P U B L I C - S E C T I O N
	/**
	 * Creates a set of SWT controls that representS the visible and editable properties for this Unit.
	 * 
	 * @param singleSelected
	 *          flag to indicate if the selection only contains a unit and this is under the interface control.
	 */
	public IPropertyPage createPropertyPage(final Composite top, boolean singleSelected) {
		// - Create a new set of controls for this EditPart.
		final WarPropertyPage page = new WarPropertyPage(top);
		page.setContainer(top);
		page.setModel((WarUnit) getModel());
		if (singleSelected) page.activatePathChange();
		page.build();
		return page;
	}

	// [04]
	// - O V E R R I D E - S E C T I O N
	@Override
	public WarUnit getCastedModel() {
		return (WarUnit) getModel();
	}

	/**
	 * Any change on the sensor properties (received at this level by sensor delegation) only has to fire the repainting
	 * of the Map to update the location and visibility of the sensor ranges. No control Part is created for sensors.
	 * 
	 * @see net.sourceforge.harpoon.parts.MovablePart#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (WarUnit.SENSORCHANGE_PROP.equals(prop)) {
			// - A change on the sensors requires to redraw the Map.
			getRootPart().getFigure().repaint();
		}
		// if (MovableUnit.VISIBILITY_PROP.equals(prop)) {
		// final MovableUnit model = getCastedModel();
		// final String state = model.getVisibilityState();
		// if (Unit.NOT_VISIBLE.equals(state))
		// getFigure().setVisible(false);
		// else
		// getFigure().setVisible(true);
		// }
		// TODO Detection changes code will update the SelectionView and not the display
		if (WarUnit.DETECTEDUNITS_PROP.equals(prop)) {
			// TODO Change the Property presentation to include the list of units on it.
		}
		// if(Unit.DETECTIONCHANGE_PROP.equals(prop)) {
		// //TODO Update the visibility state and the presentation.
		//					
		// }
		super.propertyChange(evt);
	}

	// [02]
	/**
	 * Update the visual representation of the Units (the Figure part) from the model data. In war units this controls the
	 * visibility for the units in the display.
	 * 
	 * @see net.sourceforge.harpoon.parts.MovablePart#refreshVisuals()
	 */
	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		// // - The references to the model and figure objects.
		// MovableFigure fig = (MovableFigure) getFigure();
		// WarUnit model = (WarUnit) getCastedModel();
		//
		// //- Be sure that FRIEND units are visible
		// if (Unit.FRIEND == model.getSide()) fig.setVisible(true);
		// else {
		// //- Enemy units visibility depend on the detection state. Like other units.
		// String state = model.getDetectState();
		// if (HarpoonConstants.CONTACT_STATE.equals(state)) {
		// fig.setVisible(true);
		// fig.setDataVisibility(false);
		// fig.setColorIntensity(HarpoonConstants.LOWLIGHT_INTENSITY);
		// }
		// if (HarpoonConstants.IDENTIFIED_STATE.equals(state)) {
		// fig.setVisible(true);
		// fig.setDataVisibility(true);
		// fig.setColorIntensity(HarpoonConstants.HIGHLIGHT_INTENSITY);
		// }
		// }
	}

	// - N O D E E D I T P A R T - S E C T I O N
	public ConnectionAnchor getSourceConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getSourceConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(final Request request) {
		return getConnectionAnchor();
	}

	protected ConnectionAnchor getConnectionAnchor() {
		if (anchor == null) anchor = new MovableAnchor(getFigure());
		return anchor;
	}
}

// - CLASS IMPLEMENTATION .................................................................................
class MovableAnchor extends ChopboxAnchor {
	private final MovableFigure	figure;

	public MovableAnchor(final IFigure hostFigure) {
		super(hostFigure);
		figure = (MovableFigure) hostFigure;
	}

	@Override
	protected Rectangle getBox() {
		return getFigure().getReferenceBox();
	}

	@Override
	public Point getReferencePoint() {
		final MovableFigure fig = getFigure();
		final Point loc = fig.getLocation();
		final Point hotspot = fig.getHotSpot();
		final Point finalPoint = new Point(loc.x + hotspot.x, loc.y + hotspot.y);
		return finalPoint;
	}

	private MovableFigure getFigure() {
		return figure;
	}
}
// - UNUSED CODE ............................................................................................
// [02]
//		
// // super.propertyChange(evt);
// String prop = evt.getPropertyName();
// if (WarUnit.RADAR.equals(prop))
// activateSensor(WarUnit.RADAR);
// else if (WarUnit.SONAR.equals(prop))
// activateSensor(WarUnit.SONAR);
// else if (WarUnit.ECM.equals(prop))
// activateSensor(WarUnit.ECM);
// else if (WarUnit.DETECTED.equals(prop))
// refreshVisuals();
//
// // - Causes Graph to re-layout
// ((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
// }
// public void activateSensor(String sensor) {
// if(sensor.equals(WarUnit.RADAR)) {
// //- Create a new sensor and add it to the controller through the RoorUnit
// SensorsModel newSensor = new SensorsModel(getUnit());
// newSensor.setRadarState(true);
// newSensor.setRange(10);
// // RootEditPart root = getRoot();
// EditPart r = getParent();
// // Object mod = root.getModel();
// RootMapPart root = (RootMapPart) r;
// RootMapUnit rootUnit = (RootMapUnit) root.getModel();
// //FIX-ME This is the key point.
// rootUnit.addChild(newSensor);
// }
// }

// [03]
// [04]
// public void activate() {
// if (isActive()) return;
// // - Start listening for changes in the model.
// getUnit().addPropertyChangeListener(this);
// super.activate();
// }
// public void deactivate() {
// if (!isActive()) return;
// // - Stop listening to events in the model.
// getUnit().removePropertyChangeListener(this);
// super.deactivate();
// }
