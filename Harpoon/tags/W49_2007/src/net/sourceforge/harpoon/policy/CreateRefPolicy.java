//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: CreateRefPolicy.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/policy/CreateRefPolicy.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:36 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.policy;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gef.requests.CreateRequest;

import net.sf.harpoon.model.units.ReferencePoint;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.parts.RootMapPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class CreateRefPolicy extends AbstractEditPolicy {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	// @Override
	// public void activate() {
	// // TODO Auto-generated method stub
	// super.activate();
	// }
	// @Override
	// public void deactivate() {
	// // TODO Auto-generated method stub
	// super.deactivate();
	// }
	@Override
	public void eraseSourceFeedback(Request request) {
		// TODO Auto-generated method stub
		super.eraseSourceFeedback(request);
	}

	/**
	 * Calls two more specific methods depending on the Request.
	 * 
	 * @see org.eclipse.gef.EditPolicy#eraseTargetFeedback(Request)
	 */
	@Override
	public void eraseTargetFeedback(Request request) {
		super.eraseSourceFeedback(request);
		// if (REQ_ADD.equals(request.getType())
		// || REQ_MOVE.equals(request.getType())
		// || REQ_RESIZE_CHILDREN.equals(request.getType())
		// || REQ_CREATE.equals(request.getType())
		// || REQ_CLONE.equals(request.getType()))
		// eraseLayoutTargetFeedback(request);
		//
		// if (REQ_CREATE.equals(request.getType()))
		// eraseSizeOnDropFeedback(request);
	}

	/**
	 * Factors incoming requests into various specific methods.
	 * 
	 * @see org.eclipse.gef.EditPolicy#getCommand(Request)
	 */
	@Override
	public Command getCommand(Request request) {
		// if (REQ_DELETE_DEPENDANT.equals(request.getType()))
		// return getDeleteDependantCommand(request);
		//
		// if (REQ_ADD.equals(request.getType()))
		// return getAddCommand(request);
		//
		// if (REQ_ORPHAN_CHILDREN.equals(request.getType()))
		// return getOrphanChildrenCommand(request);
		//
		// if (REQ_MOVE_CHILDREN.equals(request.getType()))
		// return getMoveChildrenCommand(request);
		//
		// if (REQ_CLONE.equals(request.getType()))
		// return getCloneCommand((ChangeBoundsRequest)request);
		//		
		if (REQ_CREATE.equals(request.getType())) return getCreateCommand((CreateRequest) request);

		return null;
	}

	// @Override
	// public EditPart getHost() {
	// // TODO Auto-generated method stub
	// return super.getHost();
	// }
	/**
	 * Returns the <i>host</i> if the Request is an ADD, MOVE, or CREATE.
	 * 
	 * @see org.eclipse.gef.EditPolicy#getTargetEditPart(Request)
	 */
	@Override
	public EditPart getTargetEditPart(Request request) {
		if (REQ_CREATE.equals(request.getType())) return getHost();
		return null;
	}

	// @Override
	// public void setHost(EditPart host) {
	// // TODO Auto-generated method stub
	// super.setHost(host);
	// }
	@Override
	public void showSourceFeedback(Request request) {
		// TODO Auto-generated method stub
		super.showSourceFeedback(request);
	}

	@Override
	public void showTargetFeedback(Request request) {
		// TODO Auto-generated method stub
		super.showTargetFeedback(request);
	}

	/**
	 * Returns <code>true</code> for move, align, add, and orphan request types. This method is never called for some of
	 * these types, but they are included for possible future use.
	 * 
	 * @see org.eclipse.gef.EditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(Request request) {
		if (REQ_CREATE.equals(request.getType())) return true;
		return super.understandsRequest(request);
	}

	// - P R O T E C T E D - S E C T I O N
	protected Command getCreateCommand(final CreateRequest request) {
		final Object childClass = request.getNewObjectType();
		if (childClass == ReferencePoint.class) {
			// TODO Locate the selection to know the Movement Path that is selected or the unit selected to know where to add
			// the point.
			final EditPart rootPart = getHost();
			if (rootPart instanceof RootMapPart) {
				// - Get the elements that we need to add a point to a MovementPath
				final RootMapPart root = (RootMapPart) rootPart;
				final MovementPath pathOwner = ((RootMapPart) rootPart).getPathOwner();

				// - Convert to DMS coordinates to create and assign them to the new ReferencePoint
				final Point xyPoint = request.getLocation();
				// - Change the sign of the latitude to adjust to our quadrant
				xyPoint.y = -xyPoint.y;
				final DMSPoint location = RootMapPart.xy2dms((RootMapUnit) root.getModel(), xyPoint);

				// - Create and initialize the point.
				final ReferencePointCreateCommand2 refCommand = new ReferencePointCreateCommand2();
				refCommand.setDMSLocation(location);
				refCommand.setOwner(pathOwner);
				return refCommand;
			}
		}
		return null;
	}
}

class ReferencePointCreateCommand2 extends Command {
	private int						index;
	private Wire					theWire;
	private DMSPoint			dmsLocation;
	private MovementPath	owner;

	public DMSPoint getDMSLocation() {
		return dmsLocation;
	}

	public void setDMSLocation(DMSPoint dmsLocation) {
		this.dmsLocation = dmsLocation;
	}

	public int getIndex() {
		return index;
	}

	public MovementPath getOwner() {
		return owner;
	}

	public void setOwner(MovementPath owner) {
		this.owner = owner;
	}

	public Wire getWire() {
		return theWire;
	}

	public void setWire(Wire theWire) {
		this.theWire = theWire;
	}

	@Override
	public void execute() {
		index = getOwner().addPoint(getDMSLocation());
		super.execute();
	}

	@Override
	public void undo() {
		super.undo();
		getOwner().removePointAtIndex(getIndex());
	}

}

// - UNUSED CODE ............................................................................................
