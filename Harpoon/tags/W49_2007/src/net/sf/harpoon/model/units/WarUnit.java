//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/WarUnit.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.11 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.10  2007-11-23 11:33:37  ldiego
//    - TASK Detection method is changed to a detection control unit by unit.
//    - DEFECT After saving a game the units return to the original position.
//    - DEFECT Changes in the movement path points is not reflected.
//    - DEFECT Enemies have not to show path.
//    - DEFECT Friend unit traces are not shown.
//
//    Revision 1.9  2007-11-12 11:23:48  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.8  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.7  2007-11-02 09:34:48  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.6.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.6  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sf.harpoon.model.units;

import java.util.HashMap;
import java.util.Iterator;

import net.sf.harpoon.app.HarpoonConstants;

import net.sourceforge.harpoon.model.DetectionModel;
import net.sourceforge.harpoon.model.SensorsModel;

//- IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
public class WarUnit extends MovableUnit implements ISensorUnit {
	private static final long								serialVersionUID		= -3514624844988656815L;
	public static final String							DETECTEDUNITS_PROP	= "WarUnit.DETECTEDUNITS_PROP";

	// - M O D E L F I E L D S
	protected SensorsModel									sensorInformation		= new SensorsModel(this);
	protected HashMap<Unit, DetectionModel>	detectedUnits				= new HashMap<Unit, DetectionModel>();

	// - G E T T E R S / S E T T E R S
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.harpoon.model.units.ISensorUnit#getSensorInformation()
	 */
	public SensorsModel getSensorInformation() {
		if (null == sensorInformation) sensorInformation = new SensorsModel(this);
		return sensorInformation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sf.harpoon.model.units.ISensorUnit#setSensorInformation(net.sourceforge.harpoon.model.SensorsModel)
	 */
	public void setSensorInformation(final SensorsModel sensor) {
		// - Check parameter validity. Null values have no effect.
		if (null == sensor) return;
		sensorInformation = sensor;
		firePropertyChange(SENSORCHANGE_PROP, null, sensor);
	}

	// /**
	// * Set the visibility of the unit and of the other objects that are related to it depending on the detection
	// * status and the type of unit. The visibility states are different form the detection states so the first
	// * task is to encode the correct visibility state from the conversion matrix.
	// */
	// public void setVisibilityState(String state) {
	// String oldState = this.visibilityState;
	// //- Convert to visibility states.
	// // String newState = convertFromDetectionState(state);
	// if (!state.equals(oldState)) {
	// this.visibilityState = state;
	// firePropertyChange(TracePath.VISIBILITY_PROP, oldState, state);
	// //- Change the state of the trace visibility.
	// this.tracePath.setVisibilityState(state);
	// }
	// super.setVisibilityState(state);
	// }

	// [03]

	// - P U B L I C - S E C T I O N
	// [02]
	// - I S E N S O R U N I T - I N T E R F A C E
	/**
	 * Adds the detected unit to the list of units that are on the detection range for this unit only if this unit is not
	 * already on this list. Then notifies the <code>Detection</code> helper associated to the detected unit of this new
	 * event to calculate the new detection level. This will then update the presentation information for this detected
	 * unit.
	 */
	public void detectedEvent(final Unit detectedUnit) {
		final DetectionModel detected = getDetectionBlock(detectedUnit);
		detected.fireDetection(DetectionModel.DETECTION_EVENT);
		firePropertyChange(DETECTEDUNITS_PROP, null, detectedUnits);
	}

	// [04]

	public void timeDetection() {
		final Iterator<DetectionModel> it = detectedUnits.values().iterator();
		while (it.hasNext()) {
			final DetectionModel detected = it.next();
			final String state = detected.fireDetection(DetectionModel.TIMEELAPSED_EVENT);
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
				detectedUnits.remove(detected);
				firePropertyChange(DETECTEDUNITS_PROP, null, null);
			}
		}
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public String toString() {
		// Iterator<Unit> it = this.detectedUnits.keySet().iterator();
		// while (it.hasNext()) {
		// this.notDetectedEvent(it.next());
		// }
		final StringBuffer buffer = new StringBuffer("[WarUnit:");
		buffer.append(sensorInformation.toString());
		buffer.append(super.toString());
		buffer.append("]");
		return buffer.toString();
	}

	// - P R O T E C T E D - S E C T I O N
	private DetectionModel getDetectionBlock(Unit detectedUnit) {
		DetectionModel detected = null;
		if (detectedUnits.containsKey(detectedUnit)) {
			detected = detectedUnits.get(detectedUnit);
		} else {
			// - Create a new detection helper and add this to the list of detected units.
			detected = new DetectionModel(detectedUnit);
			detectedUnits.put(detectedUnit, detected);
		}
		return detected;
	}

}

// - UNUSED CODE ............................................................................................
// [01]
// public boolean getRadarState() {
// return activeRadar;
// }
//
// public boolean getSonarState() {
// return activeSonar;
// }
//
// public boolean getECMState() {
// return activeECM;
// }

// public void activateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) {
// activeRadar = true;
// firePropertyChange(WarUnit.RADAR, false, true);
// }
// if (sensor.equals(WarUnit.SONAR)) {
// activeSonar = true;
// firePropertyChange(WarUnit.SONAR, false, true);
// }
// if (sensor.equals(WarUnit.ECM)) {
// activeECM = true;
// firePropertyChange(WarUnit.ECM, false, true);
// }
// // final int oldSide = this.side;
// // this.side = side;
// // - Fire a notification to all listeners to update the presentation layer.
//
// }

// public void deActivateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) activeRadar = false;
// if (sensor.equals(WarUnit.SONAR)) activeSonar = false;
// if (sensor.equals(WarUnit.ECM)) activeECM = false;
// }

// [02]
// public SensorsModel getSensors() {
// if (null == this.sensors) this.sensors = new SensorsModel(this);
// return this.sensors;
// }
// public boolean getRadarState() {
// return getSensors().getRadarState();
// }
//
// public void setRadarState(boolean newState) {
// final boolean oldState = getSensors().getRadarState();
// getSensors().setRadarState(newState);
// firePropertyChange(RADAR, oldState, newState);
// }
//
// public boolean getECMState() {
// return getSensors().getECMState();
// }
//
// public void setECMState(boolean newState) {
// final boolean oldState = getSensors().getECMState();
// getSensors().getECMState(newState);
// firePropertyChange(ECM, oldState, newState);
// }

// [03]
//
// public int getSonarRange() {
// // TODO Auto-generated method stub
// return 10;
// }
//
// public int getRadioRange() {
// // TODO Auto-generated method stub
// return 30;
// }

// public boolean getDetected() {
// return detected;
// }

// - G E T T E R S / S E T T E R S
// [01]

// [04]
// public void notDetectedEvent(final Unit detectedUnit) {
// final String state = detectedUnit.fireDetection(DetectionModel.TIMEELAPSED_EVENT);
// if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
// detectedUnits.remove(detectedUnit);
// firePropertyChange(DETECTEDUNITS_PROP, null, null);
// }
// }
