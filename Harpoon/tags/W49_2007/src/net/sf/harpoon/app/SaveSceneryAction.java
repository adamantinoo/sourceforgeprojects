//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SaveSceneryAction.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/SaveSceneryAction.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:35 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-11-02 09:35:04  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.1  2007-10-24 16:45:44  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//

package net.sf.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

import net.sf.harpoon.model.Scenery;

// - CLASS IMPLEMENTATION ...................................................................................
public class SaveSceneryAction extends Action {
	private static Logger	logger	= Logger.getLogger("net.sourceforge");

	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	public SaveSceneryAction(IWorkbenchWindow window, String label) {
		logger.log(Level.FINE, "Creating instance of Action. SaveSceneryAction");
		// this.window = window;
		setText(label);

		// - The id is used to refer to the action in a menu or toolbar.
		setId(HarpoonConstants.CMD_SAVE_SCENERY);
		// - Associate the action with a pre-defined command, to allow key bindings.
		setActionDefinitionId(HarpoonConstants.CMD_SAVE_SCENERY);
		setImageDescriptor(HarpoonRegistry.getImageDescriptor("/icons/sample3.gif"));
		// - Register this menu to be accessed later.
		HarpoonRegistry.getRegistry().put(HarpoonConstants.CMD_SAVE_SCENERY, this);
	}

	@Override
	public void run() {
		logger.log(Level.FINE, "Menu Action SaveSceneryAction fired");
		// TODO Locate the sceneries open and the .harpoon model file where to store them.
		final HashMap<Object, Object> registry = HarpoonRegistry.getRegistry();
		final Iterator<Object> instanceIt = registry.values().iterator();
		while (instanceIt.hasNext()) {
			final Object instance = instanceIt.next();
			if (instance instanceof Scenery) {
				// TODO Save the model back on the model file
				final Scenery scenery = (Scenery) instance;
				final String name = scenery.getModelFileName();
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(name);
					final ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(scenery.getModel());
					oos.close();
				} catch (final FileNotFoundException fnfe) {
					// TODO Auto-generated catch block
					fnfe.printStackTrace();
				} catch (final IOException ioe) {
					// TODO Auto-generated catch block
					ioe.printStackTrace();
				}
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
