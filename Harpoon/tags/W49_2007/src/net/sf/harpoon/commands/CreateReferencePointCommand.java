//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: CreateReferencePointCommand.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/commands/CreateReferencePointCommand.java,v $
//  LAST UPDATE:    $Date: 2007-12-10 13:56:32 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-11-30 12:11:36  ldiego
//    - MOVEMENTPATH.R01.C - Movement path changes.
//    - MOVEMENTPATH.R01.E - Movement path changes.
//    - MOVEMENTPATH.R01.D - Movement path changes.
//    - TASK All plugins unified into a single one plugin because the need
//      to access some classes between source file sets.
//    - TASK reorganization of classes into a new set of packages
//      reducing the name length.
//

package net.sf.harpoon.commands;

// - IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.policy.BendpointCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class CreateReferencePointCommand extends BendpointCommand {
	private DMSPoint			dmsLocation;
	private MovementPath	owner;

	// - G E T T E R S / S E T T E R S
	public DMSPoint getDMSLocation() {
		return dmsLocation;
	}

	public void setDMSLocation(DMSPoint dmsLocation) {
		this.dmsLocation = dmsLocation;
	}

	public MovementPath getOwner() {
		return owner;
	}

	public void setOwner(MovementPath owner) {
		this.owner = owner;
	}

	// - O V E R R I D E - S E C T I O N
	@Override
	public void execute() {
		// - Get access to the current MovementPath
		getOwner().addPointInWire(getDMSLocation(), getWire());
		super.execute();
	}

	@Override
	public void undo() {
		super.undo();
		getOwner().removePointAtIndex(getIndex());
	}
}
// - UNUSED CODE ............................................................................................
