//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: TestPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/TestPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:29 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-09-05 07:48:46  ldiego
//    - Registration of this class before comparison with working
//      previous version.
//

package net.sourceforge.harpoon.rcp.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.parts.UnitPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestPart extends UnitPart {
	public static Logger									log							= Logger.getLogger("net.sourceforge");

	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		Unit model = (Unit) this.getModel();
		log.log(Level.FINE, "Creating Figure TestFigure");
		TestFigure fig= new TestFigure();
		fig.setName(model.getName());
		fig.setSide(model.getSide());
		log.log(Level.INFO, fig.toString());
		return fig;
	}
}

class TestFigure extends UnitFigure {
	private static final int MARGIN = 2;
	private TestDrawFigure	iconic;
	private Label							nameLabel	= new Label("Test Figure");
	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a square
	 * box with some lines inside with a name label at the right center.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center of the icon.
	 */
	public TestFigure() {
		// - Create the complex internal parts of this figure.
		GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = MARGIN;
		grid.marginHeight = MARGIN;
		grid.marginWidth = MARGIN;
		grid.verticalSpacing = 0;
 		this.setLayoutManager(grid);
		iconic = new TestDrawFigure(this);
		this.add(iconic);
		nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		nameLabel.setBorder(new LineBorder(1));
		this.add(nameLabel);
		// DEBUG Set the border for debugging
		this.setBorder(new LineBorder(1));
		
		//- Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}
	// - P U B L I C S E C T I O N
	/**
	 * Sets the name of the label. The size of the parent figure has to be adjusted, but tet if the real soize
	 * of the label has also to be adjusted.
	 */
	public void setName(String name) {
		nameLabel.setText(name);
		Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		size = new Dimension((nameLabel.getText().length() + 1) * 6 + 2, FigureUtilities.getStringExtents(
				nameLabel.getText(), nameLabel.getFont()).height);
		// size.width+=8;
		nameLabel.setSize(size);
		nameLabel.setPreferredSize(size);
		// nameLabel.setBorder(new LineBorder(1));
		this.invalidate();
		TestPart.log.log(Level.FINE, "values for nameLabel after setting the new name");
		TestPart.log.log(Level.FINE, nameLabel.getText());
		TestPart.log.log(Level.FINE, nameLabel.getBounds().toString());
		// - Recalculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	public void setSide(int newSide) {
		if (Unit.FRIEND == newSide) this.setColor(HarpoonColorConstants.FRIEND);
		if (Unit.FOE == newSide) this.setColor(HarpoonColorConstants.FOE);
		if (Unit.NEUTRAL == newSide) this.setColor(HarpoonColorConstants.NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) this.setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	public Dimension getHotSpot() {
		Dimension hot = iconic.getHotSpot();
		return new Dimension(hot.width+4,hot.height+4);
	}
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		Dimension iconicSize = iconic.getSize();
		Dimension nameLabelSize2 = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		// DEBUG Compare calculated size with current label size - No size set because name not changed
		Dimension lab = nameLabel.getSize();
		Dimension nameLabelSize = new Dimension((nameLabel.getText().length() + 1) * 6 + 2, nameLabelSize2.height);

		TestPart.log.log(Level.FINE, "icon size:" + iconicSize);
		TestPart.log.log(Level.FINE, "calculated label size:" + nameLabelSize);
		TestPart.log.log(Level.FINE, "label size:" + lab);

		Dimension fullSize = new Dimension(0, 0);
		fullSize.width = MARGIN + iconicSize.width + MARGIN + nameLabelSize.width-1;
		fullSize.height = MARGIN + Math.max(iconicSize.height, nameLabelSize.height) + MARGIN + 1;
		TestPart.log.log(Level.FINE, "final size:" + fullSize);
		return fullSize;
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("TestFigure("+nameLabel.getText());
		buffer.append(",");
		buffer.append("preferredsize:"+getPreferredSize());
		buffer.append(",");
		buffer.append("size:"+getSize());
		buffer.append(",");
		buffer.append("bounds:"+getBounds());
		return buffer.toString();
	}

	class TestDrawFigure extends IconFigure {
		private static final int	FIGURE_SIZE= 20;
		private static final int	SELECTION_BORDER= 2;
//		private UnitFigure	parent;

		public TestDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			this.setSize(this.getPreferredSize());
		}

		protected void paintFigure(Graphics graphics) {
			super.paintFigure(graphics);

			// - Get drawing location. This should be already displaced from the top-left.
			Point loc = getLocation();
			Rectangle bound = getBounds().getCopy();
			//- Displace it the selection margin to get the top-left point.
			bound.x+=SELECTION_BORDER;
			bound.y+=SELECTION_BORDER;
			bound.width-=SELECTION_BORDER;
			bound.height-=SELECTION_BORDER;
			//- Reduce the box the margin for the selection border of two pixels and the dangling pixel
			bound.width-=(SELECTION_BORDER+1+1);
			bound.height-=(SELECTION_BORDER+1+1);
//			bound.expand(-1, -1);
			Rectangle border = getBounds().getCopy();
//			bound.x+=1;
//			bound.y+=1;
			border.width-=1+1;
			border.height-=1+1;

			// - Check if the figure is selected. This information is on the parent
//			if (isSelected()) {
//				AirportPart.log.log(Level.INFO, "Painting TestDrawFigure SELECTED");
				// - The unit is selected. Draw an orange border of two pixels.
				graphics.setForegroundColor(getColor());
				graphics.drawRectangle(bound);
				graphics.setForegroundColor(HarpoonColorConstants.ORANGE);
				graphics.setLineWidth(1);
				graphics.drawRectangle(border);
				graphics.drawRectangle(border.expand(-1, -1));
//			} else {
//				AirportPart.log.log(Level.INFO, "Painting TestDrawFigure UNSELECTED");
//				graphics.setForegroundColor(getColor());
//				graphics.drawRectangle(bound);
//			}

			// - Draw the figure center
		  bound = getBounds().getCopy();
			Dimension hotspot = this.getHotSpot();
			bound.x += hotspot.width+1;
			bound.y += hotspot.height+1;
			Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}

		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(FIGURE_SIZE+SELECTION_BORDER+1, FIGURE_SIZE+SELECTION_BORDER+1);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(FIGURE_SIZE/2,FIGURE_SIZE/2);
		}

	}
}

// - UNUSED CODE ............................................................................................
