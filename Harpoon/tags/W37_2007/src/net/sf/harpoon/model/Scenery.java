//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: Scenery.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/Scenery.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-12 11:26:07  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.model;

// - IMPORT SECTION .........................................................................................
import harpoonrcp.WorkflowModelManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;
import java.util.Properties;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.FileStoreEditorInput;
import org.eclipse.ui.ide.IDE;

import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class Scenery extends WorkflowModelManager {
	/** Information about the types of files than can be open from the interface menus. */
	private final static String[]		OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
	private final static String[]		OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };

	private static IWorkbenchWindow	window;
	private static Scenery					currentScenery					= null;
	private static Scenery					newScenery							= null;

	private Properties							properties							= new Properties();
	private String									sceneryName;
	private String									modelFileName;
	private IFileStore							fileStoreModel;
	private HarpoonSceneryEditor		editor;
	private RootMapUnit						units;

	public static String openDialog() {
		// TODO Save locally the previous scenery just in case of errors.
		// TODO Check is an scenery is already open. If so ask for closing.
		// - Get the user to choose an scenery file.
		// String sceneryPath = null;
		final FileDialog fileChooser = new FileDialog(getWindow().getShell(), SWT.OPEN);
		// TODO Configure the text of the dialog to be presented to the user.
		fileChooser.setText("Texto to be configured.");
		fileChooser.setFilterPath(null);
		fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
		fileChooser.setFilterNames(OPEN_FILTER_NAMES);
		final String sceneryName = fileChooser.open();
		return sceneryName;
	}

	public static IWorkbenchWindow getWindow() {
		return window;
	}

	public static void setWindow(IWorkbenchWindow window) {
		Scenery.window = window;
	}

	public static boolean checkFileStore(String sceneryName) throws PartInitException {
		if (null == sceneryName) return false;
		newScenery = new Scenery();
		newScenery.setSceneryName(sceneryName);

		// - Check that the file selected exists and is valid.
		IFileStore fileStore = EFS.getLocalFileSystem().getStore(new Path(sceneryName));
		if (!fileStore.fetchInfo().isDirectory() && fileStore.fetchInfo().exists()) {
			// - Read the scenery properties and locate the scenery object data.
			final File sceneryFile = new File(sceneryName);
			if (null == sceneryFile)
				throw new PartInitException("The scenery file selected cannot be open or is empty.");
			Properties props = new Properties();
			try {
				props.load(new BufferedInputStream(new FileInputStream(sceneryFile)));
				newScenery.setProperties(props);
				String modelFileName = props.getProperty("modelLocation");
				if (null == modelFileName)
					throw new PartInitException("The scenery model data file selected cannot be open or is empty.");
				newScenery.setModelFileName(modelFileName);

				// - Test if the model exists and then open through the interface.
				IFileStore fileStoreModel = EFS.getLocalFileSystem().getStore(new Path(modelFileName));
				if (!fileStoreModel.fetchInfo().isDirectory() && fileStoreModel.fetchInfo().exists()) {
					newScenery.setFileStore(fileStoreModel);
					return true;
				}
			} catch (FileNotFoundException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be found.");
			} catch (IOException e) {
				// EXCEPTION PartInitException("The scenery file open has failed.");
				// throw new PartInitException("The scenery file open has failed." + e.getMessage());
				e.printStackTrace();
				throw new PartInitException("The scenery model data file cannot be read or open.");
			}
		}
		return false;
	}

	private void setFileStore(IFileStore fileStoreModel) {
		// TODO Auto-generated method stub
		this.fileStoreModel = fileStoreModel;
	}

	private void setModelFileName(String modelFileName) {
		// TODO Auto-generated method stub
		this.modelFileName = modelFileName;
	}

	private void setProperties(Properties props) {
		// TODO Auto-generated method stub
		this.properties = props;
	}

	private void setSceneryName(String sceneryName) {
		// TODO Auto-generated method stub
		this.sceneryName = sceneryName;
	}

	// public static void closeOld() {
	// // TODO Auto-generated method stub
	// currentScenery.close();
	// }

	private void close() {
		// TODO Close the current scenery.
		// TODO Close the editor and the resources that are related to it
	}

	/**
	 * Once the scenery file selected is valid and accessible, this method calla the IDE to locate the specific
	 * editor that understands the selected file's extension. This will fire the EditorPart creation and
	 * initialization method with calls to the methods <code>init()</code> and <code>setInput()</code>.
	 */
	public static Scenery open() throws PartInitException {
		IWorkbenchPage page = window.getActivePage();
		HarpoonSceneryEditor editor = (HarpoonSceneryEditor) IDE.openEditorOnFileStore(page, newScenery.getFileStoreModel());
		newScenery.setEditor(editor);
		return newScenery;
	}

	private void setEditor(HarpoonSceneryEditor editor) {
		// TODO Auto-generated method stub
		this.editor = editor;
	}

	public Properties getProperties() {
		// TODO Auto-generated method stub
		return this.properties;
	}

	private IFileStore getFileStoreModel() {
		// TODO Auto-generated method stub
		return this.fileStoreModel;
	}

	public static Scenery getNewScenery() {
		// TODO Auto-generated method stub
		return newScenery;
	}

	/**
	 * Loads the model data and creates the corresponding MVC structures and initializes them.
	 * 
	 * @param input
	 * @throws PartInitException
	 */
	public RootMapUnit readModel(IEditorInput input) throws PartInitException {
		try {
			// - Convert to a suitable File class to access the URI
			FileStoreEditorInput fse = (FileStoreEditorInput) input;

			// - The URI gets the file name and path for the scenery data.
			URI sceneryURI = fse.getURI();
			FileInputStream fis = new FileInputStream(sceneryURI.getPath());

			// - There are two things to be read, the Model and the scenery properties
			ObjectInputStream ois = new ObjectInputStream(fis);
			units = (RootMapUnit) ois.readObject();
			ois.close();
			units.setMapProperties(newScenery.getProperties());
			return units;
		} catch (FileNotFoundException e) {
			throw new PartInitException("The model store file does not exist");
		} catch (IOException e) {
			throw new PartInitException("The model store file does not exist");
		} catch (ClassNotFoundException e) {
			throw new PartInitException("The model store file does not exist");
		}
	}

	public static void cleanOldEditor() {
		// TODO Auto-generated method stub
		if(null!=currentScenery)currentScenery.close();
		currentScenery = newScenery;
	}

	public RootMapUnit getRootModel() {
		return this.units;
	}
}

// - UNUSED CODE ............................................................................................
