//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonSelectionListener.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonSelectionListener.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package harpoonrcp;

// - IMPORT SECTION .........................................................................................
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor;
import net.sourceforge.rcp.harpoon.app.SelectionView;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class HarpoonSelectionListener implements ISelectionListener {

	private HarpoonSceneryEditor	editor;
	private Hashtable selectionContent = new Hashtable();
	private SelectionView	selectionView;

	public HarpoonSelectionListener(HarpoonSceneryEditor editor) {
		// TODO Auto-generated constructor stub
		this.editor=editor;
		this.selectionView=selectionView;
	}

	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		StructuredSelection sel = (StructuredSelection) selection;
//		int size = sel.size();
		if(!sel.isEmpty()) {
		Iterator it = sel.iterator();
		selectionContent = new Hashtable();
		while(it.hasNext()) {
			AbstractGraphicalEditPart element = (AbstractGraphicalEditPart) it.next();
			Unit model =((Unit) element.getModel());
			String name = model.toString();
			// - Add the selected element to the selection
			selectionContent.put(name, element);
//			int index = 1;
		}}
		editor.updateActions(editor.getEditActions());
		getSelectionView().updateSelection(selectionContent);
	}

	private SelectionView getSelectionView() {
		if (null==this.selectionView) {
			this.selectionView=SelectionView.getSingleton();
		}
		return selectionView;
	}
}

// - UNUSED CODE ............................................................................................
