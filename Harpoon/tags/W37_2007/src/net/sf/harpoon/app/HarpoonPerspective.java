//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonPerspective.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonPerspective.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-10 12:56:18  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.1  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class HarpoonPerspective implements IPerspectiveFactory {

	/**
	 * Creates the initial panes and elements that are inside the workspace. The sizes and relative positioning
	 * are handled by some predefines constants.<br>
	 * The elements defines in the Harpoon perspective are three:
	 * <ul>
	 * <li>The Editor panel where we load and display the scenery map.</li>
	 * <li>The Property and selection view, where we display selection properties or selection contents.</li>
	 * <li>The message are where we display game messages and commands results.</li>
	 * </ul>
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		// - Activate the Editor area. The maps are shown on editor parts.
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);

		// - Define the two views that compose the main presentation window
		layout.addStandaloneView(SelectionView.ID, true, IPageLayout.RIGHT, 0.75f, editorArea);
		layout.getViewLayout(SelectionView.ID).setCloseable(false);
	}
}
