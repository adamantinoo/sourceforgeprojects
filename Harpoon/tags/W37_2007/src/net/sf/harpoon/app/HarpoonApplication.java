//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonApplication.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonApplication.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.3  2007-09-10 12:56:17  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.2  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import net.sourceforge.rcp.harpoon.ICommandIds;
import net.sourceforge.rcp.harpoon.model.Scenery;

/**
 * This class controls all aspects of the application's execution
 */
public class HarpoonApplication implements IApplication {
	private static Logger	log	= Logger.getLogger("net.sourceforge");

	/**
	 * This method is called at application start. It just creates and starts the workbench with the particular
	 * configuration for this application.
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		Display display = PlatformUI.createDisplay();
		try {
			// - This line fires the creation of the perspective and all its contents. Also enters main loop
			log.log(Level.INFO,
					"Starting and creating the HarpoonApplication. Running workbench ApplicationWorkbenchAdvisor");
			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) { return IApplication.EXIT_RESTART; }
			return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed()) workbench.close();
			}
		});
	}

	/** Creates the initial windows and sets the default perspective. There should be a default perspective. */
	class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

		private static final String	PERSPECTIVE_ID	= "net.sourceforge.rcp.harpoon.perspective";

		public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			return new ApplicationWorkbenchWindowAdvisor(configurer);
		}

		public String getInitialWindowPerspectiveId() {
			return PERSPECTIVE_ID;
		}

	}

	/** Configures the application window. */
	class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

		public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			super(configurer);
		}

		public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
			return new ApplicationActionBarAdvisor(configurer);
		}

		/** This method is called before any window is open. */
		public void preWindowOpen() {
			IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
			configurer.setInitialSize(new Point(900, 700));
			configurer.setShowCoolBar(true);
			configurer.setShowStatusLine(false);

			configurer.setShowCoolBar(false);
			configurer.setShowFastViewBars(false);
			configurer.setShowMenuBar(true); // Show the application menu bar
			configurer.setShowPerspectiveBar(false);
			configurer.setShowProgressIndicator(false);
			configurer.setShowStatusLine(false);
			// - Set the title of the main window
			configurer.setTitle("Harpoon Simulation. Draft 00.02");
		}

	}

	/**
	 * An action bar advisor is responsible for creating, adding, and disposing of the actions added to a
	 * workbench window. Each window will be populated with new actions.<br>
	 * <br>
	 * The methods that should be implemented by customized classes are:
	 * <ul>
	 * <li>fillActionBars(int flags)-Configures the action bars using the given action bar configurer. </li>
	 * <li>fillCoolBar(ICoolBarManager coolBar) - Fills the cool bar with the main toolbars for the window.</li>
	 * <li>fillMenuBar(IMenuManager menuBar) - Fills the menu bar with the main menus for the window. </li>
	 * <li>fillStatusLine(IStatusLineManager statusLine) - Fills the status line with the main status line
	 * contributions for the window. </li>
	 * </ul>
	 * Other method called at startup is the method <code>makeActions(IWorkbenchWindow window)</code>.
	 */
	class ApplicationActionBarAdvisor extends ActionBarAdvisor {

		// Actions - important to allocate these only in makeActions, and then use them
		// in the fill methods. This ensures that the actions aren't recreated
		// when fillActionBars is called with FILL_PROXY.
		private IWorkbenchAction	exitAction;
		private IWorkbenchAction	aboutAction;
		private OpenSceneryAction	openSceneryAction;

		public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
			super(configurer);
		}

		/**
		 * Creates the menu actions and registers them. Registering is needed to ensure that key bindings work.
		 * The corresponding commands key bindings are defined in the plugin.xml file. Registering also provides
		 * automatic disposal of the actions when the window is closed.
		 */
		protected void makeActions(final IWorkbenchWindow window) {
			// - Creates the menu actions and registers them.
			exitAction = ActionFactory.QUIT.create(window);
			register(exitAction);

			aboutAction = ActionFactory.ABOUT.create(window);
			register(aboutAction);

			openSceneryAction = new OpenSceneryAction(window, "Open Scenery...");
			register(openSceneryAction);
		}

		protected void fillMenuBar(IMenuManager menuBar) {
			MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
			MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);

			menuBar.add(fileMenu);
			// Add a group marker indicating where action set menus will appear.
			menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			menuBar.add(helpMenu);

			// File
			fileMenu.add(openSceneryAction);
			fileMenu.add(new Separator());
			fileMenu.add(exitAction);

			// Help
			helpMenu.add(aboutAction);
		}

		public void fillActionBars(int flags) {
			super.fillActionBars(flags);
		}

		protected void fillCoolBar(ICoolBarManager coolBar) {
			super.fillCoolBar(coolBar);
		}

		protected void fillStatusLine(IStatusLineManager statusLine) {
			super.fillStatusLine(statusLine);
		}

		/**
		 * Opens a new scenery and updates the UI. If there is already an scenery open it should ask the user if
		 * the current scenery should be closed and if the modified data should be saved.<br>
		 * Changed the way to generate the presentation layer. Instead getting the main canvas from the map we
		 * create a graphical viewer from the scenery.<br>
		 * <br>
		 * The order of operations to be performed to create a suitable MVC is described next:
		 * <ul>
		 * <li>Store the current scenery to be replaced if there is any error loading the new one.</li>
		 * <li>Open a dialog to allow the user to select the file.</li>
		 * <li>Open the selected file and create a new scenery with its contents.</li>
		 * <li>Load and create the internal data structures not initialized during the scenery creation.</li>
		 * <li>Initialize the GraphicalViewer. This will trigger the creation of all the EditParts and then the
		 * corresponding Figures for the drawing./li>
		 * </ul>
		 */
		class OpenSceneryAction extends Action {
			/** Information about the types of files than can be open from the interface menus. */
			private final String[]					OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
			private final String[]					OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };

			private final IWorkbenchWindow	window;
			private int											instanceNum							= 0;

			// private final int editorId;

			public OpenSceneryAction(IWorkbenchWindow window, String label) {
				log.log(Level.FINE, "Creating instance of Action. OpenSceneryAction");
				this.window = window;
				setText(label);
				// The id is used to refer to the action in a menu or toolbar
				setId(ICommandIds.CMD_OPEN);
				// Associate the action with a pre-defined command, to allow key bindings.
				setActionDefinitionId(ICommandIds.CMD_OPEN);
				setImageDescriptor(net.sourceforge.rcp.harpoon.app.HarpoonActivator
						.getImageDescriptor("/icons/sample2.gif"));
			}

			/**
			 * This method is called when the user select the Open Scenery... menu item. This should open the file
			 * selector and then create and initialize the Scenery Editor to load and display the selected scenery.<br>
			 * There is only a single scenery open at the same time. So we have to test if we have any other open
			 * scenery in the global reference and keep a reference to it in case we detect any exception while
			 * oppening the files.<br>
			 * If there is any exception we keep the previous scenery. If all goes OK before opening the scenery we
			 * close the previous.<br>
			 * We use the <code>Scenery</code> class to help to detect this situations. This class incorporates
			 * the code for the <code>HarpoonScenery</code> and the <code>WorkFlowModelManager</code>.
			 */
			public void run() {
				log.log(Level.FINE, "Menu Action OpenSceneryAction fired");
				if (window != null) {
					try {
						Scenery.setWindow(window);
						final String sceneryName = Scenery.openDialog();
						log.log(Level.INFO, "Selected scenery: " + sceneryName);
						boolean check = Scenery.checkFileStore(sceneryName);
						if (check) {
							Scenery.open();
						}
					} catch (PartInitException e) {
						MessageDialog.openError(window.getShell(), "Title", "Error message");
					}
				}
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
