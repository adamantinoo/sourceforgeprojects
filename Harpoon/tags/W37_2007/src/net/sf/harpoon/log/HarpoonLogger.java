//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonLogger.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/log/HarpoonLogger.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:29 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.rcp.log;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class HarpoonLogger {
	private static Logger	log	= Logger.getLogger("net.sourceforge");

	public static void log(Level level, String message) {
		log.log(level,message);
	}

	public static void info(String message) {
		// TODO Auto-generated method stub
		log.info(message);
	}

}

// - UNUSED CODE ............................................................................................
