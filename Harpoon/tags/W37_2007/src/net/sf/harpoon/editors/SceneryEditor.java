//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryEditor.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryEditor.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-10 12:56:18  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.1  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PartInitException;

import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.rcp.harpoon.model.Scenery;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
/**
 * This class is created to wrap the already available code from other sources. Because the used code has a
 * lot of development that is uncertain if it is needed or not for our current development I will add new
 * classes in from where I will move validated code and new adaptations. It will also show the methods that
 * are really required to create an editor.<br>
 * This class is started when the user opens a file with a extension that is mapped to this editor as it is
 * configured in the plugin descriptor. The first method to be called is the <code>init()</code> that will
 * check the existence of the selected file and prepare the environment for the reading of the data that is
 * performed in the method <code>setInput()</code>.The method <code>createPages()</code> is also called
 * when the editor page is created and perform all visual structures creation.
 */
public class SceneryEditor extends HarpoonSceneryEditor {
	/**
	 * Creates the pages of a multipage Editor. In our implementation we have only a single page so we can move
	 * it to a local field and avoid any page change. <br>
	 * The method creates the SceneryPage and then configures the editor with the page name. All this methods
	 * can be simplified.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#createPages()
	 */
	@Override
	protected void createPages() {
		try {
			// - Create scenery page and make it the active page.
			workflowPageID = addPage(new SceneryPage(this), getEditorInput());
			setActivePage(workflowPageID);
		} catch (PartInitException pie) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"An error occured during opening the scenery.", pie.getStatus());
		}
	}

	/**
	 * Returns the scenery used by this editor.
	 * 
	 * @return the scenery
	 */
	public RootMapUnit getWorkflow() {
		if (null == this.workflow)
			return new RootMapUnit();
		else
			return this.workflow;
	}

	/**
	 * This method is called upon file selection with the matching file extension. File extension associations
	 * may be defined at the plug-in level or at the Workbench configuration Properties.
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#init(org.eclipse.ui.IEditorSite,org.eclipse.ui.IEditorInput)
	 */
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		//TODO Check that the input source is valid. This should have been performed yet
		// - Scenery is ok. Do any other initialization.
		super.init(site, input);
		// - Add selection change listener
		ISelectionService globalSelectionServide = getSite().getWorkbenchWindow().getSelectionService();
		globalSelectionServide.addSelectionListener(this.getSelectionListener());
	}
	
	/** Opens the model and reads it into the workspace. Model is a serialized object from other save points. */
	@Override
	protected void setInput(IEditorInput input) {
		// - Get access to the Scenery instance that we are reading.
		try {
			this.scenery = Scenery.getNewScenery();
			this.workflow = scenery.readModel(input);
			Scenery.cleanOldEditor();
			super.setInput(input);
		} catch (PartInitException pie) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"An error occured during opening the scenery.", pie.getStatus());
		}
	}

	// FIXME Merge this code into the mainstream. Is better written that other code
	// /**
	// * Returns the workflow object from the specified file.
	// *
	// * @param file
	// * @return the workflow object from the specified file
	// */
	// private Scenery create(IFile file) throws CoreException {
	// Scenery workflow = null;
	// modelManager = new WorkflowModelManager();
	//
	// if (file.exists()) {
	// try {
	// modelManager.load(file.getFullPath());
	// } catch (Exception e) {
	// modelManager.createWorkflow(file.getFullPath());
	// }
	//
	// workflow = modelManager.getModel();
	// if (null == workflow) { throw new CoreException(new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0,
	// "Error loading the worklow.", null)); }
	// }
	// return workflow;
	// }
	/**
	 * Saves the state of the scenery. This has to be disabled until the game logic is set.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// EMPTY
	}

	/**
	 * Saves the state of the scenery. This has to be disabled until the game logic is set.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// EMPTY
	}

	/**
	 * Save as... is not supported.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/** Not necessary. We have no markers. */
	@Override
	public void gotoMarker(IMarker marker) {
		// EMPTY
	}
}

// - UNUSED CODE ............................................................................................
