//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SelectionView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/views/SelectionView.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:25:20 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.parts.UnitPart;
import net.sourceforge.rcp.harpoon.View;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N

/**
 * This view is responsible to display the properties of the selected unit is there is only one unit selected
 * or is the selection contains more than one item, it should display the list of elements in the selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection
 * to the newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class SelectionView extends View {
	public static final String ID = "net.sourceforge.rcp.harpoon.app.selectionview";
	private Composite	link;
	private Text				selectionDescription;
	private static SelectionView	singleton;

	// - P U B L I C S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	public void createPartControl(Composite parent) {
		singleton = this;
		// - Create the base parts until the link Composite.
		link = new Composite(parent, SWT.NONE);
		FillLayout layout = new FillLayout();
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		link.setLayout(layout);
		createDefaultSelection(link);
	}

	private void createDefaultSelection(Composite link) {
		// - Default content at initialization.
		Composite banner = new Composite(link, SWT.NONE);
		banner.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL, GridData.VERTICAL_ALIGN_BEGINNING,
				true, false));
		GridLayout layout = new GridLayout();
		layout.marginHeight = 5;
		layout.marginWidth = 10;
		layout.numColumns = 1;
		banner.setLayout(layout);

		// setup bold font
		Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);

		Label title = new Label(banner, SWT.WRAP);
		title.setText("Selection Parts:");
		title.setFont(boldFont);

		selectionDescription = new Text(banner, SWT.MULTI | SWT.WRAP);
		selectionDescription.setText("<empty current selection>");
		selectionDescription.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public void updateSelection(Hashtable selectionContent) {
		// TODO Auto-generated method stub
		StringBuffer buffer=new StringBuffer();
		Enumeration it = selectionContent.elements();
		while (it.hasMoreElements()) {
			UnitPart part = (UnitPart) it.nextElement();
			Unit model = (Unit) part.getModel();
			buffer.append(model.getClass().getSimpleName()).append(":").append(model.getName()).append("\n");
		}
		selectionDescription.setText(buffer.toString());
		selectionDescription.redraw();
	}

	public static SelectionView getSingleton() {
		// TODO Auto-generated method stub
		return singleton;
	}
}

// - UNUSED CODE ............................................................................................
