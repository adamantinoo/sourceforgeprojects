//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DMSPoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/DMSPoint.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:29 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.geom;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class DMSPoint implements Serializable {
	private static final long	serialVersionUID	= -1674626351426841763L;
	private DMSCoordinate	latitude;
	private DMSCoordinate	longitude;

	public DMSPoint(DMSCoordinate lat, DMSCoordinate lon) {
		this.latitude=lat;
		this.longitude=lon;
	}

	public DMSPoint offsetPoint(DMSPoint point) {
		//-Operate by each coordinate independently
		DMSCoordinate finalLat=offset(point.getLatitude(), DMSCoordinate.LATITUDE);
		DMSCoordinate finalLon=offset(point.getLongitude(), DMSCoordinate.LONGITUDE);
		return new DMSPoint(finalLat,finalLon);
	}

	private DMSCoordinate offset(DMSCoordinate destination, int type) {
		// -Convert coordinates to seconds for operation
		long origin;
		if(DMSCoordinate.LATITUDE==type)
			origin = this.latitude.toSeconds();
		else origin = this.longitude.toSeconds();
		long dest = destination.toSeconds();
		long offset = dest-origin;
		
		// - Convert back from seconds to a DMS coordinate
		DMSCoordinate result = DMSCoordinate.fromSeconds(offset);
		if(DMSCoordinate.LATITUDE==type)
			if (offset<0) result.setSense('S'); else result.setSense('N');
		else
			if (offset<0) result.setSense('W'); else result.setSense('E');
		return result;
	}

	public DMSCoordinate getLongitude() {
		if(null==longitude) this.longitude=new DMSCoordinate();
		return this.longitude;
	}

	public DMSCoordinate getLatitude() {
		if(null==latitude) this.latitude=new DMSCoordinate();
		return this.latitude;
	}
@Override
public String toString() {
	// TODO Auto-generated method stub
	return "DMSPoint("+latitude.toString()+","+longitude.toString()+")";
}
}

// - UNUSED CODE ............................................................................................
