//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DMSCoordinate.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/DMSCoordinate.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:29 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.geom;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class DMSCoordinate implements Serializable {
	private static final long	serialVersionUID	= -8767644931020534493L;
	public static final int	 LATITUDE	= 1;
	public static final int	 LONGITUDE	= 2;
	private int	degrees=0;
	private int	minutes=0;
	private int	seconds=0;
	private char	sense='X';

	public DMSCoordinate(int degrees, int minutes, int seconds, char sense) {
		this.degrees = degrees;
		this.minutes=minutes;
		this.seconds=seconds;
		this.sense=sense;
	}

	public DMSCoordinate() {
		// TODO Auto-generated constructor stub
		this(0,0,0,'X');
	}

	public DMSCoordinate(int degrees, int minutes, int seconds) {
		this(degrees,minutes,seconds,'X');
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "DMSCoordinate("+degrees+","+minutes+","+seconds+","+sense+")";
	}

	public long toSeconds() {
		//- Convert degrees to a number of seconds
		long secs = degrees*60*60+minutes*60+seconds;
		if ('S'==sense) return -secs;
		if ('W'==sense) return -secs;
		return secs;
	}

	public void setSense(char sense) {
		// TODO Auto-generated method stub
		this.sense = sense;
	}

	public static DMSCoordinate fromSeconds(long offset) {
		//- Get the sign of the result.
		int sign = 1;
		if (offset<0) sign = -1;
		//-Calculate the number of entire degrees and minutes and recreate the DMS coordinate.
		Double value = Math.abs(new Double(offset));
		int degrees = new Double(Math.floor(value/3600.0)).intValue();
		value = value - degrees*3600.0;
		int minutes = new Double(Math.floor(value/60.0)).intValue();
		value = value - minutes*60.0;
		if (1==sign)
		return new DMSCoordinate(degrees,minutes,value.intValue());
		else
			return new DMSCoordinate(-degrees,minutes,value.intValue());
	}
}

// - UNUSED CODE ............................................................................................
