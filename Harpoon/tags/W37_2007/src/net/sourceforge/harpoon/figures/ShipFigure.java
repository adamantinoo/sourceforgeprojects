//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/ShipFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:28 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.1  2007-08-28 13:46:58  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This Figure represents the graphical view for a Ship game unit. The representation is drawn as a hollow
 * circle with a point inside. The left of this box has two labels, one with the speed and the other with the
 * direction of the unit.<br>
 * The color of the icon box depends on the side of the unit.<br>
 * The location is centered on the center point of the ship icon.<br>
 * <br>
 * The implementation is based into two classes, one local that draws the icon representation. This has a
 * fixed size and contains the location hot spot that is the point that is set as the location of the unit.
 * The other class is obtained by inheritance and contains the methods to create the Grid layout and the
 * labels that display the figure data.<br>
 * Figure data has to be updated when the model changes, so we have to implement methods to export the fields
 * interface so the EditPart can call them to set the new model values.<br>
 * <br>
 * Required values to be known by this figure to be able to draw correctly are:
 * <ul>
 * <li>speed and direction </li>
 * <li>side of the game</li>
 * </ul>
 * Location is something that currently is not managed by this representation class so global data is not
 * needed.
 */
public class ShipFigure extends MovableFigure {
	private static final int	MARGIN	= 2;

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of a Ship. This is drawn as a hollow
	 * circle with a point inside. The left of this box has two labels, one with the speed and the other with
	 * the direction of the unit.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center point of the ship icon.
	 */
	public ShipFigure() {
		// - Create the complex internal parts of this figure
		this.setLayoutManager(createStdLayout(MARGIN));
		createMovableIcon(new ShipDrawFigure(this));
		createLabelinfo();
		this.setSize(this.getPreferredSize(-1, -1));
	}

	/** This internal class is the Figure graphic element that knows how to draw the Ship iconic representation. */
	class ShipDrawFigure extends IconFigure {
		private static final int	SHIP_SIZE	= 15;

		/** Create a new instance and set the initial drawing color. */
		public ShipDrawFigure(UnitFigure shipFigure) {
			super(shipFigure);
			this.setSize(SHIP_SIZE, SHIP_SIZE);
		}

		protected void paintFigure(Graphics graphics) {
			super.paintFigure(graphics);

			// - Check if the figure is selected. This information is on the parent
			if (getContainer().isSelected()) {
				graphics.fillOval(getBounds());
			} else {
				// - Set the drawing color. Fills color are done on background color.
				graphics.setForegroundColor(getColor());
				// graphics.setBackgroundColor(this.color);
				// DEBUG Get the value and show it on the debugger
				graphics.drawOval(getBounds());
			}
			Rectangle bo = this.getBounds().getCopy();
			Point startPoint = new Point(bo.x + SHIP_SIZE / 2, bo.y + SHIP_SIZE / 2);
			Point endPoint = new Point(bo.x + SHIP_SIZE / 2 + 1, bo.y + SHIP_SIZE / 2 + 1);
			graphics.drawLine(startPoint, endPoint);
		}

		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(SHIP_SIZE, SHIP_SIZE);
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(SHIP_SIZE / 2, SHIP_SIZE / 2);
		}

	}
}
// - UNUSED CODE ............................................................................................
