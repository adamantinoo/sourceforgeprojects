//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AircraftFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/AircraftFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:28 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This Figure represents the graphical view for a Aircraft game unit. The representation is drawn as a hollow
 * square with a point inside. The left of this box has two labels, one with the speed and the other with the
 * direction of the unit.<br>
 * The color of the icon box depends on the side of the unit.<br>
 * The location is centered on the center point of the ship icon.<br>
 * <br>
 * The implementation is based into two classes, one local that draws the icon representation. This has a
 * fixed size and contains the location hot spot that is the point that is set as the location of the unit.
 * The other class is obtained by inheritance and contains the methods to create the Grid layout and the
 * labels that display the figure data.<br>
 * Figure data has to be updated when the model changes, so we have to implement methods to export the fields
 * interface so the EditPart can call them to set the new model values.<br>
 * <br>
 * Required values to be known by this figure to be able to draw correctly are:
 * <ul>
 * <li>speed and direction </li>
 * <li>side of the game</li>
 * </ul>
 * Location is something that currently is not managed by this representation class so global data is not
 * needed.
 */
public class AircraftFigure extends MovableFigure {
	private static final int MARGIN = 2;

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of a Aircraft. This is drawn as a hollow
	 * square with a point inside. The left of this box has two labels, one with the speed and the other with
	 * the direction of the unit.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center point of the ship icon.
	 */
	public AircraftFigure() {
		// - Create the complex internal parts of this figure
		this.setLayoutManager(createStdLayout(MARGIN));
		createMovableIcon(new AircraftDrawFigure(this));
		createLabelinfo();
		this.setSize(this.getPreferredSize(-1, -1));

//		// - Create the complex internal parts of this figure
//		GridLayout gridLayout = new GridLayout();
//		gridLayout.numColumns = 2;
//		this.setLayoutManager(gridLayout);
//		createMovableIcon();
//		createLabelinfo();
//
//		this.setSize(this.getPreferredSize());
//		this.setSide(Unit.UNKNOWN_SIDE);
	}

//	protected void createMovableIcon() {
//		movable = new AircraftDrawFigure(this.color);
//		this.add(movable);
//	}
	class AircraftDrawFigure extends IconFigure {
		private static final int AIR_SIZE=15;

		public AircraftDrawFigure(AircraftFigure aircraftFigure) {
			super(aircraftFigure);
			this.setSize(AIR_SIZE,AIR_SIZE);
		}

		protected void paintFigure(Graphics graphics) {
			super.paintFigure(graphics);
			// - Check if the figure is selected. This information is on the parent
			if (getContainer().isSelected()) {
				// - Set the drawing color. Fills color are done on background color.
				graphics.setForegroundColor(getColor());
				graphics.fillRectangle(getBounds());
			} else {
				// - Set the drawing color. Fills color are done on background color.
				graphics.setForegroundColor(getColor());
				graphics.drawRectangle(getBounds());
			}
		}

		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(AIR_SIZE,AIR_SIZE);
		}

		/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
		@Override
		public Dimension getHotSpot() {
			return new Dimension(AIR_SIZE/2,AIR_SIZE/2);
		}

	}
}

// - UNUSED CODE ............................................................................................
