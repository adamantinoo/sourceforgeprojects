//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/UnitFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:28 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.1  2007-08-27 10:24:52  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.gef.EditPart;
import org.eclipse.swt.graphics.Color;

import net.sourceforge.harpoon.HarpoonColorConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitFigure extends Figure {
	/** Drawing color for the unit. Depends on the game side. */
	private Color	color	= HarpoonColorConstants.UNKNOWN_SIDE;
	/** Selection state value obtained from the EditPart and used to selection visual feedback. */
	private int			selected	= EditPart.SELECTED_NONE;

	// - P U B L I C S E C T I O N
	public abstract Dimension getHotSpot();

	/** Sets the selection value to one of the three selection states. */
	public void setSelected(int value) {
		this.selected = value;
		System.out.println(this.toString() + "Selected = " + value);
	}

	/**
	 * Returns the selection state value. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state value.
	 */
	public int getSelected() {
		return this.selected;
	}

	/**
	 * Returns the selection state flag status. This method is used to inform the painting process of the visual
	 * selection state.
	 * 
	 * @return the selection state as a flag.
	 */
	public boolean isSelected() {
		if ((EditPart.SELECTED_PRIMARY == getSelected()) || (EditPart.SELECTED == getSelected()))
			return true;
		else
			return false;
	}

	public Color getColor() {
		return this.color;
	}

	public void setColor(Color newColor) {
		this.color = newColor;
//		this.movable.setColor(newColor);
//		this.movable.repaint();
		this.repaint();
	}

	// - P R O T E C T E D S E C T I O N
	protected GridLayout createStdLayout(int margin) {
		GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = margin;
		grid.marginHeight = margin;
		grid.marginWidth = margin;
		grid.verticalSpacing = 0;
		return grid;
	}

}

// - UNUSED CODE ............................................................................................
