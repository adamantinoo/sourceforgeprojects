//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPartFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/HarpoonPartFactory.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:58 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.ShipUnit;
import net.sourceforge.harpoon.rcp.test.TestPart;
import net.sourceforge.harpoon.rcp.test.TestUnit;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonPartFactory implements EditPartFactory {
	public static Logger				log	= Logger.getLogger("net.sourceforge");

//	private static RootMapUnit		modelRoot;
	private static RootMapPart		diagram;

	// private static Properties editorProperties;

	// public HarpoonPartFactory(Properties properties) {
	// // TODO Auto-generated constructor stub
	// editorProperties = properties;
	// }
	//
//	public static RootMapUnit getModelRoot() {
//		return modelRoot;
//	}

	public static RootMapPart getDiagram() {
		return diagram;
	}

	// - O V E R R I D E S
	/**
	 * This method creates any of the EditParts in the application depending on the class of the element model
	 * received as the parameter. This is the main point where we assemble the model to the internal operating
	 * model used by GEF.
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		log.log(Level.FINE, "Creating EditPart for model type: " + model.getClass().getSimpleName());
		EditPart part = null;

		// - If the EditPart requested is the root diagram EditPart then set it as the diagram root.
		if (model instanceof RootMapUnit) {
			log.log(Level.FINE, "Creating root EditPart");
			part = new RootMapPart();
//			modelRoot = (RootMapUnit) model;
			diagram = (RootMapPart) part;
		} else if (model instanceof ShipUnit)
			part = new ShipPart();
		else if (model instanceof AirUnit)
			part = new AirPart();
		else if (model instanceof AirportUnit)
			part = new AirportPart();
		else if (model instanceof TestUnit) part = new TestPart();
		// else if (model instanceof MultiUnit) {
		// //- The model and the Part have the same class but may contain a different subtype.
		// subtype = model.getSubtype();
		// part = new TestPart();
		// }

		// - Initialize the part setting the model and the root EditPart of the internal model.
		part.setModel(model);
		part.setParent(diagram);
		return part;
	}
	//
	// public static Properties getEditorProperties() {
	// return editorProperties;
	// }
}
// - UNUSED CODE ............................................................................................
