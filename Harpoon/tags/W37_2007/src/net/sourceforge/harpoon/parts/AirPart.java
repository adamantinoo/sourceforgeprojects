//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/AirPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-12 11:26:29 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.IFigure;

import net.sourceforge.harpoon.figures.AircraftFigure;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.rcp.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class AirPart extends UnitPart {

//	public AirPart(Unit unit) {
//		super(unit);
//	}

	@Override
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		AirUnit unit = (AirUnit) this.getModel();

		// - Create and initialize the figure
		AircraftFigure air = new AircraftFigure();
		HarpoonLogger.log(Level.FINE, "Creating Figure AircraftFigure");
		air.setSide(unit.getSide());
		air.setDirection(unit.getDirection());
		air.setSpeed(unit.getSpeed());
		HarpoonLogger.log(Level.FINE, air.toString());

//		// - Calculate the figure location. The part has access to the model.
//		HarpoonMap map = ((RootMapUnit) this.getParent().getModel()).getMap();
//		double topLeftLat = map.getTopLatitude();
//		double topLeftLon = map.getTopLongitude();
//		PolarCoordinate latSpan = map.getLatitude2Zoom();
//		PolarCoordinate lonSpan = map.getLongitude2Zoom();
//		
//		double latDiff = Math.abs(topLeftLat-unit.getLatitude());
//		double lonDiff = Math.abs(topLeftLon-unit.getLongitude());
//		
//		int xx = new Double(256*lonDiff/lonSpan.toDegrees()).intValue();
//		int yy = new Double(256*latDiff/latSpan.toDegrees()).intValue();
//
//		double degUnit = unit.getLatitude();
//		double degTopLeft = topLeftLat;
//		double diffLat = 0.0;
//		if (degUnit < degTopLeft) diffLat = degTopLeft - degUnit;
//		double span = diffLat / latSpan.toDegrees();
//		int x = new Double(span * 256.0).intValue();
//		int y = new Double((unit.getLongitude() - topLeftLon) / lonSpan.toDegrees() * 256.0).intValue();
//
//		// - Calculate the pixel position of the center.
//		air.setLocation(new Point(xx, yy));

		return air;
	}

//	@Override
//	protected void createEditPolicies() {
//		// TODO Auto-generated method stub
//		
//	}

//	@Override
//	public void addNotify() {
//		// TODO Auto-generated method stub
//		super.addNotify();
//	}
////	private AirUnit getUnit() {
////		return (AirUnit) this.getModel();
////	}
//	/** Refresh visual data from the model data. */
//	protected void refreshVisuals() {
//		int dummy = 1;
////		getFigure().setText(getUnit().getName());
//	}
}


//}

// - UNUSED CODE ............................................................................................
