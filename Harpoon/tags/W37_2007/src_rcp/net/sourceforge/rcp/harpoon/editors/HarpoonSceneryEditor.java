package net.sourceforge.rcp.harpoon.app;

import harpoonrcp.DelegatingCommandStack;
import harpoonrcp.HarpoonSelectionListener;
import harpoonrcp.WorkflowModelManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.gef.GEFPlugin;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CommandStackListener;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.DeleteAction;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.gef.ui.actions.PrintAction;
import org.eclipse.gef.ui.actions.RedoAction;
import org.eclipse.gef.ui.actions.SaveAction;
import org.eclipse.gef.ui.actions.SelectionAction;
import org.eclipse.gef.ui.actions.StackAction;
import org.eclipse.gef.ui.actions.UndoAction;
import org.eclipse.gef.ui.actions.UpdateAction;
import org.eclipse.gef.ui.actions.WorkbenchPartAction;
import org.eclipse.gef.ui.parts.SelectionSynchronizer;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.PropertySheetPage;

import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.rcp.harpoon.model.Scenery;


/**
 * This class is the multi page editor for our sample application. It shows how to start implementing your own
 * multi page GEF editor.
 * 
 * <p>
 * The design is really simple. We share the same model over all pages but each page is an independent
 * graphical editor and could be used separately.
 * 
 * @author Gunnar Wagenknecht
 */
public class HarpoonSceneryEditor extends MultiPageEditorPart implements IAdaptable {

	/**
	 * This class listens for command stack changes of the pages contained in this editor and decides if the
	 * editor is dirty or not.
	 * 
	 * @author Gunnar Wagenknecht
	 */
	private class MultiPageCommandStackListener implements CommandStackListener {

		/** the observed command stacks */
		private List	commandStacks	= new ArrayList(2);

		/**
		 * Adds a <code>CommandStack</code> to observe.
		 * 
		 * @param commandStack
		 */
		public void addCommandStack(CommandStack commandStack) {
			commandStacks.add(commandStack);
			commandStack.addCommandStackListener(this);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.gef.commands.CommandStackListener#commandStackChanged(java.util.EventObject)
		 */
		public void commandStackChanged(EventObject event) {
			if (((CommandStack) event.getSource()).isDirty()) {
				// at least one command stack is dirty,
				// so the multi page editor is dirty too
				setDirty(true);
			} else {
				// probably a save, we have to check all command stacks
				boolean oneIsDirty = false;
				for (Iterator stacks = commandStacks.iterator(); stacks.hasNext();) {
					CommandStack stack = (CommandStack) stacks.next();
					if (stack.isDirty()) {
						oneIsDirty = true;
						break;
					}
				}
				setDirty(oneIsDirty);
			}
		}

		/**
		 * Disposed the listener
		 */
		public void dispose() {
			for (Iterator stacks = commandStacks.iterator(); stacks.hasNext();) {
				((CommandStack) stacks.next()).removeCommandStackListener(this);
			}
			commandStacks.clear();
		}

		/**
		 * Marks every observed command stack beeing saved. This method should be called whenever the editor/model
		 * was saved.
		 */
		public void markSaveLocations() {
			for (Iterator stacks = commandStacks.iterator(); stacks.hasNext();) {
				CommandStack stack = (CommandStack) stacks.next();
				stack.markSaveLocation();
			}
		}
	}

	/**
	 * This class listens to changes to the file system in the workspace, and makes changes accordingly. 1) An
	 * open, saved file gets deleted -> close the editor 2) An open file gets renamed or moved -> change the
	 * editor's input accordingly
	 * 
	 * @author Gunnar Wagenknecht
	 */
	private class ResourceTracker implements IResourceChangeListener, IResourceDeltaVisitor {
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
		 */
		public void resourceChanged(IResourceChangeEvent event) {
			IResourceDelta delta = event.getDelta();
			try {
				if (delta != null) delta.accept(this);
			} catch (CoreException exception) {
				HarpoonActivator.getInstance().getLog().log(exception.getStatus());
				exception.printStackTrace();
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
		 */
		public boolean visit(IResourceDelta delta) {
			if (delta == null || !delta.getResource().equals(((IFileEditorInput) getEditorInput()).getFile()))
				return true;

			if (delta.getKind() == IResourceDelta.REMOVED) {
				if ((IResourceDelta.MOVED_TO & delta.getFlags()) == 0) {
					// if the file was deleted
					// NOTE: The case where an open, unsaved file is deleted is being handled by the
					// PartListener added to the Workbench in the initialize() method.
					if (!isDirty()) closeEditor(false);
				} else {
					// else if it was moved or renamed
					final IFile newFile = ResourcesPlugin.getWorkspace().getRoot().getFile(delta.getMovedToPath());
					Display display = getSite().getShell().getDisplay();
					display.asyncExec(new Runnable() {
						public void run() {
							setInput(new FileEditorInput(newFile));
						}
					});
				}
			}
			return false;
		}
	}

	/** the editor's action registry */
	private ActionRegistry								actionRegistry;

	/** id of the compound tasks page */
	private int														compoundTasksPageID;

	/** the delegating CommandStack */
	private DelegatingCommandStack				delegatingCommandStack;

	/**
	 * The <code>CommandStackListener</code> that listens for changes of the
	 * <code>DelegatingCommandStack</code>.
	 */
	private CommandStackListener					delegatingCommandStackListener	= new CommandStackListener() {
																																					public void commandStackChanged(
																																							EventObject event) {
																																						updateActions(stackActionIDs);
																																					}
																																				};

	/** the list of action ids that are editor actions */
	private List													editorActionIDs									= new ArrayList();

	/** the list of action ids that are to EditPart actions */
	private List													editPartActionIDs								= new ArrayList();

	/** the multi page editor's dirty state */
	private boolean												isDirty													= false;

	/** the <code>CommandStackListener</code> */
	private MultiPageCommandStackListener	multiPageCommandStackListener;

	// DEVEL discarded at this stage
//	/** the overview outline page */
//	private WorkflowEditorOutlinePage			outlinePage;

	// DEVEL Palette is also discarded in this kind of editor
//	/** the palette root */
//	private PaletteRoot										paletteRoot;

	/** the resource tracker instance */
	private ResourceTracker								resourceTracker;

	/** the selection listener */
	//FIXME NOW
	private ISelectionListener						selectionListener								= new HarpoonSelectionListener(this);
//private ISelectionListener						selectionListener								= new ISelectionListener() {
//																																					public void selectionChanged(
//																																							IWorkbenchPart part,
//																																							ISelection selection) {
//																																						updateActions(editPartActionIDs);
//																																					}
//																																				};

	/** the shared key handler */
	private KeyHandler										sharedKeyHandler;

	/** the list of action ids that are to CommandStack actions */
	private List													stackActionIDs									= new ArrayList();

	/** the selection synchronizer for the edit part viewer */
	private SelectionSynchronizer					synchronizer;

	/** the undoable <code>IPropertySheetPage</code> */
	private PropertySheetPage							undoablePropertySheetPage;

	// DEVEL Changed to our document that is close to the Scenery but in reality a RootMapUnit. Create new class to resemble structure
	/** this is out workflow */
//	private Workflow											workflow;
	protected RootMapUnit	workflow;

	/** id of the workflowPage */
	protected int														workflowPageID;


	public List getEditActions() {
		// TODO Auto-generated method stub
		return this.editPartActionIDs;
	}
	/**
	 * Adds an action to this editor's <code>ActionRegistry</code>. (This is a helper method.)
	 * 
	 * @param action
	 *          the action to add.
	 */
	protected void addAction(IAction action) {
		getActionRegistry().registerAction(action);
	}

	/**
	 * Adds an editor action to this editor.
	 * 
	 * <p>
	 * <Editor actions are actions that depend and work on the editor.
	 * 
	 * @param printAction
	 *          the editor action
	 */
	protected void addEditorAction(WorkbenchPartAction printAction) {
		getActionRegistry().registerAction(printAction);
		editorActionIDs.add(printAction.getId());
	}

	/**
	 * Adds an <code>EditPart</code> action to this editor.
	 * 
	 * <p>
	 * <code>EditPart</code> actions are actions that depend and work on the selected <code>EditPart</code>s.
	 * 
	 * @param action
	 *          the <code>EditPart</code> action
	 */
	protected void addEditPartAction(SelectionAction action) {
		getActionRegistry().registerAction(action);
		editPartActionIDs.add(action.getId());
	}

	/**
	 * Adds an <code>CommandStack</code> action to this editor.
	 * 
	 * <p>
	 * <code>CommandStack</code> actions are actions that depend and work on the <code>CommandStack</code>.
	 * 
	 * @param action
	 *          the <code>CommandStack</code> action
	 */
	protected void addStackAction(StackAction action) {
		getActionRegistry().registerAction(action);
		stackActionIDs.add(action.getId());
	}

	/**
	 * Closes this editor.
	 * 
	 * @param save
	 */
	void closeEditor(final boolean save) {
		getSite().getShell().getDisplay().syncExec(new Runnable() {
			public void run() {
				getSite().getPage().closeEditor(HarpoonSceneryEditor.this, save);
			}
		});
	}

	/**
	 * Creates actions and registers them to the ActionRegistry.
	 */
	protected void createActions() {
		addStackAction(new UndoAction(this));
		addStackAction(new RedoAction(this));

		addEditPartAction(new DeleteAction((IWorkbenchPart) this));
//		addEditPartAction(new WorkflowAction(this, WorkflowAction.ADD_CONDITION_TO_CHOICE));

//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.LEFT));
//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.RIGHT));
//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.TOP));
//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.BOTTOM));
//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.CENTER));
//		addEditPartAction(new AlignmentAction((IWorkbenchPart) this, PositionConstants.MIDDLE));

		addEditorAction(new SaveAction(this));
		addEditorAction(new PrintAction(this));

//		IAction zoomIn = new ZoomInAction(getDelegatingZoomManager());
//		IAction zoomOut = new ZoomOutAction(getDelegatingZoomManager());
//		addAction(zoomIn);
//		addAction(zoomOut);
//		getSite().getKeyBindingService().registerAction(zoomIn);
//		getSite().getKeyBindingService().registerAction(zoomOut);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#createPages()
	 */
	protected void createPages() {
		try {
			// -Create scenery page
			workflowPageID = addPage(new SceneryPage(this), getEditorInput());
			setPageText(workflowPageID, getSceneryPage().getPageName());

//			// create compound tasks page
//			compoundTasksPageID = addPage(new CompoundTasksPage(this), getEditorInput());
//			setPageText(compoundTasksPageID, getCompoundTasksPage().getPageName());

			// add command stacks
			getMultiPageCommandStackListener().addCommandStack(getSceneryPage().getCommandStack()); // returns the command stack for the edit domain
//			getMultiPageCommandStackListener().addCommandStack(getCompoundTasksPage().getCommandStack());

			// activate delegating command stack
			getDelegatingCommandStack().setCurrentCommandStack(getSceneryPage().getCommandStack());

			// set active page
			setActivePage(workflowPageID);
		} catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"An error occured during opening the editor.", e.getStatus());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#dispose()
	 */
	public void dispose() {
		// dispose multi page command stack listener
		getMultiPageCommandStackListener().dispose();

		// remove delegating CommandStackListener
		getDelegatingCommandStack().removeCommandStackListener(getDelegatingCommandStackListener());

		// remove selection listener
		getSite().getWorkbenchWindow().getSelectionService().removeSelectionListener(getSelectionListener());

		// disposy the ActionRegistry (will dispose all actions)
		getActionRegistry().dispose();

		// important: always call super implementation of dispose
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {
		try {
			IFile file = ((IFileEditorInput) getEditorInput()).getFile();
			if (file.exists()
					|| org.eclipse.jface.dialogs.MessageDialogWithToggle.openConfirm(getSite().getShell(),
							"Create File", "The file '" + file.getName() + "' doesn't exist. Click OK to create it.")) {
				save(file, monitor);
				getMultiPageCommandStackListener().markSaveLocations();
			}
		} catch (CoreException e) {
			ErrorDialog.openError(getSite().getShell(), "Error During Save",
					"The current workflow model could not be saved.", e.getStatus());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.ISaveablePart#doSaveAs()
	 */
	public void doSaveAs() {
		SaveAsDialog dialog = new SaveAsDialog(getSite().getShell());
		dialog.setOriginalFile(((IFileEditorInput) getEditorInput()).getFile());
		dialog.open();
		IPath path = dialog.getResult();

		if (path == null) return;

		ProgressMonitorDialog progressMonitorDialog = new ProgressMonitorDialog(getSite().getShell());
		IProgressMonitor progressMonitor = progressMonitorDialog.getProgressMonitor();

		try {
			save(ResourcesPlugin.getWorkspace().getRoot().getFile(path), progressMonitor);
			getMultiPageCommandStackListener().markSaveLocations();
		} catch (CoreException e) {
			ErrorDialog.openError(getSite().getShell(), "Error During Save",
					"The current workflow model could not be saved.", e.getStatus());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#firePropertyChange(int)
	 */
	protected void firePropertyChange(int propertyId) {
		super.firePropertyChange(propertyId);
		updateActions(editorActionIDs);
	}

	/**
	 * Returns the action registry of this editor.
	 * 
	 * @return the action registry
	 */
	protected ActionRegistry getActionRegistry() {
		if (actionRegistry == null) actionRegistry = new ActionRegistry();

		return actionRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	public Object getAdapter(Class type) {
		if (type == IPropertySheetPage.class)
			return getPropertySheetPage();
		else if (type == CommandStack.class)
			return getDelegatingCommandStack();
		else if (type == ActionRegistry.class)
			return getActionRegistry();
//		else if (type == IContentOutlinePage.class)
//			return getOutlinePage();
//		else if (type == ZoomManager.class) return getDelegatingZoomManager();

		return super.getAdapter(type);
	}

	/**
	 * Returns the page for editing the compound tasks.
	 * 
	 * @return the page for editing the compound tasks
	 */
//	private CompoundTasksPage getCompoundTasksPage() {
//		return (CompoundTasksPage) getEditor(compoundTasksPageID);
//	}

	/**
	 * Returns the current active page.
	 * 
	 * @return the current active page or <code>null</code>
	 */
	private AbstractEditorPage getCurrentPage() {
		if (getActivePage() == -1) return null;

		return (AbstractEditorPage) getEditor(getActivePage());
	}

//	/** the delegating ZoomManager */
//	private DelegatingZoomManager	delegatingZoomManager;

//	/**
//	 * Returns the <code>DelegatingZoomManager</code> for this editor.
//	 * 
//	 * @return the <code>DelegatingZoomManager</code>
//	 */
//	protected DelegatingZoomManager getDelegatingZoomManager() {
//		if (null == delegatingZoomManager) {
//			delegatingZoomManager = new DelegatingZoomManager();
//			if (null != getCurrentPage() && null != getCurrentPage().getGraphicalViewer())
//				delegatingZoomManager.setCurrentZoomManager(getZoomManager(getCurrentPage().getGraphicalViewer()));
//		}
//
//		return delegatingZoomManager;
//	}

	/**
	 * Returns the <code>CommandStack</code> for this editor.
	 * 
	 * @return the <code>CommandStack</code>
	 */
	protected DelegatingCommandStack getDelegatingCommandStack() {
		if (null == delegatingCommandStack) {
			delegatingCommandStack = new DelegatingCommandStack();
			if (null != getCurrentPage())
				delegatingCommandStack.setCurrentCommandStack(getCurrentPage().getCommandStack());
		}

		return delegatingCommandStack;
	}

	/**
	 * Returns the <code>CommandStackListener</code> for the <code>DelegatingCommandStack</code>.
	 * 
	 * @return the <code>CommandStackListener</code>
	 */
	protected CommandStackListener getDelegatingCommandStackListener() {
		return delegatingCommandStackListener;
	}

	/**
	 * Returns the global command stack listener.
	 * 
	 * @return the <code>CommandStackListener</code>
	 */
	protected MultiPageCommandStackListener getMultiPageCommandStackListener() {
		if (null == multiPageCommandStackListener)
			multiPageCommandStackListener = new MultiPageCommandStackListener();
		return multiPageCommandStackListener;
	}

//	/**
//	 * Returns the overview for the outline view.
//	 * 
//	 * @return the overview
//	 */
//	protected WorkflowEditorOutlinePage getOutlinePage() {
//		if (null == outlinePage) {
//			outlinePage = new WorkflowEditorOutlinePage(this);
//			outlinePage.initialize(getCurrentPage());
//		}
//
//		return outlinePage;
//	}

//	/**
//	 * Returns the default <code>PaletteRoot</code> for this editor and all its pages.
//	 * 
//	 * @return the default <code>PaletteRoot</code>
//	 */
//	protected PaletteRoot getPaletteRoot() {
//		if (null == paletteRoot) {
//			// create root
//			paletteRoot = new WorkflowPaletteRoot();
//		}
//		return paletteRoot;
//	}

	/**
	 * Returns the undoable <code>PropertySheetPage</code> for this editor.
	 * 
	 * @return the undoable <code>PropertySheetPage</code>
	 */
	protected PropertySheetPage getPropertySheetPage() {
		if (null == undoablePropertySheetPage) {
			undoablePropertySheetPage = new PropertySheetPage();
			undoablePropertySheetPage.setRootEntry(GEFPlugin
					.createUndoablePropertySheetEntry(getDelegatingCommandStack()));
		}

		return undoablePropertySheetPage;
	}

	/**
	 * Returns the resource tracker instance
	 * 
	 * @return
	 */
	private ResourceTracker getResourceTracker() {
		if (resourceTracker == null) {
			resourceTracker = new ResourceTracker();

		}

		return resourceTracker;
	}

	/**
	 * Returns the selection listener.
	 * 
	 * @return the <code>ISelectionListener</code>
	 */
	protected ISelectionListener getSelectionListener() {
		return selectionListener;
	}

	/**
	 * Returns the selection syncronizer object. The synchronizer can be used to sync the selection of 2 or more
	 * EditPartViewers.
	 * 
	 * @return the syncrhonizer
	 */
	protected SelectionSynchronizer getSelectionSynchronizer() {
		if (synchronizer == null) synchronizer = new SelectionSynchronizer();
		return synchronizer;
	}

	/**
	 * Returns the shared KeyHandler that should be used for all viewers.
	 * 
	 * @return the shared KeyHandler
	 */
	protected KeyHandler getSharedKeyHandler() {
		if (sharedKeyHandler == null) {
			sharedKeyHandler = new KeyHandler();

			// configure common keys for all viewers
			sharedKeyHandler.put(KeyStroke.getPressed(SWT.DEL, 127, 0), getActionRegistry().getAction(
					GEFActionConstants.DELETE));
			sharedKeyHandler.put(KeyStroke.getPressed(SWT.F2, 0), getActionRegistry().getAction(
					GEFActionConstants.DIRECT_EDIT));
		}
		return sharedKeyHandler;
	}

//	/**
//	 * Returns the scenery used by this editor.
//	 * 
//	 * @return the scenery
//	 */
//	public Scenery getWorkflow() {
//		if (null==this.workflow) return new RootMapUnit();
//		else return this.workflow;
//	}

	/**
	 * Returns the page for editing the workflow.
	 * 
	 * @return the page for editing the workflow
	 */
	protected SceneryPage getSceneryPage() {
		return (SceneryPage) getEditor(workflowPageID);
	}

//	/**
//	 * Returns the zoom manager of the specified viewer.
//	 * 
//	 * @param viewer
//	 *          the viewer to get the zoom manager from
//	 * @return the zoom manager
//	 */
//	private ZoomManager getZoomManager(GraphicalViewer viewer) {
//		// get zoom manager from root edit part
//		RootEditPart rootEditPart = viewer.getRootEditPart();
//		ZoomManager zoomManager = null;
//		if (rootEditPart instanceof ScalableFreeformRootEditPart) {
//			zoomManager = ((ScalableFreeformRootEditPart) rootEditPart).getZoomManager();
//		} else if (rootEditPart instanceof ScalableRootEditPart) {
//			zoomManager = ((ScalableRootEditPart) rootEditPart).getZoomManager();
//		}
//		return zoomManager;
//	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#gotoMarker(org.eclipse.core.resources.IMarker)
	 */
	public void gotoMarker(IMarker marker) {
		// our policy: forward this to the active page only
		IEditorPart editor = getActiveEditor();
		((AbstractEditorPage) editor).gotoMarker(marker);
	}

	/**
	 * This method is called upon file selection with the matching file extension. File extension associations
	 * may be defined at the plug-in level or at the Workbench configuration Properties.
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#init(org.eclipse.ui.IEditorSite,org.eclipse.ui.IEditorInput)
	 */
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#isDirty()
	 */
	public boolean isDirty() {
		return isDirty;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	public boolean isSaveAsAllowed() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#pageChange(int)
	 */
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);

		// refresh content depending on current page
		currentPageChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#setActivePage(int)
	 */
//	protected void setActivePage(int pageIndex) {
//		super.setActivePage(pageIndex);
//
//		// refresh content depending on current page
//		currentPageChanged();
//	}

	/**
	 * Indicates that the current page has changed.
	 * <p>
	 * We update the DelegatingCommandStack, OutlineViewer and other things here.
	 */
	protected void currentPageChanged() {
		// update delegating command stack
//		getDelegatingCommandStack().setCurrentCommandStack(getCurrentPage().getCommandStack());

//		// update zoom actions
//		getDelegatingZoomManager().setCurrentZoomManager(getZoomManager(getCurrentPage().getGraphicalViewer()));

//		// reinitialize outline page
//		getOutlinePage().initialize(getCurrentPage());
	}

	/**
	 * Changes the dirty state.
	 * 
	 * @param dirty
	 */
	private void setDirty(boolean dirty) {
		if (isDirty != dirty) {
			isDirty = dirty;
			firePropertyChange(IEditorPart.PROP_DIRTY);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
	 */
//	protected void setInput(IEditorInput input) {
//		if (getEditorInput() != null) {
//			IFile file = ((FileEditorInput) getEditorInput()).getFile();
//			file.getWorkspace().removeResourceChangeListener(getResourceTracker());
//		}
//
//		super.setInput(input);
//
//		if (getEditorInput() != null) {
//			IFile file = ((FileEditorInput) getEditorInput()).getFile();
//			file.getWorkspace().addResourceChangeListener(getResourceTracker());
//			setTitle(file.getName());
//		}
//	}

	/**
	 * Updates the specified actions.
	 * 
	 * @param actionIds
	 *          the list of ids of actions to update
	 */
	public void updateActions(List actionIds) {
		for (Iterator ids = actionIds.iterator(); ids.hasNext();) {
			IAction action = getActionRegistry().getAction(ids.next());
			if (null != action && action instanceof UpdateAction) ((UpdateAction) action).update();

		}
	}

	/** the model manager */
	private WorkflowModelManager	modelManager;

	protected Scenery	scenery;

//	/**
//	 * Returns the workflow object from the specified file.
//	 * 
//	 * @param file
//	 * @return the workflow object from the specified file
//	 */
//	private Scenery create(IFile file) throws CoreException {
//		Scenery workflow = null;
//		modelManager = new WorkflowModelManager();
//
//		if (file.exists()) {
//			try {
//				modelManager.load(file.getFullPath());
//			} catch (Exception e) {
//				modelManager.createWorkflow(file.getFullPath());
//			}
//
//			workflow = modelManager.getModel();
//			if (null == workflow) { throw new CoreException(new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0,
//					"Error loading the worklow.", null)); }
//		}
//		return workflow;
//	}
//	private Scenery create(String location) throws CoreException {
//		Scenery workflow = null;
//		modelManager = new WorkflowModelManager();
//
////		if (location..exists()) {
//			try {
//				modelManager.load(location);
//			} catch (Exception e) {
//				modelManager.createWorkflow(location);
//			}
//
//			workflow = modelManager.getModel();
//			if (null == workflow) { throw new CoreException(new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0,
//					"Error loading the worklow.", null)); }
////		}
//		return workflow;
//	}

	/**
	 * Saves the workflow under the specified path.
	 * 
	 * @param workflow
	 * @param path
	 *          workspace relative path
	 * @param progressMonitor
	 */
	private void save(IFile file, IProgressMonitor progressMonitor) throws CoreException {

		if (null == progressMonitor) progressMonitor = new NullProgressMonitor();

		progressMonitor.beginTask("Saving " + file, 2);

		if (null == modelManager) {
			IStatus status = new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0,
					"No model manager found for saving the file.", null);
			throw new CoreException(status);
		}

		// save workflow to file
		try {
			modelManager.save(file.getFullPath());

			progressMonitor.worked(1);
			file.refreshLocal(IResource.DEPTH_ZERO, new SubProgressMonitor(progressMonitor, 1));
			progressMonitor.done();
		} catch (FileNotFoundException e) {
			IStatus status = new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0, "Error writing file.", e);
			throw new CoreException(status);
		} catch (IOException e) {
			IStatus status = new Status(IStatus.ERROR, HarpoonActivator.PLUGIN_ID, 0, "Error writing file.", e);
			throw new CoreException(status);
		}
	}

	public RootMapUnit getWorkflow() {
		// TODO Auto-generated method stub
		return this.workflow;
	}
	public Properties getProperties() {
		// TODO Auto-generated method stub
		return this.scenery.getProperties();
	}

}
