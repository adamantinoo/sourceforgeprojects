//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonTestSuite.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonTestSuite.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.tests;

// - IMPORT SECTION .........................................................................................
import harpoon.testcases.HarpoonAppTests;
import harpoon.testcases.HarpoonMapTests;

import java.util.Date;
import java.util.List;


import junit.framework.Test;
import junit.framework.TestSuite;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for net.sourceforge.harpoon.tests");
		//$JUnit-BEGIN$
		suite.addTestSuite(DisplacementTest.class);
		suite.addTestSuite(PolarPointTest.class);
		suite.addTestSuite(HarpoonAppTests.class);
		suite.addTestSuite(HarpoonMapTests.class);
		//$JUnit-END$
		return suite;
	}

}

// - UNUSED CODE ............................................................................................
