//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/UnitPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:50 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.4  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.3  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.2  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.1  2007-09-21 12:11:30  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public class UnitPropertyPage {
	// - F I E L D - S E C T I O N .............................................................................
	/** Reference to the model. But limited to the interface level of a Unit. */
	private Unit			unitModel;
	/** Group element that will contain all the unit properties to be presented on selection. */
	private Group			page;
	/**
	 * Reference to the link Control where all page composition will be linked. This element is located in the
	 * <code>SelectionView</code> control contents and it is received by a sentence.
	 */
	private Composite	container;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The
	 * <code>top</code> container allows this method to hang the new interface elements from the
	 * <code>SelectionView</code> at runtime. Other fields may be <code>null</code> and in those situation
	 * the code will generate a informational message.
	 */
	public UnitPropertyPage(final Composite top) {
		container = top;
	}

	// - G E T T E R S / S E T T E R S
	// TODO Change some of the getters to detect if some required fields are not filled in before building up
	// the
	// interface
	public void setModel(final Unit model) {
		unitModel = model;
	}

	/**
	 * Get the associated model element. At this class this model will be subclassed to the <code>Unit</code>
	 * interface. <br>
	 * If the model is null then we create a label with such a message.
	 */
	private Unit getModel() {
		return unitModel;
	}

	public Composite getContainer() {
		return container;
	}

	/** Changes the parent container where to hang the new SQL interface elements. */
	public void setContainer(final Composite parent) {
		container = parent;
	}

	/**
	 * Return the constructed page with the interface SWL components. If the <code>build()</code> has not been
	 * called then if will return a fake page with a single Label.
	 */
	public Group getPropertyPage() {
		if (null == page) {
			page = new Group(container, SWT.NONE);
			page.setText("Building ERROR");
		}
		return page;
	}

	// - P U B L I C S E C T I O N
	/**
	 * Add all the common elements that are visible for any unit. This depends on the information available on
	 * the model reference at this interface level.
	 */
	public void build() {
		// - Create the common contents of the page structure.
		final RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;

		// - Create the group and set the name
		page = new Group(container, SWT.NONE);
		page.setLayout(rowLayout);
		if (null == getModel()) {
			final Text location = new Text(page, SWT.BORDER);
			location.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
			location.setEditable(false);
			location.setText("Model information is not set for this instance (UnitPropertyPage)");
			page.setText("Building ERROR");
		} else {
			if (Unit.FRIEND_SIDE == getModel().getSide()) page.setForeground(HarpoonConstants.COLOR_FRIEND_NORMAL);
			if (Unit.FOE == getModel().getSide()) page.setForeground(HarpoonConstants.COLOR_FOE_NORMAL);
			if (Unit.NEUTRAL == getModel().getSide()) page.setForeground(HarpoonConstants.COLOR_NEUTRAL_NORMAL);
			page.setText(getModel().getName());

			// - Add model and location information
			// Text modelInfo = new Text(page, SWT.BORDER);
			// modelInfo.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			// modelInfo.setEditable(false);
			// modelInfo.setText(model.getModelInfo());
			final Text location = new Text(page, SWT.BORDER);
			location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			location.setEditable(false);
			location.setText(getModel().getDMSLatitude().toDisplay() + " - "
					+ getModel().getDMSLongitude().toDisplay());
		}
	}
}

// - UNUSED CODE ............................................................................................
