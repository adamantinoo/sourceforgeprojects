//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPartFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/HarpoonPartFactory.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:51 $
//  RELEASE:        $Revision: 1.14 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.13.2.6  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.13.2.5  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.13.2.4  2007-10-23 15:54:45  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.13.2.3  2007-10-16 14:47:14  ldiego
//    - [REQUIREMENT A0114.05] - Lesson 02.05 Reference points movement.
//
//    Revision 1.13.2.2  2007-10-15 14:23:21  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.13.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.13  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.12  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.11  2007-10-03 12:36:52  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.10  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.9  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.8  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.7  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sf.harpoon.model.units.AirportUnit;
import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.ReferencePoint;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.WarUnit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.TracePath;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class implements the EditPart factory to create any of the model controllers that are required to
 * implement the user interface intervention for the user and that are a requisite of the GEF model structure.
 * Once we connect a content to a Viewer it will start to call this factory to create the EditParts of the
 * Controller side of the GEF work model.<br>
 * The class has a global access element that is the root element of the class <code>RootMapPart</code> that
 * is referenced by any Part instance to access some global information and coordinate axis location.
 */
public class HarpoonPartFactory implements EditPartFactory {
	private static Logger	logger		= Logger.getLogger("net.sourceforge");
	// - Create and initialize an empty but runnable RootMapPart
	private RootMapPart		rootPart	= new RootMapPart();
	{
		rootPart.setModel(new RootMapUnit());
		rootPart.createEmptyFigure();
		rootPart.setRootPart(rootPart);
	}

	// - P U B L I C - S E C T I O N
	/**
	 * This method creates any of the EditParts in the application depending on the class of the element model
	 * received as the parameter. This is the main point where we assemble the model to the internal operating
	 * model used by GEF.<br>
	 * <br>
	 * The model equivalences are:
	 * <dl>
	 * <dt>WarUnit</dt>
	 * <dd>All movable game units, either air or sea are mapped to this kind of controller. They include the
	 * Movement Path and the ability to select target for firing their weapons.</dd>
	 * <dt>MovableUnit</dt>
	 * <dd>Any other unit that may move but has no capability to attack or it is a Neutral unit.</dd>
	 * <dt>AirportPart</dt>
	 * <dd>Fixed units that may launch other units.</dd>
	 * <dt>ReferencePart</dt>
	 * <dd>Reference point for movement paths or waypoints that define restricted areas or battle zones and
	 * borders.</dd>
	 * <dt>Wire</dt>
	 * <dd>Connection lines. This are not part of the model but the representation of the virtual connections
	 * between point.</dd>
	 * <dt>Trace</dt>
	 * <dd>Represent the point by where a unit has moved during a period of time.</dd>
	 * </dl>
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(final EditPart context, final Object model) {
		logger.log(Level.INFO, "Creating EditPart for model type: " + model.getClass().getSimpleName());
		GamePart part = null;

		// - If the EditPart requested is the root diagram EditPart then set it as the diagram root.
		if (model instanceof RootMapUnit) {
			part = new RootMapPart();
			rootPart = (RootMapPart) part;
			rootPart.setRootPart(rootPart);
		} else if (model instanceof WarUnit)
			part = new WarPart();
		else if (model instanceof MovableUnit)
			part = new MovablePart();
		else if (model instanceof AirportUnit)
			part = new AirportPart();
		else if (model instanceof ReferencePoint)
			part = new ReferencePart();
		else if (model instanceof TracePath)
			part = new TracePathPart();
		else if (model instanceof Wire) {
			final WirePart wire = new WirePart(model);
			wire.setModel(model);
			wire.setRootPart(rootPart);
			return wire;
		}

		// - Initialize the part setting the model and the root EditPart of the internal model.
		part.setModel(model);
		part.setRootPart(rootPart);
		return part;
	}

	// - CLASS IMPLEMENTATION .................................................................................
	class TracePathPart extends GamePart {

		private PointList	pathPoints;

		@Override
		protected void createEditPolicies() {
			// Note that the Connection is already added to the diagram and knows its Router.
			// installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, new SelectionEditPolicy() {
			//
			// @Override
			// protected void showSelection() {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// @Override
			// protected void showFocus() {
			// // TODO Auto-generated method stub
			// super.showFocus();
			// }
			//
			// @Override
			// protected void hideSelection() {
			// // TODO Auto-generated method stub
			//
			// }
			//
			// @Override
			// public EditPart getTargetEditPart(final Request request) {
			// // TODO Auto-generated method stub
			// return super.getTargetEditPart(request);
			// // return null;
			// }
			//
			// });
		}

		@Override
		protected IFigure createFigure() {
			final Polyline trace = new Polyline();
			return trace;
		}

		public void propertyChange(final PropertyChangeEvent evt) {
			final String prop = evt.getPropertyName();
			if (TracePath.ADDTRACE.equals(prop)) {
				// - Change the underlying figure with the new path model data.
				final Polyline trace = getTraceFigure();
				final DMSPoint dmsPoint = (DMSPoint) evt.getNewValue();
				final Point xyPoint = RootMapPart.dms2xy(this, dmsPoint);
				pathPoints.addPoint(xyPoint);
				trace.addPoint(xyPoint);
				refreshVisuals();
			}
		}

		@Override
		protected void refreshVisuals() {
			super.refreshVisuals();
			// - The references to the model and figure objects.
			final Polyline fig = getTraceFigure();
			final TracePath model = (TracePath) getModel();

			// - Initialize the figure colors and state based on the model data.
			fig.setForegroundColor(ColorConstants.white);

			// - Initialize the points by calculating the model data point location with current map references.
			pathPoints = new PointList();
			final Iterator<DMSPoint> it = model.getTracePoints().iterator();
			while (it.hasNext()) {
				// - Calculate XY coordinates for this point.
				final DMSPoint dmsPoint = it.next();
				final Point xyPoint = RootMapPart.dms2xy(this, dmsPoint);
				pathPoints.addPoint(xyPoint);
			}
			fig.setPoints(pathPoints);
			fig.revalidate();
		}

		protected TracePath getCastedModel() {
			return (TracePath) getModel();
		}

		protected Polyline getTraceFigure() {
			return (Polyline) getFigure();
		}
	}
}
// - UNUSED CODE ............................................................................................
