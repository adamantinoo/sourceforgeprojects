//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: AirportFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/AirportFigure.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:49 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

// - CLASS IMPLEMENTATION ...................................................................................
public class AirportFigure extends UnitFigure {
	private static Logger							logger			= Logger.getLogger("net.sourceforge");

	private static final int					MARGIN			= 2;
	private static final int					CHAR_WIDTH	= 6;
	private static final int					CHAR_HEIGHT	= 13;

	/** Label to store the name of this airport. */
	protected final UnitDisplayLabel	nameLabel		= new UnitDisplayLabel("Airport");

	/**
	 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a square
	 * box with some lines inside with a name label at the right center.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center of the icon.
	 */
	public void init() {
		// - Create the complex internal parts of this figure.
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = MARGIN;
		grid.marginHeight = MARGIN;
		grid.marginWidth = MARGIN;
		grid.verticalSpacing = 0;
		setLayoutManager(grid);
		this.add(iconic);
		createRightSide();

		// - Calculate size and bounds
		this.setSize(getPreferredSize(-1, -1));
	}

	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	public Point getHotSpot() {
		return iconic.getHotSpot();
	}

	private void createRightSide() {
		nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		nameLabel.setBorder(new LineBorder(1));
		this.add(nameLabel);
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = iconicSize.width + MARGIN + rightSize.width;
		fullSize.height = MARGIN + Math.max(iconicSize.height, rightSize.height) + MARGIN;
		return fullSize;
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * Sets the name of the label. The size of the parent figure has to be adjusted, but tet if the real soize
	 * of the label has also to be adjusted.
	 */
	public void setName(String name) {
		nameLabel.setText(name);
		invalidate();
		this.setSize(getPreferredSize(-1, -1));
	}

	// - P U B L I C - S E C T I O N
	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("AirportFigure(" + nameLabel.getText());
		buffer.append(",");
		buffer.append("size:" + getPreferredSize());
		buffer.append(",");
		buffer.append("bounds:" + getBounds());
		return buffer.toString();
	}

	// - P R I V A T E - S E C T I O N
	private Dimension getRightSideSize() {
		final int maxChars = nameLabel.getText().length();
		final Dimension rightLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH, CHAR_HEIGHT);
		return rightLabelSize;
	}
}

// - CLASS IMPLEMENTATION.................................................................................
class UnitDisplayLabel extends Label {
	private static final int	CHAR_WIDTH	= 6;
	private static final int	CHAR_HEIGHT	= 12;

	public UnitDisplayLabel(final String text) {
		setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		setLabelAlignment(PositionConstants.RIGHT);
		setText(text);
	}

	@Override
	public void setText(final String newText) {
		super.setText(newText);
		final Dimension size = new Dimension((newText.length() + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT);
		this.setSize(size);
		this.setPreferredSize(size);
	}
}
// - UNUSED CODE ............................................................................................
