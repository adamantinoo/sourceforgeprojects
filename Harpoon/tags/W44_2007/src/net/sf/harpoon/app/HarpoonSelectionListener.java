//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonSelectionListener.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonSelectionListener.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:35:04 $
//  RELEASE:        $Revision: 1.9 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.8.2.1  2007-10-11 07:52:30  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.8  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.7  2007-10-02 09:04:25  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.6  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.5  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.4  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-17 15:11:39  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.MultiPageEditorPart;

import net.sourceforge.harpoon.parts.GamePart;
import net.sourceforge.harpoon.parts.ReferencePart;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.rcp.harpoon.views.SelectionView;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This is the selection listener used to maintain the selection made on the editor reflected on the <i>Selection Data</i>
 * view. The methods receive a selection from the editor and it will filter the selection to just the selectable items
 * that may be reflected on the view. <br>
 * There are no filtering operations on this release, sensor information has been removed from the <i>Controller</i>
 * and enemy units can be selected and show identification information.
 */
public class HarpoonSelectionListener implements ISelectionListener {
	/**
	 * Reference to the editor instance that contains the model and the viewer. From this editor instance one can access
	 * any <i>Model</i> <i>View</i> or <i>Controller</i> element on the scenery.
	 */
	private final MultiPageEditorPart	editor;
	/**
	 * Catched reference to the selection view where selection changes should de synchronized with this <i>Editor</i>
	 * selection.
	 */
	private SelectionView							selectionView;

	// - C O N S T R U C T O R S
	/**
	 * Creates a new <i>Selection Listener</i> and receives the editor reference to keep it for selection backtracking.
	 * This allows to update the editor actions depending on the selection.
	 */
	public HarpoonSelectionListener(MultiPageEditorPart editor) {
		this.editor = editor;
	}

	// - P U B L I C S E C T I O N
	/**
	 * This event is fired anytime the selection is modified. <br>
	 * The parameters received are the new selection list on the <code>selection</code> parameter and the
	 * <code>EditorPart</code> that points to the <i>SceneryEditor</i> on the <code>part</code> parameter.<br>
	 * <br>
	 * From this selection I have to filter out the non selectable elements that have no influence with the selection
	 * presentation.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// [01]
		if (selection instanceof StructuredSelection) {
			final StructuredSelection sel = (StructuredSelection) selection;
			if (!sel.isEmpty()) {
				// - Get the root part from any selection element.
				final GamePart selectedPart = (GamePart) sel.iterator().next();
				final RootMapPart rootPart = selectedPart.getRootPart();
				// - Clean up the model to the base model list. But only is selection is not a path point
				boolean clean = true;
				if (selectedPart instanceof ReferencePart) clean = false;
				if (selectedPart instanceof ConnectionEditPart) clean = false;
				if (selectedPart instanceof RootMapPart) clean = false;
				if (clean) {
					try {
						// final List<Object> chi = rootPart.getModelChildren();
						rootPart.refresh();
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}
			getSelectionView().updateSelection(sel, part);
		}
	}

	// - P R I V A T E S E C T I O N
	/**
	 * Accesses the <code>SelectionView</code> singleton and stores a copy in the reference cache. Selections received
	 * will be sent to this view for synchronization and presentation of aditional selection data.
	 * 
	 * @return the cached reference to the <code>SelectionView</code> singleton.
	 */
	private SelectionView getSelectionView() {
		if (null == selectionView) {
			selectionView = SelectionView.getSingleton();
		}
		return selectionView;
	}
}

class SensorPart extends AbstractGraphicalEditPart {

	@Override
	protected IFigure createFigure() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}
}

// - UNUSED CODE ............................................................................................
// [01]
// // TODO Review this implementation and optimize the generation of a new selection. It is possible that
// // better performance can be obtained by filtering this at the next call level.
// // StructuredSelection sel = (StructuredSelection) selection;
// // StructuredSelection filteredSel = new StructuredSelection();
// StructuredSelection filteredSel2 = new StructuredSelection();
// final Object[] filteredSel = new AbstractGraphicalEditPart[((StructuredSelection) selection).size()];
// // // int size = sel.size();
// if (!selection.isEmpty()) {
// final Iterator<AbstractGraphicalEditPart> it = ((StructuredSelection) selection).iterator();
// // selectionContent = new Hashtable();
// int index = 0;
// while (it.hasNext()) {
// final AbstractGraphicalEditPart element = it.next();
// if (element instanceof SensorPart)
// continue;
// else
// filteredSel[index++] = element; // - Copy to the new selection to pass away.
//
// // Unit model = ((Unit) element.getModel());
// // String name = model.toString();
// // // - Add the selected element to the selection
// // selectionContent.put(name, element);
// // // int index = 1;
// }
// filteredSel2 = new StructuredSelection(filteredSel);
//
// }
// if (part instanceof SceneryEditor) {
// final SceneryEditor sceneryEditor = (SceneryEditor) part;
// // sceneryEditor.updateActions(sceneryEditor.getEditActions());
// }
