//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonLoop.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonLoop.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:35:04 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.4  2007-10-31 14:44:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.1.2.3  2007-10-24 16:45:43  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.1.2.2  2007-10-23 15:55:17  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.1.2.1  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.AirportUnit;
import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.WarUnit;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.rcp.harpoon.editors.SceneryEditor;
import net.sourceforge.rcp.harpoon.editors.SceneryPage;
import net.sourceforge.rcp.harpoon.model.Scenery;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonLoop implements Runnable {
	private static Logger					logger			= Logger.getLogger("net.sourceforge.rcp.harpoon.app");
	static {
		logger.setLevel(Level.ALL);
	}
	private static final String		NOT_CACHED	= "NOT_CACHED";
	private static final String		UPDATED			= "UPDATED";
	private static final String		MODIFIED		= "MODIFIED";

	private final String					name;																															// Process name
	// protected Scenery referenceScenery; // Application display artifact. Possible not needed
	protected Scenery							referenceScenery;																									// Referenced scenery
	// where to retrieve
	// unit data
	protected String							cacheState	= NOT_CACHED;
	protected Vector<Unit>				friendUnits;																												// Cache list of
	// friendly units
	protected Vector<Unit>				enemyUnits;																												// Cache list of any
	// other unit that it
	// is detectable
	protected Vector<MovableUnit>	movableUnits;																											// Cache list of any
	// other unit that it
	// is detectable
	protected HashMap<Unit, Unit>	fading;																														// Cache list of any

	// other unit that it
	// is detectable

	// - C O N S T R U C T O R S
	public HarpoonLoop(Scenery sce, String name) {
		referenceScenery = sce;
		this.name = name;
	}

	// - P U B L I C - S E C T I O N
	/**
	 * Perform a single pass on the Enemy Detection Block and activate any enemies detected by the sensors that are
	 * active. Actions to be performed are:
	 * <ul>
	 * <li>Scan the list of units and get separate lists for friend War units, and enemy or other units.</li>
	 * <li>For each other unit, advance the time since it was detected, but only if it was detected.</li>
	 * <li>If this unit has been detected since at least 60 seconds, the move its state in the detection ladder.</li>
	 * <li>For any friend unit, get the sensors that have activated.</li>
	 * <li>If any unit has activated Radar sensors, scan the list of units for War units of Surface or Air type then are
	 * in range and then increment their detection state in the detection ladder.</li>
	 * <li>If any unit has the ECM sensor active, detect any unit that is Radio detectable (usually Airports).</li>
	 * </ul>
	 * <br>
	 * <br>
	 * <br>
	 * This loop is fired every 15 seconds on the exact minute seconds of 0, 15, 30 and 45. The actions to be performed on
	 * this periodic task loop are next:
	 * <ul>
	 * <li>Get the model units and classify them is the unit caches are empty or we have received a model change event.</li>
	 * <li>Match friend units with active sensors against other units to see if some detection event is generated. Update
	 * enemy uni state is detection is generated.</li>
	 * <li>If enemy units have moved to a undetected state, calculate the teoretical position area where the unit may
	 * have moved and update the model with this information.</li>
	 * <li>For each of the visible units calculate the new position from the movement algorithm. Compare this position
	 * with previous position and activate higlighting if the unit has moved.</li>
	 * </ul>
	 * 
	 */
	public void run() {
		logger.info(">>> Entering Processing Enemy Detection Loop");

		// - SECTION 01. Check if unit are cached. Otherwise separate and cache them.
		updateUnitData();

		// - For every enemy unit check if it was detected.
		// updateEnemyDetection();

		// - For every friend unit check if it detects an enemy with any of its sensors
		detectionPhase();

		// - For every movable unit calculate the new position and register all units that visially move.
		movementPhase();
		final int dum = 1;
		System.gc();

		// try {
		// Thread.sleep(2 * 1000);
		// } catch (final InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		logger.info("<<< Exiting Processing Enemy Detection Loop");
	}

	/**
	 * Checks the state of the unit cache. If some event has changed the number of elements on the model then we
	 * invalidate the caches and reprocess them again.
	 */
	protected void updateUnitData() {
		if (UPDATED.equals(cacheState)) return;
		// - Get the list of units and enemy units.
		final Iterator<Unit> it = referenceScenery.getModel().getChildren().iterator();
		friendUnits = new Vector<Unit>();
		enemyUnits = new Vector<Unit>();
		movableUnits = new Vector<MovableUnit>();
		while (it.hasNext()) {
			final Unit unit = it.next();
			// if (unit instanceof WarUnit) {
			// final WarUnit war = (WarUnit) unit;
			// }
			// if (unit instanceof Unit) {
			// final Unit thisUnit = (Unit) unit;
			if (Unit.FRIEND_SIDE == unit.getSide())
				friendUnits.add(unit);
			else
				enemyUnits.add(unit);
			if (unit instanceof MovableUnit) movableUnits.add((MovableUnit) unit);
		}// end while (it.hasNext())
		cacheState = UPDATED;
	}

	protected void updateEnemyDetection() {
		final Iterator<Unit> undetectedIt = enemyUnits.iterator();
		while (undetectedIt.hasNext()) {
			final Unit unit = undetectedIt.next();
			final String state = unit.getDetectState();
			if (HarpoonConstants.NOT_DETECTED_STATE.equals(state)) {
				// - Add the unit to the list of units to be watched for fade out of the display
				fading.put(unit, unit);
			} else {
				// - Unit is in the detection phase ladder.
				logger.info("Enemy: " + unit.toString() + " goes undetected to " + state);
				unit.degradeDetection();
			}
		}
	}

	protected void detectionPhase() {
		logger.info(">>> Entering detection phase");
		final Iterator<Unit> friendIt = friendUnits.iterator();
		while (friendIt.hasNext()) {
			final Unit unit = friendIt.next();
			// - Check it is a war unit and get the list of sensors active for this unit and add it to the list.
			if (unit instanceof WarUnit) {
				final WarUnit war = (WarUnit) unit;
				logger.info("-- Detecting with unit " + war.toString());
				final SensorsModel sensors = war.getSensorInformation();
				if (sensors.getRadarState()) radarDetectionPhase(war);
				if (sensors.getRadioState()) radarDetectionPhase(war);
			}
		}// end while (friendIt.hasNext())
	}

	/** Radar detection tries to detect and identify Movable Surface and Movable Air units. */
	protected void radarDetectionPhase(WarUnit war) {
		logger.info(">>> Entering RADAR detection phase");
		// - Detect any flying or surface unit with the radar
		final Iterator<Unit> enemyIt = enemyUnits.iterator();
		while (enemyIt.hasNext()) {
			final Unit unit = enemyIt.next();
			// [01]
			if (unit instanceof MovableUnit) {
				final MovableUnit detectableUnit = (MovableUnit) unit;
				final String type = detectableUnit.getUnitType();
				if ((HarpoonConstants.UNIT_SURFACE.equals(type)) || (HarpoonConstants.UNIT_AIR.equals(type))) {
					// - Get the locations of both elements.
					final DMSPoint targetLocation = new DMSPoint(detectableUnit.getDMSLatitude(), detectableUnit
							.getDMSLongitude());
					final DMSPoint sourceLocation = new DMSPoint(war.getDMSLatitude(), war.getDMSLongitude());
					final DMSPoint targetVector = sourceLocation.offset(targetLocation);
					final int range = war.getSensorRange(HarpoonConstants.RADAR_TYPE);
					if (targetVector.getModule() < range) {
						// - This enemy is detected
						// logger.info("Other unit: " + detectableUnit.toString() + " is being detected");
						detectableUnit.upgradeDetection();
					}
				}
			}
		}
	}

	/**
	 * This phase iterates over all Movable units to move the location over the movement path it it exists. In cases where
	 * the path has completed, the unit has to continue moving on the same direction indefinitely.<br>
	 * The new location of the unit is calculated by the space run from the starting point that is the first movement path
	 * point plus the delta movement that is calculated as the current speed by the elapsed time since the last movement
	 * adjustment.<br>
	 * A movement adjustment happens when the user changes the movement path or when there is a change on the speed. At
	 * that time point the current location is recalculated and it becomes the initial movement point for next movement
	 * calculations.
	 */
	protected void movementPhase() {
		logger.info(">>> Entering MOVEMENT phase");
		final Iterator<MovableUnit> it = movableUnits.iterator();
		while (it.hasNext()) {
			final MovableUnit unit = it.next();
			// - Get start point, elapsed time and movement direction.
			// ReferencePoint start = unit.getStartMovementPoint();
			final DMSPoint startLocation = unit.getStartMovementPoint();
			final double elapsed = unit.elapsedMoveTime();
			if (elapsed <= 0.0) continue;
			final int direction = unit.getBearing();

			// - Adjust the angle to the base trigonometric coordinates.
			final double alpha = StrictMath.toRadians((360 - (direction - 90)) % 360);
			// - Calculate the space with the speed and the elapsed time. Use also the conversion to XY
			final SceneryEditor editor = (SceneryEditor) referenceScenery.getEditor();
			final SceneryPage page = editor.getMapPage();
			final GraphicalViewer viewer = page.getGraphicalViewer();
			final EditPart root = viewer.getContents();
			double space = 0.0;
			if (root instanceof RootMapPart) {
				final RootMapPart rootPart1 = (RootMapPart) root;
				final double speed = new Double(unit.getSpeed()).doubleValue();
				final Point spoint = RootMapPart.dms2xy(rootPart1, new DMSPoint(DMSCoordinate.fromSeconds(new Double(
						speed).longValue(), DMSCoordinate.LATITUDE), DMSCoordinate.fromSeconds(new Double(speed)
						.longValue(), DMSCoordinate.LONGITUDE)));
				space = StrictMath.abs(spoint.x * (elapsed / (60.0 * 60.0)));
			}
			// - Calculate movement projection on the latitude and longitude axis.
			final double latDelta = space * StrictMath.sin(alpha);
			final double lonDelta = space * StrictMath.cos(alpha);

			// - Calculate the new point from the start point.
			final DMSPoint lastLocation = unit.getLocation();
			final DMSPoint currentLocation = startLocation.translate(latDelta, lonDelta);

			// - Calculate the new XY location and compare it with the last location to check if moved
			// SceneryEditor editor = (SceneryEditor) referenceScenery.getEditor();
			// SceneryPage page = editor.getMapPage();
			// GraphicalViewer viewer = page.getGraphicalViewer();
			// EditPart root = viewer.getContents();
			if (root instanceof RootMapPart) {
				final RootMapPart rootPart = (RootMapPart) root;
				final Point prevXY = RootMapPart.dms2xy(rootPart, lastLocation);
				final Point newXY = RootMapPart.dms2xy(rootPart, currentLocation);
				if (!prevXY.equals(newXY)) {
					// - The object has moved on the presentation map
					logger.info("Unit " + unit.toString() + " has moved to new coordinates " + newXY.toString());
					// TODO Highlight the unit for 5 seconds
					// TODO Register previous point into trace line and change unit coordinates.
					unit.addTrace(currentLocation);
					unit.setLatitude(currentLocation.getDMSLatitude());
					unit.setLongitude(currentLocation.getDMSLongitude());
				}
			}
		}
	}

	/**
	 * Radio emission detection detects any unit that is using the radio. Airports and neutral Air units are automatically
	 * detected by this sensor.
	 * 
	 * @param war
	 *          friendly unit that is using the detector
	 */
	protected void radioEmissionDetectionPhase(WarUnit war) {
		logger.info(">>> Entering RARIO detection phase");
		// - Detect any airport or Neutral Air unit
		final Iterator<Unit> enemyIt = enemyUnits.iterator();
		while (enemyIt.hasNext()) {
			final Unit unit = enemyIt.next();
			if (unit instanceof AirportUnit) unit.upgradeDetection();
			if (unit instanceof MovableUnit) {
				final MovableUnit detectableUnit = (MovableUnit) unit;
				final String type = detectableUnit.getUnitType();
				if (HarpoonConstants.UNIT_AIR.equals(type)) {
					detectableUnit.upgradeDetection();
				}
			}
		}
	}

	@Override
	public String toString() {
		return name + super.toString();
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// if (unit instanceof WarUnit) {
// final WarUnit enemy = (WarUnit) unit;
// final String type = enemy.getUnitType();
// if ((HarpoonConstants.SURFACE_UNIT.equals(type)) || (HarpoonConstants.AIR_UNIT.equals(type))) {
// // - Get the locations of both elements.
// final DMSPoint epoint = new DMSPoint(enemy.getDMSLatitude(), enemy.getDMSLongitude());
// final DMSPoint spoint = new DMSPoint(war.getDMSLatitude(), war.getDMSLongitude());
// final DMSVector enemyVector = new DMSVector(spoint, epoint);
// final int range = war.getRadarRange();
// if (enemyVector.getModule() < range) {
// // - This enemy is detected
// // HarpoonLogger.info("Enemy detected");
// HarpoonLogger.info("Enemy: " + enemy.toString() + " is being detected");
// enemy.fireDetection();
// }
// }
// }// end if (unit instanceof WarUnit)
