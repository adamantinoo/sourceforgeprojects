//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WireEndPoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/WireEndPoint.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:38 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.1.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//

package net.sourceforge.harpoon.model;

import java.util.List;

import net.sf.harpoon.model.units.Wire;

// - CLASS IMPLEMENTATION ...................................................................................
public interface WireEndPoint {

	/** Property ID to use when the list of outgoing connections is modified. */
	public static final String	SOURCE_CONNECTIONS_PROP	= "WireEndPoint.SOURCE_CONNECTIONS_PROP";
	/** Property ID to use when the list of incoming connections is modified. */
	public static final String	TARGET_CONNECTIONS_PROP	= "WireEndPoint.TARGET_CONNECTIONS_PROP";

	public MovementPath getOwner();

	public int getParentSelected();

	/**
	 * Add an incoming or outgoing connection to this shape.
	 * 
	 * @param wire
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the connection is null or has not distinct endpoints
	 */
	//	public void addConnection(final Wire wire);
	public void setSource(Wire wire);

	public void setTarget(Wire wire);

	/**
	 * Return a List of outgoing Connections.
	 */
	public abstract List<Wire> getSourceConnections();

	/**
	 * Return a List of incoming Connections.
	 */
	public abstract List<Wire> getTargetConnections();

	/**
	 * Remove an incoming or outgoing connection from this shape.
	 * 
	 * @param conn
	 *          a non-null connection instance
	 * @throws IllegalArgumentException
	 *           if the parameter is null
	 */
	public void removeConnection(final Wire conn);

}
// - UNUSED CODE ............................................................................................
