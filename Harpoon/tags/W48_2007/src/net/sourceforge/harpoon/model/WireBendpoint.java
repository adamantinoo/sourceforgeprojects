//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WireBendpoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/WireBendpoint.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:36 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
/***********************************************************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others. All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 **********************************************************************************************************************/
// LOG:
// $Log: not supported by cvs2svn $
// Revision 1.4 2007-11-23 11:33:38 ldiego
// - TASK Detection method is changed to a detection control unit by unit.
// - DEFECT After saving a game the units return to the original position.
// - DEFECT Changes in the movement path points is not reflected.
// - DEFECT Enemies have not to show path.
// - DEFECT Friend unit traces are not shown.
//
package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.io.Serializable;

import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;

// - CLASS IMPLEMENTATION ...................................................................................
public class WireBendpoint implements Serializable, Bendpoint {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N

	private static final long	serialVersionUID	= 4369662038310479984L;
	private float							weight						= 0.5f;
	private Dimension					d1;
	private Dimension					d2;

	public WireBendpoint() {
	}

	public Dimension getFirstRelativeDimension() {
		return d1;
	}

	public Point getLocation() {
		return null;
	}

	public Dimension getSecondRelativeDimension() {
		return d2;
	}

	public float getWeight() {
		return weight;
	}

	public void setRelativeDimensions(final Dimension dim1, final Dimension dim2) {
		d1 = dim1;
		d2 = dim2;
	}

	public void setWeight(final float w) {
		weight = w;
	}

}
// - UNUSED CODE ............................................................................................
