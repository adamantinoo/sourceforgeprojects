//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: MovablePropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/MovablePropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:36 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.4  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.3.2.2  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.3.2.1  2007-10-24 16:45:10  ldiego
//    - [REQUIREMENT A0152.01] - Save option for the game running in course.
//    - TASK Refactoring of code. Phase 3.
//
//    Revision 1.3  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.2  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.1  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sf.harpoon.editors.SceneryPage;
import net.sf.harpoon.model.units.MovableUnit;
import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.model.ExtendedData;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class MovablePropertyPage extends UnitPropertyPage {
	public static final String	REGISTERPATH_ACTION	= "REGISTERPATH";
	private static Logger				logger							= Logger.getLogger("net.sourceforge");
	// - F I E L D - S E C T I O N .............................................................................
	/** Reference to the model. But limited to the interface level of a Unit. */
	private MovableUnit					movableModel;
	private Composite						movementControl;
	private Spinner							speedSet;
	private Spinner							directionSet;
	private Spinner							altitudeSet;
	private Spinner							submarineSet;
	private boolean							activatePathChange	= false;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	/**
	 * During construction receive the mandatory parent where to link the SWT interface elements. The <code>top</code>
	 * container allows this method to hang the new interface elements from the <code>SelectionView</code> at runtime.
	 * Other fields may be <code>null</code> and in those situation the code will generate a informational message.
	 */
	public MovablePropertyPage(final Composite top) {
		super(top);
	}

	// - G E T T E R S / S E T T E R S
	public void setModel(final MovableUnit model) {
		movableModel = model;
		super.setModel(model);
	}

	// - P U B L I C - S E C T I O N
	public void activatePathChange() {
		activatePathChange = true;
	}

	// /**
	// * Get the associated model element. At this class this model will be subclassed to the
	// * <code>MovableUnit</code> interface. <br>
	// * Model can not be null at this level and a new empty unit is created if this is the case.
	// */
	// private Unit getModel() {
	// if(null==movableModel)this.movableModel=new MovableUnit();
	// return movableModel;
	// }

	// - O V E R R I D E - S E C T I O N
	/**
	 * Add the interface items that are specific for MovableParts. This include the movement controls.
	 * 
	 * @see net.sourceforge.harpoon.pages.UnitPropertyPage#build()
	 */
	@Override
	public void build() {
		// - Parent will create the base element and then add all data for other levels in the inheritance chain.
		super.build();

		// - This section is only visible for FRIEND units.
		final String side = movableModel.getSide();
		if (Unit.FRIEND_SIDE.equals(side)) {
			// - Create the speed and direction control
			createMovementControl(getPropertyPage());
			// - Additional movement information for special units like aircrafts and submarines
			createAdditional(getPropertyPage());

			// - Set the control values from the model data.
			directionSet.setSelection(movableModel.getBearing());
			speedSet.setSelection(movableModel.getSpeed());
			speedSet.addModifyListener(new ModifyListener() {
				public void modifyText(final ModifyEvent e) {
					logger.info("New speed value: " + speedSet.getSelection() + " for Unit:" + movableModel.toString());
					movableModel.setSpeed(speedSet.getSelection());
				}
			});

			if (activatePathChange) {
				createMovementChange(getPropertyPage());
			}
		}
	}

	/**
	 * This method initializes movementControl
	 * 
	 */
	private void createMovementChange(Group parentPage) {
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		final Group pathChange = new Group(parentPage, SWT.NONE);
		pathChange.setText("Movement Section");
		pathChange.setLayout(gridLayout);
		final Button clear = new Button(pathChange, SWT.NONE);
		clear.setText("Clear");
		clear.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// - Clear the current movement path for this unit.
				movableModel.setMovementPath(null);
			}
		});
		final Button create = new Button(pathChange, SWT.NONE);
		create.setText("Create/Add");
		create.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				// - Register the current path to reference inside the creation.
				movableModel.fireStructureChange(REGISTERPATH_ACTION, null, movableModel.getMovementPath());
				// - Activate the tool to create new reference points
				SceneryPage.setTool(SceneryPage.MOVEMENTPATHCREATION_TOOL);
			}
		});
	}

	// - P R O T E C T E D - S E C T I O N
	protected void createMovementControl(final Group group) {
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = 2;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		movementControl = new Composite(group, SWT.NONE);
		movementControl.setLayout(grid);

		// - Create the elements that are declared for MovableUnits
		final Text speedLabel = new Text(movementControl, SWT.NONE);
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedSet = new Spinner(movementControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		speedSet.setMaximum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_SPEEDMAX)).intValue());
		speedSet.setMinimum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_SPEEDMIN)).intValue());
		speedSet.setIncrement(new Integer(movableModel.getExtendedData(ExtendedData.XDT_SPEEDINCREMENT))
				.intValue());

		final Text directionLabel = new Text(movementControl, SWT.NONE);
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionLabel.setEditable(false);
		directionLabel.setText("Bearing:");
		directionSet = new Spinner(movementControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setEnabled(false);
		// directionSet.setMaximum(360);
		// directionSet.setMinimum(0);
	}

	private void createAdditional(final Group page) {
		if (movableModel.isAirBorne()) {
			final Text altitudeLabel = new Text(movementControl, SWT.NONE);
			altitudeLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			altitudeLabel.setEditable(false);
			altitudeLabel.setText("Altitude:");
			altitudeSet = new Spinner(movementControl, SWT.NONE);
			altitudeSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
			altitudeSet.setMaximum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_ALTITUDEMAX))
					.intValue());
			altitudeSet.setMinimum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_ALTITUDEMIN))
					.intValue());
			altitudeSet.setIncrement(new Integer(movableModel.getExtendedData(ExtendedData.XDT_ALTITUDEINCREMENT))
					.intValue());

			altitudeSet.setSelection(new Integer(movableModel.getExtendedData(ExtendedData.ALTITUDE)).intValue());
			// altitudeSet.addModifyListener(new ModifyListener() {
			// public void modifyText(ModifyEvent e) {
			// logger.info("New altitude value: " + altitudeSet.getSelection()+" for
			// Unit:"+movableModel.toString());
			// movableModel.setSpeed(speedSet.getSelection());
			// }
			// });
		}
		if (movableModel.isSubmarine()) {
			final Text submarineLabel = new Text(movementControl, SWT.NONE);
			submarineLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
			submarineLabel.setEditable(false);
			submarineLabel.setText("Deep:");
			submarineSet = new Spinner(movementControl, SWT.NONE);
			submarineSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
			submarineSet.setMaximum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_DEEPMAX)).intValue());
			submarineSet.setMinimum(new Integer(movableModel.getExtendedData(ExtendedData.XDT_DEEPMIN)).intValue());
			submarineSet.setIncrement(new Integer(movableModel.getExtendedData(ExtendedData.XDT_DEEPINCREMENT))
					.intValue());

			submarineSet.setSelection(new Integer(movableModel.getExtendedData(ExtendedData.DEEP)).intValue());
		}
	}
}

// - UNUSED CODE ............................................................................................
