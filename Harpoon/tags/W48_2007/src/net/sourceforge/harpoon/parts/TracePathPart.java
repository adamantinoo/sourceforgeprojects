//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: TracePathPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/TracePathPart.java,v $
//  LAST UPDATE:    $Date: 2007-11-16 10:50:18 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-11-07 16:28:44  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Iterator;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Polyline;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

import net.sf.harpoon.model.units.Unit;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.TracePath;

// - CLASS IMPLEMENTATION ...................................................................................
public class TracePathPart extends GamePart {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	private PointList	pathPoints;

	@Override
	protected void createEditPolicies() {
		//[01]
	}

	@Override
	protected IFigure createFigure() {
		final Polyline trace = new Polyline();
		return trace;
	}

	public void propertyChange(final PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (TracePath.ADDTRACE.equals(prop)) {
			// - Change the underlying figure with the new path model data.
			final Polyline trace = getTraceFigure();
			final DMSPoint dmsPoint = (DMSPoint) evt.getNewValue();
			final Point xyPoint = RootMapPart.dms2xy(this, dmsPoint);
			pathPoints.addPoint(xyPoint);
			trace.addPoint(xyPoint);
			refreshVisuals();
		}
		if (TracePath.VISIBILITY_PROP.equals(prop)) {
			String visibility = this.getCastedModel().getVisibilityState();
			if (Unit.NOT_VISIBLE.equals(visibility))
				this.getFigure().setVisible(false);
			if (Unit.IDENTIFICATION_VISIBLE.equals(visibility))
				this.getFigure().setVisible(true);
			if (Unit.FULL_VISIBLE.equals(visibility))
				this.getFigure().setVisible(true);
		}
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		final Polyline fig = getTraceFigure();
		final TracePath model = (TracePath) getModel();

		// - Initialize the figure colors and state based on the model data.
		fig.setForegroundColor(ColorConstants.white);

		// - Initialize the points by calculating the model data point location with current map references.
		pathPoints = new PointList();
		final Iterator<DMSPoint> it = model.getTracePoints().iterator();
		while (it.hasNext()) {
			// - Calculate XY coordinates for this point.
			final DMSPoint dmsPoint = it.next();
			final Point xyPoint = RootMapPart.dms2xy(this, dmsPoint);
			pathPoints.addPoint(xyPoint);
		}
		fig.setPoints(pathPoints);
		fig.revalidate();
	}

	protected TracePath getCastedModel() {
		return (TracePath) getModel();
	}

	protected Polyline getTraceFigure() {
		return (Polyline) getFigure();
	}
}
// - UNUSED CODE ............................................................................................
//[01]
// Note that the Connection is already added to the diagram and knows its Router.
// installEditPolicy(EditPolicy.SELECTION_FEEDBACK_ROLE, new SelectionEditPolicy() {
//
// @Override
// protected void showSelection() {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// protected void showFocus() {
// // TODO Auto-generated method stub
// super.showFocus();
// }
//
// @Override
// protected void hideSelection() {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public EditPart getTargetEditPart(final Request request) {
// // TODO Auto-generated method stub
// return super.getTargetEditPart(request);
// // return null;
// }
//
// });
