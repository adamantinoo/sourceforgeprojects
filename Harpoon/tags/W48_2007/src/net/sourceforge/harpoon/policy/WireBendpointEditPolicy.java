//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: WireBendpointEditPolicy.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/policy/WireBendpointEditPolicy.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
/***********************************************************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others. All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 **********************************************************************************************************************/
// LOG:
// $Log: not supported by cvs2svn $
// Revision 1.3 2007-11-23 11:33:38 ldiego
// - TASK Detection method is changed to a detection control unit by unit.
// - DEFECT After saving a game the units return to the original position.
// - DEFECT Changes in the movement path points is not reflected.
// - DEFECT Enemies have not to show path.
// - DEFECT Friend unit traces are not shown.
//
// revision 1.2
// - TASK Merged the L02_MovementPath into the HEAD revision.
// - TASK Partial implementation of Refactoring of code. Phase 4.
//
// revision 1.1.2.2
// - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
// - TASK Changed most of the selection and selection notification
// mechanics.
// - TASK Added new fields for location and selection.
// - TASK Changed some methods related to this change in the
// MovementPath functionality.
//
// revision 1.1.2.1
// - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
package net.sourceforge.harpoon.policy;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Bendpoint;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.BendpointEditPolicy;
import org.eclipse.gef.requests.BendpointRequest;

import net.sf.harpoon.commands.CreateReferencePointCommand;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Wire;

import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.WireBendpoint;
import net.sourceforge.harpoon.model.WireEndPoint;
import net.sourceforge.harpoon.parts.RootMapPart;
import net.sourceforge.harpoon.parts.WirePart;

// - CLASS IMPLEMENTATION ...................................................................................
public class WireBendpointEditPolicy extends BendpointEditPolicy {
	// - A B S T R A C T - S E C T I O N
	@Override
	protected Command getCreateBendpointCommand(final BendpointRequest request) {
		// - Get the elements that we need to add a point to a MovementPath
		final WirePart part = (WirePart) request.getSource();
		final RootMapPart root = part.getRootPart();
		final Wire theWire = (Wire) part.getModel();
		final WireEndPoint source = theWire.getSource();
		final MovementPath pathOwner = source.getOwner();
		// //- If the owner if null then the source point is a unit
		// if(null==pathOwner)pathOwner=source.

		// - Convert to DMS coordinates to create and assign them to the new ReferencePoint
		final Point xyPoint = request.getLocation();
		// - Change the sign of the latitude to adjust to our quadrant
		xyPoint.y = -xyPoint.y;
		final DMSPoint location = RootMapPart.xy2dms((RootMapUnit) root.getModel(), xyPoint);

		// - Create and initialize the point.
		CreateReferencePointCommand refCommand = new CreateReferencePointCommand();
		refCommand.setWire(theWire);
		refCommand.setDMSLocation(location);
		refCommand.setOwner(pathOwner);

		// - Locate the point index in the path.
		final int index = pathOwner.getPoints().indexOf(source);
		// int reqIndex = request.getIndex();
		refCommand.setIndex(index);
		return refCommand;
	}

	@Override
	protected Command getMoveBendpointCommand(final BendpointRequest request) {
		final MoveBendpointCommand com = new MoveBendpointCommand();
		final Point p = request.getLocation();
		final Connection conn = getConnection();

		conn.translateToRelative(p);

		com.setLocation(p);

		final Point ref1 = getConnection().getSourceAnchor().getReferencePoint();
		final Point ref2 = getConnection().getTargetAnchor().getReferencePoint();

		conn.translateToRelative(ref1);
		conn.translateToRelative(ref2);

		com.setRelativeDimensions(p.getDifference(ref1), p.getDifference(ref2));
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}

	@Override
	protected Command getDeleteBendpointCommand(final BendpointRequest request) {
		final BendpointCommand com = new DeleteBendpointCommand();
		final Point p = request.getLocation();
		com.setLocation(p);
		com.setWire((Wire) request.getSource().getModel());
		com.setIndex(request.getIndex());
		return com;
	}
	// /**
	// * Shows feedback when a bendpoint is being created. The original figure is used for feedback and the
	// * original constraint is saved, so that it can be restored when feedback is erased.
	// *
	// * @param request
	// * the BendpointRequest
	// */
	// protected void showCreateBendpointFeedback(final BendpointRequest request) {
	// try {
	// // final Connection con = getConnection();
	// final List constraint = (List) getConnection().getRoutingConstraint();
	// if (null == constraint) {
	// final int dummy = 1;
	// getConnection().setRoutingConstraint(new ArrayList());
	// }
	// final List constraint2 = (List) getConnection().getRoutingConstraint();
	// // final List constraint2 = (List) getConnection().getRoutingConstraint();
	// // // final int ind = request.getIndex();
	// // final ArrayList constraint = new ArrayList();
	// // constraint.add(request.getLocation());
	// // con.setRoutingConstraint(cons);
	// super.showCreateBendpointFeedback(request);
	// } catch (final Exception e) {
	// e.printStackTrace();
	// }
	// }
}

// - CLASS IMPLEMENTATION ...................................................................................
class CreateBendpointCommand extends BendpointCommand {

	@Override
	public void execute() {
		// return;
		final WireBendpoint wbp = new WireBendpoint();
		final Dimension start = getFirstRelativeDimension();
		final Dimension end = getSecondRelativeDimension();
		wbp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		getWire().insertBendpoint(getIndex(), wbp);
		super.execute();
	}

	@Override
	public void undo() {
		super.undo();
		getWire().removeBendpoint(getIndex());
	}

}

// - CLASS IMPLEMENTATION ...................................................................................
class MoveBendpointCommand extends BendpointCommand {

	private Bendpoint	oldBendpoint;

	@Override
	public void execute() {
		final WireBendpoint bp = new WireBendpoint();
		bp.setRelativeDimensions(getFirstRelativeDimension(), getSecondRelativeDimension());
		setOldBendpoint(getWire().getBendpoints().get(getIndex()));
		getWire().setBendpoint(getIndex(), bp);
		super.execute();
	}

	protected Bendpoint getOldBendpoint() {
		return oldBendpoint;
	}

	public void setOldBendpoint(final Bendpoint bp) {
		oldBendpoint = bp;
	}

	@Override
	public void undo() {
		super.undo();
		getWire().setBendpoint(getIndex(), getOldBendpoint());
	}

}

// - CLASS IMPLEMENTATION ...................................................................................
class DeleteBendpointCommand extends BendpointCommand {

	private Bendpoint	bendpoint;

	@Override
	public void execute() {
		bendpoint = getWire().getBendpoints().get(getIndex());
		getWire().removeBendpoint(getIndex());
		super.execute();
	}

	@Override
	public void undo() {
		super.undo();
		getWire().insertBendpoint(getIndex(), bendpoint);
	}

}

// - UNUSED CODE ............................................................................................
