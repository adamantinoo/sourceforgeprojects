//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SceneryLesson02Test.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/SceneryLesson02Test.java,v $
//  LAST UPDATE:    $Date: 2007-11-23 11:33:50 $
//  RELEASE:        $Revision: 1.9 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.8  2007-11-02 09:35:04  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.7.2.5  2007-10-31 14:44:37  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.7.2.4  2007-10-23 15:55:17  ldiego
//    - TASK Create a new model element for the movement Trace and the corresponding
//      controller and figure classes.
//    - TASK Change the implementation for the BasePart to be common to all
//      game EditParts and reduce the impact of Cast exceptions.
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement traces history.
//    - DEFECT Units without reference points are accelerated.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Movement loop.
//    - DEFECT Cleanup selection view when editor closes.
//    - DEFECT Reference points not disappearing.
//    - DEFECT The processing loops fires more than once.
//    - DEFECT The scenery read and the scenery run are not the same.
//    - DEFECT The infinite loop fails and stops.
//    - TASK Cache units on processing loop.
//
//    Revision 1.7.2.3  2007-10-22 08:14:19  ldiego
//    - Changed the location of the test scenery.
//    - Activation of the processing loop.
//
//    Revision 1.7.2.2  2007-10-15 14:23:49  ldiego
//    - Changed the code to generate the children list to include the
//      ReferencePoints of the movement path on the MovableUnits.
//    - Changes the naming of units to only generate sequential names
//      if the name is accessed and there is not value set.
//    - [REQUIREMENT A0114.04] - Lesson 02.05 Reference points representation.
//
//    Revision 1.7.2.1  2007-10-11 07:52:30  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.7  2007-10-05 11:24:44  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.6  2007-10-01 14:44:40  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.5  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.4  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.3  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.2  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.1  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//

package net.sf.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import net.sf.harpoon.app.HarpoonConstants;
import net.sf.harpoon.model.units.AirportUnit;
import net.sf.harpoon.model.units.RootMapUnit;
import net.sf.harpoon.model.units.Unit;
import net.sf.harpoon.model.units.WarUnit;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.ExtendedData;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.SensorsModel;

// - CLASS IMPLEMENTATION ...................................................................................
public class SceneryLesson02Test extends HarpoonTestCase {
	public void testLesson02Scenery() throws Exception {
		final String localWorkset = "HarpoonSet - HeadRevision/HarpoonRCP/sceneries/";
		final String targetFileName = WorksetPath + localWorkset + "Lesson02.harpoon";
		final FileOutputStream fos = new FileOutputStream(targetFileName);
		final ObjectOutputStream oos = new ObjectOutputStream(fos);

		final RootMapUnit model = new RootMapUnit();
		createLesson02Scenery(model);
		oos.writeObject(model);
		oos.close();
	}

	/**
	 * Create all the testing units for this Lesson.<br>
	 * Latitudes are made south because the model has a location of 0-0 at the top-left. Below that location (Y
	 * coordinates positive) we have units in the south hemisphere.<br>
	 * There are units of all types possible:<br>
	 * fixed, movable<br>
	 * friends, enemy and others<br>
	 * surface,air,submarine
	 */
	private void createLesson02Scenery(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// - Create Friend units
		AirportUnit base = new AirportUnit();
		base.loadDetails("Interoceanic airport");
		// base.setName("Interoceanic airport");
		base.setSide(Unit.FRIEND_SIDE);
		base.setLatitude(new DMSCoordinate(0, 55, 0, 'S'));
		base.setLongitude(new DMSCoordinate(0, 30, 0, 'E'));
		model.addChild(base);

		WarUnit air = new WarUnit();
		// - Fields for a Unit
		air.loadDetails("Eagle Warning");
		// air.setName("F16 Beta");
		air.setLatitude(new DMSCoordinate(0, 20, 0, 'S'));
		air.setLongitude(new DMSCoordinate(0, 30, 0, 'E'));
		air.setSide(Unit.FRIEND_SIDE);
		// - Fields for a MovableUnit
		air.setUnitType(HarpoonConstants.UNIT_AIR);
		air.setExtendedData(ExtendedData.ALTITUDE, 9000);
		air.setExtendedData(ExtendedData.XDT_SPEEDINCREMENT, 10);
		air.setModel("F16 Figther");
		MovementPath path = new MovementPath(air);
		path.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(90);
		path.addPoint(new DMSCoordinate(0, 25, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E'));
		path.addPoint(new DMSCoordinate(0, 30, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E'));
		air.setSpeed(500);
		air.setMovementPath(path);
		// - Data for a WarUnit.
		SensorsModel sensor = new SensorsModel(air);
		sensor.setAvailable(true, false, true);
		sensor.setAvailable(HarpoonConstants.RADAR_TYPE);
		sensor.setAvailable(HarpoonConstants.ECM_TYPE);
		air.setSensorInformation(sensor);
		model.addChild(air);

		final WarUnit surface = new WarUnit();
		// - Fields for a Unit
		surface.loadDetails("Frigate USS Solaris");
		// surface.setName("Frigate USS Solaris");
		surface.setLatitude(new DMSCoordinate(0, 13, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		surface.setSide(Unit.FRIEND_SIDE);
		// - Fields for a MovableUnit
		surface.setUnitType(HarpoonConstants.UNIT_SURFACE);
		surface.setModel("Frigate Surface vessel");
		path = new MovementPath(surface);
		path.addPoint(new DMSCoordinate(0, 12, 0, 'S'), new DMSCoordinate(0, 13, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		// - Data for a WarUnit.
		sensor = new SensorsModel(surface);
		sensor.setAvailable(false, true, true);
		surface.setSensorInformation(sensor);
		model.addChild(surface);

		final WarUnit submarine = new WarUnit();
		// - Fields for a Unit
		submarine.loadDetails("USS Dallas");
		// submarine.setName("USS Dallas");
		submarine.setLatitude(new DMSCoordinate(0, 15, 0, 'S'));
		submarine.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		submarine.setSide(Unit.FRIEND_SIDE);
		// - Fields for a MovableUnit
		submarine.setUnitType(HarpoonConstants.UNIT_SUBMARINE);
		submarine.setExtendedData(ExtendedData.DEEP, 180);
		submarine.setModel("Submarine class Charlie");
		path = new MovementPath(submarine);
		path.addPoint(new DMSCoordinate(0, 8, 0, 'S'), new DMSCoordinate(0, 13, 0, 'E')); // surface.setDirection(45);
		submarine.setSpeed(15);
		submarine.setMovementPath(path);
		// - Data for a WarUnit.
		sensor = new SensorsModel(submarine);
		sensor.setAvailable(HarpoonConstants.SONAR_TYPE);
		sensor.setAvailable(HarpoonConstants.ECM_TYPE);
		submarine.setSensorInformation(sensor);
		model.addChild(submarine);

		// - Create Foe units
		base = new AirportUnit();
		base.setName("Enemy airport");
		base.setSide(Unit.FOE);
		base.setLatitude(new DMSCoordinate(0, 10, 0, 'S'));
		base.setLongitude(new DMSCoordinate(0, 45, 0, 'E'));
		model.addChild(base);

		air = new WarUnit();
		// - Fields for a Unit
		air.setName("SUv k21");
		air.setLatitude(new DMSCoordinate(0, 10, 0, 'S'));
		air.setLongitude(new DMSCoordinate(0, 40, 0, 'E'));
		air.setSide(Unit.FOE);
		// - Fields for a MovableUnit
		air.setUnitType(HarpoonConstants.UNIT_AIR);
		air.setModel("SUV");
		path = new MovementPath(air);
		path.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(180);
		air.setSpeed(400);
		air.setMovementPath(path);
		// - Data for a WarUnit.
		sensor = new SensorsModel(air);
		sensor.setAvailable(true, false, true);
		surface.setSensorInformation(sensor);
		model.addChild(air);

		// - Create neutral units
		final AirUnit aunit = new AirUnit();
		aunit.setName("Boeing 747");
		aunit.setSide(Unit.NEUTRAL);
		aunit.setLatitude(new DMSCoordinate(0, 30, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 10, 0, 'E'));
		aunit.setSpeed(500);
		// aunit.setDirection(200.0);
		model.addChild(aunit);
	}

	private void sensorTest() {
		// DEBUG Add the sensors as a new unit to see it it is drawn on the map
		// SensorsModel sensor = new SensorsModel();
		// sensor.setRadarState(true);
		// model.addChild(sensor);
	}

	private void locationTest(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// final ReferencePoint reference = new ReferencePoint();
		// reference.setName("R01");
		// reference.setLatitude(new DMSCoordinate(0, 1, 0, 'S'));
		// reference.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		// model.addChild(reference);

		WarUnit ref2 = new WarUnit();
		// - Fields for a Unit
		ref2.setName("F16 Beta");
		ref2.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		ref2.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		ref2.setSide(Unit.FRIEND_SIDE);
		// - Fields for a MovableUnit
		ref2.setUnitType(HarpoonConstants.UNIT_AIR);
		ref2.setModel("F16 Figther");
		MovementPath path2 = new MovementPath(ref2);
		path2.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(90);
		ref2.setSpeed(500);
		ref2.setMovementPath(path2);
		// - Data for a WarUnit.
		// SensorsModel sens = new SensorsModel();
		// model.addChild(sens);
		model.addChild(ref2);
		ref2 = new WarUnit();
		// - Fields for a Unit
		ref2.setName("F16 Beta");
		ref2.setLatitude(new DMSCoordinate(0, 1, 0, 'S'));
		ref2.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		ref2.setSide(Unit.FRIEND_SIDE);
		// - Fields for a MovableUnit
		ref2.setUnitType(HarpoonConstants.UNIT_SURFACE);
		ref2.setModel("F16 Figther");
		path2 = new MovementPath(ref2);
		path2.addPoint(new DMSCoordinate(0, 20, 0, 'S'), new DMSCoordinate(0, 40, 0, 'E')); // surface.setDirection(90);
		ref2.setSpeed(500);
		ref2.setMovementPath(path2);
		// - Data for a WarUnit.
		// SensorsModel sens = new SensorsModel();
		// model.addChild(sens);
		model.addChild(ref2);

	}
}

// - UNUSED CODE ............................................................................................
