//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: CreateReferencePointCommand.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/commands/CreateReferencePointCommand.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:36 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sf.harpoon.commands;

// - IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.policy.BendpointCommand;

// - CLASS IMPLEMENTATION ...................................................................................
public class CreateReferencePointCommand extends BendpointCommand {
	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	private DMSPoint			dmsLocation;
	// private WirePart wirePart;
	private MovementPath	owner;

	public DMSPoint getDMSLocation() {
		return dmsLocation;
	}

	public void setDMSLocation(DMSPoint dmsLocation) {
		this.dmsLocation = dmsLocation;
	}

	public MovementPath getOwner() {
		return owner;
	}

	public void setOwner(MovementPath owner) {
		this.owner = owner;
	}

	@Override
	public void execute() {
		// TODO Get access to the current MovementPath
		getOwner().addPointInWire(getDMSLocation(), getWire());
		super.execute();
	}

	@Override
	public void undo() {
		super.undo();
		getOwner().removePointAtIndex(getIndex());
	}
}
// - UNUSED CODE ............................................................................................
