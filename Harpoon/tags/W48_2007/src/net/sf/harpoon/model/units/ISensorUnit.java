//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: ISensorUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/ISensorUnit.java,v $
//  LAST UPDATE:    $Date: 2007-11-30 12:11:35 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sf.harpoon.model.units;

import net.sourceforge.harpoon.model.SensorsModel;

// - CLASS IMPLEMENTATION ...................................................................................
public interface ISensorUnit {
	// - M O D E L F I E L D - I D E N T I F I E R S
	public static final String	SENSORCHANGE_PROP	= "ISensorUnit.SENSORCHANGE_PROP";

	// - G E T T E R S / S E T T E R S
	public SensorsModel getSensorInformation();

	public void setSensorInformation(final SensorsModel sensor);

	// - P U B L I C - S E C T I O N
	// [02]
	/**
	 * Adds the detected unit to the list of units that are on the detection range for this unit. Also notifies
	 * the detected unit that is being detected by this unit to activate the changes in the presentation. Units
	 * are dependent for the detection state.
	 */
	public void detectedEvent(final Unit detectedUnit);

	//	public void notDetectedEvent(final Unit detectedUnit);

	public void timeDetection();
}
// - UNUSED CODE ............................................................................................
