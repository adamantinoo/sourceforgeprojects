//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonMapTests.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonMapTests.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package harpoon.testcases;

// - IMPORT SECTION .........................................................................................


import java.util.Date;
import java.util.List;

import net.sourceforge.harpoon.figures.HarpoonMap;


import junit.framework.TestCase;


// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonMapTests extends HarpoonTestCase {
//	private boolean SWTinited = false;
	HarpoonMap	map;
//	public void testReadScenery() throws Exception {
//		initializeSWT();
//		map.readScenary("TestScenary.scenary");
//		assertEquals("Check scenary Name", "China-Taiwan Sea", map.getScenaryName());
//		assertEquals("Check scenary ZoomFactor", 11, map.getZoomFactor());
//		assertEquals("Check scenary TopLatBlock", 404, map.getTopLatBlock());
//		assertEquals("Check scenary TopLonBlock", "w2.60", map.getTopLonBlock());
//		assertEquals("Check scenary BlockWidth", 3, map.getBlockWidth());
//		assertEquals("Check scenary TopX", 52, map.getTopX());
//		assertEquals("Check scenary TopY", 26, map.getTopY());
//		closeSWT();
//	}

	public void testGenerateMapURL() throws Exception {
		initializeSWT();
		app.menuOpenEscenery();
		final String mapLocation = map.getURLReference(0, 0);

		String expectedLocation = "http://mt2.google.com/mt?n=404&v=w2.60&x=52&y=26&zoom=11";

		assertEquals("Check the value for the Google URL", expectedLocation, mapLocation);
		closeSWT();
	}

	public void testOpen() throws Exception {
		initializeSWT();
		app.menuOpenEscenery();
eventLoppSWT();
//		app.menuOpenEscenery();
//		map.openEscenery();

//		while (!harpoonShell.isDisposed())
//			if (!theDisplay.readAndDispatch()) theDisplay.sleep();
		closeSWT();
	}

}
// - UNUSED CODE ............................................................................................
