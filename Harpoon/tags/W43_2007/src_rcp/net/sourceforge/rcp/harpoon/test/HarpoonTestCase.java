//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonTestCase.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonTestCase.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package harpoon.testcases;

// - IMPORT SECTION .........................................................................................
import junit.framework.TestCase;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import net.sourceforge.harpoon.HarpoonApp;


// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonTestCase extends TestCase {

	protected Display			theDisplay;
	protected Shell				harpoonShell;
	protected HarpoonApp	app;

	protected void initializeSWT() {
		// - New and optimal version of a SWP application
		theDisplay = new Display();
		app = new HarpoonApp();
		harpoonShell = app.setup(theDisplay);
	}
	protected void eventLoppSWT() {
		while (!harpoonShell.isDisposed())
			if (!theDisplay.readAndDispatch()) theDisplay.sleep();
		harpoonShell.dispose();
		theDisplay.dispose();
	}

	protected void closeSWT() {
		// TODO Auto-generated method stub
		app.close();
		harpoonShell.dispose();
		theDisplay.dispose();
		// SWTinited=false;
	}

}

// - UNUSED CODE ............................................................................................
