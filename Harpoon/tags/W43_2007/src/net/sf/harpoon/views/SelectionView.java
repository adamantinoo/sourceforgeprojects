//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SelectionView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/views/SelectionView.java,v $
//  LAST UPDATE:    $Date: 2007-10-24 16:45:44 $
//  RELEASE:        $Revision: 1.11.2.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.11.2.2  2007-10-18 16:53:54  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.11.2.1  2007-10-11 07:52:30  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.11  2007-10-05 11:24:44  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.10  2007-10-03 16:50:47  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.9  2007-10-03 12:37:03  ldiego
//    - [A0059.01] - Implement version for Lesson 01.
//    - TASK Refactoring of code. Phase 2.
//
//    Revision 1.8  2007-10-02 09:04:25  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.7  2007-09-27 16:45:39  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.6  2007-09-26 16:59:05  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.5  2007-09-25 11:44:58  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.4  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.3  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-17 15:11:40  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//

package net.sourceforge.rcp.harpoon.views;

// - IMPORT SECTION .........................................................................................
import harpoonrcp.View;

import java.util.Iterator;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPart;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.parts.ReferencePart;
import net.sourceforge.harpoon.parts.WarPart;
import net.sourceforge.rcp.harpoon.app.HarpoonRegistry;
import net.sourceforge.rcp.harpoon.editors.SceneryEditor;
import net.sourceforge.rcp.harpoon.editors.SceneryPage;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N

/**
 * This view is responsible to display the properties of the selected unit when there is only one unit
 * selected or if the selection contains more than one item, it should display the list of elements in the
 * selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection
 * to the newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class SelectionView extends View {
	public static final String		ID	= "net.sourceforge.rcp.harpoon.app.selectionview";
	/** */
	private static SelectionView	singleton;

	/**
	 * Top element in the display hierarchy. The children of this element are the ones disposed when changing
	 * the selection.
	 */
	private Composite							top;

	// private Group link;
	// private Text selectionDescription;
	// private Composite properties;
	// private Composite banner;
	// private Hashtable selection = new Hashtable();
	// private TableViewer viewer;

	public SelectionView() {
		// - Register the view
		HarpoonRegistry.getRegistry().put(ID, this);
	}

	// - P U B L I C S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	@Override
	public void createPartControl(Composite parent) {
		singleton = this;
		top = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		// layout.type = SWT.VERTICAL;
		top.setLayout(layout);
	}

	public Composite getTopControl() {
		return top;
	}

	// [01]
	public void clearTopControl() {
		final Control[] childs = top.getChildren();
		for (int i = 0; i < childs.length; i++) {
			final Control child = childs[i];
			child.dispose();
		}
	}

	/**
	 * Updates the content of the <code>SelectionView</code> with the selection elements from the
	 * <code>EditorPanel</code>.<br>
	 * The contents presentation depends on the selection. For single units the view presents some information,
	 * for multiple units it displays a list of the selected units and if the selection goes to default (the
	 * background Map) the view displays the whole list of visible units.
	 */
	public void updateSelection(StructuredSelection selectionContent, IWorkbenchPart part) {
		if (!selectionContent.isEmpty()) {
			// - Count the elements in the selection to check if we display a property page or a table
			if (selectionContent.size() > 1) {
				clearTopControl();
				// - Display a table with the selection elements
				newTableViewer(selectionContent, part);
			} else {
				// - Get the Part selected (there is only one) and show its property controls.
				final AbstractGraphicalEditPart abstractPart = (AbstractGraphicalEditPart) selectionContent.toArray()[0];
				final Unit abstractModel = (Unit) abstractPart.getModel();

				// TODO If the selection is the RootPart then show a table viewer will all the visible units
				// - Detect if the part is a Friend. This displays the full information
				final int side = abstractModel.getSide();
				if (Unit.FRIEND == side) {
//[04]
					if (abstractPart instanceof WarPart) {
						clearTopControl();
						final WarPart war = (WarPart) abstractPart;
						war.createPropertyPage(top);
						// page.redraw();
						// page.changed(page.getChildren());
					}
				} else if (abstractPart instanceof ReferencePart) {
					clearTopControl();
					final ReferencePart reference = (ReferencePart) abstractPart;
					reference.createPropertyPage(top);
				} else {
					// - The unit selected is not a Friend unit. We can compose some small information panel
					clearTopControl();
					// - Display a table with the selection elements
					newTableViewer(selectionContent, part);
				}
			}
			top.layout();
		}
	}

	// [03]
	private void newTableViewer(StructuredSelection selectionContent, final IWorkbenchPart part2) {
		// TODO Create a list of special labels (with icon) and put them in a column
		final Composite table = new Composite(top, SWT.NONE);
		final FillLayout grid = new FillLayout();
		// grid.numColumns = 1;
		grid.marginHeight = 2;
		grid.marginWidth = 2;
		grid.type = SWT.VERTICAL;
		table.setLayout(grid);
		// table.setb
		final SelectionView vv = this;
		final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
		while (it.hasNext()) {
			final AbstractGraphicalEditPart part = it.next();
			final Unit model = (Unit) part.getModel();
			final CLabel lb = new CLabel(table, SWT.SHADOW_OUT);
			lb.setData(part);
			lb.addMouseListener(new MouseListener() {

				public void mouseDoubleClick(MouseEvent e) {
					// TODO The label has been selected and then we can forward this to the new selection
					HarpoonLogger.info("Label " + lb.getText() + " selected");
				}

				public void mouseDown(MouseEvent e) {
					// TODO Access the Editor. This is not currently accesible at this point. Check if it is possible to get a
					// reference when getting the selection
					if (part2 instanceof SceneryEditor) {
						// TODO Get a reference to the GraphicalViewer. This is located inside the Editor Page.
						final SceneryEditor editor = (SceneryEditor) part2;
						final SceneryPage page = editor.getMapPage();
						final GraphicalViewer viewer = page.getGraphicalViewer();
						// - The Label generic Data contains the EditPart to be selected.
						final EditPart data = (EditPart) lb.getData();
						// DEBUG Allow the paths to be invidible to perform selection on units
						// if(data instanceof HarpoonPartFactory$PathPart)
						// ((AbstractGraphicalEditPart) data).getFigure().setVisible(false);
						viewer.select(data);

						// TODO Update the selection again after the change
						// updateSelection(StructuredSelection selectionContent

						HarpoonLogger.info("Label " + lb.getText() + " selected");
					}
				}

				public void mouseUp(MouseEvent e) {
					// EMPTY method. Not being used
				}

			});
			final ImageDescriptor im = HarpoonRegistry.getImageDescriptor("/icons/sample2.gif");
			lb.setImage(im.createImage());
			lb.setText(model.getName());
		}
		// [02]
	}

	public static SelectionView getSingleton() {
		return singleton;
	}

}

// - UNUSED CODE ............................................................................................
// [01]
// public void createPartControl2(Composite parent) {
// singleton = this;
// // - Create the base parts until the link Composite.
// top = new Composite(parent, SWT.NONE);
// FillLayout layout = new FillLayout();
// layout.marginHeight = 2;
// layout.marginWidth = 2;
// layout.type = SWT.VERTICAL;
// top.setLayout(layout);
//
// Composite banner = new Composite(top, SWT.NONE);
// banner.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL, GridData.VERTICAL_ALIGN_BEGINNING,
// true, false));
// GridLayout layout2 = new GridLayout();
// layout2.marginHeight = 5;
// layout2.marginWidth = 10;
// layout2.numColumns = 1;
// banner.setLayout(layout2);
//
// // setup bold font
// Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);
//
// Label title = new Label(banner, SWT.WRAP);
// title.setText("Selection Parts:");
// title.setFont(boldFont);
//
// selectionDescription = new Text(banner, SWT.MULTI | SWT.WRAP);
// selectionDescription.setText("<empty current selection>");
// selectionDescription.setLayoutData(new GridData(GridData.FILL_BOTH));
//
// createDefaultSelection(top);
// }

// private void createDefaultSelection(Composite top) {
// // - Default content at initialization.
// // SashForm top = new SashForm(link, SWT.NONE);
//
// link = new Group(top, SWT.NONE);
// link.setLayout(new FillLayout());
// ((FillLayout) link.getLayout()).type = SWT.VERTICAL;
// link.setText("Properties:");
//
// // AirPart defaultPart = new AirPart();
// // AirUnit defaultUnit = new AirUnit();
// // defaultUnit.setName("default value for initializaation of the viewer");
// // defaultPart.setModel(defaultUnit);
// // selection.put(defaultUnit.getName(), defaultPart);
// //
// // viewer = new TableViewer(top, SWT.FULL_SELECTION);
// // viewer.setContentProvider(new IStructuredContentProvider() {
// // public void dispose() {
// // };
// // public Object[] getElements(Object inputElement) {
// // Collection<Object> selectionValues = selection.values();
// // return selectionValues.toArray();
// // };
// // public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object oldInput, Object newInput) {
// // }
// // });
// // createColumns();
// //
// // viewer.setLabelProvider(new OwnerDrawLabelProvider() {
// // protected void measure(Event event, Object element) {
// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
// // Unit model = (Unit) part.getModel();
// // Point size = event.gc.textExtent(model.getName());
// // event.width = viewer.getTable().getColumn(event.index).getWidth();
// // int lines = size.x / event.width + 1;
// // event.height = size.y * lines;
// // }
// // protected void paint(Event event, Object element) {
// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
// // Unit model = (Unit) part.getModel();
// // event.gc.drawText(model.getName(), event.x, event.y, true);
// // }
// // });
// //
// // viewer.setInput(this);
// // GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_BOTH);
// // viewer.getControl().setLayoutData(data);
// // OwnerDrawLabelProvider.setUpOwnerDraw(viewer);
// // viewer.setSelection(new StructuredSelection(defaultPart));
// }

// private void createColumns() {
// TableLayout layout = new TableLayout();
// viewer.getTable().setLayout(layout);
// viewer.getTable().setHeaderVisible(true);
// viewer.getTable().setLinesVisible(true);
//
// TableColumn tc = new TableColumn(viewer.getTable(), SWT.NONE, 0);
// layout.addColumnData(new ColumnPixelData(350));
// tc.setText("Selection");
// }

// [02]
// // TODO Auto-generated method stub
// viewer = new TableViewer(top, SWT.FULL_SELECTION);
// viewer.setContentProvider(new IStructuredContentProvider() {
// public void dispose() {
// };
// public Object[] getElements(Object inputElement) {
// Collection<Object> selectionValues = selection.values();
// return selectionValues.toArray();
// };
// public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object oldInput, Object newInput)
// {
// }
// });
// createColumns();
//
// viewer.setLabelProvider(new OwnerDrawLabelProvider() {
// protected void measure(Event event, Object element) {
// AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
// Unit model = (Unit) part.getModel();
// Point size = event.gc.textExtent(model.getName());
// event.width = viewer.getTable().getColumn(event.index).getWidth();
// int lines = size.x / event.width + 1;
// event.height = size.y * lines;
// }
// protected void paint(Event event, Object element) {
// AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
// Unit model = (Unit) part.getModel();
// event.gc.drawText(model.getName(), event.x, event.y, true);
// }
// });
//
// viewer.setInput(this);
// GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL |
// GridData.FILL_BOTH);
// viewer.getControl().setLayoutData(data);
// OwnerDrawLabelProvider.setUpOwnerDraw(viewer);
// viewer.setSelection(new StructuredSelection(defaultPart));

// [03]
// public void updateSelection(Hashtable selectionContent) {
// // - Clear the current contents of the selection view
// top.changed(top.getChildren());
// clearTopControl();
// top.changed(top.getChildren());
//
// // - Count the elements in the selection to check if we display a property page or a table
// if (selectionContent.size() > 1) {
// // - Display a table with the selection elements
// // newTableViewer(selectionContent);
// } else {
// // - Get the property page for the selection and link it to the view contents.
// final Enumeration<AbstractGraphicalEditPart> it = selectionContent.elements();
// while (it.hasMoreElements()) {
// final AbstractGraphicalEditPart part = it.nextElement();
// @SuppressWarnings("unused")
// final PropertyModel model = (PropertyModel) part.getModel();
// // TODO Getting the properties is only implemented on the MultiPart
// if (part instanceof MultiPart) {
// final MultiPart multi = (MultiPart) part;
// final Composite page = multi.createPropertyPage(top);
// page.redraw();
// page.changed(page.getChildren());
// }
// }
// top.redraw();
// top.layout();
// top.changed(top.getChildren());
// }
// }
//

//[04]
// if(abstractPart instanceof UnitPart) {
// UnitPart unit=(UnitPart) abstractPart;
// side = unit.get
// }
// final Iterator<AbstractGraphicalEditPart> it = selectionContent.iterator();
// while (it.hasNext()) {
// final AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) it.next();
// final PropertyModel model = (PropertyModel) part.getModel();
// TODO Getting the properties is only implemented on some types
// if (abstractPart instanceof MultiPart) {
// clearTopControl();
// final MultiPart multi = (MultiPart) abstractPart;
// final Composite page = multi.createPropertyPage(top);
// // page.redraw();
// // page.changed(page.getChildren());
// }
