/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.harpoon.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.eclipse.draw2d.Bendpoint;

public class Wire extends PropertyModel {

	public static final String	VISIBILITY	= "VISIBILITY";
	// static final long serialVersionUID = 1;
	// protected boolean value;
	protected ReferencePoint		source, target;
	// protected String
	// sourceTerminal,
	// targetTerminal;
	protected List							bendpoints	= new ArrayList();
	private boolean							visible			= false;

	// public void attachSource(){
	// if (getSource() == null || getSource().getSourceConnections().contains(this))
	// return;
	// getSource().connectOutput(this);
	// }
	//
	// public void attachTarget(){
	// if (getTarget() == null || getTarget().getTargetConnections().contains(this))
	// return;
	// getTarget().connectInput(this);
	// }
	//
	// public void detachSource(){
	// if (getSource() == null)
	// return;
	// getSource().disconnectOutput(this);
	// }
	//
	// public void detachTarget(){
	// if (getTarget() == null)
	// return;
	// getTarget().disconnectInput(this);
	// }

	public Wire(final ReferencePoint from, final ReferencePoint to) {
		setSource(from);
		setTarget(to);
		source.addConnection(this);
		target.addConnection(this);
	}

	public List getBendpoints() {
		return bendpoints;
	}

	public ReferencePoint getSource() {
		return source;
	}

	//
	// public String getSourceTerminal(){
	// return sourceTerminal;
	// }
	//
	public ReferencePoint getTarget() {
		return target;
	}

	//
	// public String getTargetTerminal(){
	// return targetTerminal;
	// }
	//
	// public boolean getValue(){
	// return value;
	// }

	public void insertBendpoint(final int index, final Bendpoint point) {
		getBendpoints().add(index, point);
		firePropertyChange("bendpoint", null, null);//$NON-NLS-1$
	}

	public void removeBendpoint(final int index) {
		getBendpoints().remove(index);
		firePropertyChange("bendpoint", null, null);//$NON-NLS-1$
	}

	public void setBendpoint(final int index, final Bendpoint point) {
		getBendpoints().set(index, point);
		firePropertyChange("bendpoint", null, null);//$NON-NLS-1$
	}

	public void setBendpoints(final Vector points) {
		bendpoints = points;
		firePropertyChange("bendpoint", null, null);//$NON-NLS-1$
	}

	public void setSource(final ReferencePoint e) {
		final Object old = source;
		source = e;
		firePropertyChange("source", old, source);//$NON-NLS-1$
	}

	// public void setSourceTerminal(String s){
	// Object old = sourceTerminal;
	// sourceTerminal = s;
	// firePropertyChange("sourceTerminal", old, sourceTerminal);//$NON-NLS-1$
	// }

	public void setTarget(final ReferencePoint e) {
		final Object old = target;
		target = e;
		firePropertyChange("target", old, target);//$NON-NLS-1$
	}

	public void setVisible(final boolean selected) {
		final boolean oldState = visible;
		visible = selected;
		firePropertyChange(VISIBILITY, oldState, selected);
	}

	public boolean isVisible() {
		return visible;
	}

	// public void setTargetTerminal(String s){
	// Object old = targetTerminal;
	// targetTerminal = s;
	// firePropertyChange("targetTerminal", old, targetTerminal);//$NON-NLS-1$
	// }

	// public void setValue(boolean value){
	// if (value == this.value) return;
	// this.value = value;
	// if (target != null)
	// target.update();
	// firePropertyChange("value", null, null);//$NON-NLS-1$
	// }

	// public String toString() {
	// return "Wire(" + getSource() + "," + getSourceTerminal() + "->" + getTarget() + "," + getTargetTerminal()
	// + ")";//$NON-NLS-5$//$NON-NLS-4$//$NON-NLS-3$//$NON-NLS-2$//$NON-NLS-1$
	// }

}
