//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SubmarineDrawFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/SubmarineDrawFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:13:07 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;

import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class SubmarineDrawFigure extends IconFigure {

	public SubmarineDrawFigure(UnitFigure figure) {
		super(figure);
		// TODO Auto-generated constructor stub
		setSize(10,10);
	}

	public SubmarineDrawFigure() {
		super();
	}

	@Override
	public Dimension getHotSpot() {
		// TODO Auto-generated method stub
		return new Dimension(10,10);
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		graphics.setForegroundColor(getColor());
		graphics.drawOval(getBounds().getCopy());
	}

}

// - UNUSED CODE ............................................................................................
