//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: TracePath.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/TracePath.java,v $
//  LAST UPDATE:    $Date: 2007-10-23 15:55:02 $
//  RELEASE:        $Revision: 1.1.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.Vector;

import org.eclipse.draw2d.geometry.Point;

import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class TracePath extends PropertyModel {
	private static final long		serialVersionUID	= -5865448843677529652L;

	public static final String	ADDTRACE					= "ADDTRACE";

	protected Vector<DMSPoint>	dmsPoints					= new Vector<DMSPoint>();
	protected Vector<Point>			xyPoints					= new Vector<Point>();

	// - C O N S T R U C T O R S
	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	// - P R I V A T E - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	public Vector<DMSPoint> getTracePoints() {
		return dmsPoints;
	}

	public void addPoint(final DMSPoint location) {
		dmsPoints.add(location);
		firePropertyChange(ADDTRACE, null, location);
	}
}

// - UNUSED CODE ............................................................................................
