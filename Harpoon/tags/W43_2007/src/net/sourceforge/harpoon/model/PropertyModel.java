//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PropertyModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/PropertyModel.java,v $
//  LAST UPDATE:    $Date: 2007-10-18 16:53:42 $
//  RELEASE:        $Revision: 1.2.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.1  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

// - CLASS IMPLEMENTATION ...................................................................................
public class PropertyModel implements Serializable, Cloneable {
	private static final long								serialVersionUID	= -3810250540551433870L;
	/**
	 * List of listeners that will be notified when a property changes. The list contains the Controller
	 * elements (EditParts) that will be interested in received events when the model field values change to
	 * reflect that changes on the visual part of the model.
	 */
	private transient PropertyChangeSupport	listeners					= new PropertyChangeSupport(this);

	// - P R O P E R T Y C H E N G E S U P P O R T W R A P P I N G
	/**
	 * Wrap the methods of the PropertyChangeSupport listener element to be accessed by the notification
	 * mechanism.
	 * 
	 * @param listener
	 *          new listener to be added to the list of listeners that will receive notifications.
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		getListeners().addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(final PropertyChangeListener l) {
		getListeners().removePropertyChangeListener(l);
	}

	public void firePropertyChange(final String property, final Object oldValue, final Object newValue) {
		final PropertyChangeSupport targets = getListeners();
		if (targets.hasListeners(property)) {
			targets.firePropertyChange(property, oldValue, newValue);
		}
	}

	public void fireStructureChange(final String property, final Object dataSet, final Object change) {
		final PropertyChangeSupport targets = getListeners();
		if (targets.hasListeners(property))
			targets.firePropertyChange(new PropertyChangeEvent(this, property, dataSet, change));
	}

	// - P R O T E C T E D S E C T I O N
	protected PropertyChangeSupport getListeners() {
		if (null == listeners) listeners = new PropertyChangeSupport(this);
		return listeners;
	}
}

// - UNUSED CODE ............................................................................................
