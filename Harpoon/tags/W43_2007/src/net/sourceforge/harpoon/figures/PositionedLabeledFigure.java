//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: PositionedLabeledFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/PositionedLabeledFigure.java,v $
//  LAST UPDATE:    $Date: 2007-10-23 15:54:44 $
//  RELEASE:        $Revision: 1.2.2.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//
//    Revision 1.1  2007-10-02 09:03:44  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.XYLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.parts.GamePart;

// - CLASS IMPLEMENTATION ...................................................................................
public class PositionedLabeledFigure extends MovableFigure {
	private static Logger			logger				= Logger.getLogger("net.sourceforge");
	// - C O N S T R U C T O R S
	// - P R O T E C T E D S E C T I O N
	private static final int	CHAR_WIDTH		= 6;
	private static final int	CHAR_HEIGHT		= 12;
	private static final int	GAP						= 2;
	private static final int	GAP3					= 3;
	private static final int	LABEL_WIDTH		= (3 + 1) * 6 + GAP;
	private static final int	LABEL_HEIGHT	= 15;

	// - G E T T E R S / S E T T E R S
	private int getSpeedLength() {
		// TODO implement the result
		return speedLen;
	}

	// - P U B L I C - S E C T I O N
	public void init() {
		// - Create the complex internal parts of this figure.
		final XYLayout grid = new XYLayout();
		setLayoutManager(grid);
		speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		speedLabel.setLabelAlignment(PositionConstants.RIGHT);
		directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		directionLabel.setLabelAlignment(PositionConstants.RIGHT);
		this.add(iconic);
		this.add(speedLabel);
		this.add(directionLabel);

		// - Position the elements inside the layout with precise point locations.
		final Dimension iconicSize = iconic.getSize();
		Rectangle elementLocation = new Rectangle();
		elementLocation.x = 0;
		elementLocation.y = 0;
		elementLocation.width = iconicSize.width;
		elementLocation.height = iconicSize.height;
		grid.setConstraint(iconic, elementLocation);

		elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width - GAP3;
		elementLocation.y = -4;
		elementLocation.width = LABEL_WIDTH;
		elementLocation.height = LABEL_HEIGHT;
		grid.setConstraint(speedLabel, elementLocation);

		elementLocation = new Rectangle();
		elementLocation.x = iconicSize.width - GAP3;
		elementLocation.y = LABEL_HEIGHT - 10;
		elementLocation.width = LABEL_WIDTH;
		elementLocation.height = LABEL_HEIGHT;
		grid.setConstraint(directionLabel, elementLocation);

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize());
		this.repaint();
	}

	public void setPart(final GamePart part) {
		this.part = part;
	}

	public Dimension getPreferredSize(final int wHint, final int hHint) {
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension rightSize = getRightSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = iconicSize.width + GAP + rightSize.width;
		fullSize.height = GAP + Math.max(iconicSize.height, rightSize.height) + GAP;
		// logger.info(">>> ENTERING");
		logger.info("iconicSize = " + iconicSize);
		logger.info("rightSize = " + rightSize);
		logger.info("fullSize = " + fullSize);
		// logger.info("<<< EXITING");
		return fullSize;
	}

	// - O V E R R I D E - S E C T I O N
	/*
	 * (non-Javadoc)
	 * 
	 * @see net.sourceforge.harpoon.figures.MovableFigure#getHotSpot()
	 */
	public Point getHotSpot() {
		return iconic.getHotSpot();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.draw2d.Figure#paintFigure(org.eclipse.draw2d.Graphics)
	 */
	protected void paintFigure(final Graphics graphics) {
		super.paintFigure(graphics);
		// - Draw the direction unit line. This line length depends on unit speed.
		final int len = getSpeedLength();
		final Point loc = getLocation();
		final Point hotspot = getHotSpot();
		loc.x += hotspot.x;
		loc.y += hotspot.y;
		final Integer dir = new Integer(directionLabel.getText());
		final double sin = StrictMath.sin(StrictMath.toRadians(dir.doubleValue()));
		final double cos = StrictMath.cos(StrictMath.toRadians(new Integer(directionLabel.getText())
				.doubleValue()));
		final Point diff = new Point();
		diff.x = new Double(len * cos).intValue();
		diff.y = new Double(len * sin).intValue();
		final Point endPoint = new Point(loc.x + diff.x, loc.y + diff.y);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(loc, endPoint);

		// - Draw the movement path
		drawMovePath(graphics);
		// // max=
		// // TODO Draw the center point in a different color
		// // - Get drawing location. This should be already displaced from the top-left.
		// final Rectangle bound = getBounds().getCopy();
		// // bound.width -= 1;
		// // bound.height -= 1;
		// // loc.x--;
		// // loc.y--;
		// // DEBUG Test if the resulting point and the location match.
		// // - Draw the figure center
		// graphics.setLineWidth(1);
	}

	protected void paintChildren(final Graphics graphics) {
		super.paintChildren(graphics);
//		IFigure child;
//
//		final Rectangle clip = Rectangle.SINGLETON;
//		for (int i = 0; i < getChildren().size(); i++) {
//			child = (IFigure) getChildren().get(i);
//			if (child instanceof Polyline) {
//				if (child.isVisible()) {
//					graphics.clipRect(clip);
//					setOpaque(false);
//					child.paint(graphics);
//					graphics.restoreState();
//				}
//			} else if (child.isVisible() && child.intersects(graphics.getClip(clip))) {
//				graphics.clipRect(child.getBounds());
//				child.paint(graphics);
//				graphics.restoreState();
//			}
//		}
	}

	protected void drawMovePath(final Graphics graphics) {
		super.drawMovePath(graphics);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[MovableFigure:");
		buffer.append(speedLabel.getText()).append("-");
		buffer.append(directionLabel.getText()).append("-");
		buffer.append(super.toString()).append("]");
		return buffer.toString();
	}

	// - P R I V A T E - S E C T I O N
	private Dimension getRightSideSize() {
		final int maxChars = StrictMath.max(speedLabel.getText().length(), directionLabel.getText().length());
		final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH, CHAR_HEIGHT);
		return leftLabelSize;
	}
}
// - UNUSED CODE ............................................................................................
