//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MultiFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/MultiFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.rcp.harpoon.test.BaseTestFigure;
import net.sourceforge.rcp.harpoon.test.UnitProfile;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
	/**
	 * This figure has three parts.
	 * <ul>
	 * <li>The left column contains the direction and speed labels.</li>
	 * <li>The center contains the iconic representation.</li>
	 * <li>The right side contains the name and any other information like the level.</li>
	 * </ul>
	 */
	public class MultiFigure extends BaseTestFigure {
		private static final int	CHAR_WIDTH			= 6;
		private static final int	CHAR_HEIGHT			= 13;
		private UnitProfile				profile;
		protected int								margin;
		private Label							directionLabel	= new Label("000");
		private Label							speedLabel			= new Label("00");

		// private Label nameLabel= new Label("label");;

		public MultiFigure() {}
			public MultiFigure(UnitProfile profile) {
			super();
			this.profile = profile;
			// - Create the complex internal parts of this figure.
			GridLayout grid = new GridLayout();
			grid.numColumns = 2;
			grid.horizontalSpacing = margin;
			grid.marginHeight = margin;
			grid.marginWidth = margin;
			grid.verticalSpacing = 0;
	 		this.setLayoutManager(grid);

			// - Add the icon drawing class instance.
			iconic = this.profile.getDrawFigure(this);
			this.add(iconic);
			
			nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			nameLabel.setBorder(new LineBorder(1));
			this.add(nameLabel);
			// DEBUG Set the border for debugging
//			this.setBorder(new LineBorder(1));
			
			//- Calculate size and bounds
			this.setSize(this.getPreferredSize(-1, -1));
		}
		// /*
		// this.margin = this.profile.getMargin();
		//
		// // - Create the complex internal parts of this figure.
		// GridLayout grid = new GridLayout();
		// grid.numColumns = 1;
		// grid.horizontalSpacing = margin;
		// grid.marginHeight = margin;
		// grid.marginWidth = margin;
		// grid.verticalSpacing = 0;
		// this.setLayoutManager(grid);
		// // this.removeAll();
		//
		// // - Create the left side.
		// // createLeftSide(this);
		// // - Add the icon drawing class instance.
		// createMovableIcon(this.profile.getDrawFigure());
		// // this.add(movable);
		//
		// // - Create the right side.
		// createRightSide(this);
		// // DEBUG Set the border for debugging
		// this.setBorder(new LineBorder(1));
		//			
		// //- Calculate size and bounds
		// this.setSize(this.getPreferredSize(-1, -1));
		// */
		// }
	}


// - UNUSED CODE ............................................................................................
