//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: GamePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/GamePart.java,v $
//  LAST UPDATE:    $Date: 2007-10-23 15:54:45 $
//  RELEASE:        $Revision: 1.1.2.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1.2.1  2007-10-11 07:52:13  ldiego
//    - [REQUIREMENT A0114.03] - Lesson 02.03 Movement path display.
//
//    Revision 1.1  2007-10-05 11:24:29  ldiego
//    - DEFECT Hotspot type should be a Point.
//    - DEFECT Optimize undetection loop.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.PropertyModel;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class GamePart extends AbstractGraphicalEditPart implements PropertyChangeListener {
	private RootMapPart	rootPart;

	//- A B S T R A C T - S E C T I O N
	protected abstract void createEditPolicies();

	protected abstract IFigure createFigure();

	public abstract void propertyChange(final PropertyChangeEvent evt);

	protected void refreshVisuals() {
		super.refreshVisuals();
	}

	//	protected void createEditPolicies() {
	//	}

	//- P U B L I C - S E C T I O N
	//	public Unit getUnit() {
	//		return (Unit) getModel();
	//	}
	//
	//	public UnitFigure getUnitFigure() {
	//		return (UnitFigure) getFigure();
	//	}

	public RootMapPart getRootPart() {
		return rootPart;
	}

	public void setRootPart(final RootMapPart rootPart) {
		this.rootPart = rootPart;
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Activates or deactivates the connection to the property listener. Any change on a target property will
	 * fire a call on the <code>propertyChange</code> method.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		((PropertyModel) getModel()).addPropertyChangeListener(this);
		super.activate();
	}

	@Override
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		((PropertyModel) getModel()).removePropertyChangeListener(this);
		super.deactivate();
	}

	/**
	 * This method handles the activation and deactivation of selections. Changes the figure selection state and
	 * signals a repaint of that figure to update the display state.
	 * 
	 * @param value
	 *          selection state. There are at least 3 states.
	 */
	public void setSelected(final int value) {
		super.setSelected(value);

		// - Get the figure and signal a repaint.
		final IFigure fig = getFigure();
		if (fig instanceof UnitFigure) {
			((UnitFigure) fig).setSelected(value);
			fig.repaint();
		}
	}
}
// - UNUSED CODE ............................................................................................
