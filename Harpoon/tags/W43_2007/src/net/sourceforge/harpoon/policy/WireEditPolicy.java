/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.harpoon.policy;

import org.eclipse.gef.commands.Command;
import org.eclipse.gef.requests.GroupRequest;

import net.sourceforge.harpoon.model.Wire;

public class WireEditPolicy extends org.eclipse.gef.editpolicies.ConnectionEditPolicy {

	protected Command getDeleteCommand(final GroupRequest request) {
		final ConnectionCommand c = new ConnectionCommand();
		c.setWire((Wire) getHost().getModel());
		return c;
	}

}
