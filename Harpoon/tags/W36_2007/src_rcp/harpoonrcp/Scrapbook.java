//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: Scrapbook.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/harpoonrcp/Scrapbook.java,v $
//  LAST UPDATE:    $Date: 2007-09-07 12:34:19 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-06 11:23:53  ldiego
//    *** empty log message ***
//


// - IMPORT SECTION .........................................................................................
import harpoonrcp.WorkflowModelManager;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;

public class Scrapbook {

	public voif dummy () {
		new CompilationUnitEditor();

	}
	// EditorPart
	/*
	Subclasses must implement the following methods: 

		IEditorPart.init - to initialize editor when assigned its site 
		IWorkbenchPart.createPartControl - to create the editor's controls 
		IWorkbenchPart.setFocus - to accept focus 
		IEditorPart.isDirty - to decide whether a significant change has occurred 
		IEditorPart.doSave - to save contents of editor 
		IEditorPart.doSaveAs - to save contents of editor 
		IEditorPart.isSaveAsAllowed - to control Save As 

		Subclasses may extend or reimplement the following methods as required: 

		IExecutableExtension.setInitializationData - extend to provide additional initialization when editor extension is instantiated 
		IWorkbenchPart.dispose - extend to provide additional cleanup 
		IAdaptable.getAdapter - reimplement to make the editor adaptable 
*/
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.MultiPageEditorPart#createPages()
	 */
	protected void createPages() {
		try {
			// create workflow page
			workflowPageID = addPage(new WorkflowPage(this), getEditorInput());
			setPageText(workflowPageID, getWorkflowPage().getPageName());

			// create compound tasks page
			compoundTasksPageID = addPage(new CompoundTasksPage(this), getEditorInput());
			setPageText(compoundTasksPageID, getCompoundTasksPage().getPageName());

			// add command stacks
			getMultiPageCommandStackListener().addCommandStack(getWorkflowPage().getCommandStack());
			getMultiPageCommandStackListener().addCommandStack(getCompoundTasksPage().getCommandStack());

			// activate delegating command stack
			getDelegatingCommandStack().setCurrentCommandStack(getWorkflowPage().getCommandStack());

			// set active page
			setActivePage(workflowPageID);
		} catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"En error occured during opening the editor.", e.getStatus());
		}

	}
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		// read workflow from input
		try {
			// we expect IFileEditorInput here,
			// ClassCassException is catched to force PartInitException
			IFile file = ((IFileEditorInput) input).getFile();
			workflow = create(file);

			// validate workflow
			if (null == getWorkflow()) throw new PartInitException("The specified input is not a valid workflow.");
		} catch (CoreException e) {
			throw new PartInitException(e.getStatus());
		} catch (ClassCastException e) {
			throw new PartInitException("The specified input is not a valid workflow.", e);
		}

		// workflow is ok
		super.init(site, input);

		// add delegating CommandStackListener
		getDelegatingCommandStack().addCommandStackListener(getDelegatingCommandStackListener());

		// add selection change listener
		getSite().getWorkbenchWindow().getSelectionService().addSelectionListener(getSelectionListener());

		// initialize actions
		createActions();
	}
	private Workflow create(IFile file) throws CoreException {
		Workflow workflow = null;
		modelManager = new WorkflowModelManager();

		if (file.exists()) {
			try {
				modelManager.load(file.getFullPath());
			} catch (Exception e) {
				modelManager.createWorkflow(file.getFullPath());
			}

			workflow = modelManager.getModel();
			if (null == workflow) { throw new CoreException(new Status(IStatus.ERROR, GEFDemoPlugin.PLUGIN_ID, 0,
					"Error loading the worklow.", null)); }
		}
		return workflow;
	}

}

// - UNUSED CODE ............................................................................................
