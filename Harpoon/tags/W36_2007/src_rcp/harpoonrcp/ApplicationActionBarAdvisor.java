package net.sourceforge.rcp.harpoon;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.ide.IDE;

import net.sourceforge.rcp.harpoon.app.SceneryEditor;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of the actions added to a
 * workbench window. Each window will be populated with new actions.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	/** Information about the types of files than can be open from the interface menus. */
	private static final String[]	OPEN_FILTER_NAMES				= new String[] { "Sceneries (*.scenery)" };
	private static final String[]	OPEN_FILTER_EXTENSIONS	= new String[] { "*.scenery" };

	// Actions - important to allocate these only in makeActions, and then use them
	// in the fill methods. This ensures that the actions aren't recreated
	// when fillActionBars is called with FILL_PROXY.
	private IWorkbenchAction			exitAction;
	private IWorkbenchAction			aboutAction;
	// private IWorkbenchAction newWindowAction;
	// private OpenViewAction openViewAction;
	// private Action messagePopupAction;
	// private IWorkbenchAction newAction;

	private OpenSceneryAction			openSceneryAction;

	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}

	protected void makeActions(final IWorkbenchWindow window) {
		// Creates the actions and registers them.
		// Registering is needed to ensure that key bindings work.
		// The corresponding commands keybindings are defined in the plugin.xml file.
		// Registering also provides automatic disposal of the actions when
		// the window is closed.

		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);

		aboutAction = ActionFactory.ABOUT.create(window);
		register(aboutAction);

		openSceneryAction = new OpenSceneryAction(window, "Open an Scenery..."/* , SceneryEditor.getEditorId() */);
		register(openSceneryAction);

		// newWindowAction = ActionFactory.OPEN_NEW_WINDOW.create(window);
		// register(newWindowAction);
		//        
		// openViewAction = new OpenViewAction(window, "Open Another Message View", View.ID);
		// register(openViewAction);
		//        
		// messagePopupAction = new MessagePopupAction("Open Message", window);
		// register(messagePopupAction);

		// // DEVEL Additions to the menu
		// newAction = ActionFactory.NEW.create(window);
		// ActionFactory.
	}

	protected void fillMenuBar(IMenuManager menuBar) {
		MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);

		menuBar.add(fileMenu);
		// Add a group marker indicating where action set menus will appear.
		menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menuBar.add(helpMenu);

		// File
		// fileMenu.add(newAction);
		fileMenu.add(openSceneryAction);
		// fileMenu.add(new Separator());
		// fileMenu.add(messagePopupAction);
		// fileMenu.add(openViewAction);
		fileMenu.add(new Separator());
		fileMenu.add(exitAction);

		// Help
		helpMenu.add(aboutAction);
	}

	// protected void fillCoolBar(ICoolBarManager coolBar) {
	// IToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
	// coolBar.add(new ToolBarContributionItem(toolbar, "main"));
	// toolbar.add(openViewAction);
	// toolbar.add(messagePopupAction);
	// }
	/**
	 * Opens a new scenery and updates the UI. If there is already an scenery open it should ask the user if the
	 * current scenery should be closed and if the modified data should be saved.<br>
	 * Changed the way to generate the presentation layer. Instead getting the main canvas from the map we
	 * create a graphical viewer from the scenery.<br>
	 * <br>
	 * The order of operations to be performed to create a suitable MVC is described next:
	 * <ul>
	 * <li>Store the current scenery to be replaced if there is any error loading the new one.</li>
	 * <li>Open a dialog to allow the user to select the file.</li>
	 * <li>Open the selected file and create a new scenery with its contents.</li>
	 * <li>Load and create the internal data structures not initialized during the scenery creation.</li>
	 * <li>Initialize the GraphicalViewer. This will trigger the creation of all the EditParts and then the
	 * corresponding Figures for the drawing./li>
	 * </ul>
	 */
	class OpenSceneryAction extends Action {

		private final IWorkbenchWindow	window;
		private int											instanceNum	= 0;

		// private final int editorId;

		public OpenSceneryAction(IWorkbenchWindow window, String label/* , int editorId */) {
			this.window = window;
			// this.editorId = editorId; // This does not exist at the time of the creation
			setText(label);
			// The id is used to refer to the action in a menu or toolbar
			setId(ICommandIds.CMD_OPEN);
			// Associate the action with a pre-defined command, to allow key bindings.
			setActionDefinitionId(ICommandIds.CMD_OPEN);
			setImageDescriptor(harpoonrcp.Activator.getImageDescriptor("/icons/sample2.gif"));
		}

		/**
		 * This method is called when the user select the Open Scenery... menu item. This should open the file
		 * selector and then create and initialize the Scenery Editor to load and display the selected scenery.
		 */
		public void run() {
			if (window != null) {
				try {
					menuOpenScenery();
				} catch (PartInitException e) {
					MessageDialog.openError(window.getShell(), "Title", "Error message");
				}
				// try {
				// window.getActivePage().showView(editorId, Integer.toString(instanceNum++),
				// IWorkbenchPage.VIEW_ACTIVATE);
				// } catch (PartInitException e) {
				// MessageDialog.openError(window.getShell(), "Error", "Error opening view:" + e.getMessage());
				// }
			}
		}

		public void menuOpenScenery() throws PartInitException {
			// TODO Save locally the previous scenery just in case of errors.
			// TODO Check is an scenery is already open. If so ask for closing.
			// - Get the user to choose an scenery file.
			String sceneryPath = null;
			final FileDialog fileChooser = new FileDialog(window.getShell(), SWT.OPEN);
			// TODO Configure the text of the dialog to be presented to the user.
			fileChooser.setText("Texto to be configured.");
			fileChooser.setFilterPath(sceneryPath);
			fileChooser.setFilterExtensions(OPEN_FILTER_EXTENSIONS);
			fileChooser.setFilterNames(OPEN_FILTER_NAMES);
			// if (sceneryPath != null) fileChooser.setFilterPath(sceneryPath);
			String sceneryName = fileChooser.open();
			// sceneryPath = fileChooser.getFilterPath();

			// - Check that the file selected exists and is valid.
			IFileStore fileStore = EFS.getLocalFileSystem().getStore(new Path(sceneryName));
			if (!fileStore.fetchInfo().isDirectory() && fileStore.fetchInfo().exists()) {
				// - Read the scenery propeties and locate the scenery object data.
				final File sceneryFile = new File(sceneryName);
				// this.scenery = new HarpoonScenery(sceneryFile, harpoonShell);
				// public HarpoonScenery(File scenery, Shell harpoonShell) {
				if (null == sceneryFile) throw new PartInitException("The scnery file open has failed.");
				// - Load the scenery configuration properties
				// They will indicate where is stored the model.
				Properties props = new Properties();
				try {
					props.load(new BufferedInputStream(new FileInputStream(sceneryFile)));
					// mapCanvas = new HarpoonMap(harpoonShell, SWT.NO_REDRAW_RESIZE | SWT.BORDER);
					// FIXME This section is on construction
					String modelFileName = props.getProperty("modelLocation");
					if (null == modelFileName) throw new PartInitException("The model store file does not exist");
					run2(modelFileName, props);
				} catch (FileNotFoundException e) {
					throw new PartInitException("The scnery file open has failed." + e.getMessage());
				} catch (IOException e) {
					throw new PartInitException("The scnery file open has failed." + e.getMessage());
				}
			}
			// }
//			if (sceneryName == null) return;
			// - Load the scenery data and create the corresponding editor viewer.
			// final File sceneryFile = new File(sceneryName);

			// TODO Locate the scenery editor and then configure the input so we can read the scenery data.
			//
			// // - With the File reference create the new scenery.
			// // TODO Detect the fail of the creation of the scenery using an Exception
			// // - Open the scenery and initialize it. This scenery gives the presentation layer that has to be
			// // registered to the interface. This is why it receives the shell.
			// // TODO Remove the shell access because it belongs to the application and to the viewer.
			// this.scenery = new HarpoonScenery(sceneryFile, harpoonShell);
			// if (null == this.scenery) {
			// // TODO Open a dialog showing that there was a problem opening the scenery.
			// // TODO Go back to the previous scenery.
			// // - There was an error during the creation of the scenery. Log the incidence and reset the opening.
			// log.warning("The creation of a new scenery did not returned a valid object.");
			// // this.sceneryName = null;
			// } else {
			// scenery.loadData();
			// // - The graphical viewer already exists and belongs to the application. Just set it to the new model
			// // root node.
			// // viewer.setRootEditPart(new BaseRootMapPart());
			// viewer.setRootEditPart(new ScalableFreeformRootEditPart());
			// // TODO Initialize the root edit part that is the map to load the data
			// // scenery.getRootPart().generateMap();
			// viewer.setEditPartFactory(new HarpoonPartFactory());
			// viewer.setContents(scenery.getModelRoot());
			//    			
			// viewer.addSelectionChangedListener(new BaseSelectionListenerActionHarpoon("nombre"));
			// DefaultEditDomain domain = new DefaultEditDomain(null);
			// viewer.setEditDomain(domain);
			// // viewer.registerAccessibleEditPart(new AccessibleGraphicalEditPart());
			//
			// // EditorPart air = HarpoonPartFactory.getDiagram();
			//    			
			// // viewer.setSelectionManager(SelectionManager model)
			//    			
			// /*
			// * c1 // composite viewer.setControl(can);
			// */
			// // DEBUG Other functions that may be needed to implement
			// // viewer.setKeyHandler(new
			// // GraphicalViewerKeyHandler(getGraphicalViewer()).setParent(getCommonKeyHandler()));
			// // getGraphicalViewer().addDropTargetListener(new
			// // TemplateTransferDropTargetListener(getGraphicalViewer()));
			//
			// // DEBUG Ths block should be reviewed
			//
			// // // TODO Move this to the Figure of the Map EditPart
			// // this.surface = scenery.generateMap();
			// // surface.setLocation(0, 0);
			// // surface.redraw();
			// // // - The size has to be calculated depending on the preferred width of the map
			// // surface.setSize(scenery.calculatePrefSize());
			// // surface.addPaintListener(new PaintListener() {
			// // public void paintControl(PaintEvent event) {
			// // scenery.drawMap(event);
			// // scenery.drawUnits(event);
			// // }
			// // });
			// }
			// // END OF BLOCK
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.action.Action#run()
		 */
		public void run2(String name, Properties props) {
			// FileDialog dialog = new FileDialog(window.getShell(), SWT.OPEN | SWT.MULTI);
			// dialog.setText(IDEWorkbenchMessages.OpenLocalFileAction_title);
			// dialog.setFilterPath(filterPath);
			// dialog.open();
			// String[] names = dialog.getFileNames();

			// if (names != null) {
			// filterPath = dialog.getFilterPath();
			//
			// int numberOfFilesNotFound = 0;
			// StringBuffer notFound = new StringBuffer();
			// for (int i = 0; i < names.length; i++) {
			IFileStore fileStore = EFS.getLocalFileSystem().getStore(new Path(name));
			// fileStore = fileStore.getChild(name);
			if (!fileStore.fetchInfo().isDirectory() && fileStore.fetchInfo().exists()) {
				IWorkbenchPage page = window.getActivePage();
				try {
					SceneryEditor editor = (SceneryEditor) IDE.openEditorOnFileStore(page, fileStore);
					editor.getWorkflow().setMapProperties(props);

				} catch (PartInitException e) {
					MessageDialog.openError(window.getShell(), "Title", "Error message");
				}
			}
			// } else {
			// if (++numberOfFilesNotFound > 1)
			// notFound.append('\n');
			// notFound.append(fileStore.getName());
			// }
			// }

			// if (numberOfFilesNotFound > 0) {
			// String msgFmt = numberOfFilesNotFound == 1 ?
			// IDEWorkbenchMessages.OpenLocalFileAction_message_fileNotFound
			// : IDEWorkbenchMessages.OpenLocalFileAction_message_filesNotFound;
			// String msg = NLS.bind(msgFmt, notFound.toString());
			// MessageDialog.openError(window.getShell(), IDEWorkbenchMessages.OpenLocalFileAction_title, msg);
			// }
		}
	}
	// }

	// }
}
