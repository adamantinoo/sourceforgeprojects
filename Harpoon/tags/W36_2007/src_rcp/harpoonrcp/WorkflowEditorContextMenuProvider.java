/**
 * Eclipse GEF redbook sample application
 * $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/harpoonrcp/WorkflowEditorContextMenuProvider.java,v $
 * $Revision: 1.1 $
 * 
 * (c) Copyright IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0 
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     Gunnar Wagenknecht - initial contribution
 * 
 */
package net.sourceforge.rcp.harpoon.app;

import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.ui.actions.ActionRegistry;
import org.eclipse.gef.ui.actions.GEFActionConstants;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.ui.IWorkbenchActionConstants;

/**
 * This is the context menu provider for our editor.
 * 
 * @author Gunnar Wagenknecht
 */
public class WorkflowEditorContextMenuProvider extends ContextMenuProvider
{
    /** the action registry */
    private final ActionRegistry actionRegistry;

    /**
     * Creates a new WorkflowEditorContextMenuProvider instance.
     * @param viewer
     */
    public WorkflowEditorContextMenuProvider(
        EditPartViewer viewer,
        ActionRegistry actionRegistry)
    {
        super(viewer);
        this.actionRegistry = actionRegistry;
    }

    /* (non-Javadoc)
     * @see org.eclipse.gef.ContextMenuProvider#buildContextMenu(org.eclipse.jface.action.IMenuManager)
     */
    public void buildContextMenu(IMenuManager menuManager)
    {
        GEFActionConstants.addStandardActionGroups(menuManager);

        appendActionToUndoGroup(menuManager, GEFActionConstants.UNDO);
        appendActionToUndoGroup(menuManager, GEFActionConstants.REDO);

        appendActionToEditGroup(menuManager, IWorkbenchActionConstants.COPY);
        appendActionToEditGroup(menuManager, IWorkbenchActionConstants.PASTE);
        appendActionToEditGroup(menuManager, IWorkbenchActionConstants.DELETE);
        appendActionToEditGroup(menuManager, GEFActionConstants.DIRECT_EDIT);

//        appendActionToAddGroup(
//            menuManager,
//            WorkflowAction.ADD_CONDITION_TO_CHOICE);

        appendAlignmentSubmenu(menuManager);

        appendActionToMenu(
            menuManager,
            IWorkbenchActionConstants.SAVE,
            GEFActionConstants.GROUP_SAVE);
    }

    /**
     * Appends the alignment subment.
     * @param menuManager
     */
    private void appendAlignmentSubmenu(IMenuManager menuManager)
    {
        // Alignment Actions
        MenuManager submenu = new MenuManager("Align");

        IAction action =
            getActionRegistry().getAction(GEFActionConstants.ALIGN_LEFT);
        if (null != action && action.isEnabled())
            submenu.add(action);

        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_CENTER);
        if (null != action && action.isEnabled())
            submenu.add(action);

        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_RIGHT);
        if (null != action && action.isEnabled())
            submenu.add(action);

        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_TOP);
        if (null != action && action.isEnabled())
            submenu.add(action);

        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_MIDDLE);
        if (null != action && action.isEnabled())
            submenu.add(action);

        action = getActionRegistry().getAction(GEFActionConstants.ALIGN_BOTTOM);
        if (null != action && action.isEnabled())
            submenu.add(action);

        if (!submenu.isEmpty())
            menuManager.appendToGroup(GEFActionConstants.GROUP_REST, submenu);
    }

    /**
     * Returns the action registry.
     * @return the action registry
     */
    protected ActionRegistry getActionRegistry()
    {
        return actionRegistry;
    }

    /**
     * Appends the specified action to the specified menu group
     * @param actionId
     * @param menuGroup
     */
    private void appendActionToMenu(
        IMenuManager menu,
        String actionId,
        String menuGroup)
    {
        IAction action = getActionRegistry().getAction(actionId);
        if (null != action && action.isEnabled())
            menu.appendToGroup(menuGroup, action);
    }

    /**
     * Appends the specified action to the specified menu group
     * @param actionId
     * @param menuGroup
     */
    private void appendActionToUndoGroup(IMenuManager menu, String actionId)
    {
        IAction action = getActionRegistry().getAction(actionId);
        if (null != action && action.isEnabled())
            menu.appendToGroup(GEFActionConstants.GROUP_UNDO, action);
    }

    /**
     * Appends the specified action to the specified menu group
     * @param actionId
     * @param menuGroup
     */
    private void appendActionToEditGroup(IMenuManager menu, String actionId)
    {
        IAction action = getActionRegistry().getAction(actionId);
        if (null != action && action.isEnabled())
            menu.appendToGroup(GEFActionConstants.GROUP_EDIT, action);
    }

    /**
     * Appends the specified action to the specified menu group
     * @param actionId
     * @param menuGroup
     */
    private void appendActionToAddGroup(IMenuManager menu, String actionId)
    {
        IAction action = getActionRegistry().getAction(actionId);
        if (null != action && action.isEnabled())
            menu.appendToGroup(GEFActionConstants.GROUP_ADD, action);
    }

}
