/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.rcp.harpoon.app;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerKeyHandler;
import org.eclipse.gef.ui.parts.ScrollingGraphicalViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.parts.HarpoonPartFactory;

/**
 * This the the main editor page for modifying a complete workflow.
 * 
 * @author Gunnar Wagenknecht
 */
public class WorkflowPage extends AbstractEditorPage
{
    /**
     * Creates a new WorkflowPage instance.
     * 
     * <p>By design this page uses its own <code>EditDomain</code>.
     * The main goal of this approach is that this page has its own
     * undo/redo command stack.
     * 
     * @param parent the parent multi page editor 
     * @param editDomain 
     */
    public WorkflowPage(HarpoonSceneryEditor parent, DefaultEditDomain editDomain)
    {
        super(parent, editDomain);
    }

    /** Return the name for this page. It seems that is something superfluous.
     * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getPageName()
     */
    protected String getPageName()
    {
        return "Workflow";
    }

    /* (non-Javadoc)
     * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#createPageControl(org.eclipse.swt.widgets.Composite)
     */
    protected void createPageControl(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setBackground(parent.getBackground());
        composite.setLayout(new GridLayout(2, false));
        
        createPaletteViewer(composite);
        GridData gd = new GridData(GridData.FILL_VERTICAL);
        gd.widthHint = 125;
        getPaletteViewer().getControl().setLayoutData(gd);
        
        createGraphicalViewer(composite);
        gd = new GridData(GridData.FILL_BOTH);
        gd.widthHint = 275;
        getGraphicalViewer().getControl().setLayoutData(gd);
    }

    /** the graphical viewer */
    protected GraphicalViewer viewer;

    /**
     * Creates the GraphicalViewer on the specified <code>Composite</code>.
     * @param parent the parent composite
     */
    private void createGraphicalViewer(Composite parent)
    {
        viewer = new ScrollingGraphicalViewer();
        viewer.createControl(parent);

        // configure the viewer
        viewer.getControl().setBackground(parent.getBackground());
        viewer.setRootEditPart(new ScalableFreeformRootEditPart());
        viewer.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

        // hook the viewer into the editor
        registerEditPartViewer(viewer);

        // configure the viewer with drag and drop
        configureEditPartViewer(viewer);

        // initialize the viewer with input
        viewer.setEditPartFactory(new HarpoonPartFactory());
        viewer.setContents(getWorkflow());
    }

    /* (non-Javadoc)
     * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getGraphicalViewerForZoomSupport()
     */
    protected GraphicalViewer getGraphicalViewer()
    {
        return viewer;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#getTitle()
     */
    public String getTitle()
    {
        return "Edit the whole workflow";
    }

}
