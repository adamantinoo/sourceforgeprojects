/*******************************************************************************
 * Copyright (c) 2000, 2003 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Common Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/cpl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package net.sourceforge.rcp.harpoon.app;

import harpoonrcp.Activator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef.ContextMenuProvider;
import org.eclipse.gef.EditDomain;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.GraphicalViewer;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.dnd.TemplateTransferDragSourceListener;
import org.eclipse.gef.ui.palette.PaletteViewer;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import net.sourceforge.harpoon.model.Scenery;

/**
 * An abstract base class for all editor pages used in the
 * <code>WorkflowEditor</code>.
 * 
 * <p>It provides basic support for typical GEF handling like 
 * <code>CommandStack</code>s, <code>GraphicalViewer</code>s, a
 * Palette and so on.
 * 
 * @author Gunnar Wagenknecht
 */
public abstract class AbstractEditorPage extends EditorPart
{
    /** the parent multi page editor */
    private final HarpoonSceneryEditor parent;

    /** the edit domain */
    private final EditDomain domain;

    /**
     * Creates a new AbstractEditorPage instance.
     * @param parent the parent multi page editor
     * @param domain the edit domain
     */
    public AbstractEditorPage(HarpoonSceneryEditor parent, EditDomain domain)
    {
        super();
        this.parent = parent;
        this.domain = domain;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.ISaveablePart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public final void doSave(IProgressMonitor monitor)
    {
        // our policy: delegate saving to the parent
        getWorkflowEditor().doSave(monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.ISaveablePart#doSaveAs()
     */
    public final void doSaveAs()
    {
        // our policy: delegate saving to the parent
        getWorkflowEditor().doSaveAs();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IEditorPart#gotoMarker(org.eclipse.core.resources.IMarker)
     */
    public void gotoMarker(IMarker marker)
    {
        // not necessary to implement, is handelt by the multi page editor
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IEditorPart#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
     */
    public void init(IEditorSite site, IEditorInput input)
        throws PartInitException
    {
        setSite(site);
        setInput(input);
        setTitle(input.getName() + ": " + getPageName());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.ISaveablePart#isDirty()
     */
    public final boolean isDirty()
    {
        // our policy: delegate saving to the parent
        // note: this method shouldn't get called anyway
        return getWorkflowEditor().isDirty();
    }

    /**
     * Returns the <code>CommandStack</code> of this editor page.
     * @return the <code>CommandStack</code> of this editor page
     */
    protected final CommandStack getCommandStack()
    {
        return getEditDomain().getCommandStack();
    }

//    /**
//     * Returns the <code>PaletteRoot</code> this editor page uses.
//     * @return the <code>PaletteRoot</code>
//     */
//    protected PaletteRoot getPaletteRoot()
//    {
//        // by default we use the root provided by the multi-page editor
//        return getWorkflowEditor().getPaletteRoot();
//    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
     */
    public final boolean isSaveAsAllowed()
    {
        // our policy: delegate saving to the parent
        return getWorkflowEditor().isSaveAsAllowed();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#setFocus()
     */
    public void setFocus()
    {
        getGraphicalViewer().getControl().setFocus();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public final void createPartControl(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.marginHeight = 10;
        layout.marginWidth = 10;
        layout.verticalSpacing = 5;
        layout.horizontalSpacing = 5;
        layout.numColumns = 1;
        composite.setLayout(layout);
        composite.setBackground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        composite.setForeground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));

        // label on top
        Label label =
            new Label(composite, SWT.HORIZONTAL | SWT.SHADOW_OUT | SWT.LEFT);
        label.setText(getTitle());
        label.setFont(
            JFaceResources.getFontRegistry().get(JFaceResources.HEADER_FONT));
        label.setBackground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        label.setForeground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
        label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        // now the main editor page
        composite = new Composite(composite, SWT.NONE);
        composite.setLayout(new FillLayout());
        composite.setBackground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
        composite.setForeground(
            parent.getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        createPageControl(composite);
    }

    /**
     * Returns the human readable name of this editor page.
     * @return the human readable name of this editor page
     */
    protected abstract String getPageName();

    /**
     * Creates the cotrol of this editor page.
     * @param parent
     */
    protected abstract void createPageControl(Composite parent);

    /**
     * Returns the multi page workflow editor this editor page is contained in.
     * @return the parent multi page editor
     */
    protected final HarpoonSceneryEditor getWorkflowEditor()
    {
        return parent;
    }

    /**
     * Returns the edit domain this editor page uses.
     * @return the edit domain this editor page uses
     */
    public final EditDomain getEditDomain()
    {
        return domain;
    }

    /**
     * Hooks a <code>EditPartViewer</code> to the rest of the Editor.
     * 
     * <p>By default, the viewer is added to the SelectionSynchronizer, 
     * which can be used to keep 2 or more EditPartViewers in sync.
     * The viewer is also registered as the ISelectionProvider
     * for the Editor's PartSite.
     * 
     * @param viewer the viewer to hook into the editor
     */
    protected void registerEditPartViewer(EditPartViewer viewer)
    {
        // register viewer to edit domain
        getEditDomain().addViewer(viewer);

        // the multi page workflow editor keeps track of synchronizing
        getWorkflowEditor().getSelectionSynchronizer().addViewer(viewer);

        // add viewer as selection provider
        getSite().setSelectionProvider(viewer);
    }

    /**
     * Configures the specified <code>EditPartViewer</code>.
     * 
     * @param viewer
     */
    protected void configureEditPartViewer(EditPartViewer viewer)
    {
        // configure the shared key handler
        if (viewer.getKeyHandler() != null)
            viewer.getKeyHandler().setParent(
                getWorkflowEditor().getSharedKeyHandler());

        // configure the context menu
        ContextMenuProvider provider =
            new WorkflowEditorContextMenuProvider(
                viewer,
                getWorkflowEditor().getActionRegistry());
        viewer.setContextMenu(provider);
        getSite().registerContextMenu(Activator.PLUGIN_ID + ".workflow.editor.contextmenu", provider, getSite().getSelectionProvider()); //$NON-NLS-1$

//        // enable viewer as drop target for template transfers
//        viewer.addDropTargetListener(
//            new WorkflowTemplateTransferDropTargetListener(viewer));
    }

    /**
     * Returns the workflow that is edited.
     * @return the workflow that is edited
     */
    protected Scenery getWorkflow()
    {
        return getWorkflowEditor().getWorkflow();
    }

    /** the palette viewer */
    private PaletteViewer paletteViewer;

    /**
     * Creates the createPaletteViewer on the specified <code>Composite</code>.
     * @param parent the parent composite
     */
    protected void createPaletteViewer(Composite parent)
    {
        // create graphical viewer
        paletteViewer = new PaletteViewer();
        paletteViewer.createControl(parent);

        // configure the viewer
        paletteViewer.getControl().setBackground(parent.getBackground());

        // hook the viewer into the EditDomain (only one palette per EditDomain)
        getEditDomain().setPaletteViewer(paletteViewer);

        // important: the palette is initialized via EditDomain
        //fancy palette: paletteViewer.setEditPartFactory(new CustomizedPaletteEditPartFactory());
//        getEditDomain().setPaletteRoot(getPaletteRoot());

        // enable the palette as source for drag operations
        paletteViewer.addDragSourceListener(
            new TemplateTransferDragSourceListener(paletteViewer));
    }

    /**
     * Returns the palette viewer.
     * @return the palette viewer
     */
    protected PaletteViewer getPaletteViewer()
    {
        return paletteViewer;
    }

    /**
     * Returns the graphical viewer of this page.
     * 
     * <p>This viewer is used for example for zoom support 
     * and for the thumbnail in the overview of the outline page.
     * 
     * @return the viewer
     */
    protected abstract GraphicalViewer getGraphicalViewer();
}
