//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPartFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/HarpoonPartFactory.java,v $
//  LAST UPDATE:    $Date: 2007-09-07 12:28:13 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;

import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.ShipUnit;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonPartFactory implements EditPartFactory {

	private static RootMapUnit		root;
	private static RootMapPart		diagram;

	public static Unit getRoot() {
		return root;
	}
	public static RootMapPart getDiagram() {
		return diagram;
	}

	// - O V E R R I D E S
	/**
	 * This method creates any of the EditParts in the application depending on the clss of the element model
	 * received as the parameter. This is the main point where we assemble the model to the internal operating
	 * model used by GEF.
	 * 
	 * @see org.eclipse.gef.EditPartFactory#createEditPart(org.eclipse.gef.EditPart, java.lang.Object)
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		EditPart part = null;

		// - If the EditPart requested is the root diagram EditPart then set it as the diagram root.
		if (model instanceof RootMapUnit) {
			part = new RootMapPart();
			root = (RootMapUnit) model;
			diagram = (RootMapPart) part;
		} else if (model instanceof ShipUnit) part = new ShipPart((Unit) model);
		else if (model instanceof AirUnit) part = new AirPart((Unit) model);
		else if (model instanceof AirportUnit) part = new AirportPart((Unit) model);
		
		//- Initialize the part setting the model and the root EditPart of the internal model.
		part.setModel(model);
		part.setParent(diagram);
		return part;
	}
}
// - UNUSED CODE ............................................................................................
