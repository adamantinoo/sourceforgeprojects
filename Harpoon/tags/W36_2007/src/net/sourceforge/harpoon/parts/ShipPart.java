//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: ShipPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/ShipPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-07 12:28:13 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.AircraftFigure;
import net.sourceforge.harpoon.figures.HarpoonMap;
import net.sourceforge.harpoon.figures.MovableFigure;
import net.sourceforge.harpoon.figures.ShipFigure;
import net.sourceforge.harpoon.geom.PolarCoordinate;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.ShipUnit;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class ShipPart extends AbstractGraphicalEditPart {
	public ShipPart(Unit model) {
		// TODO Auto-generated constructor stub
		setModel(model);
	}

	/** Creates the presentation figure and initializes it. */
	@Override
	public IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		ShipUnit unit = (ShipUnit) this.getModel();

		// - Create and initialize the figure
		MovableFigure ship = new ShipFigure();
		ship.setSide(unit.getSide());
		ship.setDirection(unit.getDirection());
		ship.setSpeed(unit.getSpeed());

		// - Calculate the figure location. The part has access to the model.
		HarpoonMap map = ((RootMapUnit) ((RootMapPart) getParent()).getModel()).getMap();
		double topLeftLat = map.getTopLatitude();
		double topLeftLon = map.getTopLongitude();
		PolarCoordinate latSpan = map.getLatitude2Zoom();
		PolarCoordinate lonSpan = map.getLongitude2Zoom();
		
		double latDiff = Math.abs(topLeftLat-unit.getLatitude());
		double lonDiff = Math.abs(topLeftLon-unit.getLongitude());
		
		int xx = new Double(256*lonDiff/lonSpan.toDegrees()).intValue();
		int yy = new Double(256*latDiff/latSpan.toDegrees()).intValue();

		double degUnit = unit.getLatitude();
		double degTopLeft = topLeftLat;
		double diffLat = 0.0;
		if (degUnit < degTopLeft) diffLat = degTopLeft - degUnit;
		double span = diffLat / latSpan.toDegrees();
		int x = new Double(span * 256.0).intValue();
		int y = new Double((unit.getLongitude() - topLeftLon) / lonSpan.toDegrees() * 256.0).intValue();

		// - Calculate the pixel position of the center.
		ship.setLocation(new Point(xx, yy));

		return ship;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub
		
	}
}
class AirPart extends UnitPart {

	public AirPart(Unit unit) {
		super(unit);
	}

	@Override
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		AirUnit unit = (AirUnit) this.getModel();

		// - Create and initialize the figure
		AircraftFigure air = new AircraftFigure();
		air.setSide(unit.getSide());
		air.setDirection(unit.getDirection());
		air.setSpeed(unit.getSpeed());

		// - Calculate the figure location. The part has access to the model.
		HarpoonMap map = ((RootMapUnit) this.getParent().getModel()).getMap();
		double topLeftLat = map.getTopLatitude();
		double topLeftLon = map.getTopLongitude();
		PolarCoordinate latSpan = map.getLatitude2Zoom();
		PolarCoordinate lonSpan = map.getLongitude2Zoom();
		
		double latDiff = Math.abs(topLeftLat-unit.getLatitude());
		double lonDiff = Math.abs(topLeftLon-unit.getLongitude());
		
		int xx = new Double(256*lonDiff/lonSpan.toDegrees()).intValue();
		int yy = new Double(256*latDiff/latSpan.toDegrees()).intValue();

		double degUnit = unit.getLatitude();
		double degTopLeft = topLeftLat;
		double diffLat = 0.0;
		if (degUnit < degTopLeft) diffLat = degTopLeft - degUnit;
		double span = diffLat / latSpan.toDegrees();
		int x = new Double(span * 256.0).intValue();
		int y = new Double((unit.getLongitude() - topLeftLon) / lonSpan.toDegrees() * 256.0).intValue();

		// - Calculate the pixel position of the center.
		air.setLocation(new Point(xx, yy));

		return air;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	public void addNotify() {
//		// TODO Auto-generated method stub
//		super.addNotify();
//	}
////	private AirUnit getUnit() {
////		return (AirUnit) this.getModel();
////	}
//	/** Refresh visual data from the model data. */
//	protected void refreshVisuals() {
//		int dummy = 1;
////		getFigure().setText(getUnit().getName());
//	}
}

// - UNUSED CODE ............................................................................................
