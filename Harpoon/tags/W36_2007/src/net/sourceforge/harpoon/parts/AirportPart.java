//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirportPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/AirportPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-07 12:28:13 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-09-05 07:48:46  ldiego
//    - Registration of this class before comparison with working
//      previous version.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.FocusEvent;
import org.eclipse.draw2d.FocusListener;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class AirportPart extends UnitPart {

	protected AirportPart(Unit unit) {
		super(unit);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#createFigure()
	 */
	@Override
	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		Unit model = (Unit) this.getModel();
		AirportFigure fig= new AirportFigure();
		fig.setName(model.getName());
		fig.setSide(model.getSide());
// TODO And where is the set of the location?
		return fig;
	}
	/* Possible not needed. */
//	protected List getModelChildren() {
//		return ((ContainerUnit) getUnit()).getChildren();
//	}
}

class AirportFigure extends UnitFigure {
	private static final int MARGIN = 2;
	private AirportDrawFigure	iconic;
	private Label							nameLabel	= new Label("Airport");
	private Color							color			= HarpoonColorConstants.UNKNOWN_SIDE;

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a square
	 * box with some lines inside with a name label at the right center.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center of the icon.
	 */
	public AirportFigure() {
		// - Create the complex internal parts of this figure
		this.setLayoutManager(createStdLayout(MARGIN));
		iconic = new AirportDrawFigure(this.color);
		iconic.addFocusListener(new FocusListener() {    
//			public void widgetSelected(SelectionEvent e) {
//				int dummy = 1;
//				dummy+=1;
//				
//			}

			public void focusGained(FocusEvent fe) {
				// TODO Auto-generated method stub
				int dummy = 1;
				dummy+=1;
				
			}

			public void focusLost(FocusEvent fe) {
				// TODO Auto-generated method stub
				int dummy = 1;
				dummy+=1;
				
			}    
		});

		this.add(iconic);
		nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		this.add(nameLabel);
		// DEBUG Set the border for debugging
		this.setBorder(new LineBorder(1));
		
		//- Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
//		this.setName("New airport");
//		
//		this.setLocation(new Point(100,100));
//		
//		this.setBorder(new LineBorder(1));
//		int width = iconic.getSize().width+3+iconic.getSize().width;
//		int height = Math.max(iconic.getSize().height,iconic.getSize().height);
//		this.setBorder(new LineBorder(1));
//		this.setSize(width,height);
		

//		// - Create the label and size it.
//		nameLabel = new Label("Airport");
//		nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
//		Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
//		Point loc = nameLabel.getLocation();
//		// size=nameLabel.getMinimumSize(-1,-1);
//		nameLabel.setPreferredSize(size);
//		nameLabel.setBorder(new LineBorder(1));
//		nameLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
//		nameLabel.setSize(size);
//		this.add(nameLabel);
//		this.setSize(getFixedSize());
//
//		// DEBUG Set the border for debugging
//		this.setBorder(new LineBorder(1));

		// this.setSize(this.getPreferredSize());
		// this.setSide(Unit.UNKNOWN_SIDE);

		// TODO Auto-generated constructor stub
	}
	// - P U B L I C S E C T I O N
	public void setName(String name) {
		nameLabel.setText(name);
//		nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 7, SWT.NORMAL));
		
		Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		size.expand(8, 2);
		Point loc = nameLabel.getLocation();
		nameLabel.setSize(size);
		nameLabel.setPreferredSize(size);
//		nameLabel.setBorder(new LineBorder(1));
//		nameLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height)/*.getExpanded(2, 2)*/);
		this.invalidate();
	}

	public void setSide(int newSide) {
		// DEBUG I think this property is not necessary.
		// this.side = newSide;
		if (Unit.FRIEND == newSide) this.setColor(HarpoonColorConstants.FRIEND);
		if (Unit.FOE == newSide) this.setColor(HarpoonColorConstants.FOE);
		if (Unit.NEUTRAL == newSide) this.setColor(HarpoonColorConstants.NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) this.setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}
	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	public Dimension getHotSpot() {
		return this.iconic.getHotSpot();
	}

	// - P R O T E C T E D S E C T I O N
	protected void setColor(Color newColor) {
		this.color = newColor;
		this.iconic.setColor(newColor);
		this.iconic.repaint();
		this.repaint();
	}

	protected void paintFigure(Graphics graphics) {
		super.paintFigure(graphics);
	}

	// public boolean intersects(Rectangle rect) {
	// return true;
	// }

	// private Color getColor() {
	// // TODO Auto-generated method stub
	// if (null != this.color)
	// return this.color;
	// else
	// return new Color(HarpoonApp.getDisplay(), 0, 0, 255);
	// }

	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		Dimension iconicSize = this.iconic.getSize();
		Dimension nameLabelSize = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());

		// DEBUG Compare calculated size with current label size - No size set because name not changed
		Dimension lab = nameLabel.getSize();
		
		// - Add layout margins
		GridLayout grid = (GridLayout) this.getLayoutManager();

		Dimension fullSize=new Dimension(0,0);
		fullSize.width=iconicSize.width+grid.horizontalSpacing+nameLabelSize.width+grid.marginWidth*2;
		fullSize.height=Math.max(iconicSize.height,nameLabelSize.height)+grid.marginHeight*2;
		
		// if (size == null) {
		// - Get the size of the label contents
		// Font labelFont = this.nameLabel.getFont();
		// Dimension size = nameLabel.getSize();// FigureUtilities.getStringExtents(this.nameLabel.getText(),
		// labelFont);

//		size.width += grid.marginWidth * 2 + grid.horizontalSpacing;
//		size.height += grid.marginHeight * 2 + grid.verticalSpacing;
//		// - Add icon size
//		size.width += this.iconic.getPreferredSize().width;
//		size.height += this.iconic.getPreferredSize().height;
//		// }
		return fullSize;
	}

//	private Dimension getFixedSize() {
//	}

	class AirportDrawFigure extends IconFigure {
		private static final int	AIRPORT_SIZE	= 31;

		public AirportDrawFigure(Color color) {
			super(color);
			this.setSize(AIRPORT_SIZE, AIRPORT_SIZE);
		}

		protected void paintFigure(Graphics graphics) {
			super.paintFigure(graphics);

			graphics.setForegroundColor(this.color);
			// graphics.setBackgroundColor(this.color);
			// graphics.drawRectangle(getBounds().getTranslated(-15, -15).getResized(new Dimension(30,30)));
			// DEBUG Get the value and show it on the debugger
			Point loc = this.getLocation();
			Rectangle bo = this.getBounds();
			// - The bounds have to be adjusted because if not the lower and right lines are not drawn
			bo.height -= 2;
			bo.width -= 2;
			graphics.drawRectangle(bo);
			
			//- Draw the figure center
			Dimension hotspot = this.getHotSpot();
			loc.x+=hotspot.width;
			loc.y+=hotspot.height;
//			Point startPoint = new Point(bo.x+SHIP_SIZE/2, bo.y+SHIP_SIZE/2);
			Point endPoint=new Point(loc.x+5, loc.y+5);
			graphics.drawLine(loc, endPoint);

		}

		public Dimension getPreferredSize(int wHint, int hHint) {
			return new Dimension(AIRPORT_SIZE, AIRPORT_SIZE);
			// dim.width = 31;
			// dim.height = 31;
			// return dim;
		}

		@Override
		public Dimension getHotSpot() {
			return new Dimension(AIRPORT_SIZE/2,AIRPORT_SIZE/2);
		}

	}
}

// - UNUSED CODE ............................................................................................
