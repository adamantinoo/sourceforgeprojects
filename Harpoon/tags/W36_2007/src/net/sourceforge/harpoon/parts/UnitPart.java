//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/UnitPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:55:59 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.HarpoonMap;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.geom.PolarPoint;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitPart extends AbstractGraphicalEditPart implements PropertyChangeListener {
//	private IPropertySource	propertySource	= null;

//- C O N S T R U C T O R S
	public UnitPart(Unit unit) {
		setModel(unit);
	}
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	//REVIEW createEditPolicies
	@Override
	protected void createEditPolicies() {
		// install the edit policy to handle connection creation
		// Does nothinf but return an null command
//		installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new WorkflowNodeEditPolicy());
//		// to set the delete command
//		installEditPolicy(EditPolicy.COMPONENT_ROLE, new WorkflowComponentEditPolicy());

//		protected void createEditPolicies() {
//			installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new ActivityNodeEditPolicy());
//			installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
//			installEditPolicy(
//				EditPolicy.SELECTION_FEEDBACK_ROLE,
//				new ActivityContainerHighlightEditPolicy());
//			installEditPolicy(EditPolicy.CONTAINER_ROLE, new ActivityContainerEditPolicy());
//			installEditPolicy(EditPolicy.LAYOUT_ROLE, new StructuredActivityLayoutEditPolicy());
//			installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new StructuredActivityDirectEditPolicy());
//		}

//		protected void createEditPolicies() {
//			installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new ActivityNodeEditPolicy());
//			installEditPolicy(EditPolicy.CONTAINER_ROLE, new ActivitySourceEditPolicy());
//			installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
//			installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new ActivityDirectEditPolicy());
//		}

	}
	//REVIEW performRequest
	public void performRequest(Request request) {
		//REQ_MOVE
		//REQ_OPEN
		//REQ_CREATE
		//REQ_DELETE
		//REQ_SELECTION
		//REQ_SELECTION_HOVER
		int DUMMY = 1;
//		if (request.getType() == RequestConstants.REQ_DIRECT_EDIT) {
//			if (request instanceof DirectEditRequest
//				&& !directEditHitTest(((DirectEditRequest) request)
//					.getLocation()
//					.getCopy()))
//				return;
//			performDirectEdit();
//		}
	}
	/**
	 * Returns the specified adapter if recognized. Delegates to the workbench adapter
	 * mechanism.
	 * <P>
	 * Additional adapter types may be added in the future. Subclasses should extend this
	 * method as needed.
	 * @see IAdaptable#getAdapter(java.lang.Class)
	 */
//	public Object getAdapter(Class key) {
////		if (AccessibleEditPart.class == key)
//			return getAccessibleEditPart();
////		return Platform.getAdapterManager().getAdapter(this, key);
//	}
	@Override
//	protected AccessibleEditPart getAccessibleEditPart() {
//		// TODO Auto-generated method stub
//		return super.getAccessibleEditPart();
//	}
//- P U B L I C S E C T I O N
	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	public void activate() {
		if (isActive()) return;

		// -Start listening for changes in the model.
		getUnit().addPropertyChangeListener(this);
		super.activate();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#deactivate()
	 */
	public void deactivate() {
		if (!isActive())return;
		// - Stop listening to events in the model.
		getUnit().removePropertyChangeListener(this);
		super.deactivate();
	}

	// - P R I V A T E S E C T I O N
	/**
	 * Updates the layout data for this object. The layout belongs to the Map root element that is associated to
	 * this Part when created by setting the <code>parent</code> property.<br>
	 * <br>
	 * This is a EditPart, so to get the location we have to access the model and the global coordinate system.<br>
	 * The method calculates the location in XY coordinates for the model DMS coordinates. then moves that point
	 * to the hot spot of the figure because model location is not mapped to the top-left point of the figure.<br><br>
	 * All parts have reference to the root part that also have reference to the top model elemen. We can get the top-left location from this chain
	 * and then calculate the XY point using that reference.
	 */
	protected void refreshVisuals() {
		//- The references to the model and figure objects.
		UnitFigure fig = this.getUnitFigure();
//		Unit mod = this.getUnit();

		// - This block calculates the coordinates XY for the DMS model location.
		PolarPoint location = getUnit().getLocation();

		
		// TODO Remove this reference to the static root object to retrieve the top-left coordinates.
//	// TODO This is a global reference that is quite difficult to solve at this level. search for solution
////	 RootMapUnit root = HarpoonApp.getScenery().getModelRoot();
//	
//		//- Move the point depending on the figure hotspot location.
//		Dimension hotspot = fig.getHotSpot();
//		loc.x-=hotspot.width;
//		loc.y-=hotspot.height;

		Dimension size = fig.getSize();
		Rectangle bo = new Rectangle(convertToMap(location), size);

		// - This triggers the parent layout to revalidate the position of the element.
		((RootMapPart) getParent()).setLayoutConstraint(this, getFigure(), bo);

//		loc = mod.getLocation();
		
		// TODO Calculate position in parent coordinates to send them to the Map layout.
		
//		UnitFigure fig = getUnitFigure();
//		Dimension size = new Dimension(fig.getWidth(), fig.getHeight());
//		Rectangle r = new Rectangle(loc, size);

	}
	private Point convertToMap(PolarPoint point) {
	// - Calculate the figure location. The part has access to the model.
//		Object o1 =  this.getRoot();
	RootMapPart root = HarpoonPartFactory.getDiagram();
	RootMapUnit mod = (RootMapUnit) root.getModel();
	HarpoonMap map = mod.getMap();
	UnitFigure fig = this.getUnitFigure();
	
//	HarpoonMap map = root.getMap();
	double topLeftLat = map.getTopLatitude();
	double topLeftLon = map.getTopLongitude();
	double latSpan = map.getLatitude2Zoom().toDegrees();
	double lonSpan = map.getLongitude2Zoom().toDegrees();
	
	double latDiff = Math.abs(topLeftLat-point.getLatitude());
	double lonDiff = Math.abs(topLeftLon-point.getLongitude());
	
	int xx = new Double(256*lonDiff/lonSpan).intValue();
	int yy = new Double(256*latDiff/latSpan).intValue();
	
	//- Move the point depending on the figure hotspot location.
	Dimension hotspot = fig.getHotSpot();
	xx-=hotspot.width;
	xx-=hotspot.height;

	return new Point(xx,yy);
		
		
	}

  /**
   * The mechanism is not clearly explained. This is the way to receive that there have been changes to the
   * model properties. This implementation intercepts event of type <em><code>Notification.SET</code></em>
   * to trigger the change on the content of one of the model properties so new updates to the presentation take
   * the new data from the model.<br>
   * This implementation does not delegate other event to the super class implementation because no other class
   * implements this interface.
   */
	public void propertyChange(PropertyChangeEvent evt) {
		String prop = evt.getPropertyName();
		if (Unit.CHILDREN.equals(prop))
			refreshChildren();
		else if (Unit.NAME.equals(prop))
			refreshVisuals();
		else if (Unit.SIDE.equals(prop))
			refreshVisuals();
		else if (Unit.LATITUDE.equals(prop))
			refreshVisuals();
		else if (Unit.LONGITUDE.equals(prop)) refreshVisuals();

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

  
  
  
	protected Unit getUnit() {
		return (Unit) this.getModel();
	}
	protected UnitFigure getUnitFigure() {
		return (UnitFigure) getFigure();
	}

	
	
	int getAnchorOffset() {
		return 9;
	}

//	protected void createEditPolicies() {
//		// installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new ActivityNodeEditPolicy());
//		// installEditPolicy(EditPolicy.CONTAINER_ROLE, new ActivitySourceEditPolicy());
//		// installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
//		// installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new ActivityDirectEditPolicy());
//	}

//	public void performRequest(Request request) {
//		// if (request.getType() == RequestConstants.REQ_DIRECT_EDIT)
//		// performDirectEdit();
//	}

	/* (non-Javadoc)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */

//	protected void setFigure(IFigure figure) {
//		figure.getBounds().setSize(0, 0);
//		super.setFigure(figure);
//	}

	public String toString() {
		StringBuffer result = new StringBuffer(this.getClass().getName() + ": [ Unit type: ");
		result.append(getUnit().getClass().getName()+" ]");
		result.append(" [ Unit figure: ");
		result.append(getFigure().getClass().getName()+" ]");
		return result.toString();
	}

}

// - UNUSED CODE ............................................................................................
