//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: IconFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/IconFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-05 09:03:50 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public abstract class IconFigure extends Figure {

	protected Color	color;

	public IconFigure(Color color) {
		this.color = color;
	}

	public void setColor(Color newColor) {
		// TODO Auto-generated method stub
		this.color = newColor;
		// FIXME I should repaint the object or update something ??
	}

	public abstract Dimension getHotSpot(); /*
		// TODO Auto-generated method stub
		return null;
	}*/

}

// - UNUSED CODE ............................................................................................
