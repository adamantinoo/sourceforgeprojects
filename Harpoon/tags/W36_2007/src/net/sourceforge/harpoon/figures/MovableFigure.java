//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/MovableFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-05 09:03:50 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
public abstract class MovableFigure extends UnitFigure {

	/** Stores the direction of movement of the unit to represent it with a vector. */
	private double					direction		= 0.0;
	/** Stores the speed of the unit to represent it with the length of a vector. */
	private int						speed				= 0;
	/** Stores the reference to the class that draws the unit icon representation. */
	private IconFigure			movable;
	/** Label to draw the value for the speed property. */
	private Label					speedlabel	= new Label(new Double(this.direction).toString());
	/** Label to draw the value if this unit direction. */
	private Label					dirLabel		= new Label(new Double(this.direction).toString());
	/** Drawing color for the unit. Depends on the game side. */
	protected Color				color				= HarpoonColorConstants.UNKNOWN_SIDE;

	// DEVEL Possible other attributes to be used for drawing.
	 private boolean selected;
	 private boolean hasFocus;

	/** Stores the color that matches the side of the unit in the game. */
	// protected Color color;
	/** Reference to a Container to draw all the elements of the Ship. */
	 private Figure labelContainer;
	// public MovableFigure() {
	// super();
	// }
	// - P U B L I C S E C T I O N
	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	@Override
	public Dimension getHotSpot() {
		return this.movable.getHotSpot();
	}
	
	/**
	 * The method sets the Figure values to be used for the presentation and also to make the size calculations.
	 * Every time a change on this values is detected then the corresponding size is recalculated.
	 */
	public void setSpeed(int newSpeed) {
		this.speed = newSpeed;

		speedlabel.setText(new Integer(this.speed).toString());
		Dimension size = FigureUtilities.getStringExtents(speedlabel.getText(), speedlabel.getFont());
		Point loc = speedlabel.getLocation();
		speedlabel.setSize(size);
		speedlabel.setPreferredSize(size);
		speedlabel.setBorder(new LineBorder(1));
		speedlabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
		this.invalidate();
	}

	/**
	 * The method sets the Figure values to be used for the presentation and also to make the size calculations.
	 * Every time a change on this values is detected then the corresponding size is recalculated.
	 */
	public void setDirection(double newDirection) {
		this.direction = newDirection;

		dirLabel.setText(new Double(this.direction).toString());
		Dimension size = FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont());
		Point loc = dirLabel.getLocation();
		dirLabel.setSize(size);
		dirLabel.setPreferredSize(size);
		dirLabel.setBorder(new LineBorder(1));
		dirLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
	}

	/**
	 * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while
	 * FOES are marked red. NEUTRAL are yellow and other are blue.
	 */
	public void setSide(int newSide) {
		// DEBUG I think this property is not necessary.
		// this.side = newSide;
		if (Unit.FRIEND == newSide) this.setColor(HarpoonColorConstants.FRIEND);
		if (Unit.FOE == newSide) this.setColor(HarpoonColorConstants.FOE);
		if (Unit.NEUTRAL == newSide) this.setColor(HarpoonColorConstants.NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) this.setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	// - P R O T E C T E D S E C T I O N
	protected void setColor(Color newColor) {
		this.color = newColor;
		this.movable.setColor(newColor);
		this.movable.repaint();
		this.repaint();
	}

	/**
	 * Creates the reference to the class that implements the graphical icon representation for the unit. An
	 * instance of that class is received as a parameter to allow generalization.
	 */
	protected void createMovableIcon(IconFigure iconInstance) {
		this.movable = iconInstance;
		this.add(movable);
	}

	/** Creates the label structures for the movable items, that it is the speed and direction labels. */
	protected void createLabelinfo() {
	 labelContainer = new Figure();
		GridLayout grid = new GridLayout();
		grid.numColumns = 1;
		grid.verticalSpacing = 2;
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		labelContainer.setLayoutManager(grid);
		speedlabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		speedlabel.setBorder(new LineBorder(1));
		labelContainer.add(speedlabel);
		dirLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		dirLabel.setBorder(new LineBorder(1));
		labelContainer.add(dirLabel);
		this.add(labelContainer);
		labelContainer.validate();
		//
		// speedlabel = new Label(new Integer(this.speed).toString());
		// speedlabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// Dimension size = FigureUtilities.getStringExtents(speedlabel.getText(), speedlabel.getFont());
		// Point loc = speedlabel.getLocation();
		// speedlabel.setPreferredSize(size);
		// speedlabel.setBorder(new LineBorder(1));
		// speedlabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
		// speedlabel.setSize(size);
		// labelContainer.add(speedlabel);
		// this.add(labelContainer);
		//
		// dirLabel = new Label(new Double(this.direction).toString());
		// dirLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// size = FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont());
		// loc = dirLabel.getLocation();
		// dirLabel.setPreferredSize(size);
		// dirLabel.setBorder(new LineBorder(1));
		// dirLabel.setBounds(new Rectangle(loc.x, loc.y, size.width, size.height).getExpanded(2, 2));
		// dirLabel.setSize(size);
		// labelContainer.add(dirLabel);

		// Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		// size=nameLabel.getMinimumSize(-1,-1);

	}

	// protected void paintFigure(Graphics graphics) {
	// this.paintFigure(graphics);
	// }

	/**
	 * The new visual representation is a colored circle with two text information at right side with the speed
	 * and direction. This adds some complexity to the creation and calculation of the right visual coordinates.<br>
	 * Also I have added a border for debug purposes.<br>
	 * <br>
	 * The complexity in the calculation of the new size resides in the margins for the layout and the threee
	 * item implied in the process.
	 */
	public Dimension getPreferredSize(int wHint, int hHint) {
		//FIXME This method has not been called before a refreshVisuals.
		//- Get the sized of the three objects.
//		Dimension iconSize = movable.getPreferredSize();
		Dimension dirLabelSize = FigureUtilities.getStringExtents(dirLabel.getText(), dirLabel.getFont());
		Dimension speedLabelSize = FigureUtilities.getStringExtents(speedlabel.getText(), speedlabel.getFont());
		
		int cals = (dirLabel.getText().length() +1 )*8;
		dirLabelSize.width=cals;

		// - Calculate the width. Icon width + grid horizontal spacing + max width for label
		GridLayout grid = (GridLayout) this.getLayoutManager();
		int fullWidth = movable.getPreferredSize().width;
		fullWidth+=grid.horizontalSpacing+Math.max(dirLabelSize.width, speedLabelSize.width);

		// - Calculate the height. max of Icon height or label container height
		int fullHeight = Math.max(movable.getPreferredSize().height, dirLabelSize.height+speedLabelSize.height+2);

		return new Dimension(fullWidth,fullHeight);

		
		
//		new Dimension()
//		Dimension shipDim = movable.getPreferredSize();
//		Dimension labelDim = new Dimension();
//		labelDim.height = Math.max(dirLabelSize.height, speedLabelSize.height);
//		labelDim.width = Math.max(dirLabelSize.width, speedLabelSize.width);
//
//		dim.width = shipDim.width + labelDim.width;
//		dim.height = Math.max(shipDim.height, labelDim.height);
//		return dim;
	}

	//
	// public void setBounds(Rectangle rect) {
	// super.setBounds(rect);
	// rect = Rectangle.SINGLETON;
	// getClientArea(rect);
	// // contents.setBounds(rect);
	// Dimension size = this.getPreferredSize();
	// // footer.setLocation(rect.getBottomLeft().translate(0, -size.height));
	// // footer.setSize(size);
	//
	// // size = header.getPreferredSize();
	// // header.setSize(size);
	// // header.setLocation(rect.getLocation());
	// }


	 public void setSelected(boolean b) {
	 selected = b;
	 repaint();
	 }
	/**
	 * Sets the focus state of this SimpleActivityLabel
	 * 
	 * @param b
	 *          true will cause a focus rectangle to be drawn around the text of the Label
	 */
	 public void setFocus(boolean b) {
	 hasFocus = b;
	 repaint();
	 }
}
// - UNUSED CODE ............................................................................................
