//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: Unit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/Unit.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:55:59 $
//  RELEASE:        $Revision: 1.7 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.6  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.5  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.4  2007-08-30 08:57:22  ldiego
//    - [A0015.0013] - Divide the main window into two parts.
//    - [A0005.01] - Creation of a test scenery.
//    - [A0007.01] - There is a test scenery file with content.
//    - [A0003.01] - Initial requirements for menu "Debug".
//
//    Revision 1.3  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//
//    Revision 1.2  2007-08-21 13:45:11  ldiego
//    - Initial implementation in a working model that has to be adapted to a more classical model.
//
//    Revision 1.1  2007-08-20 13:05:41  ldiego
//    - This first release implements the base structure for storing the initial
//      attributes for a game unit.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Vector;

import net.sourceforge.harpoon.geom.PolarPoint;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Has the base and common behavior for all game units not depending if they are air borne or ships. This
 * class is the base building block for the game user model that is manipulated by the game controller.<br>
 * Is <code>Serializable</code> to allow for game saving and model storage and also has to implement the key
 * methods for the <code>IPropertySource</code> interface to allow for controller registration.<br>
 * <br>
 * The common characteristics for all model elements on the game are defined next:
 * <dl>
 * <dt>Name</dt>
 * <dd>Not all units have a real name like USS Enterprise or Enola Gay. But all them have to have a user
 * reference to help the right identification. Names for some units may change because the creation and
 * destruction of fleets or task forces may require to do so.<br>
 * Game contacts also have to be named, so a single <code>Unit</code> may have more than one name at the
 * same time or during the game lifetime.<br>
 * So the <code>Name</code> property may have different senses depending on the environment but mainly
 * server to the purpose of identification. If the game database or user does not give a name to an unit it
 * will generate one depending on name type.</dd>
 * </dl>
 * 
 * <b>References:</b><br>
 * http://www.eclipse.org/articles/Article-GEF-diagram-editor/shape.html<br>
 * http://java.sun.com/developer/technicalArticles/Programming/serialization/
 */
public abstract class Unit 	implements Serializable, Cloneable {

	private static final long	serialVersionUID	= 1L;

	public static String NAME_DEFAULT = "Unit ";

	public static final String NAME = "name"; //$NON-NLS-1$
	public static final String SIDE = "side"; //$NON-NLS-1$
	public static final String LATITUDE = "latitude"; //$NON-NLS-1$
	public static final String LONGITUDE = "longitude"; //$NON-NLS-1$

	public static final int		UNKNOWN_SIDE			= 0;
	public static final int		FRIEND						= 1;
	public static final int		FOE								= 2;
	public static final int		NEUTRAL						= 3;
	public static final int		SPEED_CRUISE			= 11;

	public static final String	CHILDREN	= "children";

	/** Contains an identifier to the Unit. This can be a user defined name or a database supplied name. */
	protected String					name;
	/** Unit location in the global coordinates. */
	protected PolarPoint			position					= new PolarPoint(0, 0);
	/** Unit side. This allows to identify other units not known to the user or detected during the game. */
	protected int						side							= UNKNOWN_SIDE;

	private transient PropertyChangeSupport	listeners	= new PropertyChangeSupport(this);

	private Vector<Unit>	children	= new Vector<Unit>();

	// - C O N S T R U C T O R S

	// - G E T T E R S / S E T T E R S
	public String getName() {
		return name;
	}

	public void setName(String name) {
		String oldName = this.name;
		this.name = name;
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.NAME, oldName, name);
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		int oldSide = this.side;
		this.side = side;
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.SIDE, oldSide, side);
	}

	public double getLatitude() {
		return this.position.getLatitude();
	}

	public void setLatitude(double lat) {
		double oldLatitude = this.position.getLatitude();
		this.position.setLatitude(lat);
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.LATITUDE, oldLatitude, lat);
	}

	public double getLongitude() {
		return this.position.getLongitude();
	}

	public void setLongitude(double lon) {
		double oldLongitude = this.position.getLongitude();
		this.position.setLongitude(lon);
		// - Fire a notification to all listeners to update the presentation layer.
		firePropertyChange(Unit.LONGITUDE, oldLongitude, lon);
	}
/**
 * Returns a graphical point with the corresponding XY coordinates in Map reference to the latitude and
 * longitude location for this unit. It has on account for the equivalence of the top-left location in map
 * coordinates that are 0,0 in the XY axis.
 * 
 * @return draw2d point with the map layout location.
 */
	public PolarPoint getLocation() {
//		// TODO Remove this reference to the static root object to retrieve the top-left coordinates.
//		// TODO This is a global reference that is quite difficult to solve at this level. search for solution
////		 RootMapUnit root = HarpoonApp.getScenery().getModelRoot();
//		
//		// TODO Auto-generated method stub
//		// - Calculate the figure location. The part has access to the model.
//		HarpoonMap map = root.getMap();
//		double topLeftLat = map.getTopLatitude();
//		double topLeftLon = map.getTopLongitude();
//		double latSpan = map.getLatitude2Zoom().toDegrees();
//		double lonSpan = map.getLongitude2Zoom().toDegrees();
//		
//		double latDiff = Math.abs(topLeftLat-getLatitude());
//		double lonDiff = Math.abs(topLeftLon-getLongitude());
//		
//		int xx = new Double(256*lonDiff/lonSpan).intValue();
//		int yy = new Double(256*latDiff/latSpan).intValue();
//		
//		return new Point(xx,yy);
		
		return this.position;
//
//		double degUnit = unit.getLatitude();
//		double degTopLeft = topLeftLat;
//		double diffLat = 0.0;
//		if (degUnit < degTopLeft) diffLat = degTopLeft - degUnit;
//		double span = diffLat / latSpan.toDegrees();
//		int x = new Double(span * 256.0).intValue();
//		int y = new Double((unit.getLongitude() - topLeftLon) / lonSpan.toDegrees() * 256.0).intValue();
//
//		// - Calculate the pixel position of the center.
//		ship.setLocation(new Point(xx, yy));
//
//		
//		return new Point (100, 100);
	}

public void addPropertyChangeListener(PropertyChangeListener l) {
	getListeners().addPropertyChangeListener(l);
}

public void removePropertyChangeListener(PropertyChangeListener l) {
	getListeners().removePropertyChangeListener(l);
}

protected void firePropertyChange(String property, Object oldValue, Object newValue) {
	if (getListeners().hasListeners(property)) {
		getListeners().firePropertyChange(property, oldValue, newValue);
}
}

	public PropertyChangeSupport getListeners() {
		if (null == listeners) listeners = new PropertyChangeSupport(this);
		return listeners;
	}

public void addChild(Unit child) {
	Vector<Unit> oldSet = (Vector<Unit>) this.children.clone();
	children.add(child);
	fireStructureChange(CHILDREN, oldSet, child);
}

public Vector<Unit> getChildren() {
	return children;
}

public void removeChild(Unit child) {
	Vector<Unit> oldSet = (Vector<Unit>) this.children.clone();
	children.remove(child);
	fireStructureChange(CHILDREN, oldSet, child);
}

private void fireStructureChange(String fieldname, Object dataSet, Unit change) {
	getListeners().firePropertyChange(new PropertyChangeEvent(this, fieldname, dataSet, change));
}

public void clear() {
	// TODO Auto-generated method stub
	this.children.clear();
}
}

// - UNUSED CODE ............................................................................................
