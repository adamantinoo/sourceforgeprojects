//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirportUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/AirportUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:55:59 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class AirportUnit extends Unit {

	private static final long	serialVersionUID	= 1L;

	public String toString() {
		StringBuffer result = new StringBuffer(super.toString());
		result.append(" [Airport ( ");
		result.append(" )");
		result.append(" ]");
		return result.toString();
	}
}

// - UNUSED CODE ............................................................................................
