//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/MovableUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-05 09:03:51 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public abstract class MovableUnit extends Unit {

	/** Stores the speed at witch the unit is travelling. This is only valid for units that may move. */
	private int	speed	= 0;
	/** Direction of movement of this Unit. */
	private double	direction	= 0.0;

//	public MovableUnit() {
//		super();
//	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public double getDirection() {
		return direction;
	}

	public void setDirection(double dir) {
		this.direction = dir;
	}

}
// - UNUSED CODE ............................................................................................
