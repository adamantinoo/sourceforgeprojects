//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryEditor.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryEditor.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:56:18 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.FileStoreEditorInput;

import net.sourceforge.harpoon.model.Scenery;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
/**
 * This class is created to wrap the already available code from other sources. Because the used code has a
 * lot of development that is uncertain if it is needed or not for our current development I will add new
 * classes in from where I will move validated code and new adaptations. It will also show the methods that
 * are really required to create an editor.<br>
 * This class is started when the user opens a file with a extension that is mapped to this editor as it is
 * configured in the plugin descriptor. The first method to be called is the <code>init()</code> that will
 * check the existence of the selected file and prepare the environment for the reading of the data that is
 * performed in the method <code>setInput()</code>.The method <code>createPages()</code> is also called
 * when the editor page is created and perform all visual structures creation.
 */
public class SceneryEditor extends HarpoonSceneryEditor {
	/**
	 * Creates the pages of a multipage Editor. In our implementation we have only a single page so we can move
	 * it to a local field and avoid any page change. <br>
	 * The method creates the SceneryPage and then configures the editor with the page name. All this methods
	 * can be simplified.
	 * 
	 * @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#createPages()
	 */
	@Override
	protected void createPages() {
		try {
			// - Create scenery page and make it the active page.
			workflowPageID = addPage(new SceneryPage(this), getEditorInput());
			setPageText(workflowPageID, getSceneryPage().getPageName());
			setActivePage(workflowPageID);
		} catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Open Error",
					"An error occured during opening the editor.", e.getStatus());
		}
	}

	@Override
	protected void setInput(IEditorInput input) {
//		// TODO Auto-generated method stub
//		// scenery=create(file);
//		String name = input.getName();
//		String str = input.toString();
//
		FileStoreEditorInput fse = (FileStoreEditorInput) input;
//		int dummy = 1;
//
		// - The URI gets the file name and path for the scenery data. Read that data into the model and the
		// scenery will be presented.
		// TODO read the object data
		try {
			URI sceneryURI = fse.getURI();
			// if (null == modelFileName) throw new FileNotFoundException("The model store file does not exist");
			FileInputStream fis = new FileInputStream(sceneryURI.getPath());
			ObjectInputStream ois = new ObjectInputStream(fis);
			this.workflow = (Scenery) ois.readObject();
			ois.close();
			// DEBUG Initialize to the preset set of units.
			// workflow.setMapProperties(props);
//			workflow.createTestUnits();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// throw new PartInitException("Error message"+e.getMessage());
			// MessageDialog.openError(window.getShell(), "Title", "Error message"+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			// throw new PartInitException("Error message"+e.getMessage());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			// throw new PartInitException("Error message"+e.getMessage());
		}

		super.setInput(input);
	}
//FIXME This code is under construction
///**
// * Returns the workflow object from the specified file.
// * 
// * @param file
// * @return the workflow object from the specified file
// */
//private Scenery create(IFile file) throws CoreException {
//	Scenery workflow = null;
//	modelManager = new WorkflowModelManager();
//
//	if (file.exists()) {
//		try {
//			modelManager.load(file.getFullPath());
//		} catch (Exception e) {
//			modelManager.createWorkflow(file.getFullPath());
//		}
//
//		workflow = modelManager.getModel();
//		if (null == workflow) { throw new CoreException(new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0,
//				"Error loading the worklow.", null)); }
//	}
//	return workflow;
//}
	/** Saves the state of the scenery. This has to be disabled until the game logic is set.

	* @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// TODO Auto-generated method stub
//		super.doSave(monitor);
	}

	/** Saves the state of the scenery. This has to be disabled until the game logic is set.

	* @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
//		super.doSaveAs();
	}

	/** Save as... is not supported.

	* @see net.sourceforge.rcp.harpoon.app.HarpoonSceneryEditor#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/** Not necessary. We have no markers. */
	@Override
	public void gotoMarker(IMarker marker) {
		// TODO Auto-generated method stub
//		super.gotoMarker(marker);
	}
	public int getEditorId() {
		return this.workflowPageID;
	}
}

// - UNUSED CODE ............................................................................................
