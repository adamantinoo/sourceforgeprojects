//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:56:18 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//									Copyright (c) 2000, 2003 IBM Corporation and others.
//									All rights reserved. This program and the accompanying materials 
//									are made available under the terms of the Common Public License v1.0
//									which accompanies this distribution, and is available at
//									http://www.eclipse.org/legal/cpl-v10.html
//
//									Contributors:
//									    IBM Corporation - initial API and implementation
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.app;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.model.Scenery;
import net.sourceforge.harpoon.parts.HarpoonPartFactory;

/**
 * This the the main editor page. It has been modified to support for the main gae window where the scenery
 * map is shown and then the game units are displayed.
 * 
 * @author Luis de Diego
 * @author Gunnar Wagenknecht
 */
public class SceneryPage extends WorkflowPage {
	/**
	 * Creates a new SceneryPage instance.
	 * 
	 * By design this page uses its own <code>EditDomain</code>. The main goal of this approach is that this
	 * page has its own undo/redo command stack.<br>
	 * Undo/redo is something that may be disabled in the game. Set the domain to the default domain used in
	 * other examples.
	 * 
	 * @param parent
	 *          the parent multi page editor
	 */
	public SceneryPage(HarpoonSceneryEditor parent) {
		super(parent, new DefaultEditDomain(parent)); // Changed to a DefaultEditDomain
	}

	/**
	 * Return the name for this page. It seems that is something superfluous.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getPageName()
	 */
	protected String getPageName() {
		// TODO See when is used this name in the display area.
		return "Main Scenery";
	}

	/**
	 * This method is called when the interface creates the page to be displayed in the editor panel. The
	 * functionality is to create the graphic elements that compose the page and initialize them. In this
	 * particular implementation we discard the palette creation and we set the main contents to our graphical
	 * viewer.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#createPageControl(org.eclipse.swt.widgets.Composite)
	 */
	protected void createPageControl(Composite parent) {
		createGraphicalViewer(parent);
	}

	/**
	 * Creates the GraphicalViewer on the specified <code>Composite</code>.
	 * 
	 * @param parent
	 *          the parent composite
	 */
	private void createGraphicalViewer(Composite parent) {
		Composite c1 = new Composite(parent, SWT.NONE);
		c1.setBackground(parent.getBackground());
		c1.setLayout(new FillLayout());
		Canvas can = new Canvas(c1, SWT.NONE);
//		GraphicalViewerImpl vi = new GraphicalViewerImpl();
//		vi.setControl(can);
//		viewer = vi;
		can.setLayout(new FillLayout());

		
		viewer = new GraphicalViewerImpl();
		viewer.setControl(can);
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());
		// vi.setKeyHandler(new GraphicalViewerKeyHandler(viewer));

		// hook the viewer into the editor
		registerEditPartViewer(viewer);

		// - Initialize the viewer with input
		viewer.setEditPartFactory(new HarpoonPartFactory());
		viewer.setContents(getScenery());
	}

	/**
	 * Returns the scenery root of the model that is being played.
	 * 
	 * @return the scenery model
	 */
	protected Scenery getScenery() {
		return getWorkflowEditor().getWorkflow();
	}

}

// - UNUSED CODE ............................................................................................
