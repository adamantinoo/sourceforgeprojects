//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonColorConstants.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonColorConstants.java,v $
//  LAST UPDATE:    $Date: 2007-09-05 09:03:50 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon;

//- IMPORT SECTION .........................................................................................
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

//- CLASS IMPLEMENTATION ...................................................................................
//- C O N S T R U C T O R S
//- P U B L I C S E C T I O N
//- G E T T E R S / S E T T E R S
//- P R I V A T E S E C T I O N
/**
 * Contains a collection of the palette of colors that can be used in the Harpoon game.
 */
public interface HarpoonColorConstants {

	class HarpoonColorFactory {
		private static Color getColor(final int which) {
			Display display = Display.getCurrent();
			if (display != null) return display.getSystemColor(which);
			display = Display.getDefault();
			final Color result[] = new Color[1];
			display.syncExec(new Runnable() {
				public void run() {
					synchronized (result) {
						result[0] = Display.getCurrent().getSystemColor(which);
					}
				}
			});
			synchronized (result) {
				return result[0];
			}
		}
	}

	Color	UNKNOWN_SIDE	= new Color(Display.getCurrent(), 0, 0, 127);
	Color	FRIEND				= new Color(Display.getCurrent(), 0, 255, 0);
	Color	FOE						= new Color(Display.getCurrent(), 255, 0, 0);
	Color	NEUTRAL				= new Color(Display.getCurrent(), 255, 255, 0);

	// /**
	// * @see SWT#COLOR_WIDGET_HIGHLIGHT_SHADOW
	// */
	// Color buttonLightest = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_HIGHLIGHT_SHADOW);
	// /**
	// * @see SWT#COLOR_WIDGET_BACKGROUND
	// */
	// Color button = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_BACKGROUND);
	// /**
	// * @see SWT#COLOR_WIDGET_NORMAL_SHADOW
	// */
	// Color buttonDarker = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_NORMAL_SHADOW);
	// /**
	// * @see SWT#COLOR_WIDGET_DARK_SHADOW
	// */
	// Color buttonDarkest = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_DARK_SHADOW);
	//
	// /**
	// * @see SWT#COLOR_LIST_BACKGROUND
	// */
	// Color listBackground = HarpoonColorFactory.getColor(SWT.COLOR_LIST_BACKGROUND);
	// /**
	// * @see SWT#COLOR_LIST_FOREGROUND
	// */
	// Color listForeground = HarpoonColorFactory.getColor(SWT.COLOR_LIST_FOREGROUND);
	//
	// /**
	// * @see SWT#COLOR_WIDGET_BACKGROUND
	// */
	// Color menuBackground = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_BACKGROUND);
	// /**
	// * @see SWT#COLOR_WIDGET_FOREGROUND
	// */
	// Color menuForeground = HarpoonColorFactory.getColor(SWT.COLOR_WIDGET_FOREGROUND);
	// /**
	// * @see SWT#COLOR_LIST_SELECTION
	// */
	// Color menuBackgroundSelected = HarpoonColorFactory.getColor(SWT.COLOR_LIST_SELECTION);
	// /**
	// * @see SWT#COLOR_LIST_SELECTION_TEXT
	// */
	// Color menuForegroundSelected = HarpoonColorFactory.getColor(SWT.COLOR_LIST_SELECTION_TEXT);
	//
	// /**
	// * @see SWT#COLOR_TITLE_BACKGROUND
	// */
	// Color titleBackground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_BACKGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_BACKGROUND_GRADIENT
	// */
	// Color titleGradient = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT);
	// /**
	// * @see SWT#COLOR_TITLE_FOREGROUND
	// */
	// Color titleForeground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveForeground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveBackground = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	// /**
	// * @see SWT#COLOR_TITLE_INACTIVE_FOREGROUND
	// */
	// Color titleInactiveGradient = HarpoonColorFactory.getColor(SWT.COLOR_TITLE_INACTIVE_FOREGROUND);
	//
	// /**
	// * @see SWT#COLOR_INFO_FOREGROUND
	// */
	// Color tooltipForeground = HarpoonColorFactory.getColor(SWT.COLOR_INFO_FOREGROUND);
	// /**
	// * @see SWT#COLOR_INFO_BACKGROUND
	// */
	// Color tooltipBackground = HarpoonColorFactory.getColor(SWT.COLOR_INFO_BACKGROUND);
	//
	// /*
	// * Misc. colors
	// */
	// /** One of the pre-defined colors */
	// Color white = new Color(null, 255, 255, 255);
	// /** One of the pre-defined colors */
	// Color lightGray = new Color(null, 192, 192, 192);
	// /** One of the pre-defined colors */
	// Color gray = new Color(null, 128, 128, 128);
	// /** One of the pre-defined colors */
	// Color darkGray = new Color(null, 64, 64, 64);
	// /** One of the pre-defined colors */
	// Color black = new Color(null, 0, 0, 0);
	// /** One of the pre-defined colors */
	// Color red = new Color(null, 255, 0, 0);
	// /** One of the pre-defined colors */
	// Color orange = new Color(null, 255, 196, 0);
	// /** One of the pre-defined colors */
	// Color yellow = new Color(null, 255, 255, 0);
	// /** One of the pre-defined colors */
	// Color green = new Color(null, 0, 255, 0);
	// /** One of the pre-defined colors */
	// Color lightGreen = new Color(null, 96, 255, 96);
	// /** One of the pre-defined colors */
	// Color darkGreen = new Color(null, 0, 127, 0);
	// /** One of the pre-defined colors */
	// Color cyan = new Color(null, 0, 255, 255);
	// /** One of the pre-defined colors */
	// Color lightBlue = new Color(null, 127, 127, 255);
	// /** One of the pre-defined colors */
	// Color blue = new Color(null, 0, 0, 255);
	// /** One of the pre-defined colors */
	// Color darkBlue = new Color(null, 0, 0, 127);
}

// - UNUSED CODE ............................................................................................
