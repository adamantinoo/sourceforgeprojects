//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonApplication.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonApplication.java,v $
//  LAST UPDATE:    $Date: 2007-09-10 12:56:17 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-07 12:34:19  ldiego
//    - [A0035.01 ] - Create RCP application.
//

package net.sourceforge.rcp.harpoon.app;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import net.sourceforge.rcp.harpoon.ApplicationActionBarAdvisor;

/**
 * This class controls all aspects of the application's execution
 */
public class HarpoonApplication implements IApplication {

	/**
	 * This method is called at application start. It justs creates and starts the workbench with the particular
	 * configuration for tis application.
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		Display display = PlatformUI.createDisplay();
		try {
			// - This line fires the creation of the perspective and all its contents. Also enters main loop
			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
			if (returnCode == PlatformUI.RETURN_RESTART) { return IApplication.EXIT_RESTART; }
			return IApplication.EXIT_OK;
		} finally {
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed()) workbench.close();
			}
		});
	}

	/** Creates the initial windows and sets the default perspective. There should be a default perspective. */
	class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

		private static final String	PERSPECTIVE_ID	= "net.sourceforge.rcp.harpoon.perspective";

		public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			return new ApplicationWorkbenchWindowAdvisor(configurer);
		}

		public String getInitialWindowPerspectiveId() {
			return PERSPECTIVE_ID;
		}

	}

	/** Configures the application window. */
	class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

		public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
			super(configurer);
		}

		public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
			return new ApplicationActionBarAdvisor(configurer);
		}

		/** This metod is called before any window is open. */
		public void preWindowOpen() {
			IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
			configurer.setInitialSize(new Point(700, 600));
			configurer.setShowCoolBar(true);
			configurer.setShowStatusLine(false);
			
			configurer.setShowCoolBar(false); 
			configurer.setShowFastViewBars(false); 
			configurer.setShowMenuBar(true); // Show the application menu bar
			configurer.setShowPerspectiveBar(false); 
			configurer.setShowProgressIndicator(false); 
			configurer.setShowStatusLine(false); 
			configurer.setTitle("HarpoonApplication.ApplicationWorkbenchWindowAdvisor"); 
		}

	}
}

// - UNUSED CODE ............................................................................................
