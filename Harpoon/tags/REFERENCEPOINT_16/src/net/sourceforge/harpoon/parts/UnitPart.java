//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/UnitPart.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:29 $
//  RELEASE:        $Revision: 1.14 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.13  2007-10-01 14:43:59  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//
//    Revision 1.12  2007-09-28 11:24:28  ldiego
//    - [A0059.02] - Lesson 01. Startup window structure.
//    - [A0059.02] - Lesson 01. Map data presented.
//    - [A0059.02] - Lesson 01. Sensor activation / deactivation.
//    - [A0059.02] - Lesson 01. Menu definition.
//    - [A0059.02] - Lesson 01. Open menu definition.
//    - [A0059.02] - Lesson 01. Units.
//
//    Revision 1.11  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.10  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.9  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.8  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.7  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitPart extends GamePart implements PropertyChangeListener {
	private static Logger	logger	= Logger.getLogger("net.sourceforge");

	// - P R O P E R T Y C H A N G E L I S T E N E R - I N T E R F A C E
	/**
	 * The mechanism is not clearly explained. This is the way to receive that there have been changes to the
	 * model properties. This implementation intercepts event of type <em><code>Notification.SET</code></em>
	 * to trigger the change on the content of one of the model properties so new updates to the presentation
	 * take the new data from the model.<br>
	 * This implementation does not delegate other event to the super class implementation because no other
	 * class implements this interface.
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		// if (Unit.CHILDREN.equals(prop))
		// refreshChildren();
		// else
		if (Unit.NAME.equals(prop))
			refreshVisuals();
		else if (Unit.SIDE.equals(prop))
			refreshVisuals();
		else if (Unit.LATITUDE.equals(prop))
			refreshVisuals();
		else if (Unit.LONGITUDE.equals(prop))
			refreshVisuals();
		else if (Unit.DETECTED.equals(prop)) refreshVisuals();

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	// - O V E R R I D E - S E C T I O N
	/**
	 * Updates the layout data for this object. The layout belongs to the Map root element that is associated to
	 * this Part when created by setting the <code>parent</code> property.<br>
	 * It also updates the visible parts of the element that may have changed by the change of a property.<br>
	 * <br>
	 * This is a EditPart, so to get the location we have to access the model and the global coordinate system.<br>
	 * The method calculates the location in XY coordinates for the model DMS coordinates. then moves that point
	 * to the hot spot of the figure because model location is not mapped to the top-left point of the figure.<br>
	 * <br>
	 * All parts have reference to the root part that also have reference to the top model element. We can get
	 * the top-left location from this chain and then calculate the XY point using that reference.
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final Unit model = getUnit();
		final UnitFigure fig = getUnitFigure();

		// - Update other unit attributes
		fig.setSide(model.getSide());

		if (Unit.FRIEND == model.getSide())
			fig.setVisible(true);
		else {
			fig.setVisible(false);
			if (model.isDetected()) fig.setVisible(true);
		}

		logger.log(Level.FINER, "UnitPart data:" + toString());
		// logger.log(Level.FINER, "Unit model type: " + model.getClass().getSimpleName());
		logger.log(Level.FINER, "Unit DMS latitude: " + model.getDMSLatitude());
		logger.log(Level.FINER, "Unit DMS longitude: " + model.getDMSLongitude());

		// - This block calculates the coordinates XY for the DMS model location.
		final Point xyloc = RootMapPart.convertToMap(getRootPart(), model);
		logger.log(Level.FINER, "Calculated XY location: " + xyloc);

		// - Move the point depending on the figure hotspot location.
		final Point hotspot = fig.getHotSpot();
		logger.log(Level.FINER, "Obtained hotspot difference: " + hotspot);
		xyloc.x -= hotspot.x;
		xyloc.y -= hotspot.y;
		// xyloc.x += 1;
		// xyloc.y += 1;

		final Dimension size = fig.getSize();
		logger.log(Level.FINER, "Calculated figure size: " + size);
		final Rectangle bound = new Rectangle(xyloc, size);
		logger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bound);

		// - This triggers the parent layout to revalidate the position of the element.
		((AbstractGraphicalEditPart) getParent()).setLayoutConstraint(this, getFigure(), bound);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("[UnitPart:");
		// TODO Create a selection decode method to print string values
		buffer.append(getSelected()).append("-");
		buffer.append(getUnit().getClass().getSimpleName()).append("-");
		buffer.append(getUnitFigure().getClass().getSimpleName()).append("]");
		// buffer.append(super.toString()).append("]");
		return buffer.toString();
	}

	// int getAnchorOffset() {
	// return 9;
	// }

	// - P R I V A T E - S E C T I O N
	// private Point convertToMap(Unit model) {
	// // - Get the unit coordinates and the top-left coordinates.
	// final DMSCoordinate unitLat = model.getDMSLatitude();
	// final DMSCoordinate unitLon = model.getDMSLongitude();
	//
	// // - Get the model root where there are stored the conversion properties.
	// final RootMapPart rootPart = (RootMapPart) getParent();
	// final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
	// final DMSCoordinate topLat = modelRoot.getDMSLatitude();
	// final DMSCoordinate topLon = modelRoot.getDMSLongitude();
	// final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
	// final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();
	//
	// // RootMapUnit rootMap = figure;
	//
	// // RootMapUnit rootUnit = (RootMapUnit) this.getParent().getModel();
	// // HarpoonMap map = rootUnit.getMap();
	// // DMSCoordinate topLat = map.getDMSLatitude();
	// // DMSCoordinate topLon = map.getDMSLongitude();
	//
	// // - Generate a DMSPoint to perform the calculations.
	// final DMSPoint point = new DMSPoint(topLat, topLon);
	// final DMSPoint vector = point.offsetPoint(new DMSPoint(unitLat, unitLon));
	//
	// // - Get the multipliers to convert from DMS to XY
	// // long latSpan = map.getLatitude2Zoom().toSeconds();
	// // long lonSpan = map.getLongitude2Zoom().toSeconds();
	//
	// // - Calculate the final XY coordinates
	// final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
	// int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
	// // - Invert the sign for the Y axis because positive coordinates are in the upward direction
	// yy = -1 * yy;
	//
	// // UnitFigure fig = this.getUnitFigure();
	// // // - Move the point depending on the figure hotspot location.
	// // Dimension hotspot = fig.getHotSpot();
	// // xx -= hotspot.width;
	// // yy -= hotspot.height;
	//
	// return new Point(xx, yy);
	// // [01]]
	// }
}
// - UNUSED CODE ............................................................................................
