//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: RightUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/RightUnit.java,v $
//  LAST UPDATE:    $Date: 2007-10-03 16:50:10 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//

package net.sourceforge.rcp.harpoon.test;

import net.sourceforge.harpoon.model.WarUnit;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
// - P U B L I C S E C T I O N
// - P R O T E C T E D S E C T I O N
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public class RightUnit extends WarUnit {
}

// - UNUSED CODE ............................................................................................
