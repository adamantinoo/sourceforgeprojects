//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ReferencePoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/ReferencePoint.java,v $
//  LAST UPDATE:    $Date: 2007-09-25 11:44:40 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public class ReferencePoint extends Unit {
	private static final int	width	= 6;

	public int getWidth() {
		return width;
	}
}

// - UNUSED CODE ............................................................................................
