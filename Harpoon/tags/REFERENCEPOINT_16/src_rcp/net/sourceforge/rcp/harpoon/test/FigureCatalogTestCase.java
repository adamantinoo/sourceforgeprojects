//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: FigureCatalogTestCase.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/FigureCatalogTestCase.java,v $
//  LAST UPDATE:    $Date: 2007-10-05 11:24:44 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-10-01 14:44:40  ldiego
//    - DEFECT The file menu has a wrong name.
//    - DEFECT Detection states.
//    - DEFECT Direction value is not shown.
//    - DEFECT The radar range is not shown.
//    - DEFECT Enemy units are shown even when not detected.
//    - DEFECT Sensors have not to be selected.
//    - DEFECT When the sensor is unselected it is not deleted.
//    - [A0059.03] - Lesson 01. Unit properties. Editable attributes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import junit.framework.TestCase;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.app.HarpoonConstants;

// - CLASS IMPLEMENTATION ...................................................................................
public class FigureCatalogTestCase extends TestCase {
	public void testFigureCatalog() throws Exception {
		final FileOutputStream fos = new FileOutputStream(
				"U:/ldiego/Workstage/3.3_BaseWorkspace/HarpoonRCP/sceneries/figurecatalog.harpoon");
		final ObjectOutputStream oos = new ObjectOutputStream(fos);

		final RootMapUnit model = new RootMapUnit();
		figureCatalog(model);
		oos.writeObject(model);

		oos.close();
	}

	private void figureCatalog(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();
		// MovementPath path = new MovementPath(this);

		// // - Create Friend units
		// AirportUnit base = new AirportUnit();
		// base.setName("Interoceanic airport");
		// base.setSide(Unit.FRIEND);
		// base.setLatitude(new DMSCoordinate(0, 55, 0, 'S'));
		// base.setLongitude(new DMSCoordinate(0, 30, 0, 'E'));
		// model.addChild(base);

		final WarUnit air = new WarUnit();
		// - Fields for a Unit
		air.setName("F16 Beta");
		air.setLatitude(new DMSCoordinate(0, 26, 0, 'S'));
		air.setLongitude(new DMSCoordinate(0, 26, 0, 'E'));
		air.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		air.setUnitType(HarpoonConstants.UNIT_AIR);
		air.setModel("F16 Figther");
		final MovementPath path = new MovementPath(air);
		path.addPoint(new DMSCoordinate(0, 10, 0, 'S'), new DMSCoordinate(0, 20, 0, 'E')); // surface.setDirection(90);
		air.setSpeed(500);
		air.setMovementPath(path);
		// - Data for a WarUnit.
		model.addChild(air);
	}
}

// - UNUSED CODE ............................................................................................
