//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonAppTests.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonAppTests.java,v $
//  LAST UPDATE:    $Date: 2007-09-13 13:30:24 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package harpoon.testcases;

// - IMPORT SECTION .........................................................................................

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;


// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonAppTests extends HarpoonTestCase {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	public void testOpenScenery() throws Exception {
		initializeSWT();
		app.menuOpenEscenery();
		
		String expectedPath = "U:\\ldiego\\Workstage\\Harpoon\\sceneries";
		assertEquals("Check scenary directory", expectedPath, app.getSceneryPath());
		String expectedFile = "U:\\ldiego\\Workstage\\Harpoon\\sceneries\\TestScenery.scenery";
		assertEquals("Check scenary name", expectedFile, app.getSceneryName());
	}
}

// - UNUSED CODE ............................................................................................
