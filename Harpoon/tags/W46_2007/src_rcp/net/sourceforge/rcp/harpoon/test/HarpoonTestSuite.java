//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonTestSuite.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/net/sourceforge/rcp/harpoon/test/HarpoonTestSuite.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:35:04 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2.2.1  2007-10-31 14:44:38  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.2  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-13 13:30:24  ldiego
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import junit.framework.Test;
import junit.framework.TestSuite;
import net.sf.harpoon.test.SceneryLesson02Test;

// - CLASS IMPLEMENTATION ...................................................................................
public class HarpoonTestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Harpoon Simulation Test Suite. Draft 00.05 L02_MovementPath");
		// $JUnit-BEGIN$
		suite.addTestSuite(SceneryLesson02Test.class);
		// $JUnit-END$
		return suite;
	}

}

// - UNUSED CODE ............................................................................................
