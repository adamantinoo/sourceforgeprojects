//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SensorsPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/SensorsPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.HarpoonFigureFactory;
import net.sourceforge.harpoon.figures.SensorsFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.SensorsModel;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class SensorsPart extends AbstractGraphicalEditPart {
	// - O V E R R I D E S E C T I O N
	@Override
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		final Object model = getModel();
		Figure fig = new Figure();
		if (model instanceof SensorsModel) {
			final SensorsModel sensor = (SensorsModel) getModel();
			// - Identify the subtype for this model.
			final String subType = sensor.getUnitType();

			// - Create and initialize the figure
			fig = HarpoonFigureFactory.createFigure(sensor, subType);
		}
		return fig;
	}

	@Override
	protected void createEditPolicies() {
		// EMPTY method. Needs to be implemented
	}

	@Override
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		final SensorsFigure fig = (SensorsFigure) getFigure();
		final SensorsModel model = (SensorsModel) getModel();

		// - Update figure visuals from current model data.
		// fig.setRadarState(model.getRadarState());
		// fig.setSonarState(model.getSonarState());
		// fig.setECMState(model.getECMState());

		// TODO Get the coordinated from the parent.
		// - This block calculates the coordinates XY for the DMS model location.
		final Point xyloc = convertToMap(model.getReferent());
		HarpoonLogger.log(Level.FINER, "Calculated XY location: " + xyloc);

		// - Move the point depending on the figure hotspot location.
		final Dimension hotspot = fig.getHotSpot();
		xyloc.x -= hotspot.width;
		xyloc.y -= hotspot.height;
		// xyloc.x =100;
		// xyloc.y =100;

		final Dimension size = fig.getSize();
		HarpoonLogger.log(Level.FINER, "Calculated figure size: " + size);
		final Rectangle bound = new Rectangle(xyloc, size);
		HarpoonLogger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bound);

		// - This triggers the parent layout to revalidate the position of the element.
		((RootMapPart) getParent()).setLayoutConstraint(this, getFigure(), bound);

		// // - This triggers the parent layout to revalidate the position of the element.
		// // - But the parent is not the ancestor but the war unit. Get the grandparent.
		// EditPart par = getParent();
		// EditPart grandpa = par.getParent();
		// ((AbstractGraphicalEditPart) grandpa).setLayoutConstraint(this, getFigure(), bound);
	}

	private Point convertToMap(Unit model) {
		// - Get the unit coordinates and the top-left coordinates.
		final DMSCoordinate unitLat = model.getDMSLatitude();
		final DMSCoordinate unitLon = model.getDMSLongitude();

		// - Get the model root where there are stored the conversion properties.
		final RootMapPart rootPart = (RootMapPart) getParent();
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// RootMapUnit rootMap = figure;

		// RootMapUnit rootUnit = (RootMapUnit) this.getParent().getModel();
		// HarpoonMap map = rootUnit.getMap();
		// DMSCoordinate topLat = map.getDMSLatitude();
		// DMSCoordinate topLon = map.getDMSLongitude();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint point = new DMSPoint(topLat, topLon);
		final DMSPoint vector = point.offsetPoint(new DMSPoint(unitLat, unitLon));

		// - Get the multipliers to convert from DMS to XY
		// long latSpan = map.getLatitude2Zoom().toSeconds();
		// long lonSpan = map.getLongitude2Zoom().toSeconds();

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		// //- Get the figure of the parent
		// EditPart par = this.getParent();
		// WarPart paren=(WarPart) par;
		// UnitFigure fig = (UnitFigure) paren.getFigure();
		// // - Move the point depending on the figure hotspot location.
		// // Dimension hotspot = fig.getHotSpot();
		// Dimension hotspot = new Dimension(50,50);
		// xx -= hotspot.width;
		// yy -= hotspot.height;

		return new Point(xx, yy);
	}
}

// - UNUSED CODE ............................................................................................
