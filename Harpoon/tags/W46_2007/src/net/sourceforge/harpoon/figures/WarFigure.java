//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/WarFigure.java,v $
//  LAST UPDATE:    $Date: 2007-11-02 09:34:49 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3.2.1  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.3  2007-10-02 09:03:44  ldiego
//    - [B0034.02] - UI behavior for selection. Multiple selection.
//    - DEFECT Surface hotspot is wrongly located.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.figures;


// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P R I V A T E S E C T I O N
public class WarFigure extends PositionedLabeledFigure {

	private boolean		radar				= false;
	private int				radarRange	= 0;
	private boolean		ECM					= false;
	private int				EMCRange		= 0;

	// - P U B L I C S E C T I O N
	@Override
	public void init() {
		super.init();
	}

	// - G E T T E R S / S E T T E R S
	public void setRadarState(boolean radarSate) {
		// TODO Auto-generated method stub
		radar = radarSate;
		this.repaint();
	}

	public void setECMState(boolean state) {
		// TODO Auto-generated method stub
		ECM = state;
		this.repaint();
	}

	// protected void paintChildren(Graphics graphics) {
	// IFigure child;
	//		
	// List chi = this.getChildren();
	//
	// Rectangle clip = Rectangle.SINGLETON;
	// for (int i = 0; i < chi.size(); i++) {
	// child = (IFigure)chi.get(i);
	// if (child.isVisible() && child.intersects(graphics.getClip(clip))) {
	// // graphics.clipRect(child.getBounds());
	// graphics.clipRect(Rectangle.SINGLETON);
	// child.paint(graphics);
	// graphics.restoreState();
	// }
	// }
	// }
	@Override
	// protected void paintFigure(Graphics graphics) {
	// // - This paints the left side and the icon
	// super.paintFigure(graphics);
	//
	// // - Paint the sensor range circles if the sensors are activated
	// if (radar) {
	// // TODO Sensors are structures because they contain the state and the range
	// // - Ranges are already expressed in pixels
	// // - Move the center of the circle to the hotspot of the figure.
	// final Dimension hot = getHotSpot();
	// final Point loc = getLocation();
	// final Rectangle sensorBounds = new Rectangle();
	// sensorBounds.x = loc.x + hot.width - radarRange;
	// sensorBounds.y = loc.y + hot.height - radarRange;
	// sensorBounds.width = radarRange * 2;
	// sensorBounds.height = radarRange * 2;
	//
	// final Rectangle bo = getBounds().getCopy();
	// graphics.fillRectangle(bo);
	// graphics.drawOval(sensorBounds.expand(3, 3));
	// graphics.drawOval(sensorBounds.expand(-1, -1));
	// graphics.setForegroundColor(ColorConstants.black);
	// graphics.drawOval(bo);
	// graphics.drawOval(bo.expand(3, 3));
	//
	// graphics.setForegroundColor(ColorConstants.black);
	// graphics.drawOval(sensorBounds);
	// }
	// if (ECM) {
	// // TODO Sensors are structures because they contain the state and the range
	// // - Ranges are already expressed in pixels
	// // - Move the center of the circle to the hotspot of the figure.
	// final Dimension hot = getHotSpot();
	// final Point loc = getLocation();
	// final Rectangle sensorBounds = new Rectangle();
	// sensorBounds.x = loc.x + hot.width - EMCRange;
	// sensorBounds.y = loc.y + hot.height - EMCRange;
	// sensorBounds.width = EMCRange * 2;
	// sensorBounds.height = EMCRange * 2;
	//
	// graphics.setForegroundColor(ColorConstants.lightGreen);
	// graphics.fillOval(sensorBounds);
	// }
	// }
	// @Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("WarFigure(");
		buffer.append(")");
		return buffer.toString();
	}

	public void setRadarRange(int radarPixels) {
		// TODO Auto-generated method stub
		radarRange = radarPixels;
	}

	public void setECMRange(int pixels) {
		// TODO Auto-generated method stub
		EMCRange = pixels;
	}
}

// - UNUSED CODE ............................................................................................
