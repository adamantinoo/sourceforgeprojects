//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonSelectionListener.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonSelectionListener.java,v $
//  LAST UPDATE:    $Date: 2007-09-25 11:44:58 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-09-21 11:19:47  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.4  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.3  2007-09-17 15:11:39  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.2  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import java.util.Iterator;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;

import net.sourceforge.rcp.harpoon.editors.HarpoonSceneryEditor;
import net.sourceforge.rcp.harpoon.views.SelectionView;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This is the selection listener used to maintain the selection made on the editor reflected on the Selection
 * Data view. The methods receive a selection from the editor and it will filter the selection to just the
 * selectable items that may be reflected on the view. For example, sensors are not to be shown on the
 * Selection data view because they belong to some unit.
 */
public class HarpoonSelectionListener implements ISelectionListener {

	private HarpoonSceneryEditor	editor;
	// private Hashtable selectionContent = new Hashtable();
	private SelectionView					selectionView;

	// - C O N S T R U C T O R S
	/**
	 * Create the listener and receive the editor reference. This allows to update the editor actions depending
	 * on the selection.
	 */
	public HarpoonSelectionListener(HarpoonSceneryEditor editor) {
		// TODO Auto-generated constructor stub
		this.editor = editor;
		// this.selectionView = selectionView;
	}

	// - P U B L I C S E C T I O N
	/**
	 * This event is fired anytime the selection is modified. The parameter received id the new selection list.
	 * From this selection I have to filter out the non selectable elements that have no influence with the
	 * selection presentation.
	 * 
	 * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart,
	 *      org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		// TODO Review this implementation and optimize the generation of a new selection. It is possible that
		// better performance can be obtained by filtering this at the next call level.
		// StructuredSelection sel = (StructuredSelection) selection;
		// StructuredSelection filteredSel = new StructuredSelection();
		StructuredSelection filteredSel2 = new StructuredSelection();
		Object[] filteredSel = new AbstractGraphicalEditPart[((StructuredSelection) selection).size()];
		// // int size = sel.size();
		if (!selection.isEmpty()) {
			Iterator<AbstractGraphicalEditPart> it = ((StructuredSelection) selection).iterator();
			// selectionContent = new Hashtable();
			int index = 0;
			while (it.hasNext()) {
				AbstractGraphicalEditPart element = it.next();
				if (element instanceof SensorPart)
					continue;
				else
					filteredSel[index++] = element; // - Copy to the new selection to pass away.

				// Unit model = ((Unit) element.getModel());
				// String name = model.toString();
				// // - Add the selected element to the selection
				// selectionContent.put(name, element);
				// // int index = 1;
			}
			filteredSel2 = new StructuredSelection(filteredSel);

		}
		editor.updateActions(editor.getEditActions());
		getSelectionView().updateSelection(filteredSel2);
	}

	// - P R I V A T E S E C T I O N
	private SelectionView getSelectionView() {
		if (null == this.selectionView) {
			this.selectionView = SelectionView.getSingleton();
		}
		return selectionView;
	}
}

class SensorPart extends AbstractGraphicalEditPart {

	@Override
	protected IFigure createFigure() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void createEditPolicies() {
		// TODO Auto-generated method stub

	}
}

// - UNUSED CODE ............................................................................................
