//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: HarpoonConstants.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/app/HarpoonConstants.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:43:56 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.1  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
// - O V E R R I D E S E C T I O N
public interface HarpoonConstants {
	public static final String			SURFACE_UNIT						= "surface";
	public static final String			AIR_UNIT						= "air";
	String			SUBMARINE_UNIT						= "submarine";
	public static final String	SENSOR_UNIT	= "sensors";
	public static final String	M_OPEN	= "open";
	public static final String	M_HELP	= "harpoonhelp";

}

// - UNUSED CODE ............................................................................................
