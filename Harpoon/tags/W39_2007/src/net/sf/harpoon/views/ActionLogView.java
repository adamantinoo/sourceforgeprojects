//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: ActionLogView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/views/ActionLogView.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:19:47 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-17 15:11:40  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//

package net.sourceforge.rcp.harpoon.views;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This view is responsible to display the properties of the selected unit is there is only one unit selected
 * or is the selection contains more than one item, it should display the list of elements in the selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection
 * to the newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class ActionLogView extends ViewPart {
	public static final String	ID	= "net.sourceforge.rcp.harpoon.app.actionlogview";
	private static Text					logText;

	// - P U B L I C S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	public void createPartControl(Composite parent) {
		// singleton = this;
		// - Create the base parts until the link Composite.
		logText = new Text(parent, SWT.MULTI | SWT.WRAP);
		logText.setText("<empty current selection>");
		logText.setLayoutData(new GridData(GridData.FILL_BOTH));
	}

	public static void logAction(String message) {
		logText.append(message);
		logText.append("\n");
	}

	@Override
	public void setFocus() {
		System.out.println("Focus on view ActionLogView");
	}
}

// - UNUSED CODE ............................................................................................
