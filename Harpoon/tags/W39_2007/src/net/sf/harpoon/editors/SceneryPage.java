//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SceneryPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/editors/SceneryPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:16:29 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//									Copyright (c) 2000, 2003 IBM Corporation and others.
//									All rights reserved. This program and the accompanying materials 
//									are made available under the terms of the Common Public License v1.0
//									which accompanies this distribution, and is available at
//									http://www.eclipse.org/legal/cpl-v10.html
//
//									Contributors:
//									    IBM Corporation - initial API and implementation
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.3  2007-09-12 11:26:06  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-10 12:56:18  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//

package net.sourceforge.rcp.harpoon.editors;

import org.eclipse.gef.DefaultEditDomain;
import org.eclipse.gef.editparts.ScalableFreeformRootEditPart;
import org.eclipse.gef.ui.parts.GraphicalViewerImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.parts.HarpoonPartFactory;

/**
 * This the the main editor page. It has been modified to support for the main gae window where the scenery
 * map is shown and then the game units are displayed.
 * 
 * @author Luis de Diego
 * @author Gunnar Wagenknecht
 */
public class SceneryPage extends WorkflowPage {
	/**
	 * Creates a new SceneryPage instance.
	 * 
	 * By design this page uses its own <code>EditDomain</code>. The main goal of this approach is that this
	 * page has its own undo/redo command stack.<br>
	 * Undo/redo is something that may be disabled in the game. Set the domain to the default domain used in
	 * other examples.
	 * 
	 * @param parent
	 *          the parent multi page editor
	 */
	public SceneryPage(HarpoonSceneryEditor parent) {
		super(parent, new DefaultEditDomain(parent)); // Changed to a DefaultEditDomain
	}

	/**
	 * Return the name for this page. It seems that is something superfluous.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#getPageName()
	 */
	protected String getPageName() {
		// TODO See when is used this name in the display area.
		return "Main Scenery";
	}

	/**
	 * This method is called when the interface creates the page to be displayed in the editor panel. The
	 * functionality is to create the graphic elements that compose the page and initialize them.<br>
	 * <br>
	 * This particular implementation we discard the palette creation and we set the main contents to our
	 * graphical viewer. Then we load the model into the viewer to create the controllers and the view elements.
	 * 
	 * @see com.ibm.itso.sal330r.gefdemo.editor.AbstractEditorPage#createPageControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl(Composite parent) {
		// - Create a simple control and set the full fill layout. This is the base control for the viewer.
		// Composite c1 = new Composite(parent, SWT.NONE);
		// c1.setBackground(parent.getBackground());
		// c1.setLayout(new FillLayout());
		// GraphicalViewerImpl vi = new GraphicalViewerImpl();
		// vi.setControl(can);
		// viewer = vi;
		Canvas can = new Canvas(parent, SWT.NONE);
		can.setLayout(new FillLayout());
		viewer = new GraphicalViewerImpl();
		viewer.setControl(can);
		viewer.setRootEditPart(new ScalableFreeformRootEditPart());

		// hook the viewer into the editor
		registerEditPartViewer(viewer);

		// - Initialize the viewer with input
		viewer.setEditPartFactory(new HarpoonPartFactory());
		viewer.setContents(getSceneryModel());
	}

	/**
	 * Returns the scenery root of the model that is being played.
	 * 
	 * @return the scenery model
	 */
	protected RootMapUnit getSceneryModel() {
		return getWorkflowEditor().getWorkflow();
	}

}

// - UNUSED CODE ............................................................................................
