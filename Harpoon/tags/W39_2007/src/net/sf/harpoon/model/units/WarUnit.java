//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/WarUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-28 11:24:28 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.model;

//- IMPORT SECTION .........................................................................................
import java.io.Serializable;
import java.util.Calendar;

// - CLASS IMPLEMENTATION ...................................................................................
public class WarUnit extends MovableUnit {
	private static final long		serialVersionUID			= -3514624844988656815L;
	// FIXME Change values for this constants to numeric to make state implementation easier.
	private static final String	NOT_DETECTED_STATE		= "NOT_DETECTED_STATE";
	private static final String	FIRST_DETECTION_STATE	= "FIRST_DETECTION_STATE";
	private static final String	DETECTED_STATE				= "DETECTED_STATE";
	private static final String	IDENTIFIED_STATE			= "IDENTIFIED_STATE";
	private static final String	LOST_CONTACT_STATE		= "LOST_CONTACT_STATE";
	// private static final int[] states=

	/** Stores the detection or detection phase status for enemy units. */
	private UnitState						detected							= new UnitState(NOT_DETECTED_STATE);
	// private transient boolean activeRadar=false;
	// private transient boolean activeSonar=false;
	// private transient boolean activeECM=false;

	public static final String	RADAR									= "radar";
	public static final String	SONAR									= "sonar";
	public static final String	ECM										= "ecm";
	public static final String	DETECTED							= "detected";

	private SensorsModel				sensors								= new SensorsModel(this);

	// @SuppressWarnings("unused")
	// private final transient LinkedList<Weapon> weapons = new LinkedList<Weapon>();

	// - G E T T E R S / S E T T E R S

	// public boolean getDetected() {
	// return detected;
	// }
	public boolean isDetected() {
		if (DETECTED_STATE == detected.getState())
			return true;
		else if (IDENTIFIED_STATE == detected.getState()) return true;
		return false;
	}

	// - G E T T E R S / S E T T E R S
	// [01]

	// - P U B L I C S E C T I O N
	/**
	 * Initiates the detection mechanism. A unit detected keep this state at least for 60 seconds. After that time the
	 * detection state decays until the unit is no longer detected. THis is performed by some unit states. The detection
	 * resets the detection time and increments the detection state.
	 */
	public void fireDetection() {
		detected.advance();
		detected.stamp();
		if (isDetected()) firePropertyChange(WarUnit.DETECTED, false, isDetected());
	}

	public void goUndetected() {
		Calendar time = detected.getLastStamp();
		Calendar now = Calendar.getInstance();
		// - Compute if detection time has elapsed
		time.add(Calendar.SECOND, 60);
		if (now.after(time)) {
			detected = new UnitState(LOST_CONTACT_STATE);
			detected.stamp();
			firePropertyChange(WarUnit.DETECTED, true, false);
		}
	}

	@Override
	public String toString() {
		return "WarUnit-" + name + " (" + side + ")";
	}

	// [02]
	public SensorsModel getSensorInformation() {
		if (null == sensors) sensors = new SensorsModel(this);
		return sensors;
	}

	public int getRadarRange() {
		// TODO Auto-generated method stub
		return 20;
	}

	public int getSonarRange() {
		// TODO Auto-generated method stub
		return 10;
	}

	public int getRadioRange() {
		// TODO Auto-generated method stub
		return 30;
	}

	public void setsensors(SensorsModel sensor) {
		sensors = sensor;
	}

	// - CLASS IMPLEMENTATION ...................................................................................
	class UnitState implements Serializable {
		private static final long	serialVersionUID	= 4135940623738210352L;
		private String						state;
		private int								stateCode					= 0;
		private Calendar					lastStateTime			= Calendar.getInstance();

		public UnitState(String state) {
			this.state = state;
			stateCode = decode(state);
		}

		public Calendar getLastStamp() {
			if (null == lastStateTime) lastStateTime = Calendar.getInstance();
			return lastStateTime;
		}

		public void stamp() {
			lastStateTime = Calendar.getInstance();
		}

		public String getState() {
			return state;
		}

		public void advance() {
			// FIXME Change values for this constants to numeric to make state implementation easier.
			if (IDENTIFIED_STATE.equals(state)) return;
			if (LOST_CONTACT_STATE.equals(state)) {
				state = NOT_DETECTED_STATE;
				stateCode = decode(state);
				return;
			}
			stateCode++;
			state = undecode(stateCode);
		}

		private int decode(String state) {
			if (NOT_DETECTED_STATE.equals(state)) return 0;
			if (FIRST_DETECTION_STATE.equals(state)) return 1;
			if (DETECTED_STATE.equals(state)) return 2;
			if (IDENTIFIED_STATE.equals(state)) return 3;
			if (LOST_CONTACT_STATE.equals(state)) return 4;
			return 0;
		}

		private String undecode(int code) {
			if (0 == code) return NOT_DETECTED_STATE;
			if (1 == code) return FIRST_DETECTION_STATE;
			if (2 == code) return DETECTED_STATE;
			if (3 == code) return IDENTIFIED_STATE;
			if (4 == code) return LOST_CONTACT_STATE;
			return NOT_DETECTED_STATE;
		}

	}
}

// - UNUSED CODE ............................................................................................
// [01]
// public boolean getRadarState() {
// return activeRadar;
// }
//
// public boolean getSonarState() {
// return activeSonar;
// }
//
// public boolean getECMState() {
// return activeECM;
// }

// public void activateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) {
// activeRadar = true;
// firePropertyChange(WarUnit.RADAR, false, true);
// }
// if (sensor.equals(WarUnit.SONAR)) {
// activeSonar = true;
// firePropertyChange(WarUnit.SONAR, false, true);
// }
// if (sensor.equals(WarUnit.ECM)) {
// activeECM = true;
// firePropertyChange(WarUnit.ECM, false, true);
// }
// // final int oldSide = this.side;
// // this.side = side;
// // - Fire a notification to all listeners to update the presentation layer.
//
// }

// public void deActivateSensor(String sensor) {
// if (sensor.equals(WarUnit.RADAR)) activeRadar = false;
// if (sensor.equals(WarUnit.SONAR)) activeSonar = false;
// if (sensor.equals(WarUnit.ECM)) activeECM = false;
// }

// [02]
// public SensorsModel getSensors() {
// if (null == this.sensors) this.sensors = new SensorsModel(this);
// return this.sensors;
// }
// public boolean getRadarState() {
// return getSensors().getRadarState();
// }
//
// public void setRadarState(boolean newState) {
// final boolean oldState = getSensors().getRadarState();
// getSensors().setRadarState(newState);
// firePropertyChange(RADAR, oldState, newState);
// }
//
// public boolean getECMState() {
// return getSensors().getECMState();
// }
//
// public void setECMState(boolean newState) {
// final boolean oldState = getSensors().getECMState();
// getSensors().getECMState(newState);
// firePropertyChange(ECM, oldState, newState);
// }
