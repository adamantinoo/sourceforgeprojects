//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableFigure3.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/MovableFigure3.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.2  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.1  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.app.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
public class MovableFigure3 extends UnitFigure {
	private static final int	MARGIN					= 2;
	private static final int	CHAR_WIDTH			= 6;
	private static final int	CHAR_HEIGHT			= 13;

	private final Label				directionLabel	= new Label("000");
	private final Label				speedLabel			= new Label("00");
	private MovementPath			path						= new MovementPath();

	public MovableFigure3() {
	}

	// - P U B L I C S E C T I O N
	/**
	 * Create the figure structure and the list of drawing elements once all data has been stored in its place. This
	 * figure has two parts.
	 * <ul>
	 * <li>The left column contains the direction and speed labels.</li>
	 * <li>The center contains the iconic representation.</li>
	 * </ul>
	 */
	public void init() {
		// - Create the complex internal parts of this figure.
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = MARGIN;
		grid.marginHeight = MARGIN;
		grid.marginWidth = MARGIN;
		grid.verticalSpacing = 0;
		setLayoutManager(grid);

		// - Add the left part of the figure. It contains the speed and direction.
		final Figure labelContainer = createLeftSide();
		this.add(labelContainer);
		// - Add the icon drawing class instance.
		this.add(iconic);

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
		this.repaint();
	}

	/**
	 * Return a dimension with the vector from the top-left coordinate to the hotspot figure location. This takes on
	 * account the size of the figure parts and the location of their children hotspots.
	 */
	@Override
	public Dimension getHotSpot() {
		final Dimension hot = iconic.getHotSpot();
		// - Add the distance of the left labels.
		final Dimension leftSize = getLeftSideSize();
		final Dimension fullSize = getPreferredSize();
		return new Dimension(fullSize.width / 2, fullSize.height / 2);
		// return new Dimension(hot.width + leftSize.width, hot.height/ 2);
	}

	@Override
	public Dimension getPreferredSize(int wHint, int hHint) {
		HarpoonLogger.info(">>> ENTERING");
		// - Get the sized of the composition objects.
		final Dimension iconicSize = iconic.getSize();
		final Dimension leftSize = getLeftSideSize();
		final Dimension fullSize = new Dimension(0, 0);
		fullSize.width = MARGIN + leftSize.width + MARGIN + iconicSize.width + MARGIN;
		fullSize.height = MARGIN + Math.max(iconicSize.height, leftSize.height) + MARGIN;
		HarpoonLogger.info("iconicSize = " + iconicSize);
		HarpoonLogger.info("leftSize = " + leftSize);
		HarpoonLogger.info("fullSize = " + fullSize);
		HarpoonLogger.info("<<< EXITING");
		return fullSize;
	}

	// @Override
	// protected void paintFigure(Graphics graphics) {
	// // - This type of figures are only painted if they are visible
	// // if (isVisible()) {
	// // - This paints the left side and the icon
	// super.paintFigure(graphics);
	//
	// // - Paint the movement path if this unit is selected
	// if (isSelected()) {
	// path.paint();
	// }
	// // }
	// }

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("MovableFigure3(speed" + speedLabel.getText());
		buffer.append(",");
		buffer.append("direction:" + directionLabel.getText());
		buffer.append(")");
		return buffer.toString();
	}

	// - G E T T E R S / S E T T E R S
	/**
	 * The method sets the Figure values to be used for the presentation of the figure speed. The figure speed is
	 * represented as a numeric label with the speed in knots.
	 */
	public void setSpeed(int newSpeed) {
		speedLabel.setText(new Integer(newSpeed).toString());
		invalidate();
		this.setSize(this.getPreferredSize(-1, -1));
	}

	public void setDirection(int newDirection) {
		directionLabel.setText(new Integer(newDirection).toString());
		invalidate();
		this.setSize(this.getPreferredSize(-1, -1));
	}

	/**
	 * Set the movement path ordered to this unit. The presentation is a connected list of display points that define the
	 * path of movement ordered to the Unit.
	 * 
	 * @param movementPath
	 */
	public void setMovementPath(MovementPath movementPath) {
		// TODO Auto-generated method stub
		path = movementPath;
		this.repaint();
	}

	/**
	 * Set the drawing color depending on the side of the game being played. FRIENDS are marked green, while FOES are
	 * marked red. NEUTRAL are yellow and other are blue.
	 */
	@Override
	public void setSide(int newSide) {
		// DEBUG I think this property is not necessary.
		// this.side = newSide;
		if (Unit.FRIEND == newSide) setColor(HarpoonColorConstants.FRIEND);
		if (Unit.FOE == newSide) setColor(HarpoonColorConstants.FOE);
		if (Unit.NEUTRAL == newSide) setColor(HarpoonColorConstants.NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	// - P R I V A T E S E C T I O N
	private Dimension getLeftSideSize() {
		final int maxChars = StrictMath.max(speedLabel.getText().length(), directionLabel.getText().length());
		final Dimension leftLabelSize = new Dimension((maxChars + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT * 2);
		// leftLabelSize.width+=MARGIN;
		// leftLabelSize.height+=MARGIN+MARGIN;
		return leftLabelSize;
	}

	private Figure createLeftSide() {
		final Figure labelContainer = new Figure();
		final GridLayout grid = new GridLayout();
		grid.numColumns = 1;
		grid.verticalSpacing = 0;
		grid.horizontalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		labelContainer.setLayoutManager(grid);
		speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// speedlabel.setBorder(new LineBorder(1));
		labelContainer.add(speedLabel);
		directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
		// dirLabel.setBorder(new LineBorder(1));
		labelContainer.add(directionLabel);
		labelContainer.validate();
		return labelContainer;
	}
}

// - UNUSED CODE ............................................................................................
