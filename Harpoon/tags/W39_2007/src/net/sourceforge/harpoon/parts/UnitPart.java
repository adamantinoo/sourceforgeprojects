//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/UnitPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-28 11:24:28 $
//  RELEASE:        $Revision: 1.12 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.11  2007-09-27 16:45:17  ldiego
//    - DEFECT When a unit is selected the properties are not updated.
//    - [A0059.03] - Lesson 01. Unit properties. Editable sensor status.
//    - [A0019.01] - Sensors can be activated.
//
//    Revision 1.10  2007-09-26 16:56:35  ldiego
//    - [A0089.01] - Activate the game loop.
//
//    Revision 1.9  2007-09-25 11:44:41  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//
//    Revision 1.8  2007-09-21 11:22:26  ldiego
//    - [A0013.01] - Compose the main screen distribution.
//    - [A0009.01] - Presentation coordinate data.
//    - DEFECT The presentation of the location is not valid.
//    - DEFECT Open Action Log... menu item is located in the wrong menu.
//    - DEFECT Startup sizes are small.
//
//    Revision 1.7  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.6  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.5  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.4  2007-09-10 12:55:59  ldiego
//    - [A0040.01 ] - Activate the menu and open an stored scenery.
//
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      goind thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Level;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.geom.DMSPoint;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class UnitPart extends AbstractGraphicalEditPart implements PropertyChangeListener {
	// - C O N S T R U C T O R S
	// REVIEW createEditPolicies
	@Override
	protected void createEditPolicies() {
		// EMPTY
	}

	// REVIEW performRequest

	// - P U B L I C S E C T I O N
	/**
	 * Activation and deactivation is something still not investigated. Keep the code for property management.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractGraphicalEditPart#activate()
	 */
	@Override
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		getUnit().addPropertyChangeListener(this);
		super.activate();
	}

	@Override
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		getUnit().removePropertyChangeListener(this);
		super.deactivate();
	}

	// - P R I V A T E S E C T I O N
	/**
	 * Updates the layout data for this object. The layout belongs to the Map root element that is associated to this Part
	 * when created by setting the <code>parent</code> property.<br>
	 * It also updates the visible parts of the element that may have changed by the change of a property.<br>
	 * <br>
	 * This is a EditPart, so to get the location we have to access the model and the global coordinate system.<br>
	 * The method calculates the location in XY coordinates for the model DMS coordinates. then moves that point to the
	 * hot spot of the figure because model location is not mapped to the top-left point of the figure.<br>
	 * <br>
	 * All parts have reference to the root part that also have reference to the top model element. We can get the
	 * top-left location from this chain and then calculate the XY point using that reference.
	 */
	@Override
	protected void refreshVisuals() {
		// - The references to the model and figure objects.
		final UnitFigure fig = getUnitFigure();
		final Unit model = getUnit();

		// - Update other unit attributes
		fig.setSide(model.getSide());

		if (Unit.FRIEND != model.getSide()) fig.setVisible(false);
		if (model instanceof WarUnit) {
			final WarUnit war = (WarUnit) model;
			// - Detect enemy units. They are not visible by default
			if (Unit.FOE == model.getSide()) {
				if (war.isDetected()) fig.setVisible(true);
			}
		}

		HarpoonLogger.log(Level.FINER, "Unit model type: " + model.getClass().getSimpleName());
		HarpoonLogger.log(Level.FINER, "Unit DMS latitude: " + model.getDMSLatitude());
		HarpoonLogger.log(Level.FINER, "Unit DMS longitude: " + model.getDMSLongitude());

		// - This block calculates the coordinates XY for the DMS model location.
		final Point xyloc = convertToMap(model);
		HarpoonLogger.log(Level.FINER, "Calculated XY location: " + xyloc);

		// - Move the point depending on the figure hotspot location.
		final Dimension hotspot = fig.getHotSpot();
		HarpoonLogger.log(Level.FINER, "Obtained hotspot difference: " + hotspot);
		xyloc.x -= hotspot.width;
		xyloc.y -= hotspot.height;
		// xyloc.x += 1;
		// xyloc.y += 1;

		final Dimension size = fig.getSize();
		HarpoonLogger.log(Level.FINER, "Calculated figure size: " + size);
		final Rectangle bound = new Rectangle(xyloc, size);
		HarpoonLogger.info("Rebounding instance " + this.getClass().getSimpleName() + " to " + bound);

		// - This triggers the parent layout to revalidate the position of the element.
		((RootMapPart) getParent()).setLayoutConstraint(this, getFigure(), bound);
	}

	private Point convertToMap(Unit model) {
		// - Get the unit coordinates and the top-left coordinates.
		final DMSCoordinate unitLat = model.getDMSLatitude();
		final DMSCoordinate unitLon = model.getDMSLongitude();

		// - Get the model root where there are stored the conversion properties.
		final RootMapPart rootPart = (RootMapPart) getParent();
		final RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		final DMSCoordinate topLat = modelRoot.getDMSLatitude();
		final DMSCoordinate topLon = modelRoot.getDMSLongitude();
		final long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		final long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();

		// RootMapUnit rootMap = figure;

		// RootMapUnit rootUnit = (RootMapUnit) this.getParent().getModel();
		// HarpoonMap map = rootUnit.getMap();
		// DMSCoordinate topLat = map.getDMSLatitude();
		// DMSCoordinate topLon = map.getDMSLongitude();

		// - Generate a DMSPoint to perform the calculations.
		final DMSPoint point = new DMSPoint(topLat, topLon);
		final DMSPoint vector = point.offsetPoint(new DMSPoint(unitLat, unitLon));

		// - Get the multipliers to convert from DMS to XY
		// long latSpan = map.getLatitude2Zoom().toSeconds();
		// long lonSpan = map.getLongitude2Zoom().toSeconds();

		// - Calculate the final XY coordinates
		final int xx = new Double(256 * vector.getLongitude().toSeconds() / lonSpan).intValue();
		int yy = new Double(256 * vector.getLatitude().toSeconds() / latSpan).intValue();
		// - Invert the sign for the Y axis because positive coordinates are in the upward direction
		yy = -1 * yy;

		// UnitFigure fig = this.getUnitFigure();
		// // - Move the point depending on the figure hotspot location.
		// Dimension hotspot = fig.getHotSpot();
		// xx -= hotspot.width;
		// yy -= hotspot.height;

		return new Point(xx, yy);
		// [01]]
	}

	// [02]
	/** This method handles the activation and deactivation of selections. */
	@Override
	public void setSelected(int value) {
		// TODO Auto-generated method stub
		super.setSelected(value);

		final UnitFigure fig = getUnitFigure();
		fig.setSelected(value);
		// fig.invalidate();
		fig.repaint();
	}

	/**
	 * The mechanism is not clearly explained. This is the way to receive that there have been changes to the model
	 * properties. This implementation intercepts event of type <em><code>Notification.SET</code></em> to trigger the
	 * change on the content of one of the model properties so new updates to the presentation take the new data from the
	 * model.<br>
	 * This implementation does not delegate other event to the super class implementation because no other class
	 * implements this interface.
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		final String prop = evt.getPropertyName();
		if (Unit.CHILDREN.equals(prop))
			refreshChildren();
		else if (Unit.NAME.equals(prop))
			refreshVisuals();
		else if (Unit.SIDE.equals(prop))
			refreshVisuals();
		else if (Unit.LATITUDE.equals(prop))
			refreshVisuals();
		else if (Unit.LONGITUDE.equals(prop)) refreshVisuals();

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}

	protected Unit getUnit() {
		return (Unit) getModel();
	}

	protected UnitFigure getUnitFigure() {
		return (UnitFigure) getFigure();
	}

	int getAnchorOffset() {
		return 9;
	}

	@Override
	public String toString() {
		final StringBuffer result = new StringBuffer(this.getClass().getSimpleName() + ": [ Unit type: ");
		result.append(getUnit().getClass().getSimpleName() + " ]");
		result.append(" [ Unit figure: ");
		result.append(getFigure().getClass().getSimpleName() + " ]");
		return result.toString();
	}
}

// - UNUSED CODE ............................................................................................
// [01]
// RootMapPart root = HarpoonPartFactory.getDiagram();
// RootMapUnit mod = (RootMapUnit) root.getModel();
// double topLeftLat = map.getTopLatitude();
// double topLeftLon = map.getTopLongitude();
//
//		
//		
//		
//		
// long latUnitSeconds = latitude.toSeconds();
// long lonUnitSeconds = longitude.toSeconds();
// long latTopSeconds =
// // - Calculate the figure location. The part has access to the model.
// // Object o1 = this.getRoot();
// UnitFigure fig = this.getUnitFigure();
//
// // HarpoonMap map = root.getMap();
// double latSpan = map.getLatitude2Zoom().toDegrees();
// double lonSpan = map.getLongitude2Zoom().toDegrees();
//
// double latDiff = Math.abs(topLeftLat - model.getLatitude());
// double lonDiff = Math.abs(topLeftLon - model.getLongitude());

// // - Move the point depending on the figure hotspot location.
// Dimension hotspot = fig.getHotSpot();
// xx -= hotspot.width;
// xx -= hotspot.height;

// return new Point(xx, yy);

// [02]
// private Point convertToMap2(PolarPoint point) {
// // - Calculate the figure location. The part has access to the model.
// // Object o1 = this.getRoot();
// RootMapPart root = HarpoonPartFactory.getDiagram();
// RootMapUnit mod = (RootMapUnit) root.getModel();
// HarpoonMap map = mod.getMap();
// UnitFigure fig = this.getUnitFigure();
//
// // HarpoonMap map = root.getMap();
// double topLeftLat = map.getTopLatitude();
// double topLeftLon = map.getTopLongitude();
// double latSpan = map.getLatitude2Zoom().toDegrees();
// double lonSpan = map.getLongitude2Zoom().toDegrees();
//
// double latDiff = Math.abs(topLeftLat - point.getLatitude());
// double lonDiff = Math.abs(topLeftLon - point.getLongitude());
//
// int xx = new Double(256 * lonDiff / lonSpan).intValue();
// int yy = new Double(256 * latDiff / latSpan).intValue();
//
// // - Move the point depending on the figure hotspot location.
// Dimension hotspot = fig.getHotSpot();
// xx -= hotspot.width;
// xx -= hotspot.height;
//
// return new Point(xx, yy);
//
// }
// protected void createEditPolicies() {
// // installEditPolicy(EditPolicy.GRAPHICAL_NODE_ROLE, new ActivityNodeEditPolicy());
// // installEditPolicy(EditPolicy.CONTAINER_ROLE, new ActivitySourceEditPolicy());
// // installEditPolicy(EditPolicy.COMPONENT_ROLE, new ActivityEditPolicy());
// // installEditPolicy(EditPolicy.DIRECT_EDIT_ROLE, new ActivityDirectEditPolicy());
// }

// public void performRequest(Request request) {
// // if (request.getType() == RequestConstants.REQ_DIRECT_EDIT)
// // performDirectEdit();
// }

/*
 * (non-Javadoc)
 * 
 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
 */

// protected void setFigure(IFigure figure) {
// figure.getBounds().setSize(0, 0);
// super.setFigure(figure);
// }
