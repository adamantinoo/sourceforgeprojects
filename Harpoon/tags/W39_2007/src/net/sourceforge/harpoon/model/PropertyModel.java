//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PropertyModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/PropertyModel.java,v $
//  LAST UPDATE:    $Date: 2007-09-25 11:44:40 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

// - CLASS IMPLEMENTATION ...................................................................................
public class PropertyModel implements Serializable, Cloneable {
	private static final long								serialVersionUID	= -3810250540551433870L;
	/**
	 * List of listeners that will be notified when a property changes. The list contains the Controller
	 * elements (EditParts) that will be interested in received events when the model field values change to
	 * reflect that changes on the visual part of the model.
	 */
	private transient PropertyChangeSupport	listeners					= new PropertyChangeSupport(this);

	// - P R O P E R T Y C H E N G E S U P P O R T W R A P P I N G
	/**
	 * Wrap the methods of the PropertyChangeSupport listener element to be accessed by the notification
	 * mechanism.
	 * 
	 * @param listener
	 *          new listener to be added to the list of listeners that will receive notifications.
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		getListeners().addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		getListeners().removePropertyChangeListener(l);
	}

	public void firePropertyChange(String property, Object oldValue, Object newValue) {
		if (getListeners().hasListeners(property)) {
			getListeners().firePropertyChange(property, oldValue, newValue);
		}
	}

	public void fireStructureChange(String fieldname, Object dataSet, Object change) {
		getListeners().firePropertyChange(new PropertyChangeEvent(this, fieldname, dataSet, change));
	}

	// - P R O T E C T E D S E C T I O N
	protected PropertyChangeSupport getListeners() {
		if (null == listeners) listeners = new PropertyChangeSupport(this);
		return listeners;
	}

}

// - UNUSED CODE ............................................................................................
