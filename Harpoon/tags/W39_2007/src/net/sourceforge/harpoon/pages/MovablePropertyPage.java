//  PROJECT:        HarpoonModel
//  FILE NAME:      $RCSfile: MovablePropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/MovablePropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-27 16:45:17 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class MovablePropertyPage extends UnitPropertyPage {
	/** Reference to the model. But limited to the interface level of a Unit. */
	private MovableUnit	movableModel;
	private Spinner			speedSet;
	private Spinner			directionSet;

	// - G E T T E R S / S E T T E R S
	public void setModel(MovableUnit model) {
		movableModel = model;
		super.setModel(model);
	}

	// - O V E R R I D E S E C T I O N
	/**
	 * Add the interface items that are specific for MovableParts. This include the movement controls.
	 * 
	 * @see net.sourceforge.harpoon.pages.UnitPropertyPage#build()
	 */
	@Override
	public void build() {
		// - Parent will create the base element and then add all data for other levels in the inheritance chain.
		super.build();

		// - Create the speed control
		createMovementControl(page);

		// - Set the control values from the model data.
		directionSet.setSelection(movableModel.getDirection());
		speedSet.setSelection(movableModel.getSpeed());
		speedSet.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				HarpoonLogger.info("New speed value: " + speedSet.getSelection());
				movableModel.setSpeed(speedSet.getSelection());
			}
		});
	}

	// - P R O T E C T E D S E C T I O N
	protected void createMovementControl(Group group) {
		final GridLayout grid = new GridLayout();
		grid.numColumns = 2;
		grid.horizontalSpacing = 2;
		grid.verticalSpacing = 0;
		grid.marginHeight = 0;
		grid.marginWidth = 0;
		final Composite movementControl = new Composite(group, SWT.NONE);
		movementControl.setLayout(grid);

		// - Create the elements that are declared for MovableUnits
		final Text speedLabel = new Text(movementControl, SWT.NONE);
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedSet = new Spinner(movementControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));

		final Text directionLabel = new Text(movementControl, SWT.NONE);
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionLabel.setEditable(false);
		directionLabel.setText("Direction:");
		directionSet = new Spinner(movementControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setEnabled(false);
	}
}

// - UNUSED CODE ............................................................................................
