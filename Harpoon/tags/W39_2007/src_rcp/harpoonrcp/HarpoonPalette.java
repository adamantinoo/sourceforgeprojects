//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonPalette.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/harpoonrcp/HarpoonPalette.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:11:40 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package harpoon.devel;

//... IMPORT SECTION ...................................................................................................
import org.eclipse.swt.graphics.Color;

import net.sourceforge.harpoon.HarpoonApp;

//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HarpoonPalette {
	public static Color getFigureBGColor() {
		return new Color( HarpoonApp.getDisplay(), 0, 0, 0 );
	}
	public static Color getFigureFGColor() {
		return new Color( HarpoonApp.getDisplay(), 0, 0, 0 );
	}
}

//... UNUSED CODE ......................................................................................................