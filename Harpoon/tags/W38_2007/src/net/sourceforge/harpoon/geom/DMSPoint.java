//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: DMSPoint.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/geom/DMSPoint.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:12:41 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//

package net.sourceforge.harpoon.geom;

import java.io.Serializable;

// - IMPORT SECTION .........................................................................................

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class DMSPoint implements Serializable {
	private static final long	serialVersionUID	= -1674626351426841763L;
	private DMSCoordinate			latitude;
	private DMSCoordinate			longitude;

	public DMSPoint(DMSCoordinate lat, DMSCoordinate lon) {
		this.latitude = lat;
		this.longitude = lon;
	}

	public DMSPoint offsetPoint(DMSPoint point) {
		// -Operate by each coordinate independently
		DMSCoordinate finalLat = offset(point.getLatitude(), DMSCoordinate.LATITUDE);
		DMSCoordinate finalLon = offset(point.getLongitude(), DMSCoordinate.LONGITUDE);
		return new DMSPoint(finalLat, finalLon);
	}

	private DMSCoordinate offset(DMSCoordinate destination, int type) {
		// -Convert coordinates to seconds for operation
		long origin;
		if (DMSCoordinate.LATITUDE == type)
			origin = this.latitude.toSeconds();
		else
			origin = this.longitude.toSeconds();
		long dest = destination.toSeconds();
		long offset = dest - origin;

		// - Convert back from seconds to a DMS coordinate
		DMSCoordinate result = DMSCoordinate.fromSeconds(offset);
		if (DMSCoordinate.LATITUDE == type)
			if (offset < 0)
				result.setSense('S');
			else
				result.setSense('N');
		else if (offset < 0)
			result.setSense('W');
		else
			result.setSense('E');
		return result;
	}

	public DMSCoordinate getLongitude() {
		if (null == longitude) this.longitude = new DMSCoordinate();
		return this.longitude;
	}

	public DMSCoordinate getLatitude() {
		if (null == latitude) this.latitude = new DMSCoordinate();
		return this.latitude;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "DMSPoint(" + latitude.toString() + "," + longitude.toString() + ")";
	}

	public DMSPoint substract(DMSPoint point) {
		long resultLat = getLatitude().toSeconds() - point.getLatitude().toSeconds();
		long resultLon = getLongitude().toSeconds() - point.getLongitude().toSeconds();
		return DMSPoint.fromSeconds(resultLat, resultLon);
	}

	private static DMSPoint fromSeconds(long lat, long lon) {
		// TODO Auto-generated method stub
		return new DMSPoint(DMSCoordinate.fromSeconds(lat, DMSCoordinate.LATITUDE), DMSCoordinate.fromSeconds(
				lon, DMSCoordinate.LONGITUDE));
	}
}

// - UNUSED CODE ............................................................................................
