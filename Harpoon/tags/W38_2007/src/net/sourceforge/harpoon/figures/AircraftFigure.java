//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AircraftFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/AircraftFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:12:41 $
//  RELEASE:        $Revision: 1.5 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.4  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.3  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.2  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-27 10:25:13  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.rcp.harpoon.test.BasicFigure;

//- CLASS IMPLEMENTATION ...................................................................................
/**
 * This Figure represents the graphical view for a Aircraft game unit. The representation is drawn as a hollow
 * square with a point inside. The left of this box has two labels, one with the speed and the other with the
 * direction of the unit.<br>
 * The color of the icon box depends on the side of the unit.<br>
 * The location is centered on the center point of the ship icon.<br>
 * <br>
 * The implementation is based into two classes, one local that draws the icon representation. This has a
 * fixed size and contains the location hot spot that is the point that is set as the location of the unit.
 * The other class is obtained by inheritance and contains the methods to create the Grid layout and the
 * labels that display the figure data.<br>
 * Figure data has to be updated when the model changes, so we have to implement methods to export the fields
 * interface so the EditPart can call them to set the new model values.<br>
 * <br>
 * Required values to be known by this figure to be able to draw correctly are:
 * <ul>
 * <li>speed and direction </li>
 * <li>side of the game</li>
 * </ul>
 * Location is something that currently is not managed by this representation class so global data is not
 * needed.
 */
public class AircraftFigure extends MovableFigure {
	private static final int MARGIN = 2;

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of a Aircraft. This is drawn as a hollow
	 * square with a point inside. The left of this box has two labels, one with the speed and the other with
	 * the direction of the unit.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center point of the ship icon.
	 */
	public AircraftFigure() {
		// - Create the complex internal parts of this figure
		this.setLayoutManager(createStdLayout(MARGIN));
		createMovableIcon(new AircraftDrawFigure(this));
		createLabelinfo();
		this.setSize(this.getPreferredSize(-1, -1));
	}
	// - P U B L I C S E C T I O N
	public String toString() {
		//TODO Implement the bodyof this method
		return "AircraftFigure";
	}
	class AircraftDrawFigure extends BasicFigure {
		private static final int	FIGURE_SIZE	= 16;

		public AircraftDrawFigure(UnitFigure parentFigure) {
			super(parentFigure);
			setDrawingSize(FIGURE_SIZE);
		}
		protected void paintFigure(Graphics graphics) {
			// - Get drawing location. This should be already displaced from the top-left.
			Rectangle bound = getBounds().getCopy();
			bound.width-=1;
			bound.height-=1;

			// - Draw the figure body. This is a triangle
			graphics.setForegroundColor(getColor());
			graphics.setLineWidth(2);
			Point p1 = new Point(bound.x+bound.width/2,bound.y);
			Point p2 = new Point(bound.x+bound.width,bound.y+bound.height);
			Point p3 = new Point(bound.x,bound.y+bound.height);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(p1,p2);
			graphics.drawLine(p2,p3);
			graphics.drawLine(p3,p1);
			graphics.setLineWidth(1);
			if (isSelected()) {
				drawHandles(graphics);
				graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
				graphics.drawRectangle(bound);
			}

			// - Draw the figure center
			bound = getBounds().getCopy();
			Dimension hotspot = this.getHotSpot();
			bound.x += hotspot.width + 1;
			bound.y += hotspot.height + 1;
			Point endPoint = new Point(bound.x + 1, bound.y + 1);
			graphics.setLineWidth(1);
			graphics.setForegroundColor(getColor());
			graphics.drawLine(new Point(bound.x, bound.y), endPoint);
		}
	}
}

// - UNUSED CODE ............................................................................................
