//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: IconFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/IconFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:13:06 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.1  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.swt.graphics.Color;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public abstract class IconFigure extends Figure {

//	protected Color	color;
	protected UnitFigure	container;

//	public IconFigure(Color color) {
//		this.color = color;
//	}

	public IconFigure(UnitFigure figure) {
		// TODO Auto-generated constructor stub
		this.container = figure;
	}

//	public void setColor(Color newColor) {
//		// TODO Auto-generated method stub
//		this.color = newColor;
//		// FIXME I should repaint the object or update something ??
//	}

	public IconFigure() {
		// TODO Auto-generated constructor stub
	}

	public abstract Dimension getHotSpot();
	protected abstract void paintFigure(Graphics graphics);

	protected UnitFigure getContainer() {
		return this.container;
	}
	protected Color getColor() {
		if(null==this.container) return ColorConstants.black;
		return getContainer().getColor();
	}
	protected boolean isSelected() {
		return container.isSelected();
	}

}

// - UNUSED CODE ............................................................................................
