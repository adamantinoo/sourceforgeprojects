//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/WarFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P R I V A T E S E C T I O N
public class WarFigure extends MovableFigure3 {

	private boolean	radar				= false;
	private int			radarRange	= 0;
	private boolean	ECM					= false;
	private int			EMCRange		= 0;

	// - P U B L I C S E C T I O N
	@Override
	public void init() {
		super.init();
	}

	// - G E T T E R S / S E T T E R S
	public void setRadarState(boolean radarSate) {
		// TODO Auto-generated method stub
		radar = radarSate;
		this.repaint();
	}

	public void setECMState(boolean state) {
		// TODO Auto-generated method stub
		ECM = state;
		this.repaint();
	}

	@Override
	protected void paintFigure(Graphics graphics) {
		// - This paints the left side and the icon
		super.paintFigure(graphics);

		// - Paint the sensor range circles if the sensors are activated
		if (radar) {
			// TODO Sensors are structures because they contain the state and the range
			// - Ranges are already expressed in pixels
			// - Move the center of the circle to the hotspot of the figure.
			final Dimension hot = getHotSpot();
			final Point loc = getLocation();
			final Rectangle sensorBounds = new Rectangle();
			sensorBounds.x = loc.x + hot.width - radarRange;
			sensorBounds.y = loc.y + hot.height - radarRange;
			sensorBounds.width = radarRange * 2;
			sensorBounds.height = radarRange * 2;

			final Rectangle bo = getBounds().getCopy();
			graphics.fillRectangle(bo);
			graphics.drawOval(sensorBounds.expand(3, 3));
			graphics.drawOval(sensorBounds.expand(-1, -1));
			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(bo);
			graphics.drawOval(bo.expand(3, 3));

			graphics.setForegroundColor(ColorConstants.black);
			graphics.drawOval(sensorBounds);
		}
		if (ECM) {
			// TODO Sensors are structures because they contain the state and the range
			// - Ranges are already expressed in pixels
			// - Move the center of the circle to the hotspot of the figure.
			final Dimension hot = getHotSpot();
			final Point loc = getLocation();
			final Rectangle sensorBounds = new Rectangle();
			sensorBounds.x = loc.x + hot.width - EMCRange;
			sensorBounds.y = loc.y + hot.height - EMCRange;
			sensorBounds.width = EMCRange * 2;
			sensorBounds.height = EMCRange * 2;

			graphics.setForegroundColor(ColorConstants.lightGreen);
			graphics.fillOval(sensorBounds);
		}
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		buffer.append("WarFigure(");
		buffer.append(")");
		return buffer.toString();
	}

	public void setRadarRange(int radarPixels) {
		// TODO Auto-generated method stub
		radarRange = radarPixels;
	}

	public void setECMRange(int pixels) {
		// TODO Auto-generated method stub
		EMCRange = pixels;
	}
}

// - UNUSED CODE ............................................................................................
