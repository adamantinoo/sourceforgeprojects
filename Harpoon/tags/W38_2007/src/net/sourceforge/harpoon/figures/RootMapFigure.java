//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/RootMapFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:12:41 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.2  2007-09-05 09:03:50  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-08-28 13:46:58  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.Properties;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.FreeformLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class MapFigure extends HarpoonMap {
	private GoogleMapCache	mapCache	= new GoogleMapCache();

	/**
	 * Create the graphical elements that compose the figure. It is not drawn at this point, just initialized.
	 * The size represents a problem because the map has to fill the full editor area. That information will be
	 * the size to be used for this calculations. Contact the Editor panel to get the size.<br>
	 * The size can be supposed to the max extent of the screen, or the Display that is the greater element.
	 */
	public MapFigure() {
		// - Add a free form layout to this figure.
		this.setLayoutManager(new FreeformLayout());
		// DEBUG Set the border for debugging
		this.setBorder(new LineBorder(1));

		// - Calculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	@Override
	public Dimension getPreferredSize(int hint, int hint2) {
		// TODO Auto-generated method stub
		Dimension superSize = super.getPreferredSize(hint, hint2);
		Display dis = Display.getCurrent();
		Shell shell = dis.getActiveShell();
		Point siz = shell.getSize();
		return new Dimension(siz);
	}

	// @Override
	// public Rectangle getBounds() {
	// // TODO Auto-generated method stub
	// int width = this.getHorizontalBlocks()*GOOGLE_BLOCK_WIDTH;
	// int height = this.getVerticalBlocks()*GOOGLE_BLOCK_HEIGHT;
	// return new Rectangle(0,0,width,height);
	// }
	/**
	 * Draw each of the map block until all the bounding are is covered with map data. Use the bounding to
	 * calculate the blocks to be read from the cache and to be copied to the display.
	 */
	protected void paintFigure(Graphics g) {
		super.paintFigure(g);
		// - If the cache is still not initialized then it will return an exception.
		// try {
		// - Get the bounds to be painted. Calculate number of blocks for each axis.
		Dimension areaSize = getSize();
		final int hBlocks = (areaSize.width + GOOGLE_BLOCK_WIDTH - 1) / GOOGLE_BLOCK_WIDTH;
		final int vBlocks = (areaSize.height + GOOGLE_BLOCK_HEIGHT - 1) / GOOGLE_BLOCK_HEIGHT;
		// - Copy to the canvas the map data that is stored in the cache.
		for (int j = 0; j < vBlocks; j++) {
			for (int i = 0; i < hBlocks; i++) {
				// - Get a block and paint it on the right location on the final display buffer.
				Image newMap;
				try {
					newMap = new Image(Display.getCurrent(), this.mapCache.getBlock(i, j));
					int x = GOOGLE_BLOCK_WIDTH * i;
					int y = GOOGLE_BLOCK_HEIGHT * j;
					g.drawImage(newMap, x, y);
				} catch (MalformedURLException e) {
					Rectangle bounds = getBounds().getCopy();
					g.setForegroundColor(ColorConstants.darkGray);
					g.setBackgroundColor(ColorConstants.white);
					g.fillRectangle(0, 0, bounds.width, bounds.height);
				} catch (IOException e) {
					Rectangle bounds = getBounds().getCopy();
					g.setForegroundColor(ColorConstants.darkGray);
					g.setBackgroundColor(ColorConstants.white);
					g.fillRectangle(0, 0, bounds.width, bounds.height);
				}
			}
		}
	}

	/**
	 * Receives the properties to configure the Map from the Model storage that where received from the scenery
	 * properties configuration file.
	 */
	public void setConfiguration(Properties mapProperties) {
		if (null == mapProperties)
			this.props = new Properties();
		else
			this.props = mapProperties;
		// - Copy the properties to the cahe that is the class that will meke use of them.
		mapCache.setConfiguration(this.props);
	}

	class GoogleMapCache {

		private Properties		properties		= new Properties();
		private Hashtable			mapBlockCache	= new Hashtable();
		private int						accesses			= 0;
		private int						misses				= 0;
		private int						hits					= 0;
		private int						topX					= -1;
		private int						topY					= -1;

		public void setConfiguration(Properties props) {
			// TODO Auto-generated method stub
			this.properties = props;
		}

		/**
		 * Gets a block of Google map data with coordinates relative to the initial start point of the scenery.
		 * The initial coordinates from the top-left block are defined on the properties. This will get a block
		 * <code>i</code> spaces to the right of this top-left corner and <code>j</code> spaces below that
		 * same block.
		 * 
		 * @throws IOException
		 * @throws MalformedURLException
		 */
		public ImageData getBlock(int i, int j) throws MalformedURLException, IOException {
			accesses++;
			// - Generate the URL to get the data form Google. This is also the cache key.
			//DEBUG Removed the load of images to reduce startuup time
			String mapURL = getURLReference(getTopX() + 1, getTopY() + 1);
			ImageData block = (ImageData) mapBlockCache.get(mapURL);
			if (null == block) {
				// - Miss in the cache data. Got to Google for the information.
				misses++;
				block = readBlockData(mapURL);
				mapBlockCache.put(mapURL, block);
			} else
				hits++;
			return block;
		}

		private int getTopX() {
			if (-1 == this.topX) {
				final String propValue = this.properties.getProperty("topX", "52");
				final Integer numberValue = new Integer(propValue);
				try {
					this.topX = numberValue.intValue();
				} catch (NumberFormatException nfe) {
					this.topX = 0;
				}
			}
			return this.topX;
		}

		private int getTopY() {
			if (-1 == this.topY) {
				final String propValue = this.properties.getProperty("topY", "26");
				final Integer numberValue = new Integer(propValue);
				try {
					this.topY = numberValue.intValue();
				} catch (NumberFormatException nfe) {
					this.topY = 0;
				}
			}
			return this.topY;
		}

		@Override
		public String toString() {
			return "GoogleMapCache stats; (" + accesses + "," + hits + "," + misses + ")";
		}

	}
}

// - UNUSED CODE ............................................................................................
// @Override
// protected void paintChildren(Graphics graphics) {
// // TODO Auto-generated method stub
// super.paintChildren(graphics);
// }

