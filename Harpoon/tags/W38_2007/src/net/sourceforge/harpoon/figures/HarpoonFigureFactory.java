//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: HarpoonFigureFactory.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/figures/HarpoonFigureFactory.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.figures;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.harpoon.parts.AirportPart;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.BasicFigure;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class HarpoonFigureFactory {

	public static UnitFigure createFigure(Unit unit, int subType) {
		HarpoonLogger.log(Level.FINE, "Creating Figure for model type: " + unit.getClass().getSimpleName());
		// TODO Auto-generated method stub
		// - Create and initialize the figure
		UnitFigure fig = null;

		if (unit instanceof RootMapUnit)
			fig = new MapFigure();
		// else if (model instanceof AirportUnit) fig = new AirportFigure();
		else if (unit instanceof WarUnit) {
			WarFigure mfig = new WarFigure();
			switch (subType) {
				case MovableUnit.SURFACE:
					mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
					fig = mfig;
					break;

				default:
					break;
			}
			mfig.init();
		}
		else if (unit instanceof MovableUnit) {
			MovableFigure3 mfig = new MovableFigure3();
			switch (subType) {
				case MovableUnit.SURFACE:
					mfig.setDrawFigure(new SurfaceDrawFigure(mfig));
					fig = mfig;
					break;

				default:
					break;
			}
			mfig.init();
		}
		//
		// HarpoonLogger.log(Level.FINE, "Creating Figure ShipFigure");
		// fig.setSide(unit.getSide());
		// fig.setDirection(unit.getDirection());
		// fig.setSpeed(unit.getSpeed());
		// HarpoonLogger.log(Level.FINE, fig.toString());
		return fig;
		//
		// return null;
	}
}

class SurfaceDrawFigure extends BasicFigure {
	private static final int	FIGURE_SIZE	= 16;

	public SurfaceDrawFigure(UnitFigure parentFigure) {
		super(parentFigure);
		setDrawingSize(FIGURE_SIZE);
	}

	protected void paintFigure(Graphics graphics) {
		// - Get drawing location. This should be already displaced from the top-left.
		// Point loc = getLocation();
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;

		// - Draw the figure body
		graphics.setForegroundColor(getColor());
		graphics.setLineWidth(2);
		graphics.drawOval(bound);
		graphics.setLineWidth(1);
		if (isSelected()) {
			drawHandles(graphics);
			graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
			graphics.drawRectangle(bound);
		}

		// - Draw the figure center
		bound = getBounds().getCopy();
		Dimension hotspot = this.getHotSpot();
		bound.x += hotspot.width + 1;
		bound.y += hotspot.height + 1;
		Point endPoint = new Point(bound.x + 1, bound.y + 1);
		graphics.setLineWidth(1);
		graphics.setForegroundColor(getColor());
		graphics.drawLine(new Point(bound.x, bound.y), endPoint);
	}
}

// - UNUSED CODE ............................................................................................
