//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/WarPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.GraphicalEditPart;
import org.eclipse.swt.widgets.Composite;

import net.sourceforge.harpoon.figures.HarpoonFigureFactory;
import net.sourceforge.harpoon.figures.WarFigure;
import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.harpoon.pages.SurfacePropertyPage;
import net.sourceforge.rcp.harpoon.test.MultiPropertyPage;
import net.sourceforge.rcp.harpoon.test.MultiUnit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class WarPart extends MovablePart {
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		WarFigure fig = (WarFigure) this.getUnitFigure();
		WarUnit model = (WarUnit) this.getUnit();
		
		//- Update figure visuals from current model data.
		fig.setRadarState(model.getRadarState());
		fig.setECMState(model.getECMState());

		//- Get the model root where there are stored the conversion properties.
		RootMapPart rootPart=(RootMapPart) this.getParent();
		RootMapUnit modelRoot = (RootMapUnit) rootPart.getModel();
		DMSCoordinate topLat = modelRoot.getDMSLatitude();
		DMSCoordinate topLon = modelRoot.getDMSLongitude();
		long latSpan = modelRoot.getLatitude2Zoom().toSeconds();
		long lonSpan = modelRoot.getLongitude2Zoom().toSeconds();
		
		//- Generate the radar ranges converted to pixels
		int radarRange = 20;
		int radarSeconds = radarRange*60;
		int radarPixels = new Double(256 * radarSeconds / latSpan).intValue();
		int ECMRange=80;
		int ECMSeconds = ECMRange*60;
		int ECMPixels = new Double(256 * ECMSeconds / latSpan).intValue();
		
		fig.setRadarRange(radarPixels);
		fig.setECMRange(ECMPixels);
	}

	public Composite createPropertyPage(Composite top) {
		// - Create the root of the page structure.
		SurfacePropertyPage page = new SurfacePropertyPage();
		page.setParent(top);
		page.setModel((WarUnit) this.getModel());
		page.build();
		return page.getPropertyPage();
	}
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		getUnit().addPropertyChangeListener(this);
		super.activate();
	}
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		getUnit().removePropertyChangeListener(this);
		super.deactivate();
	}
	public void propertyChange(PropertyChangeEvent evt) {
		super.propertyChange(evt);
		String prop = evt.getPropertyName();
		if (WarUnit.RADAR.equals(prop))
			refreshVisuals();
		else if (WarUnit.ECM.equals(prop))
			refreshVisuals();

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}
}

// - UNUSED CODE ............................................................................................
