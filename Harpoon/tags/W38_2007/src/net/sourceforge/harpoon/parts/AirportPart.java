//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: AirportPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/AirportPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-19 13:12:41 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.4  2007-09-12 11:26:28  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-09-05 07:48:46  ldiego
//    - Registration of this class before comparison with working
//      previous version.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.FocusEvent;
import org.eclipse.draw2d.FocusListener;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.BasicFigure;
import net.sourceforge.rcp.harpoon.test.TestFigure;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
public class AirportPart extends UnitPart {
	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		Unit model = (Unit) this.getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure AirportFigure");
		AirportFigure fig = new AirportFigure();
		// DEBUG The values of the model data and the location are set on the method refreshVisuals
		fig.setName(model.getName());
		fig.setSide(model.getSide());
		// // TODO And where is the set of the location?
		// HarpoonLogger.log(Level.INFO, fig.toString());
		return fig;
	}

	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		AirportFigure fig = (AirportFigure) this.getUnitFigure();
		Unit model = this.getUnit();
		fig.setName(model.getName());
		fig.setSide(model.getSide());
		fig.setSize(fig.getPreferredSize());
	}

	class AirportFigure extends TestFigure {
		// - C O N S T R U C T O R S
		/**
		 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a
		 * square box with some lines inside with a name label at the right center.<br>
		 * The color of the icon box depends on the side of the unit.<br>
		 * The location is centered on the center of the icon.
		 */
		public AirportFigure() {
			super();
			// - Create the complex internal parts of this figure.
			GridLayout grid = new GridLayout();
			grid.numColumns = 2;
			grid.horizontalSpacing = LAYOUT_MARGIN;
			grid.marginHeight = LAYOUT_MARGIN;
			grid.marginWidth = LAYOUT_MARGIN;
			grid.verticalSpacing = 0;
			this.setLayoutManager(grid);
			// - Create the complex internal parts of this figure
			// setLayoutManager(createStdLayout(LAYOUT_MARGIN));

			iconic = new AirportDrawFigure(this);
			// movable.setParent(this);
			iconic.addFocusListener(new FocusListener() {

				public void focusGained(FocusEvent fe) {
					// TODO Auto-generated method stub
					int dummy = 1;
					dummy += 1;
					HarpoonLogger.info("Airport has gained focus");
				}

				public void focusLost(FocusEvent fe) {
					// TODO Auto-generated method stub
					int dummy = 1;
					dummy += 1;
					HarpoonLogger.info("Airport has lost focus");
				}
			});
			this.add(iconic);

			createRightSide();
			// DEBUG Set the border for debugging
			// this.setBorder(new LineBorder(1));

			// - Calculate size and bounds
			this.setSize(this.getPreferredSize(-1, -1));
		}

		private void createRightSide() {
			nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			nameLabel.setBorder(new LineBorder(1));
			this.add(nameLabel);
		}

		// - P U B L I C S E C T I O N
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("AirportFigure(" + nameLabel.getText());
			buffer.append(",");
			buffer.append("size:" + getPreferredSize());
			buffer.append(",");
			buffer.append("bounds:" + getBounds());
			return buffer.toString();
		}

		class AirportDrawFigure extends BasicFigure {
			private static final int	FIGURE_SIZE	= 24;

			public AirportDrawFigure(UnitFigure parentFigure) {
				super(parentFigure);
				setDrawingSize(FIGURE_SIZE);
			}

			protected void paintFigure(Graphics graphics) {
				super.paintFigure(graphics);
				// - Get drawing location. This should be already displaced from the top-left.
				Rectangle bound = getBounds().getCopy();
				bound.width -= 2;
				bound.height -= 2;

				// - Draw the figure body
				graphics.setForegroundColor(getColor());
				graphics.setLineWidth(2);
				graphics.drawRectangle(bound.x + 1, bound.y + 1, bound.width - 1, bound.height - 1);
				graphics.setLineWidth(1);

				// - Draw the run lanes
				graphics.setLineWidth(3);
				Point p1 = new Point(bound.x + bound.width / 3, bound.y);
				Point p2 = new Point(bound.x + 2 * bound.width / 3, bound.y + bound.height);
				graphics.drawLine(p1, p2);
				p1 = new Point(bound.x + 2 * bound.width / 3, bound.y);
				p2 = new Point(bound.x + bound.width, bound.y + bound.height / 2);
				graphics.drawLine(p1, p2);
				p1 = new Point(bounds.x, bounds.y + bounds.height / 2);
				p2 = new Point(bounds.x + bounds.width - 2, bounds.y + bounds.height / 2);
				graphics.drawLine(p1, p2);

				graphics.setLineWidth(1);
				if (isSelected()) {
					drawHandles(graphics);
					graphics.setForegroundColor(HarpoonColorConstants.SELECTION_COLOR);
					graphics.drawRectangle(bound);
				}
			}
		}
	}
}

// - UNUSED CODE ............................................................................................
