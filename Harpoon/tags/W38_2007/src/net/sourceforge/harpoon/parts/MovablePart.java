//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovablePart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/MovablePart.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.util.Date;
import java.util.List;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.GraphicalEditPart;

import net.sourceforge.harpoon.figures.HarpoonFigureFactory;
import net.sourceforge.harpoon.figures.MovableFigure3;
import net.sourceforge.harpoon.model.MovableUnit;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class MovablePart extends UnitPart {
	/**
	 * Creation of the figure depends on some subtle model information to set the right icon to the common
	 * presentation and model unit. A MovableUnit model unit may represent many unit types, detecting which
	 * one is the right one to create the proper visible figure and property page depends on this method.
	 */
	protected IFigure createFigure() {
		// - Get the model data to locate the information to load into the figure.
		MovableUnit unit = (MovableUnit) this.getModel();
		
		// - Identify the subtype for this model.
		int subType = unit.getSubType();
		
		// - Create and initialize the figure
		Figure fig = HarpoonFigureFactory.createFigure(unit,subType);
//		refreshVisuals();
////		HarpoonLogger.log(Level.FINE, "Creating Figure ShipFigure");
//		fig.setSide(unit.getSide());
//		fig.setDirection(unit.getDirection());
//		fig.setSpeed(unit.getSpeed());
//		HarpoonLogger.log(Level.FINE, fig.toString());
		return fig;
	}
	protected void refreshVisuals() {
		super.refreshVisuals();
		// - The references to the model and figure objects.
		MovableFigure3 fig = (MovableFigure3) this.getUnitFigure();
		MovableUnit model = (MovableUnit) this.getUnit();
		
		//- Update figure visuals from current model data.
		fig.setSpeed(model.getSpeed());
		fig.setDirection(model.getDirection());
		fig.setMovementPath(model.getMovementPath());
	}
	public void activate() {
		if (isActive()) return;
		// - Start listening for changes in the model.
		getUnit().addPropertyChangeListener(this);
		super.activate();
	}
	public void deactivate() {
		if (!isActive()) return;
		// - Stop listening to events in the model.
		getUnit().removePropertyChangeListener(this);
		super.deactivate();
	}
	public void propertyChange(PropertyChangeEvent evt) {
		super.propertyChange(evt);
		String prop = evt.getPropertyName();
		if (MovableUnit.MODEL.equals(prop))
			refreshVisuals();
		else if (MovableUnit.DIRECTION.equals(prop))
			refreshVisuals();
		else if (MovableUnit.SPEED.equals(prop))
			refreshVisuals();
		else if (MovableUnit.MOVEMENTPATH.equals(prop))
			refreshVisuals();

		// - Causes Graph to re-layout
		((GraphicalEditPart) (getViewer().getContents())).getFigure().revalidate();
	}
}

// - UNUSED CODE ............................................................................................
