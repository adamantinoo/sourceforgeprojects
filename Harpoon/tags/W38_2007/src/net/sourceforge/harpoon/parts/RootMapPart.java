//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: RootMapPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/parts/RootMapPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:13:06 $
//  RELEASE:        $Revision: 1.6 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.5  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.4  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.3  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.2  2007-08-28 13:33:21  ldiego
//    - [A0006.01] - The initial list of units must be displayed when the scenery is open.
//    - [A0002.01] - Change viewer implementation to use GEF.
//    - [A0001.01] - Initial requirements for menu "File".
//
//    Revision 1.1  2007-08-27 10:24:53  ldiego
//    - Preparation for Milestone W34.2007. Adding all detected files to repository before
//      going thought the Requirements phase.
//

package net.sourceforge.harpoon.parts;

// - IMPORT SECTION .........................................................................................
import java.util.List;
import java.util.logging.Level;

import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.editparts.AbstractGraphicalEditPart;

import net.sourceforge.harpoon.figures.MapFigure;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
/**
 * This is the controller of the root model element implemented as the Map. With the inheritance chain we only
 * have to implement the key methods <code>createFigure()</code> and <code>getModelChildren()</code> if
 * this part is a container.
 */
public class RootMapPart extends AbstractGraphicalEditPart {
	/**
	 * Currently I have to get access to the Map that is the already created figure because it contains some of
	 * the model data mixed with the presentation data.<br>
	 * This figure is costly to be created many times. Now just create it every time we have the part requested
	 * that now is just a single time.<br>
	 * <br>
	 * The creation of the Figure can be generalized for most EditParts. Most of the parts get the model
	 * information to set the initial visual Figure properties (that most of the time are the <code>name</code>
	 * and the <code>side</code>). The Figure class can be intantiated by indirectly so all this standard
	 * code can be refactored to the base <code>UnitPart</code> class.<br>
	 * <br>
	 * Another complicated thing to be reviewed if the way to communicate the scenery properties. This version
	 * gets the properties from the PartFactory that received the editor as a parameter and it is accessible
	 * statically. This has been changed to get the properties from the Model piece that is more easily
	 * configured.
	 */
	public IFigure createFigure() {
		// - Create the presentation figure and initialize it with the scenery properties.
		RootMapUnit model = (RootMapUnit) this.getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure MapFigure");
		MapFigure fig = new MapFigure();
		// - For the root there is no need to set the side. The name id the scenery name and it it got form the
		// properties.
		fig.setConfiguration(model.getMapProperties());
		return fig;
	}

	/**
	 * Return the model children that have to be assigned EditParts. Not all children elements generate EditPart
	 * because hidden units and enemy units not detected can not appear on the Map.
	 */
	@Override
	protected List<Unit> getModelChildren() {
		Unit model = (Unit) this.getModel();
		List<Unit> li = model.getChildren();
		return li;
	}

	public String toString() {
		return "RootMapPart:" + ((Unit) getModel()).getName();
	}

	protected void refreshVisuals() {
		// TODO Auto-generated method stub
		super.refreshVisuals();
	}

	protected void addChildVisual(EditPart childEditPart, int index) {
		// TODO Auto-generated method stub
		super.addChildVisual(childEditPart, index);
	}

	protected void createEditPolicies() {
		// EMPTY
	}

	protected void removeChildVisual(EditPart childEditPart) {
		super.removeChildVisual(childEditPart);
	}

	public DragTracker getDragTracker(Request arg0) {
		return null;
	}

}

// - UNUSED CODE ............................................................................................
// public Object getAdapter(Class key) {
// //if (AccessibleEditPart.class == key)
// return getAccessibleEditPart();
// //return Platform.getAdapterManager().getAdapter(this, key);
// }
// getModelChildren()
// @Override
// protected void addChildVisual(EditPart part, int index) {
// // TODO Auto-generated method stub
//		
// }
// @Override
// protected void removeChildVisual(EditPart arg0) {
// // TODO Auto-generated method stub
//		
// }
