//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/WarPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.pages;

// - IMPORT SECTION .........................................................................................
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.WarUnit;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;
import net.sourceforge.rcp.harpoon.test.MultiPropertyPage;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class SurfacePropertyPage extends MultiPropertyPage {
	private WarUnit	warModel;

	public void setModel(WarUnit model) {
		warModel = model;
		super.setModel(model);
	}

	@Override
	public void build() {
		// - Create the contents of the page structure.
		final RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;
		page = new Group(top, SWT.NONE);
		page.setLayout(rowLayout);
		page.setText(model.getName());
		// Text name = new Text(this, SWT.BORDER);
		// name.setText("USS Alpha");
		// name.setEditable(false);
		final Text location = new Text(page, SWT.BORDER);
		final String loc = model.getDMSLatitude().toDisplay() + " - " + model.getDMSLongitude().toDisplay();
		location.setText(loc);
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		createSpeedControl(page);
		createDirectionControl(page);
		createSensorsArea(page);

		speedSet.setSelection(warModel.getSpeed());
		// directionSet.setSelection(warModel.getDirection());
		speedSet.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				HarpoonLogger.info("New speed value: " + speedSet.getSelection());
				warModel.setSpeed(speedSet.getSelection());
			}

		});
		// speedSet.setDigits(warModel.getSpeed());
		// directionSet.setDigits(warModel.getDirection());
	}

	private void createSensorsArea(Composite parent) {
		sensorsArea = new Composite(parent, SWT.NONE);
		sensorsArea.setLayout(new RowLayout());
		sensorLabel = new Text(sensorsArea, SWT.BORDER);
		sensorLabel.setText("Sensors:");
		sensorLabel.setEditable(false);
		sensorLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		createSensorList(parent);
	}

	private void createSensorList(Composite parent) {
		final RowLayout rowLayout1 = new RowLayout();
		rowLayout1.type = org.eclipse.swt.SWT.VERTICAL;
		rowLayout1.spacing = 2;
		rowLayout1.marginBottom = 2;
		rowLayout1.marginLeft = 1;
		rowLayout1.marginRight = 1;
		rowLayout1.marginTop = 2;
		rowLayout1.wrap = false;
		final Composite sensorList = new Composite(sensorsArea, SWT.NONE);
		sensorList.setLayout(rowLayout1);
		final Button radar = new Button(sensorList, SWT.CHECK);
		radar.setText("Rad");
		radar.setSelection(warModel.getRadarState());
		radar.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				HarpoonLogger.info("Radar selected " + radar.getSelection());
				warModel.setRadarState(radar.getSelection());
			}

		});
		final Button EEM = new Button(sensorList, SWT.CHECK);
		EEM.setText("EEM");
		EEM.setSelection(warModel.getECMState());
	}
}

// - UNUSED CODE ............................................................................................
