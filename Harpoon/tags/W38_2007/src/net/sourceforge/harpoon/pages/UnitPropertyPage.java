//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: UnitPropertyPage.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/pages/UnitPropertyPage.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 12:11:30 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class MultiPropertyPage {
	protected Composite	top;
	protected Group			page;
	protected Unit	model;

	private Composite	speedControl	= null;
	private Text			speedLabel		= null;
	protected Spinner		speedSet			= null;
	private Composite	directionControl;
	private Text			directionLabel;
	protected Label		directionSet;
	protected Composite	sensorsArea		= null;
	protected Text			sensorLabel		= null;
	private Composite	sensorList		= null;
	private Button		radar					= null;
	private Button		sonar					= null;
	private Button		EEM						= null;

	// public MultiPropertyPage(Composite parent, int style) {
	// super(parent, style);
	// }

	public void setModel(Unit model) {
		this.model = model;
	}

	public Composite getPropertyPage() {
		return this.top;
	}

	public void setParent(Composite top) {
		this.top = top;
	}

	public void build() {
		// - Create the contents of the page structure.
		RowLayout rowLayout = new RowLayout();
		rowLayout.type = SWT.VERTICAL;
		rowLayout.marginLeft = 2;
		rowLayout.marginRight = 2;
		rowLayout.marginBottom = 1;
		rowLayout.marginTop = 1;
		rowLayout.wrap = false;
		page = new Group(top, SWT.NONE);
		page.setLayout(rowLayout);
		page.setText(model.getName());
		// Text name = new Text(this, SWT.BORDER);
		// name.setText("USS Alpha");
		// name.setEditable(false);
		Text location = new Text(page, SWT.BORDER);
		String loc = model.getDMSLatitude().toString() + " - " + model.getDMSLongitude().toString();
		location.setText(loc);
		location.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		location.setEditable(false);
		createDirectionControl(page);
		createSpeedControl(page);
		createSensorsArea(page);
	}

	protected void createDirectionControl(Composite parent) {
		org.eclipse.swt.layout.GridLayout gridLayout = new org.eclipse.swt.layout.GridLayout();
		gridLayout.numColumns = 2;
		gridLayout.horizontalSpacing=2;
		gridLayout.verticalSpacing=0;
		gridLayout.marginHeight=0;
		gridLayout.marginWidth=0;
		directionControl = new Composite(parent, SWT.NONE);
		directionControl.setLayout(gridLayout);
		directionLabel = new Text(directionControl, SWT.NONE);
		directionLabel.setEditable(false);
		directionLabel.setText("Direction:");
		directionLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		directionSet = new Label(directionControl, SWT.NONE);
		directionSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		directionSet.setText("000");
//		directionSet.setEditable(false);
	}

	protected void createSpeedControl(Composite parent) {
		org.eclipse.swt.layout.GridLayout gridLayout = new org.eclipse.swt.layout.GridLayout();
		gridLayout.numColumns = 2;
		speedControl = new Composite(parent, SWT.NONE);
		speedControl.setLayout(gridLayout);
		speedLabel = new Text(speedControl, SWT.NONE);
		speedLabel.setEditable(false);
		speedLabel.setText("Speed:");
		speedLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		speedSet = new Spinner(speedControl, SWT.NONE);
		speedSet.setFont(new Font(Display.getDefault(), "Tahoma", 9, SWT.NORMAL));
		speedSet.setMaximum(900);
//		speedSet.setDigits(10);
	}

	private void createSensorsArea(Composite parent) {
		sensorsArea = new Composite(parent, SWT.NONE);
		sensorsArea.setLayout(new RowLayout());
		sensorLabel = new Text(sensorsArea, SWT.BORDER);
		sensorLabel.setText("Sensors:");
		sensorLabel.setEditable(false);
		sensorLabel.setFont(new Font(Display.getDefault(), "Tahoma", 8, SWT.NORMAL));
		createSensorList(parent);
	}

	private void createSensorList(Composite parent) {
		RowLayout rowLayout1 = new RowLayout();
		rowLayout1.type = org.eclipse.swt.SWT.VERTICAL;
		rowLayout1.spacing = 2;
		rowLayout1.marginBottom = 2;
		rowLayout1.marginLeft = 1;
		rowLayout1.marginRight = 1;
		rowLayout1.marginTop = 2;
		rowLayout1.wrap = false;
		sensorList = new Composite(sensorsArea, SWT.NONE);
		sensorList.setLayout(rowLayout1);
		radar = new Button(sensorList, SWT.CHECK);
		radar.setText("Rad");
		sonar = new Button(sensorList, SWT.CHECK);
		sonar.setText("Son");
		EEM = new Button(sensorList, SWT.CHECK);
		EEM.setText("EEM");
	}
}

// - UNUSED CODE ............................................................................................
