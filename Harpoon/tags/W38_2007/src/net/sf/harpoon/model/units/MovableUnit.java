//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MovableUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/MovableUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-13 13:30:58  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//
//    Revision 1.1  2007-09-05 09:03:51  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//

package net.sourceforge.harpoon.model;

//- IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.geom.DMSPoint;

// - CLASS IMPLEMENTATION ...................................................................................
public class MovableUnit extends Unit {
	private static final long		serialVersionUID	= 2209528353626060016L;

	public static final String	MODEL							= "model";
	public static final String	DIRECTION					= "direction";
	public static final String	SPEED							= "speed";
	public static final String	MOVEMENTPATH			= "movementpath";

	public static final int			SURFACE						= 100;

	/** Stores the speed at witch the unit is traveling. This is only valid for units that may move. */
	private int									speed							= 0;
	/** Direction of movement of this Unit. This is the last value calculated from the current movement path. */
	private int									direction					= -1;
	/** This is the model description for the unit. In next releases will contain the database record reference. */
	private String							model							= "";
	/**
	 * The path points for the movement commands for the unit. This is a series of points where the unit should
	 * travel along. Once the unit reaches the last point it signal this event and continues in the same
	 * direction until a new path is set. This information is passed along to the Figure for drawing if the
	 * figure is selected.
	 */
	private MovementPath				path							= new MovementPath();

	// - C O N S T R U C T O R S
	// - P U B L I C S E C T I O N
	// - G E T T E R S / S E T T E R S
	/**
	 * Return the speed at witch is traveling the unit. There are coded ranges for each unit model and speed for
	 * conversion.
	 * 
	 * @return the current speed of the unit in knots.
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * Sets the speed on the unit. Range validation are outside the responsibilities of this class.
	 * 
	 * @param speed
	 *          speed at witch this unit should travel from this instant.
	 */
	public void setSpeed(int speed) {
		final int oldSpeed = this.speed;
		this.speed = speed;
		firePropertyChange(SPEED, oldSpeed, speed);
	}

	/**
	 * Gets the current calculated direction in degrees. North equivalence is 0 and represent the upward
	 * direction. Ranges from 0� to 360�. A no calculated value is signaled by a negative value and then the
	 * direction defaults to 0�.
	 * 
	 * @return direction of the unit in degrees.
	 */
	public int getDirection() {
		if (direction < 0) // A new calculation for the direction is fired.
			direction = getMovementPath().getDirection(new DMSPoint(latitude, longitude));
		return direction;
	}

	// /**
	// * Sets the travel direction of the unit. This property can not be set directly by through the use of a
	// * command path for movement. The first path point will be the goal towards this unit will travel and will
	// * then generate the travel direction.
	// *
	// * @param dir
	// * direction of movement. Toward the first travel path.
	// */
	// private void setDirection(int dir) {
	// //TODO Remove direct access to this attribute
	// this.direction = dir;
	// }

	// /**
	// * Sets the new unit movement direction by calculating the angle for the vector that joins the current
	// * location with the destination location.
	// *
	// * @param nextPoint
	// */
	// private void setDirection(DMSPoint nextPoint) {
	// // TODO calculate the vector between this two points and then get the direction angle.
	// // - Translate coordinates to point P1
	// DMSPoint p3 = nextPoint.substract(new DMSPoint(latitude, longitude));
	// // - Calculate quadrant depending on the point signs.
	// long lat = p3.getLatitude().toSeconds();
	// long lon = p3.getLongitude().toSeconds();
	// int quadrant = 0;
	// if ((lat >= 0) && (lon >= 0))
	// quadrant = 1;
	// else if ((lat >= 0) && (lon < 0))
	// quadrant = 4;
	// else if ((lat < 0) && (lon >= 0))
	// quadrant = 2;
	// else if ((lat < 0) && (lon < 0)) quadrant = 3;
	// // TODO Implement the rest of the quadrants
	// switch (quadrant) {
	// case 1:
	// // -Check if > 45�
	// if (lon > lat) {
	// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
	// double alpha = StrictMath.asin(lon / h);
	// this.setDirection(new Double(StrictMath.toDegrees(alpha)).intValue());
	// } else {
	// double h = StrictMath.sqrt(StrictMath.pow(lat, 2) + StrictMath.pow(lon, 2));
	// double alpha = StrictMath.acos(lat / h);
	// this.setDirection(new Double(StrictMath.toDegrees(alpha)).intValue());
	// }
	// break;
	//
	// default:
	// break;
	// }
	// }

	/**
	 * Sets the unit model description. This attribute may change in the future to a reference to a database
	 * record.
	 * 
	 * @param modeldescription
	 *          model description string.
	 */
	public void setModel(String modeldescription) {
		model = modeldescription;
	}

	/**
	 * Sets the new movement path for this unit. A movement path is a list of destination points that define the
	 * travel path for the unit.
	 * 
	 * @param movement
	 *          new travel path for this unit.
	 */
	public void setMovementPath(MovementPath movement) {
		if (null == movement) path = new MovementPath();
		path = movement;
		// - Clear direction cache so next accesses will recalculate the value.
		direction = -1;
		// if (null != this.path) setDirection(path.getNextPoint());
	}

	/**
	 * Return the current order of movement for this unit. This can be represented visually on the map as a set
	 * of connected points.
	 * 
	 * @return the current movement orders.
	 */
	public MovementPath getMovementPath() {
		if (null == path) path = new MovementPath();
		return path;
	}

	public int getSubType() {
		// TODO IMplement the code to return the correct subtype.
		return SURFACE;
	}

}
// - UNUSED CODE ............................................................................................
