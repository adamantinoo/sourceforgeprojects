//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: WarUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/model/units/WarUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.harpoon.model;

// - IMPORT SECTION .........................................................................................
import java.util.LinkedList;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class WarUnit extends MovableUnit {
	private static final long										serialVersionUID	= -3514624844988656815L;

	public static final String									RADAR							= "radar";
	public static final String									ECM								= "ecm";
	private transient SensorStructure						sensors						= new SensorStructure();
	@SuppressWarnings("unused")
	private final transient LinkedList<Weapon>	weapons						= new LinkedList<Weapon>();

	public boolean getRadarState() {
		if (null == sensors) sensors = new SensorStructure();
		return sensors.getRadarState();
	}

	public void setRadarState(boolean newState) {
		if (null == sensors) sensors = new SensorStructure();
		final boolean oldState = sensors.getRadarState();
		sensors.setRadarState(newState);
		firePropertyChange(RADAR, oldState, newState);
	}

	public boolean getECMState() {
		if (null == sensors) sensors = new SensorStructure();
		return sensors.getECMState();
	}

	public void setECMState(boolean newState) {
		if (null == sensors) sensors = new SensorStructure();
		final boolean oldState = sensors.getECMState();
		sensors.getECMState(newState);
		firePropertyChange(ECM, oldState, newState);
	}

	class SensorStructure {
		private boolean	radar	= false;
		private boolean	ECM		= false;

		// public void activateRadar() {
		// radar = true;
		// }
		//
		// public void deactivateRadar() {
		// radar = false;
		// }
		//
		// public void activateECM() {
		// ECM = true;
		// }
		//
		// public void deactivateECM() {
		// ECM = false;
		// }

		public boolean getRadarState() {
			return radar;
		}

		public boolean getECMState() {
			return ECM;
		}

		public void setRadarState(boolean newState) {
			radar = newState;
		}

		public void getECMState(boolean newState) {
			ECM = newState;
		}
	}

}

class Weapon {

}

// - UNUSED CODE ............................................................................................
