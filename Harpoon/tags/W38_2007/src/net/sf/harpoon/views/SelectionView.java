//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: SelectionView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/views/SelectionView.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:19:47 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.3  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.2  2007-09-17 15:11:40  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//
//    Revision 1.1  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//

package net.sourceforge.rcp.harpoon.views;

// - IMPORT SECTION .........................................................................................
import harpoonrcp.View;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import org.eclipse.gef.editparts.AbstractGraphicalEditPart;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.parts.WarPart;
import net.sourceforge.rcp.harpoon.test.MultiPart;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N

/**
 * This view is responsible to display the properties of the selected unit when there is only one unit
 * selected or if the selection contains more than one item, it should display the list of elements in the
 * selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection
 * to the newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class SelectionView extends View {
	public static final String		ID	= "net.sourceforge.rcp.harpoon.app.selectionview";
	private static SelectionView	singleton;

	/**
	 * Top element in the display hierarchy. The children of this element are the oned disposed when changing
	 * the selection.
	 */
	private Composite							top;

	// private Group link;
	// private Text selectionDescription;
	// private Composite properties;
	// private Composite banner;
	// private Hashtable selection = new Hashtable();
	// private TableViewer viewer;

	// - P U B L I C S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	public void createPartControl(Composite parent) {
		singleton = this;
		top = new Composite(parent, SWT.NONE);
		FillLayout layout = new FillLayout();
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		layout.type = SWT.VERTICAL;
		top.setLayout(layout);
	}

	public Composite getTopControl() {
		return this.top;
	}

	// public void createPartControl2(Composite parent) {
	// singleton = this;
	// // - Create the base parts until the link Composite.
	// top = new Composite(parent, SWT.NONE);
	// FillLayout layout = new FillLayout();
	// layout.marginHeight = 2;
	// layout.marginWidth = 2;
	// layout.type = SWT.VERTICAL;
	// top.setLayout(layout);
	//
	// Composite banner = new Composite(top, SWT.NONE);
	// banner.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL, GridData.VERTICAL_ALIGN_BEGINNING,
	// true, false));
	// GridLayout layout2 = new GridLayout();
	// layout2.marginHeight = 5;
	// layout2.marginWidth = 10;
	// layout2.numColumns = 1;
	// banner.setLayout(layout2);
	//
	// // setup bold font
	// Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);
	//
	// Label title = new Label(banner, SWT.WRAP);
	// title.setText("Selection Parts:");
	// title.setFont(boldFont);
	//
	// selectionDescription = new Text(banner, SWT.MULTI | SWT.WRAP);
	// selectionDescription.setText("<empty current selection>");
	// selectionDescription.setLayoutData(new GridData(GridData.FILL_BOTH));
	//
	// createDefaultSelection(top);
	// }

	// private void createDefaultSelection(Composite top) {
	// // - Default content at initialization.
	// // SashForm top = new SashForm(link, SWT.NONE);
	//
	// link = new Group(top, SWT.NONE);
	// link.setLayout(new FillLayout());
	// ((FillLayout) link.getLayout()).type = SWT.VERTICAL;
	// link.setText("Properties:");
	//
	// // AirPart defaultPart = new AirPart();
	// // AirUnit defaultUnit = new AirUnit();
	// // defaultUnit.setName("default value for initializaation of the viewer");
	// // defaultPart.setModel(defaultUnit);
	// // selection.put(defaultUnit.getName(), defaultPart);
	// //
	// // viewer = new TableViewer(top, SWT.FULL_SELECTION);
	// // viewer.setContentProvider(new IStructuredContentProvider() {
	// // public void dispose() {
	// // };
	// // public Object[] getElements(Object inputElement) {
	// // Collection<Object> selectionValues = selection.values();
	// // return selectionValues.toArray();
	// // };
	// // public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object oldInput, Object newInput) {
	// // }
	// // });
	// // createColumns();
	// //
	// // viewer.setLabelProvider(new OwnerDrawLabelProvider() {
	// // protected void measure(Event event, Object element) {
	// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
	// // Unit model = (Unit) part.getModel();
	// // Point size = event.gc.textExtent(model.getName());
	// // event.width = viewer.getTable().getColumn(event.index).getWidth();
	// // int lines = size.x / event.width + 1;
	// // event.height = size.y * lines;
	// // }
	// // protected void paint(Event event, Object element) {
	// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
	// // Unit model = (Unit) part.getModel();
	// // event.gc.drawText(model.getName(), event.x, event.y, true);
	// // }
	// // });
	// //
	// // viewer.setInput(this);
	// // GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL | GridData.FILL_BOTH);
	// // viewer.getControl().setLayoutData(data);
	// // OwnerDrawLabelProvider.setUpOwnerDraw(viewer);
	// // viewer.setSelection(new StructuredSelection(defaultPart));
	// }

	// private void createColumns() {
	// TableLayout layout = new TableLayout();
	// viewer.getTable().setLayout(layout);
	// viewer.getTable().setHeaderVisible(true);
	// viewer.getTable().setLinesVisible(true);
	//
	// TableColumn tc = new TableColumn(viewer.getTable(), SWT.NONE, 0);
	// layout.addColumnData(new ColumnPixelData(350));
	// tc.setText("Selection");
	// }

	private void clearTopControl() {
		Control[] childs = top.getChildren();
		for (int i = 0; i < childs.length; i++) {
			Control child = childs[i];
			child.dispose();
		}
	}

	public void updateSelection(StructuredSelection sel) {
		if (!sel.isEmpty()) {
			Iterator it = sel.iterator();
			while (it.hasNext()) {
				AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) it.next();
				Unit model = (Unit) part.getModel();
				// TODO Getting the properties is only implemented on the MultiPart
				if (part instanceof MultiPart) {
					MultiPart multi = (MultiPart) part;
					Composite page = multi.createPropertyPage(top);
					page.redraw();
					page.changed(page.getChildren());
				}
				if (part instanceof WarPart) {
					clearTopControl();
					WarPart war = (WarPart) part;
					Composite page = war.createPropertyPage(top);
					page.redraw();
					page.changed(page.getChildren());
				}
				top.redraw();
				top.changed(top.getChildren());
			}
		}
	}

	public void updateSelection(Hashtable selectionContent) {
		// - Clear the current contents of the selection view
		clearTopControl();

		// - Count the elements in the selection to check if we display a property page or a table
		if (selectionContent.size() > 1) {
			// - Display a table with the selection elements
			newTableViewer(selectionContent);
		} else {
			// - Get the property page for the selection and link it to the view contents.
			Enumeration it = selectionContent.elements();
			while (it.hasMoreElements()) {
				AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) it.nextElement();
				Unit model = (Unit) part.getModel();
				// TODO Getting the properties is only implemented on the MultiPart
				if (part instanceof MultiPart) {
					MultiPart multi = (MultiPart) part;
					Composite page = multi.createPropertyPage(top);
					page.redraw();
					page.changed(page.getChildren());
					top.redraw();
					top.changed(top.getChildren());
				}
			}
		}
	}

	private void newTableViewer(Hashtable selectionContent) {
		// TODO Auto-generated method stub
		// // viewer = new TableViewer(top, SWT.FULL_SELECTION);
		// // viewer.setContentProvider(new IStructuredContentProvider() {
		// // public void dispose() {
		// // };
		// // public Object[] getElements(Object inputElement) {
		// // Collection<Object> selectionValues = selection.values();
		// // return selectionValues.toArray();
		// // };
		// // public void inputChanged(org.eclipse.jface.viewers.Viewer viewer, Object oldInput, Object newInput)
		// {
		// // }
		// // });
		// // createColumns();
		// //
		// // viewer.setLabelProvider(new OwnerDrawLabelProvider() {
		// // protected void measure(Event event, Object element) {
		// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
		// // Unit model = (Unit) part.getModel();
		// // Point size = event.gc.textExtent(model.getName());
		// // event.width = viewer.getTable().getColumn(event.index).getWidth();
		// // int lines = size.x / event.width + 1;
		// // event.height = size.y * lines;
		// // }
		// // protected void paint(Event event, Object element) {
		// // AbstractGraphicalEditPart part = (AbstractGraphicalEditPart) element;
		// // Unit model = (Unit) part.getModel();
		// // event.gc.drawText(model.getName(), event.x, event.y, true);
		// // }
		// // });
		// //
		// // viewer.setInput(this);
		// // GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL |
		// GridData.FILL_BOTH);
		// // viewer.getControl().setLayoutData(data);
		// // OwnerDrawLabelProvider.setUpOwnerDraw(viewer);
		// // viewer.setSelection(new StructuredSelection(defaultPart));

	}

	public static SelectionView getSingleton() {
		return singleton;
	}

}

// - UNUSED CODE ............................................................................................
