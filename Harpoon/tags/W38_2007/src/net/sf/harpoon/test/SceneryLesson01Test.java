//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: SceneryLesson01Test.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/SceneryLesson01Test.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:19:47 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-19 13:16:29  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - [A0013.01] - Compose the main screen distribution.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import junit.framework.TestCase;

import net.sourceforge.harpoon.geom.DMSCoordinate;
import net.sourceforge.harpoon.model.AirUnit;
import net.sourceforge.harpoon.model.AirportUnit;
import net.sourceforge.harpoon.model.MovementPath;
import net.sourceforge.harpoon.model.RootMapUnit;
import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.model.WarUnit;

// - CLASS IMPLEMENTATION ...................................................................................
public class SceneryConstructorTest extends TestCase {
	public void testLesson01Scenery() throws Exception {
		FileOutputStream fos = new FileOutputStream("U:/ldiego/Workstage/Harpoon/sceneries/Lesson01.harpoon");
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		RootMapUnit model = new RootMapUnit();
		createLesson01Scenery(model);
		oos.writeObject(model);

		oos.close();
	}

	/**
	 * Create a testing units.<br>
	 * Latitudes are made south because the model has a location of 0-0 at the top-left. Below that location (y
	 * coordinates positive) we have units in the south hemisphere.
	 */
	private void createLesson01Scenery(RootMapUnit model) {
		// - Clean the current model first.
		model.clear();

		// - Create Friend units
		AirportUnit base = new AirportUnit();
		base.setName("Interoceanic airport");
		base.setSide(Unit.FRIEND);
		base.setLatitude(new DMSCoordinate(1, 0, 0, 'S'));
		base.setLongitude(new DMSCoordinate(1, 0, 0, 'E'));
		model.addChild(base);

		AirUnit aunit = new AirUnit();
		aunit.setName("F16 Gamma");
		aunit.setSide(Unit.FRIEND);
		aunit.setLatitude(new DMSCoordinate(0, 6, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		aunit.setSpeed(500);
		// aunit.setDirection(100.0);
		model.addChild(aunit);

		WarUnit surface = new WarUnit();
		// - Fields for a Unit
		surface.setName("Frigate USS Solaris");
		surface.setLatitude(new DMSCoordinate(0, 13, 0, 'S'));
		surface.setLongitude(new DMSCoordinate(0, 12, 0, 'E'));
		surface.setSide(Unit.FRIEND);
		// - Fields for a MovableUnit
		surface.setModel("Frigate Surface vessel");
		MovementPath path = new MovementPath();
		path.addPoint(new DMSCoordinate(0, 12, 0, 'S'), new DMSCoordinate(0, 13, 0, 'E')); // surface.setDirection(45);
		surface.setSpeed(21);
		surface.setMovementPath(path);
		model.addChild(surface);

		// - Create Foe units
		base = new AirportUnit();
		base.setName("Enemy airport");
		base.setSide(Unit.FOE);
		base.setLatitude(new DMSCoordinate(0, 12, 0, 'S'));
		base.setLongitude(new DMSCoordinate(0, 6, 0, 'E'));
		model.addChild(base);

		aunit = new AirUnit();
		aunit.setName("SUv k21");
		aunit.setSide(Unit.FOE);
		aunit.setLatitude(new DMSCoordinate(0, 2, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 6, 0, 'E'));
		aunit.setSpeed(500);
		// aunit.setDirection(200.0);
		model.addChild(aunit);

		// - Create neutral units
		aunit = new AirUnit();
		aunit.setName("Boeing 747");
		aunit.setSide(Unit.NEUTRAL);
		aunit.setLatitude(new DMSCoordinate(0, 8, 0, 'S'));
		aunit.setLongitude(new DMSCoordinate(0, 2, 0, 'E'));
		aunit.setSpeed(500);
		// aunit.setDirection(200.0);
		model.addChild(aunit);
	}
}

// - UNUSED CODE ............................................................................................
