//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MultiUnit.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/MultiUnit.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:13:07 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class MultiUnit extends Unit {

	private UnitProfile	profile	= new UnitProfile();

	public UnitProfile getProfile() {
//		if (null == this.profile) this.profile = new Defaultprofile();
		return this.profile;
	}

	public void setProfile(UnitProfile profile) {
		if (null != profile) this.profile = profile;
	}

	// public int getMargin() {
	// return this.margin;
	// }

	// public IconFigure getSubtype() {
	// // TODO Auto-generated method stub
	// if(null==this.drawingFigure)this.drawingFigure=new DefaultFigure();
	// return this.drawingFigure;
	// }

	// public void setType(IconFigure drawingFigure, String string) {
	// // TODO Auto-generated method stub
	// this.drawingFigure=drawingFigure;
	// }
//	class Defaultprofile extends UnitProfile {
//
//	}

//	class DefaultFigure extends IconFigure {
//
//		public DefaultFigure(UnitFigure figure) {
//			super(figure);
//			// TODO Auto-generated constructor stub
//		}
//
//		public DefaultFigure() {
//			// TODO Auto-generated constructor stub
//		}
//
//		@Override
//		public Dimension getHotSpot() {
//			// TODO Auto-generated method stub
//			return new Dimension(0, 0);
//		}
//
//		@Override
//		protected void paintFigure(Graphics graphics) {
//			// TODO Auto-generated method stub
//
//		}
//
//	}

}

// - UNUSED CODE ............................................................................................
