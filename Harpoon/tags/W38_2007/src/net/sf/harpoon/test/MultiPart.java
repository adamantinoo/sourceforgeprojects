//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: MultiPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/MultiPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-17 15:13:06  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.GridLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import net.sourceforge.harpoon.figures.MultiFigure;
import net.sourceforge.harpoon.parts.UnitPart;
import net.sourceforge.rcp.harpoon.log.HarpoonLogger;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class MultiPart extends UnitPart {
	@Override
	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		MultiUnit model = (MultiUnit) this.getModel();
		HarpoonLogger.log(Level.FINE, "Creating Figure MultiFigure of profile " + model.getProfile().getName());
		MultiFigure fig = new MultiFigure(model.getProfile());
		fig.setName(model.getName());
		fig.setSide(model.getSide());
		return fig;
	}

	public Composite createPropertyPage(Composite top) {
		// - Create the root of the page structure.
		MultiPropertyPage page = new MultiPropertyPage();
		page.setParent(top);
		page.setModel((MultiUnit) this.getModel());
		page.build();
		return page.getPropertyPage();
	}

	class Dummy extends MultiFigure {
		public Dummy(UnitProfile profile) {
			super(profile);
			// TODO Auto-generated constructor stub
		}

		private static final int	CHAR_WIDTH			= 6;
		private static final int	CHAR_HEIGHT			= 13;
		private UnitProfile				profile;
		private int								margin;
		private Label							directionLabel	= new Label("000");
		private Label							speedLabel			= new Label("00");

		protected void paintFigure(Graphics graphics) {
			// TODO Auto-generated method stub
			super.paintFigure(graphics);
		}

		protected void paintChildren(Graphics graphics) {
			IFigure child;

			Rectangle clip = Rectangle.SINGLETON;
			int si = 1;
			for (int i = 0; i < children.size(); i++) {
				child = (IFigure) children.get(i);
				Rectangle newClip = graphics.getClip(clip);
				boolean insersect = child.intersects(newClip);
				if (child.isVisible() && insersect) {
					graphics.clipRect(child.getBounds());
					child.paint(graphics);
					graphics.restoreState();
				}
			}
		}

		public Dimension getPreferredSize(int wHint, int hHint) {
			// - Size of left side is 3 characters wide plus margins.
			Dimension leftSize = new Dimension((3 + 1) * CHAR_WIDTH + margin, CHAR_HEIGHT * 2 + margin);

			// - Size of the icon is the result of this method.
			Dimension iconicSize = iconic.getSize();

			// - Size of the right part is the width of the name label
			Dimension rightSize = new Dimension((nameLabel.getText().length() + 1) * CHAR_WIDTH + 2, CHAR_HEIGHT);

			HarpoonLogger.log(Level.FINE, "left size:" + leftSize);
			HarpoonLogger.log(Level.FINE, "icon size:" + iconicSize);
			HarpoonLogger.log(Level.FINE, "right size:" + rightSize);

			Dimension fullSize = new Dimension(0, 0);
			fullSize.width = margin + leftSize.width + margin + iconicSize.width + margin + rightSize.width
					+ margin;
			fullSize.height = margin + Math.max(leftSize.height, Math.max(iconicSize.height, rightSize.height))
					+ margin;
			HarpoonLogger.log(Level.FINE, "final size:" + fullSize);
			return fullSize;
		}

		/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
		public Dimension getHotSpot() {
			Dimension hot = iconic.getHotSpot();
			Dimension leftSize = new Dimension((3 + 1) * CHAR_WIDTH + margin, CHAR_HEIGHT * 2 + margin);

			return new Dimension(hot.width + leftSize.width + margin, hot.height + margin);
		}

		public String toString() {
			StringBuffer buffer = new StringBuffer();
			buffer.append("MultiFigure(" + nameLabel.getText());
			buffer.append(",");
			buffer.append("preferredsize:" + getPreferredSize());
			buffer.append(",");
			buffer.append("size:" + getSize());
			buffer.append(",");
			buffer.append("bounds:" + getBounds());
			return buffer.toString();
		}

		private void createLeftSide(MultiFigure multiFigure) {
			GridLayout grid = new GridLayout();
			grid.numColumns = 1;
			grid.horizontalSpacing = margin;
			grid.marginHeight = 0;
			grid.marginWidth = margin;
			grid.verticalSpacing = 0;
			Figure left = new Figure();
			left.setLayoutManager(grid);
			directionLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			directionLabel.setBorder(new LineBorder(1));
			speedLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			speedLabel.setBorder(new LineBorder(1));
			left.add(directionLabel);
			left.add(speedLabel);
		}

		private void createRightSide(MultiFigure multiFigure) {
			GridLayout grid = new GridLayout();
			grid.numColumns = 1;
			grid.horizontalSpacing = margin;
			grid.marginHeight = 0;
			grid.marginWidth = margin;
			grid.verticalSpacing = 0;
			Figure right = new Figure();
			right.setLayoutManager(grid);
			nameLabel.setFont(new Font(Display.getDefault(), "Consolas", 8, SWT.NORMAL));
			nameLabel.setBorder(new LineBorder(1));
			right.add(nameLabel);
			this.add(right);
		}
	}

}

// - UNUSED CODE ............................................................................................
