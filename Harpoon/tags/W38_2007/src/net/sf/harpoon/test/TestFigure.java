//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: TestFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/TestFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 11:22:26 $
//  RELEASE:        $Revision: 1.3 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.2  2007-09-19 13:12:41  ldiego
//    - DEFECT The drawing for the airport is not completed.
//    - [A0037.01] - Adapt perspective to new model.
//    - SAVEPOINT Before starting the development for stage Draft 00.03
//      codenamed "Lesson 01". At this savepoint the application does not compile
//      and some files have been modified to adapt to new model changes.
//
//    Revision 1.1  2007-09-17 15:13:07  ldiego
//    - [A0056.01] - Selection has to change property page.
//    - [A0022.01] - Property viewer for each unit.
//    - TASK Instantiate Figures by name.
//    - TASK Check if the Parts may be defined as local classes.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.figures.UnitFigure;
import net.sourceforge.harpoon.model.Unit;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
abstract class HasIconFigure extends UnitFigure {
	/** Stores the reference to the class that draws the unit icon representation. */
	protected static final int LAYOUT_MARGIN = 2;
	protected Label							nameLabel	= new Label("Test Figure");
	
	public HasIconFigure(){
		super();
	}
}
public class TestFigure extends HasIconFigure {

	// - C O N S T R U C T O R S
	/**
	 * Creates all the drawing elements that compose the representation of an Airport. This is drawn as a square
	 * box with some lines inside with a name label at the right center.<br>
	 * The color of the icon box depends on the side of the unit.<br>
	 * The location is centered on the center of the icon.
	 */
	public TestFigure() {
		super();
	}
	// - P U B L I C S E C T I O N
	/**
	 * Sets the name of the label. The size of the parent figure has to be adjusted, but tet if the real soize
	 * of the label has also to be adjusted.
	 */
	public void setName(String name) {
		nameLabel.setText(name);
		Dimension size = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		size = new Dimension((nameLabel.getText().length() + 1) * 6 + 2, FigureUtilities.getStringExtents(
				nameLabel.getText(), nameLabel.getFont()).height);
		// size.width+=8;
		nameLabel.setSize(size);
		nameLabel.setPreferredSize(size);
		// nameLabel.setBorder(new LineBorder(1));
		this.invalidate();
		TestPart.log.log(Level.FINE, "values for nameLabel after setting the new name");
		TestPart.log.log(Level.FINE, nameLabel.getText());
		TestPart.log.log(Level.FINE, nameLabel.getBounds().toString());
		// - Recalculate size and bounds
		this.setSize(this.getPreferredSize(-1, -1));
	}

	public void setSide(int newSide) {
		if (Unit.FRIEND == newSide) this.setColor(HarpoonColorConstants.FRIEND);
		if (Unit.FOE == newSide) this.setColor(HarpoonColorConstants.FOE);
		if (Unit.NEUTRAL == newSide) this.setColor(HarpoonColorConstants.NEUTRAL);
		if (Unit.UNKNOWN_SIDE == newSide) this.setColor(HarpoonColorConstants.UNKNOWN_SIDE);
	}

	/** Return a dimension with the vector from the top-left coordinate to the hotspot figure location. */
	public Dimension getHotSpot() {
		Dimension hot = iconic.getHotSpot();
		return new Dimension(hot.width+LAYOUT_MARGIN*2,hot.height+LAYOUT_MARGIN*2);
	}
	public Dimension getPreferredSize(int wHint, int hHint) {
		// - Get the sized of the composition objects.
		Dimension iconicSize = iconic.getSize();
		Dimension nameLabelSize2 = FigureUtilities.getStringExtents(nameLabel.getText(), nameLabel.getFont());
		// DEBUG Compare calculated size with current label size - No size set because name not changed
		Dimension lab = nameLabel.getSize();
		Dimension nameLabelSize = new Dimension((nameLabel.getText().length() + 1) * 6 + 2, nameLabelSize2.height);

		TestPart.log.log(Level.FINE, "icon size:" + iconicSize);
		TestPart.log.log(Level.FINE, "calculated label size:" + nameLabelSize);
		TestPart.log.log(Level.FINE, "label size:" + lab);

		Dimension fullSize = new Dimension(0, 0);
		fullSize.width = LAYOUT_MARGIN + iconicSize.width + LAYOUT_MARGIN + nameLabelSize.width-1;
		fullSize.height = LAYOUT_MARGIN + Math.max(iconicSize.height, nameLabelSize.height) + LAYOUT_MARGIN + 1;
		TestPart.log.log(Level.FINE, "final size:" + fullSize);
		return fullSize;
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("TestFigure("+nameLabel.getText());
		buffer.append(",");
		buffer.append("preferredsize:"+getPreferredSize());
		buffer.append(",");
		buffer.append("size:"+getSize());
		buffer.append(",");
		buffer.append("bounds:"+getBounds());
		return buffer.toString();
	}
@Override
protected void paintFigure(Graphics graphics) {
	// TODO Auto-generated method stub
	super.paintFigure(graphics);
}

}

// - UNUSED CODE ............................................................................................
