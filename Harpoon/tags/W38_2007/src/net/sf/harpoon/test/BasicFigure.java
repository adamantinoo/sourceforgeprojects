//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: BasicFigure.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/BasicFigure.java,v $
//  LAST UPDATE:    $Date: 2007-09-21 12:11:30 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;

import net.sourceforge.harpoon.HarpoonColorConstants;
import net.sourceforge.harpoon.figures.IconFigure;
import net.sourceforge.harpoon.figures.UnitFigure;

// - CLASS IMPLEMENTATION ...................................................................................
// - C O N S T R U C T O R S
// - P U B L I C S E C T I O N
// - G E T T E R S / S E T T E R S
// - P R I V A T E S E C T I O N
public class BasicFigure extends IconFigure {
	protected int	drawingSize	= 16;

	public BasicFigure(UnitFigure parentFigure) {
		super(parentFigure);
		this.setSize(this.getPreferredSize());
	}

	public Dimension getHotSpot() {
		return new Dimension(drawingSize / 2, drawingSize / 2);
	}

	public Dimension getPreferredSize(int wHint, int hHint) {
		return new Dimension(drawingSize, drawingSize);
	}

	protected void paintFigure(Graphics graphics) {
	}

	protected void setDrawingSize(int figureSize) {
		this.drawingSize = figureSize;
		this.setSize(figureSize,figureSize);
	}

	public void drawHandles(Graphics graphics) {
		Rectangle bound = getBounds().getCopy();
		bound.width -= 1;
		bound.height -= 1;
		Point p1 = new Point(bound.x + bound.width / 2, bound.y);
		Point p2 = new Point(bound.x + bound.width / 2, bound.y + bound.height / 2);
		Point p3 = new Point(bound.x + bound.width, bound.y + bound.height / 2);
		Point p4 = new Point(bound.x + bound.width / 2, bound.y + bound.height);
		Point p5 = new Point(bound.x, bound.y + bound.height / 2);
		graphics.setBackgroundColor(HarpoonColorConstants.HANDLE_COLOR);
		// graphics.drawOval(bound.x - 1, bound.y - 1, bound.width + 1, bound.height + 1);
		graphics.fillRectangle(new Rectangle(new Point(p1.x, p1.y), new Point(p1.x + 1, p1.y + 2)));
		graphics.fillRectangle(new Rectangle(new Point(p2.x, p2.y), new Point(p2.x + 1, p2.y + 1)));
		graphics.fillRectangle(new Rectangle(new Point(p3.x - 2, p3.y), new Point(p3.x, p3.y + 1)));
		graphics.fillRectangle(new Rectangle(new Point(p4.x, p4.y), new Point(p4.x + 1, p4.y - 2)));
		graphics.fillRectangle(new Rectangle(new Point(p5.x, p5.y), new Point(p5.x + 2, p5.y + 1)));
	}
}

// - UNUSED CODE ............................................................................................
