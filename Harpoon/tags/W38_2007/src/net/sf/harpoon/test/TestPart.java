//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: TestPart.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sf/harpoon/test/TestPart.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:13:06 $
//  RELEASE:        $Revision: 1.2 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-12 11:26:29  ldiego
//    - [A0018.01] - Ship units have to be able to be selected.
//    - TASK Simplify Figure constructors.
//    - [A0021.01] - Create a palette of colors to be accessible by all code.
//    - [A0042.01] - The airport unit can be selected.
//    - [A0045.01] - New model structure. Include Scenery.
//    - TASK Change test map data to a 0-0 coordinate system.
//    - TASK Add support for logging and trace.
//    - [A0008.01] - Internal coordinate representation is decimal degress.
//    - [A0048.01] - Implement lazy evaluation for properties.
//    - [B0027.01] - Change internal coordinate calculations to seconds.
//    - TASK Refactoring of code. Phase 1.
//
//    Revision 1.3  2007-09-07 12:28:13  ldiego
//    - TASK Rename class UnitEditPart to UnitPart.
//    - TASK Merge classes to new model.
//
//    Revision 1.2  2007-09-05 09:03:52  ldiego
//    - TASK Review completely the new graphical presentation for Airports
//    - TASK The unit information appears incomplete.
//
//    Revision 1.1  2007-09-05 07:48:46  ldiego
//    - Registration of this class before comparison with working
//      previous version.
//

package net.sourceforge.rcp.harpoon.test;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.draw2d.IFigure;

import net.sourceforge.harpoon.model.Unit;
import net.sourceforge.harpoon.parts.UnitPart;

// - CLASS IMPLEMENTATION ...................................................................................
public class TestPart extends UnitPart {
	public static Logger									log							= Logger.getLogger("net.sourceforge");

	protected IFigure createFigure() {
		// - Create the new figure and the load the model data to be able to represent the information.
		Unit model = (Unit) this.getModel();
		log.log(Level.FINE, "Creating Figure TestFigure");
		TestFigure fig= new TestFigure();
		fig.setName(model.getName());
		fig.setSide(model.getSide());
		log.log(Level.INFO, fig.toString());
		return fig;
	}
}
// - UNUSED CODE ............................................................................................
