//  PROJECT:        HarpoonRCP
//  FILE NAME:      $RCSfile: TestView.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src_rcp/harpoonrcp/TestView.java,v $
//  LAST UPDATE:    $Date: 2007-09-17 15:11:40 $
//  RELEASE:        $Revision: 1.1 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: not supported by cvs2svn $
//    Revision 1.1  2007-09-13 13:25:20  ldiego
//    - TASK Separate the Model from the View on the HarpoonMap class.
//    - [A0004.01] - Map size must adapt to windows size.
//    - [A0047.01] - Implement caching for map images.
//    - DEFECT The windows does not update when resized.
//    - TASK Integrate the reading of the scenery into the UI.
//    - TASK Selection visual feedback is not updated.
//

package net.sourceforge.rcp.harpoon.app;

// - IMPORT SECTION .........................................................................................
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import net.sourceforge.rcp.harpoon.View;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This view is responsible to display the properties of the selected unit is there is only one unit selected
 * or is the selection contains more than one item, it should display the list of elements in the selection.<br>
 * If the list of elements is shown, it may be able to detect selection on an element to change the selection
 * to the newly selected unit.<br>
 * <br>
 * It inherits all the code from the example until that code is substituted and removed from the project.
 */
public class TestView extends View {
	public static final String	ID	= "net.sourceforge.rcp.harpoon.app.testview";
	private Group								link;
	private Composite						top;
	private static TestView			singleton;

	// - P U B L I C S E C T I O N
	/**
	 * This is the method called during creation and initialization of the view. The view must be able to change
	 * their presentation dynamically depending on the selection, so there should be a link point where other
	 * content structures can plug-in to be displayed.
	 */
	public void createPartControl(Composite parent) {
		singleton = this;
		// - Create the base parts until the link Composite.
		top = new Composite(parent, SWT.NONE);
		FillLayout layout = new FillLayout();
		layout.marginHeight = 2;
		layout.marginWidth = 2;
		top.setLayout(layout);
		((FillLayout) top.getLayout()).type=SWT.VERTICAL;

		// - Create the title label and then the link layer
		Font boldFont = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);

		Label title = new Label(top, SWT.WRAP);
		title.setText("Below this line the block to be changed");
		title.setFont(boldFont);

		link = new Group(top, SWT.NONE);
		link.setText("Original");
		link.setLayout(new FillLayout());
		((FillLayout) link.getLayout()).type=SWT.VERTICAL;
		Label line = new Label(link, SWT.NONE);
		line.setText("Line 1 for original");
		line = new Label(link, SWT.NONE);
		line.setText("Line 2 for original");
	}

	public void doChange() {
		// TODO Auto-generated method stub
		link.dispose();

		top.redraw();
		top.changed(top.getChildren());

		link = new Group(top, SWT.NONE);
		link.setText("Altered");
		link.setLayout(new FillLayout());
		Label line = new Label(link, SWT.NONE);
		line.setText("Line 1 for altered");
		line = new Label(link, SWT.NONE);
		line.setText("Line 2 for altered");
		link.redraw();
		link.changed(link.getChildren());

		top.redraw();
		top.changed(top.getChildren());
		this.setFocus();
	}
@Override
public void setFocus() {
	// TODO Auto-generated method stub
	super.setFocus();
}
	public static TestView getSingleton() {
		return singleton;
	}
}

// - UNUSED CODE ............................................................................................
