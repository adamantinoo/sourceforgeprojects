//  PROJECT:        Harpoon
//  FILE NAME:      $RCSfile: PropertyModel.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/eclipse/BaseWorkspace/HarpoonRCP/src/net/sourceforge/harpoon/model/PropertyModel.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2007 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: PropertyModel.java,v $
//    Revision 1.5  2007-11-16 10:50:18  ldiego
//    - MOVEMENTPATH.R01.A - Movement path changes.
//
//    Revision 1.4  2007-11-12 11:23:49  ldiego
//    - TASK Changed the implementation of the detection procedure.
//      Sets a new detection scale and updates the presentation with
//      properties updates.
//    - DEFECT Increased the size of the labels and corrected the
//      presentation bug that made short numbers to be cut.
//    - TASK The method UnitPart.refreshVisuals has to update Unit fields.
//    - TASK Unit side type has to be moved to String.
//    - TASK Merge classes MapFigure y HarpoonMap.
//    - DEFECT Range size does not match.
//    - DEFECT Deactivation of sensor does not degrade unit state.
//
//    Revision 1.3  2007-11-02 09:34:50  ldiego
//    - TASK Merged the L02_MovementPath into the HEAD revision.
//    - TASK Partial implementation of Refactoring of code. Phase 4.
//
//    Revision 1.2.2.2  2007-10-31 14:47:36  ldiego
//    - [REQUIREMENT A0114.04] - Lesson 02.04 First Reference point.
//    - TASK Changed most of the selection and selection notification
//      mechanics.
//    - TASK Added new fields for location and selection.
//    - TASK Changed some methods related to this change in the
//      MovementPath functionality.
//
//    Revision 1.2.2.1  2007-10-18 16:53:42  ldiego
//    - DEFECT During initialization the properties were not copied
//      to the RootMapFigure. Now they are.
//    - Initialize cached Map properties if properties set.
//    - Added testing code to draw the movement traces.
//    - DEFECT Corrected the calculation of coordinates and the
//      angle traslation.
//    - [REQUIREMENT A0114.02] - Lesson 02.02 Periodic processing loop.
//    - DEFECT The initialization used a Scenery that later was replaced
//      by a new instance. This missed the setup for the model at the scenery
//      and did generate exeptions on the processing loop.
//
//    Revision 1.2  2007-10-03 16:50:10  ldiego
//    - DEFECT There are more items in the menu than declared.
//    - DEFECT The direction-speed labels are too separated.
//    - DEFECT Radar ranges does not update.
//
//    Revision 1.1  2007-09-25 11:44:40  ldiego
//    - [A0059.02] - Lesson 01. Open an scenery.
//    - [A0014.01] - Map should have a border.
//    - DEFECT Action Log view does not open.
//

package es.ftgroup.gef.model;

// - IMPORT SECTION .........................................................................................
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * This class simply encapsulates the <code>PropertyChangeSupport</code> instance to allow the notification
 * mechanism when the model properties are changed by the user interface intervention or the game engine.<br>
 * This class also signals the requirement to be <code>Serializable</code> for all depending classes so the
 * model can be read or written to external storage.<br>
 * <br>
 * This class in the long term can be edited to be an extension of the <code>java.beans</code> original
 * support class and this may reduce the code to be maintained. This is kept now due to simplify debugging.
 */
public abstract class AbstractPropertyChanger implements IGEFModel, Serializable, Cloneable {
	private static final long								serialVersionUID	= -3810250540551433870L;
	private static Logger										logger						= Logger.getLogger("net.sourceforge.harpoon.model");
	/**
	 * List of listeners that will be notified when a property changes. The list contains the Controller
	 * elements (EditParts) that will be interested in received events when the model field values change to
	 * reflect that changes on the visual part of the model.
	 */
	private transient PropertyChangeSupport	propertyListeners	= new PropertyChangeSupport(this);

	// public Object getPropertyPage() {
	// return null;
	// }

	// - P R O P E R T Y C H A N G E S U P P O R T
	/**
	 * Wraps the methods of the PropertyChangeSupport listener element to be accessed by the notification
	 * mechanism.
	 * 
	 * @param listener
	 *          new listener to be added to the list of listeners that will receive notifications.
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		getListeners().addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		getListeners().removePropertyChangeListener(listener);
	}

	public void firePropertyChange(final String property, final Object oldValue, final Object newValue) {
		logger.info("[Property:" + property + " on uni:" + toString());
		final PropertyChangeSupport targets = getListeners();
		if (targets.hasListeners(property)) targets.firePropertyChange(property, oldValue, newValue);
	}

	public void fireStructureChange(final String property, final Object dataSet, final Object change) {
		final PropertyChangeSupport targets = getListeners();
		if (targets.hasListeners(property))
			targets.firePropertyChange(new PropertyChangeEvent(this, property, dataSet, change));
	}

	// - P R O T E C T E D - S E C T I O N
	protected PropertyChangeSupport getListeners() {
		if (null == propertyListeners) propertyListeners = new PropertyChangeSupport(this);
		return propertyListeners;
	}
}

// - UNUSED CODE ............................................................................................
