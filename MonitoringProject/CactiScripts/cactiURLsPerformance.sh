#!/bin/bash

######################################################################
#	BASH
#	Author:	Carlos Lozano (CL) / Luis de Diego (LDD)
#	Version: v1.0
#	Description:
#		Script para obtener los tiempos de respuesta de AWA para un
#		determinado tipo de segmento.
######################################################################

HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
DATADIR="${HOMEDIR}/tmp"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"
TMPDIR="/tmp"

# Parametros de entrada
# $1 SEGMENTO - indica si es residencial o multilinea o cualquier otro servicio
SEGMENT=$1

# Residencial Postpago
if [ ${SEGMENT} == "RPOST" ]
	then
		RESULT=`curl -d 'IDToken1=654463895&IDToken2=123456&gotoOnFail=fallo&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=EcareAuthService&arg=newsession' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_cacti.awk`
fi
# Multilinea empresa
if [ ${SEGMENT} == "MULEMP" ]
	then
		RESULT=`curl -d 'gotoOnFail=ko&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_cacti.awk`

#		RESULT=`curl -d 'gotoOnFail=ko&goto=https://areaprivada.orange.es/neos/init.neos&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_multilinea_cacti.awk`
#		RESULT=`curl -d 'gotoOnFail=ko&goto=https://areaprivada.orange.es/neos/init.neos&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_multilinea_cacti.awk`
fi

echo ${RESULT}
#echo "time:0.268 status:ok"

