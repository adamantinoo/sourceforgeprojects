#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 61 2012-01-04 14:02:41Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 15:02:41 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 61 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		04012012 (LDD) - The output format and the filtering process
#			needs more documentation. Also the dependant files used by
#			this process have to be documented.
#
#	DESCRIPTION:
#		Script to obtain from the WebLogic console the monitoring data
#			and after parsing and processing generates the output format
#			suitable for processing by Cacti. After obtaining the data
#			you can watch the graphic analitics inside Cacti.
#		The first step requires the use of an authentication cookie. In
#			case the cookie does not exists the script should create
#			a new one by performing the "login". The same should apply
#			if the system does not result any useful information, then
#			the script should create a new cookie.
#
#	PARAMETERS
#		$1 application code (NEOS, FEW, PPOW, PPOE)
#		$2 service name in the console (Ej. neos1p)
#		$3 DEBUG flag
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"

export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

export DATAPROCESSINGDIR="${CACTIDIR}/DataProcessing"

PROCESSNAME="WL.cactiscript.threadmonitor"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
#{
### GLOBAL VARIABLES
CONSOLENEOS="weblogp5"
CONSOLEPPOW="weblogp7"
CONSOLEFEW="weblogp7"
CONSOLEPPOE="PPOEWLConsole"

APPLICATIONNAME=$1
SERVICENAME=$2
DEBUG=$3

### FUNCTION DEFINITIONS
. ${LIBRARYDIR}/CactiWLConsoleLibrary.sh

### MAIN PROCESSING
	#echo " "
	#echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

### Detect the console to be used depending on the application name. In case of not found return thge error code
	DetectConsole
	if [ "${DEBUG}" == "DEBUG" ]
	then
		echo "DEBUG: APLICATIONNAME=${APPLICATIONNAME}"
		echo "DEBUG: SERVICENAME=${SERVICENAME}"
		echo "DEBUG: CONSOLENAME=${CONSOLENAME}"
		echo "DEBUG: SERVERPORT=${SERVERPORT}"
	fi
	
	# Detect the console cookie. If not exist then create it.
	DetectCookie

	# Get the console data for processing.
	DUMMY=`curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --url "http://${CONSOLENAME}:${SERVERPORT}/console/console.portal" --silent --output /dev/null`
	DUMMY=`curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --silent --url "http://${CONSOLENAME}:${SERVERPORT}/console/console.portal?_nfpb=true&_pageLabel=ServerMonitoringThreadsPage&ServerMonitoringQueuesPortlethandle=com.bea.console.handles.JMXHandle%28%22com.bea%3AName%3D${SERVICENAME}%2CType%3DServer%22%29" | sed 's/<\/td>/<\/td>\n/gi' > ${DATAPROCESSINGDIR}/threads_${SERVICENAME}.html`
	if [ "${DEBUG}" == "DEBUG" ]
	then
		echo "DEBUG:  curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --silent --url \"http://${CONSOLENAME}:${SERVERPORT}/console/console.portal?_nfpb=true&_pageLabel=ServerMonitoringThreadsPage&ServerMonitoringQueuesPortlethandle=com.bea.console.handles.JMXHandle%28%22com.bea%3AName%3D${SERVICENAME}%2CType%3DServer%22%29\" | sed 's/<\/td>/<\/td>\n/gi' > ${DATAPROCESSINGDIR}/threads_${SERVICENAME}.html"
		echo "DEBUG:  ${DUMMY}"
	fi
	# Check if the result is empty. If so then we have an error and have to drop the cookie and try again.
	if [ -z "cat ${DATAPROCESSINGDIR}/threads_${SERVICENAME}.html" ]
	then
		rm ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt
		echo "ERROR: Data invalid. Dropping the cookie."
		exit 1
	fi

	# Process the result and parse the final data for Cacti.		
	RESULT=`awk --file ${AWKDIR}/awkfilterWLNEOSConsoleAuthentification.awk ${DATAPROCESSINGDIR}/threads_${SERVICENAME}.html`

	echo ${RESULT}
#echo "activeThreads:90 completedRequests:18000000 executeThreadIdle:30 executeThreadTotal:400 hoggingThreads:0 pendingRequests:0 queueLength:-34000 standbyThreads:250 throughput:100.0 health:OK"
exit 0

