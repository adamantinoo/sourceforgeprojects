BEGIN { 
		accesoCorrectoAWA_multilinea_empresas = "ko"
		tiempoDescarga = ""
		
		ficheroLog = "cacti_acceso_AWA_multilinea_empresas_LDAP_multilinea.log"
}



{ 
	#printf( "%s \n", $0 )
	
	
	if (match($0, /Este es el resumen del estado de todas sus/))
	{
		accesoCorrectoAWA_multilinea_empresas = "ok"
	}
	if (match($0, /LocationTime:/))
	{
		split( $0, timeDownload, ":" )
		tiempoDescarga = timeDownload[2]
	}
}

END {
	if(accesoCorrectoAWA_multilinea_empresas == "ok")
	{
		printf( "%s \n", "Resultado:ok" " Time:" tiempoDescarga ) > ficheroLog
	}
	else
	{
		printf( "%s \n", "Resultado:ko" " Time:" tiempoDescarga ) > ficheroLog
	}
}