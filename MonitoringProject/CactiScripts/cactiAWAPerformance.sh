#!/bin/bash

######################################################################
#	BASH
#	Author:	Carlos Lozano (CL) / Luis de Diego (LDD)
#	Version: v1.0
#	Description:
#		Script para obtener los tiempos de respuesta de AWA para un
#		determinado tipo de segmento.
######################################################################

HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
DATADIR="${HOMEDIR}/tmp"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"
TMPDIR="/tmp"
PROCESSED="FALSE"

# Parametros de entrada
# $1 SEGMENTO - indica si es residencial o multilinea o cualquier otro servicio
SEGMENT=$1

# Residencial Postpago
if [ ${SEGMENT} == "RPOST" ]
	then
		RESULT=`curl -d 'IDToken1=654463895&IDToken2=123456&gotoOnFail=fallo&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=EcareAuthService&arg=newsession' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_cacti.awk`
	PROCESSED="TRUE"
	echo ${RESULT}
fi
# Multilinea empresa
if [ ${SEGMENT} == "MULEMP" ]
	then
		RESULT=`curl -d 'gotoOnFail=ko&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_cacti.awk`

#		RESULT=`curl -d 'gotoOnFail=ko&goto=https://areaprivada.orange.es/neos/init.neos&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_multilinea_cacti.awk`
#		RESULT=`curl -d 'gotoOnFail=ko&goto=https://areaprivada.orange.es/neos/init.neos&encoded=false&gx_charset=ISO-8859-1&service=eCareMultilineaAuthService&arg=newsession&IDToken1=B30588131&IDToken2=123456' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location" | awk -f ${AWKDIR}/procesar_autentificacion_AWA_multilinea_cacti.awk`
	PROCESSED="TRUE"
	echo ${RESULT}
fi

if [ ${PROCESSED} != "TRUE" ]
	then

### Added lines to control other pages
cadena=`date +%s`

case $1 in
	'URL1') URL="https://empleadosorange.orange.es/logs/www.orange.es/p.html?param=$cadena";;
	'URL2') URL="https://sdc.francetelecom.com/dcsbynxcr648kylesxcfk4vs7_6c8r/dcs.gif?&dcsdat=1313150702105&dcssip=areaprivada.orange.es&dcsuri=/neos/init.neos&dcsqry=%3Futm_source=homeclientes%26utm_medium=form%26utm_term=login%26utm_campaign=loginmovil&dcsref=http://areaclientes.orange.es/&WT.tz=2&WT.bh=14&WT.ul=es-ES&WT.cd=24&WT.sr=1440x900&WT.jo=No&WT.ti=NEOS%20Next%20Ecare%20Orange%20Spain&WT.js=Yes&WT.jv=1.7&WT.ct=unknown&WT.bs=1440x713&WT.fi=Yes&WT.fv=10.0&WT.tv=1.0.7&WT.es=areaprivada.orange.es/neos/init.neos&WT.vt_f_tlh=1313150668&WT.vt_sid=172.24.64.69-3627008672.30166268.1313147754398&WT.co_f=172.24.64.69-3627008672.30166268&wasc_segment=RPOST&wasc_page=Dashboard";;
	'URL3') URL="https://areaprivada.orange.es/neos/js/jquery-ui-1.7.2.custom.min.js";;
	'URL4') URL="https://areaprivada.orange.es/neos/css/css.common.css";;
	'URL5') URL="https://areaprivada.orange.es/neos/img/sed/fondoPillBorde190Ar.png";;
	'URL6') URL="https://areaclientes.orange.es/UpImages/3555/destacado_derecho__adsl_2e19c072e6607e2a45a3cb9f4.jpg";;
	'URL7') URL="https://areaclientes.orange.es/img/logo_orange.gif";;
	'URL8') URL="https://orange.es/js/newtojump.js";;
	'URL9') URL="http://clientes.orange.es/error/error.html";;
esac

curl --output /dev/null --url "$URL" --write-out "time:%{time_total}\n" --insecure --silent --max-time 60 | sed 's/,/./g' | sed 's/T/t/g'

#echo "time:0.23"
fi
#echo ${RESULT}
#echo "time:0.268 status:ok"

