#!/bin/bash

######################################################################
#	BASH
#	Author:	Carlos Lozano (CL) / Luis de Diego (LDD)
#	Version: v1.0
#	Description:
#		Script para obtener los tiempos de respuesta de AWA para un
#		determinado tipo de segmento.
######################################################################

HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/CactiScripts"
DATADIR="${HOMEDIR}/tmp"
LOGDIR="${HOMEDIR}/logs"

# Parametros de extrada
# $1 SEGMENTO - indica si es residencial o multilinea o cualquier otro servicio
SEGMENT=$1

if [ ${SEGMENT} == "RPOST" ]
	then
#	fecha=`date "+%d%m%Y"`"";
		urlFichero="${DATADIR}/temporal_acceso_AWA_residencial_prepago.log"
#	echo `date "+%d/%m/%Y %H:%M:%S"`" Fecha" > $urlFichero
#		curl -d 'IDToken1=654463895&IDToken2=123456&gotoOnFail=fallo&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=EcareAuthService&arg=newsession' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | egrep -i "(Location)" >> $urlFichero

#		RESULT=`awk -f procesar_autentificacion_AWA_cacti.awk temporal_acceso_AWA_residencial_prepago.log`

		RESULT=`curl -d 'IDToken1=654463895&IDToken2=123456&gotoOnFail=fallo&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=EcareAuthService&arg=newsession' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total}\n" --insecure --silent | grep -i "Location"`
#		RESULT=`curl -d 'IDToken1=654463895&IDToken2=123456&gotoOnFail=fallo&goto=ok&encoded=false&gx_charset=ISO-8859-1&service=EcareAuthService&arg=newsession' -ik --url "https://sso.orange.es/amserver/gateway" --write-out "LocationTime:%{time_total} - " --insecure --silent`
#done
fi

echo ${RESULT}
#echo "activeThreads:90 completedRequests:18000000 executeThreadIdle:30 executeThreadTotal:400 hoggingThreads:0 pendingRequests:0 queueLength:-34000 standbyThreads:250 throughput:100.0 health:OK"

