#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		04012012 (LDD) - The output format and the filtering process
#			needs more documentation. Also the dependant files used by
#			this process have to be documented.
#
#	DESCRIPTION:
#		Script to obtain from the WebLogic console the monitoring data
#			and after parsing and processing generates the output format
#			suitable for processing by Cacti. After obtaining the data
#			you can watch the graphic analitics inside Cacti.
#		The first step requires the use of an authentication cookie. In
#			case the cookie does not exists the script should create
#			a new one by performing the "login". The same should apply
#			if the system does not result any useful information, then
#			the script should create a new cookie.
#
#	PARAMETERS
#		$1 application code (NEOS, FEW, PPOW, PPOE)
#		$2 Data Source name (Ej. fewspservices1p)
#		$3 Service name of the service using it (Ej. neos1p)
#		$4 DEBUG flag
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"

export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

export DATAPROCESSINGDIR="${CACTIDIR}/DataProcessing"

PROCESSNAME="WL.cactiscript.datasourcemonitoring"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
#{
### GLOBAL VARIABLES
CONSOLENEOS="weblogp5"
CONSOLEPPOW="weblogp7"
CONSOLEFEW="weblogp7"
CONSOLEPPOE="PPOEWLConsole"

APPLICATIONNAME=$1
DATASOURCE=$2
SERVICENAME=$3
DEBUG=$4

### FUNCTION DEFINITIONS
. ${LIBRARYDIR}/CactiWLConsoleLibrary.sh

### MAIN PROCESSING
	#echo " "
	#echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

### Detect the console to be used depending on the application name. In case of not found return thge error code
	DetectConsole
	if [ "${DEBUG}" == "DEBUG" ]
	then
		echo "DEBUG: APLICATIONNAME=${APPLICATIONNAME}"
		echo "DEBUG: DATASOURCE=${DATASOURCE}"
		echo "DEBUG: SERVICENAME=${SERVICENAME}"
		echo "DEBUG: CONSOLENAME=${CONSOLENAME}"
		echo "DEBUG: SERVERPORT=${SERVERPORT}"
	fi
	
	# Detect the console cookie. If not exist then create it.
	DetectCookie

	# Get the console data for processing.
	DUMMY=`curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --url "http://${CONSOLENAME}:${SERVERPORT}/console/console.portal" --silent --output /dev/null`
	DUMMY=`curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --silent --url "http://${CONSOLENAME}:${SERVERPORT}/console/console.portal?_nfpb=true&_pageLabel=JdbcDatasourcesJDBCDataSourceMonitorPage&JdbcDatasourcesJDBCDataSourceMonitorStatisticsPortlethandle=com.bea.console.handles.JMXHandle%28%22com.bea%3AName%3D${DATASOURCE}%2CType%3Dweblogic.j2ee.descriptor.wl.JDBCDataSourceBean%2CParent%3D%5Binternet%5D%2FJDBCSystemResources%5B${DATASOURCE}%5D%2CPath%3DJDBCResource%5B${DATASOURCE}%5D%22%29" | sed "s/'//gi" | sed 's/<\/TR><TR bgcolor=#FFFFFF/\n\<START/gi' | sed 's/<\/TR><TR bgcolor=#F6F6F6/\n\<START/gi' | grep "<START>" | sed 's/<\/td>/<\/td>/gi' | sed "s/<\/TR><\/TABLE><\/td>/\nEND/gi" | grep ${SERVICENAME} | sed 's/<START>//gi' | sed 's/<TD id=/\n/gi' | sed 's/ align=left height=1 >/ /gi' | sed 's/<\/td>//gi' > ${DATAPROCESSINGDIR}/DataSource_${DATASOURCE}_${SERVICENAME}.html`
	
#	DUMMY2=`curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --silent --url "http://${CONSOLENAME}:${SERVERPORT}/console/console.portal?_nfpb=true&_pageLabel=JdbcDatasourcesJDBCDataSourceMonitorPage&JdbcDatasourcesJDBCDataSourceMonitorStatisticsPortlethandle=com.bea.console.handles.JMXHandle%28%22com.bea%3AName%3D${DATASOURCE}%2CType%3Dweblogic.j2ee.descriptor.wl.JDBCDataSourceBean%2CParent%3D%5Binternet%5D%2FJDBCSystemResources%5B${DATASOURCE}%5D%2CPath%3DJDBCResource%5B${DATASOURCE}%5D%22%29" | sed "s/'//gi" | sed 's/<\/TR><TR bgcolor=#FFFFFF/\n\<START/gi' | sed 's/<\/TR><TR bgcolor=#F6F6F6/\n\<START/gi' | grep "<START>" | sed 's/<\/td>/<\/td>/gi' | sed "s/<\/TR><\/TABLE><\/td>/\nEND/gi" | grep ${SERVICENAME} | sed 's/<START>//gi' | sed 's/<TD id=/\n/gi' | sed 's/ align=left height=1 >/ /gi' | sed 's/<\/td>//gi' > ${DATADIR}/DataSource_${DATASOURCE}${SERVICENAME}.html`
	
	
	if [ "${DEBUG}" == "DEBUG" ]
	then
		echo "DEBUG:  curl -b ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt --silent --url \"http://${CONSOLENAME}:${SERVERPORT}/console/console.portal?_nfpb=true&_pageLabel=JdbcDatasourcesJDBCDataSourceMonitorPage&JdbcDatasourcesJDBCDataSourceMonitorStatisticsPortlethandle=com.bea.console.handles.JMXHandle%28%22com.bea%3AName%3D${DATASOURCE}%2CType%3Dweblogic.j2ee.descriptor.wl.JDBCDataSourceBean%2CParent%3D%5Binternet%5D%2FJDBCSystemResources%5B${DATASOURCE}%5D%2CPath%3DJDBCResource%5B${DATASOURCE}%5D%22%29\" | sed 's/<\/td>/<\/td>\n/gi' | sed -n '/genericTableFormtable/,/WaitingForConnectionTotal2/p' | sed '1d' | sed '30d' | sed \"s/<TD id='\(.*\)' align='left' height='1' >\(.*\)<\/td>/\1:\2/\" > ${DATAPROCESSINGDIR}/DataSource_${DATASOURCE}_${SERVICENAME}.html"
		echo "DEBUG:  ${DUMMY}"
	fi
	# Check if the result is empty. If so then we have an error and have to drop the cookie and try again.
	if [ -z "cat ${DATAPROCESSINGDIR}/DataSource_${DATASOURCE}${SERVICENAME}.html" ]
	then
#		rm ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt
		echo "ERROR: Data invalid. Dropping the cookie."
		exit 1
	fi

	# Process the result and parse the final data for Cacti.		
	RESULT=`awk --file ${AWKDIR}/awkcactiDataSourceFilter.awk ${DATAPROCESSINGDIR}/DataSource_${DATASOURCE}_${SERVICENAME}.html`

	echo ${RESULT}
#echo "ActiveConnectionsAverageCount:0 ActiveConnectionsCurrentCount:0 ActiveConnectionsHighCount:0 ConnectionsTotalCount:0 WaitingForConnectionCurrentCount:0 WaitingForConnectionHighCount:0 Enabled:0"
exit 0

