##########################################################################################
#	AWK
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: UpdateNEOSRRD.sh 80 2012-01-23 17:21:09Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 18:21:09 +0100 (lun, 23 ene 2012) $
#	REVISION:		$Revision: 80 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#
#	DESCRIPTION:
#	Filter the output for a netstat and aggregate all the results by the network
#		state that is defined on field $6.
#
#	INPUT FORMAT:
#tcp        0      0 0.0.0.0:443             0.0.0.0:*               LISTEN     
#tcp        0      0 10.113.73.177:8090      10.113.73.177:50342     TIME_WAIT  
#
#	OUTPUT FORMAT:
#LISTEN:15 CLOSE_WAIT:2 ESTABLISHED:11 TIME_WAIT:50
##########################################################################################
BEGIN {
	OFS="\t"   # output field separator is a tab
}
{
	PortCounter[$6]++
}

END {
	for( record in PortCounter) {
		split( record, data, SUBSEP )
		printf( "%s:%1.0i ", data[1], PortCounter[data[1]] )
	}
}

