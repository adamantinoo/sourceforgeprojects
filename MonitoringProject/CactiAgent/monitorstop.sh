#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: UpdateSiebelRRD.sh 80 2012-01-23 17:21:09Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 18:21:09 +0100 (lun, 23 ene 2012) $
#	REVISION:		$Revision: 80 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#
#	DESCRIPTION:
#	Stops the motitoring processes whose PID process number are stored on some files.
#	Does not perform any check.
#	Finally deletes the PID files.
#
#	USAGE:
#		./monitorstop.sh
#
#	PARAMETERS:
######################################################################

echo "... Stopping monitor processes."
PID=`cat vmstatmonitor.pid`
echo -n "    stop vmstat (${PID})... "
echo "`kill -9 ${PID}`"
rm vmstatmonitor.pid

PID=`cat tailmonitor.pid`
echo -n "    stop awk (${PID})... "
echo "`kill -9 ${PID}`"

let "PID=$PID-1"
echo -n "    stop tail (${PID})... "
echo "`kill -9 ${PID}`"
rm tailmonitor.pid

echo "... Complete."

