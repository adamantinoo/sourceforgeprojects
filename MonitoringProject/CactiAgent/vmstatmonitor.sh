#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: UpdateSiebelRRD.sh 80 2012-01-23 17:21:09Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 18:21:09 +0100 (lun, 23 ene 2012) $
#	REVISION:		$Revision: 80 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#
#	DESCRIPTION:
#	Starts vmsta data collection into a new vmstat file and accumulates
#		the data with the date to a historic file.
#
#	USAGE:
#		./vmstatmonitor.sh
#
#	PARAMETERS:
######################################################################

echo "... Starting vmstat data collection."
vmstat -n 60 > vmstat.data &
echo $! > vmstatmonitor.pid
echo "... Starting historic collection."
tail -f vmstat.data | awk '{ print strftime("%Y-%m-%d %H:%M", systime()) $0; fflush(); }' >> vmstat.historic.data &
echo $! > tailmonitor.pid

