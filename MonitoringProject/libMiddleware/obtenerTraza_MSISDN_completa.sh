#!/bin/bash
# LANZARLO:   ./obtenerTraza_MSISDN_completa.sh 634179247 SI
if [ $2 = "SI" -o $2 = "WL5" -o  $2 = "WL6" ]; then

	if [ $2 = "SI" -o $2 = "WL5" ]; then
		ssh weblogp5 -l swfcfe "cat libAccMiddleware.log*" > todosLibAccMiddleware-WL5.log
	fi
	
	if [ $2 = "SI" -o $2 = "WL6" ]; then
		ssh weblogp6 -l swfcfe "cat libAccMiddleware.log*" > todosLibAccMiddleware-WL6.log
	fi
fi

if [ -f "trazasXML.log" ]; then
	rm trazasXML.log
fi

sed -f separador_operaciones_MDW.sed todosLibAcc* | awk -v MSISDN=$1 -f libreriaMDW_peticiones_completo.awk | grep "AMENA.MOVILES.PROD.RESPUESTA.libAccMiddleware." | sed -e 's/.*AMENA\.MOVILES\.PROD\.RESPUESTA\.libAccMiddleware\......\.weblogp.\.\(..........\........\)/$0 ~ "\1"{\n\tprintf( \"%s\\n\", $0 ) >> nombreFicheroTrazas\n}\n/g' > tmpPeticiones.awk
sed '1s/^/BEGIN {\n\tRS="~"\n\tnombreFicheroTrazas = \"trazasXML.log\"\n}\n\n/' tmpPeticiones.awk > tmpPeticiones2.awk
rm tmpPeticiones.awk
sed -f separador_operaciones_MDW.sed todosLibAcc* | awk -v MSISDN=$1 -f tmpPeticiones2.awk
rm tmpPeticiones2.awk

