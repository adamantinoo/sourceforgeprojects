#!/bin/bash

###
#	Get the logs of PPOW service and process them to get the daily accumulated for each of the middleware operations.
###

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="ProcessPPOWOperations.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
### GLOBAL VARIABLES
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
FILTERDATE=`date +%Y-%m-%d`
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

### FUNCTION DEFINITIONS
### LocatePPOWLogFile( logfileName, targetMachine )
function LocatePPOWLogFile {
DATASOURCE="/logs/ppo"
cd ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}
if [ ! -e ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}/$1 ]
	then
	echo "    Executing:  scp -B swfcfe@$2:${DATASOURCE}/$1 ."
	scp -B swfcfe@$2:${DATASOURCE}/$1 .
fi
}

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
FILTERDATE="${DAYOFMONTH}/${MONTHNUMBER}/${YEAR}"
echo "    Filter Date:   ${FILTERDATE}"

echo "... Verify or copy to source area the logs."
LocatePPOWLogFile "ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz" ${TARGET7}
LocatePPOWLogFile "ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz" ${TARGET8}
LocatePPOWLogFile "ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz" ${TARGET7}
LocatePPOWLogFile "ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz" ${TARGET8}

echo "... Process log and extract aggregated data."
HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"
WORKDIR=${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}
cd ${WORKDIR}

echo "... MDW Operations aggregated by minute for all servers unified."
echo "Executing:  gzip -dc ${WORKDIR}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsMinute.awk | grep -v ${FILTERDATE} | sort > ${HISTORICDATADIR}/PPOWMDWOperations/${YEAR}${MONTHNUMBER}/ppowmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
gzip -dc ${WORKDIR}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsMinute.awk | grep -v ${FILTERDATE} | sort > ${HISTORICDATADIR}/PPOWMDWOperations/${YEAR}${MONTHNUMBER}/ppowmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt


echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

