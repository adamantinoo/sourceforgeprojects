#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		DDMMYYYY (INITIALS) - Message.
#
#	DESCRIPTION:
#		The script checks if we have already downloaded the logs necesary for
#			processing (the same check code that the "DownloadNEOSLogs.sh" script)
#			and then generates the daily historical data for Cacti RRD reprocessing.
#		There is a final parameter used when called from the CRON job scheduler
#			to adjust the processing date to al least one day before because the
#			current day logs are still incomplete.
#
#	USAGE:
#		bash /home/swfcfe/MonitoringProject/ProcessingScripts/ProcessNEOSMonitoringData.sh `date +\%Y` `date +\%m` `date +\%d` CRON
#		bash /home/swfcfe/MonitoringProject/ProcessingScripts/ProcessNEOSMonitoringData.sh 2012 01 23 NOCRON
#
#	PARAMETERS:
#		$1	YEAR
#		$2	MONTH
#		$3	DAY
#		$4	CRON - to substract a day from date or use the direct day
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="ProcessNEOSMonitoringData.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"

### Input Parameter processing.
YEAR=$1
MONTHNUMBER=$2
DAYOFMONTH=$3
if [ -z "${YEAR}" ]
then
	YEAR=`date +%Y`
fi
CRONFLAG=$4

DATASOURCESERVICES="/logs/neos/services"
DATASOURCEWEB="/logs/neos/web"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"

### FUNCTION DEFINITIONS
source "${LIBRARYDIR}/DownloadersLibrary.sh"
### DownloadServiceLogFile( DestinationDirectory, SourceLogName, DestinationLogname )
### DownloadWebLogFile( DestinationDirectory, SourceLogName, DestinationLogname )
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [YEAR]$1 [MONTHNUMBER]$2 [DAYOFMONTH]$3 [CRONFLAG]$4"
if [ "${CRONFLAG}" == "CRON" ]
then
	### Get the date for yerterday.
	YEAR=`date --date="yesterday" +%Y`
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Year:  ${YEAR}"
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
FILTERDATE="${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}"
echo "    Filter Date:   ${FILTERDATE}"

echo "... Download NEOS services logs."
DownloadServiceLogFile ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER} neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz
DownloadServiceLogFile ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER} neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz

echo "... Process log and extract aggregated data."
HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"

echo "... MDW Operations aggregated for a DAY. Single and Multiserver grouped"
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/neosmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt &
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt &
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt

echo "... MDW Operations aggregated for every minute."
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
sleep 10
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &

echo "... NEOS Operations. Generating processing data."
echo "... Executing:  GZIPS.WL5"
echo "... Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep \" # \"  > /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep " # "  > /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt

grep -v "# GetProfile #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetProfile" > /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetOffer #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetOffer" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetBalance #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetBalance" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetLoyaltyBalance #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetLoyaltyBalance" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetProducts #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetProducts" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetDownloadsList #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetDownloadsList" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetUsage #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetUsage" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# SetOrder #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# SetOrder" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetInfoByLine #" /tmp/${TARGET5}filterNEOSOperationsTrimmed.txt | grep "# GetInfoByLine" >> /tmp/${TARGET5}filterNEOSOperations.txt

echo "... Executing:  GZIPS.WL6"
echo "... Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep \" # \"  > /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep " # "  > /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt

grep -v "# GetProfile #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetProfile" > /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetOffer #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetOffer" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetBalance #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetBalance" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetLoyaltyBalance #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetLoyaltyBalance" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetProducts #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetProducts" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetDownloadsList #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetDownloadsList" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetUsage #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetUsage" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# SetOrder #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# SetOrder" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetInfoByLine #" /tmp/${TARGET6}filterNEOSOperationsTrimmed.txt | grep "# GetInfoByLine" >> /tmp/${TARGET6}filterNEOSOperations.txt

echo "... NEOS Operations aggregated for a DAY. Single and Multiserver grouped"
echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &
awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &

echo "    Executing:  cat ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt | sort | awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt"
cat /tmp/${TARGET5}_filterNEOSOperationsday.txt /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort | awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &


echo "... NEOS Operations aggregated for every minute."
echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &

echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &


echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

