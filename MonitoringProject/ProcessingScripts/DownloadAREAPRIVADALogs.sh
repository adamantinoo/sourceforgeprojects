#!/bin/bash

###
# Download all logs front end Apaches for domain "areaprivada".
#	PARAMETERS:
#		$1	MONTH
#		$2	DAY
#		$3	CRON - to substract a day from date or use the direct day
###
HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"
PROCESSNAME="downloadlogs.areaprivada.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
TARGET3="AOTLXPRWEB00003"
TARGET4="AOTLXPRWEB00004"
DATASOURCELOGS="/web/2.2-worker/areaprivada/logs"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
echo "... Copy data files to local storage."
echo "... Copy Apache logs."
cd ${DATADEST}/areaprivada/${YEAR}${MONTHNUMBER}
if [ ! -e ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 ]
	then
	echo "    Executing:  scp -B swfcfe@${TARGET3}:${DATASOURCELOGS}/areaprivada-ssl.log.1.bz2 ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2"
	scp -B swfcfe@${TARGET3}:${DATASOURCELOGS}/areaprivada-ssl.log.1.bz2 ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2
fi
if [ ! -e ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 ]
	then
	echo "    Executing:  scp -B swfcfe@${TARGET4}:${DATASOURCELOGS}/areaprivada-ssl.log.1.bz2 ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2"
	scp -B swfcfe@${TARGET4}:${DATASOURCELOGS}/areaprivada-ssl.log.1.bz2 ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

