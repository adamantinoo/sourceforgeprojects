#!/bin/bash

###
# Process the Apache logs fro "areaprivada" and generate counter for some of the WL Application entry points.
###

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

{
### GLOBAL VARIABLES
PROCESSNAME="ProcessAccess.areaprivada.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData/LogData/areaprivada"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### FUNCTION DEFINITIONS


### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"

echo "... Process log and extract aggregated data."
HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"

echo "... Generate page counters for Access URL per server"
echo "    Executing:  bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/neos\/" | grep -v "\/neos\/js" | grep -v "\/neos\/css" | grep -v "\/neos\/img" | grep -v "\/neos\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap3_neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/neos\/" | grep -v "\/neos\/js" | grep -v "\/neos\/css" | grep -v "\/neos\/img" | grep -v "\/neos\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap3_neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/fewsp\/" | grep -v "\/fewsp\/js" | grep -v "\/fewsp\/css" | grep -v "\/fewsp\/img" | grep -v "\/fewsp\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap3_fewsp_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap3.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/ecareppo\/" | grep -v "\/ecareppo\/resources" | grep -v "\/ecareppo\/js" | grep -v "\/ecareppo\/css" | grep -v "\/ecareppo\/img" | grep -v "\/ecareppo\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap3_ecareppo_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &

bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/neos\/" | grep -v "\/neos\/js" | grep -v "\/neos\/css" | grep -v "\/neos\/img" | grep -v "\/neos\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap4_neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/fewsp\/" | grep -v "\/fewsp\/js" | grep -v "\/fewsp\/css" | grep -v "\/fewsp\/img" | grep -v "\/fewsp\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap4_fewsp_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/ap4.areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/ecareppo\/" | grep -v "\/ecareppo\/resources" | grep -v "\/ecareppo\/js" | grep -v "\/ecareppo\/css" | grep -v "\/ecareppo\/img" | grep -v "\/ecareppo\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_ap4_ecareppo_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &

echo "... Generate page counters for Access URL unified"
echo "    Executing:  bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/*areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep \"\/neos\/\" | grep -v \"\/neos\/js\" | grep -v \"\/neos\/css\" | grep -v \"\/neos\/img\" | grep -v \"\/neos\/images\" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"

bzcat ${SOURCEDATADIR}/${YEAR}${MONTHNUMBER}/*areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "\/neos\/" | grep -v "\/neos\/js" | grep -v "\/neos\/css" | grep -v "\/neos\/img" | grep -v "\/neos\/images" | awk --file ${AWKDIR}/awkfilterAREAPRIVADAPagesMinute.awk | sort > ${HISTORICDATADIR}/PageAccess/${YEAR}${MONTHNUMBER}/areaprivada_neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &


echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

