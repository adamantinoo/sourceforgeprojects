#!/bin/bash

###
# Get the logs of neos service and process them to get the daily accumulated for each of the middleware operations.
###

HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"
PROCESSNAME="mdwoperations.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
### GLOBAL VARIABLES
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCESERVICES="/logs/neos/services"
DATASOURCEWEB="/logs/neos/web"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
FILTERDATE="${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}"
echo "    Filter Date:   ${FILTERDATE}"

echo "... Copy data files to local storage."
echo "... Copy NEOS services logs."
cd ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}
if [ ! -e weblogp5.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET5}:${DATASOURCESERVICES}/neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET5}:${DATASOURCESERVICES}/neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
### Remove unused lines and compress the file again
	gzip -dc neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep " # " | gzip -c > weblogp5.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz
#	rename -v 's/neosservices/weblogp5.services/' neosservices.log*;
fi
if [ ! -e weblogp6.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET6}:${DATASOURCESERVICES}/neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET6}:${DATASOURCESERVICES}/neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
	rename -v 's/neosservices/weblogp6.services/' neosservices.log*;
fi
echo "... Process log and extract aggregated data."
HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"
#DESTINATIONDAY="${HISTORICDATADIR}/MDWOperations/neosmdw${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt"
#DESTINATIONTO="${HISTORICDATADIR}/MDWOperations/neosmdw${YEAR}${MONTHNUMBER}${DAYOFMONTH}timeout.txt"

#if [ TRUE == FALSE ]
#then
echo "... MDW Operations aggregated for a DAY. Single and Multiserver grouped"
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/neosmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt &
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt &
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt

echo "... MDW Operations aggregated for every minute."
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
sleep 10
echo "    Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/MDWOperations/${YEAR}${MONTHNUMBER}/neosmdw_${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &
#fi

echo "... NEOS Operations. Generating processing data."
echo "... Executing:  GZIPS.WL5"
echo "... Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep \" # \"  > /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep " # "  > /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt

grep -v "# GetProfile #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetProfile" > /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetOffer #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetOffer" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetBalance #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetBalance" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetLoyaltyBalance #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetLoyaltyBalance" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetProducts #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetProducts" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetDownloadsList #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetDownloadsList" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetUsage #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# GetUsage" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# SetOrder #" /tmp/${TARGET5}_filterNEOSOperationsTrimmed.txt | grep "# SetOrder" >> /tmp/${TARGET5}_filterNEOSOperationsday.txt
grep -v "# GetInfoByLine #" /tmp/${TARGET5}filterNEOSOperationsTrimmed.txt | grep "# GetInfoByLine" >> /tmp/${TARGET5}filterNEOSOperations.txt

echo "... Executing:  GZIPS.WL6"
echo "... Executing:  gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep \" # \"  > /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt"
gzip -dc ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER}/${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep " # "  > /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt

grep -v "# GetProfile #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetProfile" > /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetOffer #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetOffer" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetBalance #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetBalance" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetLoyaltyBalance #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetLoyaltyBalance" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetProducts #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetProducts" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetDownloadsList #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetDownloadsList" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetUsage #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# GetUsage" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# SetOrder #" /tmp/${TARGET6}_filterNEOSOperationsTrimmed.txt | grep "# SetOrder" >> /tmp/${TARGET6}_filterNEOSOperationsday.txt
grep -v "# GetInfoByLine #" /tmp/${TARGET6}filterNEOSOperationsTrimmed.txt | grep "# GetInfoByLine" >> /tmp/${TARGET6}filterNEOSOperations.txt

echo "... NEOS Operations aggregated for a DAY. Single and Multiserver grouped"
echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &
awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &

echo "    Executing:  cat ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt | sort | awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt"
cat /tmp/${TARGET5}_filterNEOSOperationsday.txt /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort | awk --file ${AWKDIR}/awkfilterNEOSOperationsDay.awk filterType=DATE filterValue=${FILTERDATE} | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos_${YEAR}${MONTHNUMBER}${DAYOFMONTH}day.txt &


echo "... NEOS Operations aggregated for every minute."
echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET5}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET5}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &

echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt"
awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=DATE filterValue=${FILTERDATE} /tmp/${TARGET6}_filterNEOSOperationsday.txt | sort > ${HISTORICDATADIR}/NEOSOperations/${YEAR}${MONTHNUMBER}/neos${TARGET6}_${YEAR}${MONTHNUMBER}${DAYOFMONTH}minute.txt &


echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

