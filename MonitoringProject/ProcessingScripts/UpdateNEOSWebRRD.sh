#!/bin/bash

#####
#	Script to process source data and generate the UPDATE commands that
#		insert the data values inside the RRD databases. This script controls
#		all operations for the different Data Input Methods and using the same
#		sourcedata if possible.
#
#	PARAMETERS
#		$1 server to process. Name of the WL server that is the source of the data.
#		$2 COPY flag to download new data. Skips the copy of new server data and uses the current available file if not defined.
#####

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

GRAPHDIR="${HOMEDIR}/MonitoringProject/CactiGraphs"

### GLOBAL VARIABLES
PROCESSNAME="UpdateNEOSWebRRD.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
TARGET=$1
FILTERDATE=`date +%Y-%m-%d`
FILTERMINUTE=`date +%M`
FILTERHOUR=`date +%H:%M`
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

{
### FUNCTION DEFINITIONS
### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERMINUTE=${FILTERMINUTE}"
echo "    FILTERHOUR=${FILTERHOUR}"
### Get current file sizes to only process new lines. Check if current data exists.
if [ -e ${SOURCEDATADIR}/${TARGET}/neoswebsite.lastsize.txt ]
	then
	TARGETSIZE=`cat ${SOURCEDATADIR}/${TARGET}/neoswebsite.lastsize.txt`
	else
	TARGETSIZE=0
fi
echo "    TARGETSIZE=${TARGETSIZE}"

### Get new size to limit processing. This is a remote command.
NEWTARGETSIZE=`ssh ${TARGET} -l swfcfe 'wc -l /logs/neos/web/neoswebsite.log' | awk '{ print $1 }'`
#if [ "$2" == "COPY" ]
#	then
	echo ${NEWTARGETSIZE} > ${SOURCEDATADIR}/${TARGET}/neoswebsite.lastsize.txt
#fi
echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"

### If new size is less that target then file has srink due to a date change and rotation
if [ ${TARGETSIZE} -ge ${NEWTARGETSIZE} ]
	then
	TARGETSIZE=0
	echo "    TARGETSIZE=${TARGETSIZE}"
fi
let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
### Add more lines to avoid processing gaps
if [ "${TARGETSIZE}" -ne 0 ]
	then
	let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
fi
echo "    LINES2PROCESS=${LINES2PROCESS}"

### Get log data to start processing
#if [ "$2" == "COPY" ]
#	then
	echo "... Copying source partial data from origin target."
	echo "    Executing:  ssh ${TARGET} -l swfcfe "tail -${LINES2PROCESS} /logs/neos/web/neoswebsite.log" > ${SOURCEDATADIR}/${TARGET}/neoswebsite.log"
	ssh ${TARGET} -l swfcfe "tail -${LINES2PROCESS} /logs/neos/web/neoswebsite.log" > ${SOURCEDATADIR}/${TARGET}/neoswebsite.log
#fi

echo "... Processing data."
echo "    Executing: GREPS"
grep " \/init.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log > /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/init-mobile.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/info.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/dashboard.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/dashboard.neos - usage" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/dashboard.neos - points" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/dashboard.neos - refresh" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/mymobile.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/callslist.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/callslist.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/usage.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/usage.neos - usage" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/usage.neos - balance" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
#grep " \/usage.neos - maverickBalance" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/services.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/personaldata.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/topup.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/info.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt
grep " \/priceconfiguration.neos" ${SOURCEDATADIR}/${TARGET}/neoswebsite.log >> /tmp/${TARGET}filterNEOSWebPages.txt

echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSWebPagesMinute.awk /tmp/${TARGET}filterNEOSWebPages.txt | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSWebPages.txt"
awk --file ${AWKDIR}/awkfilterNEOSWebPagesMinute.awk /tmp/${TARGET}filterNEOSWebPages.txt | grep -v " - " | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSWebPages.txt

echo "    Executing:  grep -v \"${FILTERDATE} ${FILTERHOUR}\" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSWebPages.txt | grep -v \"${FILTERDATE} 23:59\" | grep -v \" - \" | awk --file ${AWKDIR}/awkfilterNEOSWebPagesRRDUpdate.${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSWebPages.sh"
grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSWebPages.txt | grep -v "${FILTERDATE} 23:59" | grep -v " - " | awk --file ${AWKDIR}/awkfilterNEOSWebPagesRRDUpdate.${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSWebPages.sh

### Trim last lines to avoid inserting incomplete data
#head -n -20 ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSWebPagesfull.sh > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSWebPages.sh

### Execute the updates inside the RRD database.
if [ "$2" == "UPDATE" ]
	then
	echo "... Execute RRD updates. NEOS Web pages"
	bash ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSWebPages.sh 
fi
echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

