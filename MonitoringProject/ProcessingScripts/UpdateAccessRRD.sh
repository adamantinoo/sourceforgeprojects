#!/bin/bash

#####
#	Script to process source data and generate the UPDATE commands that
#		insert the data values inside the RRD databases. This script controls
#		all operations for the different Data Input Methods and using the same
#		sourcedata if possible.
#	The process expect incremental data for a single day or file. Gets from the
#		server just the data to process las minute and new minutes.
#
#	PARAMETERS
#		$1 server to process. Name of the WL server that is the source of the data.
#		$2 service. While the serves sets the working area, the service sets the processing paths
#		$3 UPDATE if we need to update the RRD database
#####

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="UpdateAccessRRD.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
TARGET=$1
SERVICE=$2
FILTERDATE=`date +%Y-%m-%d`
FILTERHOUR=`date +%H:%M`
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### FUNCTION DEFINITIONS

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERHOUR=${FILTERHOUR}"
### If target is TOTAL perform another processing.
if [ ${TARGET} == "AOTLXPRWEBTOTAL" ]
	then
	echo "    AOTLXPRWEBTOTAL=${TARGET}"
fi
if [ ${SERVICE} == "areaprivada" ]
	then
	### Get current file sizes to only process new lines. Check if current data exists.
	if [ -e ${SOURCEDATADIR}/${TARGET}/areaprivada.lastsize.txt ]
		then
		TARGETSIZE=`cat ${SOURCEDATADIR}/${TARGET}/areaprivada.lastsize.txt`
		else
		let TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"

	### Get new size to limit processing. This is a remote command.
	NEWTARGETSIZE=`ssh ${TARGET} -l swfcfe 'wc -l /web/2.2-worker/areaprivada/logs/areaprivada-ssl.log' | awk '{ print $1 }'`
	echo ${NEWTARGETSIZE} > ${SOURCEDATADIR}/${TARGET}/areaprivada.lastsize.txt
	echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"

	### If new size is less that target then file has srink duw to a date change and rotation
	if [ ${TARGETSIZE} -ge ${NEWTARGETSIZE} ]
		then
		TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"
	let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
	### Add more lines to avoid processing gaps
	if [ "${TARGETSIZE}" -ne 0 ]
		then
		let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
	fi
	echo "    LINES2PROCESS=${LINES2PROCESS}"

	### Get log data to start processing
	echo "... Copying source partial data from origin target."
	TARGETFILTERDATE=`date +%d/%b/%Y`
	echo "    Executing:  ssh ${TARGET} -l swfcfe \"tail -${LINES2PROCESS} /web/2.2-worker/areaprivada/logs/areaprivada-ssl.log | grep ${TARGETFILTERDATE} \" > ${SOURCEDATADIR}/${TARGET}/areaprivada-ssl.log"
	ssh ${TARGET} -l swfcfe "tail -${LINES2PROCESS} /web/2.2-worker/areaprivada/logs/areaprivada-ssl.log" > ${SOURCEDATADIR}/${TARGET}/areaprivada-ssl.log
#	ssh ${TARGET} -l swfcfe "tail -${LINES2PROCESS} /web/2.2-worker/areaprivada/logs/areaprivada-ssl.log | awk --file /home/swfcfe/awkfilterAREAPRIVADAAccessMinute.awk | sort " > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteAREAPRIVADAAccess.txt

	echo "... Processing data. AREAPRIVADA Access"
	awk --file ${AWKDIR}/awkfilterAREAPRIVADAAccessMinute.awk ${SOURCEDATADIR}/${TARGET}/areaprivada-ssl.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteAREAPRIVADAAccess.txt

	echo "    Executing:  grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteAREAPRIVADAAccess.txt | awk -v SERVER=${TARGET} --file ${AWKDIR}/awkfilterAREAPRIVADAAccessRRDUpdate.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDAREAPRIVADAAccess.sh"
	grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteAREAPRIVADAAccess.txt | awk -v SERVER=${TARGET} --file ${AWKDIR}/awkfilterAREAPRIVADAAccessRRDUpdate.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDAREAPRIVADAAccess.sh

	### Execute the updates inside the RRD database.
	if [ "$3" == "UPDATE" ]
		then
		echo "... Execute RRD updates. AREAPRIVADA Access"
		bash ${SOURCEDATADIR}/${TARGET}/updateRRDAREAPRIVADAAccess.sh
	fi
fi
if [ ${SERVICE} == "neos" ]
	then
	### The server name is different from the TARGET. We have to add another parameter
	SERVERNAME=$3
	### Get current file sizes to only process new lines. Check if current data exists.
	if [ -e ${SOURCEDATADIR}/${TARGET}/neos.lastsize.txt ]
		then
		TARGETSIZE=`cat ${SOURCEDATADIR}/${TARGET}/neos.lastsize.txt`
		else
		let TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"

	### Get new size to limit processing. This is a remote command.
	NEWTARGETSIZE=`ssh ${SERVERNAME} -l swfcfe "wc -l /weblogic10/user_projects/internet/servers/${TARGET}/servers/${TARGET}/logs/access.log" | awk '{ print $1 }'`
	echo ${NEWTARGETSIZE} > ${SOURCEDATADIR}/${TARGET}/neos.lastsize.txt
	echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"

	### If new size is less that target then file has srink duw to a date change and rotation
	if [ ${TARGETSIZE} -ge ${NEWTARGETSIZE} ]
		then
		TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"
	let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
	### Add more lines to avoid processing gaps
	if [ "${TARGETSIZE}" -ne 0 ]
		then
		let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
	fi
	echo "    LINES2PROCESS=${LINES2PROCESS}"

	### Get log data to start processing
	echo "... Copying source partial data from origin target."
	echo "    Executing:  ssh ${SERVERNAME} -l swfcfe \"tail -${LINES2PROCESS} /weblogic10/user_projects/internet/servers/${TARGET}/servers/${TARGET}/logs/access.log\" > ${SOURCEDATADIR}/${TARGET}/access.log"
	ssh ${SERVERNAME} -l swfcfe "tail -${LINES2PROCESS} /weblogic10/user_projects/internet/servers/${TARGET}/servers/${TARGET}/logs/access.log" > ${SOURCEDATADIR}/${TARGET}/access.log

	echo "... Processing data. NEOS Access"
	awk --file ${AWKDIR}/awkfilterAREAPRIVADAAccessMinute.awk ${SOURCEDATADIR}/${TARGET}/access.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinute${SERVICE}Access.txt

	echo "    Executing:  grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteAREAPRIVADAAccess.txt | awk -v SERVER=${TARGET} --file ${AWKDIR}/awkfilterAREAPRIVADAAccessRRDUpdate.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDAREAPRIVADAAccess.sh"
	grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinute${SERVICE}Access.txt | awk -v SERVER=${TARGET} --file ${AWKDIR}/awkfilterNEOSAccessRRDUpdate.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSAccess.sh

	### Execute the updates inside the RRD database.
	if [ "$2" == "UPDATE" ]
		then
		echo "... Execute RRD updates. AREAPRIVADA Access"
		bash ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSAccess.sh
	fi
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

