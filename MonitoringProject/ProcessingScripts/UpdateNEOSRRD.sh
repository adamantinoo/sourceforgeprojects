#!/bin/bash

#####
#	Script to process source data and generate the UPDATE commands that
#		insert the data values inside the RRD databases. This script controls
#		all operations for the different Data Input Methods and using the same
#		sourcedata if possible.
#	The process expect incremental data for a single day or file. Gets from the
#		server just the data to process las minute and new minutes.
#
#	PARAMETERS
#		$1 server to process. Name of the WL server that is the source of the data.
#		$2 UPDATE if we need to update the RRD database
#####

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="UpdateNEOSRRD.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
TARGET=$1
FILTERDATE=`date +%Y-%m-%d`
FILTERMINUTE=`date +%M`
FILTERHOUR=`date +%H:%M`
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### FUNCTION DEFINITIONS
function FilterNEOSOperations {
	echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} /tmp/${TARGET}filterNEOSOperations.txt | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt"
	awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} /tmp/${TARGET}filterNEOSOperations.txt | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt
	echo "    Executing:  grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt | awk --file ${AWKDIR}/awkfilterNEOSOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSOperations.sh"
	grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt | awk --file ${AWKDIR}/awkfilterNEOSOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSOperations.sh
}
function FilterNEOSMDWOperations {
	echo "    Executing:  awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} ${SOURCEDATADIR}/${TARGET}/neosservices.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSMDWOperations.txt"
	awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} ${SOURCEDATADIR}/${TARGET}/neosservices.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSMDWOperations.txt
	echo "    Executing:  grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSMDWOperations.txt | awk --file ${AWKDIR}/awkfilterMDWOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSMDWOperations.sh"
	grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSMDWOperations.txt | awk --file ${AWKDIR}/awkfilterMDWOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSMDWOperations.sh
}

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERMINUTE=${FILTERMINUTE}"
echo "    FILTERHOUR=${FILTERHOUR}"
### Get current file sizes to only process new lines. Check if current data exists.
if [ -e ${SOURCEDATADIR}/${TARGET}/neosservices.lastsize.txt ]
	then
	TARGETSIZE=`cat ${SOURCEDATADIR}/${TARGET}/neosservices.lastsize.txt`
	else
	let TARGETSIZE=0
fi
echo "    TARGETSIZE=${TARGETSIZE}"

### Get new size to limit processing. This is a remote command.
NEWTARGETSIZE=`ssh ${TARGET} -l swfcfe 'wc -l /logs/neos/services/neosservices.log' | awk '{ print $1 }'`
echo ${NEWTARGETSIZE} > ${SOURCEDATADIR}/${TARGET}/neosservices.lastsize.txt
echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"

### If new size is less that target then file has srink duw to a date change and rotation
if [ ${TARGETSIZE} -ge ${NEWTARGETSIZE} ]
	then
	TARGETSIZE=0
fi
echo "    TARGETSIZE=${TARGETSIZE}"
let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
### Add more lines to avoid processing gaps
if [ "${TARGETSIZE}" -ne 0 ]
	then
	let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
fi
echo "    LINES2PROCESS=${LINES2PROCESS}"

### Get log data to start processing
echo "... Copying source partial data from origin target."
echo "    Executing:  ssh ${TARGET} -l swfcfe \"tail -${LINES2PROCESS} /logs/neos/services/neosservices.log | grep \" # \" \" > ${SOURCEDATADIR}/${TARGET}/neosservices.log"
ssh ${TARGET} -l swfcfe "tail -${LINES2PROCESS} /logs/neos/services/neosservices.log | grep \" # \" " > ${SOURCEDATADIR}/${TARGET}/neosservices.log

echo "... Processing data. NEOS Operations"
echo "    Executing:  GREPS"
grep " # " ${SOURCEDATADIR}/${TARGET}/neosservices.log > /tmp/${TARGET}filterNEOSOperationsTrimmed.txt
grep -v "# GetProfile #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetProfile" > /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetOffer #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetOffer" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetBalance #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetBalance" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetLoyaltyBalance #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetLoyaltyBalance" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetProducts #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetProducts" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetDownloadsList #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetDownloadsList" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetUsage #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetUsage" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# SetOrder #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# SetOrder" >> /tmp/${TARGET}filterNEOSOperations.txt
grep -v "# GetInfoByLine #" /tmp/${TARGET}filterNEOSOperationsTrimmed.txt | grep "# GetInfoByLine" >> /tmp/${TARGET}filterNEOSOperations.txt

FilterNEOSOperations

### Execute the updates inside the RRD database.
if [ "$2" == "UPDATE" ]
	then
	echo "... Execute RRD updates. NEOS Operations"
	bash ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSOperations.sh 
fi
echo "... Processing data. NEOS Middeware Operations"
FilterNEOSMDWOperations

### Execute the updates inside the RRD database.
if [ "$2" == "UPDATE" ]
	then
	echo "... Execute RRD updates. NEOS Middeware Operations"
	bash ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSMDWOperations.sh
fi
echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

