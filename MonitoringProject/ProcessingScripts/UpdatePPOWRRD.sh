#!/bin/bash

#####
#	Script to process source data and generate the UPDATE commands that
#		insert the data values inside the RRD databases. This script controls
#		all operations for the different Data Input Methods and using the same
#		sourcedata if possible.
#	The process expects incremental data for a single day or file. Gets from the
#		server just the data to process from the last minute processed up to
#		current time.
#	This script version processes PPOW log data to extract some MDW operations
#		calls and performance if available.
#	On this version we start using a common source code library to store common
#		script code for use in other scripts.
#
#	PARAMETERS
#		$1 server to process. Name of the WL server that is the source of the data. Not used because
#				we process all servers and aggregate the data into a single record file.
#		$2 UPDATE if we allow to update the RRD database. Any other value does not update the database
#				but make all other processing. Also does not modify the stored last record processed.
#####

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="UpdatePPOWRRD.ppow.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
TARGET=$1
UPDATESWITCH=$2
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### FUNCTION DEFINITIONS
### GetRemoteData( $1 $2 TARGET TARGETLOGNAME LASTSIZENAME )
function GetRemoteData {
	GetCurrentRemoteSize $5
	RESULT=`ssh $3 -l swfcfe "wc -l /logs/ppo/$4" | awk '{ print $1 }'`
	NEWTARGETSIZE=$RESULT
	echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"
	# Get new size to limit processing. This is a remote command.
	if [ "$2" == "UPDATE" ]
		then
		echo ${NEWTARGETSIZE} > ${SOURCEDATADIR}/ppow_ecare/$5
	fi
	CalculateRemoteSize $5 $4 $3
	echo "... Copying source partial data from origin target."
	echo "    Executing:  ssh $3 -l swfcfe \"tail -${LINES2PROCESS} /logs/ppo/$4\" > ${SOURCEDATADIR}/${TARGET}/$4"
	ssh $3 -l swfcfe "tail -${LINES2PROCESS} /logs/ppo/$4" > ${SOURCEDATADIR}/ppow_ecare/$4
}
### GetCurrentRemoteSize( lastSizeFileName )
function GetCurrentRemoteSize {
	# Check if exists the file to store the current remote size
	echo "... Checking $1"
	if [ -e ${SOURCEDATADIR}/ppow_ecare/$1 ]
		then
		TARGETSIZE=`cat ${SOURCEDATADIR}/ppow_ecare/$1`
		else
		let TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"
	RESULT=$TARGETSIZE
}
### CalculateRemoteSize()
function CalculateRemoteSize {
	### If new size is less that target then file has shrink due to a date change and rotation
	if [ ${TARGETSIZE} -ge ${NEWTARGETSIZE} ]
		then
		TARGETSIZE=0
		echo "    TARGETSIZE=${TARGETSIZE}"
	fi
	let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
	### Add more lines to avoid processing gaps
	if [ "${TARGETSIZE}" -ne 0 ]
		then
		let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
	fi
	echo "    LINES2PROCESS=${LINES2PROCESS}"
	RESULT=$LINES2PROCESS
}

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... PARAMETERS: $1 $2"
FILTERDATE=`date +%Y-%m-%d`
FILTERHOUR=`date +%H:%M`
echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERHOUR=${FILTERHOUR}"

### Get filtered log data to start processing. Filtering is performed on target by use of aliases.
TARGET="weblogp7"
GetRemoteData $1 $2 ${TARGET} "ecare_ppo1p.log" "ppow_ecare_ppo1p.lastsize.txt"
GetRemoteData $1 $2 ${TARGET} "ecare_ppo3p.log" "ppow_ecare_ppo3p.lastsize.txt"

TARGET="weblogp8"
GetRemoteData $1 $2 ${TARGET} "ecare_ppo2p.log" "ppow_ecare_ppo2p.lastsize.txt"
GetRemoteData $1 $2 ${TARGET} "ecare_ppo4p.log" "ppow_ecare_ppo4p.lastsize.txt"

####################################################################################################
echo "... Unify logs and process the data. PPOW MDW Operations"
cd ${SOURCEDATADIR}/ppow_ecare

echo "... Process PPOW MDW Operations"
echo "    Executing:  cat ecare*log | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsMinute.awk | sort > ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWOperations.txt"
cat ecare*log | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsMinute.awk | sort > ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWOperations.txt
grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWOperations.txt | awk --file ${AWKDIR}/awkfilterPPOWMDWRRDUpdate.awk > ${SOURCEDATADIR}/ppow_ecare/updateRRDPPOWMDWOperations.sh

echo "... Process PPOW Error abstract"
echo "    Executing:  cat ecare*log | grep \"\[ERROR\]\" | sed 's/\[//gi' | sed 's/\]/ /gi' | awk --file ${AWKDIR}/awkfilterPPOWErrorsMinute.awk ${SOURCEDATADIR}/ppow_ecare/ecareAggregated.log | sort > ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWErrors.txt"
cat ecare*log | grep "\[ERROR\]" | sed 's/\[//gi' | sed 's/\]/ /gi' | awk --file ${AWKDIR}/awkfilterPPOWErrorsMinute.awk | sort > ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWErrors.txt
grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/ppow_ecare/filterbyMinutePPOWErrors.txt | awk --file ${AWKDIR}/awkfilterPPOWErrorsRRDUpdate.awk > ${SOURCEDATADIR}/ppow_ecare/updateRRDPPOWErrors.sh

####################################################################################################
### Execute the updates inside the RRD database.
if [ "$2" == "UPDATE" ]
	then
	echo "... Execute RRD updates. PPOW MDW Operations"
	bash ${SOURCEDATADIR}/ppow_ecare/updateRRDPPOWMDWOperations.sh
	bash ${SOURCEDATADIR}/ppow_ecare/updateRRDPPOWErrors.sh
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

