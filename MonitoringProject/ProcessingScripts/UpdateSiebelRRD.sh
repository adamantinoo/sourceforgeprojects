#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		20100109 (LDD) - Review the filtering of current date and time data to remove
#			this kind of filtering from the AWK script and left it outside in a GREP.
#
#	DESCRIPTION:
#	This kind of scripts detect and download the partial data that was not processed
#		on the last operation and after downloading the log lines launch a processing
#		function to generate, on a first pass the detail data line (usually DATE TIME COUNTERS)
#		than then is converted to the special update RRDTOOL call to load the data inside
#		the Cacti RRD databases.
#
#	This script processes MDW Siebel source data and extracts operation counts and
#		average timings for each minute and each node. Used for node monitoring.
#
#	PARAMETERS:
#		$1 node to process
#		$2 UPDATE switch if we need to update the RRD database and the file progress counters
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="UpdateSiebelRRD.siebel.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
TARGET=$1
UPDATE=$2

### LIBRARY LOADING
#source "${LIBRARYDIR}/SiebelLibrary.sh"
###
source "${LIBRARYDIR}/DownloadersLibrary.sh"
### FilterPPOOperations
### DownloadServiceLogFile( DestinationDirectory, SourceLogName, DestinationLogname )
### DownloadWebLogFile( DestinationDirectory, SourceLogName, DestinationLogname, RenamePattern )
######################################################################

### FUNCTION DEFINITIONS
### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
function GetRemoteData {
### Check if exists the file to store the current remote size
	echo "... Checking $3"
	if [ -e $4 ]
	then
		TARGETSIZE=`cat $4`
	else
		let TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"
	RESULT=`ssh $3 -l swfcfe "wc -l $5" | awk '{ print $1 }'`
	NEWTARGETSIZE=$RESULT
	echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"
### Get new size to limit processing. This is a remote command.
	if [ "${UPDATE}" == "UPDATE" ]
	then
		echo ${NEWTARGETSIZE} > $4
	fi
### If new size is less that target then file has shrink due to a date change and rotation
	if [ -z "$TARGETSIZE" ]
	then
		TARGETSIZE=0
		echo "    TARGETSIZE=${TARGETSIZE}"
	fi
	if [ "${TARGETSIZE}" -ge "${NEWTARGETSIZE}" ]
	then
		TARGETSIZE=0
		echo "    TARGETSIZE=${TARGETSIZE}"
	fi
	let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
### Add more lines to avoid processing gaps
	if [ "${TARGETSIZE}" -ne 0 ]
	then
		let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
	fi
	echo "    LINES2PROCESS=${LINES2PROCESS}"

### Get log data to start processing
	echo "... Copying source partial data from origin target."
	echo "    Executing:  ssh $3 -l swfcfe \"tail -${LINES2PROCESS} $5 $7\" > $6"
	ssh $3 -l swfcfe "tail -${LINES2PROCESS} $5 $7" > $6
}
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [TARGET]$1 [UPDATE]$2"

FILTERDATE=`date +%Y-%m-%d`
FILTERHOUR=`date +%H:%M`
echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERHOUR=${FILTERHOUR}"
WORKDIR=${SOURCEDATADIR}/Siebel
cd ${WORKDIR}

SIEBELSRV03=sieomp8
### Get filtered log data to start processing. Filtering is performed on target by use of aliases.
#GetRemoteData $1 $1 ${SIEBELSRV03} "${SOURCEDATADIR}/Siebel/siebelMDW${SIEBELSRV03}.lastsize.txt" "/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_1.log" "${SOURCEDATADIR}/Siebel/SBLLoyPesado-srv03_1.log" ""
#GetRemoteData $1 $1 ${SIEBELSRV03} "${SOURCEDATADIR}/Siebel/siebelMDW${SIEBELSRV03}.lastsize.txt" "/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_2.log" "${SOURCEDATADIR}/Siebel/SBLLoyPesado-srv03_2.log" ""
scp -B swfcfe@${SIEBELSRV03}:/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_1.log .
scp -B swfcfe@${SIEBELSRV03}:/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_2.log .
scp -B swfcfe@${SIEBELSRV03}:/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_1.log.[1-2] .
scp -B swfcfe@${SIEBELSRV03}:/tibbw3/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_sieomp8_2.log.[1-2] .

echo "... Processing data. Siebel MDW Operations"
echo "    Executing:  awk --file ${AWKDIR}/awkfilterSiebelMDWOperationsMinute.awk ${SOURCEDATADIR}/Siebel/SBLLoyPesado-Process_Archive_sieomp8_* | sort > ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV03.txt"
awk --file ${AWKDIR}/awkfilterSiebelMDWOperationsMinute.awk ${SOURCEDATADIR}/Siebel/SBLLoyPesado-Process_Archive_sieomp8_* | sort > ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV03.txt
grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV03.txt | awk -v SERVER=${SIEBELSRV03} --file ${AWKDIR}/awkfilterSiebelMDWRRDUpdates.awk > ${SOURCEDATADIR}/Siebel/updateRRDSiebelMDW_SIEBELSRV03.sh

SIEBELSRV04=siegtwp4
### Get filtered log data to start processing. Filtering is performed on target by use of aliases.
#GetRemoteData $1 $1 ${SIEBELSRV04} "${SOURCEDATADIR}/Siebel/siebelMDW${SIEBELSRV04}.lastsize.txt" "/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_1.log" "${SOURCEDATADIR}/Siebel/SBLLoyPesado-srv04_1.log" ""
#GetRemoteData $1 $1 ${SIEBELSRV04} "${SOURCEDATADIR}/Siebel/siebelMDW${SIEBELSRV04}.lastsize.txt" "/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_2.log" "${SOURCEDATADIR}/Siebel/SBLLoyPesado-srv04_2.log" ""
scp -B swfcfe@${SIEBELSRV04}:/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_1.log .
scp -B swfcfe@${SIEBELSRV04}:/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_2.log .
scp -B swfcfe@${SIEBELSRV04}:/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_1.log.[1-2] .
scp -B swfcfe@${SIEBELSRV04}:/tibbw4/log/bw/SBLLoyPesado/SBLLoyPesado-Process_Archive_siegtwp4_2.log.[1-2] .

echo "... Processing data. Siebel MDW Operations"
echo "    Executing:  awk --file ${AWKDIR}/awkfilterSiebelMDWOperationsMinute.awk ${SOURCEDATADIR}/SBLLoyPesado-Process_Archive_siegtwp4_* | sort > ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV04.txt"
awk --file ${AWKDIR}/awkfilterSiebelMDWOperationsMinute.awk ${SOURCEDATADIR}/Siebel/SBLLoyPesado-Process_Archive_siegtwp4_* | sort > ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV04.txt
grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/Siebel/filterbyMinuteSiebelMDWOperations_SIEBELSRV04.txt | awk -v SERVER=${SIEBELSRV04} --file ${AWKDIR}/awkfilterSiebelMDWRRDUpdates.awk > ${SOURCEDATADIR}/Siebel/updateRRDSiebelMDW_SIEBELSRV04.sh

### Execute the updates inside the RRD database.
if [ "${UPDATE}" == "UPDATE" ]
then
	echo "... Execute RRD updates. Siebel MDW Operations"
	bash ${SOURCEDATADIR}/Siebel/updateRRDSiebelMDW_SIEBELSRV03.sh
	bash ${SOURCEDATADIR}/Siebel/updateRRDSiebelMDW_SIEBELSRV04.sh
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

