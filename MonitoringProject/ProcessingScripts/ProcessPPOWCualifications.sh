#!/bin/bash

###
# Get the logs of the PPOW statistics and process them to obtain the next information:
#	- The detail of MSISDN and Contract, segment and campaing for a "Listado de terminales"
#		the is understood to be a qualification.
#	- The detail of MSISDN that do not qualify a campaing.
#	- The aggregated data from qualifications for DATE HOUR SEGMENT CONTRACT CAMPAING
#	- The aggregated data from qualifications for DATE SEGMENT CONTRACT CAMPAING
#	- The aggregated data from qualifications for DATE CAMPAING
#	- The aggregated data from no qualifications for DATE HOUR
###

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"
PROCESSNAME="PPOWQualifications.ppow.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/ppo"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

### FUNCTION DEFINITIONS

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"

if [ "$3" = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="2 days ago" +%m`
	DAYOFMONTH=`date --date="2 days ago" +%d`
	echo "    Being called with CRON flag." ;
fi

echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
FILTERDATE="${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}"
echo "    Filter Date:   ${FILTERDATE}"

echo "... Copy data files to local storage."
echo "... Copy PPOW stadistics logs."
cd ${DATADEST}/ppowstats/${YEAR}${MONTHNUMBER}
if [ ! -e estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
cd ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}
if [ ! -e ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi

echo "... Process log and extract aggregated data"
HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"
WORKINGDIR=${HISTORICDATADIR}/PPOWQualifications/${YEAR}${MONTHNUMBER}
cd ${WORKINGDIR}

DESTINATIONDETAILEDQUAL=${WORKINGDIR}/ppowqualification_${YEAR}${MONTHNUMBER}${DAYOFMONTH}detail.txt
DESTINATIONDETAILEDNOQUAL=${WORKINGDIR}/ppownoqualification_${YEAR}${MONTHNUMBER}${DAYOFMONTH}detail.txt
DESTINATIONHOURQUAL=${WORKINGDIR}/ppowqualification_${YEAR}${MONTHNUMBER}${DAYOFMONTH}hour.txt
DESTINATIONHOURNOQUAL=${WORKINGDIR}/ppownoqualification_${YEAR}${MONTHNUMBER}${DAYOFMONTH}hour.txt

echo "... Process source data to generate detailed files."
echo "    Executing:  gzip -dc ${DATADEST}/ppowstats/${YEAR}${MONTHNUMBER}/estadisticas*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${HOMEDIR}/awkfilterPPOWQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDQUAL}"
gzip -dc ${DATADEST}/ppowstats/${YEAR}${MONTHNUMBER}/estadisticas*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterPPOWQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDQUAL}
echo "    Executing:  gzip -dc ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}/ecare*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterPPOWNotQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDNOQUAL}"
gzip -dc ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}/ecare*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | awk --file ${AWKDIR}/awkfilterPPOWNotQualifiedDetailed.awk | sort > ${DESTINATIONDETAILEDNOQUAL}

echo "... Process detailed file and generate aggregation files."
echo "    Executing:  awk --file=${AWKDIR}/awkfilterPPOWQualificationAggregationHour.awk ${DESTINATIONDETAILEDQUAL} | sort > ${DESTINATIONHOURQUAL}"
awk --file=${AWKDIR}/awkfilterPPOWQualificationAggregationHour.awk ${DESTINATIONDETAILEDQUAL} | sort > ${DESTINATIONHOURQUAL}
echo "    Executing:  awk --file=${AWKDIR}/awkfilterPPOWNoQualificationAggregationHour.awk ${DESTINATIONDETAILEDNOQUAL} | sort > ${DESTINATIONHOURNOQUAL}"
awk --file=${AWKDIR}/awkfilterPPOWNoQualificationAggregationHour.awk ${DESTINATIONDETAILEDNOQUAL} | sort > ${DESTINATIONHOURNOQUAL}

echo "... Process PPOW MDW Operations filters. Only 02045 operation"
echo "... PPOW MDW Operations aggregated for a DAY. Single and Multiserver grouped"
echo "    Executing:  gzip -dc ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep \"consulta de socios- operacion 02045\" | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/PPOWMDWOperations/${YEAR}${MONTHNUMBER}/ppowmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt"
gzip -dc ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}/*.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz | grep "consulta de socios- operacion 02045" | awk --file ${AWKDIR}/awkfilterPPOWMDWOperationsDay.awk | sort > ${HISTORICDATADIR}/PPOWMDWOperations/${YEAR}${MONTHNUMBER}/ppowmdw_${YEAR}${MONTHNUMBER}${DAYOFMONTH}date.txt

echo "... Compress detailed information files"
gzip -9 ${DESTINATIONDETAILEDQUAL}
gzip -9 ${DESTINATIONDETAILEDNOQUAL}

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

