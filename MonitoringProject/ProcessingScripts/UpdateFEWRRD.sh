#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: UpdateFEWRRD.sh 78 2012-01-23 17:06:33Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 18:06:33 +0100 (lun, 23 ene 2012) $
#	REVISION:		$Revision: 78 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		20100109 (LDD) - Review the filtering of current date and time data to remove
#			this kind of filtering from the AWK script and left it outside in a GREP.
#
#	DESCRIPTION:
#	Script to process source data and generate the UPDATE commands that
#		insert the data values inside the RRD databases. This script controls
#		all operations for the different Data Input Methods and using the same
#		sourcedata if possible.
#	The process expects incremental data for a single day or file. Gets from the
#		server just the data to process last minute and new minutes.
#	This version is specific for FEW services log files but the source code is
#		quite similar to the code for any other MDW source processing and the
#		code is factirized and converted to functions where possible.
#
#	USAGE:
#		bash /home/swfcfe/MonitoringProject/ProcessingScripts/UpdateFEWRRD.sh undefined UPDATE
#
#	PARAMETERS:
#		$1 server to process. Name of the WL server that is the source of the data. Not used because
#				we process all servers and aggregate the data into a single record file.
#		$2 UPDATESWITCH if we allow to update the RRD database. Any other value does not update the database
#				but make all other processing. Also does not modify the stored last record processed.
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="UpdateFEWRRD.few.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
TARGET=$1
UPDATESWITCH=$2

### FUNCTION LOADING
source "${LIBRARYDIR}/CommonLibrary.sh"
### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
# Gets the end of the file from the last point processed that is a line number
#	stored in a local control file.
# PARAMETERS:
#	FILLERP1	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	FILLERP2	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	TARGETSERVER	The network name of the target servar that contains the logs.
#	SIZECONTROL		The name of the local file used to control the already processed lines.
#	SERVERLOGPATH	The path of the server log to download.
#	LOCALLOGPATH	The path of the local server log
#	TAILFILTER		The piece of script passed and oncatenated with the downloading "tail" to improve some
#		preprocessed uses.
######################################################################

### FUNCTION DEFINITIONS
### FilterFEWMDWOperations( SOURCELOG OPERATIONSMINUTE FILTER AWKRRDGENERATOR
function FilterFEWMDWOperations {
	echo "    Executing:  cat $1 | awk --file ${AWKDIR}/awkfilterMDWOperationsMinutev2.awk | sort > $2_new"
	cat $1 | awk --file ${AWKDIR}/awkfilterMDWOperationsMinutev2.awk | sort > "$2_new"
	cp "$2_new" $2
	echo "    Executing:  grep -v \"$3\" $2 | awk --assign APPLICATION=\"few\" --assign SERVER=\"multiserver\" --file ${AWKDIR}/$4 | sort > ${SOURCEDATADIR}/fewservices/updateRRDFEWMDWOperations.sh"
	grep -v "$3" $2 | awk --assign APPLICATION="few" --assign SERVER="multiserver" --file ${AWKDIR}/$4 | sort > "${SOURCEDATADIR}/fewservices/updateRRDFEWMDWOperations.sh"
}
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [TARGET]$1 [UPDATESWITCH]$2"
FILTERDATE=`date +%Y-%m-%d`
FILTERMINUTE=`date +%M`
FILTERHOUR=`date +%H:%M`
echo "    FILTERDATE=${FILTERDATE}"
echo "    FILTERMINUTE=${FILTERMINUTE}"
echo "    FILTERHOUR=${FILTERHOUR}"

WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### Get filtered log data to start processing. Filtering is performed on target by use of aliases.
FEWSRV="weblogp7"
GetRemoteData $1 $1 ${FEWSRV} "${SOURCEDATADIR}/fewservices/fewservices_${FEWSRV}.lastsize.txt" "/logs/fewspservices/fewspservices.log" "${SOURCEDATADIR}/fewservices/fewspservices_${FEWSRV}.log" " | grep ' # ' "

FEWSRV="weblogp8"
GetRemoteData $1 $1 ${FEWSRV} "${SOURCEDATADIR}/fewservices/fewservices_${FEWSRV}.lastsize.txt" "/logs/fewspservices/fewspservices.log" "${SOURCEDATADIR}/fewservices/fewspservices_${FEWSRV}.log" " | grep ' # ' "

echo "... Processing data. FEW MDW Operations"
FilterFEWMDWOperations "${SOURCEDATADIR}/fewservices/fewspservices*.log" "${SOURCEDATADIR}/fewservices/filterbyMinutefewMDWOperations.txt" "${FILTERDATE} ${FILTERHOUR}" "awkfilterFEWMDWRRDUpdate.awk"

### Execute the updates inside the RRD database.
if [ "${UPDATESWITCH}" == "UPDATE" ]
then
	echo "... Execute RRD updates. FEW MDW Operations"
	bash ${SOURCEDATADIR}/fewservices/updateRRDFEWMDWOperations.sh
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

