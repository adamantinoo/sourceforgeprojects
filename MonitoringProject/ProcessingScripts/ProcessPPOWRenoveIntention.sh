#!/bin/bash

#####
#	Script to get the daily renove intentions from Apache logs.
#
#	PARAMETERS
#		$1 month.
#		$2 day.
#		$3 CRON to substract a date to the current date for processing of previous date data.
#####

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"
export HISTORICDATADIR="${HOMEDIR}/MonitoringProject/HistoricData"

MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`
if [ "$3" = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="1 days ago" +%m`
	DAYOFMONTH=`date --date="1 days ago" +%d`
	echo "    Being called with CRON flag." ;
fi

echo "    Executing: perl ${SCRIPTDIR}/logresolvemerge.pl ${SOURCEDATADIR}/LogData/areaprivada/${YEAR}${MONTHNUMBER}/*areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep \"comando\" | awk --file ${AWKDIR}/awkfilterAREAPRIVADARenoveIntentionDay.awk | sort >> ${HISTORICDATADIR}/PPOWRenoves/RenoveIntention${YEAR}${MONTHNUMBER}.txt"
bzcat ${SOURCEDATADIR}/LogData/areaprivada/${YEAR}${MONTHNUMBER}/*areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "comando" | awk --file ${AWKDIR}/awkfilterAREAPRIVADARenoveIntentionDay.awk | sort >> ${HISTORICDATADIR}/PPOWRenoves/RenoveIntention${YEAR}${MONTHNUMBER}.txt
#perl ${SCRIPTDIR}/logresolvemerge.pl ${SOURCEDATADIR}/LogData/areaprivada/${YEAR}${MONTHNUMBER}/*areaprivada-ssl.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.bz2 | grep "comando" | awk --file ${AWKDIR}/awkfilterAREAPRIVADARenoveIntentionDay.awk | sort >> ${HISTORICDATADIR}/PPOWRenoves/RenoveIntention${YEAR}${MONTHNUMBER}.txt


