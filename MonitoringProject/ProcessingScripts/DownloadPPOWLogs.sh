#!/bin/bash

###
# Download all logs from source machines for later processing.
#	PARAMETERS:
#		$1	MONTH
#		$2	DAY
#		$3	CRON - to substract a day from date or use the direct day
###
HOMEDIR="/home/swfcfe"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"
PROCESSNAME="downloadlogs.ppow.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"
{
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCE="/logs/ppo"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=`date +%Y`

echo " "
echo "... START OF PROCESS. `date +%F` `date +%H:%M:%S`"
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="2 days ago" +%m`
	DAYOFMONTH=`date --date="2 days ago" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
echo "... Copy data files to local storage."
echo "... Copy PPOW stadistics logs."
cd ${DATADEST}/ppowstats/${YEAR}${MONTHNUMBER}
if [ ! -e estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo1.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/estadisticas_ppo3.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo2.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/estadisticas_ppo4.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi

echo "... Copy PPOW web logs."
if [ $3 = "CRON" ]
	then
	### Get the date for yerterday.
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"
cd ${DATADEST}/ppowservice/${YEAR}${MONTHNUMBER}
if [ ! -e ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo1p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET7}:${DATASOURCE}/ecare_ppo3p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo2p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi
if [ ! -e ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ]
	then
	echo "    Executing: scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ."
	scp -B swfcfe@${TARGET8}:${DATASOURCE}/ecare_ppo4p.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz .
fi

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

