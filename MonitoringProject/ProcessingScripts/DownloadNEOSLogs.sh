#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: DownloadNEOSLogs.sh 65 2012-01-09 13:59:23Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-09 14:59:23 +0100 (lun, 09 ene 2012) $
#	REVISION:		$Revision: 65 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		DDMMYYYY (INITIALS) - Message.
#
#	DESCRIPTION:
#		Download all logs from source machines for later processing.
#
#	PARAMETERS:
#		$1	MONTH
#		$2	DAY
#		$3	YEAR
#		$4	CRON - to substract a day from date or use the direct day
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="DownloadNEOSLogs.neos.cronprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Servers identifiers or network names.
TARGET5="weblogp5"
TARGET6="weblogp6"
TARGET7="weblogp7"
TARGET8="weblogp8"
DATASOURCESERVICES="/logs/neos/services"
DATASOURCEWEB="/logs/neos/web"
DATADEST="${HOMEDIR}/MonitoringProject/SourceData/LogData"

### Input Parameter processing.
MONTHNUMBER=$1
DAYOFMONTH=$2
YEAR=$3
if [ -z "${YEAR}" ]
then
	YEAR=`date +%Y`
fi
CRONFLAG=$4

### FUNCTION DEFINITIONS
source "${LIBRARYDIR}/DownloadersLibrary.sh"
### DownloadServiceLogFile( DestinationDirectory, SourceLogName, DestinationLogname )
### DownloadWebLogFile( DestinationDirectory, SourceLogName, DestinationLogname )
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [MONTHNUMBER]$1 [DAYOFMONTH]$2 [YEAR]$3 [CRONFLAG]$4"
if [ "${CRONFLAG}" == "CRON" ]
then
	### Get the date for yerterday.
	YEAR=`date --date="yesterday" +%Y`
	MONTHNUMBER=`date --date="yesterday" +%m`
	DAYOFMONTH=`date --date="yesterday" +%d`
	echo "    Being called with CRON flag." ;
fi
echo "    Year:  ${YEAR}"
echo "    Month: ${MONTHNUMBER}"
echo "    Day:   ${DAYOFMONTH}"

echo "... Download NEOS services logs."
DownloadServiceLogFile ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER} neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET5}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz
DownloadServiceLogFile ${DATADEST}/neosservices/${YEAR}${MONTHNUMBER} neosservices.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET6}.services.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz

echo "... Download NEOS web logs."
DownloadWebLogFile ${DATADEST}/neosweb/${YEAR}${MONTHNUMBER} neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET5}.website.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz 's/neoswebsite/weblogp5.website/'
DownloadWebLogFile ${DATADEST}/neosweb/${YEAR}${MONTHNUMBER} neoswebsite.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz ${TARGET6}.website.log.${YEAR}-${MONTHNUMBER}-${DAYOFMONTH}.gz 's/neoswebsite/weblogp6.website/'

echo "... Download NEOS nohup logs."
cd ${DATADEST}/neosnohup
echo "Executing:  scp -B swfcfe@${TARGET5}:${DATASOURCESERVICES}/nohupneos*.* ."
scp -B swfcfe@${TARGET5}:${DATASOURCESERVICES}/nohupneos*.* .
echo "Executing:  scp -B swfcfe@${TARGET6}:${DATASOURCESERVICES}/nohupneos*.* ."
scp -B swfcfe@${TARGET6}:${DATASOURCESERVICES}/nohupneos*.* .

echo "Executing:  scp -B swfcfe@${TARGET5}:${DATASOURCEWEB}/nohupneos*.* ."
scp -B swfcfe@${TARGET5}:${DATASOURCEWEB}/nohupneos*.* .
echo "Executing:  scp -B swfcfe@${TARGET6}:${DATASOURCEWEB}/nohupneos*.* ."
scp -B swfcfe@${TARGET6}:${DATASOURCEWEB}/nohupneos*.* .

#echo "... Download NEOS access logs. Service side."
#cd ${DATADEST}/neosservices.access
#echo "Executing:  scp -B swfcfe@${TARGET5}:/weblogic10/user_projects/internet/servers/neos1p/servers/neos1p/logs/access.log* ."
#scp -B swfcfe@${TARGET5}:/weblogic10/user_projects/internet/servers/neos1p/servers/neos1p/logs/access.log* .
#rename -v 's/access/neos1p.access/' access.log*
#gzip -9 neos1p.access*
#echo "Executing:  scp -B swfcfe@${TARGET6}:/weblogic10/user_projects/internet/servers/neos2p/servers/neos2p/logs/access.log* ."
#scp -B swfcfe@${TARGET6}:/weblogic10/user_projects/internet/servers/neos2p/servers/neos2p/logs/access.log* .
#rename -v 's/access/neos2p.access/' access.log*
#gzip -9 neos2p.access*

#echo "... Download NEOS access logs. Web side."
#cd ${DATADEST}/neosweb.access
#echo "Executing:  scp -B swfcfe@${TARGET5}:/weblogic10/user_projects/internet/servers/neoswe1p/servers/neoswe1p/logs/access.log* ."
#scp -B swfcfe@${TARGET5}:/weblogic10/user_projects/internet/servers/neoswe1p/servers/neoswe1p/logs/access.log* .
#rename -v 's/access/neos1p.access/' access.log*
#gzip -9 neos1p.access*
#echo "Executing:  scp -B swfcfe@${TARGET6}:/weblogic10/user_projects/internet/servers/neoswe2p/servers/neoswe2p/logs/access.log* ."
#scp -B swfcfe@${TARGET6}:/weblogic10/user_projects/internet/servers/neoswe2p/servers/neoswe2p/logs/access.log* .
#rename -v 's/access/neos2p.access/' access.log*
#gzip -9 neos2p.access*

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0

