#!/bin/bash

#####
#	PARAMETERS
#		$1 server to process
#####

### GLOBAL DEFINITIONS
HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
GRAPHDIR="${HOMEDIR}/MonitoringProject/CactiGraphs"
WORKDIR=${SOURCEDATADIR}
cd ${WORKDIR}

### GLOBAL VARIABLES
TARGET=$1
#TARGET6="weblogp6"

### Get current file sizes to only process new lines
#if ( exist 
TARGETSIZE=`wc -l ${SOURCEDATADIR}/${TARGET}/neosservices.log | awk '{print $1}'`
echo "DEBUG: TARGETSIZE=${TARGETSIZE}"

### Get log data to start processing
echo "... Copying source logs."
echo "    Executing:  scp -B swfcfe@${TARGET}:/logs/neos/services/neosservices.log ${SOURCEDATADIR}/${TARGET}/neosservices.log"
scp -B swfcfe@${TARGET}:/logs/neos/services/neosservices.log ${SOURCEDATADIR}/${TARGET}/neosservices.log

### Process sources
### Get new size to limit processing
NEWTARGETSIZE=`wc -l ${SOURCEDATADIR}/${TARGET}/neosservices.log | awk '{print $1}'`
let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}+2000"
echo "DEBUG: NEWTARGETSIZE=${NEWTARGETSIZE}"
echo "DEBUG: LINES2PROCESS=${LINES2PROCESS}"

echo "    Executing:  tail -${LINES2PROCESS} ${SOURCEDATADIR}/${TARGET}/neosservices.log | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk ${SOURCEDATADIR}/${TARGET}/neosservices.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinute.txt"
tail -${LINES2PROCESS} ${SOURCEDATADIR}/${TARGET}/neosservices.log | awk --file ${AWKDIR}/awkfilterMDWOperationsMinute.awk ${SOURCEDATADIR}/${TARGET}/neosservices.log | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinute.txt
echo "    Executing:  awk --file ${AWKDIR}/awkfilterMDWOperationsRRDUpdate.awk ${SOURCEDATADIR}/${TARGET}/filterbyMinute.txt > ${SOURCEDATADIR}/${TARGET}/updateRRD${TARGET}.sh"
awk --file ${AWKDIR}/awkfilterMDWOperationsRRDUpdate.awk ${SOURCEDATADIR}/${TARGET}/filterbyMinute.txt > ${SOURCEDATADIR}/${TARGET}/updateRRD${TARGET}full.sh

### Trim last lines to avoid inserting incomplete data
head -n -12 ${SOURCEDATADIR}/${TARGET}/updateRRD${TARGET}full.sh > ${SOURCEDATADIR}/${TARGET}/updateRRD${TARGET}.sh
bash ${SOURCEDATADIR}/${TARGET}/updateRRD${TARGET}.sh 

echo "... Generating Graphs."
/usr/bin/rrdtool graph - \
--imgformat=PNG \
--start=-43200 \
--end=-60 \
--title='MDW Operations Performance - NEOS 26027' \
COMMENT:"Last 12 hours" \
--base=1000 \
--height=160 \
--width=500 \
--alt-autoscale-max \
--lower-limit=0 \
--vertical-label='counts / avg response time' \
--slope-mode \
--font TITLE:12: \
--font AXIS:8: \
--font LEGEND:9: \
--font UNIT:8: \
DEF:a="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":totalCount:AVERAGE \
DEF:b="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":count60:AVERAGE \
DEF:c="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":count30:AVERAGE \
DEF:d="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":count20:AVERAGE \
DEF:e="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":count10:AVERAGE \
DEF:f="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":count5:AVERAGE \
DEF:g="/var/lib/cacti/rra/neos_weblogp5_26027.rrd":averageTime:AVERAGE \
LINE2:a#9FA4EE7F:"Total calls"  \
GPRINT:a:LAST:"Current\:%8.0lf\n"  \
AREA:b#9FA4EEFF:"Calls >60 secs"  \
GPRINT:b:AVERAGE:"Current\:%8.0lf"  \
GPRINT:b:MAX:"Max\:%8.2lf %s\n"  \
LINE1:c#FF7D0066:"Calls >30 secs"  \
GPRINT:c:LAST:" Current\:%8.0lf"  \
GPRINT:c:MAX:"Max\:%8.2lf %s\n"  \
LINE1:d#7EE600CC:"Calls >20 secs"  \
GPRINT:d:LAST:"   Current\:%8.0lf"  \
GPRINT:d:MAX:"Max\:%8.2lf %s\n"  \
LINE1:e#7EE600CC:"Calls >10 secs"  \
GPRINT:e:LAST:"   Current\:%8.0lf"  \
GPRINT:e:MAX:"Max\:%8.2lf %s\n"  \
LINE1:f#7EE600CC:"Calls >5 secs"  \
GPRINT:f:LAST:"   Current\:%8.0lf"  \
GPRINT:f:MAX:"Max\:%8.2lf %s\n"  \
LINE3:g#FF0000FF:"Average Time"  \
GPRINT:g:AVERAGE:"Current\:%8.0lf"  \
GPRINT:g:AVERAGE:"Average\:%8.2lf %s"  \
GPRINT:g:MAX:"Max\:%8.2lf %s\n" > ${GRAPHDIR}/MDWOperations26027${TARGET}.png

/usr/bin/rrdtool graph - \
--imgformat=PNG \
--start=-43200 \
--end=-60 \
--title='MDW Operations Performance - NEOS 13001' \
COMMENT:"Last 12 hours" \
--base=1000 \
--height=160 \
--width=500 \
--alt-autoscale-max \
--lower-limit=0 \
--vertical-label='counts / avg response time' \
--slope-mode \
--font TITLE:12: \
--font AXIS:8: \
--font LEGEND:9: \
--font UNIT:8: \
DEF:a="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":totalCount:AVERAGE \
DEF:b="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":count60:AVERAGE \
DEF:c="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":count30:AVERAGE \
DEF:d="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":count20:AVERAGE \
DEF:e="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":count10:AVERAGE \
DEF:f="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":count5:AVERAGE \
DEF:g="/var/lib/cacti/rra/neos_weblogp5_13001.rrd":averageTime:AVERAGE \
LINE2:a#9FA4EE7F:"Total calls"  \
GPRINT:a:LAST:"Current\:%8.0lf\n"  \
AREA:b#9FA4EEFF:"Calls >60 secs"  \
GPRINT:b:AVERAGE:"Current\:%8.0lf"  \
GPRINT:b:MAX:"Max\:%8.2lf %s\n"  \
LINE1:c#FF7D0066:"Calls >30 secs"  \
GPRINT:c:LAST:" Current\:%8.0lf"  \
GPRINT:c:MAX:"Max\:%8.2lf %s\n"  \
LINE1:d#7EE600CC:"Calls >20 secs"  \
GPRINT:d:LAST:"   Current\:%8.0lf"  \
GPRINT:d:MAX:"Max\:%8.2lf %s\n"  \
LINE1:e#7EE600CC:"Calls >10 secs"  \
GPRINT:e:LAST:"   Current\:%8.0lf"  \
GPRINT:e:MAX:"Max\:%8.2lf %s\n"  \
LINE1:f#7EE600CC:"Calls >5 secs"  \
GPRINT:f:LAST:"   Current\:%8.0lf"  \
GPRINT:f:MAX:"Max\:%8.2lf %s\n"  \
LINE3:g#FF0000FF:"Average Time"  \
GPRINT:g:AVERAGE:"Current\:%8.0lf"  \
GPRINT:g:AVERAGE:"Average\:%8.2lf %s"  \
GPRINT:g:MAX:"Max\:%8.2lf %s\n" > ${GRAPHDIR}/MDWOperations13001${TARGET}.png

/usr/bin/rrdtool graph - \
--imgformat=PNG \
--start=-43200 \
--end=-60 \
--title='MDW Operations Performance - NEOS 09020' \
COMMENT:"Last 12 hours" \
--base=1000 \
--height=160 \
--width=500 \
--alt-autoscale-max \
--lower-limit=0 \
--vertical-label='counts / avg response time' \
--slope-mode \
--font TITLE:12: \
--font AXIS:8: \
--font LEGEND:9: \
--font UNIT:8: \
DEF:a="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":totalCount:AVERAGE \
DEF:b="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":count60:AVERAGE \
DEF:c="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":count30:AVERAGE \
DEF:d="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":count20:AVERAGE \
DEF:e="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":count10:AVERAGE \
DEF:f="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":count5:AVERAGE \
DEF:g="/var/lib/cacti/rra/neos_weblogp5_09020.rrd":averageTime:AVERAGE \
LINE2:a#9FA4EE7F:"Total calls"  \
GPRINT:a:LAST:"Current\:%8.0lf\n"  \
AREA:b#9FA4EEFF:"Calls >60 secs"  \
GPRINT:b:AVERAGE:"Current\:%8.0lf"  \
GPRINT:b:MAX:"Max\:%8.2lf %s\n"  \
LINE1:c#FF7D0066:"Calls >30 secs"  \
GPRINT:c:LAST:" Current\:%8.0lf"  \
GPRINT:c:MAX:"Max\:%8.2lf %s\n"  \
LINE1:d#7EE600CC:"Calls >20 secs"  \
GPRINT:d:LAST:"   Current\:%8.0lf"  \
GPRINT:d:MAX:"Max\:%8.2lf %s\n"  \
LINE1:e#7EE600CC:"Calls >10 secs"  \
GPRINT:e:LAST:"   Current\:%8.0lf"  \
GPRINT:e:MAX:"Max\:%8.2lf %s\n"  \
LINE1:f#7EE600CC:"Calls >5 secs"  \
GPRINT:f:LAST:"   Current\:%8.0lf"  \
GPRINT:f:MAX:"Max\:%8.2lf %s\n"  \
LINE3:g#FF0000FF:"Average Time"  \
GPRINT:g:AVERAGE:"Current\:%8.0lf"  \
GPRINT:g:AVERAGE:"Average\:%8.2lf %s"  \
GPRINT:g:MAX:"Max\:%8.2lf %s\n" > ${GRAPHDIR}/MDWOperations09020${TARGET}.png

/usr/bin/rrdtool graph - \
--imgformat=PNG \
--start=-43200 \
--end=-60 \
--title='MDW Operations Performance - NEOS 03040' \
COMMENT:"Last 12 hours" \
--base=1000 \
--height=160 \
--width=500 \
--alt-autoscale-max \
--lower-limit=0 \
--vertical-label='counts / avg response time' \
--slope-mode \
--font TITLE:12: \
--font AXIS:8: \
--font LEGEND:9: \
--font UNIT:8: \
DEF:a="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":totalCount:AVERAGE \
DEF:b="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":count60:AVERAGE \
DEF:c="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":count30:AVERAGE \
DEF:d="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":count20:AVERAGE \
DEF:e="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":count10:AVERAGE \
DEF:f="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":count5:AVERAGE \
DEF:g="/var/lib/cacti/rra/neos_weblogp5_03040.rrd":averageTime:AVERAGE \
LINE2:a#9FA4EE7F:"Total calls"  \
GPRINT:a:LAST:"Current\:%8.0lf\n"  \
AREA:b#9FA4EEFF:"Calls >60 secs"  \
GPRINT:b:AVERAGE:"Current\:%8.0lf"  \
GPRINT:b:MAX:"Max\:%8.2lf %s\n"  \
LINE1:c#FF7D0066:"Calls >30 secs"  \
GPRINT:c:LAST:" Current\:%8.0lf"  \
GPRINT:c:MAX:"Max\:%8.2lf %s\n"  \
LINE1:d#7EE600CC:"Calls >20 secs"  \
GPRINT:d:LAST:"   Current\:%8.0lf"  \
GPRINT:d:MAX:"Max\:%8.2lf %s\n"  \
LINE1:e#7EE600CC:"Calls >10 secs"  \
GPRINT:e:LAST:"   Current\:%8.0lf"  \
GPRINT:e:MAX:"Max\:%8.2lf %s\n"  \
LINE1:f#7EE600CC:"Calls >5 secs"  \
GPRINT:f:LAST:"   Current\:%8.0lf"  \
GPRINT:f:MAX:"Max\:%8.2lf %s\n"  \
LINE3:g#FF0000FF:"Average Time"  \
GPRINT:g:AVERAGE:"Current\:%8.0lf"  \
GPRINT:g:AVERAGE:"Average\:%8.2lf %s"  \
GPRINT:g:MAX:"Max\:%8.2lf %s\n" > ${GRAPHDIR}/MDWOperations03040${TARGET}.png

