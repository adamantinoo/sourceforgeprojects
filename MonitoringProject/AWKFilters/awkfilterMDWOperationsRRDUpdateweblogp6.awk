#####
#	This filter gets the aggreated measures for MDW Operations and generates the
#	corresponding update statements to the RRD databases to load the resulting data.
#	During the load converts date time to mktime() format and the milliseconds to
#	seconds.
#
#	FORMAT:
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>P1 COUNT>P2 COUNT>P3 COUNT>P4 COUNT>P5
#
#	OUTPUT FORMAT
#	The output is normalized to the rrdtool update statements
#####
BEGIN {
	print "#!/bin/bash"
}
/02045/ {
	UpdateRRD( "neos_weblogp6_MDW02045.rrd" )
}
/02062/ {
	UpdateRRD( "neos_weblogp6_MDW02062.rrd" )
}
/03001/ {
	UpdateRRD( "neos_weblogp6_MDW03001.rrd" )
}
/03002/ {
	UpdateRRD( "neos_weblogp6_MDW03002.rrd" )
}
/03004/ {
	UpdateRRD( "neos_weblogp6_MDW03004.rrd" )
}
/03031/ {
	UpdateRRD( "neos_weblogp6_MDW03031.rrd" )
}
/03035/ {
	UpdateRRD( "neos_weblogp6_MDW03035.rrd" )
}
/03040/ {
	UpdateRRD( "neos_weblogp6_MDW03040.rrd" )
}
/03041/ {
	UpdateRRD( "neos_weblogp6_MDW03041.rrd" )
}
/03043/ {
	UpdateRRD( "neos_weblogp6_MDW03043.rrd" )
}
/09020/ {
	UpdateRRD( "neos_weblogp6_MDW09020.rrd" )
}
/09050/ {
	UpdateRRD( "neos_weblogp6_MDW09050.rrd" )
}
/10004/ {
	UpdateRRD( "neos_weblogp6_MDW10004.rrd" )
}
/10010/ {
	UpdateRRD( "neos_weblogp6_MDW10010.rrd" )
}
/10027/ {
	UpdateRRD( "neos_weblogp6_MDW10027.rrd" )
}
/13001/ {
	UpdateRRD( "neos_weblogp6_MDW13001.rrd" )
}
/26027/ {
	UpdateRRD( "neos_weblogp6_MDW26027.rrd" )
}
/48027/ {
	UpdateRRD( "neos_weblogp6_MDW48027.rrd" )
}
/88003/ {
	UpdateRRD( "neos_weblogp6_MDW88003.rrd" )
}

function UpdateRRD( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:countP1:countP2:countP3:countP4:countP5:TOCount" 
    printf( "%s %s:%s:%s:%s:%s:%s:%s:%s:%s\n", command, time, $4, $5/1000, $7, $8, $9 ,$10, $11, $6 )
}

