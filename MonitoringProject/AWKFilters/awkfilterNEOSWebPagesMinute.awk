#####
#	AWK Filter to extract NEOS operations calls
#	and count and measure the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		00:00:13:0698  1.834   658663739  /init.neos
#		hour           time (s) MSISDN    NEOS Operation Name
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONNAME COUNTTOTAL COUNT>60 COUNT>30 COUNT>20 COUNT>10 COUNT>5 AVGTIME
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/init.neos/ {
	PageCallAcountingSimple($4)
}
/init-mobile.neos/ {
	PageCallAcountingSimple($4)
}
/info.neos/ {
	PageCallAcountingSimple($4)
}
/dashboard.neos/ {
	PageCallAcountingSimple($4)
}
/dashboard.neos - usage/ {
	PageCallAcountingComposite($4 " - " $6)
}
/dashboard.neos - usagePrepaid/ {
	PageCallAcountingComposite($4 " - " $6)
}
/dashboard.neos - points/ {
	PageCallAcountingComposite($4 " - " $6)
}
/dashboard.neos - refresh/ {
	PageCallAcountingComposite($4 " - " $6)
}
/callslist.neos/ {
	PageCallAcountingSimple($4)
}
/callslist.neos - filter/ {
	PageCallAcountingComposite($4 " - " $6)
}
/callslist.neos - showFilters/ {
	PageCallAcountingComposite($4 " - " $6)
}
/usage.neos/ {
	PageCallAcountingSimple($4)
}
/usage.neos - usage/ {
	PageCallAcountingComposite($4 " - " $6)
}
/usage.neos - balance/ {
	PageCallAcountingComposite($4 " - " $6)
}
/usage.neos - forward/ {
	PageCallAcountingComposite($4 " - " $6)
}
/usage.neos - maverickBalance/ {
	PageCallAcountingComposite($4 " - " $6)
}
/services.neos/ {
	PageCallAcountingSimple($4)
}
/services.neos - init/ {
	PageCallAcountingComposite($4 " - " $6)
}
/services.neos - save/ {
	PageCallAcountingComposite($4 " - " $6)
}
/services.neos - success/ {
	PageCallAcountingComposite($4 " - " $6)
}
/priceconfiguration.neos/ {
	PageCallAcountingSimple($4)
}
/personaldata.neos/ {
	PageCallAcountingSimple($4)
}
/personaldata.neos - find/ {
	PageCallAcountingComposite($4 " - " $6)
}
/personaldata.neos - validateForm/ {
	PageCallAcountingComposite($4 " - " $6)
}
/personaldata.neos - update/ {
	PageCallAcountingComposite($4 " - " $6)
}
/personaldata.neos - normalize/ {
	PageCallAcountingComposite($4 " - " $6)
}
/personaldata.neos - failure/ {
	PageCallAcountingComposite($4 " - " $6)
}
/personaldata.neos - success/ {
	PageCallAcountingComposite($4 " - " $6)
}

END {
	for( registro in PageCounter ) {
		split( registro, data, SUBSEP )
		printf( "%s %s %s %2.0i %4.1f %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], PageCounter[data[1], data[2], data[3]], PageTimer[data[1], data[2], data[3]]/PageCounter[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]], PageCounterKO[data[1], data[2], data[3]] )
	}
}
function PageCallAcountingSimple(PageName)
{
	# Get the day, month and year from the source data
	split( $1, arrHora, ":" )

	date=strftime( "%Y-%m-%d", systime() )
	time = arrHora[1] ":" arrHora[2]
	PageCounter[PageName, date, time]++
	PageTimer[PageName, date, time]+=$2

	# Configuration parameters
	TimeOut=80.0
	P1=10.0
	P2=15.0
	P3=30.0
	P4=50.0
	P5=60.0
	#Create counters for different elapsed time frames
	if ( $2 > TimeOut ) PageCounterKO[PageName, date, time]++
	if ( $2 > P5 ) PageCounterP5[PageName, date, time]++
	if ( $2 > P5 ) PageCounterP4[PageName, date, time]++
	if ( $2 > P3 ) PageCounterP3[PageName, date, time]++
	if ( $2 > P2 ) PageCounterP2[PageName, date, time]++
	if ( $2 > P1 ) PageCounterP1[PageName, date, time]++
}
function PageCallAcountingComposite(PageReference)
{
	# Get the day, month and year from the source data
	split( $1, arrHora, ":" )

	date=strftime( "%Y-%m-%d", systime() )
	time = arrHora[1] ":" arrHora[2]
	PageCounter[PageReference, date, time]++
	PageTimer[PageReference, date, time]+=$2

	# Configuration parameters
	TimeOut=80.0
	P1=10.0
	P2=15.0
	P3=30.0
	P4=50.0
	P5=60.0
	#Create counters for different elapsed time frames
	if ( $2 > TimeOut ) PageCounterKO[PageReference, date, time]++
	if ( $2 > P5 ) PageCounterP5[PageReference, date, time]++
	if ( $2 > P5 ) PageCounterP4[PageReference, date, time]++
	if ( $2 > P3 ) PageCounterP3[PageReference, date, time]++
	if ( $2 > P2 ) PageCounterP2[PageReference, date, time]++
	if ( $2 > P1 ) PageCounterP1[PageReference, date, time]++
}


