#####
#	AWK Filter to extract MDW log lines from the WL functional logs from PPOW application
#	and count and measure the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		[24/11/11 00:00:28][DEBUG][DetalleTerminalFunc] Se realiza la consulta de tarifas compatibles (34002) uid [652967507] sessionId [621tTN6YQTtgnyKXZTdJyqfDvBtnqdBdrHS0r6JpFLWjTksZlWfn!2048891136!1878659261!1322089208139]
#		[DATE TIME][DEBUG][DetalleTerminalFunc] ...(OPMIDDLEWARE) uid [MSISDN] ...
#
#		[22/03/12 01:27:10][INFO ][HistoricoPedidosFunc] consulta de pedidos - operacion 64012 - Msisdn[615372740]| fin [178] mls uid [615372740] sessionId [1x6wPqxdhPmLZMsqwQQvXrqYQJ26RJyRVTQP733N4gVw96gpRntF!1467552591!1583135457!1332376029625]
#		[DATE TIME][INFO ][PPOWOPERATION] desc - operacion MDWOPCODE - Msisdn[MSISDN]| fin [MSTIME] mls...

#		[03/04/12 08:39:11][INFO ][ConsultaSocioFunc] consulta de socios - operacion 02045 - Msisdn[646536324]| fin [264] mls sessionId [wwvgP6bPsb3Hfq8xgVNBTp4Xv4JmSY7Vbw1nYyP8k3QWvnJWKmW5!2133256032!1187534696!1333435151286]


#
#	OUTPUT FORMAT
#		The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>5 COUNT>10 COUNT>20 COUNT>30 COUNT>60
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>5 COUNT>10 COUNT>20 COUNT>30 COUNT>60
#
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/consulta de socios - operacion 02045/ && /fin/ {
	Operation="02045"
    # Get the day, month and year from the source data
    split( $1, trimDate, "[" )
    split( $2, trimHour, "]" )
    split( trimHour[1], splitHour, ":" )
    split( trimDate[2], splitDate, "/" )

    date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
    time = splitHour[1] ":" splitHour[2]
#    print date " " time " "
    
    gsub(/.*fin \[/,"",$0)
	gsub(/\] mls sessionId.*/,"",$0)
	TimeElapsed = $0
#	print "TimeElapsed=" TimeElapsed
	MDWCounter[Operation, date, time]++
	MDWTimer[Operation, date, time]+=TimeElapsed
	MDWCounterTimeout[Operation, date, time]=0
	PageCallAcounting( Operation )
}

/consulta de lista de tarifas compatibles - operacion 34002/ && /fin/ {
	Operation="34002"
    # Get the day, month and year from the source data
    split( $1, trimDate, "[" )
    split( $2, trimHour, "]" )
    split( trimHour[1], splitHour, ":" )
    split( trimDate[2], splitDate, "/" )

    date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
    time = splitHour[1] ":" splitHour[2]
#    print date " " time " "
    
    gsub(/.*fin \[/,"",$0)
	gsub(/\] mls uid.*/,"",$0)
	TimeElapsed = $0
#	print "TimeElapsed=" TimeElapsed
	MDWCounter[Operation, date, time]++
	MDWTimer[Operation, date, time]+=TimeElapsed
	MDWCounterTimeout[Operation, date, time]=0
	PageCallAcounting( Operation )
}

/consulta de ofertas - operacion 85202/ && /fin/ {
	Operation="85202"
    # Get the day, month and year from the source data
    split( $1, trimDate, "[" )
    split( $2, trimHour, "]" )
    split( trimHour[1], splitHour, ":" )
    split( trimDate[2], splitDate, "/" )

    date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
    time = splitHour[1] ":" splitHour[2]
#    print date " " time " "
    
    gsub(/.*fin \[/,"",$0)
	gsub(/\] mls uid.*/,"",$0)
	TimeElapsed = $0
#	print "TimeElapsed=" TimeElapsed
	MDWCounter[Operation, date, time]++
	MDWTimer[Operation, date, time]+=TimeElapsed
	MDWCounterTimeout[Operation, date, time]=0
	PageCallAcounting( Operation )
}

/Se DESCONECTADO realiza la consulta de tarifas compatibles/ {
        # Get the day, month and year from the source data
        split( $1, trimDate, "[" )
        split( $2, trimHour, "]" )
        split( trimHour[1], splitHour, ":" )
        split( trimDate[2], splitDate, "/" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
#        print date " " time
		# No other data to get form the log. Account the line in the right array.
		MDWCounter["34002", date, time]++
}
#/error acceso middleware consultaListaTarifasCompatibles/ {
/Ha ocurrido un error al consultar la lista de tarifas compatibles/ {
        # Get the day, month and year from the source data
        split( $1, trimDate, "[" )
        split( $2, trimHour, "]" )
        split( trimHour[1], splitHour, ":" )
        split( trimDate[2], splitDate, "/" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		MDWCounter["34002", date, time]++
		MDWCounterTimeout["34002", date, time]++
}
/Realizando DESCONECTADO la consulta de ofertas/ {
        # Get the day, month and year from the source data
        split( $1, trimDate, "[" )
        split( $2, trimHour, "]" )
        split( trimHour[1], splitHour, ":" )
        split( trimDate[2], splitDate, "/" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
		MDWCounter["85202", date, time]++
#		MDWCounterTimeout["85202", date, time]++
}
/Error realizando la consulta de ofertas/ {
        # Get the day, month and year from the source data
        split( $1, trimDate, "[" )
        split( $2, trimHour, "]" )
        split( trimHour[1], splitHour, ":" )
        split( trimDate[2], splitDate, "/" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		MDWCounter["85202E", date, time]++
		MDWCounterTimeout["85202", date, time]++
}

END {
        for( registro in MDWCounter ) {
			split( registro, data, SUBSEP )
			printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], MDWCounterTimeout[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
        }
}

function PageCallAcounting( PageName ) {
	# Configuration parameters
	P1=5000
	P2=10000
	P3=20000
	P4=30000
	P5=60000
	# Create counters for different elapsed time frames
	if ( $3 > P5 ) PageCounterP5[PageName, date, time]++
	if ( $3 > P4 ) PageCounterP4[PageName, date, time]++
	if ( $3 > P3 ) PageCounterP3[PageName, date, time]++
	if ( $3 > P2 ) PageCounterP2[PageName, date, time]++
	if ( $3 > P1 ) PageCounterP1[PageName, date, time]++
}

