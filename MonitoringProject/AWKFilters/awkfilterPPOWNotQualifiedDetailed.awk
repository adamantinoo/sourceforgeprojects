######################################################################
# AWK
#	Script to filter source data end generate the detailed information
#		for users that do not qualify any campaing.
######################################################################
BEGIN {
	OFS = "\t"   # output field separator is a tab
}
/Los datos introducidos no aplican campania/{
	# Record model
	# [22/06/11 00:50:38][ERROR][AccesoPPOCmd] Los datos introducidos no aplican campania|605|Msisdn [635459193]Tipo Contrato [POSPAGO]Tipo Usuario [EMPRESA]Id Producto [null]codProvincia [30]Customer Code [1.34836805]Codigo postal [30000]campania del cliente [null]  sessionId [Pp8FTBgRsQtdl6732HpQGWpN9zScp2dyFtdydlv1m0zmGrf1zQyn!-1038801242!-1541604164!1308696625615] 

	# Print the filtered data to the new detailed file
	split( $1, arrayDate, "[" )
	split( $2, arrayHour, "]" )
	printf( "%s %s %s %s %s\n", arrayDate[2], arrayHour[1], $9, $11, $13 )
}

