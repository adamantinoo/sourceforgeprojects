#####
#	AWK Filter to extract NEOS operations calls
#	and count and measure the response time in a day aggregation.
#	Input lines are filtered to keep only the NEOS Operations log lines.
#
#	FORMAT:
#		2011-08-10 00:00:03,451  03928  100%  M633263208 # GetOffer
#		year mon date hour       time (ms)    MSISDN     # NEOS Operation Name
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONNAME COUNTTOTAL AVGTIME COUNT>P5 COUNT>P4 COUNT>P3 COUNT>P2 COUNT>P1 TIMEOUTS
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
	nowDate = strftime( "%d/%m/%Y", systime() )
}
/# / {
    # Get the day, month and year from the source data
    split( $2, splitHour, ":" )

	date=$1
    time=splitHour[1]
	NEOSCounter[$7, date, time]++
	NEOSTimer[$7, date, time]+=$3
	PageCallAcounting($7)
}
/# / && /No se recibio respuesta/ {
    # Get the day, month and year from the source data
    split( $2, splitHour, ":" )

	date=$1
    time = splitHour[1]
	PageCounterTO[$7, date, time]++
}

END {
    for( record in NEOSCounter ) {
		split( record, data, SUBSEP )
		if ( filterType == "DATE" ) {
			if ( data[2] == filterValue ) {
				printf( "%s %s %s %2.0i %4.1f %2.0i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], NEOSCounter[data[1], data[2], data[3]], NEOSTimer[data[1], data[2], data[3]]/NEOSCounter[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]], PageCounterTO[data[1], data[2], data[3]] )
			}
		}
    }
}

function PageCallAcounting(PageName)
{
	# Configuration parameters
	P1=8.0
	P2=10.0
	P3=20.0
	P4=30.0
	P5=60.0
	#Create counters for different elapsed time frames
	if ( $3 > P5 ) PageCounterP5[PageName, date, time]++
	if ( $3 > P4 ) PageCounterP4[PageName, date, time]++
	if ( $3 > P3 ) PageCounterP3[PageName, date, time]++
	if ( $3 > P2 ) PageCounterP2[PageName, date, time]++
	if ( $3 > P1 ) PageCounterP1[PageName, date, time]++
}

