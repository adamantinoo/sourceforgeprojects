#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		YYYYMMDD (INITIALS) - Message.
#
#	AWK Filter to generate the RRD update commands from the CAF (Common Analitical Format) of
#		MDW operations. That format is common to NEOS, FEW amd other sources. The ouput is the
#		RRDTool update commands to input all that information into the databases.
#	During the load converts date time to mktime() format and the milliseconds to seconds to
#		fit into the data formats expected by RRD and Cacti.
#
#	INPUT FORMAT:
#		2011/12/10 15:34 02045 300 1245.9 0
#		DATE       TIME  OPER  COUNT AV TIME TIMEOUTS COUNTP1 COUNTP2 COUNTP3 COUNTP4 COUNTP5
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>P1 COUNT>P2 COUNT>P3 COUNT>P4 COUNT>P5
#
#	OUTPUT FORMAT:
#		/usr/bin/rrdtool update /var/lib/cacti/rra/neos_weblogp5_MDW03031.rrd --template totalCount:averageTime:countP1:countP2:countP3:countP4:countP5:TOCount 1326123180:176:0.833:1:::::0

BEGIN {
	print "#!/bin/bash"
}
/02045/ {
	UpdateRRD( "siebel_"SERVER"_MDW02045.rrd" )
}
/02048/ {
	UpdateRRD( "siebel_"SERVER"_MDW02048.rrd" )
}

function UpdateRRD( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:TOCount:countP1:countP2:countP3:countP4:countP5" 
	printf( "%s %s:%s:%s:%s:%s:%s:%s:%s:%s\n", command, time, $4, $5/1000, $6, $7, $8, $9 ,$10, $11 )
}

