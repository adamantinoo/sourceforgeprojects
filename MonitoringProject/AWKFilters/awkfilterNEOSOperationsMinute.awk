#####
#	AWK Filter to extract NEOS operations calls
#	and count and measure the response time in a minute aggregation.
#	Input lines are filtered to keep only the NEOS log lines.
#
#	FORMAT:
#		2011-08-10 00:00:03,451  03928  100%  M633263208 # GetOffer
#		year mon date hour       time (ms)    MSISDN     # NEOS Operation Name
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONNAME COUNTTOTAL AVGTIME TIMEOUTCOUNT COUNT>P1 COUNT>P2 COUNT>P3 COUNT>P4 COUNT>P5 
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
#	nowMinute=strftime( "%M", systime() )
}
/# GetProfile/ {
	PageCallAcounting($7)
}
/# GetOffer/ {
	PageCallAcounting($7)
}
/# GetBalance/ {
	PageCallAcounting($7)
}
/# GetLoyaltyBalance/ {
	PageCallAcounting($7)
}
/# GetProducts/ {
	PageCallAcounting($7)
}
/# GetDownloadsList/ {
	PageCallAcounting($7)
}
/# GetUsage/ {
	PageCallAcounting($7)
}
/# SetOrder/ {
	PageCallAcounting($7)
}
/# GetInfoByLine/ {
	PageCallAcounting($7)
}

END {
		for( registro in MDWCounter ) {
			split( registro, data, SUBSEP )
			# Depending on filter drop the non matching lines
			if ( filterType == "DATE" ) {
				if ( data[2] == filterValue ) {
					printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], TOCounter[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
				}
			}
			if ( filterType == "MINUTE" ) {
#				nowMinute = filterValue
#	            split( data[3], splitHour, ":" )
#	            if ( splitHour[2] == nowMinute ) {
	            # Skip the line
#	            } else {
	            	printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], TOCounter[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
#	            }
			}
        }
}

function PageCallAcounting(PageName)
{
    # Get the day, month and year from the source data
    split( $2, arrHora, ":" )
	date=$1
    time = arrHora[1] ":" arrHora[2]
    MDWCounter[$7, date, time]++
	MDWTimer[$7, date, time]+=$3

	# Configuration parameters
	TimeOut=60000
	P1=5000
	P2=15000
	P3=20000
	P4=30000
	P5=40000
	#Create counters for different elapsed time frames
	if ( $3 > TimeOut ) TOCounter[PageReference, date, time]++
	if ( $3 > P5 ) PageCounterP5[PageName, date, time]++
	if ( $3 > P4 ) PageCounterP4[PageName, date, time]++
	if ( $3 > P3 ) PageCounterP3[PageName, date, time]++
	if ( $3 > P2 ) PageCounterP2[PageName, date, time]++
	if ( $3 > P1 ) PageCounterP1[PageName, date, time]++
}

