#####
#	This filter gets the aggreated measures for MDW Operations and generates the
#	corresponding update statements to the RRD databases to load the resulting data.
#	During the load converts date time to mktime() format and the milliseconds to
#	seconds.
#
#	FORMAT:
#		2011/11/25 00:00 34002 36  0.0  0               
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>P1 COUNT>P2 COUNT>P3 COUNT>P4 COUNT>P5
#
#	OUTPUT FORMAT
#	The output is normalized to the rrdtool update statements
#####
BEGIN {
	print "#!/bin/bash"
}
/02045/ {
	UpdateRRD( "ppow_MDW02045.rrd" )
}
/34002/ {
	UpdateRRD( "ppow_MDW34002.rrd" )
}
/85202/ {
	UpdateRRD( "ppow_MDW85202.rrd" )
}

function UpdateRRD( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "/" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:TOCount:countP1:countP2:countP3:countP4:countP5" 
	printf( "%s %s:%s:%s:%s:%s:%s:%s:%s:%s # %s - %s\n", command, time, $4, "0.0", $6, $7, $8, $9 ,$10, $11, $1, $2 )
}

