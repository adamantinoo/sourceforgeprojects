# Tracear los logs de los WL para los Threads
BEGIN { FS = ">" }

/activeExecuteThreads1/ { printf "activeExecuteThreads1:" substr($74,1,length($74) -4) " "  }
/executeThreadTotalCount1/ { printf "executeThreadTotalCount1:" substr($2,1,length($2) -4) " " }
/executeThreadIdleCount1/ { printf "executeThreadIdleCount1:" substr($2,1,length($2) -4) " " }
/queueLength1/ { printf "queueLength1:" substr($2,1,length($2) -4) * (-1) " " }
/pendingUserRequestCount1/ { printf "pendingUserRequestCount1:" substr($2,1,length($2) -4) " " }
/completedRequestCount1/ { printf "completedRequestCount1:" substr($2,1,length($2) -4) " " }
/hoggingThreadCount1/ { printf "hoggingThreadCount1:" substr($2,1,length($2) -4) " " }
/standbyThreadCount1/ { printf "standbyThreadCount1:" substr($2,1,length($2) -4) " " }
/throughput1/ {
	data=substr($2,1,length($2) -4)
#	sub(/\./, ",", data)
	printf "throughput1:" data " "
}
#/healthState1/ { printf "healthState1:" substr($2,1,length($2) -4) }

