######################################################################
# AWK
#	Script to aggregate detailed data for user not qualifying
#		into and aggregated file by DATE HOUR
######################################################################
BEGIN {
	OFS = "\t"   # output field separator is a tab
}
{
	# Record model
	# 22/06/11 00:50:38 [635459193]Tipo [POSPAGO]Tipo [EMPRESA]Id
	# Date is field $1 but from hour we have to get just the hour
	split( $2, fieldsHour, ":" )
	# Segment and Contract has to be stripped from the source data
	position=index( $4, "]" )
	position-=2
	contract=substr( $4, 2, position )
	position=index( $5, "]" )
	position-=2
	segment=substr( $5, 2, position )
	qualificationCOUNTER[ $1, fieldsHour[1] ":00", segment, contract ]++
}

END {
	for( record in qualificationCOUNTER ) {
		# Index data is: 1-Date 2-Hour 3-Segment 4-Contract
		split( record, data, SUBSEP )
		printf( "%s %s %s %s %2.0i\n", data[1], data[2], data[3], data[4], qualificationCOUNTER[ data[1], data[2], data[3], data[4] ] )
	}
}

