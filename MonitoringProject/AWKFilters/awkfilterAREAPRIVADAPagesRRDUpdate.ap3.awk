#####
#	AWK Filter to update RRD databases for Apache Access Pages in the
#		domain "areaprivada" from daily generated files from log processing.
#
#	FORMAT:
#		2011-2011-01 04:08 /neos/dashboard.neos 12 12         
#		YEAR-MONTH-DAY HOUR:MINUTE PAGE_URL COUNTTOTAL COUNT200 COUNT302 COUNT403 COUNT404
#
#	OUTPUT FORMAT
#	The output is normalized to the rrdtooL update statements
#####
BEGIN {
	print "#!/bin/bash"
}
/\/neos\/init.neos/ {
	UpdateRRD( "areaprivada_ap3_init.neos.rrd" )
}
/\/neos\/init-mobile.neos/ {
	UpdateRRD( "areaprivada_ap3_init-mobile.neos.rrd" )
}
/\/neos\/dashboard.neos/ {
	UpdateRRD( "areaprivada_ap3_dashboard.neos.rrd" )
}

function UpdateRRD(DBName)
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	month=MapMonth2MM( dateSplit[2] )
	date=dateSplit[1] " " month " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" DBName " --template totalCount:count200:count302:count403:count404" 
    printf( "%s %s:%s:%s:%s:%s:%s\n", command, time, $4, $5, $6, $7, $8 )
}

function MapMonth2MM( monthName ) {
	if ( monthName == "Sep" ) return 9
}

