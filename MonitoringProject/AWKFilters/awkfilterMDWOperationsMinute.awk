#####
#	AWK Filter to extract MDW log lines fromt the WL functioal logs
#	and count and measute the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		2011-08-10 00:00:03,451  03928  100%  M633263208 # GetOffer # 09020
#		year mon date hour       time (ms)    MSISDN     # NEOS Operation Name # MDW Operation Code
#		2011-09-28 09:59:59,455  02281  100%  M654787231 #  channel 1 # GetProfile # 26027
#		2011-11-14 16:31:32,639  02166  100%  B11477627 # none #  channel 1 # GetInfoByLine # 02045
#
#	OUTPUT FORMAT
#		The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME COUNT>5 COUNT>10 COUNT>20 COUNT>30 COUNT>60
#		2011-09-01 12:42 GetOffer 15 25672.0 14 14 12  4   
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUT COUNT>5 COUNT>10 COUNT>20 COUNT>30 COUNT>60
#
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/# / {
        # Get the day, month and year from the source data
        split( $2, splitHour, ":" )

		date=$1
        time = splitHour[1] ":" splitHour[2]
		if ( $12 == "" ) {
			### This is the old log format
			if ( $9 == "" ) skip
			else {
		    	MDWCounter[$9, date, time]++
				MDWTimer[$9, date, time]+=$3
				MDWCounterTimeout[$9, date, time]=0
				PageCallAcounting($9)
			}
		}	
		else {
			### Test if it is a multiline record
			if ($5 ~ /^M.*/) {
		    	MDWCounter[$12, date, time]++
				MDWTimer[$12, date, time]+=$3
				MDWCounterTimeout[$12, date, time]=0
				PageCallAcounting($12)
			} else {
		    	MDWCounter[$14, date, time]++
				MDWTimer[$14, date, time]+=$3
				MDWCounterTimeout[$14, date, time]=0
				PageCallAcounting($14)
			}
		}
}

/# / && /No se recibio respuesta/ {
        # Get the day, month and year from the source data
        split( $2, splitHour, ":" )

		date=$1
        time = splitHour[1] ":" splitHour[2]
		if ( $12 == "" )
			skip
		else 
        	MDWCounterTimeout[$12, date, time]++
}

END {
        for( registro in MDWCounter ) {
			split( registro, data, SUBSEP )
			# Depending on filter drop the non matching lines
			if ( filterType == "DATE" ) {
				if ( data[2] == filterValue ) {
					printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], MDWCounterTimeout[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
				}
			}
			if ( filterType == "MINUTE" ) {
				printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], MDWCounterTimeout[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
			}
        }
}

function PageCallAcounting(PageName)
{
	# Configuration parameters
	P1=5000
	P2=10000
	P3=20000
	P4=30000
	P5=60000
	# Create counters for different elapsed time frames
	if ( $3 > P5 ) PageCounterP5[PageName, date, time]++
	if ( $3 > P4 ) PageCounterP4[PageName, date, time]++
	if ( $3 > P3 ) PageCounterP3[PageName, date, time]++
	if ( $3 > P2 ) PageCounterP2[PageName, date, time]++
	if ( $3 > P1 ) PageCounterP1[PageName, date, time]++
}

