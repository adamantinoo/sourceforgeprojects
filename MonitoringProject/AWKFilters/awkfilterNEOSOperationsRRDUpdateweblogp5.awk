#####
#	This filter gets the aggreated measures for NEOS Operations and generates the
#	corresponding update statements to the RRD databases to load the resulting data.
#	During the load converts date time to mktime() format and the milliseconds to
#	seconds.
#
#	FORMAT:
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME TIMEOUTCOUNT COUNT>P1 COUNT>P2 COUNT>P3 COUNT>P4 COUNT>60
#
#	OUTPUT FORMAT
#	The output is normalized to the rrdtooL update statements
#####
BEGIN {
	print "#!/bin/bash"
}
/GetProfile/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetProfile.rrd" )
}
/GetOffer/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetOffer.rrd" )
}
/GetBalance/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetBalance.rrd" )
}
/GetLoyaltyBalance/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetLoyaltyBalance.rrd" )
}
/GetProducts/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetProducts.rrd" )
}
/GetDownloadsList/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetDownloadsList.rrd" )
}
/GetUsage/ {
	if ( $0 ~ "GetUsagePrepaid" ) {
		UpdateRRDNew( "neos_weblogp5_NEOSGetUsagePrepaid.rrd" )
	} else {
		UpdateRRDNew( "neos_weblogp5_NEOSGetUsage.rrd" )
	}	
}
/SetOrder/ {
	if ( $0 ~ "SetOrderAsync" ) {
		UpdateRRDNew( "neos_weblogp5_NEOSSetOrderAsync.rrd" )
	} else {
		UpdateRRDNew( "neos_weblogp5_NEOSSetOrder.rrd" )
	}	
}
/GetInfoByLine/ {
	UpdateRRDNew( "neos_weblogp5_NEOSGetInfoByLine.rrd" )
}

function UpdateRRD( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:count5:count10:count20:count30:count60" 
    printf( "%s %s:%s:%s:%s:%s:%s:%s:%s\n", command, time, $4, $5/1000, $7, $8, $9 ,$10, $11 )
}
function UpdateRRDNew( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:TOCount:countP1:countP2:countP3:countP4:countP5" 
    printf( "%s %s:%s:%s:%s:%s:%s:%s:%s:%s\n", command, time, $4, $5/1000, $6, $7, $8, $9 ,$10, $11 )
}

