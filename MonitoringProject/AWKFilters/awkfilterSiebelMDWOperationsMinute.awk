#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		YYYYMMDD (INITIALS) - Message.
#
#	AWK Filter to generate the Cacti output from Siebel MDW log files. The data is
#		parsed for some literals that mark the initial and end events for elapsed time
#		MDW operations.
#	After that detection the date is accounted in the same structures used for NEOS MDW
#		processing.
#
#	INPUT FORMAT:
#		2012 Jan 09 10:29:47:241 GMT +1 BW.SBLLoyPesado-Process_Archive_siegtwp4_1 User [BW-User] - Job-4831902 [Procesos/AdpPesados/02045/Pd_02045_TratPeticion.process/Group/Log_Ini_TratPeticion]: 09/01/2012 10:29:47 - TrackingId: Sh84EWuIkQpLoE5LVFzzy6U-zzw - Inicio flujo 02045. Mensaje recibido desde GDP: [<ns0:C__02045__Peticion xmlns:ns0="http://www.tibco.com/xmlns/ae2xsd/2002/05/ae/AMENA/MOVILES/SERVICIOS/CRM/C_02045">
#		2012 Jan 15 02:24:41:430 GMT +1 BW.SBLLoyPesado-Process_Archive_sieomp8_1 User [BW-User] - Job-21673075 [Procesos/AdpPesados/02045/Pd_02045_ConsSocios.process/Log Inicio control de errores]: 15/01/2012 02:24:41 - TrackingId: 7P4/ETGYrjNqqEnFc9D2XA4-2ZU - Inicio control de errores 02045. Publicando error: ErrorCode=[GDP_ERR_024501] ErrorMsg=[Se ha producido un timeout en Siebel CRM(02045)]
#		YEAR MON DATE TIME...TRACKINGID...
#
#	OUTPUT FORMAT:
#		2011/12/10 15:34 02045 300 1245.9 0
#		DATE       TIME  OPER  COUNT AV TIME TIMEOUTS COUNTP1 COUNTP2 COUNTP3 COUNTP4 COUNTP5

BEGIN {
	OFS="\t"   # output field separator is a tab
	Tracking = ""
}

/Inicio flujo 02045/ {
#	printf("%s\n", $0)
	# Get Date/Time information from line to set the start time point.
	Anio = $1
	Mes = $2
	Dia = $3
	Tiempo = $4

	# Get the Tracking id from the line. This is done by parsing the line
	gsub(/.*TrackingId: /,"",$0);
	gsub(/ - Inicio flujo.*/,"",$0);
	Tracking = $0;
#	printf("%s\n", "OPEN----" Tracking "----")	
	
	# Store this information to be retrieved when the matching end of operation is processed.
	OpenTrackingData[Tracking] = Anio " " MapMonth2MM(Mes) " " Dia " " Tiempo
}
/Se ha producido un timeout en Siebel CRM(02045)/ {
	# Get Date/Time information from line to get the end time point and calculate the elapsed
	Anio = $1
	Mes = MapMonth2MM($2)
	Dia = $3
	Tiempo = $4

	# Get the Tracking id from the line. This id is used for searching the current open trackings.
	gsub(/.*TrackingId: /,"",$0);
	gsub(/ - Inicio control de errores 02045.*/,"",$0);
	Tracking = $0;
#	printf("%s\n", "TIMEOUT----" Tracking "----")	

	if( OpenTrackingData[Tracking] != "" ) {
		# Convert times to milliseconds to perform the substraction
		# YYYY MM DD HH MM SS
		split( Tiempo, timeSplit, ":" )
		date=Anio " " Mes " " Dia " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		endtime=mktime(date)*1000+timeSplit[4]
#		print "endtime" endtime

		split( OpenTrackingData[Tracking], dateSplit, " " )
		split( dateSplit[4], timeSplit, ":" )
		date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		starttime=mktime(date)*1000+timeSplit[4]
#		print "starttime" starttime

		elapsed=endtime-starttime
#		arrayTracking[Tracking] = elapsed

		# Account the MDW operation in the data structures. Count the opearation in the corresponding time slot.
		date = dateSplit[1] "-" dateSplit[2] "-" dateSplit[3]
		time = timeSplit[1] ":" timeSplit[2]
#		print date "-" time
		MDWCounter["02045", date, time]++
		MDWTimer["02045", date, time]+=elapsed
		MDWCounterTimeout["02045", date, time]++
		PageCallAcounting("02045", elapsed)
	}
}
/Inicio flujo 02048/ {
#	printf("%s\n", $0)
	# Get Date/Time information from line to set the start time point.
	Anio = $1
	Mes = $2
	Dia = $3
	Tiempo = $4

	# Get the Tracking id from the line. This is done by parsing the line
	gsub(/.*TrackingId: /,"",$0);
	gsub(/ - Inicio flujo.*/,"",$0);
	Tracking = $0;
#	printf("%s\n", "OPEN----" Tracking "----")	
	
	# Store this information to be retrieved when the matching end of operation is processed.
	OpenTrackingData[Tracking] = Anio " " MapMonth2MM(Mes) " " Dia " " Tiempo
}
/Fin tratamiento mensaje recibido desde GDP . Operacion 02045/ {
#	printf("%s\n", $0)
	
	# Get Date/Time information from line to get the end time point and calculate the elapsed
	Anio = $1
	Mes = MapMonth2MM($2)
	Dia = $3
	Tiempo = $4

	# Get the Tracking id from the line. This id is used for searching the current open trackings.
	gsub(/.*TrackingId: /,"",$0);
	gsub(/ - Fin tratamiento.*/,"",$0);
	Tracking = $0;
#	printf("%s\n", "CLOSE----" Tracking "----")	

	if( OpenTrackingData[Tracking] != "" ) {
		# Convert times to milliseconds to perform the substraction
		# YYYY MM DD HH MM SS
		split( Tiempo, timeSplit, ":" )
		date=Anio " " Mes " " Dia " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		endtime=mktime(date)*1000+timeSplit[4]
#		print "endtime" endtime

		split( OpenTrackingData[Tracking], dateSplit, " " )
		split( dateSplit[4], timeSplit, ":" )
		date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		starttime=mktime(date)*1000+timeSplit[4]
#		print "starttime" starttime

		elapsed=endtime-starttime
#		arrayTracking[Tracking] = elapsed

		# Account the MDW operation in the data structures. Count the opearation in the corresponding time slot.
		date = dateSplit[1] "-" dateSplit[2] "-" dateSplit[3]
		time = timeSplit[1] ":" timeSplit[2]
#		print date "-" time
		MDWCounter["02045", date, time]++
		MDWTimer["02045", date, time]+=elapsed
#		MDWCounterTimeout["02045", date, time]=0
		PageCallAcounting("02045", elapsed)
	}
}
/Fin tratamiento respuesta 02048/ {
#	printf("%s\n", $0)
	
	# Get Date/Time information from line to get the end time point and calculate the elapsed
	Anio = $1
	Mes = MapMonth2MM($2)
	Dia = $3
	Tiempo = $4

	# Get the Tracking id from the line. This id is used for searching the current open trackings.
	gsub(/.*TrackingId: /,"",$0);
	gsub(/ - Fin tratamiento.*/,"",$0);
	Tracking = $0;
#	printf("%s\n", "CLOSE----" Tracking "----")	

	if( OpenTrackingData[Tracking] != "" ) {
		# Convert times to milliseconds to perform the substraction
		# YYYY MM DD HH MM SS
		split( Tiempo, timeSplit, ":" )
		date=Anio " " Mes " " Dia " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		endtime=mktime(date)*1000+timeSplit[4]
#		print "endtime" endtime

		split( OpenTrackingData[Tracking], dateSplit, " " )
		split( dateSplit[4], timeSplit, ":" )
		date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " " timeSplit[3]
		starttime=mktime(date)*1000+timeSplit[4]
#		print "starttime" starttime

		elapsed=endtime-starttime
#		arrayTracking[Tracking] = elapsed

		# Account the MDW operation in the data structures. Count the opearation in the corresponding time slot.
		date = dateSplit[1] "-" dateSplit[2] "-" dateSplit[3]
		time = timeSplit[1] ":" timeSplit[2]
#		print date "-" time
		MDWCounter["02048", date, time]++
		MDWTimer["02048", date, time]+=elapsed
#		MDWCounterTimeout["02048", date, time]=0
		PageCallAcountingLong("02048", elapsed)
	}
}

END {
    for( registro in MDWCounter ) {
		split( registro, data, SUBSEP )
		printf( "%s %s %s %2.0i %4.1f %2.1i %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], MDWCounterTimeout[data[1], data[2], data[3]], PageCounterP1[data[1], data[2], data[3]], PageCounterP2[data[1], data[2], data[3]], PageCounterP3[data[1], data[2], data[3]], PageCounterP4[data[1], data[2], data[3]], PageCounterP5[data[1], data[2], data[3]] )
    }
}

function MapMonth2MM( monthName ) {
# English
	if ( monthName == "Jan" ) return 1
	if ( monthName == "Feb" ) return 2
	if ( monthName == "Mar" ) return 3
	if ( monthName == "Apr" ) return 4
	if ( monthName == "May" ) return 5
	if ( monthName == "Jun" ) return 6
	if ( monthName == "Jul" ) return 7
	if ( monthName == "Aug" ) return 8
	if ( monthName == "Sep" ) return 9
	if ( monthName == "Oct" ) return 10
	if ( monthName == "Nov" ) return 11
	if ( monthName == "Dec" ) return 12
# Spanish
	if ( monthName == "Ene" ) return 1
	if ( monthName == "Feb" ) return 2
	if ( monthName == "Mar" ) return 3
	if ( monthName == "Abr" ) return 4
	if ( monthName == "May" ) return 5
	if ( monthName == "Jun" ) return 6
	if ( monthName == "Jul" ) return 7
	if ( monthName == "Ago" ) return 8
	if ( monthName == "Sep" ) return 9
	if ( monthName == "Oct" ) return 10
	if ( monthName == "Nov" ) return 11
	if ( monthName == "Dic" ) return 12
}
function PageCallAcounting(PageName, ElapsedTime) {
	# Configuration parameters
	P1=1000
	P2=2000
	P3=5000
	P4=10000
	P5=30000
	# Create counters for different elapsed time frames
	if ( ElapsedTime > P5 ) PageCounterP5[PageName, date, time]++
	if ( ElapsedTime > P4 ) PageCounterP4[PageName, date, time]++
	if ( ElapsedTime > P3 ) PageCounterP3[PageName, date, time]++
	if ( ElapsedTime > P2 ) PageCounterP2[PageName, date, time]++
	if ( ElapsedTime > P1 ) PageCounterP1[PageName, date, time]++
}
function PageCallAcountingLong(PageName, ElapsedTime) {
	# Configuration parameters
	P1=5000
	P2=8000
	P3=10000
	P4=30000
	P5=60000
	# Create counters for different elapsed time frames
	if ( ElapsedTime > P5 ) PageCounterP5[PageName, date, time]++
	if ( ElapsedTime > P4 ) PageCounterP4[PageName, date, time]++
	if ( ElapsedTime > P3 ) PageCounterP3[PageName, date, time]++
	if ( ElapsedTime > P2 ) PageCounterP2[PageName, date, time]++
	if ( ElapsedTime > P1 ) PageCounterP1[PageName, date, time]++
}

