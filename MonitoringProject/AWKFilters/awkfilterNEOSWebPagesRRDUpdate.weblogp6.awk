#####
#	AWK Filter to extract MDW log lines fromt the WL functioal logs
#	and count and measute the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		2011-08-10 00:00 03004 38 4.5395 1  2  2  2  5 
#		YEAR-MONTH-DAY HOUR:MINUTE OPERATIONCODE COUNTTOTAL AVGTIME COUNT>5 COUNT>10 COUNT>20 COUNT>30 COUNT>60
#
#	OUTPUT FORMAT
#	The output is normalized to the rrdtoo update statements
#####
BEGIN {
	print "#!/bin/bash"
}
/init.neos/ {
	UpdateRRD( "neosweb_weblogp6_initneos.rrd" )
}
/dashboard.neos/ {
	UpdateRRD( "neosweb_weblogp6_dashboard.rrd" )
}
/callslist.neos/ {
	UpdateRRD( "neosweb_weblogp6_callslist.rrd" )
}
/usage.neos/ {
	UpdateRRD( "neosweb_weblogp6_usage.rrd" )
}
/services.neos/ {
	UpdateRRD( "neosweb_weblogp6_services.rrd" )
}
/priceconfiguration.neos/ {
	UpdateRRD( "neosweb_weblogp6_priceconfiguration.rrd" )
}
/personaldata.neos/ {
	UpdateRRD( "neosweb_weblogp6_personaldata.rrd" )
}

function UpdateRRD( RRDFileName )
{
	# Convert the date and time to number of seconds since epoch
	# YYYY MM DD HH MM SS
	split( $1, dateSplit, "-" )
	split( $2, timeSplit, ":" )
	date=dateSplit[1] " " dateSplit[2] " " dateSplit[3] " " timeSplit[1] " " timeSplit[2] " 00"
	time=mktime(date)
	command="/usr/bin/rrdtool update /var/lib/cacti/rra/" RRDFileName " --template totalCount:averageTime:countP1:countP2:countP3:countP4:countP5:TOCount" 
    printf( "%s %s:%s:%s:%s:%s:%s:%s:%s:%s\n", command, time, $4, $5, $6, $7, $8, $9 ,$10, $11 )
}

