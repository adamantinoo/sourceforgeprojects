#####
#	AWK Filter to extract WL Apache calls.
#
#	FORMAT:
#		95.18.48.125 - - [13/Sep/2011:04:04:21 +0200] "POST /neos/charges.neos HTTP/1.1" 200 -
#		195.235.72.74 - - [13/Sep/2011:04:04:23 +0200] "GET /neos/callslist.neos?reqCode=filter&init=true&selectedItem=neos.views.left-menu.usage-2 HTTP/1.1" 200 25731
#		IP            - - [DATE:HOUR +GMT]             "GET/POST  URL          HTTP/1.1 HTTPCODE SIZE
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE PAGENAME COUNTTOTAL COUNT200 COUNT302 COUNT403 COUNT404
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/ / {
        # Get the day, month and year from the source data
        split( $4, timeData, ":" )
        split( timeData[1], dateData, "/" )
        split( dateData[1], dayData, "[" )
        year=dateData[3]
        month=dateData[2]
        day=dayData[2]
        hour=timeData[2]
        minute=timeData[3]

		date=year "-" month "-" day
        time = hour ":" minute
        
        split( $7, urlData, "?" )
        PageCounter[urlData[1], date, time]++
        if( $9 == "200" ) PageCounter200[urlData[1], date, time]++
        if( $9 == "302" ) PageCounter302[urlData[1], date, time]++
        if( $9 == "403" ) PageCounter403[urlData[1], date, time]++
        if( $9 == "404" ) PageCounter404[urlData[1], date, time]++
}

END {
        for( register in PageCounter ) {
                split( register, data, SUBSEP )
                printf( "%s %s %s %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], PageCounter[data[1], data[2], data[3]], PageCounter200[data[1], data[2], data[3]], PageCounter302[data[1], data[2], data[3]], PageCounter403[data[1], data[2], data[3]], PageCounter404[data[1], data[2], data[3]] )
        }
}

