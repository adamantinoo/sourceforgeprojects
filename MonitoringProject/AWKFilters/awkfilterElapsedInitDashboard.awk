#####
#	AWK Filter to calculate the difference time between the init.neos call
#		and the corresponding dashboard call
#####
/ \/init.neos/ {
	# Store the call time and the MSISDN and open the time counter
	# 08:48:18:0632  1.602   633369770  /init.neos
#	printf( "%s\n", $0 )
	split( $1, timeSplit, ":" )
	seconds=timeSplit[1]*3600+timeSplit[2]*60+timeSplit[3]
#	printf( "%s %2.0i %2.0i %2.0i\n", "Time detected:", timeSplit[1], timeSplit[2], timeSplit[3] )
	openCalls[$3]=seconds
}
/ \/dashboard.neos - usage/ {
	# Match pairing init with a dashboard
	# 09:21:05:0564  0.618   633369770  /dashboard.neos - usage
#	printf( "%s\n", $0 )
	if( openCalls[$3] != "" ) {
		split( $1, timeSplit, ":" )
		newseconds=timeSplit[1]*3600+timeSplit[2]*60+timeSplit[3]
		elapsed=newseconds-openCalls[$3]
#		printf( "%s %2.0i %2.0i\n", $3, newseconds, openCalls[$3] )
		if( elapsed > 60 ) {
			printf( "%s %s %4.3f\n", $1, $3, elapsed )
		}
		
		# Add data to record matrix for analitics
		analiticsCounter[timeSplit[1], timeSplit[2]]++
		analiticsAverage[timeSplit[1], timeSplit[2]]+=elapsed
		
		openCalls[$3]=""
	}
}
END {
	for( registro in analiticsCounter ) {
		split( registro, data, SUBSEP )
		printf( "%s:%s %2.0i %4.1f\n", data[1], data[2], analiticsCounter[data[1], data[2]], analiticsAverage[data[1], data[2]]/analiticsCounter[data[1], data[2]] )
	}
}

