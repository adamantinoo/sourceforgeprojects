#####
#	Get the load on the consult of bills.
#
#	FORMAT:
#		10.113.59.9 - - [16/Nov/2011:00:02:30 +0100] "GET /fewsp/loginFromNeos.few?ChoosePage=billResume HTTP/1.1" 200 9463
#
#	OUTPUT FORMAT
#		Number of calls by hour
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/ChoosePage=billResume/ {
	### Extract the date and convert to mktime format
	split( $4, splitDateTime, ":" )
	split( splitDateTime[1], splitDate, "[" )
	date=splitDate[2]

	hour=splitDateTime[2]
	minute=splitDateTime[3]

	PageCounter[ date, hour ":" minute]++
}
END {
    for( register in PageCounter ) {
        split( register, data, SUBSEP )
        printf( "%s %s %2.1i\n", data[1], data[2], PageCounter[data[1], data[2]] )
    }
}

