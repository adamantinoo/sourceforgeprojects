#####
#	AWK Filter to aggregate CMD_INICIO_PEDIDO and CMD_CONFIRMAR_PEDIDO_WEB from Apache logs
#	on a daily basis. Data is collected and aggregated by day and on future developments is will be
#	possible to segmentate result by other axis.
#
#	FORMAT:
#		77.208.27.237 - - [01/Jul/2011:08:18:29 +0200] "GET /ecareppo/controlador.srvl?JSESSIONID=Q9p2TNlc9xzydcD5TbSf7vJKtvjWPzYjCfhG2V8MWfHxy1DZ1Flk!-608585926!-526110913&JSESSIONID=Q9p2TNlc9xzydcD5TbSf7vJKtvjWPzYjCfhG2V8MWfHxy1DZ1Flk!-608585926!-526110913&comando=CMD_INICIO_PEDIDO&tipoContrato=POSPAGO&tipoUsuario=RESIDENCIAL&idProducto=1-2A1MUQ4&REQ_ORIGEN=CMD_DETALLE_TERMINAL&REQ_CODOFERTA=1-3BZB3MJ&REQ_ID_RECARGA=&REQ_TARIFA_SEL=-1 HTTP/1.1" 302 883
#		IP            - - [DATE GMT]                      "GET URL   HTTP/1.1" HTTP_CODE SIZE  
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY OPERATIONNAME COUNTTOTAL
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/CMD_INICIO_PEDIDO/ && /REQ_ORIGEN=CMD_DETALLE_TERMINAL/ {
	### Extract the date
	split( $4, splitDate, ":" )
	split( splitDate[1], date, "[" )
	INICIOCounter[date[2]]++
}
/CMD_CONFIRMAR_PEDIDO_WEB/ {
	### Extract the date
	split( $4, splitDate, ":" )
	split( splitDate[1], date, "[" )
	CONFIRMARCounter[date[2]]++
}

END {
    for( record in INICIOCounter ) {
		split( record, data, SUBSEP )
		printf( "%s %2.1i %2.1i\n", data[1], INICIOCounter[data[1]], CONFIRMARCounter[data[1]] )
    }
}

