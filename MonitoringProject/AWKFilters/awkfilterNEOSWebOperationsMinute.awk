#####
#	AWK Filter to extract NEOS operations calls
#	and count and measure the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		00:00:13:0698  1.834   658663739  /init.neos
#		hour           time (s) MSISDN    NEOS Operation Name
#
#	OUTPUT FORMAT
#	The output is normalized to YEAR-MONTH-DAY HOUR:MINUTE OPERATIONNAME COUNTTOTAL COUNT>60 COUNT>30 COUNT>20 COUNT>10 COUNT>5 AVGTIME
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}
/# GetProfile/ {
        # Get the day, month and year from the source data
        split( $2, arrHora, ":" )

		date=$1
        time = arrHora[1] ":" arrHora[2]
        MDWCounter[$7, date, time]++
		MDWTimer[$7, date, time]+=$3

		#Create counters for different elapsed time frames
		if ( $3 >= 60000 ) MDWCounter60[$7, date, time]++
		if ( $3 > 30000 ) MDWCounter30[$7, date, time]++
		if ( $3 > 20000 ) MDWCounter20[$7, date, time]++
		if ( $3 > 10000 ) MDWCounter10[$7, date, time]++
		if ( $3 > 5000 ) MDWCounter5[$7, date, time]++
}
/# GetOffer/ {
        # Get the day, month and year from the source data
        split( $2, arrHora, ":" )

		date=$1
        time = arrHora[1] ":" arrHora[2]
        MDWCounter[$7, date, time]++
		MDWTimer[$7, date, time]+=$3

		#Create counters for different elapsed time frames
		if ( $3 >= 60000 ) MDWCounter60[$7, date, time]++
		if ( $3 > 30000 ) MDWCounter30[$7, date, time]++
		if ( $3 > 20000 ) MDWCounter20[$7, date, time]++
		if ( $3 > 10000 ) MDWCounter10[$7, date, time]++
		if ( $3 > 5000 ) MDWCounter5[$7, date, time]++
}
/# GetBalance/ {
        # Get the day, month and year from the source data
        split( $2, arrHora, ":" )

		date=$1
        time = arrHora[1] ":" arrHora[2]
        MDWCounter[$7, date, time]++
		MDWTimer[$7, date, time]+=$3

		#Create counters for different elapsed time frames
		if ( $3 >= 60000 ) MDWCounter60[$7, date, time]++
		if ( $3 > 30000 ) MDWCounter30[$7, date, time]++
		if ( $3 > 20000 ) MDWCounter20[$7, date, time]++
		if ( $3 > 10000 ) MDWCounter10[$7, date, time]++
		if ( $3 > 5000 ) MDWCounter5[$7, date, time]++
}

END {
        for( registro in MDWCounter ) {
                split( registro, data, SUBSEP )
                printf( "%s %s %s %2.0i %4.1f %2.0i %2.0i %2.0i %2.0i %2.0i\n", data[2], data[3], data[1], MDWCounter[data[1], data[2], data[3]], MDWTimer[data[1], data[2], data[3]]/MDWCounter[data[1], data[2], data[3]], MDWCounter5[data[1], data[2], data[3]], MDWCounter10[data[1], data[2], data[3]], MDWCounter20[data[1], data[2], data[3]], MDWCounter30[data[1], data[2], data[3]], MDWCounter60[data[1], data[2], data[3]] )
        }
}

