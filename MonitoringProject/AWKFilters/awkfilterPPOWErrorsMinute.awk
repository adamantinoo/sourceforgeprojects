#####
#	AWK Filter to extract MDW log lines from the WL functional logs from PPOW application
#	and count and measure the response time in a minute aggregation.
#	Input lines are filtered to keep only the MDW log lines.
#
#	FORMAT:
#		06/12/11 00:00:26 ERROR ConfirmarDatosWebFunc  consultarPedidoFunc - puntos insuficientes
#		DATE TIME ERROR DetalleTerminalFunc Message
#
#	OUTPUT FORMAT
#
#####
BEGIN {
	OFS="\t"   # output field separator is a tab
}

/AccesoMiddleware   error acceso middleware consultaSocio [SB3904]/ {
        # Get the day, month and year from the source data
#        split( $1, trimDate, "[" )
#        split( $2, trimHour, "]" )
        split( $1, splitDate, "/" )
        split( $2, splitHour, ":" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		print date "-" time "-" $4
		ErrorCounterE1[date, time]++
		ErrorCounter[date, time]++
}
/ComandoBaseCmd  Error realizando la consulta de socios/ {
        # Get the day, month and year from the source data
        split( $1, splitDate, "/" )
        split( $2, splitHour, ":" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		print date "-" time "-" $4
		ErrorCounterE2[date, time]++
		ErrorCounter[date, time]++
}
/MovimientoPuntosFunc  Llamada a HistoricoPedidos produjo error/ {
        # Get the day, month and year from the source data
        split( $1, splitDate, "/" )
        split( $2, splitHour, ":" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		print date "-" time "-" $4
		ErrorCounterE3[date, time]++
		ErrorCounter[date, time]++
}
/ComandoBaseCmd  El catalogo obtenido no es valido| 9003/ {
        # Get the day, month and year from the source data
        split( $1, splitDate, "/" )
        split( $2, splitHour, ":" )

        date="20" splitDate[3] "/" splitDate[2] "/" splitDate[1]
        time = splitHour[1] ":" splitHour[2]
		# No other data to get form the log. Account the line in the right array.
#		print date "-" time "-" $4
		ErrorCounterE4[date, time]++
		ErrorCounter[date, time]++
}

END {
        for( registro in ErrorCounter ) {
			split( registro, data, SUBSEP )
			printf( "%s %s %2.1i %2.1i %2.1i %2.1i\n", data[1], data[2], ErrorCounterE1[data[1], data[2]], ErrorCounterE2[data[1], data[2]], ErrorCounterE3[data[1], data[2]], ErrorCounterE4[data[1], data[2]] )
        }
}

