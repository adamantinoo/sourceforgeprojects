#!/bin/bash

######################################################################
#	BASH LIBRARY
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: cactiWLThreadMonitoring.sh 59 2012-01-04 10:18:27Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-04 11:18:27 +0100 (mié, 04 ene 2012) $
#	REVISION:		$Revision: 59 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		DDMMYYYY (INITIALS) - Message.
#
#	DESCRIPTION:
#		Script that contains the bash functions definitions used on more
#			than one bash source script. With the use of functions the
#			code is more readable and the maintenance is easier.
#		This library contains functions related to the execution of scripts
#			from the Cacti application with call from the poller to scripts
#			defined on the Data Input Methods.
#
#	FUNCTIONS EXPORTED
### DetectConsole()
#		Checks the APPLICATIONNAME parameter against a closed list of
#			accepted values (the registered application names) to
#			set the corresponding server and port parameters to access
#			the WL console. This simplifies the usage and now the Cacti
#			interfaces have not to bing to the multiple server consoles
#			and futore changes will not affect Cacti parametrization.
### DetectCookie()
######################################################################

### FUNCTION DEFINITIONS
### DetectConsole()
function DetectConsole {
	if [ -z "${APPLICATIONNAME}" ]
	then
		# The application parameter is not informed. This is an error.
		echo "ERROR: Application not defined"
		exit 1
	fi
	if [ "${APPLICATIONNAME}" == "NEOS" ]
	then
		CONSOLENAME=${CONSOLENEOS}
		SERVERPORT=38001
		CONSOLEAUTHENTICATION="j_username=lecneos&j_password=neos0000"
	fi
	if [ "${APPLICATIONNAME}" == "FEW" ]
	then
		CONSOLENAME=${CONSOLEFEW}
		SERVERPORT=37001
		CONSOLEAUTHENTICATION="j_username=lecppo&j_password=lecppo0000"
	fi
	if [ "${APPLICATIONNAME}" == "PPOW" ]
	then
		CONSOLENAME=${CONSOLEPPOW}
		SERVERPORT=37001
		CONSOLEAUTHENTICATION="j_username=lecppo&j_password=lecppo0000"
	fi
	if [ "${APPLICATIONNAME}" == "PPOE" ]
	then
		CONSOLENAME=${CONSOLEPPOE}
		SERVERPORT=8001
		CONSOLEAUTHENTICATION="j_username=lecpposerv&j_password=pposerv0000"
	fi
}

### DetectCookie()
function DetectCookie {
	if [ ! -e ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt ]
	then
		OUTPUT=`curl -c ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt -d "${CONSOLEAUTHENTICATION}&j_character_encoding=UTF-8" -i "http://${CONSOLENAME}:${SERVERPORT}/console/j_security_check" --silent --output /dev/null`
		# --silent`
		# --output /dev/null
		if [ "${DEBUG}" == "DEBUG" ]
		then
			echo "DEBUG: Executing:  curl -c ${DATAPROCESSINGDIR}/cookie_consola_${CONSOLENAME}.txt -d \"${CONSOLEAUTHENTICATION}&j_character_encoding=UTF-8\" -i \"http://${CONSOLENAME}:${SERVERPORT}/console/j_security_check\""
			echo "DEBUG: OUTPUT=${OUTPUT}"
		fi
	fi
}
######################################################################

