#!/bin/bash

#####
#	Maintenance Script.
#	This script creates the new folders for downloaded data. The folder name is
#		the name received as the parameter $1.
#
#	PARAMETERS
#		$1 folder name
#####

	cd /home/swfcfe/MonitoringProject/SourceData/LogData/areaprivada
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/neosnohup
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/neosservices
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/neosservices.access
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/neosweb
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/neosweb.access
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/netstat
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/ppowservice
	mkdir $1
	cd /home/swfcfe/MonitoringProject/SourceData/LogData/ppowstats
	mkdir $1

	cd /home/swfcfe/MonitoringProject/HistoricData/MDWOperations
	mkdir $1
	cd /home/swfcfe/MonitoringProject/HistoricData/NEOSOperations
	mkdir $1
	cd /home/swfcfe/MonitoringProject/HistoricData/PageAccess
	mkdir $1
	cd /home/swfcfe/MonitoringProject/HistoricData/PPOWMDWOperations
	mkdir $1
	cd /home/swfcfe/MonitoringProject/HistoricData/PPOWQualifications
	mkdir $1
	cd /home/swfcfe/MonitoringProject/HistoricData/PPOWRenoves
	mkdir $1

