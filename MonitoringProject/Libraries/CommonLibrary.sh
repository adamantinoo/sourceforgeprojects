#!/bin/bash

##########################################################################################
#	Script library.
#	This file contains BASH functions to be used on other scripts. Its cotents are
#		a snippet library of source code.
#
### LIBRARY CONTENTS
### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
# Gets the end of the file from the last point processed that is a line number
#	stored in a local control file.
#
# PARAMETERS:
#	FILLERP1	To skip the real script parameters and not create confusion with
#				fuction parameters. NOT USED
#	FILLERP2	To skip the real script parameters and not create confusion with
#				fuction parameters. NOT USED
#	TARGETSERVER	The network name of the target servar that contains the logs.
#	SIZECONTROL		The name of the local file used to control the already processed lines.
#	SERVERLOGPATH	The path of the server log to download.
#	LOCALLOGPATH	The path of the local server log
#	TAILFILTER		The piece of script passed and concatenated with the downloading
#					"tail" to improve some preprocessed uses.
##########################################################################################
### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

function DetectAccessAlarm {
	ERROR=$1
	ERRORCODE=$2
	echo "DEBUG...DetectAccessAlarm = ${ERROR}"
	echo "DEBUG...DetectAccessAlarm.lastErrorCode= ${ERRORCODE}"
}

### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
# Gets the end of the file from the last point processed that is a line number
#	stored in a local control file.
#
# PARAMETERS:
#	FILLERP1	To skip the real script parameters and not create confusion with
#				fuction parameters. NOT USED
#	FILLERP2	To skip the real script parameters and not create confusion with
#				fuction parameters. NOT USED
#	TARGETSERVER	The network name of the target servar that contains the logs.
#	SIZECONTROL		The name of the local file used to control the already processed lines.
#	SERVERLOGPATH	The path of the server log to download.
#	LOCALLOGPATH	The path of the local server log
#	TAILFILTER		The piece of script passed and concatenated with the downloading
#					"tail" to improve some preprocessed uses.
function GetRemoteData {
### Load parameters and print debug information if the flag is activated
	FILLERP1=$1
	FILLERP2=$2
	TARGETSERVER=$3
	SIZECONTROL=$4
	SERVERLOGPATH=$5
	LOCALLOGPATH=$6
	TAILFILTER=$7
	if [ "${UPDATESWITCH}" == "DEBUG" ]
	then
		echo "DEBUG...FILLERP1=${FILLERP1}"
		echo "DEBUG...FILLERP2=${FILLERP2}"
		echo "DEBUG...TARGETSERVER=${TARGETSERVER}"
		echo "DEBUG...SIZECONTROL=${SIZECONTROL}"
		echo "DEBUG...SERVERLOGPATH=${SERVERLOGPATH}"
		echo "DEBUG...LOCALLOGPATH=${LOCALLOGPATH}"
		echo "DEBUG...TAILFILTER=${TAILFILTER}"
	fi
	
### Check if exists the file to store the current remote size
	echo "... Checking $3"
	if [ -e $4 ]
	then
		TARGETSIZE=`cat $4`
	else
		let TARGETSIZE=0
	fi
	echo "    TARGETSIZE=${TARGETSIZE}"
	RESULT=`ssh $3 -l swfcfe "wc -l $5" | awk '{ print $1 }'`
### Detect communication errors and control alarms
	ERRORCODE=$?
	DetectAccessAlarm ${RESULT} ${ERRORCODE}
	
	NEWTARGETSIZE=$RESULT
	echo "    NEWTARGETSIZE=${NEWTARGETSIZE}"
### Get new size to limit processing. This is a remote command.
	if [ "${UPDATESWITCH}" == "UPDATE" ]
	then
		echo ${NEWTARGETSIZE} > $4
	fi
### If new size is less that target then file has shrink due to a date change and rotation
	if [ -z "$TARGETSIZE" ]
	then
		TARGETSIZE=0
		echo "    TARGETSIZE=${TARGETSIZE}"
	fi
	if [ "${TARGETSIZE}" -ge "${NEWTARGETSIZE}" ]
	then
		TARGETSIZE=0
		echo "    TARGETSIZE=${TARGETSIZE}"
	fi
	let "LINES2PROCESS=${NEWTARGETSIZE}-${TARGETSIZE}"
### Add more lines to avoid processing gaps
	if [ "${TARGETSIZE}" -ne 0 ]
	then
		let "LINES2PROCESS=${LINES2PROCESS}+${LINES2PROCESS}/2"
	fi
	echo "    LINES2PROCESS=${LINES2PROCESS}"

### Get log data to start processing
	echo "... Copying source partial data from origin target."
	echo "    Executing:  ssh $3 -l swfcfe \"tail -${LINES2PROCESS} $5 $7\" > $6"
	ssh $3 -l swfcfe "tail -${LINES2PROCESS} $5 $7" > $6
	ERRORCODE=$?
	DetectAccessAlarm ${RESULT} ${ERRORCODE}
}

