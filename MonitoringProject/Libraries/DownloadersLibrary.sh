#!/bin/bash

##########################################################################################
#	Script library.
#	This file contains BASH functions to be used on other scripts. The
#		contents are independent functions documented to be used
#		by other scripts by sourcing the file in the script space.
#
### LIBRARY CONTENTS
### FilterPPOOperations
### DownloadServiceLogFile( TargetHost DestinationDirectory SourceLogName DestinationLogName )
# Downloads a NEOS service weblogic application log file (neosservices.log)
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
##########################################################################################
### DownloadWebLogFile( TargetHost DestinationDirectory SourceLogName DestinationLogName RenamePattern )
# Downloads a NEOS web weblogic application log file (neoswebsite.log)
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
#	RenamePattern			Pattern to use on the rename of the
#							downloaded log file.
##########################################################################################
### DownloadPPOWLogFile( TargetHost DestinationDirectory SourceLogName )
# Downloads a PPOW weblogic application log file (ecare_ppoXp.log.2012-02-01.gz)
# The logs file is already compressed and porperly named
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
#	RenamePattern			Pattern to use on the rename of the
#							downloaded log file.
##########################################################################################

### GLOBAL DEFINITIONS
HOMEDIR="/home/swfcfe"
SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
LOGDIR="${HOMEDIR}/MonitoringProject/logs"

### DownloadServiceLogFile( TargetHost DestinationDirectory SourceLogName DestinationLogName )
# Downloads a NEOS service weblogic application log file (neosservices.log)
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
##########################################################################################
function DownloadServiceLogFile {
### Load parameters and print debug information if the flag is activated
	TARGETHOST=$1
	DESTDIR=$2
	SOURCELOG=$3
	DESTLOG=$4
	if [ "${UPDATESWITCH}" == "DEBUG" ]
	then
		echo "DEBUG...TARGETHOST=${TARGETHOST}"
		echo "DEBUG...DESTDIR=${DESTDIR}"
		echo "DEBUG...SOURCELOG=${SOURCELOG}"
		echo "DEBUG...DESTLOG=${DESTLOG}"
	fi

	cd ${DESTDIR}
	if [ ! -e ${DESTLOG} ]
	then
		echo "    Executing:  scp -B swfcfe@${TARGETHOST}:${DATASOURCESERVICES}/${SOURCELOG} ."
		scp -B swfcfe@${TARGETHOST}:${DATASOURCESERVICES}/${SOURCELOG} .
	### Remove unused lines and compress the file again
		echo "    Executing:  gzip -dc ${SOURCELOG} | grep \" # \" | gzip -c > ${DESTLOG}"
		gzip -dc ${SOURCELOG} | grep " # " | gzip -c > ${DESTLOG}
	### Remove original data that has the same name in all servers
		echo "    Executing:  rm ${SOURCELOG}"
		rm ${SOURCELOG}
	fi
}

### DownloadWebLogFile( TargetHost DestinationDirectory SourceLogName DestinationLogName RenamePattern )
# Downloads a NEOS web weblogic application log file (neoswebsite.log)
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
#	RenamePattern			Pattern to use on the rename of the
#							downloaded log file.
##########################################################################################
function DownloadWebLogFile {
	TARGETHOST=$1
	DESTDIR=$2
	SOURCELOG=$3
	DESTLOG=$4
	RENAMEPATTERN=$5
	if [ "${UPDATESWITCH}" == "DEBUG" ]
	then
		echo "DEBUG...TARGETHOST=${TARGETHOST}"
		echo "DEBUG...DESTDIR=${DESTDIR}"
		echo "DEBUG...SOURCELOG=${SOURCELOG}"
		echo "DEBUG...DESTLOG=${DESTLOG}"
		echo "DEBUG...RENAMEPATTERN=${RENAMEPATTERN}"
	fi

	cd ${DESTDIR}
	if [ ! -e ${DESTLOG} ]
	then
		echo "    Executing:  scp -B swfcfe@${TARGETHOST}:${DATASOURCEWEB}/${SOURCELOG} ."
		scp -B swfcfe@${TARGETHOST}:${DATASOURCEWEB}/${SOURCELOG} .
		rename -v ${RENAMEPATTERN} neoswebsite.log*
	fi
}

### DownloadPPOWLogFile( TargetHost DestinationDirectory SourceLogName )
# Downloads a PPOW weblogic application log file (ecare_ppoXp.log.2012-02-01.gz)
# The logs file is already compressed and porperly named
# PARAMETERS:
#	TargetHost				Target host where to connect to get the log file
#	DestinationDirectory	Local destination directory
#	SourceLogName			Source log file name
#	DestinationLogName		Destination log file name
#	RenamePattern			Pattern to use on the rename of the
#							downloaded log file.
##########################################################################################
function DownloadPPOWLogFile {
### Load parameters and print debug information if the flag is activated
	TARGETHOST=$1
	DESTDIR=$2
	SOURCELOG=$3
	DESTLOG=$3
	if [ "${UPDATESWITCH}" == "DEBUG" ]
	then
		echo "DEBUG...TARGETHOST=${TARGETHOST}"
		echo "DEBUG...DESTDIR=${DESTDIR}"
		echo "DEBUG...SOURCELOG=${SOURCELOG}"
	fi

	cd ${DESTDIR}
	if [ ! -e ${DESTLOG} ]
	then
		echo "    Executing:  scp -B swfcfe@${TARGETHOST}:${DATASOURCE}/${SOURCELOG} ${DESTDIR}/${DESTLOG}"
		scp -B swfcfe@${TARGETHOST}:${DATASOURCE}/${SOURCELOG} ${DESTDIR}/${DESTLOG}
	fi
}

### FilterPPOOperations
function FilterPPOOperations {
	echo "    Executing:  awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} /tmp/${TARGET}filterNEOSOperations.txt | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt"
	awk --file ${AWKDIR}/awkfilterNEOSOperationsMinute.awk filterType=MINUTE filterValue=${FILTERMINUTE} /tmp/${TARGET}filterNEOSOperations.txt | sort > ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt
	echo "    Executing:  grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt | awk --file ${AWKDIR}/awkfilterNEOSOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSOperations.sh"
	grep -v "${FILTERDATE} ${FILTERHOUR}" ${SOURCEDATADIR}/${TARGET}/filterbyMinuteNEOSOperations.txt | awk --file ${AWKDIR}/awkfilterNEOSOperationsRRDUpdate${TARGET}.awk > ${SOURCEDATADIR}/${TARGET}/updateRRDNEOSOperations.sh
}

