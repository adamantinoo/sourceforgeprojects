#!/bin/bash

######################################################################
#	BASH
#	PROJECT:		OSP.CACTI.MONITORING
#	FILE NAME:		$Id: UpdateSiebelRRD.sh 80 2012-01-23 17:21:09Z swfcfe $
#	LAST UPDATE:	$Date: 2012-01-23 18:21:09 +0100 (lun, 23 ene 2012) $
#	REVISION:		$Revision: 80 $
#	AUTHORS:		Carlos Lozano (CL) / Luis de Diego (LDD)
#	COPYRIGHT:		Copyright © 2011,2012 by Orange Spain All Rights Reserved.
#	TO DO:
#		20100109 (LDD) - .
#
#	DESCRIPTION:
#	This script contains the list of commands to process a complete month of
#		historical data in a semi automatic way.
#
#	USAGE:
#		bash /home/swfcfe/MonitoringProject/Libraries/ReprocessingCommandsNEOS.sh 2012 01
#
#	PARAMETERS:
#		$1 YEAR
#		$2 MONTH
#		$3 UPDATESWITCH if we allow to update the RRD database. Any other value does not update the database
#				but make all other processing. Also does not modify the stored last record processed.
######################################################################

### GLOBAL DEFINITIONS
export HOMEDIR="/home/swfcfe"
export AWKDIR="${HOMEDIR}/MonitoringProject/AWKFilters"
export CACTIDIR="${HOMEDIR}/MonitoringProject/CactiScripts"
export SCRIPTDIR="${HOMEDIR}/MonitoringProject/ProcessingScripts"
export LIBRARYDIR="${HOMEDIR}/MonitoringProject/Libraries"
export SOURCEDATADIR="${HOMEDIR}/MonitoringProject/SourceData"
export LOGDIR="${HOMEDIR}/MonitoringProject/logs"

PROCESSNAME="ReprocessingCommandsNEOS.neos.manualprocess"
LOGFILE="${LOGDIR}/${PROCESSNAME}.log"

{
### GLOBAL VARIABLES
### Input Parameter processing.
#YEAR=$1
#MONTH=$2
UPDATESWITCH=$1

### FUNCTION DEFINITIONS
source "${LIBRARYDIR}/CommonLibrary.sh"
### GetRemoteData( FILLERP1 FILLERP2 TARGETSERVER SIZECONTROL SERVERLOGPATH LOCALLOGPATH [TAILFILTER] )
# Gets the end of the file from the last point processed that is a line number
#	stored in a local control file.
# PARAMETERS:
#	FILLERP1	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	FILLERP2	To skip the real script parameters and not create confusion with fuction parameters. NOT USED
#	TARGETSERVER	The network name of the target servar that contains the logs.
#	SIZECONTROL		The name of the local file used to control the already processed lines.
#	SERVERLOGPATH	The path of the server log to download.
#	LOCALLOGPATH	The path of the local server log
#	TAILFILTER		The piece of script passed and oncatenated with the downloading "tail" to improve some
#		preprocessed uses.
######################################################################

### MAIN PROCESSING
echo " "
echo "... START OF PROCESS. -${PROCESSNAME}- `date +%F` `date +%H:%M:%S`"
echo "... Parameters: [UPDATESWITCH]$1"

echo "... Remove current data and do cleanup."
cd /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup
#rm -rf neos_weblogp5_MDW*.rrd
#rm -rf neos_weblogp6_MDW*.rrd
#rm -rf neos_weblogp5_NEOS*.rrd
#rm -rf neos_weblogp6_NEOS*.rrd

echo "... Decompress last set of RRD databases."
### Compose file names
PREVIOUSYEAR=`date --date="2 months ago" +%Y`
PREVIOUSMONTH=`date --date="2 months ago" +%m`
echo "    PREVIOUSYEAR=${PREVIOUSYEAR}"
echo "    PREVIOUSMONTH=${PREVIOUSMONTH}"
MDWTARFILE=neos_MDW${PREVIOUSYEAR}${PREVIOUSMONTH}.tar
cd /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup
echo "    <...>tar xvfz ${MDWTARFILE}"
#tar xvfz ${MDWTARFILE}

echo "... Verifying database count."
RRACOUNT=`ls -a /var/lib/cacti/rra/neos_weblogp[5-6]_MDW* | wc -l`
REPROCESSCOUNT=`ls -a /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_MDW* | wc -l`
echo "    RRACOUNT=${RRACOUNT}"
echo "    REPROCESSCOUNT=${REPROCESSCOUNT}"
if [ "${RRACOUNT}" != "${REPROCESSCOUNT}" ]
then
	read -p "... Manual user intervention. Create additional databases not present the month before. Press [Enter] key to continue processing..."
fi

MDWTARFILE=neos_NEOS${PREVIOUSYEAR}${PREVIOUSMONTH}.tar
echo "    <...>tar xvfz ${MDWTARFILE}"
#tar xvfz ${MDWTARFILE}

echo "... Verifying database count."
RRACOUNT=`ls -a /var/lib/cacti/rra/neos_weblogp[5-6]_NEOS* | wc -l`
REPROCESSCOUNT=`ls -a /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_NEOS* | wc -l`
echo "    RRACOUNT=${RRACOUNT}"
echo "    REPROCESSCOUNT=${REPROCESSCOUNT}"
if [ "${RRACOUNT}" != "${REPROCESSCOUNT}" ]
then
	read -p "... Manual user intervention. Create additional databases not present the month before. Press [Enter] key to continue processing..."
fi

echo "... Preparing temporary location with historical data."
cd /home/swfcfe/tmp/ReprocessingNEOS
rm -rf *
LASTYEAR=`date --date="1 months ago" +%Y`
LASTMONTH=`date --date="1 months ago" +%m`
echo "    LASTYEAR=${LASTYEAR}"
echo "    LASTMONTH=${LASTMONTH}"
HISTORICDIR="/home/swfcfe/MonitoringProject/HistoricData"

echo "    <...>cp ${HISTORICDIR}/MDWOperations/${LASTYEAR}${LASTMONTH}/*minute*.txt ."
cp ${HISTORICDIR}/MDWOperations/${LASTYEAR}${LASTMONTH}/*minute*.txt .
NROFILES=`ls -a neosmdw_weblogp[5-6]*.txt | wc -l`
echo "    NROFILES=${NROFILES}"
echo "    <...>cp ${HISTORICDIR}/NEOSOperations/${LASTYEAR}${LASTMONTH}/*minute*.txt ."
cp ${HISTORICDIR}/NEOSOperations/${LASTYEAR}${LASTMONTH}/*minute*.txt .
NROFILES=`ls -a neosweblogp[5-6]* | wc -l`
echo "    NROFILES=${NROFILES}"
read -p "... Manual user intervention. Verify files before continuing. Press [Enter] key to continue processing..."

echo "    <...>cat neosmdw_weblogp5*.txt | sort > weblogp5_MDWOperations.out"
cat neosmdw_weblogp5*.txt | sort > weblogp5_MDWOperations.out
echo "    <...>cat neosmdw_weblogp6*.txt | sort > weblogp6_MDWOperations.out"
cat neosmdw_weblogp6*.txt | sort > weblogp6_MDWOperations.out
echo "    <...>cat neosweblogp5*.txt | sort > weblogp5_NEOSOperations.out"
cat neosweblogp5*.txt | sort > weblogp5_NEOSOperations.out
echo "    <...>cat neosweblogp6*.txt | sort > weblogp6_NEOSOperations.out"
cat neosweblogp6*.txt | sort > weblogp6_NEOSOperations.out

echo "... Perform processing and generate update files."
echo "    <...>awk -v SERVER=weblogp5 --file ${AWKDIR}/awkfilterMDWOperationsRebuild.awk weblogp5_MDWOperations.out | sort > updateRRDNEOSMDWOperationsEspecial_weblogp5.sh"
awk -v SERVER=weblogp5 --file ${AWKDIR}/awkfilterMDWOperationsRebuild.awk weblogp5_MDWOperations.out | sort > updateRRDNEOSMDWOperationsEspecial_weblogp5.sh
echo "    <...>awk -v SERVER=weblogp6 --file ${AWKDIR}/awkfilterMDWOperationsRebuild.awk weblogp6_MDWOperations.out | sort > updateRRDNEOSMDWOperationsEspecial_weblogp6.sh"
awk -v SERVER=weblogp6 --file ${AWKDIR}/awkfilterMDWOperationsRebuild.awk weblogp6_MDWOperations.out | sort > updateRRDNEOSMDWOperationsEspecial_weblogp6.sh
echo "    <...>awk --file /home/swfcfe/MonitoringProject/AWKFilters/awkfilter03.awk weblogp5_NEOSOperations.out | sort > updateRRDNEOSNEOSOperationsEspecial_weblogp5.sh"
awk --file /home/swfcfe/MonitoringProject/AWKFilters/awkfilter03.awk weblogp5_NEOSOperations.out  | sort > updateRRDNEOSNEOSOperationsEspecial_weblogp5.sh
echo "    <...>awk --file /home/swfcfe/MonitoringProject/AWKFilters/awkfilter04.awk weblogp6_NEOSOperations.out  | sort > updateRRDNEOSNEOSOperationsEspecial_weblogp6.sh"
awk --file /home/swfcfe/MonitoringProject/AWKFilters/awkfilter04.awk weblogp6_NEOSOperations.out  | sort > updateRRDNEOSNEOSOperationsEspecial_weblogp6.sh
read -p "... Manual user intervention. Verify results before going on. Press [Enter] key to continue processing..."

if [ "${UPDATESWITCH}" == "UPDATE" ]
then
	echo "... Update RRD databases. This takes time."
	echo "    <...>bash updateRRDNEOSMDWOperationsEspecial_weblogp5.sh"
	bash updateRRDNEOSMDWOperationsEspecial_weblogp5.sh
	echo "    <...>bash updateRRDNEOSMDWOperationsEspecial_weblogp6.sh"
	bash updateRRDNEOSMDWOperationsEspecial_weblogp6.sh
	echo "    <...>bash updateRRDNEOSNEOSOperationsEspecial_weblogp5.sh"
	bash updateRRDNEOSNEOSOperationsEspecial_weblogp5.sh
	echo "    <...>bash updateRRDNEOSNEOSOperationsEspecial_weblogp6.sh"
	bash updateRRDNEOSNEOSOperationsEspecial_weblogp6.sh
fi

read -p "... Manual user intervention. Checkpoint before packaging new databases. Press [Enter] key to continue processing..."

echo "... Package new databases and copy to production."
cd /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup

echo "    <...>tar cvfz neos_MDW${LASTYEAR}${LASTMONTH}.tar neos_weblogp[5-6]_MDW*"
tar cvfz neos_MDW${LASTYEAR}${LASTMONTH}.tar neos_weblogp[5-6]_MDW*
echo "    <...>cp /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_MDW* /var/lib/cacti/rra"
cp /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_MDW* /var/lib/cacti/rra
echo "    <...>sudo chown www-data neos_weblogp[5-6]_MDW* ; sudo chgrp  www-data neos_weblogp[5-6]_MDW* ; sudo chmod 666 neos_weblogp[5-6]_MDW*"
sudo chown www-data neos_weblogp[5-6]_MDW* ; sudo chgrp  www-data neos_weblogp[5-6]_MDW* ; sudo chmod 666 neos_weblogp[5-6]_MDW*

echo "    <...>tar cvfz neos_NEOS${LASTYEAR}${LASTMONTH}.tar neos_weblogp[5-6]_NEOS*"
tar cvfz neos_NEOS${LASTYEAR}${LASTMONTH}.tar neos_weblogp[5-6]_NEOS*
echo "    <...>cp /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_NEOS* /var/lib/cacti/rra"
cp /home/swfcfe/MonitoringProject/SQLData/CactiDatabasesBackup/neos_weblogp[5-6]_NEOS* /var/lib/cacti/rra
echo "    <...>sudo chown www-data neos_weblogp[5-6]_NEOS* ; sudo chgrp  www-data neos_weblogp[5-6]_NEOS* ; sudo chmod 666 neos_weblogp[5-6]_NEOS*"
sudo chown www-data neos_weblogp[5-6]_NEOS* ; sudo chgrp  www-data neos_weblogp[5-6]_NEOS* ; sudo chmod 666 neos_weblogp[5-6]_NEOS*

echo "... END OF PROCESS. `date +%F` `date +%H:%M:%S`"
} 2>&1 | tee -a $LOGFILE
exit 0


