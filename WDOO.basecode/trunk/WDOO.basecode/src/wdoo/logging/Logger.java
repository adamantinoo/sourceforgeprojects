//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: Logger.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/logging/Attic/Logger.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  TO DO:
//    YYMMDD (INIT) - <<comment>>
//  LOG:
//    $Log: Logger.java,v $
//    Revision 1.1.2.2  2005/06/15 13:27:04  ldiego
//    - Changed the package destination for this class.
//
//    Revision 1.1.2.1  2005/05/06 12:56:43  ldiego
//    - Class to keep logging functionalities that may be found on release 1.4.
//

package wdoo.logging;

//... IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;
//import java.util.logging.Logger;


//... CLASS IMPLEMENTATION ...................................................................................
public class Logger extends java.util.logging.Logger {
	public static final String DBL_RULE = "================================================================================";
	public static final String RULE = "--------------------------------------------------------------------------------";
	/**
	 * @param arg0
	 * @param arg1
	 */
	protected Logger(String arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}
	static public java.util.logging.Logger getLogger(String path) {
		Logger log = new Logger(path,null);
		return log;
	}
	public void header(String message) {
		super.info(message);
	}

//	private String				path;
//	private int					filterLevel = 3;	// FINE level
//	private WdooFormater	formatter	= new WdooFormater();
/*
	public Logger(String path) {
		// TODO Auto-generated constructor stub
		this.path = path;
	}*/
/*	public static java.util.logging.Logger getLogger(String path) {
		// TODO Auto-generated method stub
		return new Logger(path, path);
	}*/
/*
	public void log(String level, String message) {
		//TODO Write to the selected file path instead to the System output as this implementation does
		LogRecord record = new LogRecord("-NDF-", "-NDF-");
		record.setMessage(message);
		record.setLevel(level);
		System.out.print(this.formatter.format(record));
	}
	public void log(String level, LogRecord record) {
		record.setLevel(level);
		System.out.print(this.formatter.format(record));
	}
	public void info(String message) {
		if (this.filterLevel >= 2) this.log(Level.INFO, message);
	}
	public void fine(String message) {
		if (this.filterLevel >= 3)this.log(Level.FINE, message);
	}
	public void finer(String message) {
		if (this.filterLevel >= 4)this.log(Level.FINER, message);
	}
	public void entering(String className, String methodName) {
		LogRecord record = new LogRecord(className, methodName);
		if (this.filterLevel >= 5)this.log(Level.ENTER, record);
	}
	public void exiting(String className, String methodName) {
		LogRecord record = new LogRecord(className, methodName);
		if (this.filterLevel >=+ 5)this.log(Level.EXIT, record);
	}
	public void throwing(String className, String method, Exception exception) {
		//TODO Write to the selected file path instead to the System output as this implementation does
		LogRecord record = new LogRecord(className, method);
		record.setMessage(exception.getMessage());
		this.log(Level.ERROR, record);
		exception.printStackTrace(System.out);
	}*/
	public void exit(int exitCode, String message) {
		this.log(Level.SEVERE, message);
	}

	private class WdooFormater {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
		 */
		public String format(LogRecord logrec) {
			//... Compose the log message to be displayed on the console.
			//		The message format will be like this
			// TIMESTAMP THREADID LEVEL >>>/<<</ SOURCECLASSNAME SOURCEMETHODNAME - PARAMETERS MESSAGE
			StringBuffer outMessage = new StringBuffer();

			Date timeStamp = new Date(logrec.getMillis());
			Object[] params = logrec.getParameters();
			StringBuffer paramString = new StringBuffer();
			if (params != null) {
				int paramsSize = params.length;
				for (int i = 0; i < params.length; i++) {
					paramString.append(params[i].toString());
				}
			}
			outMessage.append(timeStamp.toString()).append(" ");
			outMessage.append(logrec.getThreadID()).append(" ").append(
					logrec.getLevel().getLocalizedName()).append(" - ");
			outMessage.append(logrec.getSourceClassName()).append(" ").append(
					logrec.getSourceMethodName()).append(" - ");
			outMessage.append(paramString).append(" ").append(logrec.getMessage());
			outMessage.append('\n');

			return outMessage.toString();
		}

	}
}

//... UNUSED CODE
// ............................................................................................
