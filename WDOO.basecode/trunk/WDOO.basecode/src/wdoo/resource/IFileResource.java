// PROJECT: WDOO.basecode
//  FILE NAME: $RCSfile: ProcessorApp.java,v $
//  FILE PATH: $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE: $Date$
//  RELEASE: $Revision$
//  AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER: $Author$
//  COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.resource;

//... IMPORT SECTION ...................................................................................................
import java.util.List;


import java.rmi.activation.ActivateFailedException;


import java.io.IOException;



/**
 * @author ldiego
 */

public interface IFileResource extends IResource {

	/**
	 * Sets the folder location where this resource should be stored.
	 */
	void setLocation(String path);

	/**
	 * Sets the name of the file that will store the resource contents. This method can be only called for new resources and if
	 * the file currently exists it will raise a ResourceException.
	 */
	void setName(String name) throws ResourceException;

	void setExtension(String extension) throws ResourceException;

	/**
	 * Sets the contents of the file to the string receoved as the parameter. If the resource is not initialized or not exists
	 * then a ResourceException is raised. If the filesystems can not perform the operation then an IOException is reported.
	 */
	void setContents(String contents) throws ResourceException, IOException;
	/**
	 * Adds the string to the contents of the file to the string receoved as the parameter. If the resource is not initialized
	 * or not exists then a ResourceException is raised. If the filesystems can not perform the operation then an IOException
	 * is reported.
	 */
	void appendContents(String contents) throws ResourceException, IOException;

}

//... UNUSED CODE ......................................................................................................
