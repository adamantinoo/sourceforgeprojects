// PROJECT: WDOO.basecode
//  FILE NAME: $RCSfile: ProcessorApp.java,v $
//  FILE PATH: $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE: $Date$
//  RELEASE: $Revision$
//  AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER: $Author$
//  COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.resource;

//... IMPORT SECTION ...................................................................................................
import java.util.Collection;

import java.util.Iterator;

import java.util.HashSet;
import java.util.List;

/**
 * @author ldiego
 */
public class ResourceManagerDraft {
	protected HashSet	resourceSets	= new HashSet();

	public IResource createResource(Class type) {
		//TODO Add the code to create a new resource depending on the selected type
		if (type == IFileResource.class) {
			//TODO Create a new file resource that is still not bound to a filesystem instance
		}
		return null;
	}

	public IResource getResource(Class type, Object reference) {
		//TODO Implement a resource lookup function to get the resource fomr the resource list that corresponds to the class
		return null;
	}
}
//... UNUSED CODE ......................................................................................................
