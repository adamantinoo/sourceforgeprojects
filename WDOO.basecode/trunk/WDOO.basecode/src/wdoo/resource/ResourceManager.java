//  PROJECT:        CONVERGENCIA.extraccion
//  FILE NAME:      $RCSfile: ResourceManager.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/projects/CONVERGENCIA.extraccion/extractorconsumos/src/wdoo/morgane/extractorconsumos/resources/ResourceManager.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ResourceManager.java,v $
//    Revision 1.3  2005/07/06 07:26:55  ldiego
//    - Adaptacion a los nuevos nombres de ficheros de propiedades.
//
//    Revision 1.2  2005/05/05 16:16:21  ldiego
//    - Cambio del path del fichero de recursos.
//
//    Revision 1.1  2005/05/04 14:28:08  ldiego
//    - Externalizacion de las strings y creacion de una nueva clase para procesar los
//      recursos externalizados.
//

package wdoo.resource;

//... IMPORT SECTION .........................................................................................
import java.util.MissingResourceException;
import java.util.ResourceBundle;

//... CLASS IMPLEMENTATION ...................................................................................
public class ResourceManager {
	private static String					BUNDLE_NAME			= "wdoo.morgane.extractorconsumos.resources.extractorconsumos";	//$NON-NLS-1$
	private static ResourceBundle	RESOURCE_BUNDLE	= ResourceBundle.getBundle(BUNDLE_NAME);

	private ResourceManager() {
	}
	public static void initialize(String bundleName) {
	BUNDLE_NAME = bundleName;
	RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);}

	public static String getResource(String key) {
		// TODO Auto-generated method stub
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}

//... UNUSED CODE ............................................................................................
