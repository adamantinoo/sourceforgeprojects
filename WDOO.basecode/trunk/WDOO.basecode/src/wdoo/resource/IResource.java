//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.resource;

//... IMPORT SECTION ...................................................................................................
import java.util.List;

/**
 * @author ldiego
 */
public interface IResource {
	/**
	 * Type constant (bit mask value 1) which identifies file resources.
	 */
	public static final int FILE = 0x01;
	/**
	 * Type constant (bit mask value 2) which identifies folder resources.
	 */
	public static final int FOLDER = 0x02;
	/**
	 * Type constant (bit mask value 4) which identifies fragment resources that are to be kept in memory as a virtual storage system.
	 */
	public static final int MEMORY = 0x03;

	/**
	 * Returns whether this resource exists in the resource set.
	 */
	boolean exist();

	/**
	 * Deletes this resource from the resource set.
	 */
	void delete(boolean force);

	/**
	 * Returns the file extension portion of this resource's name, or null if it does not have one.
	 */
	String getFileExtension();

	/**
	 * Returns the full, absolute path of this resource relative to the storage root.
	 */
	String getFullPath();

	/**
	 * Returns the name of this resource.
	 */
	String getName();

	/**
	 * Returns the type of this resource.
	 */
	int getType();


}

//... UNUSED CODE ......................................................................................................
