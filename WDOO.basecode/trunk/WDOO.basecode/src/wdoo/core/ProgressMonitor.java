//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: ProgressMonitor.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/core/ProgressMonitor.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//	TO DO:
//		050425 (LDDT) - Add some capability to determinate when the estimations are valid or just we have not
//										data to make them valuable.
//		050506 (LDDT) - Create a new structure where to store the estimated time in seconds into the division
//										of days, hours, minutes and seconds.
//  LOG:
//    $Log: ProgressMonitor.java,v $
//    Revision 1.1  2005/05/06 12:45:43  ldiego
//    - New classes to help in command line applications progress indication.
//

package wdoo.core;

//... IMPORT SECTION .........................................................................................
import java.util.Date;
import java.util.List;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * @author Luis de Diego
 */
public class ProgressMonitor implements IProgressMonitor {

	private String	taskName = "Default Task";
	private long		workSteps = 1;
	private long		workCompleted = 0;
	private long		elapsedTime = 0;

	/**
	 * @param taskName
	 * @param steps
	 */
	public void beginTask(String taskName, long steps) {
		// TODO Auto-generated method stub
		this.taskName = taskName;
		this.workSteps = steps;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see wdoo.core.IProgressMonitor#worked(long, long)
	 */
	public void worked(long steps, long elapsed) {
		// TODO Auto-generated method stub
		this.workCompleted += steps;
		this.elapsedTime += elapsed;
	}
	/**
	 * Returns the estimated number of seconds that are expected to finish the job. The -1 result is used to
	 * show that thre is no enought information to make an estimation and thus the end time is infinite.
	 * 
	 * @return the estimated number of seconds that are expected to finish the job.
	 */
	public long estimatedTime() {
		//... Calculate the estimated end date based on current elepsed time and job completed. If current job
		//		completed is CERO then estimates to infinite.
		if ((0 < this.workCompleted) && (this.workSteps >= this.workCompleted)) {
//			return (this.workSteps - this.workCompleted) / this.elapsedTime;
			return (this.workSteps - this.workCompleted)*(this.elapsedTime/this.workCompleted)/1000;
		} else
			return -1;
	}
	public String estimatedDays() {
		//... Calculate the estimated end date based on current elepsed time and job completed. If current job
		//		completed is CERO then estimates to infinite.
		if ((0 < this.workCompleted) && (this.workSteps >= this.workCompleted)) {
			double days = 0;
			long seconds = (this.workSteps - this.workCompleted)
					* (this.elapsedTime / this.workCompleted);
			if (seconds > 86400) {
				days = Math.floor(seconds);
				seconds-=86400*days;
			}
			double hours=Math.floor(seconds/3600);
			seconds-=3600*hours;
			double minutes=Math.floor(seconds/60);
			StringBuffer buffer = new StringBuffer();
			buffer.append(days).append(" dias ");
			buffer.append(hours).append(" horas ");
			buffer.append(Math.floor(seconds/3600)).append(" horas ").append(minutes).append(" minutos");
			return buffer.toString();
		} else
			return "no data";
	}
}

//... UNUSED CODE ............................................................................................
