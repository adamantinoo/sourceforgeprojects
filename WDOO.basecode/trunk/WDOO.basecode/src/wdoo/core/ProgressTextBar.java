//	PROJECT:        PersistenceFramework
//  FILE NAME:      $RCSfile: ProgressTextBar.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/core/ProgressTextBar.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProgressTextBar.java,v $
//    Revision 1.1  2005/05/06 12:45:43  ldiego
//    - New classes to help in command line applications progress indication.
//

package wdoo.core;

import org.eclipse.core.runtime.IProgressMonitor;
//TODO This class has no implementation. Start the codification of a simple one.
/**
 * 
 * @author ldiego
 */
public class ProgressTextBar implements IProgressMonitor {

	protected int steps = 1;
	protected int workDone = 0;
	private long	stepsLong;
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#beginTask(java.lang.String, int)
	 */
	public void beginTask(String taskMessage, long steps) {
		this.setTaskName(taskMessage);
		this.stepsLong = steps;
	}
		public void beginTask(String taskMessage, int steps) {
		this.steps = steps;
		// TODO Auto-generated method stub
		System.out.print('\n');
		System.out.print(taskMessage);
		System.out.print('\n');
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#done()
	 */
	public void done() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#internalWorked(double)
	 */
	public void internalWorked(double arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#isCanceled()
	 */
	public boolean isCanceled() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#setCanceled(boolean)
	 */
	public void setCanceled(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#setTaskName(java.lang.String)
	 */
	public void setTaskName(String arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#subTask(java.lang.String)
	 */
	public void subTask(String subtaskMessage) {
		// TODO Auto-generated method stub
		System.out.print('\n');
		System.out.print(subtaskMessage);
		System.out.print('\n');;
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.IProgressMonitor#worked(int)
	 */
	public void worked(int steps) {
		// TODO Auto-generated method stub
		this.workDone += steps;
		if (this.workDone > this.steps) this.workDone = this.steps;
		else System.out.print("*");
	}

}
