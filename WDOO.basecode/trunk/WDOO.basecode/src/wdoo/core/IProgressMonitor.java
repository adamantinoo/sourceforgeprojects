// PROJECT: WDOO.basecode
//  FILE NAME: $RCSfile: IProgressMonitor.java,v $
//  FILE PATH: $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/core/IProgressMonitor.java,v $
//  LAST UPDATE: $Date$
//  RELEASE: $Revision$
//  AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER: $Author$
//  COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: IProgressMonitor.java,v $
//    Revision 1.1  2005/05/06 12:45:43  ldiego
//    - New classes to help in command line applications progress indication.
//

package wdoo.core;

//... IMPORT SECTION
// .........................................................................................
import java.util.List;

//... CLASS IMPLEMENTATION
// ...................................................................................
/**
 * @author Luis de Diego
 */
public interface IProgressMonitor {

	/**
	 * @param steps
	 * @param elapsed
	 */
	void worked(long steps, long elapsed);
}

//... UNUSED CODE
// ............................................................................................
