package wdoo.net;

public class IPAddress {
  private byte[] address = new byte[4];

  public IPAddress(byte newIPAddress[]) {
    for (int i=0; i < newIPAddress.length; i++)
      address[i] = newIPAddress[i];
  }
  public String toString() {
    String result = "";
    for (int i=0; i < address.length; i++)
      result += (address[i]+256)%256 + ".";
    return result;
  }
}
