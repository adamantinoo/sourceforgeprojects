//  PROJECT:        CONVERGENCIA.extraccion
//  FILE NAME:      $RCSfile: ResourceManager.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/projects/CICLON/04.-\040Prototipo/src/wdoo/util/ResourceManager.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ResourceManager.java,v $
//    Revision 1.1  2006/03/02 13:56:20  ldiego
//    *** empty log message ***
//
//    Revision 1.1  2005/08/10 11:59:01  ldiego
//    - Nuevo desarrllo para procesar los ficheros de log de errores para identificar
//      los clientes rechazados durante la migracion.
//    - Adicion de un nuevo tipo de procesador para procesar los logs del ACS.
//
//    Revision 1.2  2005/05/05 16:16:21  ldiego
//    - Cambio del path del fichero de recursos.
//
//    Revision 1.1  2005/05/04 14:28:08  ldiego
//    - Externalizacion de las strings y creacion de una nueva clase para procesar los
//      recursos externalizados.
//

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.util.MissingResourceException;
import java.util.ResourceBundle;

//... CLASS IMPLEMENTATION ...................................................................................
public class ResourceManager {
	private String					bundleName	= null;
	private ResourceBundle	bundle;

	private ResourceManager(String bundleName) {
		this.bundleName = bundleName;
		bundle = ResourceBundle.getBundle(this.bundleName);
	}

	public String getResource(String key) {
		// ... Check we have an initialized object
		if (null != this.bundleName) {
			try {
				return this.bundle.getString(key);
			} catch (MissingResourceException e) {
				return '!' + key + '!';
			}
		}
		return "!NO BUNDLE DEFINED!";
	}

	public static ResourceManager getManager(String bundleName) {
		return new ResourceManager(bundleName);
	}
}

// ... UNUSED CODE
// ............................................................................................
