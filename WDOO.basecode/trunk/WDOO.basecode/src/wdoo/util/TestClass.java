//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/util/TestClass.java

package wdoo.util;


/**
 */
public class TestClass {
  
  public TestClass() {}
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1E250104
   */
  public void public_operation() {}
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1E3D020D
   */
  public static final native synchronized void public_operationFinalStaticSynchNative();
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1EB00154
   */
  protected void protected_operation() {}
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1EDA0078
   */
  protected static final native synchronized void protected_operationFSSN();
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1EF2027B
   */
  private void private_operation() {}
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1F150222
   */
  private static final native synchronized void private_operationFSSN();
}
