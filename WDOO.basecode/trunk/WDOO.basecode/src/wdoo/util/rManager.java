//  PROJECT:        PrototipoDigramaCicloFacturacion
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.util.MissingResourceException;
import java.util.ResourceBundle;

//... CLASS IMPLEMENTATION ...................................................................................
public class rManager {
	private static final String					BUNDLE_NAME			= "wdoo.ciclon.diagramaciclo.app.resources.ProcessDollarUChainsApp";	//$NON-NLS-1$

	private static final ResourceBundle	RESOURCE_BUNDLE	= ResourceBundle.getBundle(BUNDLE_NAME);

	private rManager() {
	}

	public static String getResource(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}

//... UNUSED CODE ............................................................................................
