//  PROJECT: CONVERGENCIA.extraccion
//	FILE NAME: $RCSfile: SQLResource.java,v $
//	FILE PATH: $Source: /apps/cvsrepository/projects/CONVERGENCIA.extraccion/extractorconsumos/src/wdoo/morgane/resources/SQLResource.java,v $
//	LAST UPDATE: $Date: 2005/07/06 07:26:31 $
//	RELEASE: $Revision: 1.4 $
//	AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//	LAST USER: $Author: ldiego $
//	COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//	TO DO:
//		050414 (LDDT) -	Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no existe
//										o no es accesible o esta vacio (este caso no creo que sea detectable con facilidad).
//		050506 (LDDT) - Es necesaria la configuracion de un nuevo parametro de operacion que indique
//										la localizacion de la carpeta de recursos.
//	LOG:
//		$Log: SQLResource.java,v $
//		Revision 1.4  2005/07/06 07:26:31  ldiego
//		- Se ha incluido el codigo para configurar el directorio del que proceden los
//		  recursos de codigo y se ha a�adido un nuevo metodo <<setPrefix>> para
//		  configurar este atributo.
//		
//		Revision 1.3  2005/05/09 10:25:26  ldiego
//		- Cambio del procedimiento de lectura de los ficheros SQL de recurso debido
//		  a un error de localizacion en la plataforma UNIX. Los recursos se han trasladado
//		  a una nueva carpeta al primer nivel. Ahora la localizacion de esa carpeta
//		  esta internamente codificada en la clase.
//		- Eliminado el codigo obsoleto.
//		
//		Revision 1.2  2005/05/04 14:30:29  ldiego
//		- Cambios de package.
//		
//		Revision 1.1  2005/04/14 16:39:02  ldiego
//		- Adicion de nuevos campos para completar los datos necesarios para presentar
//		  los datos. Los nuevos campos no se encuentran en los requerimientos. Se
//		  a�ade un campo para el numero de cuenta (NROACCOUNT) y otro para el NIF del
//		  cliente (NIF).
//		- Externalizacion de las strings y marcacion de las no externalizables.
//		- Creacion de una nueva clase para la gestion de las string externalizadas.
//		  Creacion del fichero de properties.
//		- Extraer las consultar SQL como recursos para no tener que editar con comillas
//		  en el codigo fuente y mejorar el mantenimiento y la legibilidad del codigo.
//		  Permitir ademas la diferenciacion de consultas de pruebas de las consultas de
//		  produccion por medio de un flag de control externo.
//		- Adicion de los ficheros de recursos tanto de texto (usage) como del comando
//		  SQL que deber� ejecutarse.
//		- Adicion del fichero de recursos de externalizacion de strings.
//		- Adicion de nuevas clases para leer recursos externos que se componen de multiples
//		  lineas de texto.
//		- Adicion de los recursos de comando SQL GETCONSUMOS4MONTH y el texto de uso.
//		- Procesar los parametros de entrada para tener una version parametrizable y
//		  posiblemente que se pueda probar en pre-produccion a traves de los scripts
//		  shell adecuados.
//		- Los campos que se repiten por linea, como LOGIN, NROACCOUNT y NIF agruparlos
//		  en la linea de separacion de cuentas, manteniendo los registro de longitud fija.
//		

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * @author Luis de Diego
 */
public class SQLResourceManager {
	//... CLASS PRIVATE CONSTANT FIELDS. CONSTANTS
	private static final String		RESOURCE_PREFIX				 	= "/sqlresource."; 	//$NON-NLS-1$
	private static final String		RESOURCE_FILEEXTENSION	= ".sql";							//$NON-NLS-1$
	private static final String		EMPTY_STATEMENT					= "SELECT SYSDATE FROM DUAL";
	
	private static String prefix = RESOURCE_PREFIX;

//... CLASS CONSTRUCTORS .....................................................................................

//... PUBLIC METHODS .........................................................................................
	public static SQLResourceManager getManager(String resourcePath) {
		String lineBuffer;
		try {
			//... Open the resource bundle and check it exists.
			FileInputStream stream = new FileInputStream(new File(resourcePath));
			if (stream != null) {
				BufferedReader input = new BufferedReader(new InputStreamReader(stream));
				//... Read the resource data.
				while ((lineBuffer = input.readLine()) != null) {
					resourceBuffer.append(" " + lineBuffer);
				}
				input.close();
			}
		} catch (NullPointerException npe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			npe.printStackTrace();
			return EMPTY_STATEMENT;
		} catch (FileNotFoundException fnfe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			fnfe.printStackTrace();
			return EMPTY_STATEMENT;
		} catch (IOException ioe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			ioe.printStackTrace();
			return EMPTY_STATEMENT;
		}
		return getMana(resourcePath);
	}
	public static String getResource(String name) {
		String lineBuffer;
		StringBuffer resourceBuffer = new StringBuffer();
		try {
			//... Open the resource bundle and check it exists.
			FileInputStream stream = new FileInputStream(new File(prefix + RESOURCE_PREFIX + name
					+ RESOURCE_FILEEXTENSION));
			if (stream != null) {
				BufferedReader input = new BufferedReader(new InputStreamReader(stream));
				//... Read the resource data.
				while ((lineBuffer = input.readLine()) != null) {
					resourceBuffer.append(" " + lineBuffer);
				}
				input.close();
			}
		} catch (NullPointerException npe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			npe.printStackTrace();
			return EMPTY_STATEMENT;
		} catch (FileNotFoundException fnfe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			fnfe.printStackTrace();
			return EMPTY_STATEMENT;
		} catch (IOException ioe) {
			//TODO Generar un tipo de excepcion similar a NullPointerException para indicar que el recurso no
			// existe
			ioe.printStackTrace();
			return EMPTY_STATEMENT;
		}
		return resourceBuffer.toString();
	}
	public static void setPrefix(String onePrefix){
		prefix = onePrefix;
	}
}

//... UNUSED CODE ......................................................................................................
