//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/util/AbstractTestClass.java

package wdoo.util;


/**
 */
public abstract class AbstractTestClass {
  
  public AbstractTestClass() {}
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1F09021A
   */
  private abstract native synchronized void private_operationASN();
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1EC103E4
   */
  protected abstract native synchronized void protected_operationASN();
  
  /**
   * @return void
   * @exception 
   * @roseuid 409A1E5E0002
   */
  public abstract native synchronized void public_operationAbstractSynchNative();
}
