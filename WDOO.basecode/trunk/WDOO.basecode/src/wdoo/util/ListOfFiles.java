//  PROJECT:        PrototipoSeguimientoPlanificacion
//  FILE NAME:      $RCSfile: ListOfFiles.java,v $
//  FILE PATH:      $Source: /apps/cvsrepository/projects/CICLON/04.-\040Prototipo/src/wdoo/util/ListOfFiles.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ListOfFiles.java,v $
//    Revision 1.1  2006/03/02 13:56:20  ldiego
//    *** empty log message ***
//

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * @author Luis de Diego
 * 
 */
public class ListOfFiles implements Enumeration {

	private File[]	listOfFiles;
	private int			current	= 0;

	public ListOfFiles(File[] listOfFiles) {
		this.listOfFiles = listOfFiles;
	}

	public boolean hasMoreElements() {
		if (current < listOfFiles.length)
			return true;
		else
			return false;
	}

	public Object nextElement() {
		InputStream in = null;

		if (!hasMoreElements())
			throw new NoSuchElementException("No more files."); //$NON-NLS-1$
		else {
			File nextElement = listOfFiles[current];
			current++;
			try {
				in = new FileInputStream(nextElement);
			} catch (FileNotFoundException e) {
				// TODO Review error processing on this new classes
				System.err.println("ListOfFiles: Can't open " + nextElement); //$NON-NLS-1$
			}
		}
		return in;
	}
}

// ... UNUSED CODE
// ............................................................................................
