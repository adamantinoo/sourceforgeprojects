//  PROJECT:        PrototipoDigramaCicloFacturacion
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.util.Calendar;
import java.util.Date;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * This class receives a date in some vaid UNIX format and determines the type of the date and then creates 
 * a date object with the corresponding date format.
 * Supported file dates are in this list:
 * Dec  3  2004 
 * Dec 14  2004 
 * Apr  3 09:34
 * Mar 31 09:31
 * There are cases where the month and date are separated by more than one space

 * @author Luis de Diego
 *
 */
public class NTDateProcessor {

	public static Date convert2Date(String sourceDate) {
		String[] compositionFields = sourceDate.split(" ");
		String[] dateFields = compositionFields[0].split("/");
		String[] timeFields = compositionFields[1].split(":");
		String monthString = dateFields[1];
		String dayString = dateFields[0];
		String yearString = dateFields[2];
		int year = Integer.parseInt(yearString);
		if (year < 100) year+=2000;
		int hour = Integer.parseInt(timeFields[0]);
		int minute = Integer.parseInt(timeFields[1]);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, Integer.parseInt(monthString) - 1);
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayString));
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}
}

//... UNUSED CODE ............................................................................................
