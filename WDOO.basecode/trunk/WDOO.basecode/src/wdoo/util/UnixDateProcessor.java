//  PROJECT:        PrototipoDigramaCicloFacturacion
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.util;

//... IMPORT SECTION .........................................................................................
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * This class receives a date in some vaid UNIX format and determines the type of the date and then creates 
 * a date object with the corresponding date format.
 * Supported file dates are in this list:
 * Dec  3  2004 
 * Dec 14  2004 
 * Apr  3 09:34
 * Mar 31 09:31
 * There are cases where the month and date are separated by more than one space

 * @author Luis de Diego
 *
 */
public class UnixDateProcessor {
	private String	sourceDate;
	private boolean	validated	= false;
	private Date	actionDate;

	public UnixDateProcessor(String dateString) {
		this.sourceDate = dateString;
	}

	public boolean validate() {
		if (!validated) {
			//... Start processing by splitting the fields that form the date
			String[] dateFields = this.sourceDate.split(" ");
			//			String[] timeFields = dateFields[dateFields.length - 1].split(":");

			//... There can be some fields with empty values. I have to skip them or to remove them
			String monthString = dateFields[0];
			String dayString = null;
			if (0 == dateFields[1].length())
				dayString = dateFields[2];
			else
				dayString = dateFields[1];

			//... Check the format for the last field. If there is no : character then it is a year. Otherwise
			//		it is the hour and minute
			int hour;
			int minute;
			int year;
			Calendar cal = Calendar.getInstance();
			if (-1 == dateFields[dateFields.length - 1].indexOf(':')) {
				year = Integer.parseInt(dateFields[dateFields.length - 1]);
				hour = 0;
				minute = 0;
			} else {
				String[] timeFields = dateFields[dateFields.length - 1].split(":");
				year = cal.get(Calendar.YEAR);
				Calendar now = Calendar.getInstance();
				if (now.get(Calendar.MONTH) < cal.get(Calendar.MONTH))
					cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1);
				hour = Integer.parseInt(timeFields[0]);
				minute = Integer.parseInt(timeFields[1]);
			}

			Date partialDate = new Date();
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
			try {
				partialDate = df.parse(monthString + " " + dayString + ", " + year);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cal.setTime(partialDate);
			//... Check that the year matches this year. Detect this by testing the month against the current month
			int dateMonth = cal.get(Calendar.MONTH);
			int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
			if (dateMonth > currentMonth) cal.set(Calendar.YEAR, year - 1);
			//... Detect dates that are from the previous year. They are detected because the month does not
			//		match with the possible month of this year

			cal.set(Calendar.HOUR, hour);
			cal.set(Calendar.MINUTE, minute);
			cal.set(Calendar.SECOND, 0);
			this.actionDate = cal.getTime();
			this.validated=true;
			return true;
		} else
		return false;
	}

	public static Date convert2Date(String sourceDate) {
		//... Start processing by splitting the fields that form the date
		String[] dateFields = sourceDate.split(" ");
		//			String[] timeFields = dateFields[dateFields.length - 1].split(":");

		//... There can be some fields with empty values. I have to skip them or to remove them
		String monthString = dateFields[0];
		String dayString = null;
		if (0 == dateFields[1].length())
			dayString = dateFields[2];
		else
			dayString = dateFields[1];

		//... Check the format for the last field. If there is no : character then it is a year. Otherwise
		//		it is the hour and minute
		int hour;
		int minute;
		int year;
		Calendar cal = Calendar.getInstance();
		if (-1 == dateFields[dateFields.length - 1].indexOf(':')) {
			year = Integer.parseInt(dateFields[dateFields.length - 1]);
			hour = 0;
			minute = 0;
		} else {
			String[] timeFields = dateFields[dateFields.length - 1].split(":");
			year = cal.get(Calendar.YEAR);
			Calendar now = Calendar.getInstance();
			if (now.get(Calendar.MONTH) < cal.get(Calendar.MONTH))
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1);
			hour = Integer.parseInt(timeFields[0]);
			minute = Integer.parseInt(timeFields[1]);
		}

		Date partialDate = new Date();
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.ENGLISH);
		try {
			partialDate = df.parse(monthString + " " + dayString + ", " + year);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			partialDate= new Date();
		}
		cal.setTime(partialDate);
		//... Check that the year matches this year. Detect this by testing the month against the current month
		int dateMonth = cal.get(Calendar.MONTH);
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
		if (dateMonth > currentMonth) cal.set(Calendar.YEAR, year - 1);
		//... Detect dates that are from the previous year. They are detected because the month does not
		//		match with the possible month of this year

		cal.set(Calendar.HOUR, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		return cal.getTime();
	}
}

//... UNUSED CODE ............................................................................................
