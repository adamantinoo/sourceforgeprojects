//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/IQueue.java

package wdoo.queue;

import java.util.ListIterator;

import wdoo.thread.ThreadStatusException;


public interface IQueue {
  public static final int ACTIVE = 1;
	public static final int CLOSING = 2;
	public static final int CLOSED = 3;
	public static final int FULL = 4;
	public static final int EMPTY = 5;
  public static final int ERROR = 9;

	/**
	 * @param newStatus
	 * @return void
	 * @exception 
	 * Sets the value for the queue status. For most queues this is not used but some of them may require some asynchronous mechanism to inform clients.
	 * @roseuid 407D6C40018B
	 * 
	 * @uml.property name="state"
	 */
	public void setState(int newState);

	/**
	 * @return int
	 * @exception 
	 * Returns the current queue status. For most queues this is always ACTIVE.
	 * @roseuid 407D6C4901CA
	 * 
	 * @uml.property name="state"
	 */
	public int getState();

  
  /**
   * @return void
   * @exception 
   * @roseuid 407E50D302B8
   */
  public void queue( Object element ) throws ThreadStatusException;
  
  /**
   * @return void
   * @exception 
   * @roseuid 407E50D9032F
   */
  public Object dequeue() throws ThreadStatusException;
  
  /**
   * @return void
   * @exception 
   * @roseuid 407E50EE028F
   */
	public void close();

	/**
	 * @return
	 */
	public ListIterator listIterator();

	/**
	 * @return
	 */
	public long size();

	/**
	 * @return
	 */
	public boolean isEmpty();

	/**
	 * @param state
	 * @return The string that corresponds to the state code received as a parameter.
	 */
//	public String decodeState(int state);
}
