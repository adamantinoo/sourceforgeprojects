//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/LIFOQueue.java

package wdoo.queue;


/**
 */
public class LIFOQueue implements IQueue {
  
	/**
	 * This is the field that must have all classes to store the queue stae.
	 */
	private int status = ACTIVE;
  
  public LIFOQueue() {}
  
	public void setStatus(int newStatus) { this.status = newStatus; }
	public int getStatus() { return this.status; }
}
