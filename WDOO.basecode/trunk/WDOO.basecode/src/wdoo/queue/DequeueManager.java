//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/DequeueManager.java

package wdoo.queue;


/**
 */
public interface DequeueManager {

	/**
	 * @param element
	 * @return
	 */
	public boolean check( ITimedObject element );
}
