//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/TimedQueue.java
//FILE NAME:      $RCSfile: TimedQueue.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/TimedQueue.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: TimedQueue.java,v $
//Revision 1.1  2004/04/27 16:27:37  ldiego
//- Base release of the source file. Added to repository to start fixing classes being used at the project.
//

package wdoo.queue;

import java.util.ListIterator;

import wdoo.thread.ThreadStatusException;

/**
 * This queue version implements a queue that used a paramrizer class to determinate when a element may be dequeued. It internally contains another standard queue received as a parameter that contains the elements. * At the dequeue() moment the customization class will decide which element may be dequeued is any.
 */

public class TimedQueue implements IQueue {

	/**
	 * This is the field that must have all classes to store the queue state.
	 * 
	 * @uml.property name="theQueue"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	//  private int status = ACTIVE;
	private IQueue theQueue;

	/**
	 * 
	 * @uml.property name="theDequeuer"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private DequeueManager theDequeuer;

  
  /**
   * @param queue
   * @param dequeuer
   * @return 
   * @exception
   * @roseuid 407CF64A0002
   */
  public TimedQueue( IQueue newQueue,  DequeueManager newDequeuer) {
  	this.theQueue = newQueue;
  	this.theDequeuer = newDequeuer;
  }
  
  /**
   * @param newStatus
   * @return void
   * @exception
   * @roseuid 407D70D70145
   */
  public synchronized void setState( int newState ) { this.theQueue.setState( newState ); }
  
  /**
   * @return int
   * @exception 
   * @roseuid 408E81340317
   */
  public synchronized int getState() { return this.theQueue.getState(); }
  
  /**
   * @param element
   * @return void
   * @exception 
   * @roseuid 408E82C4025E
   */
  public synchronized void queue( Object element ) throws ThreadStatusException {
		this.theQueue.queue( element );
  }
  
  /**
   * @return Object
   * @exception 
   * @roseuid 408E82C4033A
   */
  public synchronized Object dequeue() throws ThreadStatusException {
  	//... The order of dequeing of object will be imposed by the native queue and by the behaviour
  	//		of the DequeueManager. Iterate over the elements one by one until one is found or the queue
  	//		gets exhausted.
  	ListIterator key = this.theQueue.listIterator();
  	while( key.hasNext() ) {
  		ITimedObject element = (ITimedObject)key.next();
  		if ( this.theDequeuer.check( element ) ) {
  			//... The dequeuer has determined that this element may be dequeued. Remove it from the
  			//		queue and return it to the caller.
  			key.remove();
  			return element;
  		}
  	}
		//... If it reaches this point the queue is empty or no element may be dequeued. Return an empty element.
		return null;
		//... If it reaches this point the queue is empty or no element may be dequeued. Throw the
  	//		corresponding exception as if the queue is empty.
//  	throw new NoSuchElementException( "The queue has no elements or none can be dequeued now." );
  }
  
  /**
   * @return void
   * @exception 
   * @roseuid 408E82C403A9
   */
  public synchronized void close() {
  	this.theQueue.close();
  }
  
	public synchronized long size() {
		return this.theQueue.size();
	}

	public synchronized ListIterator listIterator() {
		return this.theQueue.listIterator();
	}

	public synchronized boolean isEmpty() {
		return this.theQueue.isEmpty();
	}
}
