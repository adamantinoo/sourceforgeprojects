//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/FIFOQueue.java
//FILE NAME:      $RCSfile: FIFOQueue.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/FIFOQueue.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: FIFOQueue.java,v $
//Revision 1.1  2004/04/27 14:59:18  ldiego
//- Base release of the source file. Added to repository to start fixing classes being used at the project.
//

package wdoo.queue;

import java.util.LinkedList;
import java.util.ListIterator;
//import java.util.logging.Logger;

import wdoo.thread.ThreadStatusException;


/**
 */
public class FIFOQueue implements IQueue {
  
//... INSTANCE PRIVATE FIELDS
//	private Logger				log = Logger.getLogger( "wdoo.morgane.tickets" );
  /**
	 * This is the field that must have all classes to store the queue state.
   */
  private int				state = IQueue.ACTIVE;
  private LinkedList theQueue = new LinkedList();
  
	/**
	 * @return 
	 * @exception
	 * @roseuid 407D70FC0094
	 */
	public FIFOQueue() {}

	/**
	 * @param newStatus
	 * @return void
	 * @exception
	 * @roseuid 407D70D60324
	 * 
	 * @uml.property name="state"
	 */
	public synchronized void setState(int newState) {
		this.state = newState;
	}

	/**
	 * @return int
	 * @exception 
	 * @roseuid 407D7422018D
	 * 
	 * @uml.property name="state"
	 */
	public synchronized int getState() {
		return this.state;
	}

  
  /**
   * @param element
   * @return void
   * @exception 
   * @roseuid 408E3B460044
   */
  public synchronized void queue( Object element ) throws ThreadStatusException {
//		log.fine( "queuing element = " + element.getClass() );
  	theQueue.addLast( element );
  }
  
  /**
   * @return Object
   * @exception 
   * @roseuid 408E3B46010D
   */
  public synchronized Object dequeue() throws ThreadStatusException {
//		log.fine( "dequeuing element = " + theQueue.getFirst().getClass() );
  	return theQueue.removeFirst();
  }
  
  /**
   * @return void
   * @exception 
   * @roseuid 408E3B460149
   */
  public synchronized void close() {
  	this.setState( IQueue.CLOSED );
  	this.theQueue.clear();
  }
  
	public synchronized ListIterator listIterator() {
		return this.theQueue.listIterator();
	}

	public synchronized long size() {
		return this.theQueue.size();
	}

	public synchronized boolean isEmpty() {
		return this.theQueue.isEmpty();
	}
}
