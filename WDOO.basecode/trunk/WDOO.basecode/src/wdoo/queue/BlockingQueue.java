//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/BlockingQueue.java
//FILE NAME:      $RCSfile: BlockingQueue.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/queue/BlockingQueue.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: BlockingQueue.java,v $
//Revision 1.1  2004/04/27 14:59:18  ldiego
//- Base release of the source file. Added to repository to start fixing classes being used at the project.
//

package wdoo.queue;

import java.util.ListIterator;

import wdoo.thread.ThreadStatusException;


public class BlockingQueue implements IQueue {

	/**
	 * This is the field that must have all classes to store the queue state.
	 * 
	 * @uml.property name="theQueue"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	//	private int		status = IQueue.ACTIVE;
	private IQueue theQueue;

//	private boolean	   closed = false;
//	private boolean	   reject_enqueue_requests = false;
	private int		     waitingThreads = 0;
  
  /**
   * @param newQueue
   * @return 
   * @exception
   * @roseuid 407D2D7401F3
   */
  public BlockingQueue( IQueue newQueue ) {
  	if ( newQueue == null ) this.theQueue = new FIFOQueue();
  	else this.theQueue = newQueue;
  }
    
  /**
   * @param newStatus
   * @return void
   * @exception
   * @roseuid 407D70D600F3
   */
  public synchronized void setState( int newStatus ) {
//		this.status = newStatus;
		this.theQueue.setState( newStatus );
	}
  
  /**
   * @return int
   * @exception
   * @roseuid 407D70D6022A
   */
  public synchronized int getState() { return this.theQueue.getState(); }
  
  /**
   * @param element
   * @return void
   * @exception 
   * @roseuid 408E6EFF0257
   */
  public synchronized void queue( Object element ) throws ThreadStatusException {
		if( ( this.getState() == IQueue.CLOSING ) || ( this.getState() == IQueue.CLOSED ) ) {
			throw new ThreadStatusException( "The queue can not accept more elements because is being closed." );
		} else {
			this.theQueue.queue( element );
			//... Send waiting threads a message to wake up and get the new element.
			notify();
		}
  }
  
  /**
   * @return Object
   * @exception 
   * @roseuid 408E6EFF0365
   */
  public synchronized Object dequeue() throws ThreadStatusException {
		if( ( this.getState() == IQueue.CLOSING ) || ( this.getState() == IQueue.CLOSED ) ) {
			throw new ThreadStatusException( "The queue can not accept more elements because is being closed." );
		} else {
//		try {	
			// If the queue is empty, wait. I've put the spin lock inside
			// an if so that the waiting_threads count doesn't jitter
			// while inside the spin lock. A thread is not considered to
			// be done waiting until it's actually acquired an element

			if( this.theQueue.size() <= 0 ) {
				++this.waitingThreads;
				while( this.theQueue.size() <= 0 ) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//... If thread is awaken by any meands. Check if the queue has closed, then if there are any
					//		new elements and otherwise sleep the thread again.
					if( this.getState() == IQueue.CLOSED ) {
						--this.waitingThreads;
						throw new ThreadStatusException( "The queue can not accept more elements because is being closed." );
					}
				}
				--this.waitingThreads;
			}
			return this.theQueue.dequeue();
		}
/*		}
		catch( NoSuchElementException e )	// Shouldn't happen
		{	throw new Error(
					"Internal error (com.holub.asynch.Blocking_queue)");
		}

  	return this.theQueue.dequeue();*/
  }
  
  /**
   * @return void
   * @exception 
   * @roseuid 408E6EFF03D3
   */
  public synchronized void close() {
		this.theQueue.close();
		notifyAll();
  }
  
	public synchronized long size() {
		return this.theQueue.size();
	}
  
  /**
   * @return boolean
   * @exception 
   * @roseuid 408E6F00008C
   */
  public synchronized boolean isEmpty() {
  	return this.theQueue.isEmpty();
  }

	/* (non-Javadoc)
	 * @see wdoo.queue.IQueue#listIterator()
	 */
	public synchronized ListIterator listIterator() {
		return this.theQueue.listIterator();
	}

	/**
	 * @return
	 */
	public synchronized int waitingThreads() {
		return this.waitingThreads;
	}
}
