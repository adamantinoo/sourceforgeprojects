//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/thread/QueuedThreadPoolConfig.java
//FILE NAME:      $RCSfile: QueuedThreadPoolConfig.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/thread/QueuedThreadPoolConfig.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: QueuedThreadPoolConfig.java,v $
//Revision 1.1  2005/04/29 12:11:07  ldiego
//- This class is being made obsolete by the new classes to read configuration data
//  from internal resource managers. Properties files are being deprecated. Stored
//  in the repository for backup purposes.
//

package wdoo.thread;

import java.util.prefs.Preferences;
import java.util.logging.Logger;

/**
 */
public class QueuedThreadPoolConfig {
  
  /**
   * Defaul max number of processing threads on a queue.
   */
  private static final long DEFAULT_MAXTHREADS = 16;
	private static Preferences localPrefs;
  
  public QueuedThreadPoolConfig() {
		localPrefs = Preferences.userNodeForPackage( QueuedThreadPoolConfig.class );
  }
  
  /**
   * @return long
   * @exception 
   * Return the max number of threads that are configured. This is a configuration limit with a default value.
   * @roseuid 408D24690144
   */
  public long getMaxThreads() {
		long value = DEFAULT_MAXTHREADS;
		try {
			value = localPrefs.getLong( "maxThreads", DEFAULT_MAXTHREADS );
		} catch ( IllegalStateException ise ) {
				Logger.getLogger( "wdoo.thread" ).throwing( "TICKETCONFIG", "GETWORKUNITGROUPFACTOR", ise );
		}
		return value;
  }
}
