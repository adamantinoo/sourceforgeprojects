//  PROJECT: WDOO.basecode
//	FILE NAME: $RCSfile: QueuedThreadPool.java,v $
//	FILE PATH: $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/thread/QueuedThreadPool.java,v $
//	LAST UPDATE: $Date$
//	RELEASE: $Revision$
//	AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//	LAST USER: $Author$
//	COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//	TO DO:
//		050420 (LDDT) - Optimize the use of the helper and configuration class QueuedThreadPoolConfig by externalizing
//										the parameters and converting them into configurable property values.
//		050421 (LDDT) - Dise�ar un nuevo modelo de proceso que permita adaptar al vuelo el numero
//										de threads en ejecucion, tanto hacia arriba como hacia abajo. El proceso no
//										tiene por que ser adaptativo ya que habitualmente se va a trabajar a tope
//										de rendimiento luego el nivel optimo de numero de threads sera el maximo.
//		050504 (LDDT) - Crear un nuevo metodo para mantener al menos un thread activo y adaptarlo al numero
//										de unidades pendientes dentro de los limites.
//	LOG:
//		$Log: QueuedThreadPool.java,v $
//		Revision 1.2  2005/04/20 14:29:50  ldiego
//		- The public class QueuedThreadPoolConfig is made internal and local to the
//		  QueuedThreadPool class.
//		- Removed most of the obsolete code and performed a full review of the code
//		  and the funcionalities.
//		- The number of initial threads is set to 1 and thre is no need for
//		  configuration on this aspect. We currently create the first thread at the
//		  class creation.
//		- Added a new constructor to the interface IThreadFactory that does not require
//		  to pass the IQueue because the funcionality is obtained from the
//		  QueuedThreadPool.
//		- Added code to control the number of threads still working and thus simplify
//		  the control when the queue has finished processing.
//		- Optimization of some of the meyhods.
//		- Added Javadoc documentation to some of the methods.
//		- Added new protected operations to manage the thread management from a single
//		  set of methods.
//		

package wdoo.thread;

//... IMPORT SECTION ...................................................................................................
import java.util.ListIterator;
import java.util.logging.Logger;

import wdoo.queue.BlockingQueue;
import wdoo.queue.IQueue;
//import wdoo.util.Logger;

//... CLASS IMPLEMENTATION .............................................................................................
public class QueuedThreadPool extends ThreadGroup implements IQueue {
  
	//... CLASS PRIVATE FIELDS
	protected static  int poolID = 1;

	//... INSTANCE PRIVATE FIELDS
	private static Logger					log = Logger.getLogger( "wdoo.thread" ); //$NON-NLS-1$
//	static {
//		log.setLevel( Level.FINER );
//	}
	/** BlockingQueue that strores the elements to be processed. THis type of queue is a wrpper around other types
	 * of queues that can be configured and used in this implementation.
	 */
	private BlockingQueue					theQueue;
	/** Factory for the thread instances required to process the elements in the queue. */
	private IThreadFactory					factory;
	/** Number of thread on the pool. The threads shown on this variable are all the threads created and recorded. */
	private int										poolSize = 0;
	/** Number of threads that are active processing any element of the queue. When a thread finishes its
	 * processing it will decrement this number before waiting for more work.
	 */
	private int										activeThreads = 0;
	private long       						maxThreads = PackagePreferences.getLong("QueuedThreadPool.0", DEFAULT_MAXTHREADS );
	/** Variable where to store any instantiation exception to be returned at other later calls to this class. */
	private InstantiationException	ie;

	private static final long	DEFAULT_MAXTHREADS	= 2;

//... CLASS CONSTRUCTORS ...............................................................................................
	public QueuedThreadPool(IThreadFactory newFactory, BlockingQueue newQueue) {
		super("THREADPOOL_" + poolID++); //$NON-NLS-1$
		log.entering("QueuedThreadPool", "CONSTRUCTOR"); //$NON-NLS-1$ //$NON-NLS-2$
		//... Load parameters into instance fields.
		this.theQueue = newQueue;
		this.factory = newFactory;
		log.fine("Numero maximo de threads del Pool: " + this.maxThreads);

		//...Create and start a single new thread. This thread may get locked to the initial empty queue.
		try {
			this.startThread();
		} catch (InstantiationException ie) {
			log.throwing("QueuedThreadPool", "CONSTRUCTOR", ie); //$NON-NLS-1$ //$NON-NLS-2$
			this.setState(IQueue.ERROR);
		}
		log.exiting("QueuedThreadPool", "CONSTRUCTOR"); //$NON-NLS-1$ //$NON-NLS-2$
	}

//... INTERFACE IMPLEMENTATION OPERATIONS ..............................................................................
//... IQUEUE
	/**
	 * Queues the Object action into the queue and checks if there are free threads to process this new element. If there
	 * are no free threads and we have not currently reached the maximun number of threads the function creates anew one.<br>
	 * If the queue is not open or has any error this is signaled throwing an exception.
	 * 
	 * @see wdoo.queue.IQueue#queue(java.lang.Object)
	 */
	public void queue(Object action) throws ThreadStatusException {
		log.entering("QueuedThreadPool", "queue"); //$NON-NLS-1$ //$NON-NLS-2$
		log.finer("action class = " + action.getClass()); //$NON-NLS-1$

		//... Get access to this class configuration properties.
//		QueuedThreadPoolConfig info = new QueuedThreadPoolConfig();
//		long maxThreads = info.getMaxThreads();
		int currentStatus = this.getState();

		//... Check current status before scheduling the operation
		if (currentStatus == IQueue.ACTIVE) {
			if ((this.poolSize < this.maxThreads) && (this.theQueue.waitingThreads() == 0)) {
				//... There is no free thread to dispatch the execution. Add one more to the pool
				synchronized (theQueue) {
					try {
						this.startThread();
					} catch (InstantiationException ie) {
						log.throwing("QueuedThreadPool", "queue", ie); //$NON-NLS-1$ //$NON-NLS-2$
						this.setState(IQueue.ERROR);
					}
//					this.theQueue.queue(action);
				} // END SYNCHRONIZED
//			} else
			}
			this.theQueue.queue(action);
		} else {
			ThreadStatusException tee = new ThreadStatusException(
					"The status of this thread factory does not allow more QUEUE operations."); //$NON-NLS-1$
			//V4 This line corresponds to release 1.4 of Java JRE
//			if (this.ie != null) tee.initCause(ie);
			throw tee;
		}
		log.exiting("QueuedThreadPool", "queue"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	/**
	 * Retrieves an element form the Queue. The element to be retireved depends on the type of the underliying queue
	 * (the first if it is a FIFO queue). The operation blocks on this call if the queue is empty and the waiting thread
	 * is awakened just in the moment a new element is queued.
	 * 
	 * @see wdoo.queue.IQueue#dequeue()
	 */
	public Object dequeue() throws ThreadStatusException {
		Object element = this.getQueue().dequeue();
		log.finer("action class = " + element.getClass()); //$NON-NLS-1$
		this.keepThreadsActive();
		return element;
	}
	/**
	 * Closes the queue so it cannot accept any more queue requests. This operation is promoted to the internal queue
	 * that will behave accordingly to its type. For blocking queues this will awake all waiting threads and disallow
	 * any further queueing or dequeing.
	 * 
	 * @see wdoo.queue.IQueue#close()
	 */
	public synchronized void close() {
		log.entering( "QueuedThreadPool", "close" ); //$NON-NLS-1$ //$NON-NLS-2$
		log.finer( "closing queue" ); //$NON-NLS-1$
		this.getQueue().close();
		log.exiting( "QueuedThreadPool", "close" ); //$NON-NLS-1$ //$NON-NLS-2$
	}
	public synchronized void setState( int newState ) {
		this.theQueue.setState( newState );
	}
	public synchronized int getState() {
		return this.getQueue().getState();
	}
	public synchronized ListIterator listIterator() {
		return this.theQueue.listIterator();
	}
	public synchronized long size() {
		return this.theQueue.size();
	}
	public synchronized boolean isEmpty() {
		if (!this.theQueue.isEmpty()) this.keepThreadsActive();
		return this.theQueue.isEmpty();
	}
	public String decodeState(int state) {
		switch (state) {
			case IQueue.ACTIVE:
				return "ACTIVE";
			case IQueue.CLOSED:
				return "CLOSED";
			case IQueue.CLOSING:
				return "CLOSING";
			case IQueue.EMPTY:
				return "EMPTY";
			case IQueue.ERROR:
				return "ERROR";
			case IQueue.FULL:
				return "FULL";
		}
		return "UNDEFINED";
	}

//... GETTER/SETTER PUBLIC OPERATIONS ..................................................................................
	/**
	 * @return Returns the maxThreads.
	 */
	public long getMaxThreads() {
		return maxThreads;
	}
	/**
	 * @param maxThreads The maxThreads to set.
	 */
	public void setMaxThreads(int maxThreads) {
		this.maxThreads = maxThreads;
	}
	public int getActive() {
		return this.activeThreads;
	}
	//TODO Check that this method can be made protected to hide the queue implementation internal to this class. Queue
	//		funcionality is exported through the IQueue interface implementation.
	public IQueue getQueue() {
		return this.theQueue;
	}
	protected IThreadFactory getFactory() {
		return factory;
	}

	//... PROTECTED OPERATIONS
	// ...................................................................................
	private void keepThreadsActive() {
		//TODO Check the number of elements waiting and the current number of threads to get the number of
		//		threads to be created.
		try {
			if (this.poolSize == 0) this.startThread();
			if ((this.poolSize < this.maxThreads) && (this.theQueue.size() > 0)) this.startThread();
		} catch (InstantiationException ie) {
			log.throwing("QueuedThreadPool", "keepThreadsActive", ie); //$NON-NLS-1$ //$NON-NLS-2$
			this.setState(IQueue.ERROR);
		}
	}
	private void startThread() throws InstantiationException {
		//... Get access to this class configuration properties.
//		QueuedThreadPoolConfig info = new QueuedThreadPoolConfig();
//		long maxThreads = PackagePreferences.getLong("QueuedThreadPool.0", DEFAULT_MAXTHREADS );

		//... Check we have not reached the limit of running threads.
		if (this.poolSize < maxThreads) {
			factory.newThread(this, this.theQueue).start();
			this.poolSize++;
			this.activeThreads++;
		}
	}
	public void threadActivates() {
		this.activeThreads++;
	}
	public void threadDeactivates() {
		this.activeThreads--;
	}
	public void threadEnds() {
		this.poolSize--;
	}
//	public void threadExits( String string ) {
//		//... The thread identified by the parameter is about to exit. Remove from internal structures.
//		this.poolSize--;
//	}

//... PROTECTED CLASSES ................................................................................................
/*
	protected class QueuedThreadPoolConfig4 {
	  
	  /** Defaul max number of processing threads on a queue. *
	  private static final long DEFAULT_MAXTHREADS = 16;
		private Preferences localPrefs;
	  
	  public QueuedThreadPoolConfig4() {
			localPrefs = Preferences.userNodeForPackage( QueuedThreadPoolConfig.class );
	  }
	  
	  /**
	   * @return long
	   * @exception 
	   * Return the max number of threads that are configured. This is a configuration limit with a default value.
	   * @roseuid 408D24690144
	   *
	  public long getMaxThreads() {
			long value = DEFAULT_MAXTHREADS;
			try {
				value = localPrefs.getLong( PackagePreferences.getString("QueuedThreadPool.0"), DEFAULT_MAXTHREADS ); //$NON-NLS-1$
			} catch ( IllegalStateException ise ) {
					Logger.getLogger( "wdoo.thread" ).throwing( "TICKETCONFIG", "GETWORKUNITGROUPFACTOR", ise ); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			}
			return value;
	  }
	}
	/**
	 * @return
	 *
	public int getActive() {
		// TODO Auto-generated method stub
		return this.activeThreads;
	}
*/
}
