//  FILE NAME:      $RCSfile: IThreadFactory.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/thread/IThreadFactory.java,v $
//  CREATION DATE:         1999/11/30 12:20:24
//  LAST UPDATE;    $Date$
//  RELEASE:        $Revision$
//  STATE:          $State: Exp $
//  AUTHORS:        Retevision Interactiva.
//                  Alehop
//                  Luis de Diego (LDD) - luis_dediego@alehop.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 1999, 2000 by Retevision Interactiva, all rights reserved.
//	TO DO:
//		050425 (LDDT) - Change the interface because some parameters are no longer needed.
//  LOG:
//    $Log: IThreadFactory.java,v $
//    Revision 1.1  2005/05/06 12:54:34  ldiego
//    - Classes to complete the parallel processing scheme.
//
//    Revision 1.1  2000/06/26 08:51:09  ldiego
//    - New files for version beta 2 of the Event Loop Processor.
//
package wdoo.thread;

import java.sql.Connection;
import java.sql.SQLException;

import wdoo.queue.BlockingQueue;

//... IMPORT SECTION ...........................................................
//import wdoo.thread.FactorizedThreadPool;

//... INTERFACE DEFINITION .....................................................
public interface IThreadFactory {
  public Thread newThread( QueuedThreadPool ParentPool, BlockingQueue parentQueue ) throws InstantiationException;
//  public Connection getConnection() throws SQLException;
}
