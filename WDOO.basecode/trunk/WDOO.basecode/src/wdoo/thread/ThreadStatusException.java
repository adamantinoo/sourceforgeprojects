/*
 * Created on 28-Apr-04
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package wdoo.thread;

/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ThreadStatusException extends Exception {

//... CLASS CONSTRUCTORS .......................................................
	public ThreadStatusException() {
		super();
	}
	public ThreadStatusException( String message ) {
		super( message );
	}
}
