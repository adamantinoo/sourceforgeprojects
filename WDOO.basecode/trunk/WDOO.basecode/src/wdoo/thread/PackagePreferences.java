//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: PackagePreferences.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/thread/PackagePreferences.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: PackagePreferences.java,v $
//    Revision 1.1  2005/05/06 12:53:44  ldiego
//    - New files to manage configuration properties that are to be set at development.
//    - Coding of default values.
//

package wdoo.thread;

//... IMPORT SECTION ...................................................................................................
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

//... CLASS IMPLEMENTATION .............................................................................................
/**
 * @author Luis de Diego
 */
public class PackagePreferences {
	private static final String					BUNDLE_NAME			= "wdoo.thread.package";		//$NON-NLS-1$
	private static final ResourceBundle	RESOURCE_BUNDLE	= ResourceBundle.getBundle(BUNDLE_NAME);

	private PackagePreferences() {
	}

	public static String getString(String key) {
		// TODO Auto-generated method stub
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	public static int getInteger(String key) {
		// TODO Auto-generated method stub
		try {
			String resource = RESOURCE_BUNDLE.getString(key);
			return Integer.parseInt(resource);
		} catch (MissingResourceException e) {
			return -1;
		}
	}
	public static long getLong(String key, long defaultValue) {
		// TODO Generate documentation
		try {
			String resource = RESOURCE_BUNDLE.getString(key);
			return Long.parseLong(resource);
		} catch (MissingResourceException e) {
			return defaultValue;
		}
	}
}

//... UNUSED CODE ......................................................................................................
