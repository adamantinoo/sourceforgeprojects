/*
 * Created on 06-May-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package wdoo.log;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class WdooFormater extends SimpleFormatter {

	/* (non-Javadoc)
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 */
	public String format( LogRecord logrec ) {
		//... Compose the log message to be displayed on the console.
		//		The message format will be like this
		// TIMESTAMP THREADID LEVEL >>>/<<</   SOURCECLASSNAME SOURCEMETHODNAME - PARAMETERS MESSAGE
		StringBuffer outMessage = new StringBuffer();
		
		Date timeStamp = new Date( logrec.getMillis() );
		Object[] params = logrec.getParameters();
		StringBuffer paramString = new StringBuffer();
		if ( params != null ) {
			int paramsSize = params.length;
			for ( int i = 0; i < params.length; i++ ) {
				paramString.append( params[ i ].toString() );
			}
		}
		outMessage.append( timeStamp.toString() ).append( " " );
		outMessage.append( logrec.getThreadID() ).append( " " ).append( logrec.getLevel().getLocalizedName() ).append( " - " );
		outMessage.append( logrec.getSourceClassName() ).append( " " ).append( logrec.getSourceMethodName() ).append( " - " );
		outMessage.append( paramString ).append( " " ).append( logrec.getMessage() );
		outMessage.append( '\n' );
		
		return outMessage.toString();
	}

}
