//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: AbstractWorkUnit.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/AbstractWorkUnit.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo España, S.L., all rights reserved.
//  LOG:
//    $Log: AbstractWorkUnit.java,v $
//    Revision 1.1  2005/05/06 12:50:02  ldiego
//    - New classes on the common scheme for Work Unit parallel processing.
//

package wdoo.process;

//... IMPORT SECTION .........................................................................................
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * Esta clase abstracta implementa la base de cualquier WorkUnit. Algunos de sus métodos deberán ser reimplementados
 * por cada nueva subclase para cumplir sus requerimientos, mientras que otros tienen como función el soporte de la
 * infraestructura y poedeen ser utilizados por cualquier imeplementación.
 * 
 * La clase <code>AbstractWorkUnit</code> exporta un helper para decodificar los estados a un formato legible o
 * utilizable en los logs. Esta clase es <code>WorkUnitStateDecoder</code>.
 * 
 * @author ldiego
 */
public abstract class AbstractWorkUnit implements IWorkUnit {

//... INSTANCE PROTECTED FIELDS
	/** Referencia al procesador pricipal. Este procesador tiene las funciones de organizador y de soporte para la
	 * localización de los diferentes componenetes parametrizables o sustituibles de cada implementación. Cada instancia
	 * de WorkUnit es creada por una factoria, perteneciente a un procesador y permite la identificación de este
	 * durante la creación de la instancia.
	 */
//	private IWorkProcessor	workProcessor;
	/** Estado de la WorkUnit. Determinados estados indican el progreso en la vida de la WorkUnit o si esta puede ser
	 * destruida.
	 * El estado inicial es NOTINITIALIZED y el estado requerido para que se pueda ejecutar es READY.
	 */
	protected int	state = IWorkUnit.NOTINITIALIZED;

//... PUBLIC METHODS .........................................................................................
	
//... INTERFACE IMPLEMENTATION OPERATIONS ....................................................................
//... IWORKUNIT
	/**
	 * Inicializa el WorkUnit conectando a la fuente de datos y realizando las comprobaciones necesarias previas a
	 * la ejecución del proceso
	 */
//	public abstract void initialize();
	public abstract String getIdentifier();
	public void fire() throws Exception {
		//TODO This is an empty method that usually is overriden
	}


	/* (non-Javadoc)
	 * @see wdoo.process.IWorkUnit#lock()
	 */
//	public void lock() throws SQLException {
//		// TODO Auto-generated method stub
//		
//	}

	/* (non-Javadoc)
	 * @see wdoo.process.IWorkUnit#unlock()
	 */
//	public void unlock() {
//		// TODO Auto-generated method stub
//		
//	}

	/* (non-Javadoc)
	 * @see wdoo.process.IWorkUnit#setThreadId(java.lang.String)
	 */
//	public void setThreadId(String string) {
//		// TODO Auto-generated method stub
//		
//	}

	/* (non-Javadoc)
	 * @see wdoo.process.IWorkUnit#setDatabaseConn(java.sql.Connection)
	 */
//	public void setDatabaseConn(Connection connection) {
//		// TODO Auto-generated method stub
//		
//	}


}

//... UNUSED CODE ......................................................................................................
