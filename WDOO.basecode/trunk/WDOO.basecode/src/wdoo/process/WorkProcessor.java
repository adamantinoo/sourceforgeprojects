//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/WorkProcessor.java
//FILE NAME:      $RCSfile: WorkProcessor.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/WorkProcessor.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: WorkProcessor.java,v $
//Revision 1.1  2004/04/27 10:25:17  ldiego
//- Base release of the source file. Added to repository to start fixing classes being used at the project.
//
//Revision 1.1  2004/04/27 09:48:15  ldiego
//- This source file will be moved to a generic implementation inside the wdoo.process
//package and changed its name.
//

package wdoo.process;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Logger;

import wdoo.app.Application;
//import wdoo.morgane.tickets.TicketConfig;
import wdoo.queue.BlockingQueue;
import wdoo.thread.QueuedThreadPool;

//import com.alehop.trace.Trace;

/**
 */
public class WorkProcessor {
  public static final long SPIN = 1;
  public static final long BEAT = 2;

//... INSTANCE PRIVATE FIELDS
	private Logger						log = Logger.getLogger( "wdoo.process" );
	private boolean					shutdownRequest = false;
	private long						spinCounter = 0;
	private QueuedThreadPool threadPool;
	private WorkGetter				workDownloader;
	private BlockingQueue		workQueue;
  
  /**
   * @param queue
   * @return 
   * @exception 
   * The initialization process receives the internal queue it has to use for work sotrage. This way we can configure the queue behaviour outside the class and deply the particular configuration.
   * @roseuid 407D1CB40175
   */
  public WorkProcessor( QueuedThreadPool threadPool, WorkGetter workDownloader, BlockingQueue workQueue ) {
  	this.threadPool = threadPool;
  	this.workDownloader = workDownloader;
  	this.workQueue = workQueue;
  }
  
  /**
   * @return void
   * @exception
   * @roseuid 407E4D060205
   */
  private static void shutdown() {}
  
  public void run() {
		log.entering( "WORKPROCESSOR", "RUN" );
		
		//... The initialization process has created all objects. Now it is time to put them to work.
		//		The first step is to enter the endless loop for work downloading, then work scheduling and
		//		other timed tasks.
		while ( shutdownRequest == false ) {
			try {
				//... Get this iteration class to classify operations depending on it. The fastest is called
				//		a spin while the next level is called a beat. This call performs any wait required so
				//		it will exit after the spin period of time.
				long time = this.getTimeClass();
				
				if ( time >= WorkProcessor.SPIN ) {
					//... First check if some of the already work units may be passed to the work chain for processing.
					Object element = this.workQueue.dequeue();
					while( element != null ) {
						//... Move elements to the processor for execution.
						this.threadPool.getQueue().queue( element );
						element = this.workQueue.dequeue();
					}
				}

				if ( time >= WorkProcessor.BEAT ) {
					//... Get access to the configuration to be able to read the properties.				
					TicketConfig info = (TicketConfig)Application.getConfiguration();
					
					//... Check if it is required to download more work units from the repository. This has to
					//		count the units already scheduled for processing and the current waiting units.
					long waitingCount = this.workQueue.count();
					long processingCount = this.threadPool.getQueue().count();
					long queueSize = waitingCount + processingCount;
					long queueLevel = info.getQueueLevel();
					if ( queueSize < queueLevel ) {
						//... Get more work pieces from the associated repository.
						long workListSize = info.getWorkUnitGroupFactor();
						LinkedList workList = this.workDownloader.getWorkList( workListSize );
						
						//... Move downloaded work units to the waiting queue.
						Iterator it = workList.iterator();
						while( it.hasNext() ) {
							Object moveElement = it.next();
							this.workQueue.queue( moveElement );
						}
					}
				} // END IF TIME
			} catch ( Exception e ) {
			}
		} // END WHILE SHUTOWNREQUEST

		//... Close telnet services
//		TelnetService.shutdown();
		//... Close statistics and print them
		// TO BE IMPLEMENTED
		log.exiting( "WORKPROCESSOR", "RUN" );
//		Trace.info( "WORKPROCESSOR.RUN.SAFETERMINATION!!" );
  }

	/**
	 * @return long
	 * @exception 
	 * @roseuid 407E6EF40223
	 */
  private long getTimeClass() {
  	long currentClass = WorkProcessor.SPIN;
  	
		//... Get access to the configuration to be able to read the properties.				
		TicketConfig info = (TicketConfig)Application.getConfiguration();

		//... Pause for the specified time before getting the time class.
		try {
			Thread.currentThread().sleep( info.getSpinTime() );
		} catch ( InterruptedException ie ) {
				Trace.exception( "PROCESSOR.GETIMECLASS.INTERRUPTEDEXCEPTION", ie );
		}
		
		//... By default this is a spin. Count the number of spins to calculate if this also is a beat.
		this.spinCounter++;
		long spins4Beat = info.getSpins4Beat();
		if ( this.spinCounter >= spins4Beat ) {
			currentClass = WorkProcessor.BEAT;
			this.spinCounter = 0;
		}
		return currentClass;
  }
}
