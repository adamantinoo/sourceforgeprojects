//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: IWorkUnit.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/IWorkUnit.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  TO DO:
//    050425 (LDDT) - Cambiar en API para permitir la configuracion generica por medio del paso de objetos
//										opacos al interface pero identificables para la implementacion. Revisar la implementacion
//										del metodo <<getAdapter()>> de eclipse para la obtencion de instancias en funcion de
//										la clase solicitada.
//		050425 (LDDT) - Quitar el metodo setConnection ya que no es aplicable a un codificacion generica.
//		050425 (LDDT) - A�adir metodos para gestionar el estado de la WorkUnit y la posibilidad de decodificacion.
//  LOG:
//    $Log: IWorkUnit.java,v $
//    Revision 1.1  2005/05/06 12:50:02  ldiego
//    - New classes on the common scheme for Work Unit parallel processing.
//

package wdoo.process;

//... IMPORT SECTION ...................................................................................................
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

//... CLASS IMPLEMENTATION .............................................................................................
/**
 * <h3>Notas de implementacion version 1.1</h3>
 * Las implementaciones actuales de los WorkUnit reciben la conexion al DataSource desde el thread, siendo
 * este el que comparte esta conexi�n con las diferentes WUs que procesa. Sin embargo una excepcion en la
 * conexion no se ve reflejada ni transmitida al thread generando esto un problema de gestion de excepciones.
 * <br>
 * Esto lleva a la conclusion de que es necesario cambiar el API de estas clases y redefinir las
 * responsabilidades para mantener en lo posible su independnecia respecto a la implementacion pero
 * permitiendo este tipo de especializaciones. <br>
 * 
 * <h3>Descripcion funcional</h3>
 * <ul>
 * <li>Define una unidad de trabajo que conoce cuales son sus elementos de comunicacion con el entorno y
 * dispone de un metodo principal donde se ejecuta el procesao para el que ha sido dise�ada.</li>
 * <li>Define el metodo <code>run()</code> procedente de <code>Runnable</code>, lo cual hace innecesario
 * el definir un metodo principal como <code>fire()</code> a no se rpara definir un interface diferente de
 * comunicacion con el exterior.</li>
 * <li>Actualmente requiere clases de <code>java.sql</code> lo cual indica que no es independiente del
 * entorno de implementacion.</li>
 * <li>Es ejecutada en el entorno de un thread que se encarga de su creacion, parametrizacion, inicio y
 * terminacion.</li>
 * <li>Su ciclo de vida no esta claramente definido ya que solo estamos describiendo un interface. Cada
 * implementacion de thread es libre de utilizar el API a su conveniencia, lo que limita la generalizacion del
 * codigo.</li>
 * <li>Ciclo de vida propuesto: <br>
 * <table>
 * <tr>
 * <td>01</td>
 * <td colspan="2">&lt;creacion&gt;</td>
 * </tr>
 * <tr>
 * <td>02</td>
 * <td><code>initialize()</code></td>
 * <td>- disparo de las funciones de inicializacion.</td>
 * </tr>
 * <tr>
 * <td>03</td>
 * <td><code>lock()</code></td>
 * <td>- bloqueo de recursos previo al disparo.</td>
 * </tr>
 * <tr>
 * <td>04</td>
 * <td><code>run()</code></td>
 * <td>- ejecucion del metodo principal.</td>
 * </tr>
 * <td>05</td>
 * <td><code>unlock()</code></td>
 * <td>- liberacion de los recursos.</td>
 * </tr>
 * <tr>
 * <td>06</td>
 * <td colspan="2">&lt;destruccion&gt;</td>
 * </tr>
 * </table>
 * Este ciclo de vida permite incluir las acciones de bloqueo y desbloqueo de forma interna si se define una funcion
 * <code>fire()</code> que englobe la ejecucion de los pasos 3, 4 y 5.</li>
 * </ul>
 * 
 * @author luis.dediego@es.wanadoo.com
 */
public interface IWorkUnit extends Runnable {
  
  public int	NOTINITIALIZED = 0x01;
  public int	READY	= 0x02;
  public int	ERROR	= 0x99;

	public void initialize(Object[] config) throws InstantiationException;
	/**
	 * This is the processing method of the IWorkUnit. It must be called to perform any qork desired to be done
	 * by the Unit. It must check that the configuration is ready and that all resources requiered to perform
	 * the job are ready and locked.
	 * 
   * @throws Exception this method is able to throw any tipe of exepction, from threasd related to SQL.
	 */
  public void fire() throws Exception;
  
  /**
   * @return String
   * @exception 
   * Return the unique identifier assigned to a IWorkUnit. This is an opaque string only understandable by the Repository.
   * @roseuid 408D3AE700F6
   */
  public String getIdentifier();
//	public void lock() throws SQLException;
//	public void unlock();
//	public void setThreadId(String string);

	/**
	 * Passes a data source connection object to the unit for configuration.
	 * @param connection the data source connection that is configured for this Work Unit.
	 */
//	public void setDatabaseConn(Connection connection);
}
