// PROJECT: WDOO.basecode
//  FILE NAME: $RCSfile: IWorkProcessor.java,v $
//  FILE PATH: $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/IWorkProcessor.java,v $
//  LAST UPDATE: $Date$
//  RELEASE: $Revision$
//  AUTHORS: Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER: $Author$
//  COPYRIGHT: (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: IWorkProcessor.java,v $
//    Revision 1.1  2005/05/06 12:50:02  ldiego
//    - New classes on the common scheme for Work Unit parallel processing.
//

package wdoo.process;

//... IMPORT SECTION ...................................................................................................
import java.sql.Connection;
import java.util.List;

import wdoo.resource.ResourceManager;

//... CLASS IMPLEMENTATION .............................................................................................
/**
 * Interface que define la estructura de un WorkProcessor. El WorkProcessor es responsable de obtener el trabajo a realizar y
 * de segmentarlo, asignarlo y priorizarlo adecuadamente. El WorkPorcessor es adem�s es centro de referencia para la
 * obtenci�n del resto de los elementos parametrizables de una implementaci�n as� como el gestor y organizador de los
 * diferentes objetos. Esta interface es necesario que se implemente con un desarrollo custoimizado a la tarea a realizar.
 * 
 * @author ldiego
 */
public interface IWorkProcessor extends Runnable {
	/** Constante que indica en que estado de proceso se encuentra el WorkProcessor. Estado SPIN */
	public static final int	SPIN	= 0x01;
	/** Constante que indica en que estado de proceso se encuentra el WorkProcessor. Estado BEAT */
	public static final int	BEAT	= 0x02;

	/**
	 * Devuelve una conexi�n libre del pool de conexiones espeficicado. En el caso que el par�metro est� vac�o se devuelve una
	 * conexi�n del pool por defecto o del �nico pool existente.
	 * 
	 * @param poolName
	 *          Nombre identificador del pool seleccionado.
	 * @return Conexi�n SQL al repositorio indicado.
	 */
	public Connection getSQLConnection(String poolName);

	/**
	 * Devuelve la referencia al ResourceManager asignado a este WoorProcessor durante la inicializaci�n. El ResourceManager no
	 * requiere diferenciaci�n en funci�n de la implementaci�n y es una clase disponible como infraestructura.
	 */
	public ResourceManager getResourceManager();

}

//... UNUSED CODE ......................................................................................................
