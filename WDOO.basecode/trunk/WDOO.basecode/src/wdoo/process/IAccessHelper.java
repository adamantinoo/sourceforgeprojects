/*
 * Created on 06-May-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package wdoo.process;

/**
 * @author ldiego
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IAccessHelper {

	/**
	 * @param unit
	 */
	void checkWorkUnit( IWorkUnit unit );

	/**
	 * @param TID
	 */
	void checkTicket( Object TID );

	/**
	 * @param TID
	 */
	void readTicket( Object TID );

	/**
	 * @param TID
	 * @return
	 */
	char executeTicket( Object TID ) throws Exception;

	/**
	 * @param string
	 * @param ticketState
	 */
	void updateTicket( Object TID, char ticketState );

}
