//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/IWorkGetter.java

package wdoo.process;

import java.util.LinkedList;

/**
 * A IWorkGetter receives commands to retireve a number of work pieces from a Repository that is configured as a complementary object instance received as a parameter at the initialization.
 * Is interface is simple and just interfaces any Repository source to a demanding application.
 */
public interface IWorkGetter {
  
  /**
   * @param workListSize
   * @return Vector
   * @exception
   * @roseuid 407E58330399
   */
  public LinkedList getWorkList(long workListSize);
}
