//	FILE NAME:      $RCSfile: AbstractWorkProcessor.java,v $
//	FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/process/AbstractWorkProcessor.java,v $
//	LAST UPDATE:    $Date$
//	RELEASE:        $Revision$
//	AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//	LAST USER:      $Author$
//	COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//	LOG:
//		$Log: AbstractWorkProcessor.java,v $
//		Revision 1.1  2005/05/06 12:50:02  ldiego
//		- New classes on the common scheme for Work Unit parallel processing.
//		
//		Revision 1.1  2004/04/27 10:25:17  ldiego
//		- Base release of the source file. Added to repository to start fixing classes being used at the project.
//
//		Revision 1.1  2004/04/27 09:48:15  ldiego
//		- This source file will be moved to a generic implementation inside the wdoo.process
//			package and changed its name.
//

package wdoo.process;

//... IMPORT SECTION ...................................................................................................
import java.sql.Connection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import wdoo.queue.IQueue;
import wdoo.thread.QueuedThreadPool;
import wdoo.thread.ThreadStatusException;

//... CLASS IMPLEMENTATION .............................................................................................
public abstract class AbstractWorkProcessor implements IWorkProcessor {
	private static Logger log = Logger.getLogger( "wdoo.process" );
	static {
		log.setLevel( Level.FINE );
	}

//... INSTANCE PRIVATE FIELDS
	private boolean					shutdownRequest = false;
	private long						spinCounter = 0;
	private QueuedThreadPool threadPool;
	private IQueue						threadQueue;
	private IWorkGetter			workDownloader;
	private IQueue						workQueue;
  
  /**
   * @param queue
   * @return 
   * @exception 
   * The initialization process receives the internal queue it has to use for work sotrage. This way we can configure the queue behaviour outside the class and deply the particular configuration.
   * @roseuid 407D1CB40175
   */
  public AbstractWorkProcessor( QueuedThreadPool threadPool, IWorkGetter workDownloader, IQueue workQueue ) {
  	this.threadPool = threadPool;
  	this.threadQueue = this.threadPool.getQueue();
  	this.workDownloader = workDownloader;
  	this.workQueue = workQueue;
  }
  
  /**
   * @return void
   * @exception
   * @roseuid 407E4D060205
   */
//  private static void shutdown() {}
//... PUBLIC METHODS ...................................................................................................
	public abstract Connection getSQLConnection(String poolName);
  
  public void run() {
		log.entering( "WORKPROCESSOR", "RUN" );
		
		//... The initialization process has created all objects. Now it is time to put them to work.
		//		The first step is to enter the endless loop for work downloading, then work scheduling and
		//		other timed tasks.
		while ( this.shutdownRequest == false ) {
			try {
				//... Get this iteration class to classify operations depending on it. The fastest is called
				//		a spin while the next level is called a beat. This call performs any wait required so
				//		it will exit after the spin period of time.
				long time = this.getTimeClass();
				
				if ( time >= AbstractWorkProcessor.SPIN ) {
					log.fine( "SPINNING" );
					//... First check if some of the already work units may be passed to the work chain for processing.
					Object element = this.workQueue.dequeue();
					while( element != null ) {
						//... Move elements to the processor for execution.
						this.threadPool.queue( element );
						element = this.workQueue.dequeue();
					}
				} // END IF TIME

				if ( time >= AbstractWorkProcessor.BEAT ) {
					log.fine( "BEATTING" );
					//... Get access to the configuration to be able to read the properties.				
					WorkProcessorConfig info = new WorkProcessorConfig();
					
					//... Check if it is required to download more work units from the repository. This has to
					//		count the units already scheduled for processing and the current waiting units.
					long waitingSize = this.workQueue.size();
					long processingSize = this.threadQueue.size();
					long queueSize = waitingSize + processingSize;
					long queueLevel = info.getQueueLevel();
					log.fine( "queue size = " + queueSize );
					log.fine( "queue level = " + queueLevel );
					if ( queueSize < queueLevel ) {
						//... Get more work pieces from the associated repository.
						long workListSize = info.getWorkUnitGroupFactor();
						LinkedList workList = this.workDownloader.getWorkList( workListSize );
						
						//... Move downloaded work units to the waiting queue.
						Iterator it = workList.iterator();
						while( it.hasNext() ) {
							Object moveElement = it.next();
							this.workQueue.queue( moveElement );
						}
					}
				} // END IF TIME
			} catch ( ThreadStatusException tee ) {
			}
		} // END WHILE SHUTOWNREQUEST

		//... Close telnet services
//		TelnetService.shutdown();
		//... Close statistics and print them
		// TO BE IMPLEMENTED
		log.exiting( "WORKPROCESSOR", "RUN" );
//		Trace.info( "WORKPROCESSOR.RUN.SAFETERMINATION!!" );
  }

	/**
	 * @return long
	 * @exception 
	 * @roseuid 407E6EF40223
	 */
  private long getTimeClass() {
  	long currentClass = AbstractWorkProcessor.SPIN;
  	
		//... Get access to the configuration to be able to read the properties.				
		WorkProcessorConfig info = new WorkProcessorConfig();

		//... Pause for the specified time before getting the time class.
		try {
			Thread.currentThread().sleep( info.getSpinTime() );
		} catch ( InterruptedException ie ) {
				log.throwing( "PROCESSOR", "GETIMECLASS", ie );
		}
		
		//... By default this is a spin. Count the number of spins to calculate if this also is a beat.
		this.spinCounter++;
		long spins4Beat = info.getSpins4Beat();
		if ( this.spinCounter >= spins4Beat ) {
			currentClass = AbstractWorkProcessor.BEAT;
			this.spinCounter = 0;
		}
		return currentClass;
  }

	public class WorkProcessorConfig {
  
		/**
		 * Default value for the number of units to receive on a single request operation to the Repository.
		 */
		static final long DEFAULT_WORKUNITGROUPFACTOR = 0;
		static final long DEFAULT_QUEUELEVEL = 10;
		static final long DEFAULT_SPINTIME = 5;
		static final long DEFAULT_SPINS4BEAT = 2;
  
//	... INSTANCE PRIVATE FIELDS
		private Logger log = Logger.getLogger( "wdoo.process" );

		private Preferences localPrefs;
  
		/**
		 * Instance initializer that get access to this class corresponding user properties node.
		 * @roseuid 407CF2BF00EF
		 */
		public WorkProcessorConfig() {
			localPrefs = Preferences.userNodeForPackage( WorkProcessorConfig.class );
		}
  
		/**
		 * @return long Returns the number of units wanted to receive in a single request operation.
		 * @exception 
		 * Returns the number of units wanted to receive in a single request operation.
		 * @roseuid 407E7C840225
		 */
		public long getWorkUnitGroupFactor() {
			long value = DEFAULT_WORKUNITGROUPFACTOR;
			try {
				value = localPrefs.getLong( "workUnitGroupFactor", DEFAULT_WORKUNITGROUPFACTOR );
			} catch ( IllegalStateException ise ) {
					Logger.getLogger( "" ).throwing( "TICKETCONFIG", "GETWORKUNITGROUPFACTOR", ise );
			}
			return value;
		}
  
		/**
		 * @return long
		 * @exception 
		 * Returns the number of units that are desirable to be queuing. Any value below this treshold will generate requests to the Repository to download more units.
		 * @roseuid 407E7C84029D
		 */
		public long getQueueLevel() {
			long value = DEFAULT_QUEUELEVEL;
			try {
				value = localPrefs.getLong( "queueLevel", DEFAULT_QUEUELEVEL );
			} catch ( IllegalStateException ise ) {
					Logger.getLogger( "" ).throwing( "TICKETCONFIG", "GETQUEUELEVEL", ise );
			}
			return value;
		}
  
		/**
		 * @return long
		 * @exception 
		 * Returns the number of seconds of a spin operation. A spin is the base time period for looped operations.
		 * @roseuid 407E7C840316
		 */
		public long getSpinTime() {
			long value = DEFAULT_SPINTIME;
			try {
				value = localPrefs.getLong( "spinTime", DEFAULT_SPINTIME );
				value *= 1000;
			} catch ( IllegalStateException ise ) {
					Logger.getLogger( "" ).throwing( "TICKETCONFIG", "GETSPINTIME", ise );
			}
			return value;
		}
    
		/**
		 * @return long
		 * @exception 
		 * Number of spins that have to elapse before considering this a Beat class iteration.
		 * @roseuid 407E7C84037A
		 */
		public long getSpins4Beat() {
			long value = DEFAULT_SPINS4BEAT;
			try {
				value = localPrefs.getLong( "queueLevel", DEFAULT_SPINS4BEAT );
			} catch ( IllegalStateException ise ) {
					Logger.getLogger( "" ).throwing( "TICKETCONFIG", "GETSPINS4BEAT", ise );
			}
			return value;
		}
	}
}
