//  PROJECT:        WDOO.basecode.1.3
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by UNI2 Telecomunicaciones, S.L., all rights reserved.
//  TO DO:
//    YYMMDD (DEV) - <<comment>>
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.messages;

//... IMPORT SECTION .........................................................................................
import java.util.MissingResourceException;
import java.util.ResourceBundle;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * @author Luis de Diego
 */
public final class Messages {
	private static String					bundleName			= "wdoo.messages.messages";								//$NON-NLS-1$
	private static ResourceBundle	resourceBundle	= ResourceBundle.getBundle(bundleName);

	private Messages() {
	}
	public static void setBundle(final String theBundleName) {
		bundleName = theBundleName;
		resourceBundle = ResourceBundle.getBundle(bundleName);
	}
	public static String getMessage(final String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException mre) {
			return "Message not found. !" + key + '!';
		}
	}
}

//... UNUSED CODE ............................................................................................
