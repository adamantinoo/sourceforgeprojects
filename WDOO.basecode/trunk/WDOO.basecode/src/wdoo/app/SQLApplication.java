//  PROJECT:        WDOO.basecode.1.4
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2005 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.app;

//... IMPORT SECTION .......................................................................................
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

//... CLASS IMPLEMENTATION .................................................................................
public class SQLApplication extends NoInterfaceApplication {
	private static Logger										log				= Logger.getLogger("wdoo.app"); //$NON-NLS-1$
	private OracleConnectionPoolDataSource	ocpds;
	private Connection											databaseConn;
	protected String												configDirectory;
	protected String												outputDirectory;
	protected String												resourceDirectory;
	protected Properties										props			= new Properties();
	/**
	 * La aplicacion no puede establecer las comunicaciones con la base de datos.
	 * <ul>
	 * <li>Verificar los parametros de conexion que se indican en el fichero de configuracion y que se
	 * descargan en el log del proceso.</li>
	 * <li>Comprobar las comunicaciones con la base de datos por medio del script de verificacion
	 * $HOMEDIR/scripts/verifydbconnection.sh.</li>
	 * <li>Notificar el error y enviar el log a desarrollo para corregir posibles defectos de configuracion.
	 * </li>
	 * </ul>
	 */
	private static final int								DATABASE	= 12;

	protected void openConnection(String username, String password, String connectstring,
			String connectmessage, Object[] messageparameters) {

		try {
			// ... Connect to the Oracle repository
			this.setConnectionPool(new OracleConnectionPoolDataSource());
			ocpds.setDriverType("thin"); //$NON-NLS-1$
			ocpds.setUser(username);
			ocpds.setPassword(password);
			ocpds.setTNSEntryName(connectstring);

			log.info(MessageFormat.format(connectmessage, messageparameters)); //$NON-NLS-1$
			this.databaseConn = this.ocpds.getConnection();
		} catch (SQLException sqle) {
			// ... Exit point 12. La aplicacion no puede establecer las comunicaciones con la base de datos.
			log.throwing("ExtractorConsumoApp", "start", sqle); //$NON-NLS-1$ //$NON-NLS-2$
			SQLApplication.exit(DATABASE);
		}

	}

	private static void exit(int database2) {
		// TODO Auto-generated method stub
		System.exit(database2);
	}

	private static Logger getLog() {
		// TODO Auto-generated method stub
		return log;
	}

	private void setConnectionPool(OracleConnectionPoolDataSource source) {
		// TODO Auto-generated method stub
		this.ocpds = source;
	}
}

// ... UNUSED CODE ..........................................................................................
