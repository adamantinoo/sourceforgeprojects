//Source file: U:/SISTCVS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/app/Application.java
//FILE NAME:      $RCSfile: Application.java,v $
//FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/app/Application.java,v $
//LAST UPDATE:    $Date$
//RELEASE:        $Revision$
//AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//LAST USER:      $Author$
//COPYRIGHT:      (c) 2004 by Wanadoo Espa�a, S.L., all rights reserved.
//LOG:
//$Log: Application.java,v $
//Revision 1.1  2004/04/22 15:31:11  ldiego
//- This class has changed since the new implementation of preferences in Java 1.4.
//- This file is kept with this version because some feature may be recovered.
//

package wdoo.app;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

import wdoo.net.IPAddress;
import wdoo.resource.ResourceManager;

/**
 */
public class Application {
  
//... CLASS PRIVATE FIELDS
//	private static Logger				log					= Logger.getLogger("net.sourceforge.project.sqlanalyzer");	//$NON-NLS-1$
	private static Logger				log = Logger.getLogger( "wdoo.app" );
	private static InetAddress	localAddress;
	private static String				localHostName;
	private ResourceManager rmanager = null;

//... INSTANCE PRIVATE FIELDS

	/**
	 * 
	 */
	public Application() {
		super();
		try {
			this.localAddress = InetAddress.getLocalHost();
			this.localHostName = localAddress.getHostName().toUpperCase();
		} catch ( UnknownHostException uhe ) {
			log.throwing( "APPLICATION", "CONSTRUCTOR", uhe );
			System.out.println( "UnknownHostException catched when getting getLocalHost() information" );
			System.exit( 1 );
		}
	}

  /**
   * @return String[]
   * @exception 
   *       that are processed here instead coding on each new implementation. <BR>
   *       Currently supported syntax is: <BR>
   *         <CODE><B>-prop[erties] <PROPERTIES_FILE_PATH></B></CODE> <BR>
   *       The processed arguments are removed from the original list and not returned on the copy this
   *       method returns. <BR>
   *       All exceptions are processed locally and not reported to the main application. <BR>
   *       @param  sysParameters array of command line arguments received when the Java application started
  *
   * @roseuid 407EA53B00E3
   */
  protected static String[] processParameters(String[] sysParameters) {
    log.entering( "APLICATION", "PROCESSPARAMETERS" );
    //... Scan arguments for supported syntax and just process all that will be accepted.
    Vector newArguments = new Vector();
    
    for ( int index = 0; index < sysParameters.length; index ++ ) {
      String argument = sysParameters[ index ];
      argument = argument.toLowerCase();
      if ( argument.startsWith( "-prop" ) ) {
        //... Next argument must exist and be a valid properties file path.
        try {
          index ++;
          String propertiesPath = sysParameters[ index ];
					if ( ( propertiesPath != null ) && ( propertiesPath.length() > 0 ) ) {
	          //... Create an input stream on the path and try to import the properties.
	          InputStream is = null;
	          is = new BufferedInputStream( new FileInputStream( propertiesPath ) );
	          Preferences.importPreferences( is );
          }
//        } catch ( ArrayIndexOutOfBoundsException aioobe ) {
//            Trace.exception( "APPLICATION.PROCESSPARAMETERS.ARRAYINDEXOUTOFBOUNDSEXCEPTION", aioobe );
        } catch ( FileNotFoundException fnfe ) {
            log.throwing( "APPLICATION", "PROCESSPARAMETERS", fnfe );
				} catch ( IOException ioe ) {
						log.throwing( "APPLICATION", "PROCESSPARAMETERS", ioe );
				} catch ( InvalidPreferencesFormatException ipfe ) {
						log.throwing( "APPLICATION", "PROCESSPARAMETERS", ipfe );
//        } catch ( Exception e ) {
//						log.throwing( "APPLICATION", "PROCESSPARAMETERS", e );
        }
      } // END IF ARGUMENT.STARTSWITH
      else newArguments.addElement( argument );
    } // END FOR
    
    //... Construct the new array of arguments from the new arguments vector
    String[] returnArray = new String[ newArguments.size() ];
    for ( int index = 0; index < newArguments.size(); index ++ )
      returnArray[ index ] = (String)newArguments.elementAt( index );
    return returnArray;}
  
//... GETTER/SETTER PUBLIC OPERATIONS ..............................................................
  /**
   * @return String
   * @exception
   * @roseuid 407EA53B00F7
   */
  public static String getHostName() {
  	// REVIEW Check the functionality and operation of this method.
  	return localHostName;
  }

	/**
	 * @return IPAddress
	 * @exception
	 * @roseuid 407EA53B0100
	 * 
	 * @uml.property name="localAddress"
	 */
	public static IPAddress getLocalAddress() throws UnknownHostException {
		InetAddress localAddress = InetAddress.getLocalHost();
		String localHostName = localAddress.getHostName().toUpperCase();
		return new IPAddress(localAddress.getAddress());
	}

	/**
	 * 
	 */
	public static String getIdentifier() {
		// TODO Auto-generated method stub
		return "TicketLoader";
	}

	public static void setLogger(Logger logger) {
		// TODO Auto-generated method stub
		log = logger;
	}
	public ResourceManager getResourceManager() {
		return this.rmanager;
	}
	public void setResourceManager(ResourceManager rman) {
		this.rmanager = rman;
	}
}
/* 
Application.getAppProperty(String){
    return appProperties.getProperty( name );}
 */
/* 
Application.getPropertyNames(){
    return appProperties.propertyNames();}
 */
/* 
Application.setupProperties(Properties){
    Trace.enter( "APPLICATION.SETUPPROPERTIES" );
    Enumeration propList = props.propertyNames();
    while ( propList.hasMoreElements() ) {
      String paramName = (String)propList.nextElement();
      Trace.info( 00, "APPLICATION.SETUPPROPERTIES.PROP.DEF.VALUES." + paramName + " = " + appProperties.getProperty( paramName ) );
      String value = props.getProperty( paramName );
      appProperties.put( paramName, props.getProperty( paramName ) );
      Trace.info( 00, "APPLICATION.SETUPPROPERTIES.CONF.PROPERTIES." + paramName + " = " + props.getProperty( paramName ) );
    }
    Trace.exit( "APPLICATION.SETUPPROPERTIES" );}
 */
/* 
Application.setAppProperty(String,String){
    appProperties.put( name, value );}
 */
/* 
Application.getProperties(){
    return appProperties;}
 */
/* 
Application.getConfiguration(){
    return appConfiguration;}
 */
/* 
Application.getAppProperty(String,String){
    return appProperties.getProperty( name, defaultValue );}
 */
