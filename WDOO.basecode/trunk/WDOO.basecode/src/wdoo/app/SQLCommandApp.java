//  PROJECT:        PrototipoDigramaCicloFacturacion
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package wdoo.app;

//... IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import wdoo.resource.ResourceManager;
import wdoo.resources.SQLResource;

//... CLASS IMPLEMENTATION ...................................................................................
public class SQLCommandApp extends CommandApp {

	/** Definicion de la configuracion y del logger a utilizar en esta aplicacion. */
	protected static Logger										log				= Logger.getLogger("wdoo.app"); //$NON-NLS-1$

	/**
	 * The application can not stablish a connection with the database.
	 */
	public static final int									DATABASE	= 12;

	//... Common fields for all this implementations
	protected OracleConnectionPoolDataSource	ocpds;
	protected Connection											databaseConn;
	protected String													sqlResourceDirectory;

	//... CLASS CONSTRUCTORS ...................................................................................
	public SQLCommandApp(String[] args) {
		//		super(args);
		// ... Process parameters and store them into the instance fields
		for (int i = 0; i < args.length; i++) {
			// ... Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-res")) { //$NON-NLS-1$
					//... Check argument array size before trying to get the parameter
					if (i + 1 < args.length) {
						this.sqlResourceDirectory = args[i + 1];
					} else {
						//... Exit point 10. No esta especificado el directorio de configuracion de la aplicacion.
						exit(NOCONFIG);
					}
				}
			}
		}
		//... Check that all required parameters have values.
		if (null != this.sqlResourceDirectory) {
			SQLResource.setPrefix(this.sqlResourceDirectory);
		} else
			exit(NOCONFIG);
	}

	protected void openConnection() {
		try {
			//... Connect to the Oracle repository
			this.ocpds = new OracleConnectionPoolDataSource();
			ocpds.setDriverType("thin"); //$NON-NLS-1$
			ocpds.setUser(this.props.getProperty("schema")); //$NON-NLS-1$
			ocpds.setPassword(this.props.getProperty("pw")); //$NON-NLS-1$
			ocpds.setTNSEntryName(this.props.getProperty("connectstring")); //$NON-NLS-1$
			Object[] mesargs = {
					this.props.getProperty("schema"), this.props.getProperty("connectstring") }; //$NON-NLS-1$ //$NON-NLS-2$
			//TODO Add a log message when the connection has completed
//			log.info(MessageFormat.format(
//					ResourceManager.getResource("ExtractorConsumoApp.conectando"), mesargs)); //$NON-NLS-1$

			this.databaseConn = this.ocpds.getConnection();
		} catch (SQLException sqle) {
			//... Exit point 12. La aplicacion no puede establecer las comunicaciones con la base de datos.
			sqle.printStackTrace();
			log.throwing("SQLCommandApp", "openConnection", sqle);
			exit(DATABASE);
		}
	}
	/**
	 * Checks if the database connection is available and initialized.
	 * 
	 * @return a <code>true</code> value if the connection is valid.
	 */
	protected boolean checkConnection() {
		if (null != this.databaseConn) return true;
		return false;
	}

}

//... UNUSED CODE ............................................................................................
