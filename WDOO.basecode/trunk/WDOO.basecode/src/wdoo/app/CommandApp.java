//  PROJECT:        WDOO.basecode
//  FILE NAME:      $RCSfile: CommandApp.java,v $
//  FILE PATH:      $Source: /data/tecno/SISTEMAS/acceso/MorganeTicketLoader/src/ticketloader/wdoo/app/CommandApp.java,v $
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author$
//  COPYRIGHT:      (c) 2006 by Wanadoo Espa�a, S.L., all rights reserved.
//  LOG:
//    $Log: CommandApp.java,v $
//    Revision 1.1  2006/09/26 13:14:23  ldiego
//    - This source file contains the common part of code for all command line applications.
//

package wdoo.app;

//... IMPORT SECTION .........................................................................................
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import wdoo.util.ResourceManager;

//... CLASS IMPLEMENTATION ...................................................................................
/**
 * @author Luis de Diego
 *
 */
public class CommandApp {

	/** Definicion de la configuracion y del logger a utilizar en esta aplicacion. */
	protected static Logger						log							= Logger.getLogger("wdoo.app"); //$NON-NLS-1$
	protected static String						errorCodePrefix	= "<undefined>.ERR";
	protected static ResourceManager	rManager;

	/**
	 * A configuration directory that is a requiered parameter is not specified.
	 */
	protected static final int				NOCONFIG				= 10;
	/**
	 * There is a configuration error. Probable parameter out of bounds.
	 */
	protected static final int				CONFIGERROR			= 11;
	/** There is missing a required configuration parameter. */
	protected static final int				NOREQUIREDPARAMETER			= 12;
	/**
	 * Unexpected exception. Error not expected at this point
	 */
	protected static final int				UNEXPECTED			= 99;

	protected String									configDirectory;
	protected Properties							props						= new Properties();

	public static void exit(int exitCode) {
		//		log.exiting(exitCode, rManager.getResource(errorCodePrefix + exitCode)); //$NON-NLS-1$
		System.exit(exitCode);
	}

	public static void SetErrorCodePrefix(String codePrefix) {
		errorCodePrefix = codePrefix;
	}

	protected void processParameters(String[] args, String ApplicationName) {
		// ... Process parameters and store them into the instance fields

		for (int i = 0; i < args.length; i++) {
			// ... Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].startsWith("-conf")) { //$NON-NLS-1$
					// ... Check argument array size before trying to get the parameter
					if (i + 1 < args.length) {
						this.configDirectory = args[i + 1];
					} else {
						// ... Exit point 10. No esta especificado el directorio de configuracion de la aplicacion.
						exit(NOCONFIG);
					}
				}
			}
		}
		//... Check that all required parameters have values.
		//... Check that all required parameters have values.
		if (null == this.configDirectory)
			exit(NOCONFIG);
		else {
			// ... Read the configuration properties. This are the fields than can be changed by an operator.
			try {
				props.load(new BufferedInputStream(new FileInputStream(new File(this.configDirectory,
						ApplicationName + ".properties"))));
			} catch (FileNotFoundException fnfe) {
				// ... Exit point 10. No esta especificado el directorio de configuracion de la aplicacion.
				log.throwing(ApplicationName, "processParameters", fnfe); //$NON-NLS-1$ //$NON-NLS-2$
				exit(NOCONFIG);
			} catch (IOException ioe) {
				// ... Exit point 10. No esta especificado el directorio de configuracion de la aplicacion.
				log.throwing(ApplicationName, "processParameters", ioe); //$NON-NLS-1$ //$NON-NLS-2$
				exit(NOCONFIG);
			}
		}
	}

	/**
	 * Open an output file for writing. The path to the file to ouput is received as a parameter and is a
	 * complete path to the file.
	 * 
	 * @param outputName
	 *          string with the path of thte file to open.
	 * @return a new <code>PrintWrriter</code> instance that references that file.
	 * @throws FileNotFoundException
	 *           is the file can not be opened.
	 */
	protected PrintWriter openOutput(String outputName) throws FileNotFoundException {
		OutputStream outputFile = new BufferedOutputStream(new FileOutputStream(new File(outputName)));
		return new PrintWriter(outputFile);
	}

	/**
	 * Verifies is a string paramter is <code>null</code> or has any contents.
	 * 
	 * @param string2Check
	 *          string value of the parameter to check
	 * @throws IllegalArgumentException -
	 *           Throws this exception if the parameter is null or has no contents.
	 */
	protected void verifyParameterNotNull(String string2Check) throws IllegalArgumentException {
		if (null == string2Check)
			throw new IllegalArgumentException("The parameter content is empty.");
		if ("" == string2Check)
			throw new IllegalArgumentException("The parameter content is empty.");
	}
}

//... UNUSED CODE ............................................................................................
