//  PROJECT:        net.sf.vgap4.projecteditor
//  FILE NAME:      $Id$
//  LAST UPDATE:    $Date$
//  RELEASE:        $Revision$
//  AUTHORS:        Luis de Diego (LDD) - luis_dediego@yahoo.com
//  COPYRIGHT:      $Copyright$

package es.ftgroup.gef.parts;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

import es.ftgroup.gef.model.AbstractNode;
import es.ftgroup.ui.figures.IFigureFactory;

import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ConnectionAnchor;
import org.eclipse.draw2d.IFigure;
import org.eclipse.gef.ConnectionEditPart;
import org.eclipse.gef.NodeEditPart;
import org.eclipse.gef.Request;

// - CLASS IMPLEMENTATION ...................................................................................
public abstract class AbstractNodeEditPart extends AbstractGenericEditPart implements NodeEditPart {
	private static Logger				logger	= Logger.getLogger("es.ftgroup.gef.parts");

	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	// private static final String CONSTANT_NAME = "CONSTANT_VALUE";

	// - F I E L D S
	protected ConnectionAnchor	anchor;

	// - M E T H O D - S E C T I O N ..........................................................................
	// - C O N S T R U C T O R S
	// public AbstractNodeEditPart() {
	// }

	// - G E T T E R S / S E T T E R S
	// - P U B L I C - S E C T I O N
	// - P R O T E C T E D - S E C T I O N
	private AbstractNode getCastedModel() {
		return (AbstractNode) getModel();
	}

	protected ConnectionAnchor getConnectionAnchor() {
		if (null == anchor) {
			anchor = new ChopboxAnchor(getFigure());
		}
		return anchor;
	}

	// - A B S T R A C T - S E C T I O N
	// - O V E R R I D E - S E C T I O N
	@Override
	protected IFigure createFigure() {
		IFigureFactory factory = this.getFactory().getFigureFactory();
		IFigure fig = factory.createFigure(this, this.getCastedModel());
		// fig.setOpaque(true);
		return fig;
	}

	@Override
	protected void createEditPolicies() {
		// EMPTY method body
	}

	// - I N T E R F A C E - N O D E E D I T P A R T
	public ConnectionAnchor getSourceConnectionAnchor(ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getSourceConnectionAnchor(Request request) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(ConnectionEditPart connection) {
		return getConnectionAnchor();
	}

	public ConnectionAnchor getTargetConnectionAnchor(Request request) {
		return getConnectionAnchor();
	}
}

// - UNUSED CODE ............................................................................................
