//  PROJECT:        com.osp.cfetools.WLLogAnalyzer
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@orange-ftgroup.com
//  COPYRIGHT:      (c) 2011 by Orange Spain - Billing Department - Customer Front End Support

package main;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
/**
 * Esta utilidad se encarga de procesar los logs de progreso de servicios realizados a operaciones de
 * middleware dentro de la arquitectura de framework de NEOS. Los datos proceden de archivos de log generados
 * por los diferentes servidores WebLogic y son trazas filtradas de todas las operaciones realizadas a traves
 * de los adaptadores de Middleware. Cada una de las lineas de traza tiene la siguiente estrucura de campos,
 * todos ellos separados por el separador " " (ESPACIO). <code>
 * --------------------------------------------------------------------------------
 * Started                     ms     %  Task name
 * --------------------------------------------------------------------------------
 * 2011-03-16 23:59:59,752  00277  100%  M655362307 # GetProfile # 26027
* </code>
 * 
 * Los datos resultantes son los datos agregados segun las siguientes definiciones.
 * <ol>
 * <li>En función del parametro <code>BARMINUTES</code> las agregaciones se empaquetan en el tiempo en minutos
 * indicado por este parametro. El minimo timepo es 1 y corresponde a un minuto. Cada elemento de agregación
 * comienza en el minuto del primer registro y agrega durante el numero de minutos especificado.</li>
 * <li>Para cada segmento de agregación se obtienen los siguientes datos:<br>
 * <dl>
 * <dt>FECHA</dt>
 * <dd>Fecha del segmento de tiempo de agregacion.</dd>
 * <dt>HORA</dt>
 * <dd>Hora del segmento de agregación. Corresponde con el primer minuto del segmento. Es posible que un
 * parametro adicional indicado por el separador <code>+</code> seguido de un numero indique el numero de
 * minutos en el segmento.</dd>
 * <dt>OPERACION</dt>
 * <dd>Texto descriptivo de la operacion middleware o de la funcion registrada.</dd>
 * <dt>CONTADOR</dt>
 * <dd>Contador de llamadas a esta funcion realizadas en el periodo agregado.</dd>
 * <dt>T. MINIMO</dt>
 * <dd>Tiempo minimo de la operacion en el periodo expresado en milisegundos.</dd>
 * <dt>T. MAXIMO</dt>
 * <dd>Tiempo maximo de la operacion en el periodo expresado en milisegundos.</dd>
 * <dt>T. PROMEDIO</dt>
 * <dd>Tiempo medio de la operacion. Este tiempo es la media aritmetica de la suma de todos los tiempos
 * deividida por el numero de llamadas.</dd>
 * </dl>
 * </li>
 * </ol>
 * 
 * @author Luis de Diego
 * 
 */
public class WLLogAnalyzerCommandApp extends BaseCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger										logger					= Logger.getLogger("main");
	private static final String							APPLICATIONNAME	= "WLLogAnalyzerCommandApp";
	private static WLLogAnalyzerCommandApp	singleton;
	private static boolean									onDebug					= false;
	static {
		WLLogAnalyzerCommandApp.logger.setLevel(Level.ALL);
	}

	// - M A I N - S E C T I O N
	/**
	 * Standard and base initialization for an instance. The application starts by performing the
	 * <code>execute</code> core method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// - The startup and initialization process must be as light as possible to allow all inherithed code
		// to be executed instead making a lot of calls.
		singleton = new WLLogAnalyzerCommandApp(args);
		singleton.execute();
		exit(0);
	}

	// - F I E L D - S E C T I O N ............................................................................
	private int			spanMinutes		= 5;
	private String	sourceLogFile	= null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	/*
	 * The constructor is the method that performs the instance initialization and the parameter processing. The
	 * parameters accepted and processed by this application are: <ul> <li><b>-conf<font
	 * color="GREY">[igurationDirectory]</font></b> ${CONFIGDIR} - sets the directory where the application will
	 * expect the configuration files and data.</li> <li><b>-res<font color="GREY">[ourcesLocation</font></b>
	 * ${RESOURCEDIR} - is the directory where the application is going to locate the files that contains the
	 * SQL statements and other application resources.
	 */
	public WLLogAnalyzerCommandApp(final String[] args) {
		// INFO The initialization process follows this rules:
		// - The instance is created and this constructor is called.
		// - Log must be initialized and the application banner printed to signal the start of the process.
		// - Parameter processing is the next task. Call the hierarchy to process any common parameters and
		// then process internally all parameters left.
		// - Parameter validations for mandatory parameters and any other parameter processing to generate
		// configuration data from the parameter data such as file reading or parameter concatenation.
		// After this steps the application instance is ready to start. The next step is the initialization that
		// maybe can be included inside this method or moved away if there are functionalities than can be
		// inherithed.
		// super(args);

		// - Store the parameters received on this invocation into the instance for method availability.
		// this.parameters = args;
		// - Initialize log and print out the banner
		banner();

		// - Process parameters and store them into the instance fields
		processParameters(args, APPLICATIONNAME);
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	/*
	 * Starts a new thread with the autopilot configuration file. From that file the pilot gets all the date
	 * needed to run the boat.
	 */
	public void execute() {
// - Open the log file and start the processing searching for the right pattern.
		try {
			BufferedReader log = new BufferedReader(new FileReader(new File(sourceLogFile)));
		String	line = log.readLine();
			while (null!=line)
				if (checkIfProcesable(line)) lineData = line.split(regex, limit)
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void processParameters(final String[] args, final String ApplicationName) {
		// super.processParameters(args, ApplicationName);
		for (int i = 0; i < args.length; i++) {
			logger.info("Application argument: args[" + i + "] = " + args[i]);
			// - Test all parameters that start with the character '-'. They are the parameter codes
			if (args[i].startsWith("-")) { //$NON-NLS-1$
				if (args[i].toLowerCase().startsWith("-bar")) // - Get the number of minutes to aggregate
					spanMinutes = argumentIntegerValue(args, i);
				if (args[i].toLowerCase().startsWith("-sourcefile")) // - Get the log file to process
					sourceLogFile = argumentStringValue(args, i);
				if (args[i].toLowerCase().startsWith("-debug")) { //$NON-NLS-1$
					onDebug = true;
					logger.setLevel(Level.ALL);
					continue;
				}
				if (args[i].toLowerCase().startsWith("-help")) { //$NON-NLS-1$
					help();
					exit(0);
				}
			}
		}
		// - Check that required parameters have values.
		if (null == sourceLogFile) exit(NOCONFIG);
	}

	private void banner() {
		System.out.println("__     _____  ____   ____    _         _              _ _       _   ");
		System.out.println("\\ \\   / / _ \\|  _ \\ / ___|  / \\  _   _| |_ ___  _ __ (_) | ___ | |_ ");
		System.out.println(" \\ \\ / / | | | |_) | |  _  / _ \\| | | | __/ _ \\| '_ \\| | |/ _ \\| __|");
		System.out.println("  \\ \\ /| |_| |  _ <| |_| |/ ___ \\ |_| | || (_) | |_) | | | (_) | |_ ");
		System.out.println("   \\_/  \\___/|_| \\_\\\\____/_/   \\_\\__,_|\\__\\___/| .__/|_|_|\\___/ \\__|");
		System.out.println("                                               |_|                  ");
		System.out.println();
		System.out.println("Version Beta 0.1.0 16/03");
		System.out.println();
	}

	private void help() {
		System.out.println("Description:");
		System.out.println("   Tool to process and aggregate log data from WL MDWR operation calls.");
		System.out.println();
		System.out.println("Command API for the " + APPLICATIONNAME + ":");
		System.out.println("   java -classpath wlloganalyzer.0.1.0.jar main." + APPLICATIONNAME);
		System.out.println("Allowed parameters:");
		System.out.println("    -bar[minutes]  -- number of minutes in the aggregation block.");
		System.out
				.println("    -sourcedir  -- directory where there are located the logs files to process. If this command"
						+ "is specified the we have to define also the sourcepattern parameter.");
		System.out.println("    -sourcefile  -- the name of the log file to be processed.");
		System.out.println("    -debug  -- activated the debugging mode.");
		System.out.println("    -help  -- shown this help text.");
		System.out.println();
	}
}

// - UNUSED CODE ............................................................................................
