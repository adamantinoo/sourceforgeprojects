//  PROJECT:        com.osp.cfetools.WLLogAnalyzer
//  FILE NAME:      $Id: ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2011 by LDD Game Development Spain, all rights reserved.

package main;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class BaseCommandApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("main");

	public static void exit(final int exitCode) {
		System.exit(exitCode);
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public BaseCommandApp() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	protected double argumentDoubleValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final double value = new Double(argument).doubleValue();
		return value;
	}

	protected int argumentIntegerValue(final String[] args, final int position) {
		// - Get the next argument.
		final String argument = argumentStringValue(args, position);
		final int value = new Integer(argument).intValue();
		return value;
	}

	protected String argumentStringValue(final String[] args, final int position) {
		// - Check argument array size before trying to get the argument value
		if (position + 1 < args.length)
			return args[position + 1];
		else {
			// - Exit point 10. There are no enough arguments in the list to find a value.
			VORGAutopilot.exit(VORGConstants.NOCONFIG);
		}
		return "";
	}
}

// - UNUSED CODE ............................................................................................
