//  PROJECT:        com.osp.swfcfe.ecare.SNMPProofOfConcept
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package main;

// - IMPORT SECTION .........................................................................................
import java.util.logging.Logger;

// - CLASS IMPLEMENTATION ...................................................................................
public class SNMPProofOfConceptApp {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("main");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SNMPProofOfConceptApp() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// - Create the SNMP Client manager and start listening.

	}
}

// - UNUSED CODE ............................................................................................
