//  PROJECT:        com.osp.dsi.cfe.eCareAnaliticsProcessor
//  FILE NAME:      $Id: IconGeneratorTest.java 174 2008-06-26 12:59:47Z boneymen $
//  LAST UPDATE:    $Date: 2008-06-26 14:59:47 +0200 (jue, 26 jun 2008) $
//  RELEASE:        $Revision: 174 $
//  AUTHORS:        Lima Delta Delta (LDD) - boneymen@netscape.com
//  COPYRIGHT:      (c) 2009 by LDD Game Development Spain, all rights reserved.

package com.osp.test;

// - IMPORT SECTION .........................................................................................
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;

// - CLASS IMPLEMENTATION ...................................................................................
public class FileListTestUnit extends TestCase {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("com.osp.test");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FileListTestUnit() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testFileSearch() throws Exception {
		try {
			// - Read the environment properties to locate the TEMP path
			Properties environment = getEnvVars();
			String tempPath = (String) environment.get("TEMP");
			String initialPath = tempPath;
			String[] extensions = { "php", "tmp" };
			File root = new File(initialPath);

			// String[] extensions = { "xml", "java", "dat" };
			boolean recursive = true;

			//
			// Finds files within a root directory and optionally its
			// subdirectories which match an array of extensions. When the
			// extensions is null all files will be returned.
			//
			// This method will returns matched file as java.io.File
			//
			Collection files = FileUtils.listFiles(root, extensions, recursive);

			for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
				File file = iterator.next();
				System.out.println("File = " + file.getAbsolutePath());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Properties getEnvVars() throws Throwable {
		Process p = null;
		Properties envVars = new Properties();
		Runtime r = Runtime.getRuntime();
		String OS = System.getProperty("os.name").toLowerCase();
		// System.out.println(OS);
		if (OS.indexOf("windows 9") > -1)
			p = r.exec("command.com /c set");
		else if ((OS.indexOf("nt") > -1) || (OS.indexOf("windows 2000") > -1) || (OS.indexOf("windows xp") > -1)) // thanks
			// to
			// JuanFran
			// for
			// the
			// xp
			// fix!
			p = r.exec("cmd.exe /c set");
		else
			// our last hope, we assume Unix (thanks to H. Ware for the fix)
			p = r.exec("env");
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = br.readLine()) != null) {
			int idx = line.indexOf('=');
			String key = line.substring(0, idx);
			String value = line.substring(idx + 1);
			envVars.setProperty(key, value);
			// System.out.println( key + " = " + value );
		}
		return envVars;
	}

}
// - UNUSED CODE ............................................................................................
